
module.exports = function () {
    var instanceRoot = ".\\build";
    var config = {
        websiteRoot: instanceRoot + "\\Website",
        sitecoreLibraries: instanceRoot + "\\Website\\bin",
        licensePath: instanceRoot + "\\Data\\license.xml",
        solutionName: "SkinAlliance",
        buildConfiguration: "Debug",
        runCleanBuilds: false,
        directories: {
          src: './src/',
          featureDirectory: './src/Feature/',
          projectDirectory: './src/Project/',    
          buildDirectory: './build/',
	     themeBuildDirectory:'./build/Website/themes/'
        },
        currentWebsite: 'SkinAlliance',
        autoPrefixerBrowsers: ['last 2 versions', 'ie >= 10', 'Safari >= 9', 'iOS >= 8'],
        bundle: {
          cssBundleName: 'bundle.css',
    jsBundleName: 'bundle.js',
    jsMapName: 'bundle.map.js',
        }
    };
  return config;
}