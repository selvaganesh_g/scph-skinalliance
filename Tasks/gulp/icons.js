const glob = require('glob');
const es = require('event-stream');
const svgSprite = require('gulp-svg-sprite');
const imagemin = require('gulp-imagemin');

const config = require('../config')();

const svgSpriteConfig = {
  mode: {
    symbol: {
      dest: 'sprite/',
      sprite: 'sprite.svg',
    },
  },
};

module.exports = function (gulp) {
  return function () {
    return glob(`${config.directories.projectDirectory}**/code/**/Icons`, (err, files) => {

      const tasks = files.map((iconsDirectoryPath) => {
        return gulp.src(`${iconsDirectoryPath}/*.svg`)
          .pipe(imagemin([
            imagemin.svgo({ plugins: [{ removeStyleElement: true }] })
          ]))
          .pipe(svgSprite(svgSpriteConfig))
          .pipe(gulp.dest(`${iconsDirectoryPath}/../images`));
      });

      // create a merged stream
      return es.merge.apply(null, tasks);
    });
  };
};
