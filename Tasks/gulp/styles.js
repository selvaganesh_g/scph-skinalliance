const production = require('gulp-environments').production;

const glob = require('glob');
const es = require('event-stream');
const sass = require('gulp-sass');
const concat = require('gulp-concat');

const cleanCSS = require('gulp-clean-css');
const postcss      = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const config = require("../config.js")();
const utils = require('./utils');
module.exports = function (gulp) {
  return function () {
    return glob(`${config.directories.projectDirectory}**/entry.scss`, (err, files) => {      
      const tasks = files.map((entry) => {
        const themeName = utils.getThemeName(entry); 
		const themeDir = config.directories.themeBuildDirectory + themeName + '/styles';
        return gulp.src([`${config.directories.featureDirectory}**/*.scss`, entry])
          //.pipe(sourcemaps.init())
          .pipe(sass().on('error', sass.logError))
          .pipe(concat(config.bundle.cssBundleName))
          .pipe(postcss([ 
            autoprefixer({ browsers: config.autoPrefixerBrowsers })
          ]))
          .pipe(production(cleanCSS()))
          //.pipe(sourcemaps.write('./'))
          .pipe(gulp.dest(themeDir));
      });

      // create a merged stream
      return es.merge.apply(null, tasks);
    });
  };
};
