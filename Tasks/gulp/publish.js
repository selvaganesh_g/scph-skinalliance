﻿const gulp = require('gulp');
const msbuild = require("gulp-msbuild");
const exec = require('child_process').exec;
const plugins = require('gulp-load-plugins')();
const debug = require("gulp-debug");
const foreach = require("gulp-foreach");
const rename = require("gulp-rename");
const watch = require("gulp-watch");
const merge = require("merge-stream");
const newer = require("gulp-newer");
const util = require("gulp-util");
const runSequence = require("run-sequence");
const path = require("path");
const config = require("../config.js")();
const nugetRestore = require('gulp-nuget-restore');
const fs = require('fs');
const unicorn = require("../../scripts/unicorn.js");
const habitat = require("../../scripts/refapp.js");
const yargs = require("yargs").argv;

module.exports.config = config;

var publishStream = function (stream, dest) {
    var targets = ["Build"];

    return stream
      .pipe(debug({ title: "Building project:" }))
      .pipe(msbuild({
          targets: targets,
          configuration: config.buildConfiguration,
          logCommand: false,
          verbosity: "minimal",
          stdout: true,
          errorOnFail: true,
          maxcpucount: 0,
          toolsVersion: 14.0,
          properties: {
              DeployOnBuild: "true",
              DeployDefaultTarget: "WebPublish",
              WebPublishMethod: "FileSystem",
              DeleteExistingFiles: "false",
              publishUrl: dest,
              _FindDependencies: "false"
          }
      }));
}

var publishProject = function (location, dest) {
    dest = dest || config.websiteRoot;

    console.log("publish to " + dest + " folder");
    return gulp.src(["./src/" + location + "/code/*.csproj"])
      .pipe(foreach(function (stream, file) {
          return publishStream(stream, dest);
      }));
}

var publishTestProjects = function (location, dest) {
    dest = dest || config.websiteRoot;

    console.log("publish to " + dest + " folder");
    return gulp.src([location + "/**/test/*.csproj"])
      .pipe(foreach(function (stream, file) {
          return publishStream(stream, dest);
      }));
};

var publishProjects = function (location, dest) {
    dest = dest || config.websiteRoot;

    console.log("publish to " + dest + " folder");
    return gulp.src([location + "/**/code/*.csproj"])
      .pipe(foreach(function (stream, file) {
          return publishStream(stream, dest);
      }));
};

gulp.task("Build-Solution", function () {
    var targets = ["Build"];
    if (config.runCleanBuilds) {
        targets = ["Clean", "Build"];
    }
    var solution = "./" + config.solutionName + ".sln";
    return gulp.src(solution)
        .pipe(msbuild({
            targets: targets,
            configuration: config.buildConfiguration,
            logCommand: false,
            verbosity: "minimal",
            stdout: true,
            errorOnFail: true,
            maxcpucount: 0,
            toolsVersion: 14.0
        }));
});

gulp.task("Publish-Foundation-Projects", function () {
    return publishProjects("./src/Foundation");
});

gulp.task("Publish-Feature-Projects", function () {
    return publishProjects("./src/Feature");
});

gulp.task("Publish-Feature-Test-Projects", function () {
    return publishTestProjects("./src/Feature");
});

gulp.task("Publish-Project-Projects", function () {
    return publishProjects("./src/Project");
});

gulp.task("Publish-Project", function () {
    if (yargs && yargs.m && typeof (yargs.m) == 'string') {
        return publishProject(yargs.m);
    } else {
        throw "\n\n------\n USAGE: -m Layer/Module \n------\n\n";
    }
});

gulp.task("Publish-Assemblies", function () {
     var root = "./src";
    var roots = [root + "/**/bin","!" + root +"**/roslyn" ];
    var files = "/**/*.dll";
    var destination = config.websiteRoot + "\\bin";
    return gulp.src(roots, { base: root }).pipe(
      foreach(function (stream, file) {
          console.log("Publishing from " + file.path);
          gulp.src(file.path + files, { base: file.path })
            .pipe(newer(destination))
            .pipe(debug({ title: "Copying " }))
            .pipe(gulp.dest(destination));
          return stream;
      })
    );
});

gulp.task("Publish-All-Views", function () {
    var root = "./src";
    var roots = [root + "/**/Views", "!" + root + "/**/obj/**/Views"];
    var files = "/**/*.cshtml";
    var destination = config.websiteRoot + "\\Views";
    return gulp.src(roots, { base: root }).pipe(
      foreach(function (stream, file) {
          console.log("Publishing from " + file.path);
          gulp.src(file.path + files, { base: file.path })
            .pipe(newer(destination))
            .pipe(debug({ title: "Copying " }))
            .pipe(gulp.dest(destination));
          return stream;
      })
    );
});

gulp.task("Publish-All-Configs", function () {
    var root = "./src";
    var roots = [root + "/**/App_Config", "!" + root + "/**/obj/**/App_Config"];
    var files = "/**/*.config";
    var destination = config.websiteRoot + "\\App_Config";
    return gulp.src(roots, { base: root }).pipe(
      foreach(function (stream, file) {
          console.log("Publishing from " + file.path);
          console.log("Publishing To" + destination );
          gulp.src(file.path + files, { base: file.path })
            .pipe(newer(destination))
            .pipe(debug({ title: "Copying " }))
            .pipe(gulp.dest(destination));
          return stream;
      })
    );
});

gulp.task("Sync-Unicorn", function (callback) {
    var options = {};
    options.siteHostName = habitat.getSiteUrl();
    options.authenticationConfigFile = config.websiteRoot + "/App_config/Include/Unicorn/Unicorn.UI.config";

    unicorn(function () { return callback() }, options);
});