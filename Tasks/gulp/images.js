const production = require('gulp-environments').production;

const glob = require('glob');
const es = require('event-stream');
const imagemin = require('gulp-imagemin');

const config = require('../config')();
const utils = require('./utils');

module.exports = function (gulp) {
  return function () {
    return glob(`${config.directories.projectDirectory}**/code/**/images`, (err, files) => {

      const tasks = files.map((imagesDirectoryPath) => {
        const themeName = utils.getThemeName(imagesDirectoryPath);

        return gulp.src(`${imagesDirectoryPath}/**/*`)
          .pipe(production(imagemin()))
          .pipe(gulp.dest(config.directories.themeBuildDirectory + themeName + '/images'));
      });

      // create a merged stream
      return es.merge.apply(null, tasks);
    });
  };
};
