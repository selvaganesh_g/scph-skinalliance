const fs = require('fs');

module.exports = {

  getFilesFromDirectory: function getFiles(dir, files_) {
    files_ = files_ || [];
    var files = fs.readdirSync(dir);
    for (var i in files) {
      var name = dir + '/' + files[i];
      if (fs.statSync(name).isDirectory()) {
        getFiles(name, files_);
      } else {
        files_.push(name);
      }
    }
    return files_;
  },

  getThemeName: (path) => {
    return path.split('/')[3];
  },

};
