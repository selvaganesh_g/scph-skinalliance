// Config that can be loaded externally, similar
// to [gulp-starter](https://github.com/greypants/gulp-starter)
// Gulp + Browserify recipe
// ------------------------
// Includes react JSX, coffeescript, uglify & sourcemaps
// Supports multiple input & output files

const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const browserify = require('browserify');
const globby = require('globby');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const config = require("../config.js")();
const exorcist = require('exorcist');
const production = require('gulp-environments').production;
const es = require('event-stream');
const utils = require('./utils');
let FeaturesFondationsSrc;
/**
 * on get the paths of all code in feature and foundation level
 * @param paths
 */
let onGetFondationFeatureCode = (paths) => {
	FeaturesFondationsSrc = paths;
	return globby(['./src/Feature/**/entry.js','./src/Project/**/entry.js','./src/Foundation/**/entry.js'])
		.then(onGetProjectcode, onError);
}

/**
 * on getting the paths of all code in project level
 * @param paths
 */
let onGetProjectcode = (files) => {
	const tasks = files.map((entry) => {
	const themeName = utils.getThemeName(entry);
    const themeDir = config.directories.themeBuildDirectory + themeName + '/scripts';
		return browserify({
			entries: [...FeaturesFondationsSrc, entry],
			ignore: '!**/*.spec.js',
			debug: true,
		})
			.transform('babelify')
			.bundle()
			//.pipe(exorcist(`${themeDir}/${config.bundle.jsMapName}`))
			.pipe(source(config.bundle.jsBundleName))
			.pipe(production(buffer()))
			.pipe(production(uglify()))
			.pipe(gulp.dest(themeDir));
	});

	// create a merged stream
	return es.merge.apply(null, tasks);
}
/**
 * On get Error on promess of this task
 * @param err
 */
let onError = (err) => {
	console.error("onError", err);
}

module.exports = function (gulp) {
	return function () {
		return globby([			
			'./src/Feature/**/entry.js','./src/Project/**/entry.js','./src/Foundation/**/entry.js'])
			.then(onGetFondationFeatureCode, onError);
	}
};
