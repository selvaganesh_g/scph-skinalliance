const glob = require('glob');
const es = require('event-stream');

const config = require('../config')();
const utils = require('./utils');

module.exports = function (gulp) {
  return function () {
    return glob(`${config.directories.projectDirectory}**/code/**/fonts`, (err, files) => {
      const tasks = files.map((fontsDirectoryPath) => {
        const themeName = utils.getThemeName(fontsDirectoryPath);

        return gulp.src(`${fontsDirectoryPath}**/*`)
          .pipe(gulp.dest(config.directories.themeBuildDirectory + themeName));
      });

      // create a merged stream
      return es.merge.apply(null, tasks);
    });
  };
};
