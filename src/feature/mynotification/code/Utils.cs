﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.MyNotification
{
    using Managers;
    using Sitecore.Data.Items;
    using Sitecore.Security.Accounts;

    public static class Utils
    {
        public static string GetUserid()
        {
            return ItemUtil.ProposeValidItemName(User.Current?.Profile?.StartUrl, "");
        }
        public static DateTime? GetDuteDate(string userid, NotificationType notification)
        {
            var usersettings = NotificationManager.Get(userid);
            if (usersettings == null) return null;
            NotificationSendType type = NotificationSendType.Never;
            switch (notification)
            {
                case NotificationType.FollowUser://One of your contacts is now following a new user
                    type = usersettings.NotificationOne;
                    break;
                case NotificationType.PostInCategory://New content in a followed category
                    type = usersettings.NotificationTwo;
                    break;
                case NotificationType.CommentInteract://Someone interacts with your post
                case NotificationType.LikeInteract:
                    type = usersettings.NotificationThree;
                    break;
                case NotificationType.Like://New comment in a post shared, liked or commented
                case NotificationType.Comment:
                    type = usersettings.NotificationFour;
                    break;
                case NotificationType.PostByUser: //One of your contacts add a new post
                    type = usersettings.NotificationFive;
                    break;
            }
            return DueDate(type);
        }
        private static DateTime? DueDate(NotificationSendType type)
        {
            DateTime? value = null;
            switch (type)
            {
                case NotificationSendType.Daily:
                    value = System.DateTime.Now.AddDays(1);
                    break;
                case NotificationSendType.Immediately:
                    value = System.DateTime.Now;
                    break;
                case NotificationSendType.Weekly:
                    value = System.DateTime.Now.AddDays(7);
                    break;
            }
            return value;
        }
    }
}