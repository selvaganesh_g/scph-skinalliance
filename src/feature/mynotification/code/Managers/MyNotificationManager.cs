﻿using Loreal.Feature.MyNotification.DAL;
using Loreal.Feature.MyNotification.Model;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Loreal.Feature.MyNotification.Managers
{
    public static class MyNotificationManager
    {
        public static Item Setting()
        {
            Item SettingsItem = ServiceLocator.Current.Resolve<IMultisiteContext>().SettingsItem;
            if (SettingsItem == null)
            {
                SettingsItem = ServiceLocator.Current.Resolve<IMultisiteContext>()?.GetSettingsItem(Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath));
            }
            Item item = SettingsItem?.Children.FirstOrDefault(x => x.TemplateID == MyNotificationTemplate.ID);
            if (item != null)
            {
                return item;
            }
            Sitecore.Diagnostics.Log.Error("Sitecore MyNotification Item missing under Settings!", "My Notification");
            return null;
        }
        public static Model.UserNotifications Get(long Id)
        {
            try
            {
                return ContextData.CurrentInstance.UserNotifications.Items.Where(x => x.Id == Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info(ex.Message, "User Notification Manager - Get");
                return null;
            }
        }
        public static void Update()
        {
            try
            {
                ContextData.CurrentInstance.SaveChanges();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info(ex.Message, "User Notification Manager - Update");
            }
        }
        public static IQueryable<Model.UserNotifications> List()
        {
            try
            {
                return ContextData.CurrentInstance.UserNotifications.Items.Where(x => x.DueDate <= System.DateTime.Now && !x.Done).OrderByDescending(x => x.NotifyUserid);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info(ex.Message, "User Notification Manager - List");
                return null;
            }
        }
        public static List<Timeline> Timeline(int offset, List<NotificationType> types, out int count)
        {
            var currentUserId = Utils.GetUserid();
            List<Timeline> timelinedata = new List<Timeline>();
            count = 0;
            try
            {
                if (!string.IsNullOrEmpty(currentUserId))
                {
                    var usernotificaton = ContextData.CurrentInstance.UserNotifications.Items.Where(x => x.NotifyUserid == currentUserId);
                    if (types != null && types.Count != 0)
                    {
                        usernotificaton = usernotificaton.Where(x => types.Contains(x.Type));
                    }
                    var orderby = usernotificaton.OrderByDescending(x => x.Date).Skip(offset * 6).Take(6).ToList();
                    foreach (var notify in orderby)
                    {
                        var date = notify.Date.ToString("MM/dd/yy");
                        if (notify.Date.Date == DateTime.Now.Date) { date = Setting()?.Fields[MyNotificationTemplate.Other.TodayText].Value; }
                        else if ((notify.Date.Date.AddDays(1) == DateTime.Now.Date)) { date = Setting()?.Fields[MyNotificationTemplate.Other.YesterdayText].Value; }
                        timelinedata.Add(new Timeline()
                        {
                            ImageUrl = notify.Image,
                            Message = notify.Message,
                            Type = notify.Type,
                            Date = string.Format("{0} at <span class=\"timeline-time-format\" >{1}</span>", date, notify.Date.ToString("hh:mm tt"))
                        });
                    }
                    count = usernotificaton.Count();
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info(ex.Message, "User Notification Manager - List");
            }

            return timelinedata;
        }
        public static void Add(Model.UserNotifications notification)
        {
            try
            {

                string message = string.Empty;
                switch (notification.Type)
                {
                    case NotificationType.FollowUser:
                        message = string.Format(Setting()?.Fields[MyNotificationTemplate.NotificationOne.Message].Value, notification.Name, notification.Message);
                        break;
                    case NotificationType.PostInCategory:
                        message = string.Format(Setting()?.Fields[MyNotificationTemplate.NotificationTwo.Message].Value, notification.Name, notification.Message);
                        break;
                    case NotificationType.LikeInteract:
                        message = string.Format(Setting()?.Fields[MyNotificationTemplate.NotificationThree.MessageLike].Value, notification.Name, notification.Message);
                        break;
                    case NotificationType.CommentInteract:
                        message = string.Format(Setting()?.Fields[MyNotificationTemplate.NotificationThree.MessageComment].Value, notification.Name, notification.Message);
                        break;
                    case NotificationType.CommentReplyInteract:
                        message = string.Format(Setting()?.Fields[MyNotificationTemplate.NotificationThree.MessageReply].Value, notification.Name, notification.Message);
                        break;
                    case NotificationType.Like:
                        message = string.Format(Setting()?.Fields[MyNotificationTemplate.NotificationFour.MessageLike].Value, notification.Name, notification.Message);
                        break;
                    case NotificationType.Comment:
                        message = string.Format(Setting()?.Fields[MyNotificationTemplate.NotificationFour.MessageComment].Value, notification.Name, notification.Message);
                        break;
                    case NotificationType.CommentReply:
                        message = string.Format(Setting()?.Fields[MyNotificationTemplate.NotificationFour.MessageReply].Value, notification.Name, notification.Message);
                        break;
                    case NotificationType.PostByUser:
                        message = string.Format(Setting()?.Fields[MyNotificationTemplate.NotificationFive.Message].Value, notification.Name, notification.Message);
                        break;
                }
                notification.Message = message;
                ContextData.CurrentInstance.UserNotifications.Add(notification);
                ContextData.CurrentInstance.SaveChanges();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info(ex.Message, "User Notification Manager - Add");
            }
        }
    }
}