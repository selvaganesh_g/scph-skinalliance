﻿using Loreal.Feature.MyNotification.DAL;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Loreal.Feature.MyNotification.Managers
{
    public static class NotificationManager
    {
        public static Model.Notification Get(string userId)
        {
            try
            {
                return ContextData.CurrentInstance.Notifications.Items.Where(x => x.UserId == userId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info(ex.Message, "Notification Manager -Get");
                return null;
            }
        }
        public static Model.Notification Get(long Id)
        {
            try
            {
                return ContextData.CurrentInstance.Notifications.Items.Where(x => x.Id == Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info(ex.Message, "Notification Manager -Get by Id");
                return null;
            }
        }
        public static void Update()
        {
            try
            {
                ContextData.CurrentInstance.SaveChanges();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info(ex.Message, "Notification Manager - Update");
            }
        }
        public static List<Model.Notification> List()
        {
            try
            {
                return ContextData.CurrentInstance.Notifications.Items.ToList();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info(ex.Message, "Notification Manager - List");
                return null;
            }
        }
        public static void Add(Model.Notification notification)
        {
            try
            {
                ContextData.CurrentInstance.Notifications.Add(notification);
                ContextData.CurrentInstance.SaveChanges();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info(ex.Message, "Notification Manager - Add");
            }
        }
    }
}