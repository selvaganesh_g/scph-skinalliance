﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.MyNotification
{
    public enum NotificationType
    {
        Like = 0, //31
        LikeInteract = 1, //41
        Comment = 2, //32
        CommentInteract = 3, //42
        PostByUser = 4,//51
        FollowUser = 5,//11
        PostInCategory = 6,//21
        CommentReply = 7, //32
        CommentReplyInteract = 8, //42
    }
}