﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loreal.Feature.MyNotification.Managers;
using System.Text;

namespace Loreal.Feature.MyNotification.Tasks
{
    public class NotificationAgent
    {
        private readonly Database _master;
        private readonly string _mailsettingId;
        private Item Mailsettings;
        /// <summary>
        /// Constructor to get the database
        /// </summary>
        /// <param name="database">master database connection string name</param>
        public NotificationAgent(string database, string mailsettingid)
        {
            _master = Database.GetDatabase(database);
            _mailsettingId = mailsettingid;
        }
        public void Run()
        {
            Assert.IsNotNull(_master, "master datbase is null");
            Mailsettings = _master.GetItem(_mailsettingId);
            Assert.IsNotNull(Mailsettings, "Mail setting item is null");
            var subject = Mailsettings.Fields[EmailService.EmailSettings.Fields.MyNotificationSubject]?.Value;
            var body = Mailsettings.Fields[EmailService.EmailSettings.Fields.MyNotificatioTemplate]?.Value;
            var bodyDynamic = body.Split('$', '$')?[1];
            var pendingNotification = MyNotificationManager.List();
            foreach (var _pending in pendingNotification.GroupBy(x => x.NotifyUserid).ToList())
            {
                var userid = _pending.Key;
                if (string.IsNullOrEmpty(userid)) continue;
                var userSettings = NotificationManager.Get(userid);
                if (userSettings != null && userSettings.Email != null && userSettings.Live)
                {
                    StringBuilder mailBody = new StringBuilder();
                    foreach (var msg in _pending)
                    {
                        mailBody.AppendFormat(bodyDynamic.Replace("#message#", msg.Message).Replace("#date#", msg.Date.ToString("dd/MM/yyyy")));
                        msg.Done = true;
                    }                    
                    EmailService.Services.EmailService.SendNotification(Mailsettings,userSettings.Email, subject, mailBody.ToString());
                    MyNotificationManager.Update();
                }
            }

        }
    }
}