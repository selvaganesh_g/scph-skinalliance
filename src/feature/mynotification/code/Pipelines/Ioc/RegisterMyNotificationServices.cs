﻿
namespace Loreal.Feature.MyNotification.Pipelines.Ioc
{
    using Microsoft.Extensions.DependencyInjection;
    using Sitecore.XA.Foundation.IOC.Pipelines.IOC;
    using Repositories;
    public class RegisterMyNotificationServices : IocProcessor
    {
        public override void Process(IocArgs args)
        {
            args.ServiceCollection.AddTransient<IMyNotificationRepository, MyNotificationRepository>();
        }
    }
}