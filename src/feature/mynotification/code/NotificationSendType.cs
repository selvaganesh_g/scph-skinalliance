﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.MyNotification
{
    public enum NotificationSendType
    {
        Never = 0,
        Immediately = 1,
        Daily = 2,
        Weekly = 3
    }
}