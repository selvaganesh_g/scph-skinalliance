﻿using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Loreal.Feature.MyNotification.Repositories;
using Loreal.Feature.MyNotification.Model;

namespace Loreal.Feature.MyNotification.Controllers
{
    public class NotificationController : StandardController
    {
        private readonly IMyNotificationRepository _myNotificationRepository;
        public NotificationController(IMyNotificationRepository myNotificationRepository)
        {
            this._myNotificationRepository = myNotificationRepository;
        }
        [HttpPost]
        public ActionResult SaveSettings(Model.Notification notification)
        {
            bool isNew = false;
            var _notification = Managers.NotificationManager.Get(notification.UserId);
            if (_notification == null)
            {
                _notification = new Model.Notification();
                _notification.UserId = Sitecore.Security.Accounts.User.Current?.Profile?.StartUrl;
                _notification.Email = Sitecore.Security.Accounts.User.Current?.Profile?.Email;
                isNew = true;
            }
            _notification.Live = true;
            _notification.NotificationOne = notification.NotificationOne;
            _notification.NotificationTwo = notification.NotificationTwo;
            _notification.NotificationThree = notification.NotificationThree;
            _notification.NotificationFour = notification.NotificationFour;
            _notification.NotificationFive = notification.NotificationFive;
            if (isNew)
            {
                Managers.NotificationManager.Add(_notification);
            }
            else
            {
                Managers.NotificationManager.Update();
            }
            return this.Redirect(Request.Url.AbsoluteUri);

        }
        [HttpGet]
        public ActionResult List(int offset, string filter)
        {
            string[] ids = filter?.Split('|');
            List<NotificationType> types = new List<NotificationType>();
            int count;
            if (ids != null && ids.Count() != 0)
            {
                foreach (var id in ids)
                {
                    if (id == "0")
                    {
                        types.Add(NotificationType.Like); types.Add(NotificationType.LikeInteract);
                    }
                    else if (id == "1")
                    {
                        types.Add(NotificationType.Comment); types.Add(NotificationType.CommentInteract);
                    }
                    else if (id == "2")
                    {
                        types.Add(NotificationType.PostInCategory);
                    }
                    else if (id == "3")
                    {
                        types.Add(NotificationType.PostByUser);
                    }
                    else if (id == "4")
                    {
                        types.Add(NotificationType.FollowUser);
                    }

                }
            }
            var result = Managers.MyNotificationManager.Timeline(offset, types, out count);
            return Json(new TimelineList { Count = count, List = result }, JsonRequestBehavior.AllowGet);
        }
        protected override string GetIndexViewName()
        {
            return "~/views/skinalliance/feature/MyNotification/MyNotification.cshtml";
        }
        protected override object GetModel()
        {
            return _myNotificationRepository.GetModel();
        }
    }
}