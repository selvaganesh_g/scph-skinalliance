﻿using System.Web;
using Sitecore;
using Sitecore.Security.Authentication;
using Sitecore.XA.Foundation.Multisite.Extensions;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.Web.UI.WebControls;
using Sitecore.Security.Accounts;

namespace Loreal.Feature.MyNotification.Repositories
{
    public class MyNotificationRepository : ModelRepository, IMyNotificationRepository, IModelRepository,
        IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            Model.NotificationViewModel viewModel = new Model.NotificationViewModel();

            if (!string.IsNullOrEmpty(User.Current?.Profile?.StartUrl))
            {
                viewModel.Notification = Managers.NotificationManager.Get(User.Current?.Profile?.StartUrl);
                int count;
                viewModel.Timeline = Managers.MyNotificationManager.Timeline(0, null, out count);
                viewModel.TimelineCount = count;
            }
            if (viewModel.Notification == null)
            {
                viewModel.Notification = new Model.Notification();
            }
            return viewModel;
        }

    }
}