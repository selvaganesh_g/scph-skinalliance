﻿
namespace Loreal.Feature.MyNotification.Repositories
{
    using Sitecore.XA.Foundation.Mvc.Repositories.Base;
    using System.Web;

    public interface IMyNotificationRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
    }
}
