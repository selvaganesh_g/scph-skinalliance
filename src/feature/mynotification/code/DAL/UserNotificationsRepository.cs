﻿namespace Loreal.Feature.MyNotification.DAL
{
    internal class UserNotificationsRepository : WrappedDataRepository<Model.UserNotifications>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserNotificationsRepository"/> class.
        /// </summary>
        /// <param name="repo">The repo.</param>
        public UserNotificationsRepository(IDataRepository<Model.UserNotifications> repo) : base(repo)
        {

        }
    }
}