﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Loreal.Feature.MyNotification.DAL
{
    internal class DBContext : DbContext
    {
        public DBContext() : base("custom")
        {
            this.Database.CommandTimeout = 0;
        }

        public DbSet<Model.Notification> Notifications { get; set; }
        public DbSet<Model.UserNotifications> UserNotifications { get; set; }
    }
}