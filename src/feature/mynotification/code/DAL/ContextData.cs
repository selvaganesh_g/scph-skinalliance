﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.MyNotification.DAL
{
    internal class ContextData : IDisposable
    {
        private static readonly object _contextDataKey = new object();
        [ThreadStatic]
        private static ContextData _instance;
        public static ContextData CurrentInstance
        {
            get
            {
                ContextData inst;
                var context = System.Web.HttpContext.Current;
                if (context != null)
                {
                    if (context.Items.Contains(_contextDataKey))
                    { inst = context.Items[_contextDataKey] as ContextData; }
                    else
                    {
                        inst = new ContextData();
                        context.Items[_contextDataKey] = inst;
                    }
                }
                else
                {
                    _instance = _instance ?? new ContextData();
                    inst = _instance;
                }

                return inst;
            }
        }
        private ContextData()
        {
            this.Context = new DBContext();
            this.Context.Database.CommandTimeout = 0;
        }
        protected DBContext Context { get; set; }

        protected NotificationRepository _notifications;
        public NotificationRepository Notifications
        {
            get
            {
                _notifications = _notifications ?? new NotificationRepository(new EFDataRepository<Model.Notification>(this.Context));
                return _notifications;
            }
        }

        protected UserNotificationsRepository _userNotifications;
        public UserNotificationsRepository UserNotifications
        {
            get
            {
                _userNotifications = _userNotifications ?? new UserNotificationsRepository(new EFDataRepository<Model.UserNotifications>(this.Context));
                return _userNotifications;
            }
        }

        public int SaveChanges()
        {
            return this.Context.SaveChanges();
        }
        public void Dispose()
        {
            if (ContextData._instance == this)
            {
                ContextData._instance = null;
            }
            this.Context.Dispose();
        }
    }
}