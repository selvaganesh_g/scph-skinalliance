﻿namespace Loreal.Feature.MyNotification.DAL
{
    internal class NotificationRepository : WrappedDataRepository<Model.Notification>
    {
        public NotificationRepository(IDataRepository<Model.Notification> repo) : base(repo)
        {

        }
    }
}