﻿using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.MyNotification
{
    public struct MyNotificationTemplate
    {
        public static readonly ID ID = new ID("{776A3832-BD98-45A1-9F79-2DE1A659BC45}");
        public struct NotificationOne
        {
            public static readonly ID Title = new ID("{99247B83-C0C0-4A2D-B6E8-3F1BB0B869E8}");
            public static readonly ID Message = new ID("{ACB56E07-21A2-4B4F-AF31-5491BDFBCE43}");
        }
        public struct NotificationTwo
        {
            public static readonly ID Title = new ID("{A51DDA8E-AFD5-4305-946F-5FF6E4E3BE82}");
            public static readonly ID Message = new ID("{2151BCD5-8170-491C-84A6-C1DFBC3B402E}");
        }
        public struct NotificationThree
        {
            public static readonly ID Title = new ID("{1E81C76F-2ADF-4180-B51A-C9B6FF5710F7}");
            public static readonly ID MessageLike = new ID("{AD689D17-A6D7-4E9A-B230-37182114899D}");
            public static readonly ID MessageComment = new ID("{224259B4-F92F-4614-B1EE-77F8EF939D7C}");
            public static readonly ID MessageReply = new ID("{128DE35F-8E33-4A02-88CF-EB6C77EE07DE}");
        }
        public struct NotificationFour
        {
            public static readonly ID Title = new ID("{01E2C638-F3B1-4EB3-9D63-89C4C6E0D81B}");
            public static readonly ID MessageLike = new ID("{C233F6ED-933E-43AA-9BA0-C026BA17A05E}");
            public static readonly ID MessageComment = new ID("{B496B68E-9CBC-4C1A-B2C8-53E658A1D75C}");
            public static readonly ID MessageReply = new ID("{42AEAC8A-F764-4424-A556-B5D192B0883A}");
        }
        public struct NotificationFive
        {
            public static readonly ID Title = new ID("{D13348DC-F74E-47CB-A69D-963BCDA4B891}");
            public static readonly ID Message = new ID("{4CD7B6EC-9F07-4B4A-A2E1-D16CC9504766}");
        }
        public struct Filter
        {
            public static readonly ID Label = new ID("{BC684B38-A515-40D2-9B48-B485262B2FD2}");
            public static readonly ID Like = new ID("{38C0B15B-40EA-4CF6-8AB5-DD93D118F7C1}");
            public static readonly ID Comment = new ID("{EFA31E17-7BD8-4147-9376-26E5D1B76413}");
            public static readonly ID PostInFollowedCategory = new ID("{667F29D5-A324-4231-8061-128F36FF361D}");
            public static readonly ID NewPostByContact = new ID("{0DA2503C-89A0-4346-B9A6-ABA1FA1919A7}");
            public static readonly ID ContactFollowedaUser = new ID("{F78933BC-D3B2-4D73-BA99-3E4688221EE2}");
        }
        public struct Other
        {
            public static readonly ID Label = new ID("{3939D743-B2E3-410A-B38F-2FBB39F5BFCA}");
            public static readonly ID SaveButtonLabel = new ID("{B305C46B-9CD8-4F71-9B54-1082CEC778EE}");
            public static readonly ID Loading = new ID("{CACA3514-1BCE-413D-835D-F57B6C0C4FA5}");
            public static readonly ID Never = new ID("{BD21E1B5-0B40-40EB-85B7-CC247F65655E}");
            public static readonly ID Immediately = new ID("{6A4696AE-A5D7-4F9D-A3AD-982900E59989}");
            public static readonly ID Daily = new ID("{E6CD3682-4077-428F-B063-BBF4E56505B2}");
            public static readonly ID Weekly = new ID("{9173ED7B-2A9E-460B-8AB3-130739591EC5}");
            public static readonly ID TodayText = new ID("{7EF22F98-55E8-485C-B468-90F0FFB09264}");
            public static readonly ID YesterdayText = new ID("{5689141B-6A55-4B87-89AF-060FD8AA0B35}");
        }
    }
}