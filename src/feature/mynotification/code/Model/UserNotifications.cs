﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Loreal.Feature.MyNotification.Model
{
    [Table("UserNotifications")]
    public class UserNotifications
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Userid { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public DateTime DueDate { get; set; }
        public bool Done { get; set; }
        public NotificationType Type { get; set; }
        public string NotifyUserid { get; set; }
    }
}