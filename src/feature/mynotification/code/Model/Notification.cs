﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Loreal.Feature.MyNotification.Model
{
    [Table("Notification")]
    public class Notification
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string UserId { get; set; }
        public string Email { get; set; }
        public NotificationSendType NotificationOne { get; set; }
        public NotificationSendType NotificationTwo { get; set; }
        public NotificationSendType NotificationThree { get; set; }
        public NotificationSendType NotificationFour { get; set; }
        public NotificationSendType NotificationFive { get; set; }
        public bool Live { get; set; }
    }
}