﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.MyNotification.Model
{
    public class Timeline
    {
        public string ImageUrl { get; set; }
        public string Message { get; set; }
        public NotificationType Type { get; set; }
        public string Date { get; set; }
    }
}