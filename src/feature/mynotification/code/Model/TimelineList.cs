﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.MyNotification.Model
{
    public class TimelineList
    {
        public List<Timeline> List { get; set; }
        public int Count { get; set; }
    }
}