﻿using Sitecore.Data.Items;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.XA.Foundation.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.MyNotification.Model
{
    public class NotificationViewModel : RenderingModelBase
    {
        protected Item Setting
        {
            get
            {
                Item item = ServiceLocator.Current.Resolve<IMultisiteContext>().SettingsItem.Children.FirstOrDefault(x => x.TemplateID == MyNotificationTemplate.ID);
                if (item != null)
                {
                    return item;
                }
                Sitecore.Diagnostics.Log.Error("Sitecore MyNotification Item missing under Settings!", this);
                return null;
            }
        }
        public Notification Notification { get; set; }
        public string Message
        {
            get
            {
                return Setting?.Fields[MyNotificationTemplate.Other.Label].Value;
            }
        }
        public string LoadingText
        {
            get
            {
                return Setting?.Fields[MyNotificationTemplate.Other.Loading].Value;
            }
        }
        public string SaveButtonText
        {
            get
            {
                return Setting?.Fields[MyNotificationTemplate.Other.SaveButtonLabel].Value;
            }
        }
        public string NotificationOneLabel
        {
            get
            {
                return Setting?.Fields[MyNotificationTemplate.NotificationOne.Title].Value;
            }
        }
        public string NotificationTwoLabel
        {
            get
            {
                return Setting?.Fields[MyNotificationTemplate.NotificationTwo.Title].Value;
            }
        }
        public string NotificationThreeLabel
        {
            get
            {
                return Setting?.Fields[MyNotificationTemplate.NotificationThree.Title].Value;
            }
        }
        public string NotificationFourLabel
        {
            get
            {
                return Setting?.Fields[MyNotificationTemplate.NotificationFour.Title].Value;
            }
        }
        public string NotificationFiveLabel
        {
            get
            {
                return Setting?.Fields[MyNotificationTemplate.NotificationFive.Title].Value;
            }
        }
        public Dictionary<int, string> Types
        {
            get
            {
                Dictionary<int, string> _types = new Dictionary<int, string>();
                _types.Add(0, Setting?.Fields[MyNotificationTemplate.Other.Never].Value);
                _types.Add(1, Setting?.Fields[MyNotificationTemplate.Other.Immediately].Value);
                _types.Add(2, Setting?.Fields[MyNotificationTemplate.Other.Daily].Value);
                _types.Add(3, Setting?.Fields[MyNotificationTemplate.Other.Weekly].Value);
                return _types;
            }
        }
        public string FilterLabel
        {
            get
            {
                return Setting?.Fields[MyNotificationTemplate.Filter.Label].Value;
            }
        }
        public Dictionary<int, string> Filters
        {
            get
            {
                Dictionary<int, string> _filters = new Dictionary<int, string>();
                _filters.Add(0, Setting?.Fields[MyNotificationTemplate.Filter.Like].Value);
                _filters.Add(1, Setting?.Fields[MyNotificationTemplate.Filter.Comment].Value);
                _filters.Add(2, Setting?.Fields[MyNotificationTemplate.Filter.PostInFollowedCategory].Value);
                _filters.Add(3, Setting?.Fields[MyNotificationTemplate.Filter.NewPostByContact].Value);
                _filters.Add(4, Setting?.Fields[MyNotificationTemplate.Filter.ContactFollowedaUser].Value);
                return _filters;
            }
        }
        public List<Timeline> Timeline { get; set; }
        public int TimelineCount { get; set; }
    }
}