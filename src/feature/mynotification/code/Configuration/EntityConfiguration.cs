﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
namespace Loreal.Feature.MyNotification.Configuration
{
    internal class EntityConfiguration
    {
        public class NotificationConfiguration : EntityTypeConfiguration<Model.Notification>
        {
            public NotificationConfiguration()
            {
                Property(col => col.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                Property(col => col.UserId).HasColumnType("nvarchar").HasMaxLength(10000).IsRequired();
                Property(col => col.Email).HasColumnType("nvarchar").HasMaxLength(1000).IsRequired();
                Property(col => col.NotificationOne).HasColumnType("int").IsRequired();
                Property(col => col.NotificationTwo).HasColumnType("int").IsRequired();
                Property(col => col.NotificationThree).HasColumnType("int").IsRequired();
                Property(col => col.NotificationFour).HasColumnType("int").IsRequired();
                Property(col => col.NotificationFive).HasColumnType("int").IsRequired();
            }
            public class UserNotificationConfiguration : EntityTypeConfiguration<Model.UserNotifications>
            {
                public UserNotificationConfiguration()
                {
                    Property(col => col.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    Property(col => col.Image).HasColumnType("nvarchar").HasMaxLength(10000).IsRequired();
                    Property(col => col.Message).HasColumnType("nvarchar").HasMaxLength(100000).IsRequired();
                    Property(col => col.Name).HasColumnType("nvarchar").HasMaxLength(10000).IsRequired();
                    Property(col => col.DueDate).HasColumnType("datetime").IsRequired();
                    Property(col => col.Done).HasColumnType("datetime").IsRequired();
                    Property(col => col.Type).HasColumnType("int").IsRequired();
                    Property(col => col.Userid).HasColumnType("nvarchar").HasMaxLength(10000).IsRequired();
                    Property(col => col.NotifyUserid).HasColumnType("nvarchar").HasMaxLength(10000).IsRequired();
                    Property(col => col.DueDate).HasColumnType("Date").IsRequired();
                }
            }
        }
    }
}