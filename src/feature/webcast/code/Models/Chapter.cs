﻿using System.Web;

namespace Loreal.Feature.Webcast.Models
{
  public class Chapter
  {
    public string Title { get; set; }
    public string Description { get; set; }
    public string FullScreenImage { get; set; }
    public string Thumbnail { get; set; }
    public int Start { get; set; }
  }
}
