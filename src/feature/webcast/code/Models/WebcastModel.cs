﻿using Sitecore.XA.Foundation.Mvc.Models;
using System.Collections.Generic;

namespace Loreal.Feature.Webcast.Models
{
    public class WebcastModel : RenderingModelBase
    {
        public string Title { get; set; }   
        public string Video { get; set; }
        public string Thumbnail { get; set; }        
        public IList<Chapter> Chapters { get; set; }       
    }
}
