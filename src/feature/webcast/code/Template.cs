﻿using Sitecore.Data;

namespace Loreal.Feature.Webcast
{
    public struct WebcastTemplates
    {
        public struct WebCast
        {
            public static readonly ID TemplateID = new ID("{B2F7E5AC-9B1A-497D-BAC3-FEF6318123C2}");
            public struct Fields
            {
                public static readonly ID Title = new ID("{A659CDFB-9DDD-4326-863B-B7E0BFC92217}");
                public static readonly ID Video = new ID("{C5B3119E-E825-4D84-AD8B-FB887950F6F3}");
                public static readonly ID Thumbnail = new ID("{EDBCA40B-34D7-4560-AE7C-8031885ECD89}");
                public static readonly ID Webcastmapping = new ID("{1B3D9128-CDBA-4381-952B-28FD51822130}");
            }
        }

        public struct Chapter
        {
            public static readonly ID TemplateID = new ID("{200FDB7A-50DA-46DF-8940-A4F5071C43B1}");
            public struct Fields
            {
                public static readonly ID Title = new ID("{90651779-A02F-4B8B-8688-EAB5DF795656}");
                public static readonly ID Description = new ID("{EDC9EEA1-B460-4B71-9B0F-4B85E8C7E624}");
                public static readonly ID FullScreen = new ID("{8A0CCBB0-1212-4A23-AF77-C91FA3D3C5D7}");
                public static readonly ID Thumbnail = new ID("{8D006C05-7C7B-4506-9B4F-D3EB8B858717}");
                public static readonly ID StartPosition = new ID("{85ABF919-985D-4713-BF4A-FFB10E0E5944}");
            }
        }
    }
}