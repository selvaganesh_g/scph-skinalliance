﻿using Loreal.Feature.Webcast.Extensions;
using Loreal.Feature.Webcast.Models;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Webcast
{
    public static class Utils
    {
        public static List<Chapter> ProcessWebcastChapter(Item webCastItem)
        {
            List<Chapter> chapters = new List<Chapter>();
            foreach (var chapter in webCastItem.Children.Where(x => x.TemplateID == WebcastTemplates.Chapter.TemplateID))
            {
                var _chapter = new Chapter();

                _chapter.Title = chapter.Fields[WebcastTemplates.Chapter.Fields.Title]?.Value;
                _chapter.Description = chapter.Fields[WebcastTemplates.Chapter.Fields.Description]?.Value;
                string startposition = chapter.Fields[WebcastTemplates.Chapter.Fields.StartPosition]?.Value;
                _chapter.Start = startposition.Equals(string.Empty) ? 0 : System.Convert.ToInt32(startposition);
                _chapter.FullScreenImage = chapter.ImageUrl(WebcastTemplates.Chapter.Fields.FullScreen);
                _chapter.Thumbnail = chapter.ImageUrl(WebcastTemplates.Chapter.Fields.Thumbnail);
                chapters.Add(_chapter);
            }
            return chapters;
        }
    }
}