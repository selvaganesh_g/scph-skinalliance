﻿using Sitecore.XA.Foundation.IOC.Pipelines.IOC;
using Loreal.Feature.Webcast.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Loreal.Feature.Webcast.Pipelines.IoC
{
    public class RegisterWebcastServices : IocProcessor
    {
        public override void Process(IocArgs args)
        {
            args.ServiceCollection.AddTransient<IWebcastDetailRepository, WebcastDetailRepository>();
        }
    }
}