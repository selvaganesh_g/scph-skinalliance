﻿using System;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;

namespace Loreal.Feature.Webcast.Extensions
{
  public static class MediaExtensions
  {
    public static string ImageUrl(this ImageField imageField, MediaUrlOptions options)
    {
      if (imageField?.MediaItem == null)
        throw new ArgumentNullException(nameof(imageField));

      return options == null
        ? imageField.ImageUrl()
        : HashingUtils.ProtectAssetUrl(MediaManager.GetMediaUrl(imageField.MediaItem, options));
    }

    public static string ImageUrl(this ImageField imageField)
    {
      if (imageField?.MediaItem == null)
        throw new ArgumentNullException(nameof(imageField));

      var options = MediaUrlOptions.Empty;
      int width, height;

      if (int.TryParse(imageField.Width, out width))
        options.Width = width;

      if (int.TryParse(imageField.Height, out height))
        options.Height = height;
      return imageField.ImageUrl(options);
    }

    public static string ImageUrl(this Item item, ID imageFieldId, MediaUrlOptions options = null)
    {
      if (item == null)
        throw new ArgumentNullException(nameof(item));

      var imageField = (ImageField)item.Fields[imageFieldId];
      return imageField?.MediaItem == null ? string.Empty : imageField.ImageUrl(options);
    }

    public static string ImageUrl(this MediaItem mediaItem, int width, int height)
    {
      if (mediaItem == null)
        throw new ArgumentNullException(nameof(mediaItem));

      var options = new MediaUrlOptions { Height = height, Width = width };
      var url = MediaManager.GetMediaUrl(mediaItem, options);
      var cleanUrl = StringUtil.EnsurePrefix('/', url);
      var hashedUrl = HashingUtils.ProtectAssetUrl(cleanUrl);

      return hashedUrl;
    }

    public static string LinkFieldUrl(this Item item, ID fieldId)
    {
      LinkField linkField = item.Fields[fieldId];
      return linkField?.GetFriendlyUrl();
    }
  }
}
