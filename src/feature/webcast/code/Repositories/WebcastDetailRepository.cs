﻿using Loreal.Feature.Webcast.Models;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;
using System.Linq;
using Loreal.Feature.Webcast.Extensions;
using Sitecore.Globalization;
using Sitecore.Data.Items;
using System.Web.Security;
using Sitecore.Web.UI.WebControls;

namespace Loreal.Feature.Webcast.Repositories
{
    public class WebcastDetailRepository : ModelRepository, IWebcastDetailRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {

            return this.GetWebcastModel(new WebcastModel(), Context.Item);
        }

        private WebcastModel GetWebcastModel(WebcastModel webcastModel, Item currentItem)
        {
            base.FillBaseProperties(webcastModel);

            LookupField droplinkfield = currentItem.Fields[WebcastTemplates.WebCast.Fields.Webcastmapping];
            var categoryItem = droplinkfield?.TargetItem;

            webcastModel.Title = categoryItem.Fields[WebcastTemplates.WebCast.Fields.Title]?.Value;
            webcastModel.Video = categoryItem.LinkFieldUrl(WebcastTemplates.WebCast.Fields.Video);
            webcastModel.Thumbnail = categoryItem.ImageUrl(WebcastTemplates.WebCast.Fields.Thumbnail);
            webcastModel.Chapters = new List<Chapter>();
            if (!string.IsNullOrEmpty(webcastModel.Video))
            {
                webcastModel.Video = webcastModel.Video.ToLower().Replace("http:", "").Replace("https:", "");
            }
            foreach (var chapter in categoryItem.Children.Where(x => x.TemplateID == WebcastTemplates.Chapter.TemplateID))
            {
                var _chapter = new Chapter();
                string startposition = chapter.Fields[WebcastTemplates.Chapter.Fields.StartPosition]?.Value;
                _chapter.Start = startposition.Equals(string.Empty) ? 0 : System.Convert.ToInt32(startposition);
                _chapter.FullScreenImage = chapter.ImageUrl(WebcastTemplates.Chapter.Fields.FullScreen);
                _chapter.Thumbnail = chapter.ImageUrl(WebcastTemplates.Chapter.Fields.Thumbnail);

                webcastModel.Chapters.Add(_chapter);
            }
            return webcastModel;
        }
    }
}