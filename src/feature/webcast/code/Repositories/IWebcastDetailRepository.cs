﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Webcast.Repositories
{
    public interface IWebcastDetailRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
    }
}
