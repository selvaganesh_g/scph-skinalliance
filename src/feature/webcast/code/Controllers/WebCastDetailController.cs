﻿using Loreal.Feature.Webcast.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Webcast.Controllers
{
    public class WebCastDetailController : StandardController
    {
        // GET: WebCast

        public IWebcastDetailRepository _iwebcastDetailRepository;
        public WebCastDetailController(IWebcastDetailRepository iwebcastDetailRepository)
        {
            _iwebcastDetailRepository = iwebcastDetailRepository;
        }
        protected override object GetModel()
        {
            return _iwebcastDetailRepository.GetModel();
        }
        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Webcast/WebcastDetail.cshtml";
        }
    }
}
