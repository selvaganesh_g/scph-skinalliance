﻿using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Article.Models
{
    public class MediaViewModel
    {
        public string ArticleMediaId { get; set; }
        public List<MediaModel> MediaList { get; set; }
    }
}