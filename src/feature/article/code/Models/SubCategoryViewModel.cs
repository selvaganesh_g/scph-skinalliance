﻿using Sitecore.XA.Foundation.Mvc.Models;
using System.Collections.Generic;

namespace Loreal.Feature.Article.Models
{
    public class SubCategoryViewModel : RenderingModelBase
    {
        public List<SubCategoryModel> SubCategories { get; set; }
    }
}