﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Article.Models
{
    public class FollowModel
    {
        public string UserFollowText { get; set; }
        public string CategoryFollowText { get; set; }
        public string CategoryFollowUrl { get; set; }
        public string UserFollowUrl { get; set; }
    }
}