﻿using Sitecore.XA.Foundation.Mvc.Models;
using System.Collections.Generic;

namespace Loreal.Feature.Article.Models
{
    public class ArticleDetailViewModel : RenderingModelBase
    {
        public ArticleModel Article { get; set; }
        public string PostDetails { get; set; }
        public string Posted { get; set; }
        public string PostedBy { get; set; }
        public string Category { get; set; }
        public string Like { get; set; }
        public string ReportToMedrator { get; set; }
        public string MedratorMessage { get; set; }
        public string MedratorButtonText { get; set; }
        public string DeletePost { get; set; }
        public string EditPost { get; set; }
        public bool TweetPost { get; set; } = false;
        public bool IsWebcast { get; set; }
        public bool IsDownload { get; set; }
        public SocialMediaShareModel SocialMediaShare
        {
            get
            {
                return new SocialMediaShareModel();
            }
        }
    }
}
