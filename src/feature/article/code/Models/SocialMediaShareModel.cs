﻿using Sitecore;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.XA.Foundation.Mvc.Models;
using System.Linq;

namespace Loreal.Feature.Article.Models
{
    public class SocialMediaShareModel : RenderingModelBase
    {
        public static Item SettingsItem
        {
            get
            {
                using (new SecurityDisabler())
                {
                    if (ServiceLocator.Current.Resolve<IMultisiteContext>().SettingsItem == null)
                    {
                        return ServiceLocator.Current.Resolve<IMultisiteContext>()?
                            .GetSettingsItem(Context.Database.GetItem(Context.Site.StartPath));
                    }
                    else
                    {
                        return ServiceLocator.Current.Resolve<IMultisiteContext>().SettingsItem;
                    }
                }
            }
        }

        protected Item SocialConnectorItem
        {
            get
            {
                using (new SecurityDisabler())
                {
                    Item item = SettingsItem.Children.
                    FirstOrDefault(x => x.TemplateID == SocialShare.ID);
                    if (item != null)
                    {
                        return item;
                    }
                }
                return null;
            }
        }

        public string FBShareAPI
        {
            get
            {
                return Utils.GetFriendlyUrl(this.SocialConnectorItem, SocialShare.Fields.FBShareAPI);
            }
        }

        public string FBShareLinkText
        {
            get
            {
                return this.SocialConnectorItem?.GetFieldValue(SocialShare.Fields.FBShareLinkText);
            }
        }

        public string FBShareIcon
        {
            get
            {
                return Utils.GetImageUrl(this.SocialConnectorItem, SocialShare.Fields.FBShareIcon);
            }
        }

        public string TwitterShareAPI
        {
            get
            {
                return Utils.GetFriendlyUrl(this.SocialConnectorItem, SocialShare.Fields.TwitterShareAPI);
            }
        }

        public string TwitterShareLinkText
        {
            get
            {
                return this.SocialConnectorItem?.GetFieldValue(SocialShare.Fields.TwitterShareLinkText);
            }
        }

        public string TwitterShareIcon
        {
            get
            {
                return Utils.GetImageUrl(this.SocialConnectorItem, SocialShare.Fields.TwitterShareIcon);
            }
        }

        public string LinkedInShareAPI
        {
            get
            {
                return Utils.GetFriendlyUrl(this.SocialConnectorItem, SocialShare.Fields.LinkedInShareAPI);
            }
        }

        public string LinkedInShareLinkText
        {
            get
            {
                return this.SocialConnectorItem?.GetFieldValue(SocialShare.Fields.LinkedInShareLinkeText);
            }
        }

        public string LinkedInShareIcon
        {
            get
            {
                return Utils.GetImageUrl(this.SocialConnectorItem, SocialShare.Fields.LinkedInShareIcon);
            }
        }
    }
}