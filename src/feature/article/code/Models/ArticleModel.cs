﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Article.Models
{
    public class ArticleModel
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Body { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Category { get; set; }
        public string CategoryID { get; set; }
        public string ExternalUrl { get; set; }
        public string ExternalUrlText { get; set; }
        public int Like { get; set; }
        public int Comments { get; set; }
        public string Link { get; set; }
        public FollowModel Follow { get; set; }
        
    }
}