﻿using Sitecore.XA.Foundation.Mvc.Models;
using System.Collections.Generic;
using System.Web;

namespace Loreal.Feature.Article.Models
{
    public class CreatePostViewModel : RenderingModelBase
    {
        public ArticleModel Article { get; set; }
        public HtmlString AnswerCaption { get; set; }
        public HttpPostedFileBase ThumbnailImage { get; set; }
        public List<ArticleQuestionModel> Questionaire { get; set; }
        public List<CategoryModel> Categories { get; set; }
        public List<HttpPostedFileBase> MediaFiles { get; set; }
        public string AddQuestionCaption { get; set; }
        public string AnswerACaption { get; set; }
        public string AnswerBCaption { get; set; }
        public string AnswerCCaption { get; set; }
        public string AnswerDCaption { get; set; }
        public string CategoryCaption { get; set; }
        public string CorrectCaption { get; set; }
        public string ImageCaption { get; set; }
        public string ImageFileFormat { get; set; }
        public Dictionary<string, List<CategoryModel>> CategoryList { get; set; }
        public string MediaApiUrl { get; set; }
        public string MediaCaption { get; set; }
        public string PreviewCaption { get; set; }
        public string PublishApiUrl { get; set; }
        public string PublishCaption { get; set; }
        public string PublishInstanceUrl { get; set; }
        public string QuestionnaireCaption { get; set; }
        public string ResponseCaption { get; set; }
        public string ResponseInPercentageCaption { get; set; }
        public string SaveCaption { get; set; }
        public string TitleCaption { get; set; }
        public string TitleValidationMessage { get; set; }

        public string AddMediaValidationMessage { get; set; }

        public string UploadCapion { get; set; }

        public string RTEWatermark { get; set; } 

        public string WebsiteUrlCaption { get; set; }

        public string PreviewUrl { get; set; }

        public string ArticleMediaId { get; set; }

        public HtmlString SuccessMessage { get; set; }

        public HtmlString FailureMessage { get; set; }

        public string IsQuestionRequired { get; set; }

        public string IsAnswerRequired { get; set; }

        public string IsCategoryRequired { get; set; }

        public string IsCheckBoxRequired { get; set; }
        public string QuestionPlacehoderText { get; set; }
        public List<string> CategoryIds { get; set; }
        public string QuestionnaireJson { get; set; }

        public bool IsUpdate { get; set; }

    }
}