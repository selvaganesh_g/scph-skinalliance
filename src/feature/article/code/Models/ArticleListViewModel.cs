﻿using Sitecore.XA.Foundation.Mvc.Models;
using System.Collections.Generic;

namespace Loreal.Feature.Article.Models
{
    public class ArticleListViewModel : RenderingModelBase
    {
        public string Title { get; set; }
        public string ViewAll { get; set; }
        public string ViewUrl { get; set; }
        public string SortByLabel { get; set; }
        public string NotFoundLabel { get; set; }
        public List<SortbyModel> Sortby { get; set; }
        public List<ArticleModel> Articles { get; set; }
        public int Count { get; set; }
    }
}