﻿namespace Loreal.Feature.Article.Models
{
    public class MediaModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Extension { get; set; }
    }
}