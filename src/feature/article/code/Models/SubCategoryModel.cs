﻿using System.Collections.Generic;

namespace Loreal.Feature.Article.Models
{
    public class SubCategoryModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string ViewAll { get; set; }
        public List<ArticleModel> Articles { get; set; }

        public int Count { get; set; }
    }
}
