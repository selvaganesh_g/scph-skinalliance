﻿using System.Collections.Generic;

namespace Loreal.Feature.Article.Models
{
    public class CategoryViewModel : CategoryModel
    {
        public List<CategoryModel> Categories { get; set; }
    }
}