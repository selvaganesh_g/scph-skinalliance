﻿using System.Collections.Generic;

namespace Loreal.Feature.Article.Models
{
    public class ArticleQuestionModel
    {
        public List<Answer> Answers { get; set; }
        public string ID { get; set; }
        public string Question { get; set; }
        public string Correct { get; set; }
        public string Response { get; set; }
        public bool ResponseInPercentage { get; set; }
    }
}