﻿using Sitecore.XA.Foundation.Mvc.Models;
using System.Collections.Generic;

namespace Loreal.Feature.Article.Models
{
    public class CategoryModel : RenderingModelBase
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
        public List<SubCategoryModel> SubCategories { get; set; }
    }
}