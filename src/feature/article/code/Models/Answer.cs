﻿namespace Loreal.Feature.Article.Models
{
    public class Answer
    {
        public string Opt { get; set; }
        public string Ans { get; set; }
        public bool isChecked { get; set; }
    }
}