﻿using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Article
{
    public struct SocialShare
    {
        public static readonly ID ID = new ID("{1E3B4807-9FFA-467D-B781-B3BB76B6C9E2}");
        public struct Fields
        {
            public static readonly ID FBShareAPI = new ID("{E37A82EA-75AB-470E-BFE4-50D6BC8CFCA5}");
            public static readonly ID FBShareLinkText = new ID("{1707BB31-6596-4310-A304-6DC4A40E0CAF}");
            public static readonly ID FBShareIcon = new ID("{39D1BA8B-C4A1-471A-A2DD-47B0847630A6}");
            public static readonly ID TwitterShareAPI = new ID("{B96EEB61-77E7-46D2-BF2F-3CC47B76CE10}");
            public static readonly ID TwitterShareLinkText = new ID("{40CD0DEA-76C9-424A-8A48-03C1B9DB34F6}");
            public static readonly ID TwitterShareIcon = new ID("{D31CE06C-1EDB-4ADD-8267-D26DF4D768C6}");
            public static readonly ID LinkedInShareAPI = new ID("{271245D7-3BD5-4927-B112-741E6DFE83FD}");
            public static readonly ID LinkedInShareLinkeText = new ID("{DB4A5D81-0E30-4B21-A793-BF0E4823B7E2}");
            public static readonly ID LinkedInShareIcon = new ID("{78B062BA-9848-41B0-8208-33FBCA896EAA}");
        }
    }

    public class ArticleTemplates
    {
        public static readonly ID ContentDatabase = new ID("{DE4D8A4D-63D0-4B4D-A246-44622CE59B0C}");
        public struct Category
        {
            public static readonly ID ID = new ID("{9E50F55E-7062-4E14-88B9-A27A31944AD0}");
            public static readonly ID GroupID = new ID("{BA1A2CD6-4FB0-4A7C-987A-D46A32C4ACA4}");
            public static readonly ID PageID = new ID("{3F543531-DAD5-4728-B182-62BDBBF885F3}");
            public static readonly ID PageSource = new ID("{F826868E-AE42-4448-9FBC-B84F60AAC99B}");
            public struct Fields
            {
                public static readonly ID Name = new ID("{6948080E-4246-4BF7-B0C1-6F4E807EE8BB}");
                public static readonly ID Description = new ID("{9B8F61CD-FC5F-4760-B0DD-C156EBB632F1}");
                public static readonly ID Image = new ID("{CA7F34F2-EC87-488A-9509-57E34BF7F639}");

            }
        }

        public struct SubCategory
        {
            public static readonly ID ID = new ID("{B50E94BE-B89E-43E1-8A5E-0348E1F41A5B}");
            public static readonly ID PageID = new ID("{34D931AB-2BB2-45FF-A2D2-02AABBE8290C}");
            public static readonly ID PageSource = new ID("{B765A279-F7D9-44ED-A42E-B86D4607A909}");

        }

        public struct CongressCategory
        {
            public static readonly ID ID = new ID("{AAA951C7-5207-4C9C-B4BA-C1DD2B57FDBD}");
            public static readonly ID PageID = new ID("{8D832090-41A5-4877-B55A-674E9589F5A6}");
            public static readonly ID PageSource = new ID("{F40C0298-F925-419A-A166-705DA6A8A76E}");
        }
        public struct WebcastCategory
        {
            public static readonly ID ID = new ID("{17B8C5E5-E649-4A66-B1AA-5378BFD11560}");
            public static readonly ID PageID = new ID("{4F5472B3-B4C3-40C6-A9D7-6CC3A0E90815}");
            public static readonly ID PageSource = new ID("{7409339B-8311-4FAB-BA89-EED29956D560}");
        }
        public struct CategorySlider
        {
            public static readonly ID ID = new ID("{C94E018E-8125-4958-B2AE-D09E89A364E1}");
            public struct RenderingParameters
            {
                public static readonly ID Category = new ID("{53A1E67E-73C0-42C1-9AA4-3EAA892070E1}");
                public static readonly String CategoryName = "Category";
            }
        }
        public struct Article
        {
            public static readonly ID Continer = new ID("{28B34313-6290-4172-936F-85D4B827055E}");
            public static readonly ID Group = new ID("{D8B9B968-BC60-4356-B9C7-CA95A6D37651}");
            public struct Details
            {
                public static readonly ID ID = new ID("{B86FE4A5-903B-4DFB-A4BA-7E84FDDF3B4C}");
                public static readonly ID RenderingID = new ID("{CB4AEA75-33F1-423E-B634-072577B6676F}");
                public static readonly ID BannerRenderingID = new ID("{A8DDCEE8-C1E6-4865-8B2C-8CC2A5573289}");

                public struct Fields
                {
                    public static readonly ID Name = new ID("{71FCA010-73D7-498A-A5B1-926F5A6954F9}");
                    public static readonly ID Image = new ID("{7C3759C6-BC46-4E47-9FF5-ECB8995F6CAF}");
                    public static readonly ID Body = new ID("{AD12EBF3-09AF-4321-9DD9-F63B0E1FFE6A}");
                    public static readonly ID Categories = new ID("{C584C839-B789-4491-9ADA-A0B903F4E07A}");
                    public static readonly ID ExternalUrl = new ID("{F7B49A93-9D55-429E-BF69-3F72C5F079CB}");
                    public static readonly ID LikeCount = new ID("{460DFC42-054E-4718-A4EE-9EED323F3A22}");
                    public static readonly ID CommentCount = new ID("{0FFA82D9-E576-4442-8626-BDBBB2E89CA2}");
                    public static readonly ID WebCastMapping = new ID("{1B3D9128-CDBA-4381-952B-28FD51822130}");

                }

            }
            public struct Question
            {
                public static readonly ID ID = new ID("{6467B8D7-8D47-40C6-A954-64ED3E300826}");
                public struct Fields
                {
                    public static readonly ID Question = new ID("{E5BA867A-ABC2-4CC1-BB16-1C06704AC732}");
                    public static readonly ID A = new ID("{EF6951B3-D86F-4D15-9875-A14E4A312277}");
                    public static readonly ID B = new ID("{817CABE2-5A51-45E9-84F6-896B11584994}");
                    public static readonly ID C = new ID("{184FCE8D-9B10-44E1-8D34-C6216F376E88}");
                    public static readonly ID D = new ID("{5E141DD6-7621-4AEF-B340-FC717845CA7A}");
                    public static readonly ID Correct = new ID("{6E2923CF-A636-4231-8E43-C51E89848CC2}");
                    public static readonly ID Response = new ID("{48C8D538-C415-40BE-95CA-9E0DCECBBFD3}");
                    public static readonly ID ShowPercentage = new ID("{3CE475D3-1608-42EF-BE8E-D818DA7DEFF1}");

                }
            }

        }
        public static readonly ID NavigationTitle = new ID("{71FCA010-73D7-498A-A5B1-926F5A6954F9}");
        public static readonly ID NavigationFilter = new ID("{1FBC6A49-6281-48EE-87E9-A3970B145ADB}");
        public struct HomeArticleWidget
        {
            public static readonly ID RenderingParameterID = new ID("{AB85916F-81FE-49DC-A3DD-33F80EF6A175}");
        }
    }
}