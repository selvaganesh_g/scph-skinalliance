﻿using Loreal.Feature.Article.Models;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.SecurityModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Linq;
using Sitecore.Web.UI.WebControls;
using Sitecore.Diagnostics;
using System.Net;

namespace Loreal.Feature.Article.Extensions
{
    public static class SCItemExtensions
    {
        /// <summary>
        /// Gets the field value.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="item">The item.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static string GetFieldValue(string fieldName, Item item, string parameters) => FieldRenderer.Render(item, fieldName, parameters);

        /// <summary>
        /// Gets the field value.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static string GetFieldValue(string fieldName, Item item) => FieldRenderer.Render(item, fieldName);

        /// <summary>
        /// Fields the exists.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static bool FieldExists(string fieldName, Item item) => (item[fieldName] != null) ? true : false;

        /// <summary>
        /// Gets the field value.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="field">The field.</param>
        /// <returns></returns>
        public static string GetFieldValue(this Item item, ID field) => item.Fields[field]?.Value;

        /// <summary>
        /// Creates the item.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="parentID">The parent identifier.</param>
        /// <param name="templateID">The template identifier.</param>
        /// <param name="itemName">Name of the item.</param>
        /// <returns></returns>
        public static Item CreateItem(Database database, ID parentID, ID templateID, string itemName)
        {
            using (new SecurityDisabler())
            {
                Item parentItem = database.Items[parentID];
                TemplateItem template = database.GetTemplate(templateID);
                return parentItem.Add(itemName, template);
            }
        }
        /// <summary>
        /// Gets the string field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldId">The field identifier.</param>
        /// <returns></returns>
        public static string GetStringField(this Item item, ID fieldId)
        {
            string fieldValue = string.Empty;
            if (item != null)
            {
                Field field = item.Fields[fieldId];
                if ((field != null) && (!string.IsNullOrEmpty(field.Value)))
                    fieldValue = field.Value;
            }
            return fieldValue;
        }
        /// <summary>
        /// Gets the media item field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public static MediaItem GetMediaItemField(this Item item, string fieldName)
        {
            Field fieldValue = item.Fields[fieldName];
            MediaItem mediaItem = null;

            if ((fieldValue != null) && (fieldValue.HasValue))
                mediaItem = ((ImageField)fieldValue).MediaItem;

            return mediaItem;
        }
        /// <summary>
        /// Gets the boolean field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public static bool GetBooleanField(this Item item, string fieldName)
        {
            Field fieldValue = item.Fields[fieldName];

            if (fieldValue != null)
                return (fieldValue.Value == "1" ? true : false);
            else
                return false;
        }
        public static bool GetBooleanField(this Item item, ID fieldID)
        {
            Field fieldValue = item.Fields[fieldID];

            if (fieldValue != null)
                return (fieldValue.Value == "1" ? true : false);
            else
                return false;
        }
        public static HtmlField GetHtmlField(this Item item, String fieldName)
        {
            HtmlField htmlField = item.Fields[fieldName];
            return htmlField;
        }
        /// <summary>
        /// Gets the HTML field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public static HtmlField GetHtmlField(this Item item, ID fieldName)
        {
            HtmlField htmlField = item.Fields[fieldName];
            return htmlField;
        }
        /// <summary>
        /// Gets the link field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public static LinkField GetLinkField(this Item item, ID fieldId)
        {
            LinkField linkField = item.Fields[fieldId];
            return linkField;
        }
        /// <summary>
        /// URLs the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static string Url(this Item item)
        {
            return LinkManager.GetItemUrl(item);
        }

        /// <summary>
        /// Adds the file to media library.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="target">The target.</param>
        /// <param name="articleName">Name of the article.</param>
        /// <returns></returns>
        public static MediaItem AddFileToMediaLibrary(HttpPostedFileBase file)
        {

            using (new SecurityDisabler())
            {
                var database = Sitecore.Context.ContentDatabase == null ? Sitecore.Context.Database : Sitecore.Context.ContentDatabase;


                //TODO : Need to check folder exisits or not if create new one and proceed.
                var target = database.GetItem(CreatePostTemplates.Article.ArticleMediaFolder);

                var articleGroup = database.GetTemplate(CreatePostTemplates.Article.ArticleGroup);

                var articleMediaItem = Utils.GetMonthItem(target, articleGroup);

                string fileNameWithoutExt = ItemUtil.ProposeValidItemName(Path.GetFileNameWithoutExtension(file.FileName));

                var options = new MediaCreatorOptions()
                {
                    AlternateText = Path.GetFileNameWithoutExtension(file.FileName),
                    FileBased = false,
                    IncludeExtensionInItemName = false,
                    Versioned = false,
                    Destination = string.Concat(articleMediaItem.Paths.FullPath, "/" + fileNameWithoutExt),
                    Database = database
                };
                string fileName = string.Concat(fileNameWithoutExt, Path.GetExtension(file.FileName));
                var mediaItem = MediaManager.Creator.CreateFromStream(file.InputStream, fileName, options);
                mediaItem.Reload();
                return mediaItem;
            }
        }

        public static MediaItem AddFileToMediaLibrary(string imageUrl, string mediaName)
        {

            using (new SecurityDisabler())
            {
                var database = Sitecore.Context.ContentDatabase == null ? Sitecore.Context.Database : Sitecore.Context.ContentDatabase;
                //TODO : Need to check folder exisits or not if create new one and proceed.
                var target = database.GetItem(CreatePostTemplates.Article.ArticleMediaFolder);
                var articleGroup = database.GetTemplate(CreatePostTemplates.Article.ArticleGroup);

                var articleMediaItem = Utils.GetMonthItem(target, articleGroup);

                WebRequest webRequest = WebRequest.Create(imageUrl);
                using (var webResponse = webRequest.GetResponse())
                {
                    using (var stream = webResponse.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var memoryStream = new MemoryStream())
                            {
                                var filename = imageUrl.Substring(imageUrl.LastIndexOf("/") + 1);
                                stream.CopyTo(memoryStream);

                                Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
                                options.Database = database;
                                options.AlternateText = mediaName;
                                options.Destination = string.Format("{0}/{1}", articleMediaItem.Paths.FullPath.TrimEnd('/'), ItemUtil.ProposeValidItemName(filename));
                                options.OverwriteExisting = true;
                                var item = Sitecore.Resources.Media.MediaManager.Creator.CreateFromStream(memoryStream, string.Format("{0}.png", mediaName), options);
                                item.Reload();
                                return item;

                            }
                        }
                    }
                }
                return null;
            }
        }
        /// <summary>
        /// Adds the files to media library.
        /// </summary>
        /// <param name="files">The files.</param>
        /// <param name="target">The target.</param>
        /// <param name="articleName">Name of the article.</param>
        /// <returns></returns>
        public static MediaViewModel AddFilesToMediaLibrary(List<HttpPostedFileBase> fileCollection)
        {


            MediaViewModel mediaViewModel = new MediaViewModel();
            mediaViewModel.MediaList = new List<MediaModel>();
            using (new SecurityDisabler())
            {
                var database = Sitecore.Context.ContentDatabase == null ? Sitecore.Context.Database : Sitecore.Context.ContentDatabase;
                //TODO : Need to check folder exisits or not if create new one and proceed.
                var target = database.GetItem(CreatePostTemplates.Article.ArticleMediaFolder);
                var articleGroup = database.GetTemplate(CreatePostTemplates.Article.ArticleGroup);

                var articleMediaItem = Utils.GetMonthItem(target, articleGroup);

                foreach (var filebase in fileCollection)
                {
                    string fileNameWithoutExt = ItemUtil.ProposeValidItemName(Path.GetFileNameWithoutExtension(filebase.FileName));

                    var options = new MediaCreatorOptions()
                    {
                        AlternateText = Path.GetFileNameWithoutExtension(filebase.FileName),
                        FileBased = false,
                        IncludeExtensionInItemName = false,
                        Versioned = false,
                        Destination = string.Concat(articleMediaItem.Paths.FullPath, "/" + fileNameWithoutExt),
                        Database = database
                    };
                    string fileName = string.Concat(fileNameWithoutExt, Path.GetExtension(filebase.FileName));
                    var mediaItem = MediaManager.Creator.CreateFromStream(filebase.InputStream, fileName, options);
                    mediaItem.Reload();
                    string mediaPath = string.Format("{0}{1}?ts={1}", HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority), Sitecore.StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(mediaItem)), System.DateTime.Now.ToString("yyMMddhhmmss"));
                    mediaViewModel.MediaList.Add(new MediaModel() { Name = fileName, Path = mediaPath, Extension = Path.GetExtension(filebase.FileName) });
                }
            }
            return mediaViewModel;
        }

        /// <summary>
        /// Assigns the media item.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="mediaItem">The media item.</param>
        public static void AssignMediaItem(ImageField field, Item mediaItem)
        {
            field.MediaID = mediaItem.ID;
            field.SetAttribute("mediapath", mediaItem.Paths.MediaPath);
            field.SetAttribute("showineditor", "1");
        }
        /// <summary>
        /// Updates the field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        public static void UpdateField(this Item item, ID field, string value)
        {
            if (item.Fields[field] != null)
            {
                item.Fields[field].Value = value;
            }
        }
    }
}