﻿using Loreal.Feature.Article.Models;
using Loreal.Foundation.Search.Models;
using Loreal.Foundation.Search.Services;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Loreal.Feature.Article
{
    public static class Utils
    {
        public static List<ArticleModel> GetArticlesListByCategory(string categoryId, int offset, int limit, Foundation.Search.SortType type, Item subCategoryPageItem, out int count, string excludeId = "")
        {
            count = 0;
            List<ArticleModel> articles = new List<ArticleModel>();
            var result = SearchArticlesCategory(categoryId, offset, limit, type);
            if (result != null)
            {                
                articles = Utils.ProcessArticles(result.Items, subCategoryPageItem, excludeId);
                count = result.Count;
            }
            return articles;
        }
        public static List<ArticleModel> GetArticlesByCategory(string categoryId, int offset, int limit, Item subCategoryPageItem, out int count, string excludeId = "")
        {
            count = 0;
            List<ArticleModel> articles = new List<ArticleModel>();
            var result = SearchArticlesCategory(categoryId, offset, limit, Foundation.Search.SortType.Date);
            if (result != null && result.Items != null)
            {
                articles = Utils.ProcessArticles(result.Items, subCategoryPageItem, excludeId);
                count = result.Count;
            }
            return articles;
        }

        public static List<ArticleModel> GetLatestArticles(int limit, Item pageItem, out int count)
        {
            List<ArticleModel> articles = new List<ArticleModel>();
            List<Tuple<string, string, string>> filters = new List<Tuple<string, string, string>>();
            filters.Add(new Tuple<string, string, string>("location", Utils.ArticleContiner()?.ID.ToString(), "must"));
            filters.Add(new Tuple<string, string, string>("template", ArticleTemplates.Article.Details.ID.ToString(), "must"));
            var result = AzureSearch.Search(filters, Foundation.Search.SortType.Date, Context.Database.GetItem(Utils.ArticleContiner()?.ID), 0, limit);
            if (result != null && result.Items != null)
            {
                articles = Utils.ProcessArticles(result.Items, pageItem, "");
            }
            count = result.Count;
            return articles;
        }
        public static List<ArticleModel> SearchArticles(string query, Foundation.Search.SortType type, int offset, int limit, Item pageItem, out int count)
        {
            List<ArticleModel> articles = new List<ArticleModel>();
            List<Tuple<string, string, string>> filters = new List<Tuple<string, string, string>>();
            filters.Add(new Tuple<string, string, string>("location", Utils.ArticleContiner()?.ID.ToString(), "must"));
            filters.Add(new Tuple<string, string, string>("template", ArticleTemplates.Article.Details.ID.ToString(), "must"));
            filters.Add(new Tuple<string, string, string>("custom", string.Format("title|*{0}*", query), "should"));
            filters.Add(new Tuple<string, string, string>("text", string.Format("*{0}*", query), "should"));
            filters.Add(new Tuple<string, string, string>("custom", string.Format("content|*{0}*", query), "should"));
            filters.Add(new Tuple<string, string, string>("custom", string.Format("article_search_categories|*{0}*", query), "should"));
            var result = AzureSearch.Search(filters, type, Context.Database.GetItem(Utils.ArticleContiner()?.ID), offset, limit);
            if (result != null && result.Items != null)
            {
                articles = Utils.ProcessArticles(result.Items, pageItem, "");
            }
            count = result.Count;
            return articles;
        }
        public static SearchResponse SearchArticlesCategory(string categoryId, int offset, int limit, Foundation.Search.SortType type)
        {

            List<Tuple<string, string, string>> filters = new List<Tuple<string, string, string>>();
            filters.Add(new Tuple<string, string, string>("location", Utils.ArticleContiner()?.ID.ToString(), "must"));
            filters.Add(new Tuple<string, string, string>("template", ArticleTemplates.Article.Details.ID.ToString(), "must"));
            filters.Add(new Tuple<string, string, string>("custom", string.Format("articlecategories|{0}", categoryId), "must"));
            return AzureSearch.Search(filters, type, Context.Database.GetItem(Utils.ArticleContiner()?.ID), offset, limit);

        }
        public static Item GetSubCategoryPageById(string id)
        {
            List<Tuple<string, string, string>> filters = new List<Tuple<string, string, string>>();
            var homeItemid = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath);
            filters.Add(new Tuple<string, string, string>("template", ArticleTemplates.SubCategory.PageID.ToString(), "must"));
            filters.Add(new Tuple<string, string, string>("custom", string.Format("subcategory|{0}", id), "must"));
            var result = AzureSearch.Search(filters, Foundation.Search.SortType.Date, homeItemid, 0, 1);
            return result.Items.FirstOrDefault();
        }
        public static List<ArticleModel> ProcessArticles(List<Item> items, Item subCategoryPageItem, string excludeId = "")
        {
            List<ArticleModel> articles = new List<ArticleModel>();
            if (subCategoryPageItem == null)
            {
                return articles;
            }

            foreach (var item in items)
            {
                if (item == null) continue;

                if (!string.IsNullOrEmpty(excludeId) && item.ID.ToString() == excludeId) { continue; }
                ArticleModel article = new ArticleModel();
                article.Name = item.Fields[ArticleTemplates.Article.Details.Fields.Name]?.Value;
                ImageField image = item.Fields[ArticleTemplates.Article.Details.Fields.Image];
                if (image != null && image.MediaItem != null)
                {

                    article.Image = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(image.MediaItem));


                }
                article.Link = Utils.GetArticleUrl(item, subCategoryPageItem);
                article.Like = FollowandComments.Managers.LikeManager.Get(item.ID.ToString());
                article.Comments = FollowandComments.Managers.CommentManager.List(item.ID.ToString()).Count;
                article.CreatedDate = Sitecore.DateUtil.ToServerTime(item.Statistics.Created).ToString("dd/MMM/yyyy");
                if (!string.IsNullOrEmpty(item.Statistics.CreatedBy))
                {
                    article.CreatedBy = Sitecore.Security.Accounts.User.FromName(item.Statistics.CreatedBy, false)?.Profile.FullName;
                }
                articles.Add(article);
            }
            return articles;
        }
        public static Item ArticleContiner()
        {
            try
            {
                return ServiceLocator.Current.Resolve<IMultisiteContext>().DataItem.Children.FirstOrDefault(x => x.TemplateID == ArticleTemplates.Article.Continer);

            }
            catch
            {
                Sitecore.Diagnostics.Log.Error("Article container not found!", "Article Container");
                return null;
            }
        }
        public static string GetArticleUrl(Item item, Item parnet)
        {
            var url = string.Format("{0}/{1}-{2}", LinkManager.GetItemUrl(parnet), item.Name, item.ID.ToShortID().ToString().TrimStart('{').TrimEnd('}'));
            if (Sitecore.Data.IDTables.IDTable.GetID("article-url", url) == null)
            {
                Sitecore.Data.IDTables.IDTable.Add("article-url", url, new Sitecore.Data.ID(), parnet.ID, item.ID.ToString());
            }
            return url;
        }
        public static Item GetCategoryByCurrentArticle(Item subCategoryItem)
        {
            if (subCategoryItem != null && subCategoryItem.TemplateID == ArticleTemplates.SubCategory.PageID)
            {
                return subCategoryItem;
            }
            MultilistField category = Context.Item.Fields[ArticleTemplates.Article.Details.Fields.Categories];
            var subcategory = category?.GetItems().FirstOrDefault();
            if (subcategory != null)
            {
                return GetSubCategoryPageById(subcategory.ID.ToString());
            }
            return null;
        }
        public static List<SortbyModel> GetSortByModel()
        {
            List<SortbyModel> sortByModel = new List<SortbyModel>();
            sortByModel.Add(new SortbyModel()
            {
                Id = Dictionary.Sortby.Date,
                Title = Sitecore.Globalization.Translate.Text(Dictionary.Sortby.Date)
            });
            sortByModel.Add(new SortbyModel()
            {
                Id = Dictionary.Sortby.Popularity,
                Title = Sitecore.Globalization.Translate.Text(Dictionary.Sortby.Popularity)
            });
            return sortByModel;
        }

        public static Item GetMonthItem(Item root, TemplateItem group)
        {
            var year = DateTime.Now.Date.Year.ToString();
            var month = DateTime.Now.Date.Month.ToString("00");
            Item yrItem = null;
            Item moItem = null;
            if (root.Children[year] == null)
            {
                yrItem = root.Add(year, group);
            }
            else
            {
                yrItem = root.Children[year];
            }
            if (yrItem.Children[month] == null)
            {
                moItem = yrItem.Add(month, group);
            }
            else
            {
                moItem = yrItem.Children[month];
            }
            return moItem;
        }

        public static string DateFormat(DateTime date)
        {
            if (date == null) { return string.Empty; }

            string daystr = Sitecore.Globalization.Translate.Text(Dictionary.DateFormat.th);
            string day = date.Day.ToString();

            if (int.Parse(day) < 11 || int.Parse(day) > 20)
            {
                day = day.ToCharArray()[day.ToCharArray().Length - 1].ToString();
                switch (day)
                {
                    case "1":
                        daystr = Sitecore.Globalization.Translate.Text(Dictionary.DateFormat.st);
                        break;
                    case "2":
                        daystr = Sitecore.Globalization.Translate.Text(Dictionary.DateFormat.nd);
                        break;
                    case "3":
                        daystr = Sitecore.Globalization.Translate.Text(Dictionary.DateFormat.rd);
                        break;
                }
            }
            return string.Format("{0}{1} {2} {3}", date.ToString("dd"), daystr, date.ToString("MMMM"), date.ToString("yyyy"));
        }

        public static bool HideCategory(Item category)
        {
            //
            MultilistField listfield = category.Fields[ArticleTemplates.NavigationFilter];
            if (listfield != null && listfield.Count != 0)
            {
                foreach(var item in listfield.GetItems())
                {
                    if (item.Name.ToLower() == "main navigation")
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public static string GetFriendlyUrl(this Item item, ID fieldId)
        {
            LinkField linkField = item.Fields[fieldId];
            return linkField.GetFriendlyUrl();
        }
        public static string GetFieldValue(this Item item, ID field)
        {
            return item.Fields[field]?.Value;
        }
        public static string GetImageUrl(Item currentItem, ID fieldId)
        {
            var imageUrl = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = currentItem.Fields[fieldId];
            if (imageField?.MediaItem != null)
            {
                var image = new MediaItem(imageField.MediaItem);
                imageUrl = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(image));
            }
            return imageUrl;
        }
    }
}

