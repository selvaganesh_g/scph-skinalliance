﻿using Loreal.Feature.Article.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.XA.Foundation.IOC.Pipelines.IOC;

namespace Loreal.Feature.Article.Pipelines.IoC
{
    public class RegisterArticleServices : IocProcessor
    {
        public override void Process(IocArgs args)
        {
            args.ServiceCollection.AddTransient<ICategoryRepository, CategoryRepository>();
            args.ServiceCollection.AddTransient<ICategorySliderRepository, CategorySliderRepository>();
            args.ServiceCollection.AddTransient<ISubCategoryRepository, SubCategoryRepository>();
            args.ServiceCollection.AddTransient<IArticleListRespository, ArticleListRepository>();
            args.ServiceCollection.AddTransient<IArticleDetailRespository, ArticleDetailRepository>();
            args.ServiceCollection.AddTransient<ICreatePostRepository, CreatePostRepository>();
            args.ServiceCollection.AddTransient<IRelatedArticleRepository, RelatedArticleRepository>();
            args.ServiceCollection.AddTransient<ICategoryWidgetRepository, CategoryWidgetRepository>();
            args.ServiceCollection.AddTransient<ILatestArticleRepository, LatestArticleRepository>();
            args.ServiceCollection.AddTransient<IFavoriteArticleRepository, FavoriteArticleRepository>();
            args.ServiceCollection.AddTransient<IArticleSearchRespository, ArticleSearchRepository>();

        }
    }
}