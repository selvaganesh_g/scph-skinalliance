﻿using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.HttpRequest;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Article.Pipelines.Processor
{
    public class ArticleDetailResolver : HttpRequestProcessor
    {
        public override void Process(HttpRequestArgs args)
        {

            Assert.ArgumentNotNull(args, "args");
            /*In Case Sitecore has mapped the item, do not do anything and simply return*/
            if ((Context.Item != null || Context.Database == null || args.Url.ItemPath.Length == 0))

            {
                if (Context.Item != null && Context.Item.Name != "404")
                {
                    return;
                }
            }
            string id = string.Empty;
            //My notification url resolve
            if (args.Context.Request.Url.PathAndQuery.Contains("notificationarticle"))
            {

                var queryValues = HttpUtility.ParseQueryString(args.Context.Request.Url.Query);
                if (!string.IsNullOrEmpty(queryValues["id"]))
                {
                    id = queryValues["id"];
                }
            }
            else
            {
                var requesturl = Sitecore.Data.IDTables.IDTable.GetKeys("article-url").Where(x => x.Key == args.Context.Request.Url.PathAndQuery).FirstOrDefault();
                if (requesturl != null)
                {
                    id = requesturl.CustomData;
                }
                /*If not, Check for item based on the FilePath*/
            }
            if (!string.IsNullOrEmpty(id))
            {
                Item contextItem = Sitecore.Context.Database.GetItem(id);
                if (contextItem != null)
                {
                    Context.Item = contextItem;
                }
            }

        }
    }
}