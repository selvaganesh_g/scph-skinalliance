﻿using Sitecore.Pipelines;
using System.Web.Mvc;
using System.Web.Routing;

namespace Loreal.Feature.Article.Pipelines
{
    public class RegisterArticleRoute
    {
        public virtual void Process(PipelineArgs args)
        {
            RouteTable.Routes.MapRoute("articlelist", "articlelist", new { controller = "ArticleList", action = "List" });
            RouteTable.Routes.MapRoute("searcharticle", "searcharticle", new { controller = "SearchArticle", action = "List" });
            RouteTable.Routes.MapRoute("articledetail", "saapi/{controller}/{action}", new { controller = "aticledetail", action = "reporttomodarte" });
            RouteTable.Routes.MapRoute("servey", "saapi/{controller}/{action}", new { controller = "servey", action = "update" });
        }
    }
}