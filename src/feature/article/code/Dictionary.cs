﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Article
{
    public class Dictionary
    {
        public struct PostDeatils
        {
            public static readonly string Title = "postdetailtitle";
            public static readonly string Posted = "posted";
            public static readonly string PostedBy = "postedby";
            public static readonly string Category = "postcategory";
            public static readonly string Like = "postliketext";
        }
        public struct RelatedPost
        {
            public static readonly string Title = "relatedposttitle";
        }
        public struct Sortby
        {
            public static readonly string Title = "sortybytitle";
            public static readonly string Date = "sortbymostrecent";
            public static readonly string Popularity = "sortbypopularity";
        }
        public struct DateFormat
        {
            public static readonly string st = "st";
            public static readonly string nd = "nd";
            public static readonly string rd = "rd";
            public static readonly string th = "th";
        }
        public struct Actions
        {
            public static readonly string EditPost = "editpost";
            public static readonly string DeletePost = "deletepost";
            public static readonly string ReportToModerator = "reporttomoderator";
            public static readonly string ModeratorMessage = "moderationconfirmation";
            public static readonly string ModeratorButtonText = "confirmationclose";
        }

        public static readonly string DownloadButtonLabel = "downloadbtntxt";
        public static readonly string AccessSiteButtonLabel = "accesssitebtntxt";
        public static readonly string ArticlesNotFoundLabel = "articlesnotfoundlabel";
        public static readonly string ViewAllText = "articleviewall";
    }
}