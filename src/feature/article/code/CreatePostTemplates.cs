﻿using Sitecore.Data;

namespace Loreal.Feature.Article
{
    public class CreatePostTemplates
    {
        public struct CreatePost
        {
            public static readonly ID ID = new ID("{760A8DEA-0D55-4382-97C1-CDC2DF2732BB}");
            public static readonly ID MediaFolderTemplate = new ID("{FE5DD826-48C6-436D-B87A-7C4210C7413B}");
            public struct Fields
            {
                public static readonly ID TitleCaption = new ID("{8C6D3B5C-0F99-4691-B884-D2692DFA90B0}");
                public static readonly ID RTEWatermark = new ID("{8611E957-2349-4847-AF9B-80151F93CB71}");
                public static readonly ID ImageCaption = new ID("{F123D11F-4080-412F-8AE6-5B1EB01D4A1C}");
                public static readonly ID MediaCaption = new ID("{29D2DB7D-DF40-4BD8-8AE5-653CB9E3A6E2}");
                public static readonly ID WebsiteUrlCaption = new ID("{9A22C2AA-5899-4A6A-B44D-42E1A7AA70F4}");
                public static readonly ID QuestionnaireCaption = new ID("{2DD20F87-F442-491D-A3AC-CB0C639B6266}");
                public static readonly ID CategoryCaption = new ID("{5EBDB496-6E10-462B-8874-18B882D7EA0E}");
                public static readonly ID AnswerCaption = new ID("{51B186E3-E9E3-4FD5-AE02-C90D7A42902F}");
                public static readonly ID AddQuestionCaption = new ID("{CA779D1A-2D93-47E5-87D2-293678890402}");
                public static readonly ID AnswerACaption = new ID("{F79F5894-03FA-44B8-8DB9-853E04600FAA}");
                public static readonly ID AnswerBCaption = new ID("{6B53727A-1DF1-43F5-A72F-2AE1DDE0E9C8}");
                public static readonly ID AnswerCCaption = new ID("{30F311F5-9D0D-4A56-B4F2-CACA9F4E2319}");
                public static readonly ID AnswerDCaption = new ID("{B7769405-C0A5-4CBC-8554-BC2B3346F707}");
                public static readonly ID ResponseCaption = new ID("{CC8C1096-C20C-41DD-8679-9BAE241EAF70}");
                public static readonly ID ResponseInPercentageCaption = new ID("{127E0EBC-F969-4AF6-B0F4-1F7A7DCD3F1E}");
                public static readonly ID SaveCaption = new ID("{80152C82-13FF-4BA9-BE26-D763DA198B24}");
                public static readonly ID PublishCaption = new ID("{819F85FD-76C6-4B78-BCAC-F80B5B87E789}");
                public static readonly ID PreviewCaption = new ID("{1F64379F-F8DC-4270-9339-7951A1097F30}");
                public static readonly ID CorrectCaption = new ID("{79876EED-4058-409F-A67C-7F254EA5EF9F}");
                public static readonly ID ImageFileFormat = new ID("{FB900834-4373-470C-8BE1-CCD1D2AF7564}");
                public static readonly ID TitleValidation = new ID("{AD568E32-5401-468F-A036-F6960E1251EA}");
                public static readonly ID ImageValidation = new ID("{EBD2CE96-2354-49CA-9557-8F907304BD8E}");
                public static readonly ID CategoryTemplateID = new ID("{6E0CC227-141B-41AC-9BB7-6D3B5D97626D}");
                public static readonly ID MainCategoryID = new ID("{D51C19D2-31F2-49B1-A849-D542DBA3FFE5}");
                public static readonly ID MainCategoryTitle = new ID("{71FCA010-73D7-498A-A5B1-926F5A6954F9}");
                public static readonly ID PublishInstanceUrl = new ID("{256922AE-EE5D-40E5-A94E-5E8721DF08C7}");
                public static readonly ID MediaApiUrl = new ID("{B5E9257C-43C7-4015-83E2-969EA95F8FCE}");
                public static readonly ID PublishApiUrl = new ID("{3088133E-BF99-4A01-9C3A-7596FDA12E84}");
                public static readonly ID UploadCaption = new ID("{B77A1AC0-C487-489D-8DC6-BDEC080CBDEE}");
                public static readonly ID TitleValidationMessage = new ID("{AD568E32-5401-468F-A036-F6960E1251EA}");
                public static readonly ID AddMediaValidationMessage = new ID("{52E4182C-408A-434C-9095-6F9B5AC098F3}");
                public static readonly ID PreviewUrl = new ID("{8E5560CE-0AB7-462C-B4CE-C00648EA7BE7}");
                public static readonly ID SuccessMessage = new ID("{AA01A95E-F9F8-48BE-96BB-69B33A56BAD3}");
                public static readonly ID FailureMessage = new ID("{B51B6720-F2F2-495F-ACC7-99958DBBC495}");
                public static readonly ID IsQuestionRequired = new ID("{747908C9-2A8F-4E0E-9209-B46A5B30D3DF}");
                public static readonly ID IsAnswerRequired = new ID("{4FE195E2-C542-4C1E-ACDF-F51DB56D4C60}");
                public static readonly ID IsCategoryRequired = new ID("{351A7950-234F-40ED-972F-22DCE1B7E940}");
                public static readonly ID IsCheckBoxRequired = new ID("{37928F0E-EDC7-41B6-AF5D-C9D5DB8D3AAF}");
                public static readonly ID QuestionPlacehoderText = new ID("{10AE2C9E-1CE5-40B3-9871-8F26A52B35A8}");

            }
        }

        public struct CategoryGroup
        {
            public struct Fields
            {
                public static readonly ID Name = new ID("{131E644A-05C9-4467-BD9F-4A3F68B06ECF}");
            }
        }
        public struct SubCategory
        {
            public struct Fields
            {
                public static readonly ID Name = new ID("{A3A8DDD0-1420-482C-97BA-E79D1D9D83E9}");
            }
        }

        public struct Article
        {
            public static readonly ID ArticleTemplate = new ID("{28B34313-6290-4172-936F-85D4B827055E}");
            public static readonly ID ArticleMediaFolder = new ID("{BB213521-6F1D-454A-9095-6D18BC9B6364}");
            public static readonly ID ID = new ID("{B86FE4A5-903B-4DFB-A4BA-7E84FDDF3B4C}");
            public static readonly ID ArticleGroup = new ID("{D8B9B968-BC60-4356-B9C7-CA95A6D37651}");
            public struct Fields
            {
                public static readonly ID Name = new ID("{71FCA010-73D7-498A-A5B1-926F5A6954F9}");
                public static readonly ID Image = new ID("{7C3759C6-BC46-4E47-9FF5-ECB8995F6CAF}");
                public static readonly ID Body = new ID("{AD12EBF3-09AF-4321-9DD9-F63B0E1FFE6A}");
                public static readonly ID Categories = new ID("{C584C839-B789-4491-9ADA-A0B903F4E07A}");
                public static readonly ID ExternalUrl = new ID("{F7B49A93-9D55-429E-BF69-3F72C5F079CB}");
            }
        }
        public struct ArticleQuestion
        {
            public static readonly ID ID = new ID("{6467B8D7-8D47-40C6-A954-64ED3E300826}");
            public struct Fields
            {
                public static readonly ID Question = new ID("{E5BA867A-ABC2-4CC1-BB16-1C06704AC732}");
                public static readonly ID A = new ID("{EF6951B3-D86F-4D15-9875-A14E4A312277}");
                public static readonly ID B = new ID("{817CABE2-5A51-45E9-84F6-896B11584994}");
                public static readonly ID C = new ID("{184FCE8D-9B10-44E1-8D34-C6216F376E88}");
                public static readonly ID D = new ID("{5E141DD6-7621-4AEF-B340-FC717845CA7A}");
                public static readonly ID Correct = new ID("{6E2923CF-A636-4231-8E43-C51E89848CC2}");
                public static readonly ID Response = new ID("{48C8D538-C415-40BE-95CA-9E0DCECBBFD3}");
                public static readonly ID DisplayResponseinPercentage = new ID("{3CE475D3-1608-42EF-BE8E-D818DA7DEFF1}");

            }
        }
    }
}