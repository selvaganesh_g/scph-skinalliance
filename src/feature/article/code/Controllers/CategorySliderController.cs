﻿using Loreal.Feature.Article.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Article.Controllers
{
    public class CategorySliderController : StandardController
    {
        public ICategorySliderRepository _iCategorySliderRepositrory;
        public CategorySliderController(ICategorySliderRepository iCategorySliderRepository)
        {
            _iCategorySliderRepositrory = iCategorySliderRepository;
        }

        protected override object GetModel()
        {
            return _iCategorySliderRepositrory.GetModel();
        }
        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Article/CategorySlider.cshtml";
        }
    }
}