﻿using Loreal.Feature.Article.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System.Web.Mvc;

namespace Loreal.Feature.Article.Controllers
{
    public class ArticleListController : StandardController
    {
        public IArticleListRespository _articleRepository;
        public ArticleListController(IArticleListRespository articleRepository)
        {
            _articleRepository = articleRepository;
        }
        protected override string GetIndexViewName()
        {
            return "~/views/skinalliance/feature/article/ArticleList.cshtml";
        }
        public ActionResult List(string id, string sortBy, int offset = 0)
        {
            Sitecore.Context.Item = Sitecore.Context.Database.GetItem(id);
            return Json(_articleRepository.GetModel(sortBy == Dictionary.Sortby.Date.ToString() ? Foundation.Search.SortType.Date : Foundation.Search.SortType.Like, offset), JsonRequestBehavior.AllowGet);
        }
        protected override object GetModel()
        {
            return _articleRepository.GetModel(Foundation.Search.SortType.Date, 0);
        }
    }
}