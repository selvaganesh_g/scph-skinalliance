﻿using Loreal.Feature.Article.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Article.Controllers
{
    public class CategoryWidgetController : StandardController
    {
        public ICategoryWidgetRepository _ihomeArticleWidgetRepository;
        public CategoryWidgetController(ICategoryWidgetRepository ihomeArticleWidgetRepository)
        {
            _ihomeArticleWidgetRepository = ihomeArticleWidgetRepository;
        }
        protected override object GetModel()
        {
            return _ihomeArticleWidgetRepository.GetModel();
        }
        protected override string GetIndexViewName()
        {
            return "~/views/SkinAlliance/Feature/Article/HomeWidgetArticle.cshtml";
        }
    }
}