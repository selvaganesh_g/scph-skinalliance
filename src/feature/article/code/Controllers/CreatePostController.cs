﻿using Loreal.Feature.Article.API.CustomAttribute;
using Loreal.Feature.Article.Extensions;
using Loreal.Feature.Article.Models;
using Loreal.Feature.Article.Repositories;
using Newtonsoft.Json;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Loreal.Feature.Article.Controllers
{
    [AllowCORS]
    public class CreatePostController : Controller
    {
        public ICreatePostRepository _createPostRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreatePostController"/> class.
        /// </summary>
        /// <param name="createPostRepository">The create post repository.</param>
        public CreatePostController(ICreatePostRepository createPostRepository)
        {
            _createPostRepository = createPostRepository;
        }

        public ActionResult Index()
        {
            return View(GetIndexViewName(), GetModel());
        }

        /// <summary>
        /// Gets the model.
        /// </summary>
        /// <returns></returns>
        protected object GetModel()
        {
            return _createPostRepository.GetModel();
        }

        /// <summary>
        /// Gets the name of the index view.
        /// </summary>
        /// <returns></returns>
        protected string GetIndexViewName()
        {
            return "~/views/skinalliance/feature/article/createpost.cshtml";
        }

        /// <summary>
        /// Adds the media files.
        /// </summary>
        /// <param name="mediaFiles">The media files.</param>
        /// <param name="articleName">Name of the article.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddMediaFiles(List<HttpPostedFileBase> mediaFiles)
        {
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                return Json(SCItemExtensions.AddFilesToMediaLibrary(mediaFiles),
                    JsonRequestBehavior.AllowGet);
            }
            return Json(new MediaViewModel(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the post.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddPost(string model, string userid)
        {
            var viewModel = JsonConvert.DeserializeObject<CreatePostViewModel>(model);

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                viewModel.ThumbnailImage = new HttpPostedFileWrapper(System.Web.HttpContext.Current.Request.Files.Get(0));
            }
            string name;
            string id;
            var returnVal = _createPostRepository.AddPost(viewModel, userid, out name, out id);
            //Send Notification;
            if (returnVal)
            {
                var url = string.Format("{0}/notificationarticle?id={1}", Request.Url.GetLeftPart(UriPartial.Path), id);
                var categoryids = viewModel.Categories.Where(m => m.SubCategories != null).SelectMany(x => x.SubCategories.Select(y => y.ID));
                FollowandComments.Managers.NotificationManager.NewPostByFollowedUser(Sitecore.Security.Accounts.User.Current?.Profile?.StartUrl, url, name);
                FollowandComments.Managers.NotificationManager.NewPostFollowCategoryNotify(categoryids?.ToList(), url, name);
                EmailService.Services.EmailService.Newpost(url, name);
            }
            return Json(returnVal, JsonRequestBehavior.AllowGet);
        }
    }
}