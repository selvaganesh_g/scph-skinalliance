﻿using Loreal.Feature.Article.Repositories;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Loreal.Feature.Article.Controllers
{
    public class ArticleDetailController : StandardController
    {
        public IArticleDetailRespository _articleRepository;
        public ArticleDetailController(IArticleDetailRespository articleRepository)
        {
            _articleRepository = articleRepository;
        }
        public override ActionResult Index()
        {
            Item subCategoryItem = null;
            int endIndex = Request.Url.LocalPath.LastIndexOf("/");
            if (endIndex != -1)
            {
                subCategoryItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath + Request.Url.LocalPath.Substring(0, endIndex));
            }
            return View(GetIndexViewName(), _articleRepository.GetModel(subCategoryItem));
        }
        public ActionResult Banner()
        {
            Item subCategoryItem = null;
            int endIndex = Request.Url.LocalPath.LastIndexOf("/");
            if (endIndex != -1)
            {
                subCategoryItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath + Request.Url.LocalPath.Substring(0, endIndex));
            }
            if (User == null) return Redirect("~/");
            return PartialView("~/views/skinalliance/feature/article/ArticleDetailBanner.cshtml", _articleRepository.GetModel(subCategoryItem));
        }

        [HttpPost]
        public ActionResult ReportToModarte(bool delete)
        {
            EmailService.Services.EmailService.ReportModerate(Request.UrlReferrer.Equals(Request.Url) ? Request.Url.AbsoluteUri : Request.UrlReferrer.AbsoluteUri, delete);
            return Json("success");
        }

        protected override string GetIndexViewName()
        {
            return "~/views/skinalliance/feature/article/ArticleDetail.cshtml";
        }
    }
}