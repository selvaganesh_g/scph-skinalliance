﻿using System.Web.Mvc;
using Loreal.Feature.Article.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Article.Controllers
{
    public class LatestArticleController : StandardController
    {
        public ILatestArticleRepository _ilatestArticleRespository;
        public LatestArticleController(ILatestArticleRepository ilatestArticleRespository)
        {
            _ilatestArticleRespository = ilatestArticleRespository;
        }
        public override ActionResult Index()
        {
            return PartialView("~/views/SkinAlliance/Feature/Article/HomeWidgetArticle.cshtml", _ilatestArticleRespository.GetModel(12));
        }
        public ActionResult List()
        {
            return PartialView("~/views/SkinAlliance/Feature/Article/ArticleList.cshtml", _ilatestArticleRespository.GetModel(24, true));
        }
    }
}