﻿using Loreal.Feature.Article.API.CustomAttribute;
using System.IO;
using System.Net;
using System.Web.Mvc;

namespace Loreal.Feature.Article.Controllers
{
  /// <summary>
  /// Class WebClipperController.
  /// </summary>
  /// <seealso cref="System.Web.Mvc.Controller" />
  [AllowCORS]
  public class WebClipperController : Controller
  {
    /// <summary>
    /// Sites the content.
    /// </summary>
    /// <param name="post_url">The post URL.</param>
    /// <returns>System.String.</returns>
    [HttpGet]
    public string SiteContent(string post_url)
    {
      string siteContent = string.Empty;
      WebRequest webRequest = WebRequest.Create(post_url);
      ServicePointManager.Expect100Continue = true;
      ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

      using (var webResponse = webRequest.GetResponse())
      {
        using (var stream = webResponse.GetResponseStream())
        {
          if (stream == null) return siteContent;
          using (var streamReader = new StreamReader(stream))       // Load the stream reader to read the response
          {
            siteContent = streamReader.ReadToEnd(); // Read the entire response and store it in the siteContent variable
          }
        }
      }
      return siteContent;
    }
  }
}