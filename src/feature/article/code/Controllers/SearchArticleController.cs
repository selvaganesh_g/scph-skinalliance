﻿using System.Web.Mvc;
using Loreal.Feature.Article.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System.Web;

namespace Loreal.Feature.Article.Controllers
{
    public class SearchArticleController : StandardController
    {
        public IArticleSearchRespository _iarticleSearchRespository;
        public SearchArticleController(IArticleSearchRespository iarticleSearchRespository)
        {
            _iarticleSearchRespository = iarticleSearchRespository;
        }
        protected override string GetIndexViewName()
        {
            return "~/views/skinalliance/feature/article/ArticleList.cshtml";
        }
        public ActionResult List(string id, string sortBy, int offset = 0)
        {
            Sitecore.Context.Item = Sitecore.Context.Database.GetItem(id);
            var query = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);
            return Json(_iarticleSearchRespository.GetModel(query["q"], sortBy == Dictionary.Sortby.Date.ToString() ? Foundation.Search.SortType.Date : Foundation.Search.SortType.Like, offset), JsonRequestBehavior.AllowGet);
        }
        protected override object GetModel()
        {
            return _iarticleSearchRespository.GetModel(Request.QueryString["q"], Foundation.Search.SortType.Date, 0);
        }

    }
}