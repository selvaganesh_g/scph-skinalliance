﻿using Loreal.Feature.Article.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Article.Controllers
{
    public class SubCategoryController : StandardController
    {
        public ISubCategoryRepository _isubCategoryRepository;
        public SubCategoryController(ISubCategoryRepository isubCategoryRepository)
        {
            _isubCategoryRepository = isubCategoryRepository;
        }
        protected override object GetModel()
        {
            return _isubCategoryRepository.GetModel();
        }
        protected override string GetIndexViewName()
        {
            return "~/views/SkinAlliance/Feature/Article/SubCategoryList.cshtml";
        }
    }
}