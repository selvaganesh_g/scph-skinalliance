﻿using Loreal.Feature.Article.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Article.Controllers
{
    public class RelatedArticleController : StandardController
    {
        public IRelatedArticleRepository _irelatedArticleRepository;
        public RelatedArticleController(IRelatedArticleRepository irelatedArticleRepository)
        {
            _irelatedArticleRepository = irelatedArticleRepository;
        }
        protected override object GetModel()
        {
            return _irelatedArticleRepository.GetModel();
        }
        protected override string GetIndexViewName()
        {
            return "~/views/SkinAlliance/Feature/Article/RelatedArticle.cshtml";
        }
    }
}