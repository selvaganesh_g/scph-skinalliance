﻿using System.Web.Mvc;
using Loreal.Feature.Article.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Article.Controllers
{
    public class FavoriteArticlesController : StandardController
    {
        public IFavoriteArticleRepository _ifavoriteArticleRepository;
        public FavoriteArticlesController(IFavoriteArticleRepository ifavoriteArticleRepository)
        {
            _ifavoriteArticleRepository = ifavoriteArticleRepository;
        }
        public override ActionResult Index()
        {
            return PartialView("~/views/SkinAlliance/Feature/Article/HomeWidgetArticle.cshtml", _ifavoriteArticleRepository.GetModel(12));
        }
        public ActionResult List()
        {
            return PartialView("~/views/SkinAlliance/Feature/Article/ArticleList.cshtml", _ifavoriteArticleRepository.GetModel(24, true));
        }
    }
}