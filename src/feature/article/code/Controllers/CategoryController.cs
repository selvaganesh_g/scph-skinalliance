﻿using Loreal.Feature.Article.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Article.Controllers
{
    public class CategoryController : StandardController
    {
        public ICategoryRepository _icategoryRepository;
        public CategoryController(ICategoryRepository categoryRepository)
        {
            _icategoryRepository = categoryRepository;
        }
        protected override object GetModel()
        {
            return _icategoryRepository.GetModel();
        }
        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Article/CategoryList.cshtml";
        }
    }
}