﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;
using System.Linq;
using System;
using Sitecore.Data.Fields;

namespace Loreal.Feature.Article.ComputedFields
{
    public class Categories : AbstractComputedIndexField
    {
        public override object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;
            if (item == null) { return null; }
            string[] categories = null;
            if (item.Fields[ArticleTemplates.Article.Details.Fields.Categories] != null)
            {
                try
                {
                    MultilistField multilist = item.Fields[ArticleTemplates.Article.Details.Fields.Categories];
                    if (multilist != null)
                    {
                        var items = multilist.GetItems();
                        categories = items?.Select(x => x.Name).ToArray();
                    }
                }
                catch (Exception ex)
                {
                    Sitecore.Diagnostics.Log.Error(ex.Message, this);
                }
            }
            return categories;
        }
    }
}