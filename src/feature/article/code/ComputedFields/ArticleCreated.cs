﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;
using System.Linq;
using System;

namespace Loreal.Feature.Article.ComputedFields
{
    public class ArticleCreated : AbstractComputedIndexField
    {
        public override object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;
            if (item == null) { return null; }
            try
            {
                return (DateTime)item.Statistics.Created;
            }
            catch
            {
                Sitecore.Diagnostics.Log.Info(string.Format("{0} -{1}", item.ID.ToString(), item.Name), "INDEX ARTICLE CREATED");
                return null;
            }
        }
    }
}