﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;
using System.Linq;
using System;

namespace Loreal.Feature.Article.ComputedFields
{
    public class LikeCount : AbstractComputedIndexField
    {
        public override object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;
            if (item == null) { return null; }
            int? count;
            if (item.Fields["likecount"] != null)
            {
                try
                {
                    count = FollowandComments.Managers.LikeManager.Get(item.ID.ToString());
                }
                catch
                {
                    count = 0;
                }
            }
            else
            {
                count = null;
            }
            return count;
        }
    }
}