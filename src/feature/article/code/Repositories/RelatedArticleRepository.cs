﻿using Loreal.Feature.Article.Models;
using Sitecore.Data.Fields;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Article.Repositories
{
    public class RelatedArticleRepository : ModelRepository, IRelatedArticleRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            ArticleListViewModel viewModel = new ArticleListViewModel();
            base.FillBaseProperties(viewModel);
            var category = Utils.GetCategoryByCurrentArticle(null);
            if (category != null && category.TemplateID == ArticleTemplates.SubCategory.PageID)
            {
                LookupField droplinkfield = category.Fields[ArticleTemplates.SubCategory.PageSource];
                var subcategoryItem = droplinkfield?.TargetItem;
                if (subcategoryItem == null) return viewModel;
                int count;
                viewModel.Articles = Utils.GetArticlesByCategory(subcategoryItem.ID.ToString(), 0, 3, category, out count, Sitecore.Context.Item.ID.ToString());
                viewModel.Count = count;
            }

            return viewModel;
        }

    }
}