﻿using Loreal.Feature.Article.Models;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Article.Repositories
{
    public class CategoryWidgetRepository : ModelRepository, ICategoryWidgetRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            ArticleListViewModel viewModel = new ArticleListViewModel();
            base.FillBaseProperties(viewModel);
            viewModel.ViewAll = Sitecore.Globalization.Translate.Text(Dictionary.ViewAllText);
            viewModel.NotFoundLabel = Sitecore.Globalization.Translate.Text(Dictionary.ArticlesNotFoundLabel);
            if (Rendering.DataSourceItem != null && Rendering.DataSourceItem.TemplateID == ArticleTemplates.SubCategory.PageID)
            {
                viewModel.Title = Rendering.Parameters["Title"];
                viewModel.ViewUrl = LinkManager.GetItemUrl(Rendering.DataSourceItem);
                LookupField droplinkfield = Rendering.DataSourceItem.Fields[ArticleTemplates.SubCategory.PageSource];
                var subcategoryItem = droplinkfield?.TargetItem;
                if (subcategoryItem == null) return viewModel;
                int count;                
                viewModel.Articles = Utils.GetArticlesByCategory(subcategoryItem.ID.ToString(), 0, 12, Rendering.DataSourceItem, out count,Sitecore.Context.Item.ID.ToString());
                viewModel.Count = count;
            }

            return viewModel;
        }

    }
}