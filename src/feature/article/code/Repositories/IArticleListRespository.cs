﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Article.Repositories
{
    public interface IArticleListRespository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        object GetModel(Foundation.Search.SortType sort, int offset);
    }
}
