﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Article.Repositories
{
    public interface ILatestArticleRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        object GetModel(int count,bool isList=false);
    }
}