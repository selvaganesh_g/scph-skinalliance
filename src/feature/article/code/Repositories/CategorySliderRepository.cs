﻿using Loreal.Feature.Article.Models;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Article.Repositories
{
    public class CategorySliderRepository : ModelRepository, ICategorySliderRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            CategoryViewModel model = new CategoryViewModel();
            base.FillBaseProperties(model);
            List<CategoryModel> categories = new List<CategoryModel>();
            if (this.Rendering.Item.Fields[ArticleTemplates.CategorySlider.RenderingParameters.Category] == null)
            {
                model.Categories = categories;
                Sitecore.Diagnostics.Log.Error("Datasource is nulll or empty", this);
                return model;
            }

            foreach (var itemId in this.Rendering.Parameters[ArticleTemplates.CategorySlider.RenderingParameters.CategoryName].Split('|'))
            {
                var Item = Sitecore.Context.Database.GetItem(itemId);
                if (Item != null && Item.TemplateID == ArticleTemplates.Category.PageID)
                {
                    CategoryModel categorySlider = new CategoryModel();
                    categorySlider.Name = Item.Fields[ArticleTemplates.NavigationTitle].Value;
                    LookupField droplinkfield = Item.Fields[ArticleTemplates.Category.PageSource];
                    var categoryItem = droplinkfield?.TargetItem;
                    if (categoryItem == null) continue;
                    categorySlider.Description = categoryItem.Fields[ArticleTemplates.Category.Fields.Description].Value;
                    ImageField media = categoryItem.Fields[ArticleTemplates.Category.Fields.Image];
                    if (media != null && media.MediaItem != null)
                    {
                        categorySlider.Image = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(media.MediaItem));
                    }
                    categorySlider.Link = LinkManager.GetItemUrl(Item);
                    categories.Add(categorySlider);
                }
            }
            model.Categories = categories;
            return model;
        }
    }
}