﻿using Loreal.Feature.Article.Models;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using Loreal.Foundation.Search.Services;
using Sitecore;
using Sitecore.Resources.Media;

namespace Loreal.Feature.Article.Repositories
{
    public class SubCategoryRepository : ModelRepository, ISubCategoryRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            SubCategoryViewModel model = new SubCategoryViewModel();
            base.FillBaseProperties(model);
            List<SubCategoryModel> subCategories = new List<SubCategoryModel>();
            var rootItem = Sitecore.Context.Item;
            if (Rendering.DataSourceItem != null)
            {
                rootItem = Rendering.DataSourceItem;
            }
            foreach (var subItem in rootItem.Children.Where(x => x.TemplateID == ArticleTemplates.SubCategory.PageID))
            {
                if (Utils.HideCategory(subItem))
                {
                    continue;
                }
                LookupField droplinkfield = subItem.Fields[ArticleTemplates.SubCategory.PageSource];
                var subcategoryItem = droplinkfield?.TargetItem;
                if (subcategoryItem == null) continue;
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.ViewAll = Sitecore.Globalization.Translate.Text(Dictionary.ViewAllText);
                subCategory.Name = subItem.Fields[ArticleTemplates.NavigationTitle].Value;
                subCategory.Link = LinkManager.GetItemUrl(subItem);
                int count;
                subCategory.Articles = Utils.GetArticlesByCategory(subcategoryItem.ID.ToString(), 0, 12, subItem,out count);
                subCategory.Count = count;
                subCategories.Add(subCategory);
            }
            model.SubCategories = subCategories;
            return model;
        }

    }
}