﻿using Loreal.Feature.Article.Extensions;
using Loreal.Feature.Article.Models;
using Newtonsoft.Json;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Workflows;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Article.Repositories
{
    /// <summary>
    /// Repository for CreatePost
    /// </summary>
    /// <seealso cref="ModelRepository" />
    /// <seealso cref="ICreatePostRepository" />
    /// <seealso cref="IModelRepository" />
    /// <seealso cref="Sitecore.XA.Foundation.Mvc.Repositories.Base.IAbstractRepository{IRenderingModelBase}" />
    public class CreatePostRepository : ModelRepository, ICreatePostRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public CreatePostRepository()
        {
            CurrentDb = Sitecore.Context.ContentDatabase == null ? Sitecore.Context.Database : Sitecore.Context.ContentDatabase;
            var sitePath = Sitecore.Context.Site.RootPath;
            if (!string.IsNullOrEmpty(sitePath))
            {
                this.DataItem = ServiceLocator.Current.Resolve<IMultisiteContext>()?.GetDataItem(CurrentDb.GetItem(sitePath));
                this.SettingItem = ServiceLocator.Current.Resolve<IMultisiteContext>()?.GetSettingsItem(CurrentDb.GetItem(sitePath));
            }
        }
        protected Database CurrentDb { get; set; }
        /// <summary>
        /// Gets the multisite context.
        /// </summary>
        /// <value>
        /// The multisite context.
        /// </value>
        protected Item DataItem { get; set; }
        protected Item SettingItem { get; set; }

        /// <summary>
        /// Gets the article folder.
        /// </summary>
        /// <value>
        /// The article folder.
        /// </value>
        protected Item ArticleFolder => this.DataItem.Children.FirstOrDefault(x => x.TemplateID == CreatePostTemplates.Article.ArticleTemplate);

        /// <summary>
        /// Gets the create post item.
        /// </summary>
        /// <value>
        /// The create post item.
        /// </value>
        protected Item CreatePostItem => this.SettingItem.Children.FirstOrDefault(x => x.TemplateID == CreatePostTemplates.CreatePost.ID);

        /// <summary>
        /// Gets the model.
        /// </summary>
        /// <returns></returns>
        public override IRenderingModelBase GetModel()
        {
            var dataSource = CreatePostItem;

            var model = new CreatePostViewModel();
            base.FillBaseProperties(model);

            model.AddQuestionCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.AddQuestionCaption);
            model.AnswerACaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.AnswerACaption);
            model.AnswerBCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.AnswerBCaption);
            model.AnswerCaption = new HtmlString(dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.AnswerCaption));
            model.AnswerCCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.AnswerCCaption);
            model.AnswerDCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.AnswerDCaption);
            model.CategoryCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.CategoryCaption);
            model.CorrectCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.CorrectCaption);
            model.ImageCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.ImageCaption);
            model.ImageFileFormat = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.ImageFileFormat);
            model.MediaCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.MediaCaption);
            model.PreviewCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.PreviewCaption);
            model.PublishCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.PublishCaption);
            model.QuestionnaireCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.QuestionnaireCaption);
            model.ResponseCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.ResponseCaption);
            model.ResponseInPercentageCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.ResponseInPercentageCaption);
            model.SaveCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.SaveCaption);
            model.TitleCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.TitleCaption);
            model.TitleValidationMessage = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.TitleValidationMessage);
            model.AddMediaValidationMessage = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.AddMediaValidationMessage);
            model.WebsiteUrlCaption = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.WebsiteUrlCaption);
            model.PublishInstanceUrl = SettingItem.GetStringField(CreatePostTemplates.CreatePost.Fields.PublishInstanceUrl);
            model.MediaApiUrl = string.Concat(model.PublishInstanceUrl, dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.MediaApiUrl));
            model.PublishApiUrl = string.Concat(model.PublishInstanceUrl, dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.PublishApiUrl));
            model.UploadCapion = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.UploadCaption);
            model.PreviewUrl = dataSource.GetLinkField(CreatePostTemplates.CreatePost.Fields.PreviewUrl).GetFriendlyUrl();
            model.SuccessMessage = new HtmlString(dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.SuccessMessage));
            model.FailureMessage = new HtmlString(dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.FailureMessage));
            model.IsQuestionRequired = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.IsQuestionRequired);
            model.IsAnswerRequired = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.IsAnswerRequired);
            model.IsCategoryRequired = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.IsCategoryRequired);
            model.IsCheckBoxRequired = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.IsCheckBoxRequired);
            model.QuestionPlacehoderText = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.QuestionPlacehoderText);
            model.RTEWatermark = dataSource.GetStringField(CreatePostTemplates.CreatePost.Fields.RTEWatermark);


            var categoryItem = this.DataItem.Children.FirstOrDefault(x => x.TemplateID == CreatePostTemplates.CreatePost.Fields.CategoryTemplateID);

            model.CategoryList = new Dictionary<string, List<CategoryModel>>();

            foreach (var item in categoryItem.Axes.GetDescendants().Where(x => x.TemplateID == ArticleTemplates.Category.GroupID))
            {
                string groupName = item[CreatePostTemplates.CategoryGroup.Fields.Name];
                if (string.IsNullOrEmpty(groupName))
                {
                    if (!string.IsNullOrEmpty(item.DisplayName))
                        groupName = item.DisplayName;
                    else
                        groupName = item.Name;
                }

                model.CategoryList.Add(groupName, GetCategories(item));
            }
            string articleId = string.Empty;

            if (HttpContext.Current.Request.QueryString.AllKeys.Contains("id"))
            {
                model.IsUpdate = true;
                articleId = HttpContext.Current.Request.QueryString["id"];
                model.ArticleMediaId = articleId;
                var articleItem = CurrentDb.GetItem(articleId);
                if (articleItem != null)
                {
                    model.Article = new ArticleModel();
                    model.Article.Name = articleItem.GetStringField(CreatePostTemplates.Article.Fields.Name);
                    model.Article.Body = articleItem.GetStringField(CreatePostTemplates.Article.Fields.Body);

                    ImageField imgField = articleItem.Fields[CreatePostTemplates.Article.Fields.Image];

                    if (imgField != null)
                    {
                        model.Article.Image = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                    }
                    LinkField link = articleItem.Fields[CreatePostTemplates.Article.Fields.ExternalUrl];
                    if (link != null)
                    {
                        model.Article.ExternalUrl = link.GetFriendlyUrl();
                        model.Article.ExternalUrlText = link.IsInternal ? Translate.Text(Dictionary.DownloadButtonLabel) : Translate.Text(Dictionary.AccessSiteButtonLabel);
                    }
                    model.Questionaire = new List<ArticleQuestionModel>();
                    foreach (var quest in articleItem.Children.Where(x => x.TemplateID == CreatePostTemplates.ArticleQuestion.ID))
                    {
                        var question = new ArticleQuestionModel()
                        {
                            ID = quest.ID.ToString(),
                            Question = quest.GetStringField(CreatePostTemplates.ArticleQuestion.Fields.Question),
                            Response = quest.GetStringField(CreatePostTemplates.ArticleQuestion.Fields.Response),
                            ResponseInPercentage = quest.GetBooleanField(CreatePostTemplates.ArticleQuestion.Fields.DisplayResponseinPercentage),
                            Correct = quest.GetStringField(CreatePostTemplates.ArticleQuestion.Fields.Correct),
                        };
                        question.Answers = new List<Answer>();

                        foreach (string key in "A,B,C,D".Split(','))
                        {
                            var answer = string.Empty;
                            switch (key)
                            {
                                case "A":
                                    answer = quest.GetStringField(CreatePostTemplates.ArticleQuestion.Fields.A);
                                    break;
                                case "B":
                                    answer = quest.GetStringField(CreatePostTemplates.ArticleQuestion.Fields.B);
                                    break;
                                case "C":
                                    answer = quest.GetStringField(CreatePostTemplates.ArticleQuestion.Fields.C);
                                    break;
                                case "D":
                                    answer = quest.GetStringField(CreatePostTemplates.ArticleQuestion.Fields.D);
                                    break;
                            }
                            question.Answers.Add(new Answer()
                            {
                                Opt = key,
                                Ans = answer,
                                isChecked = !string.IsNullOrEmpty(question.Correct) ? question.Correct.Contains(key) : false
                            });
                        }
                        model.Questionaire.Add(question);
                    }

                    model.QuestionnaireJson = JsonConvert.SerializeObject(model.Questionaire);

                    model.CategoryIds = new List<string>();

                    MultilistField refMultilistField = articleItem.Fields[CreatePostTemplates.Article.Fields.Categories];
                    if (refMultilistField != null)
                    {
                        Item[] items = refMultilistField.GetItems();
                        foreach (var item in items)
                        {
                            model.CategoryIds.Add(item.ID.ToString());
                        }
                    }
                }

            }
            return model;
        }
        private List<CategoryModel> GetCategories(Item categoryGroup)
        {
            var categories = new List<CategoryModel>();
            foreach (var Item in categoryGroup.Axes.GetDescendants().Where(x => x.TemplateID == ArticleTemplates.Category.ID))
            {
                var category = new CategoryModel()
                {
                    Name = string.IsNullOrEmpty(Item[ArticleTemplates.Category.Fields.Name]) ? Item.Name : Item[ArticleTemplates.Category.Fields.Name],
                    ID = Item.ID.ToString()
                };
                category.SubCategories = new List<SubCategoryModel>();
                foreach (var subItem in Item.Children.Where(x => x.TemplateID == ArticleTemplates.SubCategory.ID))
                {
                    var subCategory = new SubCategoryModel()
                    {
                        Name = string.IsNullOrEmpty(subItem[CreatePostTemplates.SubCategory.Fields.Name]) ? subItem.Name : subItem[CreatePostTemplates.SubCategory.Fields.Name],
                        ID = subItem.ID.ToString()
                    };
                    category.SubCategories.Add(subCategory);
                }
                categories.Add(category);
            }
            return categories;
        }
        /// <summary>
        /// Adds the post.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public bool AddPost(CreatePostViewModel model, string userid, out string Name, out string Id)
        {
            Name = string.Empty;
            Id = string.Empty;
            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                if (model.Article != null && !string.IsNullOrEmpty(model.Article.Name))
                {
                    Item newItem = null;
                    TemplateItem articleGroup = CurrentDb.GetTemplate(CreatePostTemplates.Article.ArticleGroup);

                    TemplateItem template = CurrentDb.GetTemplate(CreatePostTemplates.Article.ID);
                    if (model.IsUpdate)
                    {
                        newItem = CurrentDb.GetItem(model.ArticleMediaId);
                        Name = newItem.DisplayName;
                        if (newItem.Versions.Count > 3)
                        {
                            newItem.Versions[Version.First].Delete();
                        }
                        newItem = newItem.Versions.AddVersion();
                    }
                    else
                    {
                        var articleName = ItemUtil.ProposeValidItemName(model.Article.Name);
                        var monthItem = Utils.GetMonthItem(ArticleFolder, articleGroup);
                        newItem = monthItem.Add(articleName, template);
                        newItem.Editing.BeginEdit();
                        newItem.Fields[Sitecore.FieldIDs.CreatedBy].Value = userid;
                        newItem.Editing.EndEdit();
                        Name = newItem.DisplayName;
                    }
                    Id = newItem.ID.ToString();
                    string workflowID = template.StandardValues[Sitecore.FieldIDs.DefaultWorkflow];

                    if (!string.IsNullOrEmpty(workflowID))
                    {
                        newItem.Editing.BeginEdit();


                        newItem.Fields[Sitecore.FieldIDs.Workflow].Value = workflowID;
                        IWorkflow wf = CurrentDb.WorkflowProvider.GetWorkflow(workflowID);
                        wf.Start(newItem);
                        newItem.Editing.EndEdit();
                    }
                    try
                    {
                        newItem.Editing.BeginEdit();
                        if (!string.IsNullOrEmpty(model.Article.Name))
                        {
                            newItem.Fields[ArticleTemplates.Article.Details.Fields.Name].Value = model.Article.Name;
                        }
                        if (!string.IsNullOrEmpty(model.Article.Body))
                        {
                            var bodyContent = HttpUtility.UrlDecode(model.Article.Body);
                            //To make sure the CM url removed while publishing the content.
                            bodyContent = bodyContent.Replace(HttpContext.Current.Request.Url.GetLeftPart(System.UriPartial.Authority), "");
                            newItem.Fields[ArticleTemplates.Article.Details.Fields.Body].Value = bodyContent;
                        }
                        if (!string.IsNullOrEmpty(model.Article.ExternalUrl))
                        {
                            LinkField lnkField = newItem.Fields[ArticleTemplates.Article.Details.Fields.ExternalUrl];
                            if (lnkField != null)
                            {
                                lnkField.LinkType = "external";
                                lnkField.Url = model.Article.ExternalUrl;
                            }

                        }
                        if (model.Categories != null)
                        {
                            MultilistField multilistField = newItem.Fields[ArticleTemplates.Article.Details.Fields.Categories];
                            if (multilistField != null)
                            {
                                multilistField.Value = string.Empty;
                                foreach (var subcategory in model.Categories.Where(m => m.SubCategories != null).SelectMany(x => x.SubCategories.Select(y => y.ID)))
                                {
                                    if (!multilistField.Contains(subcategory))
                                    {
                                        multilistField.Add(subcategory);
                                    }
                                }
                            }
                        }
                        if (model.ThumbnailImage != null && (model.ThumbnailImage.ContentLength > 0))
                        {
                            var image = SCItemExtensions.AddFileToMediaLibrary(model.ThumbnailImage);
                            SCItemExtensions.AssignMediaItem(newItem.Fields[ArticleTemplates.Article.Details.Fields.Image], image);
                        }
                        if (!string.IsNullOrEmpty(model.Article.Image))
                        {
                            if ((model.Article.Image.StartsWith("http://") || model.Article.Image.StartsWith("https://")))
                            {
                                var image = SCItemExtensions.AddFileToMediaLibrary(model.Article.Image, newItem.Name);
                                if (image != null)
                                {
                                    SCItemExtensions.AssignMediaItem(newItem.Fields[ArticleTemplates.Article.Details.Fields.Image], image);
                                }
                            }
                        }

                        newItem.Editing.EndEdit();
                        UpdateQuestionaire(model.Questionaire, CurrentDb, newItem, model.IsUpdate);
                    }
                    catch (System.Exception ex)
                    {
                        Log.Error("Could not update item " + newItem.Paths.FullPath + ": " + ex.Message, this);
                        newItem.Editing.CancelEdit();
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Updates the questionaire.
        /// </summary>
        /// <param name="questionaire">The questionaire.</param>
        /// <param name="database">The database.</param>
        /// <param name="parent">The parent.</param>
        private void UpdateQuestionaire(List<ArticleQuestionModel> questionaire, Database database, Item parent, bool isUpdate)
        {
            TemplateItem QuestionTemplate = database.GetTemplate(CreatePostTemplates.ArticleQuestion.ID);

            if (questionaire != null)
            {
                foreach (var quest in questionaire)
                {
                    Item newQuestItem = null;
                    var questName = ItemUtil.ProposeValidItemName(quest.Question);
                    if (isUpdate)
                    {
                        if (quest.ID != null)
                        {
                            newQuestItem = parent.Children.Where(x => x.ID.ToString() == quest.ID).FirstOrDefault();
                        }
                        else
                        {
                            newQuestItem = parent.Add(questName, QuestionTemplate);
                        }
                    }
                    else
                    {
                        newQuestItem = parent.Add(questName, QuestionTemplate);
                    }
                    if (newQuestItem == null) return;
                    newQuestItem.Editing.BeginEdit();

                    if (!string.IsNullOrEmpty(quest.Question))
                    {
                        newQuestItem.UpdateField(CreatePostTemplates.ArticleQuestion.Fields.Question, quest.Question);
                    }
                    foreach (var answer in quest.Answers)
                    {
                        switch (answer.Opt)
                        {
                            case "A":
                                newQuestItem.UpdateField(CreatePostTemplates.ArticleQuestion.Fields.A, answer.Ans);
                                break;
                            case "B":
                                newQuestItem.UpdateField(CreatePostTemplates.ArticleQuestion.Fields.B, answer.Ans);
                                break;
                            case "C":
                                newQuestItem.UpdateField(CreatePostTemplates.ArticleQuestion.Fields.C, answer.Ans);
                                break;
                            case "D":
                                newQuestItem.UpdateField(CreatePostTemplates.ArticleQuestion.Fields.D, answer.Ans);
                                break;
                        }
                    }

                    if (!string.IsNullOrEmpty(quest.Correct))
                    {
                        newQuestItem.UpdateField(CreatePostTemplates.ArticleQuestion.Fields.Correct, quest.Correct);
                    }
                    if (!string.IsNullOrEmpty(quest.Response))
                    {
                        newQuestItem.UpdateField(CreatePostTemplates.ArticleQuestion.Fields.Response, quest.Response);
                    }
                    newQuestItem.UpdateField(CreatePostTemplates.ArticleQuestion.Fields.DisplayResponseinPercentage, (quest.ResponseInPercentage) ? "1" : "0");

                    newQuestItem.Editing.EndEdit();
                }
            }
        }
    }
}