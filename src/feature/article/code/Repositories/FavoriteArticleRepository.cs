﻿using Loreal.Feature.Article.Models;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Security.Accounts;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;
using System.Linq;

namespace Loreal.Feature.Article.Repositories
{
    public class FavoriteArticleRepository : ModelRepository, IFavoriteArticleRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {

        public object GetModel(int count, bool isList = false)
        {
            ArticleListViewModel viewModel = new ArticleListViewModel();
            base.FillBaseProperties(viewModel);
            viewModel.ViewAll = Sitecore.Globalization.Translate.Text(Dictionary.ViewAllText);
            viewModel.Title = Rendering.Parameters["Title"];
            viewModel.NotFoundLabel = Sitecore.Globalization.Translate.Text(Dictionary.ArticlesNotFoundLabel);
            var datasource = Rendering.DataSourceItem;
            if (isList) { datasource = Sitecore.Context.Item; }
            viewModel.ViewUrl = LinkManager.GetItemUrl(datasource);
            //Get Favorite articles from DB
            var favoriteArticles = FollowandComments.Managers.LikeManager.GetTopThree();
            if (favoriteArticles != null && favoriteArticles.Count > 0)
            {
                List<Item> listItems = new List<Item>();
                int i = 0;
                foreach (var article in favoriteArticles)
                {
                    listItems.Add(Sitecore.Context.Database.GetItem(article.ArticleID));
                    i++;
                    if (i == count) { break; }
                }
                viewModel.Count = favoriteArticles.Count;
                viewModel.Articles = Utils.ProcessArticles(listItems, datasource);
            }
            else
            {
                int rcount;
                viewModel.Articles = Utils.GetArticlesByCategory(Rendering.Parameters["DefaultCategory"], 0, count, datasource, out rcount);
                viewModel.Count = rcount;
            }
            return viewModel;
        }
    }
}