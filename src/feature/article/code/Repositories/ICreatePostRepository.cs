﻿using Loreal.Feature.Article.Models;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Article.Repositories
{
    public interface ICreatePostRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        bool AddPost(CreatePostViewModel model, string userid, out string name, out string id);
    }
}
