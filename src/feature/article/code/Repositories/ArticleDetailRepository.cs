﻿using Loreal.Feature.Article.Models;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Security.Accounts;
using Sitecore.Web.UI.WebControls;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Text.RegularExpressions;
using System.Web.Security;

namespace Loreal.Feature.Article.Repositories
{
    public class ArticleDetailRepository : ModelRepository, IArticleDetailRespository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public object GetModel(Item subCategoryItem)
        {
            ArticleDetailViewModel viewModel = new ArticleDetailViewModel();
            base.FillBaseProperties(viewModel);

            viewModel.PostDetails = Translate.Text(Dictionary.PostDeatils.Title);
            viewModel.Posted = Translate.Text(Dictionary.PostDeatils.Posted);
            viewModel.PostedBy = Translate.Text(Dictionary.PostDeatils.PostedBy);
            viewModel.Category = Translate.Text(Dictionary.PostDeatils.Category);
            viewModel.Like = Translate.Text(Dictionary.PostDeatils.Like);

            viewModel.MedratorMessage = Translate.Text(Dictionary.Actions.ModeratorMessage);
            viewModel.MedratorButtonText = Translate.Text(Dictionary.Actions.ModeratorButtonText);

            if (Sitecore.Context.Item.TemplateID == ArticleTemplates.Article.Details.ID)
            {

                var item = Sitecore.Context.Item;
                ArticleModel article = new ArticleModel();
                article.Name = FieldRenderer.Render(item, item.Fields[ArticleTemplates.Article.Details.Fields.Name]?.Name);
                article.Image = FieldRenderer.Render(item, item.Fields[ArticleTemplates.Article.Details.Fields.Image]?.Name, "class=img-responsive");
                article.CreatedDate = Utils.DateFormat(Sitecore.DateUtil.ToServerTime(item.Statistics.Created));
                article.CreatedBy = Sitecore.Security.Accounts.User.FromName(item.Statistics.CreatedBy, false)?.Profile.FullName;
                article.Body = item.Fields[ArticleTemplates.Article.Details.Fields.Body]?.Value;
                article.Like = FollowandComments.Managers.LikeManager.Get(item.ID.ToString());
                LinkField link = item.Fields[ArticleTemplates.Article.Details.Fields.ExternalUrl];
                if (link != null)
                {
                    article.ExternalUrl = link.GetFriendlyUrl();
                    bool isDownload = new Regex(@"(?:([^:/?#]+):)?(?://([^/?#]*))?([^?#]*\.(doc|pdf))(?:\?([^#]*))?(?:#(.*))?").IsMatch(article.ExternalUrl);
                    article.ExternalUrlText = (link.IsInternal || isDownload) ? Translate.Text(Dictionary.DownloadButtonLabel) : Translate.Text(Dictionary.AccessSiteButtonLabel);
                    viewModel.IsDownload = (link.IsInternal || isDownload);
                }
                viewModel.Article = article;
                // Follow.
                var categoryItem = Utils.GetCategoryByCurrentArticle(subCategoryItem);
                FollowModel follow = new FollowModel();
                if (categoryItem != null)
                {
                    article.Category = categoryItem.Fields[ArticleTemplates.NavigationTitle]?.Value;
                    follow.CategoryFollowText = FollowandComments.Managers.FollowCategoryArticle.Get(categoryItem.ID.ToString());
                    follow.CategoryFollowUrl = LinkManager.GetItemUrl(categoryItem);
                }

                var user = Membership.GetUser(item.Statistics.CreatedBy, false)?.ProviderUserKey.ToString();
                if (user != null && User.Current?.Profile?.StartUrl != user)
                {
                    // Enable follow user button post is not created by login user.
                    follow.UserFollowText = FollowandComments.Managers.FollowUserArticle.Get(user);
                    follow.UserFollowUrl = user;
                    viewModel.ReportToMedrator = Translate.Text(Dictionary.Actions.ReportToModerator);
                }
                // Enable Edit and delete post if the post created by login user and administrator
                if ((user != null && User.Current?.Profile?.StartUrl == user) || User.Current.IsAdministrator)
                {
                    viewModel.EditPost = Translate.Text(Dictionary.Actions.EditPost);
                    viewModel.DeletePost = Translate.Text(Dictionary.Actions.DeletePost);
                    viewModel.TweetPost = true;
                }
                article.Follow = follow;
                // Check article maped to webcast or not
                viewModel.IsWebcast = string.IsNullOrEmpty(item.Fields[ArticleTemplates.Article.Details.Fields.WebCastMapping]?.Value) ? false : true;

            }
            return viewModel;
        }
    }
}