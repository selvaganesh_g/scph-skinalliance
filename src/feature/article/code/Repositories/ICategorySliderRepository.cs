﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Article.Repositories
{
    public interface ICategorySliderRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
    }
}
