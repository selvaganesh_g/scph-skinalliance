﻿using Loreal.Feature.Article.Models;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;

namespace Loreal.Feature.Article.Repositories
{
    public class ArticleSearchRepository : ModelRepository, IArticleSearchRespository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public object GetModel(string query, Foundation.Search.SortType sortBy, int offset)
        {
            ArticleListViewModel viewModel = new ArticleListViewModel();
            viewModel.SortByLabel = Sitecore.Globalization.Translate.Text(Dictionary.Sortby.Title);
            viewModel.ViewAll = Sitecore.Globalization.Translate.Text(Dictionary.ViewAllText);
            viewModel.Articles = new List<ArticleModel>();
            int count = 0;
            if (!string.IsNullOrEmpty(query))
            {
                viewModel.Articles = Utils.SearchArticles(query, sortBy, offset, 12, Sitecore.Context.Item, out count);
            }
            viewModel.Count = count;
            viewModel.NotFoundLabel = Sitecore.Globalization.Translate.Text(Dictionary.ArticlesNotFoundLabel);
            viewModel.Sortby = Utils.GetSortByModel();
            return viewModel;
        }
    }
}