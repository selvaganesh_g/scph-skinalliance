﻿using Loreal.Feature.Article.Models;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;

namespace Loreal.Feature.Article.Repositories
{
    public class ArticleListRepository : ModelRepository, IArticleListRespository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public object GetModel(Foundation.Search.SortType sortBy, int offset)
        {
            ArticleListViewModel viewModel = new ArticleListViewModel();

            if (Sitecore.Context.Item.TemplateID == ArticleTemplates.SubCategory.PageID)
            {
                viewModel.SortByLabel = Sitecore.Globalization.Translate.Text(Dictionary.Sortby.Title);

                viewModel.ViewAll = Sitecore.Globalization.Translate.Text(Dictionary.ViewAllText);
                LookupField droplinkfield = Sitecore.Context.Item.Fields[ArticleTemplates.SubCategory.PageSource];
                var subcategoryItem = droplinkfield?.TargetItem;
                if (subcategoryItem == null) return viewModel;
                int count;
                viewModel.Articles = Utils.GetArticlesListByCategory(subcategoryItem.ID.ToString(), offset, 12, sortBy, Sitecore.Context.Item, out count);
                viewModel.Count = count;
                
            }
            viewModel.NotFoundLabel = Sitecore.Globalization.Translate.Text(Dictionary.ArticlesNotFoundLabel);
            viewModel.Sortby = Utils.GetSortByModel();
            return viewModel;
        }
    }
}