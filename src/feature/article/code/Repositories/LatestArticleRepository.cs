﻿using Loreal.Feature.Article.Models;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;

namespace Loreal.Feature.Article.Repositories
{
    public class LatestArticleRepository : ModelRepository, ILatestArticleRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {

        public object GetModel(int count, bool isList = false)
        {
            ArticleListViewModel viewModel = new ArticleListViewModel();
            base.FillBaseProperties(viewModel);

            viewModel.ViewAll = Sitecore.Globalization.Translate.Text(Dictionary.ViewAllText);
            viewModel.NotFoundLabel = Sitecore.Globalization.Translate.Text(Dictionary.ArticlesNotFoundLabel);
            var datasource = Rendering.DataSourceItem;
            if (isList) { datasource = Sitecore.Context.Item; }
            if (Rendering.DataSourceItem != null)
            {
                viewModel.Title = Rendering.Parameters["Title"];
                viewModel.ViewUrl = LinkManager.GetItemUrl(datasource);
                int rcount;
                viewModel.Articles = Utils.GetLatestArticles(count, datasource, out rcount);
                viewModel.Count = rcount;
            }
            return viewModel;
        }
    }
}