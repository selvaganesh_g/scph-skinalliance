﻿using Loreal.Feature.Article.Models;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Article.Repositories
{
    public class CategoryRepository : ModelRepository, ICategoryRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            CategoryViewModel categoryViewModel = new CategoryViewModel();
            List<CategoryModel> categories = new List<CategoryModel>();
            base.FillBaseProperties(categoryViewModel);
            foreach (var Item in Sitecore.Context.Item.Children.Where(x => x.TemplateID == ArticleTemplates.Category.PageID))
            {
                if (Utils.HideCategory(Item))
                {
                    continue;
                }
                CategoryModel category = new CategoryModel();
                category.Name = Item.Fields[ArticleTemplates.NavigationTitle].Value;
                LookupField droplinkfield = Item.Fields[ArticleTemplates.Category.PageSource];
                var categoryItem = droplinkfield?.TargetItem;
                if (categoryItem == null) continue;
                category.Description = categoryItem.Fields[ArticleTemplates.Category.Fields.Description].Value;
                ImageField media = categoryItem.Fields[ArticleTemplates.Category.Fields.Image];
                if (media != null && media.MediaItem != null)
                {
                    category.Image = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(media.MediaItem));
                }
                category.Link = LinkManager.GetItemUrl(Item);
                categories.Add(category);
                List<SubCategoryModel> subCategories = new List<SubCategoryModel>();
                foreach (var subItem in Item.Children.Where(x => x.TemplateID == ArticleTemplates.SubCategory.PageID))
                {
                    if (Utils.HideCategory(subItem))
                    {
                        continue;
                    }
                    droplinkfield = subItem.Fields[ArticleTemplates.SubCategory.PageSource];
                    var subcategoryItem = droplinkfield?.TargetItem;
                    if (subcategoryItem == null) continue;
                    SubCategoryModel subCategory = new SubCategoryModel();
                    subCategory.Name = subItem.Fields[ArticleTemplates.NavigationTitle].Value;
                    subCategory.Link = LinkManager.GetItemUrl(subItem);
                    subCategories.Add(subCategory);
                }
                category.SubCategories = subCategories;
            }
            categoryViewModel.Categories = categories;
            return categoryViewModel;
        }
    }
}