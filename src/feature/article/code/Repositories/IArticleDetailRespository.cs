﻿using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Article.Repositories
{
    public interface IArticleDetailRespository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        object GetModel(Item subCategroyItem);
    }
}
