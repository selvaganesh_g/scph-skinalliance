﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Article.Repositories
{
    public interface IArticleSearchRespository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        object GetModel(string query, Foundation.Search.SortType sortBy, int offset);
    }
}
