﻿using Sitecore.Data;

namespace Loreal.Feature.EmailService
{
    public struct EmailSettings
    {
        public static readonly ID ID = new ID("{DCD5804A-4EE9-412C-98F6-C64E02800AFC}");
        public struct Fields
        {
            public static readonly ID Host = new ID("{5E166853-D46B-42DC-9364-89A3D5EF5185}");
            public static readonly ID Port = new ID("{2966F64A-5396-490C-9315-001F49DDC76C}");
            public static readonly ID EnableSSL = new ID("{A69CE092-754B-4241-9D54-162A578A0882}");
            public static readonly ID Username = new ID("{DB137E15-7103-48EB-B64E-63BCDC62785A}");
            public static readonly ID Password = new ID("{C5267507-C687-46FF-B773-E6D81CA7452C}");
            public static readonly ID FromAddress = new ID("{E290370E-F649-4F16-B5EC-6781F1DF5014}");
            public static readonly ID ForgotTemplate = new ID("{6C0EC1B9-5CAF-4B92-847C-719BAC260435}");
            public static readonly ID ForgotSubject = new ID("{F252A2C7-865D-4DB5-9595-FBE09FF5D183}");
            public static readonly ID WelcomeTemplate = new ID("{D3B093EB-B17A-4C4C-951F-E298B15A09D8}");
            public static readonly ID WelcomeSubject = new ID("{F4A22064-F75C-403E-BDD0-E92EA919F3E7}");
            public static readonly ID ModerateEmailAddress = new ID("{1BA56A98-446B-40C7-B2F1-75AA395EC910}");
            public static readonly ID ReplyTemplate = new ID("{4F8556F4-E084-49B6-8691-251369913D27}");
            public static readonly ID ReplySubject = new ID("{EB8B00AC-D11D-4439-869F-452AE7A99605}");
            public static readonly ID ReportModerateTemplate = new ID("{44373252-197B-4F52-A069-6595AABC0BD8}");
            public static readonly ID ReportModerateSubject = new ID("{E4E971F7-8586-493D-B282-03BEE3DCD850}");
            public static readonly ID MyNotificationSubject = new ID("{76429873-6417-4755-B502-6BED54D7EA95}");
            public static readonly ID MyNotificatioTemplate = new ID("{2FEF99D9-E528-461C-A291-2517E53B2C93}");
            public static readonly ID DataCenter = new ID("{704CD652-A734-4CAE-8927-F1900534317A}");
            public static readonly ID ApiKey = new ID("{6D39F34D-54CD-4CF6-A4E5-734A58651F9C}");
            public static readonly ID ListID = new ID("{155860BF-11C0-4893-AEAC-822F7000E352}");
            public static readonly ID BCC = new ID("{4D6B90E4-27A1-430F-9D0B-161AE9BBC2A9}");
            public static readonly ID NewuserSubject = new ID("{9B89CF11-79DB-4F2F-8C81-2461A8A84849}");
            public static readonly ID NewuserBody = new ID("{27F1AF50-8124-4E33-867E-95FC732FBF1C}");
            public static readonly ID NewPostSubject = new ID("{9E2F327D-B09C-4224-BAC5-1DAF60606C80}");
            public static readonly ID NewPostBody = new ID("{2E28EABE-3D73-4340-A4EC-D7120C4A9DE0}");
            public static readonly ID NotifyUser = new ID("{E0503F8B-4620-40CA-B91D-5E21D890AC46}");
            public static readonly ID NotifyBCC = new ID("{B3D4E085-F781-4C80-A49D-DF2E46A9AE46}");
            public static readonly ID IUserSubject = new ID("{2D2FAE5D-C95A-4CA1-AA6B-B248191DD7C6}");
            public static readonly ID IUserBody = new ID("{C7CE7587-4653-4071-AB28-44D23A95FD38}");
        }

    }
}
