﻿using Newtonsoft.Json;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Loreal.Feature.EmailService.Services
{
    public class EmailService
    {
        private static Item _SettingsItem;
        public static Item MailSettings()
        {
            if (_SettingsItem != null) { return _SettingsItem; }

            Item SettingsItem = ServiceLocator.Current.Resolve<IMultisiteContext>().SettingsItem;
            if (SettingsItem == null)
            {
                SettingsItem = ServiceLocator.Current.Resolve<IMultisiteContext>()?.GetSettingsItem(Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath));
            }
            Item item = SettingsItem?.Children.FirstOrDefault(x => x.TemplateID == EmailSettings.ID);
            if (item != null)
            {
                return item;
            }
            return null;

        }
        public static void SendForgotpassword(string userName, string emailaddress, string password)
        {
            var body = MailSettings().Fields[EmailSettings.Fields.ForgotTemplate].Value;
            if (!string.IsNullOrEmpty(body))
            {
                body = body.Replace("#password#", password).Replace("#username#", userName);
            }
            SendMail(emailaddress, MailSettings().Fields[EmailSettings.Fields.ForgotSubject]?.Value, body);
        }
        public static void SendReply(string replyText, string link)
        {
            var body = MailSettings().Fields[EmailSettings.Fields.ReplyTemplate].Value;
            if (!string.IsNullOrEmpty(body))
            {
                body = body.Replace("#reply#", replyText).Replace("#link#", link);
            }
            SendMail(MailSettings().Fields[EmailSettings.Fields.ModerateEmailAddress]?.Value, MailSettings().Fields[EmailSettings.Fields.ReplySubject]?.Value, body);
        }

        public static void ReportModerate(string link, bool isDelete)
        {
            var body = MailSettings().Fields[EmailSettings.Fields.ReportModerateTemplate].Value;
            var subject = string.Format("{0} {1}", isDelete ? "DELETE ARTICLE" : "MODERATE ARTICLE", MailSettings().Fields[EmailSettings.Fields.ReportModerateSubject]?.Value);
            if (!string.IsNullOrEmpty(body))
            {
                body = body.Replace("#link#", link);
            }
            var bcc = MailSettings().Fields[EmailSettings.Fields.BCC].Value;
            SendMail(MailSettings().Fields[EmailSettings.Fields.ModerateEmailAddress]?.Value, subject, body, bcc);
        }
        public static void Newpost(string link, string name)
        {

            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                var body = MailSettings().Fields[EmailSettings.Fields.NewPostBody]?.Value;
                var subject = MailSettings().Fields[EmailSettings.Fields.NewPostSubject]?.Value;
                if (!string.IsNullOrEmpty(body))
                {
                    body = body.Replace("#link#", link);
                }
                var bcc = MailSettings().Fields[EmailSettings.Fields.BCC].Value;
                SendMail(MailSettings().Fields[EmailSettings.Fields.ModerateEmailAddress]?.Value, subject, body, bcc);
            }
        }

        public static void SendWelcomeMail(string userName, string email)
        {
            var body = MailSettings().Fields[EmailSettings.Fields.WelcomeTemplate].Value;
            if (!string.IsNullOrEmpty(body))
            {
                body = body.Replace("#name#", userName);
            }
            SendMail(email, MailSettings().Fields[EmailSettings.Fields.WelcomeSubject]?.Value, body);
            SendWelcomeBCC(userName, email);
        }
        public static void SendImportUserWelcomeMail(string userName, string email)
        {
            var body = MailSettings().Fields[EmailSettings.Fields.IUserBody].Value;
            if (!string.IsNullOrEmpty(body))
            {
                body = body.Replace("#name#", userName).Replace("#email#",email);
            }
            var subject = MailSettings().Fields[EmailSettings.Fields.IUserSubject].Value;
            if (!string.IsNullOrEmpty(subject))
            {
                subject = subject.Replace("#name#", userName).Replace("#email#", email);
            }

            bool notityUser = Sitecore.MainUtil.GetBool(MailSettings().Fields[EmailSettings.Fields.NotifyUser].Value, false);
            if (notityUser)
            {
                SendMail(email, string.Format(MailSettings().Fields[EmailSettings.Fields.IUserSubject]?.Value, userName), body);
            }
            bool notifyBCC = Sitecore.MainUtil.GetBool(MailSettings().Fields[EmailSettings.Fields.NotifyBCC].Value, false);
            if (notifyBCC)
            {
                string nUsrBody = MailSettings().Fields[EmailSettings.Fields.NewuserBody]?.Value;
                nUsrBody = nUsrBody.Replace("#name#", userName).Replace("#email#", email);
                string nUsrSubject = MailSettings().Fields[EmailSettings.Fields.NewuserSubject]?.Value;
                nUsrSubject = nUsrSubject.Replace("#name#", userName).Replace("#email#", email);
                SendMail(MailSettings().Fields[EmailSettings.Fields.BCC].Value, nUsrSubject, nUsrBody);
            }
        }

        public static void SendWelcomeBCC(string userName, string email)
        {
            var body = MailSettings().Fields[EmailSettings.Fields.NewuserBody].Value;
            if (!string.IsNullOrEmpty(body))
            {
                body = body.Replace("#name#", userName).Replace("#email#", email);
            }
            var bcc = MailSettings().Fields[EmailSettings.Fields.BCC].Value;
            if (!string.IsNullOrEmpty(bcc))
            {
                SendMail(bcc, MailSettings().Fields[EmailSettings.Fields.NewuserSubject]?.Value, body);
            }
        }
        public static void SendMail(string toEmailAddress, string subject, string body, string bcc = "")
        {
            try
            {
                string hostName = MailSettings().Fields[EmailSettings.Fields.Host]?.Value;
                string port = MailSettings().Fields[EmailSettings.Fields.Port]?.Value;

                bool enableSsl = MailSettings().Fields[EmailSettings.Fields.EnableSSL]?.Value == "1" ? true : false;

                if (string.IsNullOrEmpty(hostName)) { return; }

                SmtpClient client = !string.IsNullOrEmpty(port) ? new SmtpClient(hostName, Convert.ToInt32(port)) : new SmtpClient(hostName);

                client.EnableSsl = enableSsl;

                if (!string.IsNullOrEmpty(MailSettings().Fields[EmailSettings.Fields.Username]?.Value) && !string.IsNullOrEmpty(MailSettings().Fields[EmailSettings.Fields.Password]?.Value))
                {
                    client.Credentials = new NetworkCredential(MailSettings().Fields[EmailSettings.Fields.Username]?.Value,
                        MailSettings().Fields[EmailSettings.Fields.Password]?.Value);
                }

                MailMessage message = new MailMessage(MailSettings().Fields[EmailSettings.Fields.FromAddress]?.Value, toEmailAddress, subject, body);
                message.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(bcc))
                {
                    message.Bcc.Add(new MailAddress(bcc));
                }
                client.Send(message);

            }
            catch (Exception exception)
            {
                Sitecore.Diagnostics.Log.Error(string.Format("SMTP EXCEPTION TYPE : {0}", exception.InnerException == null ? exception.Message : exception.InnerException.Message), exception);
                Sitecore.Diagnostics.Log.Error(string.Format("SMTP EXCEPTION TYPE : {0}", exception.StackTrace), exception);

            }
        }
        public static void SendContacUs(string email, string subject, string body)
        {
            var bcc = MailSettings().Fields[EmailSettings.Fields.BCC].Value;
            SendMail(email, subject, body, bcc);
        }
        public static void SendNotification(Item MailSettings, string email, string subject, string body)
        {
            _SettingsItem = MailSettings;
            SendMail(email, subject, body);
        }

        public static void AddOrUpdateListMember(string subscriberEmail, string firstName, string lastName, bool subscribe, string country, string lang)
        {
            if (MailSettings() != null)
            {
                string dataCenter = MailSettings().Fields[EmailSettings.Fields.DataCenter].Value;
                string apiKey = MailSettings().Fields[EmailSettings.Fields.ApiKey].Value;
                string listId = MailSettings().Fields[EmailSettings.Fields.ListID].Value;
                var sampleListMember = JsonConvert.SerializeObject(
                new
                {
                    email_address = subscriberEmail,
                    merge_fields =
                    new
                    {
                        FNAME = firstName,
                        LNAME = lastName
                    },
                    language = lang,
                    status = subscribe ? "subscribed" : "unsubscribed",
                    update_existing = true
                });

                var hashedEmailAddress = string.IsNullOrEmpty(subscriberEmail) ? "" : CalculateMD5Hash(subscriberEmail.ToLower());
                var uri = string.Format("https://{0}.api.mailchimp.com/3.0/lists/{1}/members/{2}", dataCenter, listId, hashedEmailAddress);
                try
                {
                    using (var webClient = new WebClient())
                    {
                        webClient.Headers.Add("Accept", "application/json");
                        webClient.Headers.Add("Authorization", "apikey " + apiKey);

                        webClient.UploadString(uri, "PUT", sampleListMember);
                    }
                }
                catch (WebException we)
                {
                    using (var sr = new StreamReader(we.Response.GetResponseStream()))
                    {
                        Sitecore.Diagnostics.Log.Error(sr.ReadToEnd(), "MailChimp");
                    }
                }
            }
        }

        private static string CalculateMD5Hash(string input)
        {
            // Step 1, calculate MD5 hash from input.
            var md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // Step 2, convert byte array to hex string.
            var sb = new StringBuilder();
            foreach (var @byte in hash)
            {
                sb.Append(@byte.ToString("X2"));
            }
            return sb.ToString();
        }
    }
}