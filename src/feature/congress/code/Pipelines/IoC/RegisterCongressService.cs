﻿using Loreal.Feature.Congress.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.XA.Foundation.IOC.Pipelines.IOC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Congress.Pipelines.IoC
{
    public class RegisterCongressService : IocProcessor
    {
        public override void Process(IocArgs args)
        {
            args.ServiceCollection.AddTransient<ICongressCategoryRepository, CongressCategoryRepository>();
            args.ServiceCollection.AddTransient<ICongressCategoryListRepository, CongressCategoryListRepository>();
            args.ServiceCollection.AddTransient<ICongressDetailRepository, CongressDetailRepository>();
            args.ServiceCollection.AddTransient<IUpComingCongressRepository, UpComingCongressRepository>();
        }
    }
}