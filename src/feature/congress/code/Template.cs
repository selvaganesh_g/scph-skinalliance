﻿using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Congress
{
    public class CongressTemplates
    {
        public struct Congress
        {
            public static readonly ID CategoryPage = new ID("{8D832090-41A5-4877-B55A-674E9589F5A6}");
            public static readonly ID Page = new ID("{A544FAB9-0691-4CBB-9442-F859938C66DB}");
            public struct Fields
            {
                public static readonly ID Archive = new ID("{A2D19836-F9BC-4B04-9828-3C1CB5B34462}");
                public static readonly ID Title = new ID("{C622D8AD-04E6-45BF-A5C3-69984A73AE90}");
                public static readonly ID Description = new ID("{7F8865A6-BC37-4085-9AC3-960AD6B56276}");
                public static readonly ID Date = new ID("{C6B3A3E2-C3AE-40EE-B72A-D6E501E0480A}");
                public static readonly ID Image = new ID("{111E3C77-92E9-4201-97FB-2E73B9C0D26A}");
                public static readonly ID Details = new ID("{ED868A17-266A-469C-B8C7-CDA226B500B3}");
                public static readonly ID RegisterLink = new ID("{9C934B71-6E7C-4C1D-968B-1399D53C6A52}");
                public static readonly ID OtherCongressText = new ID("{C6159AF4-49DC-4EBF-9148-323B3CE39A03}");
                public static readonly ID OtherCongress = new ID("{C1338C1F-2831-4F02-B41A-1A862E03EEC4}");
            }
        }
        public static readonly ID NavigationTitle = new ID("{71FCA010-73D7-498A-A5B1-926F5A6954F9}");
    }
}