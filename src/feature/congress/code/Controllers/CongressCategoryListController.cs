﻿using Loreal.Feature.Congress.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Congress.Controllers
{
    public class CongressCategoryListController : StandardController
    {
        public ICongressCategoryListRepository _iCongressCategoryListRepository;
        public CongressCategoryListController(ICongressCategoryListRepository icongressCategoryListRepository)
        {
            _iCongressCategoryListRepository = icongressCategoryListRepository;
        }
        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Congress/CongressCategoryList.cshtml";
        }
        protected override object GetModel()
        {
            return _iCongressCategoryListRepository.GetModel();
        }
    }
}