﻿using Loreal.Feature.Congress.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Congress.Controllers
{
    public class CongressCategoryController : StandardController
    {
        public ICongressCategoryRepository _iCongressCategoryRepository;
        public CongressCategoryController(ICongressCategoryRepository icongressCategoryRepository)
        {
            _iCongressCategoryRepository = icongressCategoryRepository;
        }
        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Congress/CongressCategory.cshtml";
        }
        protected override object GetModel()
        {
            return _iCongressCategoryRepository.GetModel();
        }
    }
}