﻿using Loreal.Feature.Congress.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Congress.Controllers
{
    public class UpComingCongressController : StandardController
    {

        public IUpComingCongressRepository _iUpComingCongressRepository;

        public UpComingCongressController(IUpComingCongressRepository iUpComingCongressRepository)
        {
            _iUpComingCongressRepository = iUpComingCongressRepository;
        }

        protected override object GetModel()
        {
            return _iUpComingCongressRepository.GetModel();
        }

        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Congress/UpComingCongress.cshtml";
        }
    }
}