﻿using Loreal.Feature.Congress.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System.Web.Mvc;

namespace Loreal.Feature.Congress.Controllers
{
    public class CongressDetailController : StandardController
    {
        public ICongressDetailRepository _iCongressDetailRepository;
        public CongressDetailController(ICongressDetailRepository iCongressDetailRepository)
        {
            _iCongressDetailRepository = iCongressDetailRepository;
        }
        public ActionResult Banner()
        {
            return PartialView("~/Views/SkinAlliance/Feature/Congress/CongressDetailBanner.cshtml", _iCongressDetailRepository.GetModel());
        }
        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Congress/CongressDetail.cshtml";
        }
        protected override object GetModel()
        {
            return _iCongressDetailRepository.GetModel();
        }
    }
}