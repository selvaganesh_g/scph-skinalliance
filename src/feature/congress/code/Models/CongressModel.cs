﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Congress.Models
{
    public class CongressModel
    {
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public int Day { get; set; }
        public string Month { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }
        public string RegisterLink { get; set; }
        public string OtherText { get; set; }
        public string Link { get; set; }
    }
}