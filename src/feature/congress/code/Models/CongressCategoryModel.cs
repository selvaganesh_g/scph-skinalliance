﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Congress.Models
{
    public class CongressCategoryModel
    {
        public string Name { get; set; }

        public string Link { get; set; }

        public int Count { get; set; }

        public List<CongressModel> List { get; set; }
    }
}