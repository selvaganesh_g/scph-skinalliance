﻿using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Mvc.Models;
using System.Collections.Generic;

namespace Loreal.Feature.Congress.Models
{
    public class CongressDetailViewModel : RenderingModelBase
    {
        public string CategoryName { get; set; }
        public CongressModel Congress { get; set; }
        public List<CongressModel> OtherCongress { get; set; }
    }
}