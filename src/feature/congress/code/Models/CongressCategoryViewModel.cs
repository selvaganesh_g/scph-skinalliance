﻿using Sitecore.XA.Foundation.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Congress.Models
{
    public class CongressCategoryViewModel : RenderingModelBase
    {
        public List<CongressModel> List { get; set; }
    }
}