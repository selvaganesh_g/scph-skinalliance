﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Congress.Repositories
{
    public interface IUpComingCongressRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
    }
}
