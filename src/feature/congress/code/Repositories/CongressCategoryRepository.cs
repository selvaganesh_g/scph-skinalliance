﻿using Loreal.Feature.Congress.Models;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Loreal.Feature.Congress.Repositories
{
    public class CongressCategoryRepository : ModelRepository, ICongressCategoryRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            CongressCategoryViewModel model = new CongressCategoryViewModel();
            base.FillBaseProperties(model);
            List<CongressModel> congresslist = new List<CongressModel>();
            var rootItem = Sitecore.Context.Item;
            if (Rendering.DataSourceItem != null)
            {
                rootItem = Rendering.DataSourceItem;
            }
            foreach (var congress in rootItem.Children.Where(x => x.TemplateID == CongressTemplates.Congress.Page).OrderBy(x=>x.Fields[CongressTemplates.Congress.Fields.Date].Value))
            {
                var data = new CongressModel();
                Sitecore.Data.Fields.DateField dateField = congress.Fields[CongressTemplates.Congress.Fields.Date];
                if (dateField == null) continue;
                var utcDate = Sitecore.DateUtil.ToServerTime(dateField.DateTime);
                data.Day = utcDate.Day;
                data.Date = utcDate;
                data.Month = utcDate.ToString("MMM", CultureInfo.InvariantCulture);
                data.Description = congress.Fields[CongressTemplates.Congress.Fields.Description]?.Value;
                data.Image = FieldRenderer.Render(congress, congress.Fields[CongressTemplates.Congress.Fields.Image]?.Name, "class=img-responsive");
                data.Link = LinkManager.GetItemUrl(congress);
                congresslist.Add(data);
            }
            congresslist.OrderBy(x => x.Date);
            model.List = congresslist;
            return model;
        }
    }
}