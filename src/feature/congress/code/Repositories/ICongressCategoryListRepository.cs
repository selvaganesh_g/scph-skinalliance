﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Congress.Repositories
{
    public interface ICongressCategoryListRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
    }
}
