﻿using Loreal.Feature.Congress.Models;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Loreal.Feature.Congress.Repositories
{
    public class UpComingCongressRepository : ModelRepository, IUpComingCongressRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {

            CongressCategoryListViewModel model = new CongressCategoryListViewModel();
            base.FillBaseProperties(model);
            List<CongressCategoryModel> congressCategories = new List<CongressCategoryModel>();
            var congressItems = Sitecore.Context.Database.SelectItems(string.Format("fast://*[@@id = '{0}']//*[@@TemplateId = '{1}']", this.Rendering.DataSourceItem.ID, CongressTemplates.Congress.Page));
            var category = congressItems?.OrderBy(x => x.Fields[CongressTemplates.Congress.Fields.Date].Value).Where(x => !string.IsNullOrEmpty(x.Fields[CongressTemplates.Congress.Fields.Date].Value));

            if (category == null) { return model; }
            List<CongressModel> congresslist = new List<CongressModel>();
            int i = 0;
            foreach (var congress in category)
            {
                var data = new CongressModel();
                Sitecore.Data.Fields.DateField dateField = congress.Fields[CongressTemplates.Congress.Fields.Date];
                if (dateField == null) continue;
                var utcDate = Sitecore.DateUtil.ToServerTime(dateField.DateTime);
                if (utcDate.Date < System.DateTime.Now.Date) { continue; }
                data.Day = utcDate.Day;
                data.Month = utcDate.ToString("MMM", CultureInfo.InvariantCulture);
                data.Description = congress.Fields[CongressTemplates.Congress.Fields.Description]?.Value;
                data.Image = FieldRenderer.Render(congress, congress.Fields[CongressTemplates.Congress.Fields.Image]?.Name);
                data.Link = LinkManager.GetItemUrl(congress);
                congresslist.Add(data);
                i++;
                if (i == 4) { break; }
            }
            congressCategories.Add(new CongressCategoryModel()
            {
                Name = this.Rendering.Parameters["Title"],
                Link = LinkManager.GetItemUrl(this.Rendering.DataSourceItem),
                List = congresslist
            });

            model.List = congressCategories;
            return model;
        }
    }
}