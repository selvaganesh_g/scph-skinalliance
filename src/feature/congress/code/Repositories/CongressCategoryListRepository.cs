﻿using Loreal.Feature.Congress.Models;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Loreal.Feature.Congress.Repositories
{
    public class CongressCategoryListRepository : ModelRepository, ICongressCategoryListRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            CongressCategoryListViewModel model = new CongressCategoryListViewModel();
            base.FillBaseProperties(model);
            List<CongressCategoryModel> congressCategories = new List<CongressCategoryModel>();
            var rootItem = Sitecore.Context.Item;
            if (Rendering.DataSourceItem != null)
            {
                rootItem = Rendering.DataSourceItem;
            }
            foreach (var category in rootItem.Children.Where(x => x.TemplateID == CongressTemplates.Congress.CategoryPage))
            {
                if (!category.Children.Any() || category.Fields[CongressTemplates.Congress.Fields.Archive].Value == "1") continue;
                List<CongressModel> congresslist = new List<CongressModel>();
                int i = 0;
                var congresses = category.Children.Where(x => x.TemplateID == CongressTemplates.Congress.Page)
                    .OrderBy(x => x.Fields[CongressTemplates.Congress.Fields.Date].Value)
                    .Where(x => !string.IsNullOrEmpty(x.Fields[CongressTemplates.Congress.Fields.Date].Value));

                foreach (var congress in congresses)
                {
                    if (i == 4) { break; }
                    var data = new CongressModel();
                    Sitecore.Data.Fields.DateField dateField = congress.Fields[CongressTemplates.Congress.Fields.Date];
                    if (dateField == null) continue;
                    var utcDate = Sitecore.DateUtil.ToServerTime(dateField.DateTime);
                    data.Date = utcDate;
                    data.Day = utcDate.Day;
                    data.Month = utcDate.ToString("MMM", CultureInfo.InvariantCulture);
                    data.Description = congress.Fields[CongressTemplates.Congress.Fields.Description]?.Value;
                    data.Image = FieldRenderer.Render(congress, congress.Fields[CongressTemplates.Congress.Fields.Image]?.Name);
                    data.Link = LinkManager.GetItemUrl(congress);
                    congresslist.Add(data);
                    i++;
                }
                congresslist.OrderBy(x => x.Date);
                congressCategories.Add(new CongressCategoryModel()
                {
                    Name = category.Fields[CongressTemplates.NavigationTitle].Value,
                    Link = LinkManager.GetItemUrl(category),
                    List = congresslist,
                    Count = congresses.Count()
                });
            }
            model.List = congressCategories;
            return model;
        }
    }
}