﻿using Loreal.Feature.Congress.Models;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Loreal.Feature.Congress.Repositories
{
    public class CongressDetailRepository : ModelRepository, ICongressDetailRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            CongressDetailViewModel model = new CongressDetailViewModel();
            base.FillBaseProperties(model);
            Item contextItem = Sitecore.Context.Item;

            if (contextItem.TemplateID != CongressTemplates.Congress.Page)
            {
                Log.Error("current page template id not matching with congress page template id! ", this);
                return model;
            }
            Sitecore.Data.Fields.DateField dateField = Sitecore.Context.Item.Fields[CongressTemplates.Congress.Fields.Date];
            var utcDate = Sitecore.DateUtil.ToServerTime(dateField.DateTime);
            model.Congress = new CongressModel()
            {
                Title = FieldRenderer.Render(contextItem, contextItem.Fields[CongressTemplates.Congress.Fields.Title]?.Name),
                Day = utcDate.Day,
                Month = utcDate.ToString("MMM", CultureInfo.InvariantCulture),
                Description = FieldRenderer.Render(contextItem, contextItem.Fields[CongressTemplates.Congress.Fields.Description]?.Name),
                Image = FieldRenderer.Render(contextItem, contextItem.Fields[CongressTemplates.Congress.Fields.Image]?.Name, "class=img-responsive"),
                RegisterLink = FieldRenderer.Render(contextItem, contextItem.Fields[CongressTemplates.Congress.Fields.RegisterLink]?.Name, "class=btn btn-primary"),
                Details = FieldRenderer.Render(contextItem, contextItem.Fields[CongressTemplates.Congress.Fields.Details]?.Name),
                OtherText = FieldRenderer.Render(contextItem, contextItem.Fields[CongressTemplates.Congress.Fields.OtherCongressText]?.Name)
            };
            model.CategoryName = contextItem.Parent.Fields[CongressTemplates.NavigationTitle]?.Value;

            List<CongressModel> congresslist = new List<CongressModel>();
            int i = 0;
            MultilistField listField = contextItem.Fields[CongressTemplates.Congress.Fields.OtherCongress];
            List<Item> otherCongress = new List<Item>();
            if (listField != null && listField.GetItems().Count() > 0)
            {
                otherCongress = listField.GetItems().Where(x => x.TemplateID == CongressTemplates.Congress.Page).ToList();
            }
            else
            {
                otherCongress = contextItem.Parent.Children.OrderBy(x => x.Fields[CongressTemplates.Congress.Fields.Date].Value).Where(x => !string.IsNullOrEmpty(x.Fields[CongressTemplates.Congress.Fields.Date].Value)).Where(x => x.TemplateID == CongressTemplates.Congress.Page && x.ID != contextItem.ID).ToList();
            }
            foreach (var congress in otherCongress)
            {
                var data = new CongressModel();
                DateField congdataField = congress.Fields[CongressTemplates.Congress.Fields.Date];
                if (congdataField == null) continue;
                var utcDates = Sitecore.DateUtil.ToServerTime(congdataField.DateTime);
                if (utcDates.Date < System.DateTime.Now.Date) { continue; }
                data.Day = utcDates.Day;
                data.Month = utcDates.ToString("MMM", CultureInfo.InvariantCulture);
                data.Description = congress.Fields[CongressTemplates.Congress.Fields.Description]?.Value;
                data.Image = FieldRenderer.Render(congress, congress.Fields[CongressTemplates.Congress.Fields.Image]?.Name, "class=img-responsive");
                data.Link = LinkManager.GetItemUrl(congress);
                congresslist.Add(data);
                i++;
                if (i == 2) { break; }
            }
            model.OtherCongress = congresslist;
            return model;
        }
    }
}