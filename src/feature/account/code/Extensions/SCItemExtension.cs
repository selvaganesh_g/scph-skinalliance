﻿using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Loreal.Feature.Account.Extensions
{
    public static class SCItemExtension
    {
        public static string GetFieldValue(this Item item, ID field)
        {
            return item.Fields[field]?.Value;
        }

        public static string LinkFieldUrl(this Item item, ID fieldId)
        {
            LinkField linkField = item.Fields[fieldId];

            if (linkField != null)
            {
                return linkField.GetFriendlyUrl();
            }
            return string.Empty;
        }
    }
}