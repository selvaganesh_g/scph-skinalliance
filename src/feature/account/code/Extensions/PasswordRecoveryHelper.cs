﻿using Sitecore.Security.Accounts;
using Sitecore.SecurityModel;
using System;
using System.Linq;

namespace Loreal.Feature.Account.Extensions
{
    public static class PasswordRecoveryHelper
    {
        public static void StoreTokenOnUser(User user, string confirmToken)
        {
            using (new SecurityDisabler())
            {
                user.Profile.SetCustomProperty(AccountTemplate.ConfirmTokenKey, confirmToken);
                user.Profile.Save();
            }
        }

        public static string GenerateConfirmLink(string token, string userName)
        {
            var serverUrl = Sitecore.StringUtil.EnsurePostfix('/', Sitecore.Web.WebUtil.GetServerUrl());
            return string.Concat(serverUrl, "passwordrecovery?username=", userName.Replace('\\', '|'), "&token=", token);
        }

        public static void DeleteToken(User user)
        {
            using (new SecurityDisabler())
            {
                user.Profile.SetCustomProperty(AccountTemplate.ConfirmTokenKey, string.Empty);
                user.Profile.Save();
            }
        }

        public static bool TokenIsValid(User user, string token)
        {
            return !string.IsNullOrEmpty(token) && TokenExists(user, token);
        }

        public static bool IsResetPasswordTimeExpired(string token)
        {
            bool expired = true;
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    string time = token.Split('|').LastOrDefault();
                    DateTime expiryTime = DateTime.ParseExact(time, "yyyyMMddHHmmss", null);
                    int offset = Convert.ToInt32(Sitecore.Configuration.Settings.GetSetting("PasswordResetTimeOut"));
                    if (expiryTime.Subtract(DateTime.UtcNow).Days > offset)
                    {
                        expired = true;
                    }
                }
            }
            catch
            {
                expired = false;
            }
            return expired;
        }

        public static string GetToken(User user)
        {
            using (new SecurityDisabler())
            {
                return user.Profile.GetCustomProperty(AccountTemplate.ConfirmTokenKey);
            }
        }

        public static bool TokenExists(User user, string confirmToken)
        {
            using (new SecurityDisabler())
            {
                var tokenOnProfile = user.Profile.GetCustomProperty(AccountTemplate.ConfirmTokenKey);
                return !string.IsNullOrEmpty(tokenOnProfile) && tokenOnProfile.Equals(confirmToken, StringComparison.InvariantCultureIgnoreCase);
            }
        }
    }
}