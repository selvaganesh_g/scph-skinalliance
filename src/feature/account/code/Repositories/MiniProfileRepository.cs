﻿using Loreal.Feature.Account.Extensions;
using Loreal.Feature.Account.Models;
using Loreal.Feature.Account.Services;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Security.Accounts;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;

namespace Loreal.Feature.Account.Repositories
{
    public class MiniProfileRepository : ModelRepository, IMiniProfileRepository, IModelRepository,
        IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {

            MiniProfileModel model = new MiniProfileModel();
            base.FillBaseProperties(model);
            if (Rendering.DataSourceItem == null)
            {
                Sitecore.Diagnostics.Log.Error("Datasource is null or empty", this);
                return model;
            }
            model.Visible = PageContext.CurrentUser.IsAuthenticated;
            model.LogoutText = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.MiniProfile.Fields.LogoutText);
            model.WelcomeTitle = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.MiniProfile.Fields.WelcomeTitle);
            model.UserName = User.Current.Profile.FullName;
            IProfileService iprofileService = new ProfileService();
            model.Image = iprofileService.GetProfileImage(User.Current?.Profile);
            model.IsAccepted = iprofileService.GetAcceptedValue(User.Current?.Profile);
            LinkField link = Rendering.DataSourceItem.Fields[AccountTemplate.Account.MiniProfile.Fields.CreatePostLink];
            model.CreatePostLink = link?.GetFriendlyUrl();

            if (!model.IsAccepted)
            {
                var SiteSettings = ServiceLocator.Current.Resolve<IMultisiteContext>().GetSettingsItem(Sitecore.Context.Item);
                if (SiteSettings != null)
                {
                    model.TermsandConditions = new System.Web.HtmlString(SiteSettings.Fields[AccountTemplate.Settings.TermsandConditions]?.Value);
                    model.IAgreeText = SiteSettings.Fields[AccountTemplate.Settings.IAgreeText]?.Value;
                    model.TermsandConditionsTittle = string.Format("{0} {1}", SiteSettings.Fields[AccountTemplate.Settings.WelcomeText]?.Value, User.Current.Profile.FullName);
                }
            }
            MultilistField listField = Rendering.DataSourceItem.Fields[AccountTemplate.Account.MiniProfile.Fields.NavigationItems];
            List<KeyValuePair<string, string>> navigationLinks = new List<KeyValuePair<string, string>>();
            if (listField != null && listField.Count > 0)
            {
                foreach (Item item in listField.GetItems())
                {
                    navigationLinks.Add(new KeyValuePair<string, string>(string.IsNullOrEmpty(item.DisplayName) ? item.Name : item.DisplayName, LinkManager.GetItemUrl(item)));
                }
            }

            model.NavigationItems = navigationLinks;
            return model;
        }
    }
}