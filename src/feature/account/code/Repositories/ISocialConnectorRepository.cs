﻿using Sitecore.Data;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;

namespace Loreal.Feature.Account.Repositories
{
    public interface ISocialConnectorRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        string GetItemValue(ID field);
        IRenderingModelBase GetSocialModel(string redirctPath);
    }
}
