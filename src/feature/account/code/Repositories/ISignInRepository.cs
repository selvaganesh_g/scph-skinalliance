﻿namespace Loreal.Feature.Account.Repositories
{
    using Models;
    using Sitecore.XA.Foundation.Mvc.Repositories.Base;
    using System.Web;

    public interface ISignInRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        bool Login(string login, string password, bool rememberMe = true);
        string GetRedirectUrl(HttpRequestBase request, bool success);
        DiscoverDermocosmeticsScienceModel GetDDSModel();
    }
}
