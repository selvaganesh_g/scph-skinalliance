﻿
using Loreal.Feature.Account.Extensions;
using Loreal.Feature.Account.Models;
using Loreal.Feature.Account.Services;
using Sitecore;
using Sitecore.Security.Accounts;
using Sitecore.XA.Foundation.Multisite.Extensions;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Loreal.Feature.Account.Repositories
{
    public class ProfileRepository : ModelRepository, IProfileRepository, IModelRepository,
        IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            SignupViewModel model = new SignupViewModel();
            this.FillBaseProperties(model);
            if (Rendering.DataSourceItem == null)
            {
                Sitecore.Diagnostics.Log.Error("Datasource is null or empty", this);
                return model;
            }
            model.Introduction = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.Introduction);
            model.FirstNameCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.FirstNameLabel);
            model.LastNameCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.LastNameLabel);
            model.EmailCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.EmailLabel);
            model.PasswordCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PasswordLabel);
            model.ConfirmPasswordCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.ConfirmPasswordLabel);
            model.SignupButtonCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.SignupButtonLabel);
            model.PhoneNumberCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PhoneNumberCaption);
            model.AddressCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.AddressCaption);
            model.SummaryCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.SummaryCaption);
            model.CountryCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.CountryCaption);
            model.CityCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.CityCaption);
            model.ZipCodeCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.ZipCodeCaption);
            model.PhysicianLicenseNumberCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PhysicianLicenseNumberCaption);
            model.MedicalSpecialtyCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.MedicalSpecialtyCaption);
            model.ConfirmPasswordCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.ConfirmPasswordLabel);
            model.FirstNameRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.FirstNameRequired);
            model.LastNameRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.LastNameRequired);
            model.EmailRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.EmailRequired);
            model.InvalidEmail = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.InvalidEmail);
            model.PasswordRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PasswordRequired);
            model.ConfirmPasswordRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.ConfirmPasswordRequired);
            model.CountryRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.CountryRequired);
            model.PhysicianLicenseNumberRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PhysicianLicenseNumberRequired);
            model.MedicalSpecialtyRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.MedicalSpecialtyRequired);
            model.InvalidPassword = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.InvalidPassword);
            model.PasswordHint = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PasswordHintText);
            model.PasswordMismatch = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PasswordMismatch);
            model.FailureText = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.FailureText);
            model.UserExistText = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.UserExistsText);
            model.MedicalSpecialties = Utils.GetLookupItems(Context.Database.GetItem(AccountTemplate.Account.Signup.Fields.MedicalSpecialtyFolder));
            model.ActivityCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.ActivityCaption);
            model.PreferredService1Caption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PreferredService1Caption);
            model.PreferredService2Caption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PreferredService2Caption);
            model.NewsletterSubscribeCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.NewsletterSubscribeCaption);
            model.TermsandConditionText = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.TermsandConditionText);
            model.NewsLetterSubscribe = false;
            model.UploadResumeCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.ResumeCaption);
            model.PreferredLanguageCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PreferredLanguageCaption);
            model.AttachmentCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.MyProfile.AttachmentCaption);
            model.PhoneNumberValidationMessage = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.MyProfile.PhoneNumberValidationMessage);
            var countries = Utils.GetCountries(Context.Database.GetItem(AccountTemplate.Account.Signup.Fields.CountriesFolder));
            model.Countries = countries;

            model.PreferredLanguages = Utils.GetLanguages(AccountTemplate.Account.Language.LanguageFolder);
            model.TitleCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.MyProfile.TitleCaption);
            model.Titles = Utils.GetLookupItems(Context.Database.GetItem(AccountTemplate.Account.Signup.Fields.UserTitlesFolder));
            var profile = User.Current?.Profile;
            var profileService = new ProfileService();

            if (profile != null)
            {
                model.Image = profileService.GetProfileImage(profile);
                model.FullName = profile.FullName;
                model.Email = profile.Email;
                model.FirstName = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.FirstName));
                model.LastName = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.LastName));
                model.PhoneNumber = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.MobileNumber));
                model.Address = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.AddressOne));
                model.MedicalSpecialty = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.MedicalSpecialty));
                model.MedicalSpecialtyDisplayName = profileService.GetMedicalSpecialty(model.MedicalSpecialty, model.MedicalSpecialties);
                model.PhysicianLicenseNumber = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PhysicianLisenceNumber));
                model.Summary = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Summary));
                var countryCode = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Country));
                model.Country = countryCode;
                model.City = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.City));
                model.ZipCode = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Zipcode));
                model.PreferredLanguage = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PreferredLanguage));
                model.Activity = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Activity));
                var preferredService1 = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PreferredService1));
                var preferredService2 = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PreferredService2));
                var subscribeNewsLetter = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.NewsletterSubscribe));
                model.PreferredService1 = preferredService1.AsBoolean();
                model.PreferredService2 = preferredService2.AsBoolean();
                model.NewsLetterSubscribe = subscribeNewsLetter.AsBoolean();
                string resume = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Resume));
                model.CountryDisplayName = profileService.GetCountryDisplayName(countryCode, countries);
                model.CountryFlag = profileService.GetCountryFlag(countryCode, countries);
                if (!string.IsNullOrEmpty(resume))
                {
                    model.Resume = resume.Split('|').FirstOrDefault();
                    model.AttachmentDisplayName = resume.Split('|').LastOrDefault();
                }
                model.Title = profile.GetCustomProperty(profileService.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Title));

            }
            return model;
        }
        protected virtual string DestinationUrl
        {
            get
            {
                return this.Rendering.DataSourceItem.GetSourceUrl(AccountTemplate.Account.Signup.Fields.DestinationUrl);
            }
        }

        public string GetRedirectUrl(HttpRequestBase request, bool success)
        {
            if (request.Url == null)
            {
                return string.Empty;
            }
            if (!success)
            {
                return this.GetFailUrl(request);
            }
            return !string.IsNullOrEmpty(this.DestinationUrl) ? this.DestinationUrl : request.Url.ToString();
        }

        public void UpdateUser(SignupViewModel model)
        {
            var fullName = Context.Domain.GetFullName(model.Email);
            User user = User.FromName(fullName, true);
            IProfileService iprofileService = new ProfileService();
            iprofileService.SetProfile(user.Profile, new SignupModel
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address = model.Address,
                MedicalSpecialty = model.MedicalSpecialty,
                Country = model.Country,
                PhysicianLicenseNumber = model.PhysicianLicenseNumber,
                Summary = model.Summary,
                PhoneNumber = model.PhoneNumber,
                City = model.City,
                ZipCode = model.ZipCode,
                Activity = model.Activity,
                PreferredLanguage = model.PreferredLanguage,
                PreferredService1 = model.PreferredService1,
                PreferredService2 = model.PreferredService2,
                NewsLetterSubscribe = model.NewsLetterSubscribe,
                Resume = model.Resume,
                Title = model.Title
            });
            user.Profile.Save();
            EmailService.Services.EmailService.AddOrUpdateListMember(model.Email, model.FirstName, model.LastName, model.NewsLetterSubscribe, model.Country, model.PreferredLanguage);
        }

        public bool ChangePassword(string oldPassword, string newPassword)
        {
            MembershipUser user = Membership.GetUser(Context.User.Name);
            return user != null && user.ChangePassword(oldPassword, newPassword);
        }

        protected virtual string GetFailUrl(HttpRequestBase request)
        {
            return Utils.GetFailureUrl(request);
        }

    }
}