﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Account.Repositories
{
    public interface IMiniProfileRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
    }
}
