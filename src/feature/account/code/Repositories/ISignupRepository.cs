﻿
namespace Loreal.Feature.Account.Repositories
{
    using Models;
    using Sitecore.XA.Foundation.Mvc.Repositories.Base;
    using System.Web;

    public interface ISignupRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        bool Exists(string emil);
        void Signup(SignupViewModel model);
        string GetRedirectUrl(HttpRequestBase request, bool success);
    }
}