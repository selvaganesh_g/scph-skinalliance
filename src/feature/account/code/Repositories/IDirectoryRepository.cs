﻿using System.Collections.Generic;
using Loreal.Feature.Account.Models;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.Feature.Account.Repositories
{
    public interface IDirectoryRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        DirectoryViewModel Filter(List<string> countries, List<string> medicalSpecialties);
        SignupViewModel ProfileById(string id);
    }
}