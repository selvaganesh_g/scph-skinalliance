﻿using Loreal.Feature.Account.Extensions;
using Loreal.Feature.Account.Models;
using Sitecore.XA.Foundation.Multisite.Extensions;
using System.Web.Security;

namespace Loreal.Feature.Account.Repositories
{
    using Services;
    using Sitecore;
    using Sitecore.Data.Fields;
    using Sitecore.Diagnostics;
    using Sitecore.Security.Accounts;
    using Sitecore.XA.Foundation.Mvc.Repositories.Base;
    using System.Web;

    public class SignupRepository : ModelRepository, IAbstractRepository<IRenderingModelBase>, ISignupRepository, IModelRepository
    {
        public bool Exists(string email)
        {
            var fullName = Context.Domain.GetFullName(email);

            return User.Exists(fullName);
        }

        public override IRenderingModelBase GetModel()
        {
            SignupViewModel signupViewModel = new SignupViewModel();
            this.FillBaseProperties(signupViewModel);
            if (Rendering.DataSourceItem == null)
            {
                Sitecore.Diagnostics.Log.Error("Datasource is null or empty", this);
                return signupViewModel;
            }
            signupViewModel.Introduction = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.Introduction);
            signupViewModel.SubHeading = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.SubHeading);
            signupViewModel.LoginInfo = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.LoginInfo);
            signupViewModel.PerosnalInfo = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PersonalInfo);
            signupViewModel.FirstNameCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.FirstNameLabel);
            signupViewModel.LastNameCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.LastNameLabel);
            signupViewModel.EmailCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.EmailLabel);
            signupViewModel.PasswordCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PasswordLabel);
            signupViewModel.ConfirmPasswordCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.ConfirmPasswordLabel);
            signupViewModel.SignupButtonCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.SignupButtonLabel);
            signupViewModel.PhoneNumberCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PhoneNumberCaption);
            signupViewModel.AddressCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.AddressCaption);
            signupViewModel.SummaryCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.SummaryCaption);
            signupViewModel.CountryCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.CountryCaption);
            signupViewModel.ZipCodeCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.ZipCodeCaption);
            signupViewModel.CityCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.CityCaption);
            signupViewModel.PhysicianLicenseNumberCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PhysicianLicenseNumberCaption);
            signupViewModel.MedicalSpecialtyCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.MedicalSpecialtyCaption);
            signupViewModel.ConfirmPasswordCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.ConfirmPasswordLabel);
            signupViewModel.FirstNameRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.FirstNameRequired);
            signupViewModel.LastNameRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.LastNameRequired);
            signupViewModel.EmailRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.EmailRequired);
            signupViewModel.InvalidEmail = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.InvalidEmail);
            signupViewModel.PasswordRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PasswordRequired);
            signupViewModel.ConfirmPasswordRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.ConfirmPasswordRequired);
            signupViewModel.CountryRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.CountryRequired);
            signupViewModel.PhysicianLicenseNumberRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PhysicianLicenseNumberRequired);
            signupViewModel.MedicalSpecialtyRequired = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.MedicalSpecialtyRequired);
            signupViewModel.InvalidPassword = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.InvalidPassword);
            signupViewModel.PasswordMismatch = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PasswordMismatch);
            signupViewModel.FailureText = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.FailureText);
            signupViewModel.PasswordHint = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PasswordHintText);
            signupViewModel.UserExistText = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.UserExistsText);
            signupViewModel.Countries = Utils.GetCountries(Sitecore.Context.Database.GetItem(AccountTemplate.Account.Signup.Fields.CountriesFolder));
            signupViewModel.MedicalSpecialties = Utils.GetLookupItems(Sitecore.Context.Database.GetItem(AccountTemplate.Account.Signup.Fields.MedicalSpecialtyFolder));
            signupViewModel.PreferredLanguageCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.PreferredLanguageCaption);
            signupViewModel.NewsLetterSubscribe = false;
            signupViewModel.NewsletterSubscribeCaption = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.NewsletterSubscribeCaption);
            signupViewModel.TermsandConditionText = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.TermsandConditionText);
            signupViewModel.PreferredLanguages = Utils.GetLanguages(AccountTemplate.Account.Language.LanguageFolder);
            signupViewModel.OrText = this.Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.Signup.Fields.OrText);
            CheckboxField chkField = this.Rendering.DataSourceItem.Fields[AccountTemplate.Account.Signup.Fields.PhysicianLicenseNumberMandatoy];
            if (chkField != null)
            {
                signupViewModel.PhysicianNumberIsRequired = chkField.Checked;
            }
            return signupViewModel;
        }

        protected virtual string DestinationUrl
        {
            get
            {
                return this.Rendering.DataSourceItem.GetSourceUrl(AccountTemplate.Account.Signup.Fields.DestinationUrl);
            }
        }

        public string GetRedirectUrl(HttpRequestBase request, bool success)
        {
            if (request.Url == null)
            {
                return string.Empty;
            }
            if (!success)
            {
                return this.GetFailUrl(request);
            }
            return !string.IsNullOrEmpty(this.DestinationUrl) ? this.DestinationUrl : request.Url.ToString();
        }

        public void Signup(SignupViewModel model)
        {
            Assert.ArgumentNotNullOrEmpty(model.Email, nameof(model.Email));
            Assert.ArgumentNotNullOrEmpty(model.ConfirmPassword, nameof(model.ConfirmPassword));

            var fullName = Context.Domain.GetFullName(model.Email);
            Assert.IsNotNullOrEmpty(fullName, "Can't retrieve full userName");
            var user = User.Create(fullName, model.ConfirmPassword);
            user.Profile.Email = model.Email;
            user.Profile.ProfileItemId = AccountTemplate.Account.Profile.Id.ToString();
            user.Profile.Name = model.LastName;
            user.Profile.StartUrl = Membership.GetUser(fullName, false)?.ProviderUserKey.ToString();
            string role = string.Format(@"{0}", Sitecore.Configuration.Settings.GetSetting("UserRole"));
            if (Role.Exists(role))
            {
                user.Roles.Add(Role.FromName(role));
            }
            user.Profile.Save();
            IProfileService iprofileService = new ProfileService();
            iprofileService.SetProfile(user.Profile, new SignupModel
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address = model.Address,
                Email = model.Email,
                MedicalSpecialty = model.MedicalSpecialty,
                Country = model.Country,
                PhysicianLicenseNumber = model.PhysicianLicenseNumber,
                Summary = model.Summary ?? string.Empty,
                Image = iprofileService.SetProfileImage(user.Profile, true),
                City = model.City,
                ZipCode = model.ZipCode,
                PreferredLanguage = model.PreferredLanguage,
                PreferredService1 = false,
                PreferredService2 = false,
                NewsLetterSubscribe = model.NewsLetterSubscribe
            });
            user.Profile.Save();
            //DisableUser(fullName);
            MyNotification.Managers.NotificationManager.Add(new MyNotification.Model.Notification()
            {
                Email = model.Email,
                Live = true,
                UserId = user.Profile.StartUrl,
                NotificationOne = MyNotification.NotificationSendType.Weekly,
                NotificationTwo = MyNotification.NotificationSendType.Immediately,
                NotificationThree = MyNotification.NotificationSendType.Immediately,
                NotificationFour = MyNotification.NotificationSendType.Immediately,
                NotificationFive = MyNotification.NotificationSendType.Weekly
            });
            if (model.NewsLetterSubscribe)
            {
                EmailService.Services.EmailService.AddOrUpdateListMember(model.Email, model.FirstName, model.LastName, model.NewsLetterSubscribe, model.Country, model.PreferredLanguage);
            }

        }
        private void DisableUser(string fullName)
        {
            var m_user = Membership.GetUser(fullName);
            m_user.IsApproved = false;
            Membership.UpdateUser(m_user);
        }

        public bool ChangePassword(string oldPassword, string newPassword)
        {
            MembershipUser user = Membership.GetUser(Context.User.Name);
            return user != null && user.ChangePassword(oldPassword, newPassword);
        }
        protected virtual string GetFailUrl(HttpRequestBase request)
        {
            return Utils.GetFailureUrl(request);
        }

    }
}