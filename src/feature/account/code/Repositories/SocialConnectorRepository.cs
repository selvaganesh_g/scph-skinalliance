﻿using Facebook;
using Loreal.Feature.Account.Extensions;
using Loreal.Feature.Account.Models;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.XA.Foundation.SitecoreExtensions.Extensions;
using Sparkle.LinkedInNET;
using Sparkle.LinkedInNET.OAuth2;
using System;
using System.Collections.Specialized;
using System.Linq;
using Tweetinvi;
using Tweetinvi.Models;

namespace Loreal.Feature.Account.Repositories
{
    public class SocialConnectorRepository : ModelRepository, ISocialConnectorRepository, IModelRepository,
        IAbstractRepository<IRenderingModelBase>
    {

        protected Item SocialConnectorItem
        {
            get
            {
                Item item = ServiceLocator.Current.Resolve<IMultisiteContext>().SettingsItem.Children.FirstOrDefault(x => x.TemplateID == AccountTemplate.Account.SocialConnector.ID);
                if (item != null)
                {
                    return item;
                }
                return null;
            }
        }
        public string GetItemValue(ID field)
        {

            return this.SocialConnectorItem?.GetFieldValue(field);
        }
        public string LinkedInLink(string apiKey, string secretkey, string redirectUri)
        {
            var api = new LinkedInApi(new LinkedInApiConfiguration(apiKey, secretkey));
            var url = api.OAuth2.GetAuthorizationUrl(AuthorizationScope.ReadEmailAddress, Guid.NewGuid().ToString(), redirectUri);
            return url.AbsoluteUri;
        }
        protected string FacebookLink(string apiKey, string secretKey, string redirectUri)
        {

            if (string.IsNullOrEmpty(apiKey) || string.IsNullOrEmpty(secretKey))
            {
                return string.Empty;
            }
            try
            {
                var fb = new FacebookClient();
                var loginUrl = fb.GetLoginUrl(new
                {
                    client_id = apiKey,
                    client_secret = secretKey,
                    redirect_uri = redirectUri,
                    response_type = "code",
                    scope = "email"
                });
                return loginUrl.AbsoluteUri;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, this);
                return string.Empty;
            }
        }
        protected string TwitterLink(string key, string secret, string redirectUri)
        {
            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(secret))
            {
                return string.Empty;
            }

            try
            {
                var authenticationContext = AuthFlow.InitAuthentication(new ConsumerCredentials(key, secret), redirectUri);

                return authenticationContext.AuthorizationURL;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, this);
                return string.Empty;
            }
        }
        public IRenderingModelBase GetSocialModel(string redirctPath)
        {
            SocialConnectorViewModel model = new SocialConnectorViewModel();
            base.FillBaseProperties(model);
            if (Rendering.DataSourceItem == null)
            {
                Sitecore.Diagnostics.Log.Error("Datasource is null or empty", this);
                return model;
            }
            bool isregister = !string.IsNullOrEmpty(this.Rendering.Parameters["register"]);
            model.FacebookAppID = this.SocialConnectorItem?.GetFieldValue(AccountTemplate.Account.SocialConnector.Fields.FacebookAPIKey);
            model.FacebookSecertKey = this.SocialConnectorItem?.GetFieldValue(AccountTemplate.Account.SocialConnector.Fields.FacebookSecretKey);
            model.FacebookText = this.SocialConnectorItem?.GetFieldValue(isregister ? AccountTemplate.Account.SocialConnector.Fields.FacebookRegisterTitle : AccountTemplate.Account.SocialConnector.Fields.FacebookLoginTitle);

            model.TwitterAppID = this.SocialConnectorItem?.GetFieldValue(AccountTemplate.Account.SocialConnector.Fields.TwitterAPIKey);
            model.TwitterSeceretKey = this.SocialConnectorItem?.GetFieldValue(AccountTemplate.Account.SocialConnector.Fields.TwitterSecretKey);
            model.TwitterText = this.SocialConnectorItem?.GetFieldValue(isregister ? AccountTemplate.Account.SocialConnector.Fields.TwitterRegisterTitle : AccountTemplate.Account.SocialConnector.Fields.TwitterLoginTitle);

            model.LinkedInAppID = this.SocialConnectorItem?.GetFieldValue(AccountTemplate.Account.SocialConnector.Fields.LinkedInAPIKey);
            model.LinkedInSeceretKey = this.SocialConnectorItem?.GetFieldValue(AccountTemplate.Account.SocialConnector.Fields.LinkedInSecretKey);
            model.LinkedInText = this.SocialConnectorItem?.GetFieldValue(isregister ? AccountTemplate.Account.SocialConnector.Fields.LinkedInRegisterTitle : AccountTemplate.Account.SocialConnector.Fields.LinkedInLoginTitle);

            model.SeparatorText = this.SocialConnectorItem?.GetFieldValue(AccountTemplate.Account.SocialConnector.Fields.SeparatedText);
            model.FacebookLink = this.FacebookLink(model.FacebookAppID, model.FacebookSecertKey, redirctPath);
            model.TwitterLink = this.TwitterLink(model.TwitterAppID, model.TwitterSeceretKey, redirctPath);
            model.LinkedInLink = this.LinkedInLink(model.LinkedInAppID, model.LinkedInSeceretKey, redirctPath);
            LinkField linkField = this.SocialConnectorItem?.Fields[AccountTemplate.Account.SocialConnector.Fields.SocialRegisterURL];
            if (linkField != null)
            {
                model.RegisterURL = linkField.GetFriendlyUrl();
            }

            return model;

        }
    }
}