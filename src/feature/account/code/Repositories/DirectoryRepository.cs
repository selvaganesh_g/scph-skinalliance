﻿using Loreal.Feature.Account.Models;
using Loreal.Feature.Account.Services;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Loreal.Feature.Account.Repositories
{
    public class DirectoryRepository : ModelRepository, IDirectoryRepository, IModelRepository,
      IAbstractRepository<IRenderingModelBase>
    {
        private readonly ProfileService _profileService = new ProfileService();

        public override IRenderingModelBase GetModel()
        {

            DirectoryViewModel viewModel = new DirectoryViewModel();
            base.FillBaseProperties(viewModel);
            viewModel.List = _profileService.GetUserDirectory(new List<string> { }, new List<string> { }, false);
            return viewModel;
        }

        public DirectoryViewModel Filter(List<string> countries, List<string> medicalSpecialties)
        {
            return new DirectoryViewModel { List = _profileService.GetUserDirectory(countries, medicalSpecialties, true), };
        }

        public SignupViewModel ProfileById(string id)
        {
            return _profileService.GetProfileById(id);
        }

    }
}
