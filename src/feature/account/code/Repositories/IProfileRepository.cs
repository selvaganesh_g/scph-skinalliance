﻿using Loreal.Feature.Account.Models;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Web;

namespace Loreal.Feature.Account.Repositories
{
    public interface IProfileRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        void UpdateUser(SignupViewModel model);
        string GetRedirectUrl(HttpRequestBase request, bool success);
        bool ChangePassword(string oldPassword, string newPassword);
    }
}
