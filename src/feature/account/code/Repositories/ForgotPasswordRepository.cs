﻿using System;
using System.Web;
using Loreal.Feature.Account.Models;
using Sitecore.Data.Fields;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.XA.Foundation.Multisite.Extensions;
using Sitecore;
using Sitecore.Security.Accounts;

namespace Loreal.Feature.Account.Repositories
{
    public class ForgotPasswordRepository : ModelRepository, IForgotPasswordRepository, IModelRepository,
      IAbstractRepository<IRenderingModelBase>
    {
        protected virtual string DestinationUrl => Rendering.DataSourceItem.GetSourceUrl(AccountTemplate.Account.ForgotPassword.Fields.DestinationUrl);
        public bool Exists(string email)
        {
            var fullName = Context.Domain.GetFullName(email);
            return User.Exists(fullName);
        }

        public override IRenderingModelBase GetModel()
        {
            ForgotPasswordViewModel model = new ForgotPasswordViewModel();
            base.FillBaseProperties(model);
            if (Rendering.DataSourceItem == null)
            {
                Sitecore.Diagnostics.Log.Error("Datasource is null or empty", this);
                return model;
            }
            var item = base.Rendering.DataSourceItem;
            model.DataSource = item.ID.ToString();
            model.Title = item.Fields[Account.AccountTemplate.Account.ForgotPassword.Fields.Title]?.Value;
            model.Placeholder = item.Fields[Account.AccountTemplate.Account.ForgotPassword.Fields.PlaceholderText]?.Value;
            model.ButtonLabel = item.Fields[Account.AccountTemplate.Account.ForgotPassword.Fields.SendButtonLabel]?.Value;
            model.BackButtonLabel = item.Fields[Account.AccountTemplate.Account.ForgotPassword.Fields.BackButtonLabel]?.Value;
            model.ErrorMessage = item.Fields[Account.AccountTemplate.Account.ForgotPassword.Fields.ErrorMessage]?.Value;
            model.SuccessMessage = item.Fields[Account.AccountTemplate.Account.ForgotPassword.Fields.SuccessMessage]?.Value;
            model.EmailIdRequired = item.Fields[Account.AccountTemplate.Account.ForgotPassword.Fields.EmailIdRequired]?.Value;
            LinkField link = item.Fields[Account.AccountTemplate.Account.ForgotPassword.Fields.DestinationUrl];
            if (link != null)
            {
                model.DestinationUrl = link.GetFriendlyUrl();
            }

            return model;
        }

        public string GetPassword(string email, out string userName)
        {
            var fullName = Context.Domain.GetFullName(email);
            var user = System.Web.Security.Membership.GetUser(Context.Domain.GetFullName(email));
            userName = User.FromName(fullName, false)?.Profile?.FullName;
            if (user.IsLockedOut)
            {
                user.UnlockUser();
            }
            return user.ResetPassword();
        }

        public string GetRedirectUrl(HttpRequestBase request, bool success)
        {
            if (request.Url == null)
                return string.Empty;
            if (!success)
                return GetFailUrl(request);
            return !string.IsNullOrEmpty(DestinationUrl) ? DestinationUrl : Utils.GetSuccessUrl(request);
        }
        protected virtual string GetFailUrl(HttpRequestBase request)
        {
            return Utils.GetFailureUrl(request);
        }
    }
}
