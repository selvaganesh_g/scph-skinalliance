﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Web;

namespace Loreal.Feature.Account.Repositories
{
    public interface IForgotPasswordRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        bool Exists(string email);
        string GetPassword(string email, out string userName);
        string GetRedirectUrl(HttpRequestBase request, bool success);
    }
}
