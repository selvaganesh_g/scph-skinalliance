﻿using System.Web;
using Loreal.Feature.Account.Extensions;
using Loreal.Feature.Account.Models;
using Sitecore;
using Sitecore.Security.Authentication;
using Sitecore.XA.Foundation.Multisite.Extensions;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System;

namespace Loreal.Feature.Account.Repositories
{
    public class SignInRepository : ModelRepository, ISignInRepository, IModelRepository,
        IAbstractRepository<IRenderingModelBase>
    {
        protected virtual string DestinationUrl => Rendering.DataSourceItem.GetSourceUrl(AccountTemplate.Account.SignIn.Fields
            .DestinationUrl);

        public string GetRedirectUrl(HttpRequestBase request, bool success)
        {
            if (request.Url == null)
                return string.Empty;
            if (!success)
                return GetFailUrl(request);
            return !string.IsNullOrEmpty(DestinationUrl) ? DestinationUrl : request.Url.ToString();
        }
        public bool Login(string login, string password)
        {
            return this.Login(login, password, false);
        }
        public bool Login(string login, string password, bool rememberMe)
        {
            var m_user = Membership.GetUser(login, true);

            if (m_user != null)
            {
                if (m_user.IsLockedOut && m_user.IsApproved && Math.Abs(m_user.LastLockoutDate.ToUniversalTime().Subtract(DateTime.UtcNow).TotalMinutes) > 10)
                {
                    m_user.UnlockUser();
                }
            }
            return AuthenticationManager.Login(login, password, rememberMe);
        }

        public override IRenderingModelBase GetModel()
        {
            var signInRenderingModel = new SignInViewModel();
            FillBaseProperties(signInRenderingModel);
            if (Rendering.DataSourceItem == null)
            {
                Sitecore.Diagnostics.Log.Error("Datasource is null or empty", this);
                return signInRenderingModel;
            }
            signInRenderingModel.Visible = !PageContext.CurrentUser.IsAuthenticated || !Context.PageMode.IsNormal;
            signInRenderingModel.Introduction = new HtmlString(Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.Introduction));
            signInRenderingModel.SubHeading = new HtmlString(Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.SubHeading));
            signInRenderingModel.DisplayRememberMe = MainUtil.GetBool(Rendering.DataSourceItem[AccountTemplate.Account.SignIn.Fields.DisplayRememberMe], false);
            signInRenderingModel.UsernameLabel = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.UsernameLabel);
            signInRenderingModel.PasswordLabel = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.PasswordLabel);
            signInRenderingModel.RememberMeLabel = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.RememberMeLabel);
            signInRenderingModel.UsernameRequired = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.UsernameRequired);
            signInRenderingModel.InvalidUsername = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.InvalidUsername);
            signInRenderingModel.PasswordRequired = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.PasswordRequired);
            signInRenderingModel.LoginButtonLabel = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.LoginButtonCaption);
            signInRenderingModel.FailureText = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.FailureText);
            signInRenderingModel.RegisterUrlPrefixText = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.RegisterUrlPrefixText);
            signInRenderingModel.ForgotPasswordUrl = new HtmlString(FieldRenderer.Render(Rendering.DataSourceItem, Rendering.DataSourceItem.Fields[AccountTemplate.Account.SignIn.Fields.ForgotPasswordUrl]?.Name).Replace("</a>", " <i class=\"fa fa-angle-right\"></i></a>"));
            signInRenderingModel.RegistrationUrl = new HtmlString(FieldRenderer.Render(Rendering.DataSourceItem, Rendering.DataSourceItem.Fields[AccountTemplate.Account.SignIn.Fields.RegistrationUrl]?.Name).Replace("</a>", " <i class=\"fa fa-angle-right\"></i></a>"));
            signInRenderingModel.OrText = Rendering.DataSourceItem.GetFieldValue(AccountTemplate.Account.SignIn.Fields.OrText);
            return signInRenderingModel;
        }

        public DiscoverDermocosmeticsScienceModel GetDDSModel()
        {
            var model = new DiscoverDermocosmeticsScienceModel();

            var dataSource = Rendering.DataSourceItem;
            if (dataSource == null)
            {
                return model;
            }
            model.Title = dataSource.GetFieldValue(DiscoverDermocosmeticsScience.Fields.Title);
            model.Description = new HtmlString(FieldRenderer.Render(dataSource, dataSource.Fields[DiscoverDermocosmeticsScience.Fields.Description]?.Name));

            model.QuickLinks = new List<QuickLinkModel>();

            foreach (var item in dataSource.Children.Where(x => x.TemplateID == DiscoverDermocosmeticsScience.QuickLink.ID))
            {
                model.QuickLinks.Add(new QuickLinkModel()
                {
                    Title = FieldRenderer.Render(item, item.Fields[DiscoverDermocosmeticsScience.QuickLink.Fields.Title]?.Name),
                    Description = FieldRenderer.Render(item, item.Fields[DiscoverDermocosmeticsScience.QuickLink.Fields.Description]?.Name),
                    Image = Utils.GetImageUrl(item, DiscoverDermocosmeticsScience.QuickLink.Fields.Image),
                    CTA = FieldRenderer.Render(item, item.Fields[DiscoverDermocosmeticsScience.QuickLink.Fields.CTA]?.Name)
                });
            }
            return model;
        }
        protected virtual string GetFailUrl(HttpRequestBase request)
        {
            return Utils.GetFailureUrl(request);
        }
    }
}