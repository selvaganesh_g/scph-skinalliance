﻿using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Text;
using Sitecore.Web.UI.Sheer;
using System;
using System.Web;

namespace Loreal.Feature.Account.Commands
{
    public class ImportUserCommand : Command
    {
        public override void Execute(CommandContext context)
        {
            if (context.Parameters.Count == 0)
            {
                return;
            }
            else
            {
                context.Parameters.Add("pageurl", "/importuser");
                Sitecore.Context.ClientPage.Start(this, "Run", context.Parameters);
            }
        }

        protected void Run(Sitecore.Web.UI.Sheer.ClientPipelineArgs args)
        {
            string pageUrl = args.Parameters["pageurl"];
            Sitecore.Text.UrlString _url = new Sitecore.Text.UrlString(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + pageUrl);
            Sitecore.Context.ClientPage.ClientResponse.ShowModalDialog(_url.ToString(),"800","500");
        }
    }
}