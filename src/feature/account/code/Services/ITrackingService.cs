﻿using Loreal.Feature.Account.Models;
using Sitecore.Analytics.Tracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loreal.Feature.Account.Services
{
    public interface ITrackingService
    {
        void IdentifyContact(string userName, SignupViewModel model);
        void SetContactData(Contact identifiedUser, SignupViewModel model);
    }
}
