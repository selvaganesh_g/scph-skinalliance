﻿using Sitecore.Analytics.Model;
using System.Linq;
using Sitecore.Analytics.Tracking;
using Sitecore.Analytics.Model.Entities;
using Loreal.Feature.Account.Models;

namespace Loreal.Feature.Account.Services
{
    public class TrackingService : ITrackingService
    {
        public void IdentifyContact(string userName, SignupViewModel model)
        {
            if (Sitecore.Analytics.Tracker.Current == null)
            {
                Sitecore.Analytics.Tracker.StartTracking();
            }
            var tracker = Sitecore.Analytics.Tracker.Current;
            if (tracker == null || !tracker.IsActive || tracker.Contact == null)
            {
                return;
            }
            var identifiers = tracker.Contact.Identifiers;
            if (identifiers.IdentificationLevel == ContactIdentificationLevel.Anonymous || identifiers.IdentificationLevel == ContactIdentificationLevel.None)
            {
                tracker.Session.Identify(userName);
            }
            SetContactData(tracker.Contact, model);

        }
        public void SetContactData(Contact identifiedUser, SignupViewModel model)
        {

            if (identifiedUser.Identifiers.IdentificationLevel == ContactIdentificationLevel.Known && model != null)
            {
                var emailFacet = identifiedUser.GetFacet<IContactEmailAddresses>("Emails");
                var addressFacet = identifiedUser.GetFacet<IContactAddresses>("Addresses");
                var personalFacet = identifiedUser.GetFacet<IContactPersonalInfo>("Personal");
                var phoneFacet = identifiedUser.GetFacet<IContactPhoneNumbers>("Phone Numbers");
                IEmailAddress email = emailFacet.Entries.Contains("Work Email") ? emailFacet.Entries["Work Email"] : emailFacet.Entries.Create("Work Email");
                IAddress address = addressFacet.Entries.Contains("Work Address") ? addressFacet.Entries["Work Address"] : addressFacet.Entries.Create("Work Address");
                IPhoneNumber workPhone = phoneFacet.Entries.Contains("Work Phone") ? phoneFacet.Entries["Work Phone"] : phoneFacet.Entries.Create("Work Phone");
                email.SmtpAddress = model.Email;
                emailFacet.Preferred = "Work Email";
                address.StreetLine1 = model.Address;
                address.City = model.City;
                address.Country = model.Country;
                address.PostalCode = model.ZipCode;
                personalFacet.FirstName = model.FirstName;
                personalFacet.Surname = model.LastName;
                personalFacet.JobTitle = model.MedicalSpecialty;
                personalFacet.Title = model.Title;
                workPhone.CountryCode = model.Country;
                Sitecore.Diagnostics.Log.Info(string.Format("Successfully synced the known user: {0} {1}", model.FirstName, model.LastName), this);
            }
        }
    }
}
