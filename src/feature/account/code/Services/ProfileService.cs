﻿using Loreal.Feature.Account.Models;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Resources.Media;
using Sitecore.Security;
using Sitecore.Security.AccessControl;
using Sitecore.Security.Accounts;
using Sitecore.Security.Domains;
using Sitecore.SecurityModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Loreal.Feature.Account.Services
{
    public class ProfileService : IProfileService
    {
        protected Item profile;
        protected virtual Item Profile => profile ?? (profile = ProfielItem());
        protected virtual string Image => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Image);
        protected virtual string FirstName => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.FirstName);
        protected virtual string LastName => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.LastName);
        protected virtual string MobileNumber => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.MobileNumber);
        protected virtual string HomeNumber => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.HomeNumber);
        protected virtual string AddressOne => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.AddressOne);
        protected virtual string AddressTwo => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.AddressTwo);
        protected virtual string City => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.City);
        protected virtual string Country => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Country);
        protected virtual string Zipcode => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Zipcode);
        protected virtual string DoB => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.DoB);
        protected virtual string Portrait => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Portrait);
        protected virtual string MedicalSpecialty => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.MedicalSpecialty);
        protected virtual string PhysicianLicenseNumber => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PhysicianLisenceNumber);
        protected virtual string Summary => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Summary);
        protected virtual string IsAccepted => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.IsAccepted);
        protected virtual string PreferredLanguage => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PreferredLanguage);
        protected virtual string Activity => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Activity);
        protected virtual string PreferredService1 => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PreferredService1);
        protected virtual string PreferredService2 => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PreferredService2);
        protected virtual string NewsletterSubscribe => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.NewsletterSubscribe);
        protected virtual string Resume => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Resume);

        protected virtual string Title => GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Title);

        protected void SetCustomProfile(UserProfile userProfile, IDictionary<string, string> properties)
        {
            Assert.ArgumentNotNull(userProfile, nameof(userProfile));
            Assert.ArgumentNotNull(properties, nameof(properties));

            foreach (var property in properties)
                if (property.Value == null)
                    userProfile[property.Key] = string.Empty;
                else
                    userProfile[property.Key] = property.Value;

            userProfile.Save();
        }
        private Item ProfielItem()
        {
            using (new SecurityDisabler())
            {
                var database = Database.GetDatabase(Settings.ProfileItemDatabase);
                var targetItem = database.GetItem(AccountTemplate.Account.Profile.Id);
                return targetItem;
            }
        }

        public string GetMediaPath(string path)
        {
            return GetMediaPath(path, false);
        }
        private string GetMediaPath(string value, bool isThumbNail = false)
        {
            var mediaPath = "";
            using (new SecurityDisabler())
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var database = Database.GetDatabase(Settings.ProfileItemDatabase);
                    var mediaItem = database.GetItem(value);
                    if (mediaItem != null)
                    {

                        if (isThumbNail)
                        {
                            MediaUrlOptions mediaOption = new MediaUrlOptions();
                            mediaOption.Height = 60;
                            mediaOption.Width = 60;
                            mediaPath = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(mediaItem, mediaOption));
                            mediaPath = string.Format("{0}&sc_content={1}&dt={2}", mediaPath, database.Name, mediaItem.Statistics.Updated.ToString("yyyyMMddHHmmss"));
                        }
                        else
                        {
                            mediaPath = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(mediaItem));
                            mediaPath = string.Format("{0}?sc_content={1}&dt={2}", mediaPath, database.Name, mediaItem.Statistics.Updated.ToString("yyyyMMddHHmmss"));
                        }

                    }
                }
            }

            return mediaPath;
        }
        public bool GetAcceptedValue(UserProfile profile)
        {
            bool result = false;
            using (new SecurityDisabler())
            {
                if (profile != null)

                    Boolean.TryParse(profile[IsAccepted], out result);

            }
            return result;
        }
        public void SetAcceptedValue(UserProfile profile, bool value)
        {
            using (new SecurityDisabler())
            {
                if (profile != null)
                {
                    profile[IsAccepted] = value.ToString();
                    profile.Save();
                }
            }

        }
        public virtual void SetProfile(UserProfile userProfile, SignupModel model)
        {
            var properties = new Dictionary<string, string>
            {
                [nameof(userProfile.Name)] = model.FirstName,
                [nameof(userProfile.FullName)] = $"{model.FirstName} {model.LastName}".Trim(),
                [FirstName] = model.FirstName,
                [LastName] = model.LastName,
                [MobileNumber] = model.PhoneNumber,
                [AddressOne] = model.Address,
                [Country] = model.Country,
                [PhysicianLicenseNumber] = model.PhysicianLicenseNumber,
                [MedicalSpecialty] = model.MedicalSpecialty,
                [Summary] = model.Summary,
                [City] = model.City,
                [Zipcode] = model.ZipCode,
                [Activity] = model.Activity,
                [PreferredLanguage] = model.PreferredLanguage,
                [PreferredService1] = System.Convert.ToString((model.PreferredService1 ? "1" : "0")),
                [PreferredService2] = System.Convert.ToString(model.PreferredService2 ? "1" : "0"),
                [NewsletterSubscribe] = System.Convert.ToString(model.NewsLetterSubscribe ? "1" : "0"),
                [Title] = model.Title
            };

            if (!string.IsNullOrEmpty(model.Resume))
                properties.Add(Resume, model.Resume);

            if (!string.IsNullOrEmpty(model.Image))
                properties.Add(Image, model.Image);

            SetCustomProfile(userProfile, properties);
            userProfile.Save();
            userProfile.Icon = GetProfileImage(userProfile, true);
            userProfile.Save();
        }
        public string GetPortraitImageUrl(Sitecore.Security.Accounts.User user)
        {
            if (user == null
                || user.Profile == null
                || string.IsNullOrWhiteSpace(user.Profile.Portrait))
            {
                return null;
            }

            string imageUrl = string.Empty;
            string imageTypesSetting = Sitecore.Configuration.Settings.GetSetting("ImageTypes", "|jpg|img|gif|");
            string[] imageExtensions = imageTypesSetting.Split('|');

            string portraitValue = user.Profile.Portrait;

            foreach (string imageExtension in imageExtensions)
            {
                if (!string.IsNullOrWhiteSpace(imageExtension) && portraitValue.EndsWith(imageExtension))
                {
                    portraitValue = portraitValue.Replace(imageExtension, "ashx");
                }
            }

            //string portraitsFolder = SettingManager.GetPortraitImageFolder();

            imageUrl = string.Concat("/temp/IconCache/People/Profile_Images", " / ", portraitValue);

            return imageUrl;
        }
        public virtual string GetProfileImage(UserProfile userProfile, bool isThumbnail = false)
        {
            using (new SecurityDisabler())
            {
                if (userProfile != null)
                    return GetMediaPath(userProfile[Image], isThumbnail);
            }
            return string.Empty;
        }
        public virtual string GetProfileImageMediaPathByName(string imageName)
        {
            var mediaPath = "";
            using (new SecurityDisabler())
            {
                if (!string.IsNullOrEmpty(imageName))
                {
                    var database = Database.GetDatabase(Settings.ProfileItemDatabase);
                    var item = database.SelectSingleItem(string.Format("/sitecore/media library/LRP//*[@@name='{0}']", imageName));
                    if (item != null)
                    {
                        return item.Paths.MediaPath;
                    }
                }
            }
            return mediaPath;
        }
        public virtual string GetProfileImageByName(string imageName)
        {
            var mediaPath = "";

            using (new SecurityDisabler())
            {
                if (!string.IsNullOrEmpty(imageName))
                {
                    var database = Database.GetDatabase(Settings.ProfileItemDatabase);
                    var item = database.SelectSingleItem(string.Format("/sitecore/media library/LRP//*[@@name='{0}']", imageName));
                    if (item != null)
                    {
                        mediaPath = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(item));
                        mediaPath = string.Format("{0}?sc_content={1}&dt={2}", mediaPath, database.Name, item.Statistics.Updated.ToString("yyyyMMddHHmmss"));
                    }
                }
            }
            return mediaPath;
        }


        public string UploadResume(UserProfile userProfile, bool newProfile, HttpPostedFileBase file)
        {
            string mediaPath = string.Empty;
            using (new SecurityDisabler())
            {
                var database = Database.GetDatabase(Settings.ProfileItemDatabase);
                var stream = file.InputStream;
                if (stream != null)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        stream.CopyTo(memoryStream);
                        TemplateItem template = database.GetTemplate(AccountTemplate.Account.Profile.MediaFolterID);
                        Item parent = database.Items[AccountTemplate.Account.Profile.MediaRoot];
                        var rootItem = parent.Children.SingleOrDefault(x => x.Name.Equals(AccountTemplate.Account.Profile.DocumentRootItemName));
                        if (rootItem == null)
                        {
                            rootItem = parent.Add(AccountTemplate.Account.Profile.DocumentRootItemName, template);
                            Role myRole = Role.FromName(string.Format(@"{0}", Settings.GetSetting("UserRole")));
                            AccessRuleCollection accessRules = rootItem.Security.GetAccessRules();
                            accessRules.Helper.AddAccessPermission(myRole, AccessRight.ItemRead, PropagationType.Any, AccessPermission.Allow);
                            rootItem.Editing.BeginEdit();
                            rootItem.Security.SetAccessRules(accessRules);
                            rootItem.Editing.EndEdit();
                        }

                        var fileNameWithoutExt = ItemUtil.ProposeValidItemName(userProfile.Email.Split('@').FirstOrDefault());
                        var item = database.GetItem(rootItem.Paths.FullPath + "/" + fileNameWithoutExt);
                        if (item != null)
                        {
                            item.Delete();
                        }
                        var mediaCreator = new MediaCreator();
                        var options = new MediaCreatorOptions
                        {
                            Versioned = false,
                            IncludeExtensionInItemName = true,
                            Database = database,
                            Destination = rootItem.Paths.FullPath + "/" + fileNameWithoutExt
                        };
                        var mediaItem = mediaCreator.CreateFromStream(memoryStream, string.Concat(fileNameWithoutExt, Path.GetExtension(file.FileName)), options);
                        mediaItem.Editing.BeginEdit();
                        mediaItem.Appearance.DisplayName = Path.GetFileNameWithoutExtension(file.FileName);
                        if (mediaItem != null)
                        {
                            mediaPath = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(mediaItem));
                            mediaPath = string.Format("{0}?sc_content={1}&dt={2}|{3}", mediaPath, database.Name, mediaItem.Statistics.Updated.ToString("yyyyMMddHHmmss"), mediaItem.Appearance.DisplayName);
                        }
                        mediaItem.Editing.EndEdit();
                        mediaItem.Reload();
                    }
                }
            }
            return mediaPath;
        }
        public string SetProfileImage(UserProfile userProfile, bool newProfile)
        {
            return SetProfileImage(userProfile, newProfile, null);
        }
        public string SetProfileImage(UserProfile userProfile, bool newProfile, Stream file)
        {
            using (new SecurityDisabler())
            {
                var database = Database.GetDatabase(Settings.ProfileItemDatabase);
                if (newProfile)
                {
                    var profileImage = CreateMediaItem(database, ItemUtil.ProposeValidItemName(userProfile.Email));
                    profileImage.Editing.BeginEdit();
                    profileImage.Appearance.DisplayName = userProfile.Name;
                    profileImage.Editing.EndEdit();
                    return profileImage.Paths.FullPath;
                }
                var mediaItem = database.GetItem(userProfile[Image]);
                if (mediaItem == null) mediaItem = CreateMediaItem(database, ItemUtil.ProposeValidItemName(userProfile.Email));
                var media = MediaManager.GetMedia(mediaItem);
                media.SetStream(file, media.Extension);
                mediaItem.Reload();
                return string.Empty;
            }
        }
        private Item CreateMediaItem(Database database, string name)
        {
            var defaultProfileImageItem = database.GetItem(AccountTemplate.Account.Profile.ImageId);
            return defaultProfileImageItem.CopyTo(database.GetItem(AccountTemplate.Account.Profile.MediaRoot), name);
        }
        public string GetUserProfileFieldName(ID fieldId)
        {
            using (new SecurityDisabler())
            {
                return Profile.Database.GetItem(fieldId).Name;
            }
        }
        public string GetMedicalSpecialty(string key, List<LookupItem> medicalSpecialties)
        {
            if (medicalSpecialties == null || medicalSpecialties.Count <= 0)
            {
                medicalSpecialties = Utils.GetLookupItems(Context.Database.GetItem(AccountTemplate.Account.Signup.Fields.MedicalSpecialtyFolder));
            }
            return medicalSpecialties.Any() ? medicalSpecialties.SingleOrDefault(x => x.Key == key)?.Phrase : key;
        }
        public string GetCountryFlag(string countryCode, List<CountryModel> countries)
        {
            if (countries == null || countries.Count <= 0)
            {
                countries = Utils.GetCountries(Context.Database.GetItem(AccountTemplate.Account.Signup.Fields.CountriesFolder));
            }
            return countries.Any() ? countries.SingleOrDefault(x => x.Code == countryCode)?.Image : string.Empty;
        }
        public string GetCountryDisplayName(string countryCode, List<CountryModel> countries)
        {
            if (countries == null || countries.Count <= 0)
            {
                countries = Utils.GetCountries(Context.Database.GetItem(AccountTemplate.Account.Signup.Fields.CountriesFolder));
            }
            return countries.Any() ? countries.SingleOrDefault(x => x.Code == countryCode)?.Name : string.Empty;
        }
        public List<DirectoryListModel> GetUserDirectory(List<string> countries, List<string> medicalSpecialties, bool isFiltering)
        {
            Role myRole = Role.FromName(string.Format(@"{0}", Settings.GetSetting("UserRole")));

            var users = Domain.GetDomain(Context.Domain.Name ?? Context.Site.Domain.Name)?.GetUsers();
            if (Role.Exists(myRole.Name))
            {
                users = users.Where(x => x.IsInRole(myRole));
            }

            var profiles = users.Select(x => x.Profile).Where(x => !String.IsNullOrEmpty(x.FullName) && x.UserName != Sitecore.Context.User.Name);

            if (isFiltering)
            {
                profiles = profiles.Where(p => (countries.Contains(p.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Country))))
                        || medicalSpecialties.Contains(p.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.MedicalSpecialty))));
            }

            return this.GroupUserProfiles(profiles).Select(p => new DirectoryListModel
            {
                Key = p.Key,
                Profiles = p.Profiles.Where(x => x.PreferredService2.Equals(true)).ToList()
            }).ToList();
        }

        private List<DirectoryListModel> GroupUserProfiles(IEnumerable<UserProfile> userProfiles)
        {
            var directoryList = new List<DirectoryListModel>();
            if (userProfiles == null) return directoryList;
            var groups = userProfiles.GroupBy(name => name.FullName.ToUpperInvariant().First()).OrderBy(g => g.Key);

            foreach (var g in groups)
            {
                directoryList.Add(new DirectoryListModel()
                {
                    Key = g.Key.ToString(),
                    Profiles = g.Select(x => new DirectoryModel()
                    {
                        Name = x.FullName,
                        Image = GetProfileImage(x),
                        Link = x.StartUrl,
                        FirstName = x.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.FirstName)),
                        CountryCode = x.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Country)),
                        SpecialtyCode = x.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.MedicalSpecialty)),
                        PreferredService2 = x.GetCustomProperty(GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PreferredService2)).AsBoolean(),
                        Title = x.GetCustomProperty(GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Title)),
                    }).OrderBy(x => x.FirstName).ToList(),
                });
            }
            return directoryList;
        }
        public SignupViewModel GetProfileById(string id)
        {

            SignupViewModel signupViewModel = new SignupViewModel();
            if (id != null)
            {

                var currentProfile = Context.Domain.GetUsers().Where(x => x.Profile.StartUrl == id).FirstOrDefault()?.Profile;
                if (currentProfile == null) { return signupViewModel; }
                signupViewModel.Image = this.GetProfileImage(currentProfile);
                signupViewModel.FullName = currentProfile.FullName;
                signupViewModel.Email = currentProfile.Email;
                signupViewModel.Title = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Title));
                signupViewModel.FirstName = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.FirstName));
                signupViewModel.LastName = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.LastName));
                signupViewModel.PhoneNumber = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.MobileNumber));
                signupViewModel.Address = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.AddressOne));
                signupViewModel.MedicalSpecialty = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.MedicalSpecialty));
                signupViewModel.MedicalSpecialtyDisplayName = this.GetMedicalSpecialty(signupViewModel.MedicalSpecialty, new List<LookupItem>());
                signupViewModel.PhysicianLicenseNumber = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PhysicianLisenceNumber));
                signupViewModel.Summary = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Summary));
                signupViewModel.Activity = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Activity));
                var resume = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Resume));
                var countryCode = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Country));
                signupViewModel.Country = countryCode;
                signupViewModel.CountryDisplayName = this.GetCountryDisplayName(countryCode, new List<CountryModel>());
                signupViewModel.CountryFlag = this.GetCountryFlag(countryCode, new List<CountryModel>());
                signupViewModel.City = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.City));
                signupViewModel.ZipCode = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.Zipcode));
                signupViewModel.PreferredLanguage = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PreferredLanguage));
                signupViewModel.PreferredService1 = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PreferredService1)).AsBoolean();
                signupViewModel.PreferredService2 = currentProfile.GetCustomProperty(this.GetUserProfileFieldName(AccountTemplate.Account.Profile.Fields.PreferredService2)).AsBoolean();
                if (!string.IsNullOrEmpty(resume))
                {
                    signupViewModel.Resume = resume.Split('|').FirstOrDefault();
                    signupViewModel.AttachmentDisplayName = resume.Split('|').LastOrDefault();
                }
            }
            return signupViewModel;

        }
    }
}
