﻿using Loreal.Feature.Account.Models;
using Sitecore.Data.Items;
using Sitecore.Security;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace Loreal.Feature.Account.Services
{
    public interface IProfileService
    {
        void SetProfile(UserProfile userProfile, SignupModel model);
        string SetProfileImage(UserProfile userProfile, bool newProfile, Stream file = null);
        string GetProfileImage(UserProfile userProfile, bool isThumbnail = false);
        SignupViewModel GetProfileById(string id);
        bool GetAcceptedValue(UserProfile profile);
        void SetAcceptedValue(UserProfile profile, bool value);
        List<DirectoryListModel> GetUserDirectory(List<string> countries, List<string> medicalSpecialties, bool isFiltering);
        string UploadResume(UserProfile userProfile, bool newProfile, HttpPostedFileBase file);
        string GetMediaPath(string path);
        string GetPortraitImageUrl(Sitecore.Security.Accounts.User user);
        string GetProfileImageByName(string imageName);
    }
}
