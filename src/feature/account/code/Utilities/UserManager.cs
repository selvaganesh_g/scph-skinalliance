﻿using Loreal.Feature.Account.Services;
using Newtonsoft.Json;
using OfficeOpenXml;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Security.Accounts;
using Sitecore.SecurityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;

namespace Loreal.Feature.Account.Utilities
{
    public struct UserEntityConstants
    {
        public static readonly ID ProfileItemId = new ID("{772F2290-5FA4-4E31-8A4B-985CAB04929E}");
    }

    public class UserEntity
    {
        public bool NewsLetterSubscribe { get; set; }
        public bool PreferredService1 { get; set; }
        public bool PreferredService2 { get; set; }
        public bool PreferredService3 { get; set; }
        public bool Status { get; set; }
        public string Activity { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Comment { get; set; }
        public string Country { get; set; }
        public string EmailAddress { get; set; }
        public string ErrorLogs { get; set; }
        public string FirstName { get; set; }
        public string Image { get; set; }
        public string ImmatriculationLicenseNumber { get; set; }
        public string LastName { get; set; }
        public string LinkedInProfileUrl { get; set; }
        public string MedicalSpecialty { get; set; }
        public string MobileNumber { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string PhysicalLicenseNumber { get; set; }
        public string PostalCode { get; set; }
        public string PreferredLanguage { get; set; }
        public string Resume { get; set; }
        public string Summary { get; set; }
        public string Title { get; set; }
    }

    public static class UserManager
    {
        public static void SaveUser(UserEntity cUser, User scUser)
        {
            string userRole = string.Format(@"{0}", Settings.GetSetting("UserRole"));

            scUser.Profile.ProfileItemId = UserEntityConstants.ProfileItemId.ToString();
            scUser.Profile.Name = $"{ cUser.FirstName} {cUser.LastName}".Trim();
            scUser.Profile.Email = cUser.EmailAddress;
            //  scUser.Profile.Comment = (userEntity.Comment != null ? userEntity.Comment : string.Empty);
            scUser.Profile.StartUrl = Membership.GetUser(Sitecore.Context.Domain.GetFullName(cUser.EmailAddress), false)?.ProviderUserKey.ToString();
            if (Role.Exists(userRole) && !scUser.IsInRole(userRole))
            {
                scUser.Roles.Add(Role.FromName(userRole));
            }
            scUser.Profile.Save();
            var pService = new ProfileService();

            string[] ghostProfile = Settings.GetSetting("GhostProfile").Split(',');
            bool isGhost = true;
            foreach (var gh in ghostProfile)
            {
                if (cUser.EmailAddress.Contains(gh)) isGhost = false; break;
            }
            pService.SetProfile(scUser.Profile, new Models.SignupModel
            {
                Address = cUser.Address,
                City = cUser.City,
                Country = cUser.Country.ToLower(),
                Email = cUser.EmailAddress,
                FirstName = cUser.FirstName,
                LastName = cUser.LastName,
                MedicalSpecialty = (cUser.MedicalSpecialty == null && !isGhost) ? "" : cUser.MedicalSpecialty,
                NewsLetterSubscribe = cUser.NewsLetterSubscribe,
                PhoneNumber = cUser.MobileNumber,
                PhysicianLicenseNumber = cUser.PhysicalLicenseNumber,
                PreferredLanguage = cUser.PreferredLanguage,
                PreferredService1 = isGhost,
                PreferredService2 = isGhost,
                Summary = cUser.Summary ?? string.Empty,
                Title = cUser.Title,
                ZipCode = cUser.PostalCode,
                Activity = cUser.Activity,
            });

            cUser.Image = pService.SetProfileImage(scUser.Profile, true);
            if (!string.IsNullOrEmpty(cUser.Image))
            {
                scUser.Profile.SetCustomProperty("Image", cUser.Image);
            }

            //if (!string.IsNullOrEmpty(cUser.Image))
            //{
            //    string imgPath = pService.GetProfileImageByName(cUser.Image);
            //    if (!string.IsNullOrEmpty(imgPath))
            //    {
            //        scUser.Profile.SetCustomProperty("Image", imgPath);
            //    }
            //}

            scUser.Profile.Save();
            if (cUser.NewsLetterSubscribe)
            {
                EmailService.Services.EmailService.AddOrUpdateListMember(cUser.EmailAddress, cUser.FirstName, cUser.LastName, cUser.NewsLetterSubscribe, cUser.Country, cUser.PreferredLanguage);
            }
            EmailService.Services.EmailService.SendImportUserWelcomeMail(cUser.FirstName, cUser.EmailAddress);
        }

        public static void ImportUsers(string json)
        {
            var iUsers = new List<UserEntity>();
            iUsers = JsonConvert.DeserializeObject<List<UserEntity>>(json);
            CreateUsers(iUsers);
        }

        public static string CreateUsers(List<UserEntity> iUsers)
        {
            StringBuilder err = new StringBuilder();
            int count = 0;
            var stopwatch = new System.Diagnostics.Stopwatch();

            stopwatch.Start();

            foreach (var userEntity in iUsers.Where(x => !string.IsNullOrEmpty(x.EmailAddress)))
            {

                try
                {
                    string fullName = string.Format(@"{0}\{1}", Settings.GetSetting("UserDomain"), userEntity.EmailAddress);

                    if (!User.Exists(fullName))
                    {

                        var cUser = User.Create(fullName, Membership.GeneratePassword(10, 3));

                        if (cUser != null)
                        {
                            using (new SecurityDisabler())
                            {
                                SaveUser(userEntity, cUser);
                                err.Append(string.Format("\nUser {0} Created", userEntity.EmailAddress));
                            }
                        }
                        count++;

                    }
                }
                catch (Exception ex)
                {

                    err.Append(string.Format("\n\n{0} : Error in import user:{1}", DateTime.Now, userEntity.EmailAddress));
                    err.Append("\nError Message:" + ex.Message);
                    if (null != ex.InnerException)
                        err.Append("\nInner Error Message:" + ex.InnerException.Message);
                    err.Append("\n\nStack Trace:" + ex.StackTrace);
                    Sitecore.Diagnostics.Log.Error(ex.Message, string.Format("Invalid data for User :{0}", userEntity.EmailAddress));
                    continue;
                }

            }
            err.Insert(0, string.Format("\nImported {0} Users  & Time taken to import users {1} ms.\n", count, stopwatch.ElapsedMilliseconds));
            return err.ToString();
        }

        ///<summary>
        /// Populate award objects from spreadsheet
        /// </summary>

        /// <param name="workSheet"></param>
        /// <param name="firstRowHeader"></param>
        /// <returns></returns>
        public static IEnumerable<UserEntity> PopulateUsers(ExcelWorksheet workSheet, bool firstRowHeader)
        {
            IList<UserEntity> users = new List<UserEntity>();

            if (workSheet != null)
            {
                Dictionary<string, int> header = new Dictionary<string, int>();

                for (int rowIndex = workSheet.Dimension.Start.Row; rowIndex <= workSheet.Dimension.End.Row; rowIndex++)
                {
                    //Assume the first row is the header. Then use the column match ups by name to determine the index.
                    //This will allow you to have the order of the columns change without any affect.

                    if (rowIndex == 1 && firstRowHeader)
                    {
                        header = ExcelHelper.GetExcelHeader(workSheet, rowIndex);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "EmailAddress")))
                        {
                            continue;
                        }
                        users.Add(new UserEntity
                        {
                            Title = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "Title"),
                            FirstName = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "FirstName"),
                            LastName = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "LastName"),
                            EmailAddress = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "EmailAddress"),
                            MedicalSpecialty = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "MedicalSpecialty"),
                            Summary = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "Summary"),
                            MobileNumber = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "MobileNumber"),
                            Address = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "Address"),
                            City = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "City"),
                            Country = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "Country"),
                            PostalCode = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "PostalCode"),
                            PhysicalLicenseNumber = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "PhysicianLicenseNumber"),
                            PreferredService1 = MainUtil.StringToBool(ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "PreferredService1"), false),
                            PreferredService2 = MainUtil.StringToBool(ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "PreferredService2"), false),
                            NewsLetterSubscribe = MainUtil.StringToBool(ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "NewsLetterSubscribe"), false),
                            Image = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "Image"),
                            PreferredLanguage = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "PreferredLanguage"),
                            Comment = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "Comment"),
                            Activity = ExcelHelper.ParseWorksheetValue(workSheet, header, rowIndex, "Activity")

                        });
                    }
                }
            }

            return users;
        }
    }
}

public static class ExcelHelper
{
    ///<summary>
    /// Gets the excel header and creates a dictionary object based on column name in order to get the index.
    /// Assumes that each column name is unique.
    /// </summary>

    /// <param name="workSheet"></param>
    /// <returns></returns>
    public static Dictionary<string, int> GetExcelHeader(ExcelWorksheet workSheet, int rowIndex)
    {
        Dictionary<string, int> header = new Dictionary<string, int>();

        if (workSheet != null)
        {
            for (int columnIndex = workSheet.Dimension.Start.Column; columnIndex <= workSheet.Dimension.End.Column; columnIndex++)
            {
                if (workSheet.Cells[rowIndex, columnIndex].Value != null)
                {
                    string columnName = workSheet.Cells[rowIndex, columnIndex].Value.ToString();

                    if (!header.ContainsKey(columnName) && !string.IsNullOrEmpty(columnName))
                    {
                        header.Add(columnName, columnIndex);
                    }
                }
            }
        }

        return header;
    }

    ///<summary>
    /// Parse worksheet values based on the information given.
    /// </summary>

    /// <param name="workSheet"></param>
    /// <param name="rowIndex"></param>
    /// <param name="columnName"></param>
    /// <returns></returns>
    public static string ParseWorksheetValue(ExcelWorksheet workSheet, Dictionary<string, int> header, int rowIndex, string columnName)
    {
        string value = string.Empty;
        int? columnIndex = header.ContainsKey(columnName) ? header[columnName] : (int?)null;

        if (workSheet != null && columnIndex != null && workSheet.Cells[rowIndex, columnIndex.Value].Value != null)
        {
            value = workSheet.Cells[rowIndex, columnIndex.Value].Value.ToString();
        }

        return value;
    }
}