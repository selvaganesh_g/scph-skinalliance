﻿using Loreal.Feature.Account.Services;
using Loreal.Feature.FollowandComments.Managers;
using Loreal.Feature.FollowandComments.Model;
using Newtonsoft.Json;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Security.Accounts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;


namespace Loreal.Feature.Account.Utilities
{
    public static class Import
    {
        public static void ImportUser(string path,bool live=false)
        {
            List<User> users = new List<User>();

            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                users = JsonConvert.DeserializeObject<List<User>>(json);
            }
            // 
            foreach (var usr in users)
            {

                try
                {
                    var fullName = Context.Domain.GetFullName(usr.Email);
                    MembershipUser userex = Membership.GetUser(fullName, false);
                    if (userex != null)
                    {
                        Sitecore.Diagnostics.Log.Error(usr.Email, "User Alredy exits and updating the Notification Settings");
                        var notification = MyNotification.Managers.NotificationManager.Get(userex.ProviderUserKey.ToString());
                        bool isnew = false;
                        if (notification == null)
                        {
                            isnew = true;
                            notification = new MyNotification.Model.Notification();
                            notification.UserId = userex.ProviderUserKey.ToString();
                            notification.Email = userex.Email;
                        }
                        notification.NotificationOne = GetType(usr.notification_1);
                        notification.NotificationTwo = GetType(usr.notification_3);
                        notification.NotificationThree = GetType(usr.notification_4);
                        notification.NotificationFour = GetType(usr.notification_5);
                        notification.NotificationFive = GetType(usr.notification_6);
                        notification.Live = live;
                        if (isnew == true)
                        {
                            MyNotification.Managers.NotificationManager.Add(notification);
                        }
                        else
                        {
                            MyNotification.Managers.NotificationManager.Update();
                        }
                        continue;
                    }
                    var user = Sitecore.Security.Accounts.User.Create(fullName, "$sfsdf@13vsdf");
                    user.Profile.Email = usr.Email;
                    user.Profile.ProfileItemId = AccountTemplate.Account.Profile.Id.ToString();
                    user.Profile.Name = usr.Name;
                    user.Profile.StartUrl = Membership.GetUser(fullName, false)?.ProviderUserKey.ToString();
                    user.Roles.Add(Role.FromName(@"lrp\users"));
                    user.Profile.Save();
                    usr.scid = user.Profile.StartUrl;
                    IProfileService iprofileService = new ProfileService();

                    // Read stream from file.
                    System.IO.MemoryStream photostream = null;
                    if (!string.IsNullOrEmpty(usr.Photo))
                    {
                        try
                        {
                            WebRequest webRequest = WebRequest.Create(string.Format("https://www.skin-alliance.com/{0}", usr.Photo));
                            using (var webResponse = webRequest.GetResponse())
                            {
                                using (var stream = webResponse.GetResponseStream())
                                {
                                    if (stream != null)
                                    {
                                        using (var memoryStream = new MemoryStream())
                                        {
                                            stream.CopyTo(memoryStream);
                                            SaveUser(iprofileService, usr, user, memoryStream);
                                        }
                                    }
                                }
                            }
                        }
                        catch
                        {
                            //update user with out image
                            SaveUser(iprofileService, usr, user, photostream);
                        }
                    }
                    SaveUser(iprofileService, usr, user, photostream);
                }
                catch (Exception ex)
                {
                    Sitecore.Diagnostics.Log.Error(usr.Email, "IMPORT USER");
                    Sitecore.Diagnostics.Log.Error(ex.Message, "IMPORT USER");
                }
            }

            System.IO.File.WriteAllText(path, JsonConvert.SerializeObject(users));
        }

        public static void ImportFollow(string userpath, string followPath, string url)
        {
            List<User> users = new List<User>();
            List<FollowImport> followList = new List<FollowImport>();
            using (StreamReader r = new StreamReader(userpath))
            {
                string json = r.ReadToEnd();
                users = JsonConvert.DeserializeObject<List<User>>(json);
            }
            using (StreamReader r = new StreamReader(followPath))
            {
                string json = r.ReadToEnd();
                followList = JsonConvert.DeserializeObject<List<FollowImport>>(json);
            }
            foreach (var distinctFollowUser in followList.Select(x => x.follow_contact).Distinct())
            {
                var currentUser = users.Where(x => x.Id == distinctFollowUser).FirstOrDefault();
                if (currentUser == null) continue;
                var fullName = Context.Domain.GetFullName(currentUser.Email);
                if (fullName == null) continue;
                MembershipUser currentMemberShipUser = Membership.GetUser(fullName, false);
                if (currentMemberShipUser == null) continue;
                var currentMemberShipUserId = currentMemberShipUser.ProviderUserKey.ToString();
                if (string.IsNullOrEmpty(currentMemberShipUserId)) continue;
                var currentMemberShipUserProfile = Context.Domain.GetUsers().Where(x => x.Profile.StartUrl == currentMemberShipUserId).FirstOrDefault()?.Profile;
                if (currentMemberShipUserProfile == null) continue;
                var userss = new List<string>();
                //login user
                foreach (var followedUser in followList.Where(x => x.follow_contact == distinctFollowUser))
                {

                    if (userss.Contains(followedUser.follow_typeid + distinctFollowUser))
                    {
                        Sitecore.Diagnostics.Log.Info(string.Format("duplicte {0} -{1}", distinctFollowUser, followedUser.follow_typeid), "Follow-Import");
                        continue;
                    }
                    userss.Add(followedUser.follow_typeid + distinctFollowUser);
                    var followUser = users.Where(x => x.Id == followedUser.follow_typeid).FirstOrDefault();
                    if (followUser == null) continue;
                    var followUserFullName = Context.Domain.GetFullName(followUser.Email);
                    if (followUserFullName == null) continue;
                    MembershipUser currentMemberFollowUser = Membership.GetUser(followUserFullName, false);
                    if (currentMemberFollowUser == null) continue;
                    var currentMemberFollowShipUserId = currentMemberFollowUser.ProviderUserKey.ToString();
                    if (string.IsNullOrEmpty(currentMemberFollowShipUserId)) continue;
                    var currentMemberFollowShipUserProfile = Context.Domain.GetUsers().Where(x => x.Profile.StartUrl == currentMemberFollowShipUserId).FirstOrDefault()?.Profile;
                    if (currentMemberFollowShipUserProfile == null) continue;
                    if (!string.IsNullOrEmpty(currentMemberFollowShipUserProfile.StartUrl)
                        && !string.IsNullOrEmpty(currentMemberFollowShipUserProfile.FullName)
                        && !string.IsNullOrEmpty(currentMemberFollowShipUserProfile.Icon)
                        && !string.IsNullOrEmpty(currentMemberShipUserProfile.StartUrl)
                        && !string.IsNullOrEmpty(currentMemberShipUserProfile.FullName)
                        && !string.IsNullOrEmpty(currentMemberShipUserProfile.Icon)
                        )
                    {
                        FollowManager.Add(new Follow()
                        {
                            FollowID = currentMemberFollowShipUserProfile.StartUrl,
                            Link = string.Format("{0}?id={1}", url, currentMemberFollowShipUserProfile.StartUrl),
                            Type = FollowandComments.Enums.FollowType.User,
                            Name = currentMemberFollowShipUserProfile.FullName,
                            ImageUrl = currentMemberFollowShipUserProfile.Icon,
                            UserId = currentMemberShipUserProfile.StartUrl,
                            FollowedName = currentMemberShipUserProfile.FullName,
                            FollowedImageUrl = currentMemberShipUserProfile.Icon,
                            FollowedLink = string.Format("{0}?id={1}", url, currentMemberShipUserProfile.StartUrl)
                        });
                    }
                }

            }

        }

        public static void ImportPost(string path)
        {
            List<Post> posts = new List<Post>();

            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                posts = JsonConvert.DeserializeObject<List<Post>>(json);
            }
            posts = posts.OrderBy(x => x.date).ToList();

            Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
            var articleRoot = master.GetItem(new Sitecore.Data.ID("{0936254B-D0BC-4358-AA61-783AF098E510}"));
            var articleMediaRoot = master.GetItem(new Sitecore.Data.ID("{BB213521-6F1D-454A-9095-6D18BC9B6364}"));
            var skinAllianceMedia = master.GetItem(new Sitecore.Data.ID("{9CA786A7-87AE-4B7B-9127-78206FFC0A33}"));
            var articleTemplate = master.GetTemplate(new Sitecore.Data.ID("{B86FE4A5-903B-4DFB-A4BA-7E84FDDF3B4C}"));
            var articleGroupFolderTemp = master.GetTemplate(new Sitecore.Data.ID("{D8B9B968-BC60-4356-B9C7-CA95A6D37651}"));

            foreach (var _post in posts)
            {
                if (string.IsNullOrEmpty(_post.Category.Trim()) || string.IsNullOrEmpty(ItemUtil.ProposeValidItemName(_post.Title.Trim()))) continue;
                //1. Create folder by year
                //2. Create folder by month
                var monthItem = GetMonthItem(_post, articleRoot, articleGroupFolderTemp);
                var monthMediaItem = GetMonthItem(_post, articleMediaRoot, articleGroupFolderTemp);
                var skinAllianceMediaItem = GetMonthItem(_post, skinAllianceMedia, articleGroupFolderTemp);
                //3. Create Article 
                if (monthItem == null) continue;
                if (monthItem.Children[ItemUtil.ProposeValidItemName(_post.Title)] != null) continue;
                var articleitem = monthItem.Add(ItemUtil.ProposeValidItemName(_post.Title), articleTemplate);
                if (articleitem == null) continue;
                articleitem.Editing.BeginEdit();
                articleitem.Fields["title"].Value = _post.Title;
                articleitem.Fields["content"].Value = _post.Summary;
                _post.scid = articleitem.ID.ToString();
                MultilistField multilistField = articleitem.Fields["Categories"];
                if (multilistField != null)
                {
                    foreach (var cat in _post.Category.Split('|'))
                    {
                        multilistField.Add(cat);
                    }
                }
                if (!string.IsNullOrEmpty(_post.URL))
                {
                    LinkField lnkField = articleitem.Fields["ExternalUrl"];
                    if (lnkField != null && _post.URL.StartsWith("http"))
                    {
                        lnkField.LinkType = "external";
                        lnkField.Target = "_blank";
                        lnkField.Url = _post.URL;
                    }
                    else
                    {
                        lnkField.LinkType = "internal";
                        lnkField.Url = DownloadMediaLink(_post.URL, master, skinAllianceMediaItem);
                    }
                }
                articleitem.Fields[Sitecore.FieldIDs.Created].Value = Sitecore.DateUtil.ToIsoDate(_post.date);
                articleitem.Fields[Sitecore.FieldIDs.CreatedBy].Value = string.Format("lrp\\{0}", _post.Contact);

                if (monthMediaItem != null)
                {
                    try
                    {
                        if (_post.postimage != null)
                        {
                            var imageUrl = _post.postimage.StartsWith("http") ? _post.postimage : string.Format("https://www.skin-alliance.com/{0}", _post.postimage);
                            WebRequest webRequest = WebRequest.Create(imageUrl);
                            using (var webResponse = webRequest.GetResponse())
                            {
                                using (var stream = webResponse.GetResponseStream())
                                {
                                    if (stream != null)
                                    {
                                        using (var memoryStream = new MemoryStream())
                                        {
                                            string fileName = imageUrl.Substring(imageUrl.LastIndexOf("/") + 1);
                                            stream.CopyTo(memoryStream);
                                            var mediaitem = SetProfileImage(master, memoryStream, articleitem.Name, fileName, _post.Title, monthMediaItem);

                                            ImageField field = articleitem.Fields["Image"];
                                            if (field != null)
                                            {
                                                field.MediaID = mediaitem.ID;
                                                field.SetAttribute("mediapath", mediaitem.Paths.MediaPath);
                                                field.SetAttribute("showineditor", "1");
                                            }
                                            articleitem.Editing.EndEdit();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        Sitecore.Diagnostics.Log.Error(articleitem.Name, "article Item Skipped");
                        // Nothing to do;
                        articleitem.Editing.CancelEdit();
                    }
                    articleitem.Editing.EndEdit();
                }
                System.IO.File.WriteAllText(path, JsonConvert.SerializeObject(posts));
            }
        }

        public static void ImportLikes(string postPath, string path)
        {
            List<LikeImport> likeList = new List<LikeImport>();

            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                likeList = JsonConvert.DeserializeObject<List<LikeImport>>(json);
            }
            List<Post> posts = new List<Post>();
            using (StreamReader r = new StreamReader(postPath))
            {
                string json = r.ReadToEnd();
                posts = JsonConvert.DeserializeObject<List<Post>>(json);
            }
            foreach (var like in likeList)
            {
                var post = posts.Where(x => x.Id == like.post_id).FirstOrDefault();
                if (post == null) continue;
                if (post.scid == null) continue;
                for (int i = 0; i < like.post_nblike; i++)
                {
                    LikeManager.Add(new Like()
                    {
                        ArticleID = post.scid, 
                        UserId = Guid.NewGuid().ToString(),
                        Date = System.DateTime.UtcNow
                    });
                }

            }

        }

        public static void ImportComments(string userpath, string postPath, string path, string url)
        {
            List<User> users = new List<User>();
            List<PostComment> commentList = new List<PostComment>();
            using (StreamReader r = new StreamReader(userpath))
            {
                string json = r.ReadToEnd();
                users = JsonConvert.DeserializeObject<List<User>>(json);
            }
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                commentList = JsonConvert.DeserializeObject<List<PostComment>>(json);
            }
            List<Post> posts = new List<Post>();
            using (StreamReader r = new StreamReader(postPath))
            {
                string json = r.ReadToEnd();
                posts = JsonConvert.DeserializeObject<List<Post>>(json);
            }
            foreach (var comment in commentList)
            {
                var currentUser = users.Where(x => x.Id == comment.postcommentaire_contact).FirstOrDefault();
                if (currentUser == null) continue;
                var fullName = Context.Domain.GetFullName(currentUser.Email);
                if (fullName == null) continue;
                MembershipUser currentMemberShipUser = Membership.GetUser(fullName, false);
                if (currentMemberShipUser == null) continue;
                var currentMemberShipUserId = currentMemberShipUser.ProviderUserKey.ToString();
                if (string.IsNullOrEmpty(currentMemberShipUserId)) continue;
                var currentMemberShipUserProfile = Context.Domain.GetUsers().Where(x => x.Profile.StartUrl == currentMemberShipUserId).FirstOrDefault()?.Profile;
                if (currentMemberShipUserProfile == null) continue;
                var post = posts.Where(x => x.Id == comment.postcommentaire_post).FirstOrDefault();
                if (post == null) continue;
                CommentManager.Add(new Comment()
                {
                    ArticleID = post.scid,
                    UserId = currentMemberShipUserId,
                    Name = currentMemberShipUserProfile.FullName,
                    ImageUrl = currentMemberShipUserProfile.Icon,
                    Link = string.Format("{0}?id={1}", url, currentMemberShipUserProfile.StartUrl),
                    Text = comment.postcommentaire_content,
                    Date = comment.postcommentaire_date
                });
            }

        }
        public static MyNotification.NotificationSendType GetType(string value)
        {
            switch (value)
            {
                case "0":
                default:
                    return MyNotification.NotificationSendType.Never;
                case "1":
                    return MyNotification.NotificationSendType.Immediately;
                case "2":
                    return MyNotification.NotificationSendType.Daily;
                case "3":
                    return MyNotification.NotificationSendType.Weekly;
            }
        }
        public static string DownloadMediaLink(string internalLink, Sitecore.Data.Database databae, Item mediamonth)
        {
            try
            {
                if (internalLink != null)
                {
                    var imageUrl = string.Format("https://www.skin-alliance.com/{0}", internalLink);
                    WebRequest webRequest = WebRequest.Create(imageUrl);
                    using (var webResponse = webRequest.GetResponse())
                    {
                        using (var stream = webResponse.GetResponseStream())
                        {
                            if (stream != null)
                            {
                                using (var memoryStream = new MemoryStream())
                                {
                                    string fileName = internalLink.Substring(internalLink.LastIndexOf("/") + 1);
                                    stream.CopyTo(memoryStream);
                                    Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
                                    options.Database = databae;
                                    options.Destination = string.Format("{0}/{1}", mediamonth.Paths.FullPath.TrimEnd('/'), ItemUtil.ProposeValidItemName(fileName));
                                    options.OverwriteExisting = true;

                                    var item = Sitecore.Resources.Media.MediaManager.Creator.CreateFromStream(memoryStream, fileName, options);
                                    item.Reload();
                                    return item.Paths.MediaPath;
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                // Nothing to do;
                return string.Empty;
            }
            return string.Empty;
        }

        public static Item SetProfileImage(Sitecore.Data.Database databae, Stream file, string medianame, string filename, string title, Item mediamonth)
        {
            Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
            options.AlternateText = title;
            options.Database = databae;
            options.Destination = string.Format("{0}/{1}", mediamonth.Paths.FullPath.TrimEnd('/'), ItemUtil.ProposeValidItemName(filename));
            options.OverwriteExisting = true;

            var item = Sitecore.Resources.Media.MediaManager.Creator.CreateFromStream(file, string.Format("{0}.png", medianame), options);
            item.Reload();
            return item;
        }

        private static Item GetMonthItem(Post post, Item root, TemplateItem group)
        {
            var year = post.date.Year.ToString();
            var month = post.date.Month.ToString();
            Item yrItem = null;
            Item moItem = null;
            if (root.Children[year] == null)
            {
                yrItem = root.Add(year, group);
            }
            else
            {
                yrItem = root.Children[year];
            }
            if (yrItem.Children[month] == null)
            {
                moItem = yrItem.Add(month, group);
            }
            else
            {
                moItem = yrItem.Children[month];
            }
            return moItem;
        }

        public static void SaveUser(IProfileService iprofileService, User usr, Sitecore.Security.Accounts.User user, MemoryStream photostream)
        {
            string[] ghostProfile = new string[] { "loreal.com", "rd.loreal.com", "us.loreal.com", "rd.us.loreal.com", "loreal.intra", "skinceuticals", "newscosmetique", "agencesurf", "technicis" };
            bool isGhost = true;
            foreach (var gh in ghostProfile)
            {
                if (usr.Email.Contains(gh)) isGhost = false; break;
            }
            iprofileService.SetProfile(user.Profile, new Models.SignupModel
            {
                Title = usr.Title,
                FirstName = usr.FirstName,
                LastName = usr.Name,
                Address = usr.Addressone + usr.AddressTwo,
                Email = usr.Email,
                MedicalSpecialty = (usr.Specility == null && !isGhost) ? "dermatologist" : usr.Specility,
                Country = usr.Countrycode.ToLower(),
                PhysicianLicenseNumber = null,
                Summary = usr.Summary ?? string.Empty,
                Image = iprofileService.SetProfileImage(user.Profile, photostream == null ? true : false, photostream),
                City = usr.Area,
                ZipCode = usr.Zipcode,
                PreferredService1 = isGhost,
                PreferredService2 = isGhost
            });
            user.Profile.Save();
        }
        public class User
        {

            public string Id { get; set; }
            public string Title { get; set; }
            public string Name { get; set; }
            public string FirstName { get; set; }
            public string Email { get; set; }
            public string Specility { get; set; }
            public string Addressone { get; set; }
            public string AddressTwo { get; set; }
            public string Area { get; set; }
            public string Zipcode { get; set; }
            public string PhoneNumber { get; set; }
            public string Countrycode { get; set; }
            public string Summary { get; set; }
            public string notification_1 { get; set; }
            public string notification_2 { get; set; }
            public string notification_3 { get; set; }
            public string notification_4 { get; set; }
            public string notification_5 { get; set; }
            public string notification_6 { get; set; }
            public string Photo { get; set; }
            public string scid { get; set; }
        }

        public class Post
        {
            public string Id { get; set; }

            public string Contact { get; set; }
            public string URL { get; set; }
            public string Title { get; set; }
            public string Summary { get; set; }
            public DateTime date { get; set; }
            public string postimage { get; set; }
            public string categoryId { get; set; }
            public string Category { get; set; }
            public string sapostcol { get; set; }
            public string scid { get; set; }
        }

        public class PostComment
        {
            public string postcommentaire_id { get; set; }
            public string postcommentaire_contact { get; set; }
            public string postcommentaire_post { get; set; }
            public DateTime postcommentaire_date { get; set; }
            public string postcommentaire_content { get; set; }

        }
        public class FollowImport
        {
            public string follow_id { get; set; }
            public string follow_contact { get; set; }
            public string follow_type { get; set; }
            public string follow_typeid { get; set; }         

        }

        public class LikeImport
        {
            public string post_id { get; set; }
            public int post_nblike { get; set; }
        }
    }
}