﻿namespace Loreal.Feature.Account
{
    using Sitecore.Data;
    public struct DiscoverDermocosmeticsScience
    {
        public static readonly ID ID = new ID("{1D0C94EE-3DE0-4C2F-B8A0-669C6C781A31}");
        public struct Fields
        {
            public static readonly ID Title = new ID("{DDB1C737-E082-4570-BFE5-EF2EBC7B8666}");
            public static readonly ID Description = new ID("{9036C02F-AEC5-4605-9C76-EE91CFEF3C32}");
            public static readonly ID QuickLinks = new ID("{87BDC1A2-E384-404E-B9D6-4ABA796E0602}");

        }
        public struct QuickLink
        {

            public static readonly ID ID = new ID("{AD85BAF6-AFE3-45C8-BA9D-078BE9FC2E6D}");

            public struct Fields
            {
                public static readonly ID Title = new ID("{AA72B13A-872F-48F7-8F4F-6425F96F05A1}");
                public static readonly ID Description = new ID("{79A488E4-3956-4559-9FB2-343C2FA83D77}");
                public static readonly ID Image = new ID("{6B95E3FA-D886-48F8-9200-E8C92A7FDE82}");
                public static readonly ID CTA = new ID("{33B15B07-5BE1-4284-9E2D-8C0AC793D091}");
            }
        }
    }
    public struct AccountTemplate
    {
        public static readonly string WelcomeText = "WelcomeText";
        public static readonly string SearchText = "SearchText";
        internal const string ConfirmTokenKey = "Conform Password Token";
        public static readonly string ResetPasswordButtonText = "SignIn";

        public struct Account
        {
            public struct SignIn
            {
                public struct Fields
                {
                    public static readonly ID Introduction = new ID("{6E6C0B3C-7041-4FFA-B0FC-849D1975D368}");
                    public static readonly ID SubHeading = new ID("{B160E952-0281-4583-9C75-B6D9485B2B19}");
                    public static readonly ID UsernameLabel = new ID("{53E213E6-E020-4C32-A559-EC90B5B67179}");
                    public static readonly ID PasswordLabel = new ID("{B80F0331-68E7-4515-8485-903B5C8B2390}");
                    public static readonly ID DisplayRememberMe = new ID("{82D265E1-99D0-4A5F-964F-775A14DACE71}");
                    public static readonly ID RememberMeLabel = new ID("{129B54AB-7D51-4C6B-899C-642BB3EE4B85}");
                    public static readonly ID LoginButtonCaption = new ID("{99C9FDD7-4503-489B-8A21-115B963AA341}");
                    public static readonly ID FailureText = new ID("{BC5CD635-130E-4A03-A5D0-E1AFB9AE6055}");
                    public static readonly ID DestinationUrl = new ID("{D7BE3834-097A-4151-827D-D852B54FF530}");
                    public static readonly ID RegistrationUrl = new ID("{B958DD1F-1DC4-4A50-83A1-DDC86EBA8FA1}");
                    public static readonly ID ForgotPasswordUrl = new ID("{F055DD10-7125-4484-97AB-A7A4AA2E162D}");
                    public static readonly ID UsernameRequired = new ID("{AD1774AA-6074-4734-A43F-34AFD21FC4A9}");
                    public static readonly ID InvalidUsername = new ID("{FB9F7D88-CBD0-435E-A4E3-D352E4F6EAC0}");
                    public static readonly ID PasswordRequired = new ID("{0CD84B69-9537-4B3D-90DB-9C790A74942E}");
                    public static readonly ID RegisterUrlPrefixText = new ID("{FB02D8F3-B129-4F22-ADB3-EE5BDD64F469}");
                    public static readonly ID OrText = new ID("{0F2BF30A-906F-4305-A9B1-F4BD2FAFC701}");

                }
            }
            public struct Signup
            {
                public struct Fields
                {
                    public static readonly ID Introduction = new ID("{97D1C1ED-0C08-4C81-BC38-83DCB84C95B2}");
                    public static readonly ID SubHeading = new ID("{E52680F2-D227-40A6-A598-E6870D6C71ED}");
                    public static readonly ID PersonalInfo = new ID("{393E5AAB-348E-4A7B-92D6-2EA3E9C42D6B}");
                    public static readonly ID LoginInfo = new ID("{9304F29F-93D5-4F07-9652-E9BAF9F0DF35}");
                    public static readonly ID FirstNameLabel = new ID("{776FDC2A-88D6-4D68-B292-462133CA9CA1}");
                    public static readonly ID LastNameLabel = new ID("{3FB7ED6A-EC9B-4B81-B604-C3A1635FA754}");
                    public static readonly ID EmailLabel = new ID("{32D38BD7-9D5F-41C4-959D-4CC1DB0F2575}");
                    public static readonly ID PasswordLabel = new ID("{DA4AA677-2146-4F1C-91D4-D3401736A280}");
                    public static readonly ID ConfirmPasswordLabel = new ID("{9AA3806C-04D8-4115-A090-90970F139C05}");
                    public static readonly ID PhoneNumberCaption = new ID("{5D6051A4-C767-46C0-A355-CE2721FE1900}");
                    public static readonly ID AddressCaption = new ID("{1661B742-2F46-49A2-AA59-72E9AD4DD2D7}");
                    public static readonly ID CountryCaption = new ID("{1E2D71EE-FB63-4361-B8D5-118265534016}");
                    public static readonly ID PhysicianLicenseNumberCaption = new ID("{3064B23A-C88F-4E8E-935B-34E41ACEB01B}");
                    public static readonly ID MedicalSpecialtyCaption = new ID("{A87BCA9F-2A57-4873-840E-AE74D2662EE3}");
                    public static readonly ID UsernameCaption = new ID("{CAB8CBA6-106A-4F67-8DA0-EE6B10192B9D}");
                    public static readonly ID SignupButtonLabel = new ID("{ADAE1A06-6E53-4450-9A5E-7C3D5C8F034A}");
                    public static readonly ID DestinationUrl = new ID("{C72F90AA-5F25-4B7D-9B57-9F7DC16486AA}");
                    public static readonly ID FirstNameRequired = new ID("{0557A13C-0C5A-4AF8-80DA-23BE6708D322}");
                    public static readonly ID LastNameRequired = new ID("{98FD082B-D547-454A-B929-F815B33E4EE3}");
                    public static readonly ID EmailRequired = new ID("{A66C9346-EA0D-4D45-BF79-4FA4158C5DF9}");
                    public static readonly ID InvalidEmail = new ID("{E01BF558-F530-40F4-BA8F-462A4176BA6D}");
                    public static readonly ID PasswordRequired = new ID("{EF2E4F65-0DDC-438F-A0DA-5CC29A379338}");
                    public static readonly ID ConfirmPasswordRequired = new ID("{612DB901-DF88-4F9B-B5F2-1EDBE5C3DB4B}");
                    public static readonly ID CountryRequired = new ID("{9517D7DF-0F5D-49A6-B7E2-23F16F59130A}");
                    public static readonly ID PhysicianLicenseNumberRequired = new ID("{72F314DE-A2AB-48D2-8237-3F8B9E42EF80}");
                    public static readonly ID MedicalSpecialtyRequired = new ID("{CE5FD3AC-5A63-41A6-9A6E-EA28119C3876}");
                    public static readonly ID InvalidPassword = new ID("{51293936-AF5E-46B6-8EFD-71AE37D09958}");
                    public static readonly ID PasswordMismatch = new ID("{EEC790E3-B15E-4E90-BFF4-CFD6ABE96BE7}");
                    public static readonly ID FailureText = new ID("{13D44AD5-F9FC-42F7-97C0-DDD92B55DD90}");
                    public static readonly ID UserExistsText = new ID("{EEEC8980-7BE7-4612-856D-94F922C713EC}");
                    public static readonly ID ThanksPageDestinationUrl = new ID("{4761E5DD-7FB3-431D-93DC-6A75FB7A1EF9}");
                    public static readonly ID CountriesFolder = new ID("{FD051860-7CDF-40CA-B760-E79F17719A5C}");
                    public static readonly ID MedicalSpecialtyFolder = new ID("{6E9BD07D-6ABA-47DD-9C92-3F7475CA3F7F}");
                    public static readonly ID SummaryCaption = new ID("{A5B3E892-9312-4937-A497-98FAB7FA7E67}");
                    public static readonly ID CityCaption = new ID("{BA12753E-9DD3-40B6-B2B2-FFE4D385910C}");
                    public static readonly ID ZipCodeCaption = new ID("{F0F6E9C8-B5A4-4A27-A25B-89C83292D00D}");
                    public static readonly ID PreferredService1Caption = new ID("{0BB250B9-4B69-47DC-BFDF-1B5EE4899518}");
                    public static readonly ID PreferredService2Caption = new ID("{45E9DA66-FD9D-4F0C-8832-9A35B437A80C}");
                    public static readonly ID NewsletterSubscribeCaption = new ID("{CF5C7E2A-E30C-41D2-954E-0A4EBB2B2D6F}");
                    public static readonly ID TermsandConditionText = new ID("{4182C94D-CECD-4236-B990-0CE33E2C5D2D}");
                    public static readonly ID ActivityCaption = new ID("{7A2DA031-449F-4042-B34B-9B88C35E7A64}");
                    public static readonly ID ResumeCaption = new ID("{0EA57A7E-357C-43D1-8B8D-647F4018D5B0}");
                    public static readonly ID PreferredLanguageCaption = new ID("{CF7E1071-0134-46F1-9261-465D27B1C8C6}");
                    public static readonly ID UserTitlesFolder = new ID("{6D3EE3FA-208E-4EDC-A077-E1117B0F0C7D}");
                    public static readonly ID PhysicianLicenseNumberMandatoy = new ID("{1D8877AA-5FCB-41A7-BE36-611C0C4C3A34}");
                    public static readonly ID PasswordHintText = new ID("{4717689D-89E5-40D3-AC26-0C299CDC9312}");
                    public static readonly ID OrText = new ID("{FBC44A03-059C-4F54-A70C-E31D46B44B24}");
                }
            }
            public struct Profile
            {
                public static readonly ID Id = new ID("{772F2290-5FA4-4E31-8A4B-985CAB04929E}");
                public static readonly ID ImageId = new ID("{76807981-28BA-49FF-BAA2-9DC1C40F2F73}");
                public static readonly ID MediaRoot = new ID("{17087B1A-E3D3-478F-851D-D8B9E8B5E823}");
                public static readonly string DocumentRootItemName = "Documents";
                public static readonly ID MediaFolterID = new ID("{FE5DD826-48C6-436D-B87A-7C4210C7413B}");

                public struct Fields
                {
                    public static readonly ID FirstName = new ID("{D6598390-89C1-43A0-BC6A-CC2BF5E1246E}");
                    public static readonly ID LastName = new ID("{CCF3F488-C6D3-45AD-8F39-62135DE3E6E8}");
                    public static readonly ID MobileNumber = new ID("{C244C6FE-FAF0-4A51-8DA9-61F0ED7D56E5}");
                    public static readonly ID HomeNumber = new ID("{2F32C06A-88BA-4973-BF8A-18715C16CB7D}");
                    public static readonly ID AddressOne = new ID("{C39E9E17-49F0-4336-B97E-79287D2A1722}");
                    public static readonly ID AddressTwo = new ID("{2C3B38A4-F697-4336-8F4D-2736954E5966}");
                    public static readonly ID City = new ID("{AB26C8D6-531A-4E74-A6A9-28C64321001D}");
                    public static readonly ID Country = new ID("{EC3CA525-2332-4C80-8A3F-949A0844E38E}");
                    public static readonly ID Zipcode = new ID("{EEB2A4C7-D9AA-4E67-8A55-AADC123DA580}");
                    public static readonly ID DoB = new ID("{75B9100C-A8EF-432C-ADC8-B9233A2C0033}");
                    public static readonly ID Portrait = new ID("{8E9497D4-0BC1-4BDC-9038-B31EBDD01EC7}");
                    public static readonly ID MedicalSpecialty = new ID("{4594901F-4DFE-457B-97FE-89B02D7D2B46}");
                    public static readonly ID PhysicianLisenceNumber = new ID("{01F32055-6622-48E1-8BF1-7B2A74AA778E}");
                    public static readonly ID Image = new ID("{35B4B576-CD2F-4310-9587-CBC13155B86A}");
                    public static readonly ID Summary = new ID("{6CB63D35-0736-4CBF-86D1-FA3CDC9CEF0B}");
                    public static readonly ID IsAccepted = new ID("{236CA763-B4C9-4A2F-BCD9-A06820D14B85}");
                    public static readonly ID PreferredLanguage = new ID("{C0F02A9E-7C61-413D-A091-A5B7C0B75061}");
                    public static readonly ID Activity = new ID("{56983D87-5890-464E-97DF-C12AF0980928}");
                    public static readonly ID PreferredService1 = new ID("{AB3C0C62-5C53-489E-955D-2470DD5BFF99}");
                    public static readonly ID PreferredService2 = new ID("{D7132977-61D7-463C-B940-A0E0054A43F1}");
                    public static readonly ID NewsletterSubscribe = new ID("{14BCA5A7-070D-43D0-B170-EE8D540B848A}");
                    public static readonly ID Resume = new ID("{80F20DCC-24D7-4EB2-8971-C4AC0EE7E15D}");
                    public static readonly ID Title = new ID("{B6CB4FFF-B310-4B9D-BE9F-7AB583FFB067}");
                }
            }
            public struct MiniProfile
            {
                public static readonly ID ID = new ID("{CA3FEDA0-8313-4331-95E8-226558897D22}");
                public struct Fields
                {
                    public static readonly ID WelcomeTitle = new ID("{071B1BC1-4B3F-45D7-AF20-4E40AD511ECD}");
                    public static readonly ID NavigationItems = new ID("{25BD7493-C8F2-4499-A47C-0F24894B35D2}");
                    public static readonly ID LogoutText = new ID("{270D229D-D523-40EB-B10D-F2E0C362426C}");
                    public static readonly ID CreatePostLink = new ID("{BEBD7E51-96A0-42EF-AD93-ED1DADC19D91}");
                    public static readonly ID MobileNavigationItems = new ID("{86651BB8-5AF8-48AC-9375-A302727A740E}");
                }
            }
            public struct SocialConnector
            {
                public static readonly ID ID = new ID("{1E3B4807-9FFA-467D-B781-B3BB76B6C9E2}");
                public struct Fields
                {
                    public static readonly ID FacebookAPIKey = new ID("{8D106032-CBA2-4973-9657-8473B9A5BB8A}");
                    public static readonly ID FacebookSecretKey = new ID("{16E17ED7-C2FB-4637-ADCF-FE5CF3E3CD71}");
                    public static readonly ID FacebookLoginTitle = new ID("{7847D279-0FD4-4372-9187-9644EA153396}");
                    public static readonly ID FacebookRegisterTitle = new ID("{44750BAC-8D96-43C2-B906-289B4BDAD82E}");
                    public static readonly ID TwitterAPIKey = new ID("{6F452205-D0A3-4ED4-B0CC-65F5AF836A41}");
                    public static readonly ID TwitterSecretKey = new ID("{47B8B989-0C7F-4217-8845-42112C403F05}");
                    public static readonly ID TwitterLoginTitle = new ID("{4F37161F-F924-4558-8C50-7DE3DC36E6B1}");
                    public static readonly ID TwitterRegisterTitle = new ID("{113C9F5B-5473-4368-A61C-E2A37A902760}");
                    public static readonly ID LinkedInAPIKey = new ID("{6C88A568-5689-45BB-9912-044004C3D413}");
                    public static readonly ID LinkedInSecretKey = new ID("{4FFB0D0A-4EE5-44E2-A349-95C810033F85}");
                    public static readonly ID LinkedInLoginTitle = new ID("{971046DF-3161-479F-BBFE-23B3732FB8B0}");
                    public static readonly ID LinkedInRegisterTitle = new ID("{4FD30189-8913-4E3F-9383-68093DA1B6F7}");
                    public static readonly ID SeparatedText = new ID("{7FC0288E-6A59-43C7-969D-B5464B433A2F}");
                    public static readonly ID SocialRegisterURL = new ID("{0A764D14-AAA9-4F7B-9067-8F2116A7CF75}");
                    public static readonly ID SharedSecretKey = new ID("{EF482729-90EF-4B85-90AB-A59147C0CAEA}");
                    public static readonly ID LRPWebSiteUrl = new ID("{78B7EEF0-90B9-4BD9-905E-B9BB87FD4BF1}");
                    public static readonly ID OffsetTime = new ID("{9B922935-4E7D-4FFF-8172-BAFD1C0DBCAF}");
                }
            }
            public struct MyProfile
            {
                public static readonly string ChangePasswordText = "changepasswordtext";
                public static readonly string EditProfileText = "editprofiletext";
                public static readonly string EditProfileButtonText = "editprofilebuttontext";
                public static readonly string InvalidImage = "invalidimage";
                public static readonly string ImageBrowseText = "imagebrowsetext";
                public static readonly string UploadResumeText = "uploadresumetext";
                public static readonly string UploadResumeHint = "uploadresumehint";
                public static readonly string PasswordStrength = "passwordstrength";
                public static readonly string PasswordMatch = "passwordmatch";
                public static readonly ID AttachmentCaption = new ID("{F9CD343B-B498-4844-B800-C7231B6A4DB4}");
                public static readonly ID TitleCaption = new ID("{6F3EDD92-CDC8-49AE-A241-D4BC863F7099}");
                public static readonly ID Attachment = new ID("{66DCCA45-69F5-4BFD-9059-17F841636E66}");
                public static readonly ID PhoneNumberValidationMessage = new ID("{6609D5EB-6BE1-434B-9D3F-F9995668D946}");

                public struct ChangePassword
                {
                    public static readonly string OldPasswordCaption = "oldpasswordcaption";
                    public static readonly string OldPasswordRequired = "oldpasswordrequired";
                    public static readonly string ChangePasswordButtonText = "changepasswordbuttontext";
                    public static readonly string InvalidOldPasswordText = "invalidoldpassword";
                    public static readonly ID ID = new ID("{6C9CD096-7016-4172-B451-3195B4F89A24}");
                    public struct Fields
                    {
                        public static readonly ID ModalTitle = new ID("{1AE22FF5-0CB6-41A3-A1A1-FA72E83A98B3}");
                        public static readonly ID ExistingPasswordCaption = new ID("{42BD50B2-784B-417E-81A6-96D7A714B8E2}");
                        public static readonly ID ExistingPasswordRequired = new ID("{9195980E-B1F8-42F4-98FA-AA1FF1A20D6D}");
                        public static readonly ID ExistingPasswordIncorrect = new ID("{51CFE5E2-D85B-40E2-8842-89FE1EBE6456}");
                        public static readonly ID NewPasswordCaption = new ID("{01441608-E17B-4BC9-982C-6C7F3788A1BE}");
                        public static readonly ID NewPasswordRequired = new ID("{A8194B2C-C5A5-4AAB-82DA-C351AF666190}");
                        public static readonly ID NewPasswordStrength = new ID("{0D188D1B-C8AC-4B06-A1C5-F54C75E3BC17}");
                        public static readonly ID NewPasswordIncorrect = new ID("{E62929D6-2086-4001-BC1D-3FD26984E550}");
                        public static readonly ID NewPasswordStrengthText = new ID("{66A4EAAA-7CD8-4D42-A470-71A8B699E5C2}");
                        public static readonly ID ConfirmPasswordCaption = new ID("{F4788EC9-B1F2-4738-A98D-77E45034CCD3}");
                        public static readonly ID ConfirmPasswordRequired = new ID("{F0C1D141-32D3-4943-BF98-2B4BDD4BC524}");
                        public static readonly ID ConfirmPasswordIncorrect = new ID("{711E89EC-85F7-4504-9284-132F54A6519D}");
                        public static readonly ID SubmitButtonText = new ID("{8320D227-7755-44E4-A966-EFEBD93807D3}");
                        public static readonly ID Oldasnewpassword = new ID("{97B1429C-223B-4600-A9F2-DAFCE26C93BD}");
                    }

                }
            }
            public struct Directory
            {
                public static readonly ID Template = new ID("{EE5200F7-4FDE-4956-A81F-446740176970}");
                public struct Fields
                {
                    public static readonly ID ExpandLabel = new ID("{721B1375-73C7-4DDF-A649-F911B75EEF88}");
                    public static readonly ID CollapseLabel = new ID("{4454954E-041A-4CF3-B432-F7D739833375}");
                    public static readonly ID FilterByCountry = new ID("{4C84C533-BE42-4FC6-850E-1265BBAF9CA5}");
                    public static readonly ID FilterBySpecialty = new ID("{1F1F142F-90E7-40FC-96E4-6C882D792BF6}");
                    public static readonly ID Flag = new ID("{E7623190-2437-4BCA-8468-8E289A40E540}");
                    public static readonly ID NotFoundMessage = new ID("{7A4F1ED1-00CF-474E-9B27-D7D2B1F7D89F}");
                }
                public static readonly string FollowLink = "FollowLink";
                public static readonly string SummaryLabel = "SummaryLabel";
                public static readonly string AttachmentLabel = "AttachmentLabel";

            }
            public struct Country
            {
                public static readonly ID Template = new ID("{E46EDF57-6CD2-4961-B85C-8DBCAFAD784E}");
                public struct Fields
                {
                    public static readonly ID Code = new ID("{18836CB1-9377-4CC4-8A54-3EF4C08C53CF}");
                    public static readonly ID Name = new ID("{26EE3643-B1AE-4875-95FF-00CE1B81172A}");
                    public static readonly ID Flag = new ID("{E7623190-2437-4BCA-8468-8E289A40E540}");
                }
            }
            public struct City
            {
                public static readonly ID ID = new ID("{399C7802-437D-4FD2-95C2-7A45025444B8}");
                public static readonly ID CityFolder = new ID("{B72763D1-A47D-49C9-97D7-BAAE2FA30EAE}");
                public struct Fields
                {
                    public static readonly ID ID = new ID("{7D24AF4A-8350-4559-9F2C-B6BBAF37A499}");
                    public static readonly ID CityName = new ID("{F2673B5F-D978-4354-9173-B93C1524EF27}");
                    public static readonly ID CityDisplayName = new ID("{ABF8073E-2599-477F-BBB7-9258D60173B6}");

                }
            }
            public struct Language
            {
                public static readonly ID ID = new ID("{AF93A750-C6F7-475F-8AC7-FF10F00B08C5}");
                public static readonly ID LanguageFolder = new ID("{76173248-5785-4B39-B73B-C6414AC7C336}");
                public struct Fields
                {
                    public static readonly ID Name = new ID("{BEDCD419-DF8B-40DD-BDF6-182586FB809D}");
                    public static readonly ID DisplayName = new ID("{FF32719F-8EE3-4C6A-A291-B2DD8E9BF6B5}");
                    public static readonly ID ISOCode = new ID("{D92AF19F-1CC2-41C8-B1E8-CEAE4761A221}");

                }
            }
            public struct ThankYourForRegistration
            {
                public static readonly ID Template = new ID("{9B9E512F-F81C-43F3-8AEA-770281E96489}");
                public struct Fields
                {
                    public static readonly ID Title = new ID("{89E228D5-DF88-4C7F-A480-6FF3DCBF27BD}");
                    public static readonly ID Description = new ID("{477FECDA-2439-4652-9176-1E362FEE7BC5}");
                    public static readonly ID DestinationUrl = new ID("{4761E5DD-7FB3-431D-93DC-6A75FB7A1EF9}");
                    public static readonly ID BackToLoginLabel = new ID("{2A75DCC2-6A93-4CAB-8A2D-30C6B7CDA1D5}");
                    public static readonly ID CompleteProfileUrl = new ID("{863E684C-605F-4B24-8941-54E90CDAC1FA}");
                }
            }

            public struct ForgotPassword
            {
                public static readonly ID Template = new ID("{A547C56C-87B7-415A-A025-2EF51AA12319}");
                public struct Fields
                {
                    public static readonly ID Title = new ID("{787CBCFF-90DF-46D3-8A32-40F1C63E9ABE}");
                    public static readonly ID PlaceholderText = new ID("{4FBDD7CD-1D86-4977-A02D-5205F222CF7D}");
                    public static readonly ID SendButtonLabel = new ID("{54B5F484-B983-46D2-BA10-4739223A79EC}");
                    public static readonly ID ErrorMessage = new ID("{9163E2E3-4DBF-495E-B23E-F9A4B8624400}");
                    public static readonly ID BackButtonLabel = new ID("{3C642D6B-BD80-408F-B9BE-0276E7E43AE1}");
                    public static readonly ID DestinationUrl = new ID("{ED62750F-2A4E-4A30-96A6-289745685959}");
                    public static readonly ID SuccessMessage = new ID("{9DABA997-C7FE-4F80-A19C-1C43D7260EBD}");
                    public static readonly ID EmailIdRequired = new ID("{B39C2AB4-9632-41BB-B322-178CEA7857C6}");
                    public static readonly ID EmailTemplate = new ID("{4B22FF2F-F5D2-4F2F-8666-6AB0C929025B}");
                    public static readonly ID EmailSubject = new ID("{40EF2CFD-A3C6-42A6-9905-8C2F2C2AA3F8}");
                }
            }
        }

        public struct Settings
        {
            public static readonly ID TermsandConditions = new ID("{A5B904E9-163E-4CB8-84B1-B9D00587D77E}");
            public static readonly ID IAgreeText = new ID("{A5BCEEB0-936E-4AAB-A56E-32B4BEFE90A1}");
            public static readonly ID WelcomeText = new ID("{7031EA01-A4A8-4B6D-92D5-F2D3E9CFE432}");
            public static readonly ID MyProfileUrl = new ID("{D4B92393-F15D-4D41-9986-2E8048EFCF04}");

        }

        public struct MailChimp
        {
            public static readonly ID Datacenter = new ID("{508B98E0-F3FE-4B61-921A-669C6EB9F36B}");
            public static readonly ID APIKey = new ID("{49A03DFE-2469-4F82-94B9-AC73D7B4F9C0}");
            public static readonly ID ListId = new ID("{D78F9754-974C-4261-9F35-EB9A0AB20CE7}");
        }
    }
}

