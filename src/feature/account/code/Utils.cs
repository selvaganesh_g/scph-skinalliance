﻿using Loreal.Feature.Account.Models;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.SecurityModel;
using Sitecore.XA.Foundation.SitecoreExtensions.Extensions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Loreal.Feature.Account
{
    public static class Utils
    {

        public static Dictionary<string, object> ToDictionary(object source)
        {
            return source.ToDictionary<object>();
        }

        public static Dictionary<string, T> ToDictionary<T>(this object source)
        {
            if (source == null) ThrowExceptionWhenSourceArgumentIsNull();

            var dictionary = new Dictionary<string, T>();
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(source))
            {
                object value = property.GetValue(source);
                if (IsOfType<T>(value))
                {
                    dictionary.Add(property.Name, (T)value);
                }
            }
            return dictionary;
        }

        private static bool IsOfType<T>(object value)
        {
            return value is T;
        }
        private static void ThrowExceptionWhenSourceArgumentIsNull()
        {
            throw new System.NullReferenceException("Unable to convert anonymous object to a dictionary. The source anonymous object is null.");
        }

        public static bool TryAddCookie(this System.Net.WebRequest webRequest, System.Net.Cookie cookie)
        {
            System.Net.HttpWebRequest httpRequest = webRequest as System.Net.HttpWebRequest;
            if (httpRequest == null)
            {
                return false;
            }

            if (httpRequest.CookieContainer == null)
            {
                httpRequest.CookieContainer = new System.Net.CookieContainer();
            }

            httpRequest.CookieContainer.Add(cookie);
            return true;
        }
        public static string GetFailureUrl(HttpRequestBase request)
        {
            return GenerateUrl(request, false);
        }

        private static string GenerateUrl(HttpRequestBase request, bool success)
        {
            var nameValueCollection = HttpUtility.ParseQueryString(request.Url.Query);
            nameValueCollection.Remove("success");
            nameValueCollection.Add("success", success ? "true" : "false");
            var arg = request.Url.ToString().Split('?').First();
            var arg2 = nameValueCollection.ToQueryString();
            return string.Format("{0}?{1}", arg, arg2);
        }

        public static string GetSuccessUrl(HttpRequestBase request)
        {
            return GenerateUrl(request, true);
        }


        public static List<CityDataModel> GetCities(Item item)
        {
            var countries = new List<CityDataModel>();
            if (item != null && item.Children.Any())
            {
                foreach (var country in item.Children.Where(t => t.TemplateID == AccountTemplate.Account.City.ID))
                {
                    countries.Add(new CityDataModel
                    {
                        ID = country[AccountTemplate.Account.City.Fields.ID],
                        CityName = country[AccountTemplate.Account.City.Fields.CityName],
                        CityDisplayName = country[AccountTemplate.Account.City.Fields.CityDisplayName],
                    });
                }
            }
            return countries;
        }
        public static List<LanguageModel> GetLanguages(ID languageFolder)
        {
            var item = Context.Database.GetItem(languageFolder);
            var languages = new List<LanguageModel>();
            if (item != null && item.Children.Any())
            {
                foreach (var country in item.Children.Where(t => t.TemplateID == AccountTemplate.Account.Language.ID))
                {
                    languages.Add(new LanguageModel
                    {
                        Name = country[AccountTemplate.Account.Language.Fields.Name],
                        DisplayName = country[AccountTemplate.Account.Language.Fields.DisplayName],
                        ISOCode = country[AccountTemplate.Account.Language.Fields.ISOCode],
                    });
                }
            }
            return languages;
        }
        public static List<CountryModel> GetCountries(Item item)
        {
            var countries = new List<CountryModel>();
            if (item != null && item.Children.Any())
            {
                foreach (var country in item.Children.Where(t => t.TemplateID == AccountTemplate.Account.Country.Template))
                {
                    countries.Add(new CountryModel
                    {
                        Code = country[AccountTemplate.Account.Country.Fields.Code],
                        Name = country[AccountTemplate.Account.Country.Fields.Name],
                        Image = GetImageUrl(country, AccountTemplate.Account.Country.Fields.Flag)
                    });
                }
            }
            return countries;
        }

        public static string GetImageUrl(Item currentItem, ID fieldId)
        {
            var imageUrl = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = currentItem.Fields[fieldId];
            if (imageField?.MediaItem != null)
            {
                var image = new MediaItem(imageField.MediaItem);
                imageUrl = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(image));
            }
            return imageUrl;
        }
        public static List<LookupItem> GetLookupItems(Item item)
        {
            var lst = new List<LookupItem>();
            if (item != null)
                foreach (Item child in item.Children)
                    lst.Add(new LookupItem { Key = child["Key"], Phrase = child["Phrase"] });
            return lst;
        }
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email)) return false;
            Regex emailRegex = new Regex(@"^\b[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b$");
            Match match = emailRegex.Match(email);
            return match.Success;
        }
        public static bool IsValidPassword(string password)
        {
            if (string.IsNullOrEmpty(password)) return false;
            Regex emailRegex = new Regex(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w~@#$%^&*+=`|{}:;!.?\""()\[\] -]{8,16}$");
            Match match = emailRegex.Match(password);
            return match.Success;
        }

        public static Item GetCoreDataBaseItem(string path)
        {
            using (new SecurityDisabler())
            {
                return Database.GetDatabase(Settings.ProfileItemDatabase)?.GetItem(path);
            }
        }
        public static bool AsBoolean(this string value)
        {
            var strValue = value.ToLowerInvariant().Trim();
            switch (strValue)
            {
                case "false":
                case "no":
                case "0":
                default:
                    return false;

                case "true":
                case "yes":
                case "1":
                    return true;
            }

        }
        public static string GetFriendlyUrl(this Item item, ID fieldId)
        {
            LinkField linkField = item.Fields[fieldId];
            return linkField.GetFriendlyUrl();
        }
    }
}
