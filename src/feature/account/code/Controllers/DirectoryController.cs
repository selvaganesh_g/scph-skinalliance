﻿using Loreal.Feature.Account.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Loreal.Feature.Account.Controllers
{
    public class DirectoryController : StandardController
    {
        private readonly IDirectoryRepository _directoryRepository;
        public DirectoryController(IDirectoryRepository directoryRepository)
        {
            this._directoryRepository = directoryRepository;
        }

        protected override object GetModel()
        {
            return this._directoryRepository.GetModel();
        }

        public ActionResult Filter(List<string> countries, List<string> medicalSpecialties)
        {
            var model = this._directoryRepository.Filter(countries, medicalSpecialties);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProfileByUserName(string id)
        {
            var model = this._directoryRepository.ProfileById(id);
            Session["uname"] = model.FullName;
            return PartialView("~/Views/SkinAlliance/Feature/Account/UserProfile.cshtml", model);
        }

        protected override string GetIndexViewName()
        {
            return "~/views/SkinAlliance/feature/account/directory.cshtml";
        }
    }
}
