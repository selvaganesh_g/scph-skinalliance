﻿using Loreal.Feature.Account.Repositories;
using Loreal.Feature.Account.Services;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System.Web.Mvc;

namespace Loreal.Feature.Account.Controllers
{
    public class MiniProfileController : StandardController
    {
        public IMiniProfileRepository _iMiniProfileRepository { get; set; }
        public IProfileService _iprofileService { get; set; }
        public MiniProfileController(IMiniProfileRepository miniProfileRepository, IProfileService profileService)
        {
            _iMiniProfileRepository = miniProfileRepository;
            _iprofileService = profileService;
        }

        [HttpPost]
        public ActionResult Accepted()
        {
            try
            {
                if (Sitecore.Context.User != null && Request.Form["Accepted"] != null && Request.Form["Accepted"].ToLower() == "on")
                {
                    _iprofileService.SetAcceptedValue(Sitecore.Context.User?.Profile, true);
                }
                return Redirect(Request.UrlReferrer .Equals(Request.Url) ? Request.Url.AbsoluteUri : Request.UrlReferrer.AbsoluteUri);
            }
            catch
            {
                return Redirect(Request.Url.AbsoluteUri);
            }
        }
        protected override string GetIndexViewName()
        {
            return "~/views/SkinAlliance/feature/account/MiniProfile.cshtml";
        }
        protected override object GetModel()
        {
            return _iMiniProfileRepository.GetModel();
        }
    }
}