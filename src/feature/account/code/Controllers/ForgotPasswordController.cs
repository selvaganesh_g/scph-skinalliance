﻿using Loreal.Feature.Account.Extensions;
using Loreal.Feature.Account.Models;
using Loreal.Feature.Account.Repositories;
using Loreal.Feature.Account.Services;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.Mvc.Presentation;
using Sitecore.SecurityModel;
using Sitecore.XA.Feature.Security.Attributes;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace Loreal.Feature.Account.Controllers
{
    public class ForgotPasswordController : StandardController
    {
        public IForgotPasswordRepository _forgotpasswordRepository;

        public ForgotPasswordController(IForgotPasswordRepository forgotpasswordRepository)
        {
            _forgotpasswordRepository = forgotpasswordRepository;

        }
        public override ActionResult Index()
        {
            if (User != null && User.Identity != null && User.Identity.IsAuthenticated) return Redirect("~/");
            return PartialView(this.GetIndexViewName(), this.GetModel());
        }

        protected override object GetModel()
        {
            return _forgotpasswordRepository.GetModel();
        }
        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ResetPassword(string userName)
        {
            using (new SecurityDisabler())
            {
                var model = new ResetPasswordViewModel();
                model.UserName = userName;
                var dataSource = Rendering.DataSourceItem;
                if (dataSource != null)
                {
                    model.Title = Context.Item.Fields["Title"]?.Value;
                    model.NewPasswordRequired = dataSource.Fields[AccountTemplate.Account.MyProfile.ChangePassword.Fields.NewPasswordRequired]?.Value;
                    model.NewPasswordCaption = dataSource.Fields[AccountTemplate.Account.MyProfile.ChangePassword.Fields.NewPasswordCaption]?.Value;
                    model.ConfirmPasswordRequired = dataSource.Fields[AccountTemplate.Account.MyProfile.ChangePassword.Fields.ConfirmPasswordRequired]?.Value;
                    model.ConfirmPasswordCaption = dataSource.Fields[AccountTemplate.Account.MyProfile.ChangePassword.Fields.ConfirmPasswordCaption]?.Value;
                    model.ConfirmPasswordIncorrect = dataSource.Fields[AccountTemplate.Account.MyProfile.ChangePassword.Fields.ConfirmPasswordIncorrect]?.Value;

                }
                model.SaveButtonText = Translate.Text(AccountTemplate.ResetPasswordButtonText);

                return PartialView("~/views/skinalliance/feature/account/ResetPassword.cshtml", model);
            }
        }
        [HttpPost]
        public ActionResult ResetPassword(string newPassword, string userName)
        {
            using (new SecurityDisabler())
            {
                var user = Sitecore.Security.Accounts.User.FromName(userName, true);
                var token = PasswordRecoveryHelper.GetToken(user);
                if (!string.IsNullOrEmpty(token) && PasswordRecoveryHelper.IsResetPasswordTimeExpired(token))
                {
                    MembershipUser m_user = Membership.GetUser(userName);

                    if (m_user.IsLockedOut)
                        m_user.UnlockUser();

                    m_user.ChangePassword(m_user.ResetPassword(), newPassword);

                    FormsAuthentication.SetAuthCookie(userName, true);
                    PasswordRecoveryHelper.DeleteToken(user);
                }
                return Redirect("~/");
            }
        }

        [LoginAntiforgeryHandleError, HttpPost, ValidateAntiForgeryToken]
        public ActionResult SendPassword(ForgotPasswordViewModel model)
        {
            try
            {
                if (_forgotpasswordRepository.Exists(model.Email))
                {
                    //string userName;
                    //var password = _forgotpasswordRepository.GetPassword(email, out userName);
                    //EmailService.Services.EmailService.SendForgotpassword(userName, email, password);

                    var user = Sitecore.Security.Accounts.User.FromName(Context.Domain.GetFullName(model.Email), true);

                    string time = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                    var token = string.Concat(Sitecore.Data.ID.NewID.ToShortID().ToString(), "|", time);

                    string userName = user?.Profile.UserName;
                    var confirmLink = PasswordRecoveryHelper.GenerateConfirmLink(token, userName);
                    PasswordRecoveryHelper.StoreTokenOnUser(user, token);
                    var dataSource = Context.Database.GetItem(model.DataSource);
                    model.EmailSubject = dataSource.Fields[AccountTemplate.Account.ForgotPassword.Fields.EmailSubject]?.Value;
                    model.EmailTemplate = dataSource.Fields[AccountTemplate.Account.ForgotPassword.Fields.EmailTemplate]?.Value;
                    string fullName = user?.Profile.FullName;
                    string message = model.EmailTemplate.Replace("#resetlink#", confirmLink).Replace("#username#", fullName);
                    string subject = model.EmailSubject.Replace("#username", fullName);

                    EmailService.Services.EmailService.SendMail(model.Email, subject, message);

                    return this.Redirect(this._forgotpasswordRepository.GetRedirectUrl(base.HttpContext.Request, true));

                }
                return this.Redirect(this._forgotpasswordRepository.GetRedirectUrl(base.HttpContext.Request, false));
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return this.Redirect(this._forgotpasswordRepository.GetRedirectUrl(base.HttpContext.Request, false));
            }
        }
        protected override string GetIndexViewName()
        {
            return "~/views/skinalliance/feature/account/ForgotPassword.cshtml";
        }
    }
}