﻿using Loreal.Feature.Account.Models;
using Loreal.Feature.Account.Repositories;
using Loreal.Feature.Account.Services;
using Loreal.Feature.Account.Utilities;
using Sitecore.Diagnostics;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Loreal.Feature.Account.Controllers
{
    public class ProfileController : StandardController
    {
        private readonly IProfileRepository _profielRepository;
        private readonly IProfileService _profielService;
        private readonly ITrackingService _trackingService;

        public ProfileController(IProfileRepository profileRepository, IProfileService profileSerivce, ITrackingService trackingService)
        {
            _profielRepository = profileRepository;
            _profielService = profileSerivce;
            _trackingService = trackingService;
        }

        protected override object GetModel()
        {
            return _profielRepository.GetModel();
        }

        public override ActionResult Index()
        {
            return View(this.GetIndexViewName(), this.GetModel());
        }

        [HttpPost]
        public ActionResult UpdateUser(SignupViewModel model)
        {
            try
            {
                ModelState.Remove(nameof(model.Password));
                ModelState.Remove(nameof(model.ConfirmPassword));
                var profile = Sitecore.Security.Accounts.User.Current?.Profile;
                if (!string.IsNullOrEmpty(model.Summary))
                {
                    model.Summary = System.Web.HttpUtility.UrlDecode(model.Summary);
                }
                if (!profile.Email.Equals(model.Email))
                {
                    return this.Redirect(this._profielRepository.GetRedirectUrl(base.HttpContext.Request, false));
                }
                if (!base.ModelState.IsValid)
                {
                    return this.PartialView(this.GetIndexViewName(), this.GetModel());
                }
                if (Request.Files["resume"] != null && Request.Files["resume"].ContentLength > 0)
                {
                    model.Resume = this._profielService.UploadResume(profile, false, Request.Files["resume"]);
                }

                this._profielRepository.UpdateUser(model);

                if (Request.Files["preview"] != null && Request.Files["preview"].ContentLength > 0)
                {
                    this._profielService.SetProfileImage(Sitecore.Security.Accounts.User.Current?.Profile, false, Request.Files["preview"].InputStream);
                }
                _trackingService.IdentifyContact(model.Email, model);

                return this.Redirect(this._profielRepository.GetRedirectUrl(base.HttpContext.Request, true));
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, this);
                return this.Redirect(this._profielRepository.GetRedirectUrl(base.HttpContext.Request, false));
            }
        }

        [HttpPost]
        public ActionResult ChangePassword(string password, string newPassword)
        {
            if (!string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(newPassword))
            {
                return Json(new { Success = _profielRepository.ChangePassword(password, newPassword) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Account/MyProfile.cshtml";
        }

        public ActionResult ImportUser(HttpPostedFileBase file)
        {
            var model = new UserEntity();
            model.Status = false;
            if (file != null && file.ContentLength > 0)
            {
                // get contents to string
                using (var pck = new OfficeOpenXml.ExcelPackage())
                {
                    pck.Load(file.InputStream);
                    var ws = pck.Workbook.Worksheets.First();
                    IEnumerable<UserEntity> users = UserManager.PopulateUsers(ws, true);

                    string errorLogs = UserManager.CreateUsers(users.ToList());
                    model.ErrorLogs = errorLogs;
                    model.Status = true;
                }
            }
            return View("~/Views/SkinAlliance/Feature/Account/UserImport.cshtml", model);
        }
    }
}