﻿using Facebook;
using Loreal.Feature.Account.Extensions;
using Loreal.Feature.Account.Models;
using Loreal.Feature.Account.Repositories;
using Loreal.Feature.Account.Services;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.XA.Foundation.Mvc.Controllers;
using Sparkle.LinkedInNET;
using Sparkle.LinkedInNET.Profiles;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Tweetinvi;

namespace Loreal.Feature.Account.Controllers
{
    public class SocialConnectorController : StandardController
    {
        public ISocialConnectorRepository _iSocialConnectorRepository;
        public ISignupRepository _signUpRepository;
        public ITrackingService _trackingService;

        public SocialConnectorController(ISocialConnectorRepository isocialConnectorRepository, ISignupRepository signUpRepository, ITrackingService trackingService)
        {
            _iSocialConnectorRepository = isocialConnectorRepository;
            _signUpRepository = signUpRepository;
            _trackingService = trackingService;
        }

        public static Item SettingsItem
        {
            get
            {
                using (new SecurityDisabler())
                {
                    if (ServiceLocator.Current.Resolve<IMultisiteContext>().SettingsItem == null)
                    {
                        return ServiceLocator.Current.Resolve<IMultisiteContext>()?
                            .GetSettingsItem(Context.Database.GetItem(Context.Site.StartPath));
                    }
                    else
                    {
                        return ServiceLocator.Current.Resolve<IMultisiteContext>().SettingsItem;
                    }
                }
            }
        }

        protected Item SocialConnectorItem
        {
            get
            {
                using (new SecurityDisabler())
                {
                    Item item = SettingsItem.Children.
                    FirstOrDefault(x => x.TemplateID == AccountTemplate.Account.SocialConnector.ID);
                    if (item != null)
                    {
                        return item;
                    }
                }
                return null;
            }
        }

        public string GetItemValue(ID field)
        {
            using (new SecurityDisabler())
            {
                return this.SocialConnectorItem?.GetFieldValue(field);
            }
        }

        protected override object GetModel()
        {
            var model = _iSocialConnectorRepository.GetSocialModel(Request.Url.GetLeftPart(UriPartial.Path));
            var signupModel = _signUpRepository.GetModel();
            return model;
        }

        protected override string GetIndexViewName()
        {
            return "~/views/SkinAlliance/feature/account/SocialConnecter.cshtml";
        }

        public override ActionResult Index()
        {
            try
            {
                var model = (SocialConnectorViewModel)_iSocialConnectorRepository.GetSocialModel(Request.Url.GetLeftPart(UriPartial.Path));
                var signupModel = (SignupViewModel)_signUpRepository.GetModel();
                if (model == null || signupModel == null)
                {
                    RedirectToAction(Request.Url.AbsoluteUri);
                }
                /// 1. Get Facebook data if query string contains the code alone.
                if (Request.QueryString.AllKeys.Contains("code") && !Request.QueryString.AllKeys.Contains("state"))
                {
                    GetFacebookProfileData(Request.QueryString["code"], model, signupModel);
                }
                /// 2. Get Twitter data if query string contains the authorization_id and oauth_verifier
                else if (Request.QueryString.AllKeys.Contains("authorization_id") && Request.QueryString.AllKeys.Contains("oauth_verifier"))
                {
                    GetTwitterProfileData(Request.QueryString["authorization_id"], Request.QueryString["oauth_verifier"], model, signupModel);
                }/// 3. Get LinkedIn data if query string contains the code and State
                else if (Request.QueryString.AllKeys.Contains("code") && Request.QueryString.AllKeys.Contains("state"))
                {
                    var api = new LinkedInApi(new LinkedInApiConfiguration(model.LinkedInAppID, model.LinkedInSeceretKey));
                    var userToken = api.OAuth2.GetAccessToken(Request.QueryString["code"], Request.Url.GetLeftPart(UriPartial.Path));
                    var user = new UserAuthorization(userToken.AccessToken);
                    var profile = api.Profiles.GetMyProfile(user, new string[] { "en" }, FieldSelector.For<Person>().WithFirstName().WithLastName().WithEmailAddress());
                    GetLinkedInData(profile, signupModel);
                }
                else
                {
                    return View(this.GetIndexViewName(), this.GetModel());
                }
                ///Auto login and redirect to home page if the social login user already register.
                if (_signUpRepository.Exists(signupModel.Email))
                {
                    FormsAuthentication.SetAuthCookie(Context.Domain.GetFullName(signupModel.Email), true);
                    _trackingService.IdentifyContact(signupModel.Email, signupModel);
                    if (!string.IsNullOrEmpty(Request.QueryString["returnUrl"]))
                    {
                        return Redirect(Url.Content(string.Format("~/{0}", Request.QueryString["returnUrl"])));
                    }
                    return Redirect(Url.Content("~/"));
                }
                ///Set captured user data in session variable to register.
                Session["sa_signupsocial"] = signupModel;
                return Redirect(model.RegisterURL);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return Redirect(string.Format("{0}?success=false", Request.Url.AbsolutePath));
            }
        }

        public ActionResult Register()
        {
            if (Session["sa_signupsocial"] == null)
            {
                return Redirect(Url.Content("~/"));
            }
            var model = (SignupViewModel)Session["sa_signupsocial"];
            if (Session["sa_signupsocial_modelstate"] != null && (bool)Session["sa_signupsocial_modelstate"] == true)
            {
                if (string.IsNullOrEmpty(model.Country)) base.ModelState.AddModelError(nameof(model.Country), model.CountryRequired);
                if (string.IsNullOrEmpty(model.MedicalSpecialty)) base.ModelState.AddModelError(nameof(model.MedicalSpecialty), model.MedicalSpecialtyRequired);
                if (string.IsNullOrEmpty(model.PhysicianLicenseNumber)) base.ModelState.AddModelError(nameof(model.PhysicianLicenseNumber), model.PhysicianLicenseNumberRequired);
            }
            return this.PartialView("~/Views/SkinAlliance/Feature/Account/SignupSocial.cshtml", model);
        }

        [HttpPost]
        public ActionResult Register(SignupViewModel model)
        {
            try
            {
                base.ModelState.Remove(nameof(model.ConfirmPassword));
                base.ModelState.Remove(nameof(model.Password));
                var resultModel = (SignupViewModel)Session["sa_signupsocial"];
                if (!resultModel.PhysicianNumberIsRequired)
                {
                    ModelState.Remove(nameof(model.PhysicianLicenseNumber));
                }
                if (!base.ModelState.IsValid)
                {
                    var modelUpdate = SetModel(model, resultModel);
                    Session["sa_signupsocial"] = modelUpdate;
                    Session["sa_signupsocial_modelstate"] = true;
                    return Redirect(Request.Url.AbsoluteUri);
                }
                Session["sa_signupsocial_modelstate"] = false;
                if (!_signUpRepository.Exists(model.Email))
                {
                    _signUpRepository.Signup(SetModel(model, model));
                }
                FormsAuthentication.SetAuthCookie(Context.Domain.GetFullName(model.Email), true);
                _trackingService.IdentifyContact(model.Email, model);
                EmailService.Services.EmailService.SendWelcomeMail(model.FirstName, model.Email);
                return Redirect(Url.Content("~/"));
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return this.Redirect(this._signUpRepository.GetRedirectUrl(base.HttpContext.Request, false));
            }
        }

        [HttpGet]
        public ActionResult Authorize(string client_secret, string redirect_uri, string sso_time)
        {
            string failureUrl = string.Format("{0}/login?success=false", redirect_uri);
            try
            {
                if (User != null && User.Identity != null && User.Identity.IsAuthenticated)
                {
                    if (!string.IsNullOrEmpty(client_secret))
                    {
                        var profile = new ProfileService().GetProfileById(Context.User.Profile.StartUrl);
                        return this.RedirectAndPost(redirect_uri + "/salogin", Utils.ToDictionary(profile));
                    }
                }
                else
                {
                    DateTime appTime = DateTime.UtcNow;
                    int offsetTime = System.Convert.ToInt32(GetItemValue(AccountTemplate.Account.SocialConnector.Fields.OffsetTime));
                    try
                    {
                        appTime = DateTime.ParseExact(sso_time, "yyyyMMddHHmmss", null);
                    }
                    catch
                    {
                        return Redirect(failureUrl);
                    }
                    if (Math.Abs(appTime.Subtract(DateTime.UtcNow).TotalSeconds) > offsetTime)
                    {
                        return Redirect(failureUrl);
                    }
                    bool isValid = false;

                    string shared_secret_key = GetItemValue(AccountTemplate.Account.SocialConnector.Fields.SharedSecretKey);
#pragma warning disable CS0618 // Type or member is obsolete
                    string hash = FormsAuthentication.HashPasswordForStoringInConfigFile(string.Format("{0},{1}", shared_secret_key, sso_time), "md5");
#pragma warning restore CS0618 // Type or member is obsolete
                    if (string.Compare(client_secret, hash, true) == 0)
                    {
                        if (Math.Abs(appTime.Subtract(DateTime.UtcNow).TotalSeconds) > offsetTime)
                        {
                            return Redirect(failureUrl);
                        }
                        else
                        {
                            isValid = true;
                        }
                    }
                    if (isValid)
                    {
                        return Redirect(Url.Content(string.Format("~/login?{0}", Request.QueryString)));
                    }
                    else
                    {
                        return Redirect(failureUrl);
                    }
                }
                return Redirect(failureUrl);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return Redirect(failureUrl);
            }
        }

        protected SignupViewModel SetModel(SignupViewModel model, SignupViewModel modelOrg)
        {
            var modelSet = modelOrg;
            modelSet.FirstName = model.FirstName;
            modelSet.LastName = model.LastName;
            modelSet.Email = model.Email;
            modelSet.Country = model.Country;
            modelSet.PhysicianLicenseNumber = model.PhysicianLicenseNumber;
            modelSet.MedicalSpecialty = model.MedicalSpecialty;
            modelSet.Password = string.IsNullOrEmpty(model.Password) ? new Guid().ToString() : model.Password;
            modelSet.ConfirmPassword = string.IsNullOrEmpty(model.ConfirmPassword) ? modelSet.Password : model.ConfirmPassword;
            return modelSet;
        }

        protected void GetFacebookProfileData(string code, SocialConnectorViewModel socialModel, SignupViewModel signupModel)
        {
            var fb = new FacebookClient();
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = socialModel.FacebookAppID,
                client_secret = socialModel.FacebookSecertKey,
                redirect_uri = Request.Url.AbsoluteUri,
                code = code
            });
            fb.AccessToken = result.access_token;
            dynamic userInfo = fb.Get("me?fields=first_name,middle_name,last_name,id,email");
            signupModel.FirstName = userInfo.first_name;
            signupModel.LastName = userInfo.last_name;
            signupModel.Email = userInfo.email;
            signupModel.Password = new Guid().ToString();
            signupModel.ConfirmPassword = signupModel.Password;
        }

        protected void GetTwitterProfileData(string authorization_id, string oauth_verifier, SocialConnectorViewModel socialModel, SignupViewModel signupModel)
        {
            var userCreds = AuthFlow.CreateCredentialsFromVerifierCode(oauth_verifier, authorization_id);
            var user = Tweetinvi.User.GetAuthenticatedUser(userCreds);
            signupModel.FirstName = user.ScreenName;
            signupModel.LastName = user.Name;
            signupModel.Email = user.Email;
            signupModel.Password = new Guid().ToString();
            signupModel.ConfirmPassword = signupModel.Password;
        }

        protected void GetLinkedInData(Person ps, SignupViewModel signupModel)
        {
            signupModel.FirstName = ps.Firstname;
            signupModel.LastName = ps.Lastname;
            signupModel.Email = ps.EmailAddress;
            signupModel.Password = new Guid().ToString();
            signupModel.ConfirmPassword = signupModel.Password;
        }
    }
}