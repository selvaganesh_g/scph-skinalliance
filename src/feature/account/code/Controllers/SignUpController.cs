﻿namespace Loreal.Feature.Account.Controllers
{

    using Models;
    using Repositories;
    using Services;
    using Sitecore.XA.Foundation.Mvc.Controllers;
    using System;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Register controller
    /// </summary>

    public class SignupController : StandardController
    {
        public ISignupRepository SignupRepository { get; set; }
        public ITrackingService _trackingService { get; set; }
        public SignupController(ISignupRepository signupRepository, ITrackingService trackingService)
        {
            this.SignupRepository = signupRepository;
            this._trackingService = trackingService;
        }
        public ActionResult Signup()
        {
            if (User != null && User.Identity != null && User.Identity.IsAuthenticated) return Redirect("~/");
            if (base.Rendering.DataSourceItem != null)
            {
                return this.PartialView(this.GetIndexViewName(), this.GetModel());
            }
            return null;
        }
        [HttpPost]
        public ActionResult Signup(SignupViewModel model)
        {
            try
            {
                var validateModel = ValidateModel(model);
                if (!base.ModelState.IsValid)
                {
                    return this.PartialView(this.GetIndexViewName(), SetModel(model, validateModel));
                }

                if (this.SignupRepository.Exists(model.Email))
                {
                    validateModel = SetModel(model, (SignupViewModel)this.GetModel());
                    validateModel.EmailRequired = validateModel.UserExistText;
                    this.ModelState.AddModelError(nameof(model.Email), model.EmailRequired);
                    return this.PartialView(this.GetIndexViewName(), validateModel);
                }
                this.SignupRepository.Signup(model);
                HttpCookie cookie = new HttpCookie("com-email-auto");
                cookie.HttpOnly = true;
                cookie.Values.Add("com-email", model.Email);
                cookie.Expires.AddMinutes(10);
                Response.Cookies.Add(cookie);
                _trackingService.IdentifyContact(model.Email,model);
                EmailService.Services.EmailService.SendWelcomeMail(model.FirstName, model.Email);
                return this.Redirect(this.SignupRepository.GetRedirectUrl(base.HttpContext.Request, true));
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, this);
                return this.Redirect(this.SignupRepository.GetRedirectUrl(base.HttpContext.Request, false));
            }
        }

        public ActionResult Import(bool live = false)
        {
            Utilities.Import.ImportUser(Server.MapPath("~/sausers.json"), live);
            return Json(new object(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ImportPost()
        {
            Utilities.Import.ImportPost(Server.MapPath("~/saposts.json"));
            return Json(new object(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Importfollow()
        {
            Utilities.Import.ImportFollow(Server.MapPath("~/sausers.json"), Server.MapPath("~/safollow.json"), Request.Url.GetLeftPart(UriPartial.Authority) + "/community/directory/userprofile");
            return Json(new object(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ImportLike()
        {
            Utilities.Import.ImportLikes(Server.MapPath("~/saposts.json"), Server.MapPath("~/salike.json"));
            return Json(new object(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ImportComment()
        {
            Utilities.Import.ImportComments(Server.MapPath("~/sausers.json"), Server.MapPath("~/saposts.json"), Server.MapPath("~/sacomment.json"), Request.Url.GetLeftPart(UriPartial.Authority) + "/community/directory/userprofile");
            return Json(new object(), JsonRequestBehavior.AllowGet);
        }

        protected override object GetModel()
        {
            return this.SignupRepository.GetModel();
        }
        protected override string GetIndexViewName()
        {
            return "~/views/SkinAlliance/feature/account/signup.cshtml";
        }
        protected SignupViewModel ValidateModel(SignupViewModel model)
        {
            SignupViewModel resultModel = (SignupViewModel)this.GetModel();

            if (!resultModel.PhysicianNumberIsRequired)
            {
                ModelState.Remove(nameof(model.PhysicianLicenseNumber));
            }
            if (!string.IsNullOrEmpty(model.Password) && !Utils.IsValidPassword(model.Password))
            {
                resultModel.PasswordRequired = resultModel.InvalidPassword;
                this.ModelState.AddModelError(nameof(resultModel.Password), resultModel.PasswordRequired);
            }
            if (!string.IsNullOrEmpty(model.Password) && !this.ModelState.ContainsKey(nameof(model.ConfirmPassword)) && !Utils.IsValidPassword(model.ConfirmPassword))
            {
                resultModel.ConfirmPasswordRequired = resultModel.InvalidPassword;
                this.ModelState.AddModelError(nameof(resultModel.ConfirmPassword), resultModel.PasswordRequired);
            }
            if (!string.IsNullOrEmpty(model.Password) && !String.IsNullOrEmpty(model.ConfirmPassword) && !this.ModelState.ContainsKey(nameof(model.ConfirmPassword)) && !model.Password.Equals(model.ConfirmPassword))
            {
                resultModel.ConfirmPasswordRequired = resultModel.PasswordMismatch;
                this.ModelState.AddModelError(nameof(resultModel.ConfirmPassword), resultModel.ConfirmPasswordRequired);
            }

            if (!string.IsNullOrEmpty(model.Email) && !Utils.IsValidEmail(model.Email))
            {
                resultModel.EmailRequired = resultModel.InvalidEmail;
                this.ModelState.AddModelError(nameof(resultModel.Email), resultModel.EmailRequired);
            }
            return resultModel;
        }
        protected SignupViewModel SetModel(SignupViewModel model, SignupViewModel modelOrg)
        {
            var modelSet = modelOrg;
            modelSet.FirstName = model.FirstName;
            modelSet.LastName = model.LastName;
            modelSet.Email = model.Email;
            modelSet.Country = model.Country;
            modelSet.PhysicianLicenseNumber = model.PhysicianLicenseNumber;
            modelSet.MedicalSpecialty = model.MedicalSpecialty;
            modelSet.Password = model.Password;
            modelSet.ConfirmPassword = model.ConfirmPassword;
            modelSet.City = model.City;
            modelSet.ZipCode = model.ZipCode;
            modelSet.PreferredLanguage = model.PreferredLanguage;
            return modelSet;
        }

    }
}