﻿using System.Web.Configuration;

namespace Loreal.Feature.Account.Controllers
{
    using Models;
    using Repositories;
    using Services;
    using Sitecore;
    using Sitecore.XA.Feature.Security.Attributes;
    using Sitecore.XA.Foundation.Mvc.Controllers;
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;

    /// <summary>
    /// Signin Controller.
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />

    public class SALogoutController : Controller
    {
        /// <summary>
        /// Logs the off.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LogOff()
        {
            var logoutCookie = new HttpCookie("Login") { Expires = DateTime.Now.AddDays(-1d) };
            Response.Cookies.Add(logoutCookie);

            FormsAuthentication.SignOut();
            Session.Abandon();

            var cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "") { Expires = DateTime.Now.AddDays(-1d) };
            Response.Cookies.Add(cookie1);

            var sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            if (sessionStateSection != null)
            {
                var cookie2 = new HttpCookie(sessionStateSection.CookieName, "") { Expires = DateTime.Now.AddDays(-1d) };
                Response.Cookies.Add(cookie2);
            }


            return Redirect(Request.Url.GetLeftPart(UriPartial.Authority));
        }

    }
}