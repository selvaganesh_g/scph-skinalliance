﻿namespace Loreal.Feature.Account.Controllers
{
    using Loreal.Feature.Account;
    using Loreal.Feature.Account.Extensions;
    using Models;
    using Repositories;
    using Services;
    using Sitecore;
    using Sitecore.XA.Feature.Security.Attributes;
    using Sitecore.XA.Foundation.Mvc.Controllers;
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;

    /// <summary>
    /// Signin Controller.
    /// </summary>
    /// 

    public class SignInController : StandardController
    {
        public ISignInRepository _signInRepository { get; set; }
        public ITrackingService _trackingService { get; set; }
        public SignInController(ISignInRepository signInRepository, ITrackingService trackingService)
        {
            this._signInRepository = signInRepository;
            _trackingService = trackingService;
        }
        public ActionResult Login()
        {
            if (User != null && User.Identity != null && User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["returnUrl"]))
                {
                    return Redirect(Url.Content(string.Format("~/{0}", Request.QueryString["returnUrl"])));
                }
                return Redirect("~/");
            }
            if (Request.QueryString.AllKeys.Contains("redirect_uri"))
            {
                ViewBag.RedirectUrl = Request.QueryString.Get("redirect_uri");
            }
            if (base.Rendering.DataSourceItem != null)
            {
                var model = (SignInViewModel)this.GetModel();
                if (Request.Cookies["Login"] != null)
                {
                    model.Username = Request.Cookies["Login"].Values["Username"];
                    model.Password = Request.Cookies["Login"].Values["Password"];
                    model.RememberMe = false;
                    var accountName = string.Empty;
                    var domain = Context.Domain;
                    if (domain != null)
                    {
                        accountName = domain.GetFullName(model.Username);
                        FormsAuthentication.SetAuthCookie(accountName, true);
                        _trackingService.IdentifyContact(model.Username, null);

                        if (!string.IsNullOrEmpty(Request.QueryString["returnUrl"]))
                        {
                            return Redirect(Url.Content(string.Format("~/{0}", Request.QueryString["returnUrl"])));
                        }
                        return Redirect(Url.Content("~/"));
                    }
                }
                return this.PartialView(this.GetIndexViewName(), model);
            }
            return null;
        }
        [LoginAntiforgeryHandleError, HttpPost, ValidateAntiForgeryToken]
        public ActionResult Login(SignInViewModel model, string redirect_uri)
        {
            bool isValid = true;
            if (!string.IsNullOrEmpty(model.Username))
            {
                isValid = Utils.IsValidEmail(model.Username);
            }
            if (!base.ModelState.IsValid || !isValid)
            {
                SignInViewModel signViewModel = (SignInViewModel)this.GetModel();
                if (!isValid)
                {
                    signViewModel.UsernameRequired = signViewModel.InvalidUsername;
                    this.ModelState.AddModelError(nameof(model.Username), signViewModel.UsernameRequired);
                }
                return this.PartialView(this.GetIndexViewName(), signViewModel);
            }
            var accountName = string.Empty;
            var domain = Context.Domain;
            if (domain != null) accountName = domain.GetFullName(model.Username);
            if (base.ModelState.IsValid && this._signInRepository.Login(accountName, model.Password, model.RememberMe))
            {
                if (model.RememberMe)
                {
                    HttpCookie cookie = new HttpCookie("Login");
                    cookie.HttpOnly = true;
                    cookie.Values.Add("Username", model.Username);
                    cookie.Values.Add("Password", model.Password);
                    cookie.Expires = DateTime.Now.AddDays(15);
                    Response.SetCookie(cookie);
                    FormsAuthentication.SetAuthCookie(accountName, true);
                }
                if (!string.IsNullOrEmpty(redirect_uri))
                {
                    var profile = new ProfileService().GetProfileById(Context.User.Profile.StartUrl);

                    return this.RedirectAndPost(redirect_uri + "/salogin", Utils.ToDictionary(profile));
                }
                _trackingService.IdentifyContact(model.Username, null);
                if (!string.IsNullOrEmpty(Request.QueryString["returnUrl"]))
                {
                    return Redirect(Url.Content(string.Format("~/{0}", Request.QueryString["returnUrl"])));
                }
                return this.Redirect(this._signInRepository.GetRedirectUrl(base.HttpContext.Request, true));
            }

            if (!string.IsNullOrEmpty(redirect_uri))
            {
                return Redirect(redirect_uri);
            }

            return this.Redirect(this._signInRepository.GetRedirectUrl(base.HttpContext.Request, false));
        }
        protected override object GetModel()
        {
            return this._signInRepository.GetModel();
        }

        [HttpGet]
        public ActionResult GetDiscoverDermocosmeticsScience()
        {
            var model = this._signInRepository.GetDDSModel();
            return View("~/views/SkinAlliance/feature/account/DiscoverDermocosmeticsScience.cshtml", model);
        }

        protected override string GetIndexViewName()
        {
            return "~/views/SkinAlliance/feature/account/signin.cshtml";
        }
    }
}