﻿using Loreal.Feature.Account.Extensions;
using Sitecore.SecurityModel;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System.Web.Mvc;

namespace Loreal.Feature.Account.Controllers
{
    public class ConfirmRecoveryController : StandardController
    {
        [HttpGet]
        public ActionResult Confirm(string userName, string token)
        {
            using (new SecurityDisabler())
            {
                userName = userName.Replace('|', '\\');
                var user = Sitecore.Security.Accounts.User.FromName(userName, false);

                var m_user = System.Web.Security.Membership.GetUser(userName, true);

                if (user == null || !m_user.IsApproved || !PasswordRecoveryHelper.TokenIsValid(user, token) || !PasswordRecoveryHelper.IsResetPasswordTimeExpired(token))
                {
                    return Redirect(Url.Content("/"));
                }
                else
                {
                    return Redirect(string.Format("/reset-password?userName={0}", userName));
                }
            }
        }
    }
}