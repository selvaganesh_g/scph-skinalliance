﻿using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.Mvc.Presentation;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using System.Web.Mvc;
using System.Web.Security;

namespace Loreal.Feature.Account.Controllers
{

    public class ThankyouRegisterController : Controller
    {
        public ActionResult Index()
        {
            if (User != null && User.Identity != null && User.Identity.IsAuthenticated) return Redirect("~/");
            return PartialView(this.GetIndexViewName(), Sitecore.Context.Database.GetItem(RenderingContext.Current.Rendering.DataSource));
        }
        protected string GetIndexViewName()
        {
            return "~/views/skinalliance/feature/account/ThanksYouForReg.cshtml";
        }

        [HttpPost]
        public ActionResult CompleteProfile()
        {

            if (Request.Cookies["com-email-auto"] != null)
            {
                var autoEmail = Request.Cookies["com-email-auto"].Values["com-email"];
                if (!string.IsNullOrEmpty(autoEmail))
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        FormsAuthentication.SetAuthCookie(Context.Domain.GetFullName(autoEmail), true);
                        LinkField linkField = ServiceLocator.Current.Resolve<IMultisiteContext>().SettingsItem.Fields[AccountTemplate.Settings.MyProfileUrl];
                        if (linkField != null && !string.IsNullOrEmpty(linkField.GetFriendlyUrl()))
                        {
                            Request.Cookies.Remove("com-email-auto");
                            return Redirect(string.Format("{0}?pcom=true", linkField.GetFriendlyUrl()));
                        }
                    }

                }
            }
            return Redirect("~/");
        }

    }
}