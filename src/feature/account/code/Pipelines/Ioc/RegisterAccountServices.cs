﻿
namespace Loreal.Feature.Account.Pipelines.Ioc
{
    using Microsoft.Extensions.DependencyInjection;
    using Sitecore.XA.Foundation.IOC.Pipelines.IOC;
    using Repositories;
    using Services;

    public class RegisterAccountServices : IocProcessor
    {
        public override void Process(IocArgs args)
        {
            args.ServiceCollection.AddTransient<ISignInRepository, SignInRepository>();
            args.ServiceCollection.AddTransient<ISignupRepository, SignupRepository>();
            args.ServiceCollection.AddTransient<IProfileService, ProfileService>();
            args.ServiceCollection.AddTransient<IProfileRepository, ProfileRepository>();
            args.ServiceCollection.AddTransient<IMiniProfileRepository, MiniProfileRepository>();
            args.ServiceCollection.AddTransient<ISocialConnectorRepository, SocialConnectorRepository>();
            args.ServiceCollection.AddTransient<IDirectoryRepository, DirectoryRepository>();
            args.ServiceCollection.AddTransient<ITrackingService, TrackingService>();
            args.ServiceCollection.AddTransient<IForgotPasswordRepository, ForgotPasswordRepository>();
        }
    }
}