﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Account.Models
{
    public class DiscoverDermocosmeticsScienceModel
    {
        public string Title { get; set; }
        public HtmlString Description { get; set; }
        public List<QuickLinkModel> QuickLinks { get; set; }
    }
}