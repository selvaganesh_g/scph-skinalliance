﻿using System.ComponentModel.DataAnnotations;
using Sitecore.XA.Foundation.Mvc.Models;

namespace Loreal.Feature.Account.Models
{
    public class SignInModel : RenderingModelBase
    {
        [DataType(DataType.EmailAddress)]
        [Required]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }
    }
}