﻿namespace Loreal.Feature.Account.Models
{
  public class CountryModel
  {
    public string Code { get; set; }
    public string Name { get; set; }
    public string Image { get; set; }
  }
}