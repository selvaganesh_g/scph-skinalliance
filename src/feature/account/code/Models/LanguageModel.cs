﻿namespace Loreal.Feature.Account.Models
{
    public class LanguageModel
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string ISOCode { get; set; }
    }
}