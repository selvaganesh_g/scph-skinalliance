﻿using Sitecore.XA.Foundation.Mvc.Models;

namespace Loreal.Feature.Account.Models
{
    public class SocialConnectorViewModel : RenderingModelBase
    {

        public string FacebookLink { get; set; }
        public string FacebookText { get; set; }
        public string TwitterLink { get; set; }
        public string TwitterText { get; set; }
        public string LinkedInLink { get; set; }
        public string LinkedInText { get; set; }
        public string SeparatorText { get; set; }
        public string FacebookAppID { get; set; }
        public string FacebookSecertKey { get; set; }
        public string TwitterAppID { get; set; }
        public string TwitterSeceretKey { get; set; }
        public string LinkedInAppID { get; set; }
        public string LinkedInSeceretKey { get; set; }
        public string RegisterURL { get; set; }
    }
}