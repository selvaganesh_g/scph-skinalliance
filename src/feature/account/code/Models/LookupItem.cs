﻿namespace Loreal.Feature.Account.Models
{
  public class LookupItem
  {
    public string Key { get; set; }
    public string Phrase { get; set; }
  }
}