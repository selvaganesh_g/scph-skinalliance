﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Loreal.Feature.Account.Models
{
    public class SignupViewModel : SignupModel
    {
        public string Introduction { get; set; }
        public string SubHeading { get; set; }
        public string PerosnalInfo { get; set; }
        public string LoginInfo { get; set; }
        public string FirstNameCaption { get; set; }
        public string LastNameCaption { get; set; }
        public string EmailCaption { get; set; }
        public string PasswordCaption { get; set; }
        public string ConfirmPasswordCaption { get; set; }
        public string SignupButtonCaption { get; set; }
        public string PhoneNumberCaption { get; set; }
        public string AddressCaption { get; set; }
        public string SummaryCaption { get; set; }
        public string CountryCaption { get; set; }
        public string CityCaption { get; set; }
        public string ZipCodeCaption { get; set; }
        public string PhysicianLicenseNumberCaption { get; set; }
        public string MedicalSpecialtyCaption { get; set; }
        public string CountryRequired { get; set; }
        public string PhysicianLicenseNumberRequired { get; set; }
        public bool PhysicianNumberIsRequired { get; set; }
        public string MedicalSpecialtyRequired { get; set; }
        public string FirstNameRequired { get; set; }
        public string LastNameRequired { get; set; }
        public string EmailRequired { get; set; }
        public string InvalidEmail { get; set; }
        public string PasswordRequired { get; set; }
        public string ConfirmPasswordRequired { get; set; }
        public string InvalidPassword { get; set; }
        public string PasswordMismatch { get; set; }
        public string FailureText { get; set; }
        public string UserExistText { get; set; }
        public List<CountryModel> Countries { get; set; }
        public List<LookupItem> MedicalSpecialties { get; set; }
        public List<LookupItem> Languages { get; set; }
        public List<CityDataModel> CityList { get; set; }
        public string FullName { get; set; }
        public string PreferredLanguageCaption { get; set; }
        public string MedicalSpecialtyDisplayName { get; set; }
        public string CountryFlag { get; set; }
        public string CountryDisplayName { get; set; }
        public List<LanguageModel> PreferredLanguages { get; set; }
        public string PreferredService1Caption { get; set; }
        public string PreferredService2Caption { get; set; }
        public string NewsletterSubscribeCaption { get; set; }
        public string TermsandConditionText { get; set; }
        public string ActivityCaption { get; set; }
        public string UploadResumeCaption { get; set; }
        public string AttachmentCaption { get; set; }
        public string AttachmentDisplayName { get; set; }
        public string TitleCaption { get; set; }

        public List<LookupItem> Titles { get; set; }

        public string PhoneNumberValidationMessage { get; set; }
        public string PasswordHint { get; set; }

        /// <summary>
        /// Gets or sets the or text.
        /// </summary>
        /// <value>
        /// The or text.
        /// </value>
        public string OrText { get; set; }
    }
}