﻿using System.Web;

namespace Loreal.Feature.Account.Models
{
    public class SignInViewModel : SignInModel
    {
        public HtmlString Introduction { get; set; }
        public HtmlString SubHeading { get; set; }
        public bool RememberMe { get; set; }
        public bool DisplayRememberMe { get; set; }
        public bool Visible { get; set; }
        public string UsernameLabel { get; set; }
        public string PasswordLabel { get; set; }
        public string RememberMeLabel { get; set; }
        public string UsernameRequired { get; set; }
        public string InvalidUsername { get; set; }
        public string PasswordRequired { get; set; }
        public string LoginButtonLabel { get; set; }
        public string FailureText { get; set; }
        public string RegisterUrlPrefixText { get; set; }
        public HtmlString RegistrationUrl { get; set; }
        public HtmlString ForgotPasswordUrl { get; set; }
        /// <summary>
        /// Gets or sets the or text.
        /// </summary>
        /// <value>
        /// The or text.
        /// </value>
        public string OrText { get; set; }

    }
}