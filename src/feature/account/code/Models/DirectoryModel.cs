﻿namespace Loreal.Feature.Account.Models
{
    public class DirectoryModel
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
        public string CountryCode { get; set; }
        public string SpecialtyCode { get; set; }

        public bool PreferredService2 { get; set; }
        public string Title { get; set; }

        public string FirstName { get; set; }
    }
}
