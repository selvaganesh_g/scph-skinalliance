﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Sitecore.XA.Foundation.Mvc.Models;

namespace Loreal.Feature.Account.Models
{
    public class SignupModel : RenderingModelBase
    {
        [DataType(DataType.Text)]
        [Required]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [Required]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required]
        public string Email { get; set; }
        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [DataType(DataType.Text)]
        public string Address { get; set; }

        [DataType(DataType.Text)]
        [Required]
        public string Country { get; set; }

        [DataType(DataType.Text)]
        [Required]
        public string PhysicianLicenseNumber { get; set; }

        [DataType(DataType.Text)]
        [Required]
        public string MedicalSpecialty { get; set; }
        [AllowHtml]
        public string Summary { get; set; }

        public string Image { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }

        public string Activity { get; set; }

        public string Resume { get; set; }

        public bool PreferredService1 { get; set; }

        public bool PreferredService2 { get; set; }
        public bool NewsLetterSubscribe { get; set; }

        public string PreferredLanguage { get; set; }

        public string Title { get; set; }


    }
}