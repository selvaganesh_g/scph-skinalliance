﻿using System.Collections.Generic;

namespace Loreal.Feature.Account.Models
{
    public class DirectoryListModel
    {
        public string Key { get; set; }
        public List<DirectoryModel> Profiles { get; set; }
    }
}
