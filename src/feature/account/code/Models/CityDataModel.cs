﻿using System;

namespace Loreal.Feature.Account.Models
{
    public class CityDataModel
    {
        public string ID { get; set; }
        public string CityName { get; set; }
        public string CityDisplayName { get; set; }
    }
}