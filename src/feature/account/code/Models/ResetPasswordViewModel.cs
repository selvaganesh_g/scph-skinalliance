﻿namespace Loreal.Feature.Account.Models
{
    public class ResetPasswordViewModel
    {
        public string Title { get; set; }
        public string NewPasswordCaption { get; set; }
        public string NewPasswordRequired { get; set; }
        public string NewPasswordStrength { get; set; }
        public string ConfirmPasswordRequired { get; set; }
        public string ConfirmPasswordCaption { get; set; }
        public string ConfirmPasswordIncorrect { get; set; }
        public string SaveButtonText { get; set; }
        public string UserName { get; set; }
    }
}