﻿using Sitecore.XA.Foundation.Mvc.Models;
using System.Collections.Generic;
using System.Web;

namespace Loreal.Feature.Account.Models
{
    public class MiniProfileModel : RenderingModelBase
    {
        public string WelcomeTitle { get; set; }
        public string UserName { get; set; }
        public List<KeyValuePair<string, string>> NavigationItems { get; set; }
        public bool Visible { get; set; }
        public string Image { get; set; }
        public string LogoutText { get; set; }
        public string CreatePostLink { get; set; }
        public bool IsAccepted { get; set; }
        public HtmlString TermsandConditions { get; set; }
        public string IAgreeText { get; set; }
        public string TermsandConditionsTittle { get; set; }

    }
}