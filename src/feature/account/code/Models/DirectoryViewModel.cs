﻿using System.Collections.Generic;
using Sitecore.XA.Foundation.Mvc.Models;

namespace Loreal.Feature.Account.Models
{
    public class DirectoryViewModel : RenderingModelBase
    {
        public List<DirectoryListModel> List { get; set; }

        public List<CountryModel> Countries => Utils.GetCountries(Sitecore.Context.Database.GetItem(AccountTemplate.Account.Signup.Fields.CountriesFolder));

        public List<LookupItem> MedicalSpecialties => Utils.GetLookupItems(Sitecore.Context.Database.GetItem(AccountTemplate.Account.Signup.Fields.MedicalSpecialtyFolder));
    }
}