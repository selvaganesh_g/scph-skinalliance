﻿using Sitecore.XA.Foundation.Mvc.Models;

namespace Loreal.Feature.Account.Models
{
    public class ForgotPasswordViewModel : RenderingModelBase
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the placeholder.
        /// </summary>
        /// <value>
        /// The placeholder.
        /// </value>
        public string Placeholder { get; set; }
        /// <summary>
        /// Gets or sets the button label.
        /// </summary>
        /// <value>
        /// The button label.
        /// </value>
        public string ButtonLabel { get; set; }
        /// <summary>
        /// Gets or sets the back button label.
        /// </summary>
        /// <value>
        /// The back button label.
        /// </value>
        public string BackButtonLabel { get; set; }
        /// <summary>
        /// Gets or sets the email identifier required.
        /// </summary>
        /// <value>
        /// The email identifier required.
        /// </value>
        public string EmailIdRequired { get; set; }

        public string EmailTemplate { get; set; }

        public string EmailSubject { get; set; }

        public string Email { get; set; }
        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrorMessage { get; set; }
        /// <summary>
        /// Gets or sets the success message.
        /// </summary>
        /// <value>
        /// The success message.
        /// </value>
        public string SuccessMessage { get; set; }
        /// <summary>
        /// Gets or sets the destination URL.
        /// </summary>
        /// <value>
        /// The destination URL.
        /// </value>
        public string DestinationUrl { get; set; }
        public string SendDestinationUrl { get; set; }

        public string DataSource { get; set; }

    }
}