﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;

namespace Loreal.Feature.Account.Security
{
    public class HashFallbackSqlMembershipProvider : SqlMembershipProvider
    {
        private bool _enableFallback = true;
        private string _connectionString;
        private readonly HashAlgorithm _fallbackAlgorithm;

        public HashFallbackSqlMembershipProvider() : this(new SHA1Managed())
        {
        }

        protected HashFallbackSqlMembershipProvider(HashAlgorithm fallbackAlgorithm)
        {
            _fallbackAlgorithm = fallbackAlgorithm;
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            _enableFallback = (config["passwordFormat"] ?? "Hashed") == "Hashed";

            _connectionString = config["connectionString"];
            if (string.IsNullOrEmpty(_connectionString))
            {
                string connectionStringName = config["connectionStringName"];
                if (!string.IsNullOrEmpty(connectionStringName))
                    _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            }

            base.Initialize(name, config);
        }

        public override bool ValidateUser(string username, string password)
        {
            if (base.ValidateUser(username, password)) return true;

            if (_enableFallback)
                return FallbackValidateUser(username, password);

            return false;
        }

        private bool FallbackValidateUser(string username, string password)
        {
            var targetHash = GetPasswordHash(username);

            if (targetHash.Hash == null || targetHash.Salt == null) return false;

            var hash = HashFallbackPassword(password, targetHash.Salt);

            return hash.Equals(targetHash.Hash);
        }

        private KeyedHash GetPasswordHash(string username)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var sqlCommand = new SqlCommand("dbo.aspnet_Membership_GetPasswordWithFormat", connection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add(CreateInputParam("@ApplicationName", SqlDbType.NVarChar, ApplicationName));
                    sqlCommand.Parameters.Add(CreateInputParam("@UserName", SqlDbType.NVarChar, username));
                    sqlCommand.Parameters.Add(CreateInputParam("@UpdateLastLoginActivityDate", SqlDbType.Bit, 0));
                    sqlCommand.Parameters.Add(CreateInputParam("@CurrentTimeUtc", SqlDbType.DateTime, DateTime.UtcNow));

                    var sqlParameter = new SqlParameter("@ReturnValue", SqlDbType.Int);
                    sqlParameter.Direction = ParameterDirection.ReturnValue;
                    sqlCommand.Parameters.Add(sqlParameter);

                    connection.Open();
                    using (var sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        var result = new KeyedHash();

                        if (sqlDataReader.Read())
                        {
                            result.Hash = sqlDataReader.GetString(0);
                            result.Salt = sqlDataReader.GetString(2);
                        }
                        else
                        {
                            result.Hash = null;
                            result.Salt = null;
                        }

                        return result;
                    }
                }
            }
        }

        private SqlParameter CreateInputParam(string paramName, SqlDbType dbType, object objValue)
        {
            var sqlParameter = new SqlParameter(paramName, dbType);
            if (objValue == null)
            {
                sqlParameter.IsNullable = true;
                sqlParameter.Value = DBNull.Value;
            }
            else
                sqlParameter.Value = objValue;
            return sqlParameter;
        }

        private string HashFallbackPassword(string password, string salt)
        {
            byte[] passwordBytes = Encoding.Unicode.GetBytes(password);
            byte[] saltBytes = Convert.FromBase64String(salt);
            byte[] hashBytes;

            var keyedAlgorithm = _fallbackAlgorithm as KeyedHashAlgorithm;
            if (keyedAlgorithm != null)
            {
                if (keyedAlgorithm.Key.Length == saltBytes.Length)
                    keyedAlgorithm.Key = saltBytes;
                else if (keyedAlgorithm.Key.Length < saltBytes.Length)
                {
                    var completeHashBytes = new byte[keyedAlgorithm.Key.Length];
                    Buffer.BlockCopy(saltBytes, 0, completeHashBytes, 0, completeHashBytes.Length);
                    keyedAlgorithm.Key = completeHashBytes;
                }
                else
                {
                    var keyBytes = new byte[keyedAlgorithm.Key.Length];
                    int dstOffset = 0;
                    while (dstOffset < keyBytes.Length)
                    {
                        int count = Math.Min(saltBytes.Length, keyBytes.Length - dstOffset);
                        Buffer.BlockCopy(saltBytes, 0, keyBytes, dstOffset, count);
                        dstOffset += count;
                    }
                    keyedAlgorithm.Key = keyBytes;
                }

                hashBytes = keyedAlgorithm.ComputeHash(passwordBytes);
            }
            else
            {
                var buffer = new byte[saltBytes.Length + passwordBytes.Length];
                Buffer.BlockCopy(saltBytes, 0, buffer, 0, saltBytes.Length);
                Buffer.BlockCopy(passwordBytes, 0, buffer, saltBytes.Length, passwordBytes.Length);
                hashBytes = _fallbackAlgorithm.ComputeHash(buffer);
            }

            return Convert.ToBase64String(hashBytes);
        }

        private class KeyedHash
        {
            public string Salt { get; set; }
            public string Hash { get; set; }
        }
    }
}