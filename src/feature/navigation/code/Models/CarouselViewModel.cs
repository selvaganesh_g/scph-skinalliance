﻿using Sitecore.XA.Foundation.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Navigation.Models
{
    public class CarouselViewModel : RenderingModelBase
    {
        public IList<CarouselModel> Slides { get; set; }
    }
}