﻿using Sitecore.XA.Foundation.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.Navigation.Models
{
    public class CarouselModel
    {
        public int Index { get; set; }
        public HtmlString Caption { get; set; }
        public HtmlString Title { get; set; }
        public HtmlString Description { get; set; }
        public HtmlString Image { get; set; }
        public string ReadMoreText { get; set; }
        public string ReadMore { get; set; }
    }
}