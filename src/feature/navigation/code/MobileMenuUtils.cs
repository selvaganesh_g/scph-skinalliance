﻿using Loreal.Feature.Account;
using Loreal.Feature.Account.Services;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Loreal.Feature.Navigation
{
    public static class MobileMenuUtils
    {
        public static Item MiniProfileItem()
        {

            Item item = ServiceLocator.Current.Resolve<IMultisiteContext>().DataItem.Children["security"]?.Children?.FirstOrDefault(x => x.TemplateID == AccountTemplate.Account.MiniProfile.ID);
            if (item != null)
            {
                return item;
            }
            return null;

        }
        public static string GetProfileImage()
        {
            IProfileService iprofileService = new ProfileService();
            return iprofileService.GetProfileImage(Sitecore.Security.Accounts.User.Current?.Profile);
        }
        public static string GetWelcomeText()
        {
            return string.Format("{0}<span class=\"user-name\">{1}</span>", MiniProfileItem().Fields[AccountTemplate.Account.MiniProfile.Fields.WelcomeTitle]?.Value,
                Sitecore.Security.Accounts.User.Current?.Profile.FullName);
        }
        public static string LogoutText()
        {
            return MiniProfileItem().Fields[AccountTemplate.Account.MiniProfile.Fields.LogoutText]?.Value;
        }
        public static List<KeyValuePair<string, string>> GetMobileProfleLinks()
        {
            MultilistField listField = MiniProfileItem().Fields[AccountTemplate.Account.MiniProfile.Fields.MobileNavigationItems];
            List<KeyValuePair<string, string>> navigationLinks = new List<KeyValuePair<string, string>>();
            if (listField != null && listField.Count > 0)
            {
                foreach (Item item in listField.GetItems())
                {
                    navigationLinks.Add(new KeyValuePair<string, string>(string.IsNullOrEmpty(item.DisplayName) ? item.Name : item.DisplayName, LinkManager.GetItemUrl(item)));
                }
            }
            return navigationLinks;
        }
        public static bool NavigationPrevent(Item pageItem)
        {
            bool _prevent = false;
            CheckboxField field = pageItem.Fields[NavigationTemplates.NavigationPrevent];
            if (field != null)
            {
                return field.Checked;
            }
            return _prevent;
        }
    }
}