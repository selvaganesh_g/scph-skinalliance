﻿using Sitecore.XA.Feature.Navigation.Controllers;
using Sitecore.XA.Feature.Navigation.Repositories.Navigation;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Navigation.Controllers
{
    public class FooterController : StandardController
    {
        private readonly INavigationRepository _navigationRepository;
        public FooterController(INavigationRepository navigationRepository)
        {
            
            _navigationRepository = navigationRepository;
        }
        protected override object GetModel()
        {
            //To make sure all the footer links are visiable for login,signup and static pages.
            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                return _navigationRepository.GetModel();
            }
        }

        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Navigation/Footer.cshtml";
        }
    }
}