﻿using Loreal.Feature.Navigation.Repositories;
using Sitecore;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Navigation.Controllers
{
    public class CarouselExtnController : StandardController
    {
        private readonly ICarouselRepository _carouselRepository;

        public CarouselExtnController(ICarouselRepository carouselRepository)
        {
            _carouselRepository = carouselRepository;
        }

        protected override object GetModel()
        {
            return _carouselRepository.GetModel();
        }

        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Navigation/Carousel.cshtml";
        }
    }
}