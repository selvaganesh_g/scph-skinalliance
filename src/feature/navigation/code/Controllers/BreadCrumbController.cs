﻿using Loreal.Feature.Navigation.Repositories;
using Sitecore.XA.Feature.Navigation.Repositories.Breadcrumb;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Navigation.Controllers
{
    public class BreadCrumbController : StandardController
    {
        private readonly BreadcrumbExtnRepository _breadcrumbRepository;
        public BreadCrumbController()
        {
            _breadcrumbRepository = new BreadcrumbExtnRepository();

        }
        protected override object GetModel()
        {
            _breadcrumbRepository.UrlReffer = Request.UrlReferrer?.LocalPath;
            return _breadcrumbRepository.GetModel();
        }

        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Navigation/BreadCrumb.cshtml";
        }
    }
}