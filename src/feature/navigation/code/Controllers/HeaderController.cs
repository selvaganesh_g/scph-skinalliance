﻿using Loreal.Feature.Navigation.Repositories;
using Sitecore;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Navigation.Controllers
{
    public class HeaderController : StandardController
    {
        private readonly IHeaderRepository _headerRepository;

        public HeaderController(IHeaderRepository headeRepository)
        {
            _headerRepository = headeRepository;
        }

        protected override object GetModel()
        {
            return _headerRepository.Logo(Context.Item, NavigationTemplates.Settings.Fields.Logo);
        }

        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Navigation/Header.cshtml";
        }
    }
}