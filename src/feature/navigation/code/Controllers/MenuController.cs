﻿using Sitecore.XA.Feature.Navigation.Controllers;
using Sitecore.XA.Feature.Navigation.Repositories.Navigation;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Loreal.Feature.Navigation.Controllers
{
    public class MenuController : StandardController
    {
        private readonly INavigationRepository _navigationRepository;
        public MenuController(INavigationRepository navigationRepository)
        {
            _navigationRepository = navigationRepository;
        }
        protected override object GetModel()
        {
            return _navigationRepository.GetModel();
        }

        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/Navigation/Menu.cshtml";
        }
    }
}