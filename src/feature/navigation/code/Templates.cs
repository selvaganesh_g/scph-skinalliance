﻿using Sitecore.Data;

namespace Loreal.Feature.Navigation
{
    public class NavigationTemplates
    {
        public struct Settings
        {
            public static readonly string homePageName = "home";
            public static readonly string userProfilePageName = "userprofile";

            public struct Fields
            {
                public static readonly string Logo = "Logo";
            }
            public struct Footer
            {
                public static readonly string CopyrightText = "copyrighttext";
                public static readonly string FooterTitle = "footertitle";
                public struct RedirectConfirmModal
                {
                    public static readonly string Heading = "modaltitle";
                    public static readonly string Body = "modalbody";
                    public static readonly string Close = "modalclose";
                }
            }
        }

        public static readonly ID ArtcilePageID = new ID("{B86FE4A5-903B-4DFB-A4BA-7E84FDDF3B4C}");
        public static readonly ID NavigationTitle = new ID("{71FCA010-73D7-498A-A5B1-926F5A6954F9}");
        public static readonly ID NavigationPrevent = new ID("{429B383A-1134-4B0A-8E6E-44565940F583}");
        public struct ExternalPage
        {
            public static readonly ID ID = new ID("{CAD8744D-242B-407F-A7E7-A99E70E2753E}");
            public struct Fields
            {
                public static readonly ID Title = new ID("{BBFF04F6-02D8-4C0F-9239-D02AAD67A9D8}");
                public static readonly ID Url = new ID("{3E3CD769-BFD7-4D1C-B597-CF6E9761464D}");
                public static readonly ID Image = new ID("{0F036465-2E42-4EF4-8593-BEDA9FCD0F93}");
            }
        }
        public struct Carousel
        {
            public static readonly ID ID = new ID("{4166672F-EB20-4B69-981F-9F376B9C3721}");
            public struct Fields
            {
                public static readonly ID Caption = new ID("{1A125D5D-4AE9-4A50-812B-8972212ABBD2}");
                public static readonly ID Title = new ID("{7209159D-6B3E-4281-983E-F7F82104FA1B}");
                public static readonly ID Description = new ID("{FE69F5BA-6F97-4E1C-AA6A-02BAF57F7EEB}");
                public static readonly ID Image = new ID("{427EC0BD-3BF2-45B9-880F-BDB0054E7925}");
                public static readonly ID ReadMoreText = new ID("{EE7EE576-36C4-4320-9088-FFCE43436AC9}");
                public static readonly ID ReadMore = new ID("{B31D5DDD-1EDA-4B22-B97D-FF71FC8E72BC}");
            }
        }

    }
}