﻿using Loreal.Feature.Navigation.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.XA.Feature.Navigation.Repositories.Breadcrumb;
using Sitecore.XA.Feature.Navigation.Repositories.Navigation;
using Sitecore.XA.Foundation.IOC.Pipelines.IOC;

namespace Loreal.Feature.Navigation.Pipelines.IoC
{
    public class RegisterNavigationServices : IocProcessor
    {
        public override void Process(IocArgs args)
        {
            args.ServiceCollection.AddTransient<IHeaderRepository, HeaderRepository>();
            args.ServiceCollection.AddTransient<ICarouselRepository, CarouselRepository>();
            args.ServiceCollection.AddTransient<INavigationRepository, NavigationRepository>();
            args.ServiceCollection.AddTransient<IBreadcrumbRepository, BreadcrumbExtnRepository>();
        }
    }
}