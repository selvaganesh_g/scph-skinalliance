﻿using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;

namespace Loreal.Feature.Navigation.Repositories
{
    public class HeaderRepository : IHeaderRepository
    {
        public virtual string Logo(Item contextItem, string field)
        {

            var settingsItem = GetSettingsItem(contextItem);
            ImageField image = settingsItem?.Fields[field];
            if (image != null && image.MediaItem != null)
            {
                return StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(image.MediaItem));
            }
            return string.Empty;

        }

        protected virtual Item GetSettingsItem(Item contextItem)
        {
            return ServiceLocator.Current.Resolve<IMultisiteContext>().GetSettingsItem(contextItem);
        }
    }
}