﻿using Sitecore.Data;
using Sitecore.Data.Items;

namespace Loreal.Feature.Navigation.Repositories
{
    public interface IHeaderRepository
    {
        string Logo(Item contextItem,string field);
    }
}