﻿using Loreal.Feature.Navigation.Models;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;

namespace Loreal.Feature.Navigation.Repositories
{
    public class CarouselRepository : ModelRepository, ICarouselRepository, IModelRepository,
        IAbstractRepository<IRenderingModelBase>
    {

        public override IRenderingModelBase GetModel()
        {
            CarouselViewModel carousel = new CarouselViewModel();
            base.FillBaseProperties(carousel);
            List<CarouselModel> slides = new List<CarouselModel>();
            int i = 0;
            foreach (Item item in Rendering.DataSourceItem.Children)
            {

                var slide = new CarouselModel()
                {
                    Title = new HtmlString(FieldRenderer.Render(item, item.Fields[NavigationTemplates.Carousel.Fields.Title]?.Name)),
                    Caption = new HtmlString(FieldRenderer.Render(item, item.Fields[NavigationTemplates.Carousel.Fields.Caption]?.Name)),
                    Description = new HtmlString(FieldRenderer.Render(item, item.Fields[NavigationTemplates.Carousel.Fields.Description]?.Name)),
                    Image = new HtmlString(new Regex("style=\"[^\"]*\"").Replace(FieldRenderer.Render(item, item.Fields[NavigationTemplates.Carousel.Fields.Image]?.Name, "class=media-object img-rounded img-responsive"), "")),
                    ReadMoreText = item.Fields[NavigationTemplates.Carousel.Fields.ReadMoreText]?.Value,
                    Index = i

                };
                LinkField linkField = item.Fields[NavigationTemplates.Carousel.Fields.ReadMore];
                slide.ReadMore = linkField?.GetFriendlyUrl();
                slides.Add(slide);
                i++;
            }
            carousel.Slides = slides;
            return carousel;
        }

    }
}