﻿using Sitecore.Data.Items;
using Sitecore.XA.Feature.Navigation.Models;
using Sitecore.XA.Feature.Navigation.Repositories.Breadcrumb;
using System.Collections.Generic;
using System.Linq;

namespace Loreal.Feature.Navigation.Repositories
{
    public class BreadcrumbExtnRepository : BreadcrumbRepository
    {
        public string UrlReffer { get; set; }
        public override IEnumerable<BreadcrumbRenderingModel> GetBreadcrumbItems(Item currentItem, Item rootItem)
        {
            if (currentItem.TemplateID == NavigationTemplates.ArtcilePageID)
            {
                if (Sitecore.Context.RawUrl.Contains("notificationarticle") && !string.IsNullOrEmpty(this.UrlReffer))
                {
                    var item = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath + UrlReffer);
                    if (item != null)
                    {
                        currentItem = item;
                    }
                }
                else
                {
                    var requesturl = Sitecore.Data.IDTables.IDTable.GetKeys("article-url").Where(x => x.Key == Sitecore.Context.RawUrl).FirstOrDefault();
                    if (requesturl != null)
                    {
                        var item = Sitecore.Context.Database.GetItem(requesturl.ParentID);
                        if (item != null)
                        {
                            currentItem = item;
                        }
                    }
                }
            }
            return base.GetBreadcrumbItems(currentItem, rootItem);
        }
    }
}