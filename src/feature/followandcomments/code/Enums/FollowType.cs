﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.FollowandComments.Enums
{
    public enum FollowType
    {
        Category = 0,
        SubCategory = 1,
        User = 2
    }
}