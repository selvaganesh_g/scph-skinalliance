﻿using Loreal.Feature.FollowandComments.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Loreal.Feature.FollowandComments.Model
{
    [DataContract, Table("Follow")]
    public class Follow
    {

        /// <summary>
        ///  Userid
        /// </summary>
        [DataMember, Key, Column(Order = 0), Required, MaxLength(100)]
        public string UserId { get; set; }
        /// <summary>
        ///  ID to Follow or Unfollow
        /// </summary>
        [DataMember, Key, Column(Order = 1), Required, MaxLength(100)]
        public string FollowID { get; set; }

        [DataMember, Required]
        public FollowType Type { get; set; }
        [DataMember, Required]
        public string Name { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember, Required]
        public string Link { get; set; }
        public string FollowedName { get; set; }
        public string FollowedImageUrl{ get; set; }
        public string FollowedLink { get; set; }
    }
}