﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.FollowandComments.Model
{
    public class FollowViewModel
    {
        public string Label { get; set; }
        public bool Followed { get; set; }
        
    }
}