﻿using System.Collections.Generic;

namespace Loreal.Feature.FollowandComments.Model
{
    public class ArticleQuestion
    {
        public bool Active { get; set; }
        public string ArticleId { get; set; }
        public string QuizId { get; set; }
        public string Question { get; set; }
        public string AnswerAText { get; set; }
        public string AnswerBText { get; set; }
        public string AnswerCText { get; set; }
        public string AnswerDText { get; set; }
        public string Correct { get; set; }
        public string Response { get; set; }
        public bool ResponseInPercentage { get; set; }
        public string ChkOptionA { get; set; }
        public string ChkOptionB { get; set; }
        public string ChkOptionC { get; set; }
        public string ChkOptionD { get; set; }
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
        public double D { get; set; }        
    }
}