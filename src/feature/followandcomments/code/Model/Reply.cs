﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Loreal.Feature.FollowandComments.Model
{
    public class Reply
    {
        [DataMember, Key, Column(Order = 0), Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int CommentId { get; set; }
        public string Message { get; set; }
        [DataMember, Required, MaxLength(100)]
        public string UserId { get; set; }
        [DataMember, Required]
        public string Name { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        public  Comment Comment { get; set; }
    }
}