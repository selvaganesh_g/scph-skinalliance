﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.FollowandComments.Model
{
    public class FollowUserListViewModel
    {
        public string FollowedLabel { get; set; }
        public string FollowingLabel { get; set; }
        public List<Follow> Followed { get; set; }
        public List<Follow> Following { get; set; }
    }
}