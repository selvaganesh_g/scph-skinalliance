﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.FollowandComments.Model
{
    public class LikeViewModel
    {
        public string Label { get; set; }
        public int Count { get; set; }
        public bool Like { get; set; }
    }
}