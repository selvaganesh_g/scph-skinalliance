﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.FollowandComments.Model
{
    public class FollowCategoryListViewModel
    {
        public string FollowCategoryLabel { get; set; }
        public string FollowSubCategoryLabel { get; set; }
        public List<Follow> Categories { get; set; }
        public List<Follow> SubCategories { get; set; }
    }
}