﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
namespace Loreal.Feature.FollowandComments.Model
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract, Table("Comment")]
    public class Comment
    {

        [DataMember, Key, Column(Order = 0), Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        /// <summary>
        ///  Userid
        /// </summary>
        [DataMember, Required, MaxLength(100)]
        public string UserId { get; set; }
        [DataMember, Required]
        public string Name { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember, Required]
        public string Link { get; set; }
        [DataMember, Required]
        public DateTime Date { get; set; }
        [DataMember, Key, Column(Order = 1), Required, MaxLength(100)]
        public string ArticleID { get; set; }
        /// <summary>
        ///  ID to Follow or Unfollow
        /// </summary>
        [DataMember, Required, MaxLength(500)]
        public string Text { get; set; }

        public ICollection<Reply> Replies { get; set; }
    }
}