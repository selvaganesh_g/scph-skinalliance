﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
namespace Loreal.Feature.FollowandComments.Model
{
    [DataContract, Table("Like")]
    public class Like
    {
        /// <summary>
        ///  Userid
        /// </summary>
        [DataMember, Key, Column(Order = 0), Required, MaxLength(100)]
        public string UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember, Key, Column(Order = 1), Required, MaxLength(100)]
        public string ArticleID { get; set; }
        [DataMember, Required]
        public DateTime Date { get; set; }
    }
}