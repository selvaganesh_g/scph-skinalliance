﻿using System.Collections.Generic;

namespace Loreal.Feature.FollowandComments.Model
{
    public class ServeyQuestionViewModel
    {
        public List<ArticleQuestion> Questionnaire { get; set; }
        public string TabHeading { get; set; }
        public string SubmitButtonText { get; set; }
        public string AnswerACaption { get; set; }
        public string AnswerBCaption { get; set; }
        public string AnswerCCaption { get; set; }
        public string AnswerDCaption { get; set; }
    }
}