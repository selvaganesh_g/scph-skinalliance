﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.FollowandComments.Model
{
    public class CommentViewModel
    {
        public string Title { get; set; }
        public string PostCommentLabel { get; set; }
        public string PostButtonText { get; set; }
        public string ReplyText { get; set; }
        public string ReplyTextRequired { get; set; }
        public string ReplyButtonText { get; set; }
        public string ReplyLabel { get; set; }
        public string EditText { get; set; }
        public string EditTextRequired { get; set; }
        public string EditButtonText { get; set; }
        public string EditLabel { get; set; }
        public string Message { get; set; }
        public string RequiredMessage { get; set; }
        public string UserId { get; set; }
        public List<Comment> Comments { get; set; }
    }
}