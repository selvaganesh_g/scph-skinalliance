﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
namespace Loreal.Feature.FollowandComments.Model
{
    [DataContract, Table("Quiz")]
    public class Quiz
    {
        [DataMember, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [DataMember, Column(Order = 0), Required, MaxLength(50), DataType(DataType.Text)]
        public string UserId { get; set; }
        /// <summary>
        ///  Userid
        /// </summary>
        [DataMember, Column(Order = 1), MaxLength(50), DataType(DataType.Text)]
        public string QuizId { get; set; }
        /// <summary>
        /// Article Id
        /// </summary>
        [DataMember, Column(Order = 2), MaxLength(50), DataType(DataType.Text)]
        public string ArticleId { get; set; }

        /// <summary>
        /// Gets or sets the answer a.
        /// </summary>
        /// <value>
        /// The answer a.
        /// </value>
        [DataMember, Column(Order = 3), DataType(DataType.Text)]
        public string ChkOptionA { get; set; }
        /// <summary>
        /// Gets or sets the answer b.
        /// </summary>
        /// <value>
        /// The answer b.
        /// </value>
        [DataMember, Column(Order = 4), DataType(DataType.Text)]
        public string ChkOptionB { get; set; }
        /// <summary>
        /// Gets or sets the answer c.
        /// </summary>
        /// <value>
        /// The answer c.
        /// </value>
        [DataMember, Column(Order = 5), DataType(DataType.Text)]
        public string ChkOptionC { get; set; }
        /// <summary>
        /// Gets or sets the answer d.
        /// </summary>
        /// <value>
        /// The answer d.
        /// </value>
        [DataMember, Column(Order = 6), DataType(DataType.Text)]
        public string ChkOptionD { get; set; }
    }
}