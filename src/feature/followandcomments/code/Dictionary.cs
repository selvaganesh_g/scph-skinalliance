﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.FollowandComments
{
    public class Dictionary
    {

        public struct Follow
        {
            public struct Category
            {
                public static readonly string FollowButtonText = "followcategorybuttontext";
                public static readonly string UnfollowButtonText = "unfollowcategorybuttontext";
                public static readonly string FollowCategoryLabel = "followcategorylabel";
                public static readonly string FollowSubCategoryLabel = "followsubcategorylabel";
            }
            public struct User
            {
                public static readonly string FollowButtonText = "followuserbuttontext";
                public static readonly string UnfollowButtonText = "unfollowuserbuttontext";
                public static readonly string FollowedByLabel = "followedbylabel";
                public static readonly string FollowingLabel = "followinglabel";
            }
            public struct Like
            {
                public static readonly string LikeButtonText = "postliketext";
                public static readonly string UnlikeButtonText = "postunliketext";
            }
            public struct Comment
            {
                public static readonly string Title = "commenttitle";
                public static readonly string ReplyText = "commentreplytext";
                public static readonly string EditText = "commentedittext";
                public static readonly string PostCommentLabel = "postcommentlabel";
                public static readonly string PostButtonText = "postbuttontext";
                public static readonly string RequiredMessage = "postcommentrequired";
                public struct Reply
                {
                    public static readonly string TextRequired = "replytxtrequired";
                    public static readonly string ButtonText = "replybuttontxt";
                    public static readonly string Label = "replylabel";
                }
                public struct Edit
                {
                    public static readonly string TextRequired = "edittxtrequired";
                    public static readonly string ButtonText = "editbuttontxt";
                    public static readonly string Label = "editlabel";
                }
                public struct ClinicalComment
                {
                    public static readonly string CommentText = "Commenttext";
                    public static readonly string WrongText = "wrongtext";
                    public static readonly string CorrectText = "correcttext";
                }
            }
        }
    }
}
