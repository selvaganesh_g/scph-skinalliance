﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Loreal.Feature.FollowandComments.DAL
{
    internal class DBContext : DbContext
    {
        public DBContext() : base("custom")
        {
            this.Database.CommandTimeout = 0;
        }

        public DbSet<Model.Comment> Comment { get; set; }
        public DbSet<Model.Like> Like { get; set; }
        public DbSet<Model.Follow> Follow { get; set; }
        public DbSet<Model.Reply> Reply { get; set; }

        public DbSet<Model.Quiz> Quiz { get; set; }
    }
}