﻿namespace Loreal.Feature.FollowandComments.DAL
{
    internal class CommentRepository : WrappedDataRepository<Model.Comment>
    {
        public CommentRepository(IDataRepository<Model.Comment> repo) : base(repo)
        {

        }
    }
}