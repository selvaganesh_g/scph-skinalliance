﻿namespace Loreal.Feature.FollowandComments.DAL
{
    internal class ReplyRepository : WrappedDataRepository<Model.Reply>
    {
        public ReplyRepository(IDataRepository<Model.Reply> repo) : base(repo)
        {

        }
    }
}