﻿using System.Linq;

namespace Loreal.Feature.FollowandComments.DAL
{

    internal interface IDataRepository<T> where T : class
    {
        IQueryable<T> Items { get; }
        void Add(T item);
        void Delete(T item);

        
    }
}