﻿namespace Loreal.Feature.FollowandComments.DAL
{
    internal class LikeRepository : WrappedDataRepository<Model.Like>
    {
        public LikeRepository(IDataRepository<Model.Like> repo) : base(repo)
        {

        }
    }
}