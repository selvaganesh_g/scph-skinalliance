﻿

namespace Loreal.Feature.FollowandComments.DAL
{
    internal class FollowRepository : WrappedDataRepository<Model.Follow>
    {
        public FollowRepository(IDataRepository<Model.Follow> repo) : base(repo)
        {

        }
    }
}