﻿namespace Loreal.Feature.FollowandComments.DAL
{
    internal class QuizRepository : WrappedDataRepository<Model.Quiz>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QuizRepository"/> class.
        /// </summary>
        /// <param name="repo">The repo.</param>
        public QuizRepository(IDataRepository<Model.Quiz> repo) : base(repo)
        {

        }
    }
}