﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.FollowandComments.DAL
{
    internal class ContextData : IDisposable
    {
        private static readonly object _contextDataKey = new object();
        [ThreadStatic]
        private static ContextData _instance;
        public static ContextData CurrentInstance
        {
            get
            {
                ContextData inst;
                var context = System.Web.HttpContext.Current;
                if (context != null)
                {
                    if (context.Items.Contains(_contextDataKey))
                    { inst = context.Items[_contextDataKey] as ContextData; }
                    else
                    {
                        inst = new ContextData();
                        context.Items[_contextDataKey] = inst;
                    }
                }
                else
                {
                    _instance = _instance ?? new ContextData();
                    inst = _instance;
                }

                return inst;
            }
        }
        private ContextData()
        {
            this.Context = new DBContext();
            this.Context.Database.CommandTimeout = 0;
        }
        protected DBContext Context { get; set; }

        protected CommentRepository _comment;
        public CommentRepository Comment
        {
            get
            {
                _comment = _comment ?? new CommentRepository(new EFDataRepository<Model.Comment>(this.Context));
                return _comment;
            }
        }

        protected LikeRepository _like;
        public LikeRepository Like
        {
            get
            {
                _like = _like ?? new LikeRepository(new EFDataRepository<Model.Like>(this.Context));
                return _like;
            }
        }

        protected QuizRepository _quiz;
        public QuizRepository Quiz
        {
            get
            {
                _quiz = _quiz ?? new QuizRepository(new EFDataRepository<Model.Quiz>(this.Context));
                return _quiz;
            }
        }

        protected ReplyRepository _reply;
        public ReplyRepository Reply
        {
            get
            {
                _reply = _reply ?? new ReplyRepository(new EFDataRepository<Model.Reply>(this.Context));
                return _reply;
            }
        }

        protected FollowRepository _follow;
        public FollowRepository Follow
        {
            get
            {
                _follow = _follow ?? new FollowRepository(new EFDataRepository<Model.Follow>(this.Context));
                return _follow;
            }
        }
        public int SaveChanges()
        {
            return this.Context.SaveChanges();
        }
        public void Dispose()
        {
            if (ContextData._instance == this)
            {
                ContextData._instance = null;
            }
            this.Context.Dispose();
        }
    }
}