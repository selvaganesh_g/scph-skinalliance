﻿using Sitecore.Data;

namespace Loreal.Feature.FollowandComments
{
    public static class ServeyTemplates
    {
        public struct ArticleQuestion
        {
            public static readonly ID ID = new ID("{6467B8D7-8D47-40C6-A954-64ED3E300826}");
            public struct Fields
            {
                public static readonly ID Question = new ID("{E5BA867A-ABC2-4CC1-BB16-1C06704AC732}");
                public static readonly ID A = new ID("{EF6951B3-D86F-4D15-9875-A14E4A312277}");
                public static readonly ID B = new ID("{817CABE2-5A51-45E9-84F6-896B11584994}");
                public static readonly ID C = new ID("{184FCE8D-9B10-44E1-8D34-C6216F376E88}");
                public static readonly ID D = new ID("{5E141DD6-7621-4AEF-B340-FC717845CA7A}");
                public static readonly ID Correct = new ID("{6E2923CF-A636-4231-8E43-C51E89848CC2}");
                public static readonly ID Response = new ID("{48C8D538-C415-40BE-95CA-9E0DCECBBFD3}");
                public static readonly ID DisplayResponseinPercentage = new ID("{3CE475D3-1608-42EF-BE8E-D818DA7DEFF1}");
                public static readonly ID AnswerACaption = new ID("{F79F5894-03FA-44B8-8DB9-853E04600FAA}");
                public static readonly ID AnswerBCaption = new ID("{6B53727A-1DF1-43F5-A72F-2AE1DDE0E9C8}");
                public static readonly ID AnswerCCaption = new ID("{30F311F5-9D0D-4A56-B4F2-CACA9F4E2319}");
                public static readonly ID AnswerDCaption = new ID("{B7769405-C0A5-4CBC-8554-BC2B3346F707}");
                public static readonly ID QuizTabHeading = new ID("{16859B4D-951D-4416-9FEC-BD75F68214C5}");
                public static readonly ID SubmitButtonText = new ID("{4518E001-7D33-405A-9154-979957968370}");
            }
        }
    }

}