﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Loreal.Feature.FollowandComments.Configuration
{
    internal class EntityConfiguration
    {
        public class FollowConfiguration : EntityTypeConfiguration<Model.Follow>
        {
            public FollowConfiguration()
            {
                Property(col => col.UserId).HasColumnType("nvarchar").HasMaxLength(100).IsRequired();
                Property(col => col.FollowID).HasColumnType("nvarchar").HasMaxLength(100).IsRequired();
                Property(col => col.Name).HasColumnType("nvarchar").HasMaxLength(500).IsRequired();
                Property(col => col.ImageUrl).HasColumnType("nvarchar");
                Property(col => col.Type).HasColumnType("int").IsRequired();
                Property(col => col.Link).HasColumnType("nvarchar").HasMaxLength(1000).IsRequired();
                Property(col => col.FollowedImageUrl).HasColumnType("nvarchar");
                Property(col => col.FollowedName).HasColumnType("nvarchar");
                Property(col => col.FollowedLink).HasColumnType("nvarchar");
                HasKey(col => new { col.FollowID, col.UserId });
            }
        }
        public class QuizConfiguration : EntityTypeConfiguration<Model.Quiz>
        {
            public QuizConfiguration()
            {
                Property(col => col.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                Property(col => col.UserId).HasColumnType("nvarchar").HasMaxLength(50);
                Property(col => col.ArticleId).HasColumnType("nvarchar").HasMaxLength(50);
                Property(col => col.QuizId).HasColumnType("nvarchar").HasMaxLength(50);
                Property(col => col.ChkOptionA).HasColumnType("nvarchar");
                Property(col => col.ChkOptionB).HasColumnType("nvarchar");
                Property(col => col.ChkOptionC).HasColumnType("nvarchar");
                Property(col => col.ChkOptionD).HasColumnType("nvarchar");

            }
        }

        public class LikeConfiguration : EntityTypeConfiguration<Model.Like>
        {
            public LikeConfiguration()
            {
                Property(col => col.UserId).HasColumnType("nvarchar").HasMaxLength(100).IsRequired();
                Property(col => col.ArticleID).HasColumnType("nvarchar").HasMaxLength(100).IsRequired();
                Property(col => col.Date).HasColumnType("date").IsRequired();
                HasKey(col => new { col.ArticleID, col.UserId });
            }
        }

        public class CommentConfiguration : EntityTypeConfiguration<Model.Comment>
        {
            public CommentConfiguration()
            {
                Property(col => col.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                Property(col => col.UserId).HasColumnType("nvarchar").HasMaxLength(100).IsRequired();
                Property(col => col.ArticleID).HasColumnType("nvarchar").HasMaxLength(100);
                Property(col => col.Text).HasColumnType("nvarchar").HasMaxLength(500).IsRequired();
                Property(col => col.Name).HasColumnType("nvarchar").HasMaxLength(500).IsRequired();
                Property(col => col.Link).HasColumnType("nvarchar").HasMaxLength(1000).IsRequired();
                Property(col => col.Date).HasColumnType("date").IsRequired();
                Property(col => col.ImageUrl).HasColumnType("nvarchar");

            }
        }
        public class ReplyConfiguration : EntityTypeConfiguration<Model.Reply>
        {
            public ReplyConfiguration()
            {
                Property(col => col.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                Property(col => col.CommentId).IsRequired();
                HasRequired(n => n.Comment).WithMany(b => b.Replies).HasForeignKey(p => p.CommentId);
                Property(col => col.UserId).HasColumnType("nvarchar").HasMaxLength(100).IsRequired();
                Property(col => col.Name).HasColumnType("nvarchar").HasMaxLength(500).IsRequired();
                Property(col => col.ImageUrl).HasColumnType("nvarchar");
            }
        }

    }
}