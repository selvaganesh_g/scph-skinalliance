﻿using Loreal.Feature.FollowandComments.Managers;
using Loreal.Feature.FollowandComments.Model;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Security.Accounts;
using System.Linq;
using System.Web.Security;

namespace Loreal.Feature.FollowandComments
{
    public static class Utils
    {
        public static string GetUserid()
        {
            return ItemUtil.ProposeValidItemName(User.Current?.Profile?.StartUrl, "");
        }
        public static bool ShowComments()
        {
            return Sitecore.Context.User.IsInRole(@"lrp\hidecomments");
        }
     
        public static string GetPostCreatedBy()
        {
            return Membership.GetUser(Context.Item.Statistics.CreatedBy, false)?.ProviderUserKey.ToString();
        }
        public static string GetCategoryId()
        {
            if (Sitecore.Context.Item.TemplateID == FollowTemplates.SubCategoryID)
            {
                return Sitecore.Context.Item.Fields[FollowTemplates.SubCategoryField]?.Value;
            }
            return Sitecore.Context.Item.ID.ToString();
        }
        public static Enums.FollowType GetFollowType()
        {
            if (Sitecore.Context.Item.TemplateID == FollowTemplates.SubCategoryID)
            {
                return Enums.FollowType.SubCategory;
            }
            return Enums.FollowType.Category;
        }
        public static FollowCategoryListViewModel GetCategoryList(string userid)
        {
            FollowCategoryListViewModel followList = new FollowCategoryListViewModel();
            followList.FollowCategoryLabel = Translate.Text(Dictionary.Follow.Category.FollowCategoryLabel);
            followList.FollowSubCategoryLabel = Translate.Text(Dictionary.Follow.Category.FollowSubCategoryLabel);
            followList.Categories = FollowManager.List().Where(x => x.UserId == userid && x.Type == Enums.FollowType.Category).ToList();
            followList.SubCategories = FollowManager.List().Where(x => x.UserId == userid && x.Type == Enums.FollowType.SubCategory).ToList();
            return followList;
        }
        public static FollowUserListViewModel GetUserList(string userid)
        {
            FollowUserListViewModel followList = new FollowUserListViewModel();
            followList.FollowedLabel = Translate.Text(Dictionary.Follow.User.FollowedByLabel);
            followList.FollowingLabel = Translate.Text(Dictionary.Follow.User.FollowingLabel);
            followList.Following = FollowManager.List().Where(x => x.UserId == userid && x.Type == Enums.FollowType.User).ToList();
            followList.Followed = FollowManager.List().Where(x => x.FollowID == userid && x.Type == Enums.FollowType.User).ToList();
            return followList;
        }
        public static bool GetBooleanField(this Item item, ID fieldID)
        {
            Field fieldValue = item.Fields[fieldID];

            if (fieldValue != null)
                return (fieldValue.Value == "1" ? true : false);
            else
                return false;
        }
        public static string GetFieldValue(this Item item, ID fieldId)
        {
            string fieldValue = string.Empty;
            if (item != null)
            {
                Field field = item.Fields[fieldId];
                if ((field != null) && (!string.IsNullOrEmpty(field.Value)))
                    fieldValue = field.Value;
            }
            return fieldValue;
        }
    }
}