﻿using Loreal.Feature.FollowandComments.Managers;
using Loreal.Feature.FollowandComments.Model;
using Sitecore.Globalization;
using Sitecore.Links;
using System;
using System.Web.Mvc;

namespace Loreal.Feature.FollowandComments.Controllers
{
    public class FollowController : Controller
    {
        private readonly String _viewName = "~/views/SkinAlliance/Feature/Follow/FollowToggle.cshtml";
        [HttpGet]
        public ActionResult Index()
        {
            FollowViewModel viewModel = new FollowViewModel();
            viewModel.Followed = false;
            viewModel.Label = Translate.Text(Dictionary.Follow.Category.FollowButtonText);
            try
            {
                if (FollowManager.Get(Utils.GetCategoryId(), Utils.GetFollowType(), Utils.GetUserid()))
                {
                    viewModel.Followed = true;
                    viewModel.Label = Translate.Text(Dictionary.Follow.Category.UnfollowButtonText);
                }
                return PartialView(this._viewName, viewModel);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, this);
                return PartialView(this._viewName, viewModel);
            }

        }
        [HttpPost]
        public ActionResult Toggle(FollowViewModel model)
        {

            try
            {
                if (model.Followed)
                {

                    FollowManager.Delete(Utils.GetCategoryId(), Utils.GetFollowType(), Utils.GetUserid());
                }
                else
                {

                    FollowManager.Add(new Follow()
                    {
                        FollowID = Utils.GetCategoryId(),
                        Link = LinkManager.GetItemUrl(Sitecore.Context.Item),
                        Type = Utils.GetFollowType(),
                        Name = Sitecore.Context.Item.Fields["Title"].Value,
                        UserId = Utils.GetUserid()
                    });

                }

                return Redirect(Request.UrlReferrer.OriginalString);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace + ex.InnerException, "Follow ERROR");
                return Redirect(Request.UrlReferrer.OriginalString);
            }
        }

        
    }
}
