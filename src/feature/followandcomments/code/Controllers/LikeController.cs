﻿using Loreal.Feature.FollowandComments.Managers;
using Loreal.Feature.FollowandComments.Model;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.SecurityModel;
using System;
using System.Web.Mvc;

namespace Loreal.Feature.FollowandComments.Controllers
{
    public class LikeController : Controller
    {
        private readonly String _viewName = "~/views/SkinAlliance/Feature/Like/LikeToggle.cshtml";
        [HttpGet]
        public ActionResult Index()
        {
            LikeViewModel viewModel = new LikeViewModel();
            viewModel.Like = false;
            viewModel.Label = Translate.Text(Dictionary.Follow.Like.LikeButtonText);
            viewModel.Count = LikeManager.Get(Sitecore.Context.Item.ID.ToString());
            try
            {
                int isliked = LikeManager.Get(Sitecore.Context.Item.ID.ToString(), Utils.GetUserid());
                if (isliked > 0)
                {
                    viewModel.Like = true;
                    viewModel.Label = Translate.Text(Dictionary.Follow.Like.UnlikeButtonText);
                }
                return PartialView(this._viewName, viewModel);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return PartialView(this._viewName, viewModel);
            }

        }
        [HttpPost]
        public ActionResult Toggle(LikeViewModel model)
        {
            CustomActionResult.RedirectResultNoBody result = new CustomActionResult.RedirectResultNoBody(Request.UrlReferrer.OriginalString);
            try
            {
                int count = 0;
                Int32.TryParse(Sitecore.Context.Item.Fields[FollowTemplates.LikeCountFieldName].Value, out count);
                if (model.Like)
                {
                    LikeManager.Delete(Sitecore.Context.Item.ID.ToString(), Utils.GetUserid());
                    if (count > 0)
                    {
                        UpdateCount(Convert.ToString(LikeManager.Get(Sitecore.Context.Item.ID.ToString()) - 1));
                    }
                }
                else
                {
                    var userId = Utils.GetUserid();
                    LikeManager.Add(new Like()
                    {
                        ArticleID = Sitecore.Context.Item.ID.ToString(),
                        UserId = userId,
                        Date = DateTime.UtcNow

                    });

                    var users = LikeManager.GetLikedUsers(Sitecore.Context.Item.ID.ToString());
                    //NotificationManager.LikeNotify(FollowManager.GetFollowedUsers(Enums.FollowType.User, userId), Request.Url.AbsoluteUri, Sitecore.Context.Item.Fields["title"]?.Value);
                    NotificationManager.LikeNotify(users, Request.Url.AbsoluteUri, Sitecore.Context.Item.Fields["title"]?.Value);
                    UpdateCount(Convert.ToString(LikeManager.Get(Sitecore.Context.Item.ID.ToString()) + 1));
                }

                return result;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return result;
            }
        }

        private void UpdateCount(string value)
        {
            using (new SecurityDisabler())
            {
                Sitecore.Context.Item.Editing.BeginEdit();

                Sitecore.Context.Item.Fields[FollowTemplates.LikeCountFieldName].Value = value;

                Sitecore.Context.Item.Editing.EndEdit();
            }
        }
    }
}
