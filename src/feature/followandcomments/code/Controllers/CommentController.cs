﻿using Loreal.Feature.FollowandComments.Managers;
using Loreal.Feature.FollowandComments.Model;
using Sitecore;
using Sitecore.Globalization;
using System.Linq;
using Sitecore.SecurityModel;
using System;
using System.Web.Mvc;

namespace Loreal.Feature.FollowandComments.Controllers
{
    public class CommentController : Controller
    {
        private readonly String _viewName = "~/views/SkinAlliance/Feature/Comment/Comment.cshtml";
        [HttpGet]
        public ActionResult Index()
        {
            CommentViewModel viewModel = new CommentViewModel();
            try
            {
                viewModel.Title = Translate.Text(Dictionary.Follow.Comment.Title);
                viewModel.Comments = CommentManager.List(Context.Item.ID.ToString());
                viewModel.PostCommentLabel = Translate.Text(Dictionary.Follow.Comment.PostCommentLabel);
                viewModel.PostButtonText = Translate.Text(Dictionary.Follow.Comment.PostButtonText);
                viewModel.RequiredMessage = Translate.Text(Dictionary.Follow.Comment.RequiredMessage);
                viewModel.ReplyText = Translate.Text(Dictionary.Follow.Comment.ReplyText);
                viewModel.ReplyButtonText = Translate.Text(Dictionary.Follow.Comment.Reply.ButtonText);
                viewModel.ReplyLabel = Translate.Text(Dictionary.Follow.Comment.Reply.Label);
                viewModel.ReplyTextRequired = Translate.Text(Dictionary.Follow.Comment.Reply.TextRequired);
                viewModel.EditText = Translate.Text(Dictionary.Follow.Comment.EditText);
                viewModel.EditButtonText = Translate.Text(Dictionary.Follow.Comment.Edit.ButtonText);
                viewModel.EditLabel = Translate.Text(Dictionary.Follow.Comment.Edit.Label);
                viewModel.EditTextRequired = Translate.Text(Dictionary.Follow.Comment.Edit.TextRequired);
                viewModel.UserId = Utils.GetUserid();
                return PartialView(this._viewName, viewModel);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return PartialView(this._viewName, viewModel);
            }

        }
        [HttpPost]
        public ActionResult Add(CommentViewModel model)
        {
            CustomActionResult.RedirectResultNoBody result = new CustomActionResult.RedirectResultNoBody(Request.UrlReferrer.OriginalString);
            try
            {

                var currentProfile = Context.User?.Profile;
                var userId = Utils.GetUserid();
                CommentManager.Add(new Comment()
                {
                    ArticleID = Sitecore.Context.Item.ID.ToString(),
                    UserId = userId,
                    Name = currentProfile.FullName,
                    ImageUrl = currentProfile.Icon,
                    Link = string.Format("{0}?id={1}", Request.Url.AbsoluteUri, currentProfile.StartUrl),
                    Text = model.Message,
                    Date = System.DateTime.Now.Date
                });
                var users = CommentManager.List(Sitecore.Context.Item.ID.ToString()).Select(x => x.UserId).ToList();
                //NotificationManager.CommentNotify(FollowManager.GetFollowedUsers(Enums.FollowType.User, userId), Request.Url.AbsoluteUri, Context.Item.Fields["title"]?.Value);
                NotificationManager.CommentNotify(users, Request.Url.AbsoluteUri, Context.Item.Fields["title"]?.Value);
                UpdateCount(System.Convert.ToString(CommentManager.List(Context.Item.ID.ToString())?.Count + 1));

                return result;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return result;
            }
        }
        [HttpPost]
        public ActionResult Reply(Reply model)
        {
            CustomActionResult.RedirectResultNoBody result = new CustomActionResult.RedirectResultNoBody(Request.UrlReferrer.OriginalString);
            try
            {
                var currentProfile = Context.User?.Profile;
                model.UserId = Utils.GetUserid();
                model.Name = currentProfile.FullName;
                model.ImageUrl = currentProfile.Icon;
                ReplyManager.Add(model);
                var users = CommentManager.List(Sitecore.Context.Item.ID.ToString()).Select(x => x.UserId).ToList();
                NotificationManager.CommentReplyNotify(users, Request.Url.AbsoluteUri, Context.Item.Fields["title"]?.Value);
                EmailService.Services.EmailService.SendReply(model.Message, Request.UrlReferrer.Equals(Request.Url) ? Request.Url.AbsoluteUri : Request.UrlReferrer.AbsoluteUri);
                return result;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return result;
            }
        }
        [HttpPost]
        public ActionResult Edit(Comment comment)
        {
            CustomActionResult.RedirectResultNoBody result = new CustomActionResult.RedirectResultNoBody(Request.UrlReferrer.OriginalString);
            try
            {
                CommentManager.Edit(comment);
                return result;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return result;
            }
        }
        private void UpdateCount(string value)
        {
            using (new SecurityDisabler())
            {
                Sitecore.Context.Item.Editing.BeginEdit();

                Sitecore.Context.Item.Fields[FollowTemplates.CommentCountFieldName].Value = value;

                Sitecore.Context.Item.Editing.EndEdit();
            }
        }
    }
}
