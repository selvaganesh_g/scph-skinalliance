﻿using System;
using System.Web.Mvc;

namespace Loreal.Feature.FollowandComments.Controllers
{
    public class FollowProfileController : Controller
    {
        private readonly String _followCategoriesListByProfile = "~/views/SkinAlliance/Feature/Follow/FollowCategoryListByUser.cshtml";
        [HttpGet]
        public ActionResult FollowCategoryListByProfile()
        {
            try
            {
                return PartialView(_followCategoriesListByProfile, Utils.GetCategoryList(Utils.GetUserid()));
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return PartialView(this._followCategoriesListByProfile, null);
            }
        }

        private readonly String _followUserListByUser = "~/views/SkinAlliance/Feature/Follow/FollowUserListByUser.cshtml";
        [HttpGet]
        public ActionResult FollowUserListByProfile()
        {
            try
            {
                return PartialView(_followUserListByUser, Utils.GetUserList(Utils.GetUserid()));
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, this);
                return PartialView(this._followUserListByUser, null);
            }
        }

    }
}