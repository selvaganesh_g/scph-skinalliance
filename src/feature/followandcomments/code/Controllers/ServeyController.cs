﻿using Loreal.Feature.FollowandComments.Model;
using Sitecore;
using System;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using Sitecore.Mvc.Presentation;
using Loreal.Feature.FollowandComments.Managers;
using Sitecore.Data.Items;

namespace Loreal.Feature.FollowandComments.Controllers
{
    public class ServeyController : Controller
    {
        private readonly string _viewName = "~/views/SkinAlliance/Feature/Servey/ServeyQuestions.cshtml";
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            var viewModel = new ServeyQuestionViewModel();

            var dataSourceId = RenderingContext.CurrentOrNull.Rendering.DataSource;

            if (!string.IsNullOrEmpty(dataSourceId))
            {
                var dataSource = Context.Database.GetItem(dataSourceId);

                viewModel.AnswerACaption = dataSource.Fields[ServeyTemplates.ArticleQuestion.Fields.AnswerACaption]?.Value;
                viewModel.AnswerBCaption = dataSource.Fields[ServeyTemplates.ArticleQuestion.Fields.AnswerBCaption]?.Value;
                viewModel.AnswerCCaption = dataSource.Fields[ServeyTemplates.ArticleQuestion.Fields.AnswerCCaption]?.Value;
                viewModel.AnswerDCaption = dataSource.Fields[ServeyTemplates.ArticleQuestion.Fields.AnswerDCaption]?.Value;
                viewModel.TabHeading = dataSource.Fields[ServeyTemplates.ArticleQuestion.Fields.QuizTabHeading]?.Value;
                viewModel.SubmitButtonText = dataSource.Fields[ServeyTemplates.ArticleQuestion.Fields.SubmitButtonText]?.Value;
                viewModel.Questionnaire = new List<ArticleQuestion>();
                bool flag = true;
                foreach (var item in Context.Item.Children.Where(x => x.TemplateID == ServeyTemplates.ArticleQuestion.ID))
                {
                    var questModel = new ArticleQuestion()
                    {
                        Question = Utils.GetFieldValue(item, ServeyTemplates.ArticleQuestion.Fields.Question),
                        AnswerAText = Utils.GetFieldValue(item, ServeyTemplates.ArticleQuestion.Fields.A),
                        AnswerBText = Utils.GetFieldValue(item, ServeyTemplates.ArticleQuestion.Fields.B),
                        AnswerCText = Utils.GetFieldValue(item, ServeyTemplates.ArticleQuestion.Fields.C),
                        AnswerDText = Utils.GetFieldValue(item, ServeyTemplates.ArticleQuestion.Fields.D),
                        Correct = Utils.GetFieldValue(item, ServeyTemplates.ArticleQuestion.Fields.Correct),
                        Response = Utils.GetFieldValue(item, ServeyTemplates.ArticleQuestion.Fields.Response),
                        ResponseInPercentage = Utils.GetBooleanField(item, ServeyTemplates.ArticleQuestion.Fields.DisplayResponseinPercentage),
                        ArticleId = dataSource.ID.ToString(),
                        QuizId = item.ID.ToString(),
                    };
                    if (flag)
                    {
                        questModel.Active = flag;
                        flag = false;
                    }

                    viewModel.Questionnaire.Add(questModel);
                }
            }
            return PartialView(this._viewName, viewModel);
        }



        /// <summary>
        /// Updates the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(ArticleQuestion model)
        {
            var startUrl = Context.User?.Profile?.StartUrl;
            var quiz = new Quiz()
            {
                UserId = startUrl,
                ArticleId = model.ArticleId,
                QuizId = model.QuizId,
                ChkOptionA = model.ChkOptionA,
                ChkOptionB = model.ChkOptionB,
                ChkOptionC = model.ChkOptionC,
                ChkOptionD = model.ChkOptionD
            };
            var item = Context.Database.GetItem(model.QuizId);
            var correctAnswers = Utils.GetFieldValue(item, ServeyTemplates.ArticleQuestion.Fields.Correct);
            var answers = correctAnswers.Split(',');
            QuizManager.Update(quiz);

            var items = QuizManager.GetItems().Where(x => x.ArticleId == model.ArticleId && x.QuizId == item.ID.ToString());
            if (items != null)
            {
                var noOfAnswers = items.Count();
                model.A = Math.Round(((double)items.Where(x => x.ChkOptionA != null).Count() / (double)noOfAnswers) * 100);
                model.B = Math.Round(((double)items.Where(x => x.ChkOptionB != null).Count() / (double)noOfAnswers) * 100);
                model.C = Math.Round(((double)items.Where(x => x.ChkOptionC != null).Count() / (double)noOfAnswers) * 100);
                model.D = Math.Round(((double)items.Where(x => x.ChkOptionD != null).Count() / (double)noOfAnswers) * 100);

            }
            model.Active = true;
            model.Correct = correctAnswers;
            model.ArticleId = model.ArticleId;
            model.QuizId = model.QuizId;


            return Json(model, JsonRequestBehavior.AllowGet);
        }

    }
}
