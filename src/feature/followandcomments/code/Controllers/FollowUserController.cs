﻿using Loreal.Feature.FollowandComments.Managers;
using Loreal.Feature.FollowandComments.Model;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Loreal.Feature.FollowandComments.Controllers
{
    public class FollowUserController : Controller
    {
        private readonly String _viewName = "~/views/SkinAlliance/Feature/Follow/FollowUserToggle.cshtml";
        [HttpGet]
        public ActionResult Index()
        {

            FollowViewModel viewModel = new FollowViewModel();
            viewModel.Followed = false;
            viewModel.Label = Translate.Text(Dictionary.Follow.User.FollowButtonText);
            try
            {
                var profileUserId = this.GetUserId();
                var userId = Utils.GetUserid();
                if (profileUserId == userId)
                {
                    viewModel.Label = string.Empty;
                }
                if (FollowManager.Get(ItemUtil.ProposeValidItemName(profileUserId, ""), Enums.FollowType.User, userId))
                {
                    viewModel.Followed = true;
                    viewModel.Label = Translate.Text(Dictionary.Follow.User.UnfollowButtonText);
                }

                return PartialView(this._viewName, viewModel);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, this);
                return PartialView(this._viewName, viewModel);
            }

        }
        [HttpPost]
        public ActionResult Toggle(FollowViewModel model)
        {

            try
            {

                if (model.Followed)
                {

                    FollowManager.Delete(this.GetUserId(true), Enums.FollowType.User, Utils.GetUserid());
                }
                else
                {
                    var profile = Context.Domain.GetUsers().Where(x => x.Profile.StartUrl == GetUserId(true)).FirstOrDefault()?.Profile;
                    var currentProfile = Context.User?.Profile;
                    if (profile != null && currentProfile != null)
                    {
                        var userId = Utils.GetUserid();
                        FollowManager.Add(new Follow()
                        {
                            FollowID = GetUserId(true),
                            Link = Request.UrlReferrer.AbsoluteUri,
                            Type = Enums.FollowType.User,
                            Name = profile.FullName,
                            ImageUrl = profile.Icon,
                            UserId = userId,
                            FollowedName = currentProfile.FullName,
                            FollowedImageUrl = currentProfile.Icon,
                            FollowedLink = string.Format("{0}?id={1}", Request.Url.AbsoluteUri, currentProfile.StartUrl)
                        });
                        NotificationManager.FollowUserNotify(FollowManager.GetFollowedUsers(Enums.FollowType.User, userId), Request.UrlReferrer.OriginalString, profile.FullName);
                    }
                }

                return Redirect(Request.UrlReferrer.OriginalString);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, this);
                return Redirect(Request.UrlReferrer.OriginalString);
            }
        }


        private readonly String _followCategoriesListByUser = "~/views/SkinAlliance/Feature/Follow/FollowCategoryListByUser.cshtml";
        [HttpGet]
        public ActionResult FollowCategoryListByUser()
        {
            try
            {
                return PartialView(_followCategoriesListByUser, Utils.GetCategoryList(GetUserId()));
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return PartialView(this._followCategoriesListByUser, null);
            }
        }

        private readonly String _followUserListByUser = "~/views/SkinAlliance/Feature/Follow/FollowUserListByUser.cshtml";
        [HttpGet]
        public ActionResult FollowUserListByUser()
        {
            try
            {
                return PartialView(_followUserListByUser, Utils.GetUserList(GetUserId()));
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, this);
                return PartialView(this._followUserListByUser, null);
            }
        }



        private string GetUserId(bool fromReferrer = false)
        {

            var urlReferrer = fromReferrer ? HttpUtility.ParseQueryString(Request.UrlReferrer.Query) : HttpUtility.ParseQueryString(Request.Url.Query);
            if (urlReferrer != null && urlReferrer["id"] != null)
            {
                return urlReferrer["id"];
            }
            return string.Empty;
        }

    }
}