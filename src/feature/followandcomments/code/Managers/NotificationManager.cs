﻿using Loreal.Feature.MyNotification.Managers;
using System;
using System.Collections.Generic;
using Loreal.Feature.MyNotification;
namespace Loreal.Feature.FollowandComments.Managers
{
    using Model;
    using Sitecore;
    using System.Linq;

    public static class NotificationManager
    {
        /// <summary>
        /// One of your contacts is now following a new user 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="url"></param>
        /// <param name="userName"></param>
        public static void FollowUserNotify(List<Follow> value, string url, string userName)
        {
            Notify(value, url, NotificationType.FollowUser, userName);
        }
        /// <summary>
        /// New content in a followed category
        /// </summary>
        /// <param name="id"></param>
        /// <param name="url"></param>
        public static void NewPostFollowCategoryNotify(List<string> id, string url, string postTitle)
        {
            if (id != null)
            {
                List<Follow> value = FollowManager.GetFollowedUsersBySubCategory(Enums.FollowType.SubCategory, id);
                Notify(value, url, NotificationType.PostInCategory, postTitle);
            }
        }
        /// <summary>
        /// One of your contacts add a new post
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="url"></param>
        public static void NewPostByFollowedUser(string userid, string url, string postTitle)
        {
            List<Follow> value = FollowManager.GetFollowedUsers(Enums.FollowType.User, userid);
            Notify(value, url, NotificationType.PostByUser, postTitle);
        }
        /// <summary>
        /// New comment in a post shared, liked or commented 
        /// Someone interacts with your post
        /// </summary>
        /// <param name="value"></param>
        /// <param name="url"></param>
        /// <param name="postTitle"></param>
        public static void LikeNotify(List<Follow> value, string url, string postTitle)
        {
            var usrID = Utils.GetPostCreatedBy();
            if (!string.IsNullOrEmpty(usrID))
            {
                Notify(usrID, url, NotificationType.LikeInteract, postTitle);
            }
            Notify(value, url, NotificationType.Like, postTitle);
        }
        public static void LikeNotify(List<string> value, string url, string postTitle)
        {
            var usrID = Utils.GetPostCreatedBy();
            if (!string.IsNullOrEmpty(usrID))
            {
                Notify(usrID, url, NotificationType.LikeInteract, postTitle);
            }
            Notify(value, url, NotificationType.Like, postTitle);
        }
        public static void CommentNotify(List<Follow> value, string url, string postTitle)
        {
            var usrID = Utils.GetPostCreatedBy();
            if (!string.IsNullOrEmpty(usrID))
            {
                Notify(usrID, url, NotificationType.CommentInteract, postTitle);
            }
            Notify(value, url, NotificationType.Comment, postTitle);
        }
        public static void CommentNotify(List<string> value, string url, string postTitle)
        {
            var usrID = Utils.GetPostCreatedBy();
            if (!string.IsNullOrEmpty(usrID))
            {
                Notify(usrID, url, NotificationType.CommentInteract, postTitle);
            }
            Notify(value, url, NotificationType.Comment, postTitle);
        }
        public static void CommentReplyNotify(List<Follow> value, string url, string postTitle)
        {
            var usrID = Utils.GetPostCreatedBy();
            if (!string.IsNullOrEmpty(usrID))
            {
                Notify(usrID, url, NotificationType.CommentReplyInteract, postTitle);
            }
            Notify(value, url, NotificationType.CommentReply, postTitle);
        }
        public static void CommentReplyNotify(List<string> value, string url, string postTitle)
        {
            var usrID = Utils.GetPostCreatedBy();
            if (!string.IsNullOrEmpty(usrID))
            {
                Notify(usrID, url, NotificationType.CommentReplyInteract, postTitle);
            }
            Notify(value, url, NotificationType.CommentReply, postTitle);
        }
        private static void Notify(List<Follow> value, string url, MyNotification.NotificationType type, string messageName)
        {
            var currentProfile = Context.User?.Profile;
            foreach (var follow in value)
            {
                DateTime? dueDate = MyNotification.Utils.GetDuteDate(follow.UserId, type);
                Sitecore.Diagnostics.Log.Info(follow.UserId + type.ToString(), "Notification Log");
                var _userid = Utils.GetUserid();
                if (_userid == follow.UserId) continue;
                if (dueDate == null) continue;
                MyNotificationManager.Add(new MyNotification.Model.UserNotifications()
                {
                    DueDate = (DateTime)dueDate,
                    Done = false,
                    Image = currentProfile.Icon,
                    Name = currentProfile.FullName,
                    Type = type,
                    Date = DateTime.Now,
                    Userid = _userid,
                    NotifyUserid = follow.UserId,
                    Message = string.Format("<a href=\"{0}\" >{1}</a>", url, messageName)
                });
            }
        }

        private static void Notify(List<string> value, string url, MyNotification.NotificationType type, string messageName)
        {
            var currentProfile = Context.User?.Profile;
            foreach (var follow in value)
            {
                DateTime? dueDate = MyNotification.Utils.GetDuteDate(follow, type);
                Sitecore.Diagnostics.Log.Info(follow + type.ToString(), "Notification Log");
                if (dueDate == null) continue;
                var _userid = Utils.GetUserid();
                if (_userid == follow) continue;
                MyNotificationManager.Add(new MyNotification.Model.UserNotifications()
                {
                    DueDate = (DateTime)dueDate,
                    Done = false,
                    Image = currentProfile.Icon,
                    Name = currentProfile.FullName,
                    Type = type,
                    Date = DateTime.Now,
                    Userid = _userid,
                    NotifyUserid = follow,
                    Message = string.Format("<a href=\"{0}\" >{1}</a>", url, messageName)
                });
            }
        }
        private static void Notify(string userid, string url, MyNotification.NotificationType type, string messageName)
        {
            var currentProfile = Context.User?.Profile;
            DateTime? dueDate = MyNotification.Utils.GetDuteDate(userid, type);
            if (dueDate == null) return;
            var _userid = Utils.GetUserid();
            if (_userid == userid) return;
            MyNotificationManager.Add(new MyNotification.Model.UserNotifications()
            {
                DueDate = (DateTime)dueDate,
                Done = false,
                Image = currentProfile.Icon,
                Name = currentProfile.FullName,
                Type = type,
                Date = DateTime.Now,
                Userid = Utils.GetUserid(),
                NotifyUserid = userid,
                Message = Context.Item == null ? url : string.Format("<a href=\"{0}\" >{1}</a>", url, messageName)
            });

        }
    }
}