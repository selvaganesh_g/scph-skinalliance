﻿using Loreal.Feature.FollowandComments.DAL;
using Loreal.Feature.FollowandComments.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Loreal.Feature.FollowandComments.Managers
{
    public static class QuizManager
    {
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <returns></returns>
        public static List<Quiz> GetItems()
        {
            return ContextData.CurrentInstance.Quiz.Items.ToList();
        }

        /// <summary>
        /// Updates the specified quiz.
        /// </summary>
        /// <param name="quiz">The quiz.</param>
        public static void Update(Quiz quiz)
        {
            try
            {
                ContextData.CurrentInstance.Quiz.Add(quiz);
                ContextData.CurrentInstance.SaveChanges();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "QuizManager-Update");
            }
        }
    }
}