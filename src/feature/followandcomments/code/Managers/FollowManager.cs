﻿using Loreal.Feature.FollowandComments.DAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Loreal.Feature.FollowandComments.Managers
{
    public static class FollowManager
    {

        public static bool Get(string id, Enums.FollowType type, string userid)
        {
            try
            {
                var item = ContextData.CurrentInstance.Follow.Items.Where(x => x.FollowID == id && x.UserId == userid && x.Type == type).FirstOrDefault();
                if (item == null) { return false; }
                return true;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "FollowManger-Get");
                return false;
            }
        }

        public static List<Model.Follow> GetFollowedUsers(Enums.FollowType type, string userid)
        {
            try
            {
                return ContextData.CurrentInstance.Follow.Items.Where(x => x.FollowID == userid && x.Type == type).ToList();

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "FollowManger-Get");
                return new List<Model.Follow>();
            }
        }
        public static List<Model.Follow> GetFollowedUsersBySubCategory(Enums.FollowType type, List<string> subCategoryId)
        {
            try
            {
                return ContextData.CurrentInstance.Follow.Items.Where(x => subCategoryId.Contains(x.FollowID) && x.Type == type).ToList();

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "FollowManger-Get");
                return new List<Model.Follow>();
            }
        }

        public static IQueryable<Model.Follow> List()
        {
            return ContextData.CurrentInstance.Follow.Items;
        }

        public static void Add(Model.Follow follow)
        {
            try
            {
                ContextData.CurrentInstance.Follow.Add(follow);
                ContextData.CurrentInstance.SaveChanges();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "FollowManger-Add");
            }
        }

        public static void Delete(string id, Enums.FollowType type, string userid)
        {
            try
            {
                var follow = ContextData.CurrentInstance.Follow.Items.Where(x => x.FollowID == id && x.UserId == userid & x.Type == type).FirstOrDefault();
                if (follow != null)
                {
                    ContextData.CurrentInstance.Follow.Delete(follow);
                    ContextData.CurrentInstance.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "FollowManger-Delete");
            }
        }
    }
}