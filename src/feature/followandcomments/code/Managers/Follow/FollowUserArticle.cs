﻿using Sitecore.Globalization;
using System;

namespace Loreal.Feature.FollowandComments.Managers
{
    public static class FollowUserArticle
    {
        public static string Get(string userid)
        {
            var followText = Translate.Text(Dictionary.Follow.User.FollowButtonText);
            try
            {
                if (FollowManager.Get(userid, Enums.FollowType.User, Utils.GetUserid()))
                {
                    followText = Translate.Text(Dictionary.Follow.User.UnfollowButtonText);
                }
                return followText;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "Follow user in Article");
                return followText;
            }
        }
    }
}