﻿using Sitecore.Globalization;
using System;

namespace Loreal.Feature.FollowandComments.Managers
{
    public static class FollowCategoryArticle
    {
        public static string Get(string id)
        {
            var followText = Translate.Text(Dictionary.Follow.Category.FollowButtonText);
            try
            {
                if (FollowManager.Get(id, Enums.FollowType.SubCategory, Utils.GetUserid()))
                {
                    followText = Translate.Text(Dictionary.Follow.Category.UnfollowButtonText);
                }
                return followText;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "Follow category in Article");
                return followText;
            }
        }
    }
}