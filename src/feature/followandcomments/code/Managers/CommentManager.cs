﻿using Loreal.Feature.FollowandComments.DAL;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Loreal.Feature.FollowandComments.Managers
{
    public static class CommentManager
    {

        public static List<Model.Comment> List(string id)
        {
            try
            {
                var comments = ContextData.CurrentInstance.Comment.Items.Where(x => x.ArticleID == id).ToList();
                foreach (var comment in comments)
                {
                    comment.Replies = ReplyManager.Get(comment.Id);
                }
                return comments;

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "CommentManager-List");
                return new List<Model.Comment>();
            }
        }

        public static void Edit(Model.Comment comment)
        {
            try
            {
                var _comment= ContextData.CurrentInstance.Comment.Items.Where(x => x.Id == comment.Id).FirstOrDefault();                
                _comment.Text = comment.Text;
                ContextData.CurrentInstance.SaveChanges();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "CommentManager-Get");
            }
        }

        public static void Add(Model.Comment comment)
        {
            try
            {
                ContextData.CurrentInstance.Comment.Add(comment);
                ContextData.CurrentInstance.SaveChanges();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "CommentManager-Add");
            }
        }

    }
}