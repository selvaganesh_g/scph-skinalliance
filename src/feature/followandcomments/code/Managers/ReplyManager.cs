﻿using Loreal.Feature.FollowandComments.DAL;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Loreal.Feature.FollowandComments.Managers
{
    public static class ReplyManager
    {

        public static void Add(Model.Reply reply)
        {
            try
            {
                ContextData.CurrentInstance.Reply.Add(reply);
                ContextData.CurrentInstance.SaveChanges();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "Reply-Add");
            }
        }

        public static List<Model.Reply> Get(int commentId)
        {
            try
            {
                return ContextData.CurrentInstance.Reply.Items.Where(x => x.CommentId == commentId).ToList();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "List-Add");
                return new List<Model.Reply>();
            }
        }
    }
}