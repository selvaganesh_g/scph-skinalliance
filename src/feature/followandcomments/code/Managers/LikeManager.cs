﻿using Loreal.Feature.FollowandComments.DAL;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Loreal.Feature.FollowandComments.Managers
{
    public static class LikeManager
    {

        public static int Get(string articleid, string userid)
        {
            try
            {
                return ContextData.CurrentInstance.Like.Items.Where(x => x.ArticleID == articleid && x.UserId == userid).Count();

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "LikeManager-Get");
                return 0;
            }
        }

        public static List<Model.Like> List(string userid)
        {
            try
            {
                return ContextData.CurrentInstance.Like.Items.Where(x => x.UserId == userid).ToList();

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "LikeManager-list");
                return new List<Model.Like>();
            }
        }

        public static List<Model.Like> GetTopThree()
        {
            try
            {
                List<Model.Like> likeList = new List<Model.Like>();
                var month = DateTime.Now.AddMonths(-6);
                var listItems = ContextData.CurrentInstance.Like.Items.Where(x => x.Date >= month).GroupBy(x => x.ArticleID).OrderByDescending(x => x.Count()).Take(3);
                foreach (var listItem in listItems)
                {
                    likeList.Add(new Model.Like()
                    {
                        ArticleID = listItem.Key
                    });
                }
                return likeList;

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "LikeManager-list");
                return new List<Model.Like>();
            }
        }

        public static int Get(string articleid)
        {
            try
            {
                return ContextData.CurrentInstance.Like.Items.Where(x => x.ArticleID == articleid).Count();

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "LikeManager-Get");
                return 0;
            }
        }

        public static List<string> GetLikedUsers(string articleid)
        {
            try
            {
                return ContextData.CurrentInstance.Like.Items.Where(x => x.ArticleID == articleid).Select(x => x.UserId).ToList();

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "LikeManager-Get");
                return new List<string>();
            }
        }

        public static void Add(Model.Like like)
        {
            try
            {
                ContextData.CurrentInstance.Like.Add(like);
                ContextData.CurrentInstance.SaveChanges();

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "LikeManager-Add");
            }
        }
        public static void Delete(string articleid, string userid)
        {
            try
            {

                var like = ContextData.CurrentInstance.Like.Items.Where(x => x.ArticleID == articleid && x.UserId == userid).FirstOrDefault();
                if (like != null)
                {
                    ContextData.CurrentInstance.Like.Delete(like);
                    ContextData.CurrentInstance.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "LikeManager-Delete");
            }
        }

    }
}