﻿using Sitecore.Data;

namespace Loreal.Feature.FollowandComments
{
    public static class FollowTemplates
    {
        public static readonly ID SubCategoryID = new ID("{34D931AB-2BB2-45FF-A2D2-02AABBE8290C}");
        public static readonly ID SubCategoryField = new ID("{B765A279-F7D9-44ED-A42E-B86D4607A909}");
        public static readonly string LikeCountFieldName = "LikeCount";
        public static readonly string CommentCountFieldName = "CommentCount";
    }

}