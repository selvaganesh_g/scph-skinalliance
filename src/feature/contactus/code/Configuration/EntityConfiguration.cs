﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Loreal.Feature.ContactUs.Configuration
{
    internal class EntityConfiguration
    {
        public class ContactUsConfiguration : EntityTypeConfiguration<Models.ContactUs>
        {
            public ContactUsConfiguration()
            {
                Property(col => col.ID).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                Property(col => col.FirstName).IsRequired().HasColumnType("nvarchar").HasMaxLength(200);
                Property(col => col.LastName).IsRequired().HasColumnType("nvarchar").HasMaxLength(200);
                Property(col => col.PhoneNumber).HasColumnType("nvarchar").HasMaxLength(200);
                Property(col => col.Email).IsRequired().HasColumnType("nvarchar").HasMaxLength(200);
                Property(col => col.Message).IsRequired().HasColumnType("nvarchar");
            }
        }
    }
}