﻿using Loreal.Feature.ContactUs.DAL;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Loreal.Feature.ContactUs.Managers
{
    public static class ContactUsManager
    {
        public static List<Models.ContactUs> List(int id)
        {
            return ContextData.CurrentInstance.ContactUs.Items.Where(x => x.ID == id).ToList();
        }
        public static void Add(Models.ContactUs contactus)
        {
            ContextData.CurrentInstance.ContactUs.Add(contactus);
            ContextData.CurrentInstance.SaveChanges();
        }
    }
}