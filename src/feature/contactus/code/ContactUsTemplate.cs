﻿namespace Loreal.Feature.ContactUs
{
    using Sitecore.Data;
    public struct ContactUsTemplate
    {
        public static readonly ID ID = new ID("{06663B37-309D-49E7-B26F-7CFB17D060AB}");
        public struct Label
        {
            public static readonly ID Title = new ID("{AD44DCBE-5DD0-4809-B0AE-359CD581C282}");
            public static readonly ID FirstName = new ID("{A8332A1D-59ED-4AE7-A2A9-CD7AEBD2616B}");
            public static readonly ID LastName = new ID("{1A0C6879-A4D2-4C41-9373-08DE4F76B056}");
            public static readonly ID Email = new ID("{B9871F63-9DA3-4164-9EAC-19885F157838}");
            public static readonly ID PhoneNumber = new ID("{94B745EA-EFCE-4AEE-926F-B475328235D9}");
            public static readonly ID Messge = new ID("{AD06814C-C345-45AD-90E8-BBEC04C5EA9E}");
            public static readonly ID Button = new ID("{B7AAF160-908C-40CE-8DBA-C61162DC3936}");
        }
        public struct Message
        {
            public static readonly ID FirstNameRequired = new ID("{835182A2-1BEE-4D9E-979C-FBC467C04ECE}");
            public static readonly ID LastNameRequired = new ID("{A8D2A3E1-A63F-433B-A8E8-10F99A181E65}");
            public static readonly ID EmailRequired = new ID("{F0C532F7-A519-4279-920A-A582D8787378}");
            public static readonly ID InvalidEmail = new ID("{2135F4A9-DC94-4F83-8490-ACC80B84DD19}");
            public static readonly ID PhoneNumberRequired = new ID("{939C4B32-B430-494C-A5E1-2AAC3D39FE7B}");
            public static readonly ID InvalidPhoneNumber = new ID("{866084BA-B106-4433-94DF-C2DE3FBBF4B8}");
            public static readonly ID MessgeRequired = new ID("{F51FE147-2C65-4F5D-9FE1-0201ADD12FC3}");
            public static readonly ID FailureText = new ID("{268C7EBD-B77C-4BA1-8934-F0CFA93FA09A}");
            public static readonly ID SuccessText = new ID("{B3F22B76-2844-4880-9E22-21A76034BB2D}");
        }

        public struct Data
        {
            public static readonly ID Title = new ID("{9A9122E1-238D-4638-B2C1-7968DE9DE48C}");
            public static readonly ID Address = new ID("{5FFA5FDB-C866-438B-9E35-4E8002D4E8D6}");
            public static readonly ID Email = new ID("{A61FDCA1-AC5D-46A8-AAF0-2BBBC8F9E4B1}");
            public static readonly ID Phone = new ID("{388DCF3B-D9DC-45DE-8891-141BABA78ED2}");
            public static readonly ID Url = new ID("{3F924072-7B31-4CDD-8C09-49B027CE52BB}");
        }
        public struct Mail
        {
            public static readonly ID Subject = new ID("{FFC97966-E373-4225-905E-13C43A6F5C4B}");
            public static readonly ID Body = new ID("{F030FA21-E77C-4578-AB98-005F3254072E}");
        }
    }
}