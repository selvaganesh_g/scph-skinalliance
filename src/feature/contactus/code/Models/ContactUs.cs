﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Loreal.Feature.ContactUs.Models
{
    [DataContract, Table("ContactUs")]
    public class ContactUs
    {
        [DataMember, Key, Required, DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [DataMember, Required]
        public string FirstName { get; set; }
        [DataMember, Required]
        public string LastName { get; set; }
        [DataMember, Required]
        public string Email { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember, Required]
        public string Message { get; set; }
    }
}