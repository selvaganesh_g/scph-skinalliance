﻿using Sitecore.Data.Items;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Loreal.Feature.ContactUs.Models
{
    public class ContactUsViewModel
    {

        protected Item Setting
        {
            get
            {
                Item item = ServiceLocator.Current.Resolve<IMultisiteContext>().SettingsItem.Children.FirstOrDefault(x => x.TemplateID == ContactUsTemplate.ID);
                if (item != null)
                {
                    return item;
                }
                Sitecore.Diagnostics.Log.Error("Sitecore ContactUS Item missing under Settings!", this);
                return null;
            }
        }
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string Message { get; set; }
        public string Title
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Label.Title]?.Value;
            }
        }
        public string FirstNameLabel
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Label.FirstName]?.Value;
            }
        }
        public string LastNameLabel
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Label.LastName]?.Value;
            }
        }
        public string EmailLabel
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Label.Email]?.Value;
            }
        }
        public string PhoneNumberLabel
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Label.PhoneNumber]?.Value;
            }
        }
        public string MessageLabel
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Label.Messge]?.Value;
            }
        }
        public string FirstNameRequired
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Message.FirstNameRequired]?.Value;
            }
        }
        public string LastNameRequired
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Message.LastNameRequired]?.Value;
            }
        }
        public string EmailRequired
        {

            get
            {
                return Setting?.Fields[ContactUsTemplate.Message.EmailRequired]?.Value;
            }

        }
        public string InvalidEmail
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Message.InvalidEmail]?.Value;
            }
        }
        public string PhoneNumberRequired
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Message.PhoneNumberRequired]?.Value;
            }
        }
        public string InvalidPhoneNumber
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Message.InvalidPhoneNumber]?.Value;
            }
        }
        public string MessageRequired
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Message.MessgeRequired]?.Value;
            }
        }
        public string ButtonLabel
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Label.Button]?.Value;
            }
        }
        public string SiteInfoTitle
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Data.Title]?.Value;
            }
        }
        public string Address
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Data.Address]?.Value;
            }
        }
        public string Phone
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Data.Phone]?.Value;
            }
        }
        public string SiteEmail
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Data.Email]?.Value;
            }
        }
        public string Url
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Data.Url]?.Value;
            }
        }

        public string SuccessText
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Message.SuccessText]?.Value;
            }
        }

        public string FailureText
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Message.FailureText]?.Value;
            }
        }

        internal string MailSubject
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Mail.Subject]?.Value;
            }
        }
        internal string MailBody
        {
            get
            {
                return Setting?.Fields[ContactUsTemplate.Mail.Body]?.Value;
            }
        }

    }
}