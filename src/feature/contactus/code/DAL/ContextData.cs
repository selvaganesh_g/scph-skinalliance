﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.Feature.ContactUs.DAL
{
    internal class ContextData : IDisposable
    {
        private static readonly object _contextDataKey = new object();
        [ThreadStatic]
        private static ContextData _instance;
        public static ContextData CurrentInstance
        {
            get
            {
                ContextData inst;
                var context = System.Web.HttpContext.Current;
                if (context != null)
                {
                    if (context.Items.Contains(_contextDataKey))
                    { inst = context.Items[_contextDataKey] as ContextData; }
                    else
                    {
                        inst = new ContextData();
                        context.Items[_contextDataKey] = inst;
                    }
                }
                else
                {
                    _instance = _instance ?? new ContextData();
                    inst = _instance;
                }

                return inst;
            }
        }
        private ContextData()
        {
            this.Context = new DBContext();
            this.Context.Database.CommandTimeout = 0;
        }
        protected DBContext Context { get; set; }

        protected ContactUsRepository _contactus;
        public ContactUsRepository ContactUs
        {
            get
            {
                _contactus = _contactus ?? new ContactUsRepository(new EFDataRepository<Models.ContactUs>(this.Context));
                return _contactus;
            }
        }


        public int SaveChanges()
        {
            return this.Context.SaveChanges();
        }
        public void Dispose()
        {
            if (ContextData._instance == this)
            {
                ContextData._instance = null;
            }
            this.Context.Dispose();
        }
    }
}