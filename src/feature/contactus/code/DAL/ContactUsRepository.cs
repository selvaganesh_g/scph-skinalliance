﻿namespace Loreal.Feature.ContactUs.DAL
{
    internal class ContactUsRepository : WrappedDataRepository<Models.ContactUs>
    {
        public ContactUsRepository(IDataRepository<Models.ContactUs> repo) : base(repo)
        {

        }
    }
}