﻿using System.Data.Entity;
using System.Linq;

namespace Loreal.Feature.ContactUs.DAL
{
    internal class EFDataRepository<T> : IDataRepository<T> where T : class
    {
        public EFDataRepository(DbContext context)
        {
            this.Context = context;
            this.ItemsInternal = context.Set<T>();
        }
        protected DbContext Context { get; set; }
        internal DbSet<T> ItemsInternal { get; set; }
        public IQueryable<T> Items
        {
            get { return this.ItemsInternal; }
        }
        public void Add(T item)
        {
            this.ItemsInternal.Add(item);
        }
        public void Delete(T item)
        {
            this.ItemsInternal.Remove(item);
        }
    }
}