﻿using System.Linq;

namespace Loreal.Feature.ContactUs.DAL
{
    internal class WrappedDataRepository<T> : IDataRepository<T> where T : class
    {

        protected IDataRepository<T> Repository { get; set; }

        public WrappedDataRepository(IDataRepository<T> repo)
        {
            this.Repository = repo;
        }
        public IQueryable<T> Items
        {
            get { return this.Repository.Items; }
        }

        public void Add(T item)
        {
            this.Repository.Add(item);
        }

        public void Delete(T item)
        {
            this.Repository.Delete(item);
        }
    }
}