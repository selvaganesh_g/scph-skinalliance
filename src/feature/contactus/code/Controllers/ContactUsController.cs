﻿using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Loreal.Feature.ContactUs.Controllers
{
    public class ContactUsController : StandardController
    {
        public override ActionResult Index()
        {
            return PartialView(this.GetIndexViewName(), new Models.ContactUsViewModel());
        }

        [HttpPost]
        public ActionResult Index(Models.ContactUsViewModel model)
        {
            try
            {
                ModelState.Remove(nameof(model.PhoneNumber));
                var validateModel = ValidateModel(model);
                if (!base.ModelState.IsValid)
                {
                    return this.PartialView(this.GetIndexViewName(), SetModel(model, validateModel));
                }
                Managers.ContactUsManager.Add(new Models.ContactUs()
                {

                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber,
                    Message = model.Message
                });
                EmailService.Services.EmailService.SendContacUs(model.SiteEmail, TokenReplace(model.MailSubject, model), TokenReplace(model.MailBody, model));
                return this.Redirect(string.Format("{0}?success=true", Request.Url.ToString().Split('?').First()));
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, this);
                return this.Redirect(string.Format("{0}?success=false", Request.Url.ToString().Split('?').First()));
            }
        }

        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/ContactUs/ContactUs.cshtml";
        }

        protected Models.ContactUsViewModel ValidateModel(Models.ContactUsViewModel model)
        {
            Models.ContactUsViewModel resultModel = new Models.ContactUsViewModel();
            if (!string.IsNullOrEmpty(model.Email) && !IsValidEmail(model.Email))
            {
                this.ModelState.AddModelError(nameof(resultModel.Email), resultModel.EmailRequired);
            }
            if (!string.IsNullOrEmpty(model.PhoneNumber) && !IsValidPhone(model.PhoneNumber))
            {
                this.ModelState.AddModelError(nameof(resultModel.Email), resultModel.EmailRequired);
            }
            return resultModel;
        }

        protected Models.ContactUsViewModel SetModel(Models.ContactUsViewModel model, Models.ContactUsViewModel modelOrg)
        {
            var modelSet = modelOrg;
            modelSet.FirstName = model.FirstName;
            modelSet.LastName = model.LastName;
            modelSet.Email = model.Email;
            modelSet.PhoneNumber = model.PhoneNumber;
            modelSet.Message = model.Message;
            return modelSet;
        }
        private bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email)) return false;
            Regex emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = emailRegex.Match(email);
            return match.Success;
        }

        private bool IsValidPhone(string number)
        {
            if (string.IsNullOrEmpty(number)) return false;
            Regex emailRegex = new Regex(@"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$");
            Match match = emailRegex.Match(number);
            return match.Success;
        }
        private string TokenReplace(string input, Models.ContactUsViewModel model)
        {
            return input.Replace("#firstname#", model.FirstName)
                    .Replace("#lastname#", model.LastName)
                    .Replace("#email#", model.Email)
                    .Replace("#phonenumber#", model.PhoneNumber)
                    .Replace("#message#", model.Message);

        }
    }
}