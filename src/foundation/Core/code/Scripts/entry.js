import Vue from 'vue';

import KeySiteCore from './sitecore/KeySiteCore';
import eventBus from './eventbus/eventbus';

/**
 * Attach the eventbus to the window so that
 * it can be referenced on feature and project level
 */
window.eventBus = eventBus;

/**
 * Initiate the sitecore vue plugin
 */
Vue.use(KeySiteCore);
