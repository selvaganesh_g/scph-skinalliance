﻿using Loreal.Foundation.Search.Models;
using Loreal.Foundation.Search.SearchTypes;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Loreal.Foundation.Search.Services
{
    public static class AzureSearch
    {
        public static SearchResponse Search(IEnumerable<Tuple<string, string, string>> filters, SortType type, Item startItem, int offset, int limit)
        {
            SearchResponse response = new SearchResponse();
            response.Items = new List<Item>();
            response.Count = 0;
            try
            {
                using (var context = ContentSearchManager.CreateSearchContext((SitecoreIndexableItem)Sitecore.Context.Item))
                {
                    List<SearchStringModel> facets = new List<SearchStringModel>();
                    foreach (Tuple<string, string, string> facet in filters)
                    {
                        facets.Add(new SearchStringModel()
                        {
                            Type = facet.Item1,
                            Value = facet.Item2,
                            Operation = facet.Item3
                        });
                    }
                    switch (type)
                    {
                        case SortType.Date:
                        default:
                            facets.Add(new SearchStringModel()
                            {
                                Type = "sort",
                                Value = "article_created[desc]"
                            });

                            break;
                        case SortType.Like:
                            facets.Add(new SearchStringModel()
                            {
                                Type = "sort",
                                Value = "sort_likecount[desc]"
                            });
                            break;
                    }
                    var query = LinqHelper.CreateQuery<ArticleSearchResultItem>(context, facets, startItem, null);
                    query = query.Where(l => l.Language == Sitecore.Context.Language.CultureInfo.Name);
                    query = query.Skip(offset * limit).Take(limit);
                    foreach (var re in query)
                    {
                        response.Items.Add(re.GetItem());
                    }
                    response.Count = query.Count();

                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, "Azure Search Service Error");
            }
            return response;
        }
    }
}