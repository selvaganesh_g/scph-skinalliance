﻿using Sitecore.Data.Items;
using System.Collections.Generic;

namespace Loreal.Foundation.Search.Models
{
    public class SearchResponse
    {
        public int Count { get; set; }
        public List<Item> Items { get; set; }
    }
}