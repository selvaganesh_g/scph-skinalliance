﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using System;
using System.Runtime.Serialization;

namespace Loreal.Foundation.Search.SearchTypes
{
    public class ArticleSearchResultItem : SearchResultItem
    {
        [DataMember]
        [IndexField("sort_likecount")]
        public virtual int LikeCount { get; set; }

        [DataMember]
        [IndexField("article_created")]
        public virtual DateTime Date { get; set; }
    }
}