﻿using Sitecore.Pipelines;
using System.Web.Mvc;
using System.Web.Routing;

namespace Loreal.SkinAlliance.Web.Pipelines
{
    public class RegisterRouteService
    {
        public virtual void Process(PipelineArgs args)
        {
            RouteTable.Routes.MapRoute(
                  name: "Password Recovery",
                  url: "passwordrecovery",
                  defaults: new { controller = "ConfirmRecovery", action = "Confirm", },
                  namespaces: new[] { "Loreal.Feature.Account.Controllers" }
              );
            RouteTable.Routes.MapRoute(
                 name: "SignOut",
                 url: "logout",
                 defaults: new { controller = "SALogout", action = "LogOff", },
                 namespaces: new[] { "Loreal.Feature.Account.Controllers" }
             );
            RouteTable.Routes.MapRoute(
                 name: "OAuth",
                 url: "oauth",
                 defaults: new { controller = "SocialConnector", action = "Authorize", },
                 namespaces: new[] { "Loreal.Feature.Account.Controllers" }
             );
            RouteTable.Routes.MapRoute(
                name: "ImportUser",
                url: "importuser",
                defaults: new { controller = "Profile", action = "ImportUser", },
                namespaces: new[] { "Loreal.Feature.Account.Controllers" }
            );
            RouteTable.Routes.MapRoute("profile", "saapi/{controller}/{action}", new { controller = "profile", action = "changepassword" });
            RouteTable.Routes.MapRoute("mynotification", "saapi/{controller}/{action}", new { controller = "notification", action = "list" });
        }
    }
}