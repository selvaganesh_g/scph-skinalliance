﻿using Sitecore.Common;
using Sitecore.Diagnostics;
using Sitecore.Security.Accounts;
using System.Collections.Generic;
using System.Web.Security;

namespace Loreal.SkinAlliance.Web.Pipelines
{
    public class CustomUserProvider : UserProvider
    {
        protected override IEnumerable<User> GetUsersByName(int pageIndex, int pageSize, string pattern)
        {
            int num;

            Assert.ArgumentNotNull(pattern, "pattern");

            if (pattern.ToLower().Contains("ms"))
            {
                pattern = pattern.Trim().Replace("ms:", "").Replace("*", "");

                if (string.IsNullOrEmpty(pattern))
                    return new User[] { };
                var users = new List<User>();

                var membershipUsers = Membership.GetAllUsers();
                foreach (MembershipUser membershipUser in membershipUsers)
                {
                    var user = User.FromName(membershipUser.UserName, false);
                    if (user.Profile.GetCustomProperty("MedicalSpecialty").Contains(pattern))
                    {
                        users.Add(user);
                    }
                }
                return users;
            }
            if (pattern.ToLower().Contains("cc"))
            {
                pattern = pattern.Trim().Replace("cc:", "").Replace("*", "");

                if (string.IsNullOrEmpty(pattern))
                    return new User[] { };
                var users = new List<User>();

                var membershipUsers = Membership.GetAllUsers();
                foreach (MembershipUser membershipUser in membershipUsers)
                {
                    var user = User.FromName(membershipUser.UserName, false);
                    if (user.Profile.GetCustomProperty("Country").Contains(pattern))
                    {
                        users.Add(user);
                    }
                }
                return users;
            }
            if (pattern.ToLower().Contains("lang"))
            {
                pattern = pattern.Trim().Replace("lang:", "").Replace("*", "");

                if (string.IsNullOrEmpty(pattern))
                    return new User[] { };
                var users = new List<User>();

                var membershipUsers = Membership.GetAllUsers();
                foreach (MembershipUser membershipUser in membershipUsers)
                {
                    var user = User.FromName(membershipUser.UserName, false);
                    if (user.Profile.GetCustomProperty("PreferredLanguage").Contains(pattern))
                    {
                        users.Add(user);
                    }
                }
                return users;
            }
            else
            {
                return new Enumerable<User>(() => Membership.FindUsersByName(pattern, pageIndex, pageSize, out num).GetEnumerator(), (object o) => User.FromName(((MembershipUser)o).UserName, false));
            }
        }
    }
}