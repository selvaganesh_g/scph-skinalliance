﻿using Sitecore.Data.Items;

namespace Loreal.SkinAlliance.Web.CutomFieldTypes
{
    public class TreeListSAEx : Sitecore.Shell.Applications.ContentEditor.TreeList
    {
        protected override string GetHeaderValue(Item item)
        {
            return item.Paths.FullPath.Replace(base.DataSource, "").TrimStart('/');
        }

    }
}