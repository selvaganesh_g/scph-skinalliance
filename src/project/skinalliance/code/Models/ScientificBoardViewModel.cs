﻿using System.Collections.Generic;

namespace Loreal.SkinAlliance.Web.Models
{
    public class ScientificBoardViewModel : ScientificBoardModel
    {

        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ReadMore { get; set; }
        public string ReadMoreLink { get; set; }
        public List<string> BannerImage { get; set; }
        public List<ScientificBoardModel> List { get; set; }
    }
}