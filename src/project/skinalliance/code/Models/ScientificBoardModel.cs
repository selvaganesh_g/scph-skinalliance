﻿using Sitecore.XA.Foundation.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.SkinAlliance.Web.Models
{
    public class ScientificBoardModel : RenderingModelBase
    {
        public string Name { get; set; }
        public string Position { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string LargeImage { get; set; }
        public string Link { get; set; }
    }
}