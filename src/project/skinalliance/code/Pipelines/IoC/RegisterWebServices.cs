﻿using Loreal.SkinAlliance.Web.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.XA.Foundation.IOC.Pipelines.IOC;

namespace Loreal.SkinAlliance.Web.Pipelines.IoC
{
    public class RegisterWebServices : IocProcessor
    {
        public override void Process(IocArgs args)
        {
            args.ServiceCollection.AddTransient<IScientificBoardRepository, ScientificBoardRepository>();

        }
    }
}