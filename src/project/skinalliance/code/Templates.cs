﻿using Sitecore.Data;

namespace Loreal.SkinAlliance.Web
{
    public class Templates
    {
        public struct ScientificBoard
        {
            public static readonly ID ID = new ID("{22B71786-3514-4A91-9AD0-93275F1BDDBA}");
            public struct Fields
            {
                public static readonly ID Name = new ID("{3918C363-2D85-401F-BFCF-763758FF8E11}");
                public static readonly ID Position = new ID("{CDF66871-B9F5-4EBD-B141-8970F6BF6A82}");
                public static readonly ID Description = new ID("{CBC2F060-BA5E-43A5-B606-DAE46B31B325}");
                public static readonly ID Image = new ID("{4DA56C53-F1F7-4DBF-A0E1-CFEA9D93D935}");
                public static readonly ID ProfileId = new ID("{06B83018-EA8D-4F16-96E5-338441356BC5}");
                public static readonly ID LargeImage = new ID("{3F8A0763-6EC5-4A3C-81B2-0C352488D5C6}");

            }
        }
        public struct ScientificBroadFolder
        {
            public static readonly ID ID = new ID("{8C960FFD-8335-4912-B5FC-74BF092F0247}");
            public struct Fields
            {
                public static readonly ID Title = new ID("{2858B487-AAC4-463B-BDFC-EB3E93426EA3}");
                public static readonly ID ShortDescription = new ID("{F33CEABB-872F-4399-8097-967C12A302C4}");
                public static readonly ID Description = new ID("{875CD3D5-1665-4F43-BA6F-E8573DC4A4B1}");
                public static readonly ID ReadMoreText = new ID("{0E6320F7-22C8-438A-92A2-F3C7BC68B0CD}");
                public static readonly ID ReadMoreLink = new ID("{BAF297ED-5BEA-4926-849C-AFFC9D423814}");
                public static readonly ID Image = new ID("{4B5A5A09-8759-47C2-945E-E26D9AA2C348}");
            }
        }
    }
}
