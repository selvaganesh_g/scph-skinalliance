﻿using Sitecore.Data.Items;
using Sitecore.XA.Foundation.IoC;
using Sitecore.XA.Foundation.Multisite;
using System.Collections.Generic;
using Sitecore.Globalization;
using System.Globalization;
using Sitecore.Collections;
using Sitecore.Data.Managers;

namespace Loreal.SkinAlliance.Web
{
    public static class Utils
    {
        public static List<KeyValuePair<string, string>> GetLanguages()
        {
            List<KeyValuePair<string, string>> languages = new List<KeyValuePair<string, string>>();
            LanguageCollection languageCollection = LanguageManager.GetLanguages(Sitecore.Context.ContentDatabase ?? Sitecore.Context.Database);

            List<CultureInfo> cultureInfos = new List<CultureInfo>();
            foreach (Language lang in languageCollection)
            {
                languages.Add(new KeyValuePair<string, string>(lang.CultureInfo.DisplayName, lang.CultureInfo.Name));
            }
            return languages;
        }
        public static string GetCurrentLanguage()
        {
            return Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName;
        }
    }
}