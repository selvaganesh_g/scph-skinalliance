﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Loreal.SkinAlliance.Web.Repositories
{
    public interface IScientificBoardRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
    }
}
