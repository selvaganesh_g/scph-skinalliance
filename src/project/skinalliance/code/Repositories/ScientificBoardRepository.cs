﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loreal.SkinAlliance.Web.Repositories
{
    using Models;
    using Sitecore.Data.Fields;
    using Sitecore.Web.UI.WebControls;

    public class ScientificBoardRepository : ModelRepository, IScientificBoardRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            ScientificBoardViewModel model = new Models.ScientificBoardViewModel();
            base.FillBaseProperties(model);
            List<ScientificBoardModel> list = new List<ScientificBoardModel>();
            if (this.Rendering.DataSourceItem == null)
            {
                model.List = list;
                return model;
            }
            var dataSource = this.Rendering.DataSourceItem;
            model.Title = FieldRenderer.Render(dataSource, dataSource.Fields[Templates.ScientificBroadFolder.Fields.Title]?.Name);
            model.ShortDescription = FieldRenderer.Render(dataSource, dataSource.Fields[Templates.ScientificBroadFolder.Fields.ShortDescription]?.Name);
            model.LongDescription = FieldRenderer.Render(dataSource, dataSource.Fields[Templates.ScientificBroadFolder.Fields.Description]?.Name);
            model.ReadMore = dataSource.Fields[Templates.ScientificBroadFolder.Fields.ReadMoreText]?.Value;
            LinkField linkField = dataSource.Fields[Templates.ScientificBroadFolder.Fields.ReadMoreLink];
            model.ReadMoreLink = linkField?.GetFriendlyUrl();
            List<string> bannerImages = new List<string>();
            foreach (var item in this.Rendering.DataSourceItem.Children.Where(x => x.TemplateID == Templates.ScientificBoard.ID))
            {
                list.Add(new ScientificBoardModel()
                {
                    Name = FieldRenderer.Render(item, item.Fields[Templates.ScientificBoard.Fields.Name]?.Name),
                    Description = FieldRenderer.Render(item, item.Fields[Templates.ScientificBoard.Fields.Description]?.Name),
                    Position = FieldRenderer.Render(item, item.Fields[Templates.ScientificBoard.Fields.Position]?.Name),
                    Image = FieldRenderer.Render(item, item.Fields[Templates.ScientificBoard.Fields.Image]?.Name),
                    LargeImage = FieldRenderer.Render(item, item.Fields[Templates.ScientificBoard.Fields.LargeImage]?.Name),
                    Link = item.Fields[Templates.ScientificBoard.Fields.ProfileId]?.Value
                });
                bannerImages.Add(FieldRenderer.Render(item, item.Fields[Templates.ScientificBoard.Fields.Image]?.Name, "class=img-responsive board-mem"));
            }
            model.BannerImage = bannerImages;
            model.List = list;
            return model;
        }
    }
}