import Vue from 'vue';
import VueResource from 'vue-resource';
import KeySiteCore from 'key-sitecore-vue-plugin';
import 'jquery-validation';
import 'jquery-slimscroll';
import 'dropify/dist/js/dropify.min';
import './plugins/multislider'; //for category items slider
import './plugins/jquery-valiadation-additional'; //for category items slider
import './plugins/editor'; //for category items slider
import SignUp from './modules/SignUp/SignUp';
import SignIn from './modules/SignIn/SignIn';
import './modules/NavMenu/NavMenu';
import WelcomeBanner from './Components/WelcomeBanner/WelcomeBanner.vue';
import InnerTitle from './Components/InnerTitle/InnerTitle.vue';
import PostLists from './Components/PostLists/PostLists.vue';
import Previews from './Components/Previews/Previews.vue';
import './modules/categorySlider/categorySlider';
import './modules/MypicUpload/MypicUpload';
import './modules/MyProfile/MyProfile';
import './modules/SAslimSroll/SAslimSroll'; 
import './modules/DirectoryListing/DirectoryListing';
import './modules/Webcasting/Webcasting';
import './modules/Footer/Footer';
import ContactUs from './modules/ContactUs/ContactUs';
import Comment from './modules/Comment/comment';
import './modules/CreatePost/CreatePost'; 
import './modules/UploadAvatar/UploadAvatar';
import './modules/articleDetailSurvey/articleDetailSurvey';
import ArticleList from './modules/articlelist/articlelist';
import MyNotification from './modules/mynotification/mynotification';



Vue.use(KeySiteCore);
Vue.use(VueResource);
/**
 * Add WelcomeBanner as global Component
 */
Vue.component(WelcomeBanner.name, WelcomeBanner);
Vue.component(InnerTitle.name, InnerTitle);
Vue.component(PostLists.name, PostLists);
Vue.component(Previews.name, Previews);

import {
    Carousel
} from './directives/carousel/carousel'; 

window.$ = $;

import bootstrapSass from 'bootstrap-sass';

const SignUpRef = new SignUp();
const SignInRef = new SignIn();
const ContactUsRef = new ContactUs();
const CommentRef = new Comment();
const articleListRef = new ArticleList();
const MyNotificationRef = new MyNotification();

const App = new Vue({
	el: '#wrapper',
	directives: {
	  Carousel
	}
});




