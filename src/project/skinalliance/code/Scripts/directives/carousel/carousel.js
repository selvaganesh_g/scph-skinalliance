/**
 * @module Directives/Carousel
 * @desc Carousel Custom Directives
 */

const carouselOptions = {
    interval: false
};

/** Carousel Module */
export const Carousel = {
    /**
     * @function bind the elements
     * @desc bind the elements of custom directives
     * @param  {element} el
     */
    bind: (el, binding) => {
        const $el = $(el);
        const options = Object.assign({}, carouselOptions, binding.value);

        setTimeout(() => {
            $el.carousel(options);
        }, 0);
    },
    data: {

    },
};
