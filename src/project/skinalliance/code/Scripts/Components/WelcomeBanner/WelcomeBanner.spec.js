﻿import Vue from 'vue';
import WelcomeBanner from './WelcomeBanner.vue';

// helper function that mounts and returns the rendered text
function getRenderedText(Component, propsData) {
	const Ctor = Vue.extend(Component)
	const vm = new Ctor({ propsData }).$mount()
	return vm.$el.textContent.trim();
}

describe('WelcomeBanner.vue', () => {

	it('should have props', () => {
		expect(typeof WelcomeBanner.props).toBe('object');
	})

	it('renders correctly with different props', () => {
		expect(getRenderedText(WelcomeBanner, {
			title: 'SKinAlliance'
		})).toBe('SKinAlliance')		
	})
});
