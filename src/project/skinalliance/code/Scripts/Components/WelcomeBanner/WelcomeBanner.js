﻿/**
 * @module Component/WelcomeBanner
 * @desc to display the welcome banner
 */

export default {
/**
 * @member {type} name tag name of the component
 * @readonly
 */
name: 'WelcomeBanner',
    props: {
        /**
         * @member {string} title
         * @desc title of the page
         */
    title: {
                type: String,
                required: true,
                }
    },

data: () => {
    return {

    };
},
};
