import Vue from 'vue';
import Previews from './Previews.vue';

// helper function that mounts and returns the rendered text
function getRenderedText(Component, propsData) {
	const Ctor = Vue.extend(Component)
	const vm = new Ctor({ propsData }).$mount()
	return vm.$el.textContent.trim();
}

describe('Previews.vue', () => {

	it('should have methods', () => {
		expect(typeof Previews.showPreview).toBe('object');
	})
});
