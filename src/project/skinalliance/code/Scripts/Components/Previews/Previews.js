/**
 * @module Component/CreatePostPreview
 * @desc to display the preview of create post
 */

export default {
/**
 * @member {type} name tag name of the component
 * @readonly
 */
name: 'Previews',
    props: {
        /**
         * @member {string} title
         * @desc title of the page
         */
		title: {
			type: String,
			required: true,
		}/*  ,
		
		createPostData: {
			type: Object,
			required: false
		}  */
    },
	
	methods: {
		showPreview(){
			this.createPostData = JSON.parse(localStorage.getItem('previewObject'));
		}
	},
		
	 /**
	   * @function created
	   * @desc on mounting,initiate the function
	   */
	created () {    
		this.showPreview();
	},
  
	/**
	   * @function data
	   * @desc init the data when the component mount
	   */
	data: () => {
		return {
			
		};
	}
	
};
