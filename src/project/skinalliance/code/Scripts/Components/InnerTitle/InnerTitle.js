﻿/**
 * @module Component/InnerTitle
 * @desc to display the inner title
 */

export default {
/**
 * @member {type} name tag name of the component
 * @readonly
 */
name: 'InnerTitle',
    props: {
        /**
         * @member {string} title
         * @desc title of the page
         */
    title: {
                type: String,
                required: true,
                }
    },

data: () => {
    return {

    };
},
};
