﻿import Vue from 'vue';
import InnerTitle from './InnerTitle.vue';

// helper function that mounts and returns the rendered text
function getRenderedText(Component, propsData) {
	const Ctor = Vue.extend(Component)
	const vm = new Ctor({ propsData }).$mount()
	return vm.$el.textContent.trim();
}

describe('InnerTitle.vue', () => {

	it('should have props', () => {
		expect(typeof InnerTitle.props).toBe('object');
	})

	it('renders correctly with different props', () => {
		expect(getRenderedText(InnerTitle, {
			title: 'ABOUT US'
		})).toBe('ABOUT US')		
	})
});
