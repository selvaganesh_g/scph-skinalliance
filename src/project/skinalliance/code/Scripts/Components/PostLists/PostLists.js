/**
 * @module Component/PostLists
 * @desc to display the latest post, favourite post and News
 */

export default {
/**
 * @member {type} name tag name of the component
 * @readonly
 */
name: 'PostLists',
    props: {
        /**
         * @member {string} title
         * @desc title of the page
         */
		title: {
			type: String,
			required: true,
		}
    },
	
methods: {
	 /**
     * @function getPostDetails
     * @desc get the post details based on the category.
     */
    getPostDetails ()  { 
		const category = this.title;
		this.$http.get('http://www.mocky.io/v2/58f0d1741000006a19a14f4c?offset=1&count=5&categoryId=1').then(response => {
			
			if(category === "Latest Post"){
				// get body data
				this.postList = {
					"details":[
						{
							"pDate": "01/June/2017",
							"src":"/themes/SkinAlliance/images/assets/postimg0.png",
							"url": "#",
							"content":"Dr Enzo Berardesca on skin and pollution reporting from EADV meeting",
							"star": 2,
							"comment": 2
							
						},
						{
							"pDate": "01/June/2017",
							"src":"/themes/SkinAlliance/images/assets/postimg1.png",
							"url": "#",
							"content":"Dr Enzo Berardesca on skin and pollution reporting from EADV meeting",
							"star": 2,
							"comment": 2
						}
					]
				};
			} else if(category === "Favorite Post"){
				// get body data
				this.postList = {
					"details":[
						{
							"pDate": "01/June/2017",
							"src":"/themes/SkinAlliance/images/assets/postimg2.png",
							"url": "#",
							"content":"Dr Enzo Berardesca on skin and pollution reporting from EADV meeting",
							"star": 2,
							"comment": 2							
						},
						{
							"pDate": "01/June/2017",
							"src":"/themes/SkinAlliance/images/assets/postimg3.png",
							"url": "#",
							"content":"Dr Enzo Berardesca on skin and pollution reporting from EADV meeting",
							"star": 2,
							"comment": 2
						}
					]
				};
			} else {
				// get body data
				this.postList = {
					"details":[
						{
							"pDate": "01/June/2017",
							"src":"/themes/SkinAlliance/images/assets/postimg4.png",
							"url": "#",
							"content":"Dr Enzo Berardesca on skin and pollution reporting from EADV meeting",
							"star": 2,
							"comment": 2
						},
						{
							"pDate": "01/June/2017",
							"src":"/themes/SkinAlliance/images/assets/postimg5.png",
							"url": "#",
							"content":"Dr Enzo Berardesca on skin and pollution reporting from EADV meeting",
							"star": 2,
							"comment": 2

						}
					]
				};
				
				$("#post-value").css("display","block");
				
				
			}
		}, response => {
			// error callback
		});  
	}
},
	
 /**
   * @function created
   * @desc on mounting,initiate the function
   */
created () {    
    this.getPostDetails();
}, 
  
/**
   * @function data
   * @desc init the data when the component mount
   */
data: () => {
    return {
		postList : []
    };
}
	
};
