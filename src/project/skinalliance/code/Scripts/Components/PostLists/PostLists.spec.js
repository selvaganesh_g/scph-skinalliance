import Vue from 'vue';
import PostLists from './PostLists.vue';

// helper function that mounts and returns the rendered text
function getRenderedText(Component, propsData) {
	const Ctor = Vue.extend(Component)
	const vm = new Ctor({ propsData }).$mount()
	return vm.$el.textContent.trim();
}

describe('PostLists.vue', () => {

	it('should have props', () => {
		expect(typeof PostLists.props).toBe('object');
	})

	it('renders correctly with different props', () => {
		expect(getRenderedText(PostLists, {
			title: 'Latest Post'
		})).toBe('Latest Post View All')		
	})
});
