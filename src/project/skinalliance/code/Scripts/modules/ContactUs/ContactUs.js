/**
 * @module Modules/ContactUs
 * @desc ContactUs page validation
 */

export default class ContactUs {
    /**
     * ContactUs
     * @constructor
     */
    constructor() {
        this.init();
    }

    /**
     * Init the validation methods and bind the events
     * @return {object} The validation class and methods
     */
    init() {this.postRender();}

	postRender() {
        /**
         * @function document ready
         * @desc Once document loaded then this function would be triggred
         */
	var jqry = jQuery.noConflict();
	(function(jqry){
		jqry(document).ready(function(){		
		
			jqry('#contactus_submit').on('click', () => {
				let invNumber = jqry("#contact_phonenumber").data("invalidphonenumber");
				let invalidEmailMsg = jqry("#contact_email").attr("data-invalidemail");	
				//For complete email validation
			    jqry.validator.addMethod("validateEmail",function(value, element) {
				    if(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test( value )){
					   return true;
					} else {
					  return false;
					}    
			   },invalidEmailMsg);
			   
			    jqry.validator.addMethod("validNumber", function(phoneNumber, element) {
					phoneNumber = phoneNumber.replace( /\s+/g, "" );
				return this.optional( element ) || phoneNumber.length > 9 &&
					phoneNumber.match(/^[0-9-+()]{7,18}$/);
				},invNumber);
				
				jqry("#contactusform").validate({
					// Trim the value of every element
					normalizer: function(value) {
						return $.trim(value);
					},
					rules : {
						Email: {
							required: true,
							validateEmail: true
						}, 
						FirstName: {
							required: true							
						},
						LastName: {
							required: true							
						},
						PhoneNumber: {
							validNumber: true
						},
						Message: {
							required: true							
						}					
					},
					messages: {
						FirstName: jqry("#contact_firstname").data("firstname"),
						LastName: jqry("#contact_lastname").data("lastname"),
						PhoneNumber: {
							remote: true
						},
						Email: {
							required: jqry("#contact_email").data("email"),
							remote: true	
						},
						Message: jqry("#contact_message").data("message")
					}
				});
			});
		});
	})(jqry);}
}