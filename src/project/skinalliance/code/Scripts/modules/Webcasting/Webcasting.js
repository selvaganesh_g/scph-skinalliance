         var sliderTimeArray = []; //new FormData();
		 
		 var $v = jQuery.noConflict();
		(function($v){
			$v(document).ready(function(){
			  
				$v('#webcast-slider .thumb').each(function(index){
					let sliderTime = parseInt($(this).attr('data-video-time'));
					sliderTimeArray.push(sliderTime);
				});	
				
				if(document.querySelector('iframe') !== null) {
					const iframe = (/iPad|iPhone/i.test(navigator.userAgent)) ? document.querySelector('#videos #player') : document.querySelector('iframe');
					const player = new Vimeo.Player(iframe);
					
					 //Vimeo player default play option control
					 player.on('play', function() {
						player.setVolume(1);
						$v('.button_player.play').removeClass('play').addClass('pause');
						$v('button.fullscreen').remove();
					 });

					  //Vimeo player default pause option control
					 player.on('pause', function() {
						$v('.button_player.pause').removeClass('pause').addClass('play');
					 });
	 
					 //For auto slide updating based on video play
					 player.on('timeupdate', function(data) {  
						let currentVideoTime = parseInt(data.seconds);
						let webCastSliderId = ($('#webcast-slider').is(':visible')) ? 'webcast-slider' :  ($('#mwebcast-slider').is(':visible') ? 'mwebcast-slider' : 'dwebcast-slider');
						if($.inArray(currentVideoTime, sliderTimeArray) !== -1){
							let selectedSlide  = $v('#'+webCastSliderId).find("[data-video-time='" + currentVideoTime + "']").attr('id').split('_')[1];
							$v('.media-carousel .thumb a').css('border','none');
							$v('.selected').removeClass('selected');
							$v('#slide_'+selectedSlide+', '+'#icon_'+selectedSlide).addClass('selected');
							$v('.media-carousel .thumb.selected a').css('border','2px solid #00ACC2');
						}
					 }); 
					
					 //For slide updating based on video forward	
					 player.on('seeked', function(data) {  
						let currentVideoTime = parseInt(data.seconds);
						let webCastSliderId = ($('#webcast-slider').is(':visible')) ? 'webcast-slider' :  ($('#mwebcast-slider').is(':visible') ? 'mwebcast-slider' : 'dwebcast-slider');
						let sliderTime = Math.max.apply(Math, sliderTimeArray.filter(function(x){return x <= currentVideoTime}));
						let selectedSlide  = $v('#'+webCastSliderId).find("[data-video-time='" + sliderTime + "']").attr('id').split('_')[1];
						$v('.media-carousel .thumb a').css('border','none');
						$v('.selected').removeClass('selected');
						$v('#slide_'+selectedSlide+', '+'#icon_'+selectedSlide).addClass('selected');
						$v('.media-carousel .thumb.selected a').css('border','2px solid #00ACC2');	
					 }); 
					
					//Slider thumbnail click event
					$v('#webcast-slider .thumb, #mwebcast-slider .thumb, #dwebcast-slider .thumb').click(function(){
						let selectedSlide = $v(this).attr('id').split('_')[1];
						let selectedSlideTime = $v('#icon_'+selectedSlide).attr('data-video-time');
						$v('.media-carousel .thumb a').css('border','none');
						$v('.selected').removeClass('selected');
						$v('#slide_'+selectedSlide+', '+'#icon_'+selectedSlide).addClass('selected');
						$v('.media-carousel .thumb.selected a').css('border','2px solid #00ACC2');
						player.setCurrentTime(parseInt(selectedSlideTime)).catch(function() {
							//console.log("Fast forward");
						});
					});
					
					jQuery('#webcast-slider, #mwebcast-slider, #dwebcast-slider').carousel({
						pause: true,
						interval: false,
						wrap: false
					 });
					
					//To pause the video when clicking modal close button		
					 $v('#webCasting .modal-header .close').on('click',function(){
						 player.pause();
					 });  

					//Customized video next and previous control event
					//Customized video next and previous control event
					$v('.list_controls .next, .list_controls .previous').click(function(){
						//console.log("selctedButton", $v(this).hasClass('next'));
						let activeSlideId = $v('#webCasting .selectable.selected').attr('id');
						if(activeSlideId === undefined){
							return;
						}
						activeSlideId = (activeSlideId.split('_')[1] !== '')?parseInt(activeSlideId.split('_')[1]):'';
						if($v(this).hasClass('next') && ($v('#webCasting .thumb img').length === activeSlideId + 1)){
							//console.log("Next Event Stopped");
							return;
						}
						if($v(this).hasClass('previous') && activeSlideId === 0){
							//console.log("Previous Event Stopped");
							return;
						}
						let nextSlide =  ($v(this).hasClass('next')) ? parseInt(activeSlideId)+1 : parseInt(activeSlideId)-1;
						let nextSlideTime = $v('#icon_'+nextSlide).attr('data-video-time');
						$v('.media-carousel .thumb a').css('border','none');
						$v('.selected').removeClass('selected');
						$v('#slide_'+nextSlide+', '+'#icon_'+nextSlide).addClass('selected');
						$v('.active').removeClass('active');
						$v('#icon_'+nextSlide).parents().addClass('active');
						$v('.media-carousel .thumb.selected a').css('border','2px solid #00ACC2');
						player.setCurrentTime(nextSlideTime).catch(function() {
							//console.log("HI");
						});
					});

					//Customized vimeo video play, pause control event
					$v('.button_player').on('click', function(){
						if ($v(this).hasClass('play')){
							player.play();
							player.setVolume(1);
							$v(this).removeClass('play').addClass('pause');
						}
						else if ($v(this).hasClass('pause')){
							player.pause();
							$v(this).removeClass('pause').addClass('play');
						}
						else if ($v(this).hasClass('sound')){
							player.setVolume(0);
							$v(this).removeClass('sound').addClass('mute');
						}
						else if ($v(this).hasClass('mute')){
							player.setVolume(1);
							$v(this).removeClass('mute').addClass('sound');
						} else {
							player.play();
						}
					});
					

					//For ipad video load issue
					jQuery('#webCasting').on('shown.bs.modal', function() {
						if(/iPad|iPhone/i.test(navigator.userAgent)){
							$v('#webcast-iframe-desktop').addClass('hidden');
							$v('#webcast-mobile').removeClass('hidden');
							$v('#webCasting .mover_controls').css('visibility','hidden');
							let vimeoSrc =  $v('#videos #player').attr('src');	
							$v('#videos #player').attr('src',vimeoSrc);	
						} else {
							$v('#webcast-mobile').addClass('hidden');
							$v('#webcast-iframe-desktop').removeClass('hidden');
						}
					});				
				}		 		 
			});
	})($v);
