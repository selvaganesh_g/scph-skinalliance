/**
 * @module Modules/SocialRegister
 * @desc SocialRegister Validation and form submit
 */

export default class SocialRegister {
    /**
     * Navigation Menu
     * @constructor
     */
    constructor() {
        this.init();
    }

    /**
     * Init the validation methods and bind the events
     * @return {object} The validation class and methods
     */
    init() {		
		this.postRender();
	}

	postRender() {
        /**
         * @function document ready
         * @desc Once document loaded then this function would be triggred
         */
		var $ = jQuery.noConflict();
		(function($){ 		 
			$(document).ready(() => {			
							 
			});
		})($);
    }
}
