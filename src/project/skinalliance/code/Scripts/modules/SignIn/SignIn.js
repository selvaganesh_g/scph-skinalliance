/**
 * @module Modules/SignIn
 * @desc SignIn Validation
 */

export default class SignIn {
    /**
     * Navigation Menu
     * @constructor
     */
    constructor() {
        this.init();
    }

    /**
     * Init the validation methods and bind the events
     * @return {object} The validation class and methods
     */
    init() {
		this.postRender();
	}

	postRender() {
        /**
         * @function document ready
         * @desc Once document loaded then this function would be triggred
         */
		var $ = jQuery.noConflict();
		(function($){ 		 
			$(document).ready(() => {		
			
				var validEmail = $("#email").attr("data-invalid")
			
				//For complete email validation
				$.validator.addMethod("validateEmail",function(value, element) {
				    if(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test( value )){
					   return true;
					} else {
					  return false;
					}    
			   }, validEmail);
			
				$('#login_submit').on('click', () => {
					$("#signInForm .error-message").css("display","none");
					$("#signInForm").validate({	
						// Trim the value of every element
						normalizer: function(value) {
							return $.trim(value);
						},
						rules : {
							Username: {
								required: true,
								validateEmail: true
							}
						},
						messages: {
							Username: $("#login_email").data("uname"),
							Password: $("#login_password").data("upassword")
						}
					});
				});
				
				$('#send-btn').on('click', () => {		
					$('.ForgotPassword .error-message').css('display', 'none');
					$('.ForgotPassword .success-message').css('display', 'none');
					$("#forgotPassword").validate({	
						// Trim the value of every element
						normalizer: function(value) {
							return $.trim(value);
						},
						rules : {
							email: {
								required: true,
								validateEmail: true
							}
						},
						messages: {
							email: {
								required: $("#email").data("required"),
								remote: true
								
							}
						}
					});
					
				});
				
				
				
			});
		})($);
    }
}
