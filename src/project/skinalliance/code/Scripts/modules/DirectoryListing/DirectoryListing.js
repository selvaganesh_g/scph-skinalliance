/**
 * @module Modules/Directory
 * @desc Directory listing based on country 
 */	
$(document).ready(function(){	
	/**
     * @function doFilter
     * @desc get the directory details based on country and speciality filter.
	 */
	function doFilter(){
		$('.Dir-List').hide();
		$('.Dir-List li').hide();
		let i=0, cCode = '';
		if($('.country-checkbox:checked').length){
			$('.country-checkbox').each(function() {
			if($(this).is(':checked')){
				cCode = 'li.country-code-'+$(this).data('country-code');
				if(specialityList.length){
					$(specialityList).each(function(key,val){
						let dirList = cCode + '.speciality-code-'+val;
						$(dirList).parents('.Dir-List').show();
						$(dirList).show();	
					});
				}
				else{
					$(cCode).parents('.Dir-List').show();
					$(cCode).show();
				}
				i++;
			}
			
		});
		}
		else {
			$(specialityList).each(function(key,val){
				cCode = '.speciality-code-'+val;
				$(cCode).parents('.Dir-List').show();
				$(cCode).show();
				i++;
			});
		}
		 
		if(i===0){ 
			$('.Dir-List').show();
			$('.Dir-List li').show();
		}
		
		if($('.DirectoryList .Dir-List:visible').length === 0) {
			$('.no-result-found').css('display','block');
			$('.dir-expand').css('display','none');
			$('.dir-collapse').css('display','none');
		} else {
			$('.no-result-found').css('display','none');
			$('.dir-expand').css('display','inline-block');
			$('.dir-collapse').css('display','inline-block');
		} 
		
	}
	let specialityList = [];
	$('.dir-user-country').change(function() {
		doFilter();
	});
	
	//Event to get directory details based on country and speciality filter
	$('.dir-user-speciality').change(function() {
		if($(this).is(':checked')){
			if(specialityList.indexOf($(this).data('speciality-code')) === -1){
				specialityList.push($(this).data('speciality-code'));
			}			
		}
		else{
			const spIndex = specialityList.indexOf($(this).data('speciality-code'));
			if(spIndex !== -1){
				specialityList.splice(spIndex, 1);
			}
		}	
		//Trigger the filtering 
		 doFilter();
	});
	
//Expand and collapse for each item
	$(".expcollap").click(function () {
		let dirListToggleDiv = $(this).parent().next().attr('id');		
		//Toggle image change
		$('#'+dirListToggleDiv).slideToggle( 'slow', function(){ 
			 let expCollapDiv = $(this).parent().find('.expcollap');
			 if (expCollapDiv.hasClass('fa-angle-down')) {
				expCollapDiv.removeClass( "fa-angle-down" ).addClass("fa-angle-up");
			 } else {
				 expCollapDiv.removeClass( "fa-angle-up" ).addClass("fa-angle-down");
			 }	
        }); 
	});

	
	$(window).scroll(function() {
		$.each($('img'), function() {
			if ( $(this).attr('data-src') && $(this).offset().top < ($(window).scrollTop() + $(window).height() + 100) ) {
				var source = $(this).data('src');
				$(this).attr('src', source);
				$(this).removeAttr('data-src');
			}
		})
	})
	//Total Expand
	$('.dir-expand').click(function () {
		$('.dir-item-list').slideDown("slow");
		$(".expcollap").removeClass( "fa-angle-up" ).addClass( "fa-angle-down");
	});
	
	//Total Collapse
	$('.dir-collapse').click(function () {
		$('.dir-item-list').slideUp("slow")
		$(".expcollap").removeClass( "fa-angle-down" ).addClass( "fa-angle-up");
	});
	
	if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
		jQuery('#countryFilter').collapse('hide');
		jQuery('#countrySpeciality').collapse('hide');
	}

	
});