$(document).ready(function(){
	var clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");
	$('.footer li').on(clickHandler, function(e){
		if($(this).parents().find('.footer').attr('data-isloggedin') === "False"){
			jQuery('#redirectConfirmModal').modal('show');
			e.preventDefault();
		} else {
			let IS_IPHONE = (navigator.userAgent.match(/iPhone/i) != null) || (navigator.userAgent.match(/iPod/i) != null);
			if (IS_IPHONE) {
				$('a').on(clickHandler, function() { 
					var link = $(this).attr('href');   
					if($(this).attr('target') !== ''){
						window.open(link,'_blank'); // opens in new window as requested
					} else {
						window.location.href = link;
					}
					return false; // prevent anchor click    
				});     
			}	
		}		
	});
    
	$("#terms_of_use_agree").on('click',function(){
		window.open($(this).attr('href'),'_blank'); // opens in new window as requested
    });

//if( $(document).height() < $(window).height() )
//{    $('body').height    (
//        $(window).height - $('.footer').height()
//    );
//}


   let docHeight = $(window).height();
   let footerHeight = $('.footer').height();
   if($('.footer').length > 0){
		let footerTop = $('.footer').position().top + footerHeight;
		if (footerTop < docHeight) {
			$('.footer').css('margin-top', 10+ (docHeight - footerTop) + 'px');
		}
   }
});	

 $(document).ready(function(){
        var ua = navigator.userAgent;
        var browser = "unsupported";
        if (ua.indexOf('Android') === -1 && ua.indexOf('iPad') === -1 && ua.indexOf('iPhone') === -1) {
            if (ua.indexOf('Firefox') !== -1) {
                browser = 'firefox';
            }
            else if (ua.indexOf('Chrome') !== -1) {
                browser = 'chrome';
            }
            else if (ua.indexOf('Safari') !== -1) {
                browser = 'safari';
            }
            else if (ua.indexOf('MSIE ') !== -1 || ua.indexOf('Trident/') !== -1) {
                browser = 'msie';
            }
        }
        
        $('.webbrowser').hide();
        $('#'+browser).show();
		jQuery('video').bind('contextmenu',function() { return false; });
});