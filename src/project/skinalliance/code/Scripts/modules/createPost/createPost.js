let jsonObject = {}, extensionImageUrl = {}; let Questionaire = []; let SubCategories = [], Categories=[];
var insImgContainer = []; //new FormData();
window.imageContainer ={
	count : 1,
};
var $t = jQuery.noConflict();
(function($){
	$t(document).ready(function(){
		let viewer = '';
		var uid='', ext = '';

		//editQuestionaire();

		extensionImageUrl = {
			'pdf': '/themes/SkinAlliance/images/assets/pdf-file.png',
			'txt': '/themes/SkinAlliance/images/assets/txt-text.png',
			'csv': '/themes/SkinAlliance/images/assets/text.png',
			'zip': '/themes/SkinAlliance/images/assets/text.png',
			'mp4': '/themes/SkinAlliance/images/assets/videoframe.png',
			'doc': '/themes/SkinAlliance/images/assets/doc-file.png',
			'docx': '/themes/SkinAlliance/images/assets/doc-file.png',
			'xlsx':  '/themes/SkinAlliance/images/assets/document.png',
			'ppt': '/themes/SkinAlliance/images/assets/ppt-file.png'
		};

		jQuery("#postEditor").Editor({
			'fonts':false,
			'styles':false,
			'font_size':false,
			'block_quote':false,
			'source':false
		});

		$t('#uploadFiles').hide();


		$t('#createPostForm .dropify').dropify({
		    tpl: {
		        'message':  '<div class="dropify-message"><span class="fa fa-plus fa-2x" /> <p>{{ default }}</p></div>',
				'filename': ''
		    },
		    messages: {
		    	'default': $t('#createPostForm .dropify').attr('data-default-label'),
		        'replace': $t('#createPostForm .dropify').attr('data-default-label'),
		        'remove':  'Remove',
		        'error':   ''
		    }
		});

		$t('#createPostForm .dropify').css('display','block');
		$t('#mediaDiv').css('display','block');

		//For delete Questions
		$t(document).on('click','ul.saved-questions-list li i',function(e) {
			e.stopPropagation()
			let questionIndex = $t(this).parent('li').index( "li" );
			//console.log("questionIndex >>>> ",questionIndex);
			Questionaire.splice(parseInt(questionIndex),1);
			//console.log("Questionaire >>>> ",JSON.stringify(Questionaire));
			$t(this).parent().remove();
			if($t('.saved-questions-list li').length === 0){
				$t('.saved-questions').removeClass('is-question-exist');
			}
			clearAllfields();
			$t('ul.saved-questions-list li').each(function(index, value){
				$t(this).attr('data-index',index);
			});
		});


		//For Select Thumbnail Image Issue Fix
		if($('#input-file-now.dropify').val() === "") {
			$('#createPostForm .dropify-preview').css('display', 'none')
		}

		//Add media upload for videos and images
		$t(".addmedia input:file").change(function (){
			uid = Math.floor(Math.random() * 26) + Date.now();
			let videoTag = '<div class="file-wrapper" data-imageIndex="'+window.imageContainer.count+'"><video id="video-'+uid+'" src="" style="display:block" controls></video><div class="file-name" id="videoname-'+uid+'"></div><span class="file-close addmedia-delete" title="Delete ?"><i class="fa fa-times"></span></div>';
			let imageTag = '<div class="file-wrapper" data-imageIndex="'+window.imageContainer.count+'"><img class="file-type-thumb" id="img-'+uid+'" src="" style="display:block"></img><div class="file-name" id="imagename-'+uid+'"></div><span class="file-close addmedia-delete" title="Delete ?"><i class="fa fa-times"></span></div>';
			let mediaFile = this.files[0];
			ext = mediaFile.name.substring(mediaFile.name.lastIndexOf('.') + 1).toLowerCase();
			if(ext === 'mp4' || ext === 'webm' || ext === 'ogv'){
				$t('#mediaList').append(videoTag);
			} else if(ext === 'png' || ext === 'jpg' || ext === 'jpeg' || ext === 'pdf' || ext === 'txt' || ext === 'docx' || ext === 'ppt') {
				$t('#mediaList').append(imageTag);
			} else {
				//console.log('Format not supported');
				return;
			}
			let fileReader = new FileReader();
			fileReader.onload = viewer.load;
			fileReader.readAsDataURL(mediaFile);
			viewer.setProperties(mediaFile);
			var files = $t("#file_1").get(0).files;
            if (files.length > 0) {
				var obj ={
					index :"file_"+ window.imageContainer.count,
					value : files[0],
					fileExtension: ext
				}
                insImgContainer.push(obj);
            }
            window.imageContainer.count += 1;
            $('#uploadFiles').show();
		});

	    viewer = {
			load: function(e){
				if(ext === 'mp4' || ext === 'webm' || ext === 'ogv'){
					$t('#video-'+uid).attr('src', e.target.result);
				} else if(ext === 'pdf'){
					$t('#img-'+uid).attr('src', '/themes/SkinAlliance/images/assets/pdf-file.png');
			    } else if(ext === 'docx' || ext === 'doc'){
					$t('#img-'+uid).attr('src', '/themes/SkinAlliance/images/assets/doc-file.png');
				} else if(ext === 'txt'){
					$t('#img-'+uid).attr('src', '/themes/SkinAlliance/images/assets/txt-text.png');
				} else if(ext === 'ppt'){
					$t('#img-'+uid).attr('src', '/themes/SkinAlliance/images/assets/ppt-file.png');
				} else {
					$t('#img-'+uid).attr('src', e.target.result);
				}
			},
			setProperties: function(mediaFile){
				if(ext === 'mp4' || ext === 'webm' || ext === 'ogv'){
					$t('#videoname-'+uid).text(mediaFile.name);
				} else {
					$t('#imagename-'+uid).text(mediaFile.name);
				}
				$t('#filename').text(mediaFile.name);
				$t('#filetype').text(mediaFile.type);
				$t('#filesize').text(Math.round(mediaFile.size/2048));
			},
		}

		//Question
		$t('#saveQuestion').click(function(e){
			e.preventDefault();
			let question = $.trim($t('#question').val());
			let questionID = $.trim($t('#question').attr('data-question-id'));
			let questValidate = $t('#question').attr('data-question');
			let answerValidate = $t('.answers-input-list').attr('data-answer');
			let checkboxValidate = $t('.answers-input-list').attr('data-checkbox');

			if(question === ""){
				$t('.quest-errmsg').text(questValidate);
			} else {
				$t('.quest-errmsg').text('');
			}

			let emptyTextBox = $(".options").filter(function(){
				return $.trim($(this).val()) === '';
			}).length;

			if(emptyTextBox > 2) {
				$t('.answer-errmsg').text(answerValidate);
			} else {
				$t('.answer-errmsg').text('');
			}

			if(!$('.add-answers input:checkbox').is(':checked')) {
				$t('.chkbox-errmsg').text(checkboxValidate);
			} else {
				$t('.chkbox-errmsg').text('');
			}

			if($t('.QuestionnaireSection .error-message').text() !== '') {
				$(window).scrollTop(parseInt(($('.QuestionnaireSection').offset().top)-10));
				return;
			}

			var optionA = $.trim($t('#optionA').val());
			var optionB = $.trim($t('#optionB').val());
			var optionC = $.trim($t('#optionC').val());
			var optionD = $.trim($t('#optionD').val());
			var response = $.trim($t('#response').val());

			//chkOptionA
			var chkOptionA = $t('#chkOptionA').is(':checked') ? 'A,':'';
			var chkOptionB = $t('#chkOptionB').is(':checked')? 'B,':'';
			var chkOptionC = $t('#chkOptionC').is(':checked')? 'C,':'';
			var chkOptionD = $t('#chkOptionD').is(':checked')? 'D,':'';
			var chkOptions = chkOptionA+chkOptionB+chkOptionC+chkOptionD;
			chkOptions = chkOptions.slice(0,-1);
			var userResponse = $t('#userResponse').is(':checked');

			var obj = {	
			  "ID": questionID,	
			  "Question": question,
			  "Answers": [{"Opt": "A", "Ans": optionA, "isChecked":$t('#chkOptionA').is(':checked')},
			  {"Opt": "B", "Ans": optionB, "isChecked":$t('#chkOptionB').is(':checked')},
			  {"Opt": "C", "Ans": optionC, "isChecked":$t('#chkOptionC').is(':checked')},
			  {"Opt": "D", "Ans": optionD, "isChecked":$t('#chkOptionD').is(':checked')}],
			  "Correct": chkOptions,
			  "Response": response,
			  "ResponseInPercentage": userResponse
			}

			if($t('#checkEditQuestion').val() === "-1"){
				Questionaire.push(obj);
				jsonObject ={
				  "Questionaire": Questionaire,
				};
				let index = Questionaire.length-1;
				var listHmtl = '<li data-index="'+index+'" data-qid="'+questionID+'">'+question+'<i class="fa fa-times" aria-hidden="true" ></i></li>';
				$t('ul.saved-questions-list').append(listHmtl);
				if($t('.saved-questions-list li').length > 0){
					$t('.saved-questions').addClass('is-question-exist');
				}
			}else{
				if($t('#checkEditQuestion').val() !== undefined){
					let itemIndex = parseInt(jQuery('#checkEditQuestion').val());
					jsonObject.Questionaire[itemIndex] = obj;

					$t('ul.saved-questions-list li').each(function(){
						if($t(this).attr('data-index') === jQuery('#checkEditQuestion').val()){
							//$t(this).remove();
							$t(this).attr('data-index',itemIndex);
							$t(this).html(question+'<i class="fa fa-times" aria-hidden="true" ></i>');
							/*var listHmtl = '<li data-index="'+jQuery('#checkEditQuestion').val()+'">'+question+'<i class="fa fa-times" aria-hidden="true" ></i></li>';
							$t('ul.saved-questions-list').append(listHmtl);*/
						}
					});
				}
			}
			clearAllfields();
		});

		$t(document).on('click','ul.saved-questions-list li',function(e) {
			e.preventDefault();
			let itemIndex = parseInt($t(this).attr('data-index'));
			let questID = $t(this).data('qid');
			if(jsonObject.Questionaire.length > 0){
				$t('#add-question').collapse({
				    toggle: false
				});
				$t('#add-question').collapse('show');
				$t('#question').attr('data-question-id',questID);
				if(jsonObject.Questionaire.length > 0){
					$t(jsonObject.Questionaire).each(function(index,item){
						if(itemIndex === index){
							$t("#checkEditQuestion").val(index);
							$t('#question').val(item.Question);
							$t(item.Answers).each(function(ind,itm){
								$t('#option'+itm.Opt).val(itm.Ans);
							});
							$t('#question').val(item.Question);
							if(item.ResponseInPercentage){
								$t('#userResponse').prop('checked',true);
								$t('#userResponse').attr('checked',true);
								$t('#userResponse').parent('label').addClass('active');
							}
							$t('#response').val(item.Response);
							var obj = item.Correct.split(',');
							$(".answers-input-list input[type='checkbox']").prop('checked',false);
							$(".answers-input-list input[type='checkbox']").parent('label').removeClass('active');
							$(".answers-input-list input[type='checkbox']").parent('label').removeClass('disabled');
							$t(obj).each(function(i,elm){
								switch(elm){
									case 'A':
											$t('#chkOptionA').prop('checked',true);
											$t('#chkOptionA').attr('checked',true);
											$t('#chkOptionA').parent('label').addClass('active');
											break;
									case 'B':
											$t('#chkOptionB').prop('checked',true);
											$t('#chkOptionB').attr('checked',true);
											$t('#chkOptionB').parent('label').addClass('active');
											break;
									case 'C':
											$t('#chkOptionC').prop('checked',true);
											$t('#chkOptionC').attr('checked',true);
											$t('#chkOptionC').parent('label').addClass('active');
											break;
									case 'D':
											$t('#chkOptionD').prop('checked',true);
											$t('#chkOptionD').attr('checked',true);
											$t('#chkOptionD').parent('label').addClass('active');
											break;
									default:
										break;
								}
							});
						}
					});
				}
			}
		});

		$t('#add-question').on('hidden.bs.collapse', function () {
		  	clearAllfields();
		})

		function clearAllfields(){
			$t('.QuestionnaireSection').find('input').each(function(index){
				$t(this).val('');
				if($t(this).attr('type') === "checkbox" && $t(this).is(':checked')){
					$t(this).prop('checked', false);
					$t(this).parent('label').removeClass('active');
				}
			});
			$t('#response').val('');
			$t("#checkEditQuestion").val('-1');
		}

		$('#createPostPublish, #mcreatePostPublish').click(function(e){
			e.preventDefault();
			publishForm(e, function(resp){				
				try { 
					$('#loader-overlay').show();
					localStorage.setItem('publishObject', JSON.stringify(resp));
					//document.cookie = "publishObject="+JSON.stringify(resp)+"";
					let data = new FormData();
					let files = $('#input-file-now').get(0).files;
					let publishURL = $('div.create-post').attr('data-postUrl');
					data.append("model",JSON.stringify(resp));
					data.append("articleimage",files[0]);
					data.append("userid",$("#fromreq").val());
					$.ajax({
						url:publishURL,
						type:"POST",
						cache:false,
						data:data,
						processData: false,
						contentType: false,
						success:function(result){
							$('.addmedia').attr('data-article','');
							$('#loader-overlay').hide()
							$('#publishPostModal').modal('show');
						},
						error:function(result){
							$('#loader-overlay').hide();
							//console.log("publish error")
						}

					}); 
				} catch (e) {
					//console.log('You are in Private Browsing mode');					
					$('#iphonePrivateBrowser').modal('show');
				}
			});
		});

		$t('#createPostPreview, #mcreatePostPreview').click(function(e){
			e.preventDefault();
			publishForm(e,function(resp){
				try { 
					// Put the object into storage
					localStorage.setItem('previewObject', JSON.stringify(resp));
					//console.log('JSON>>>',JSON.stringify(resp));
					let redirectURL = $('#createPostPreview').attr('data-preview-url');
					window.open(redirectURL, '_blank'); 
				} catch (e) {
					//console.log('You are in Private Browsing mode');					
					$('#iphonePrivateBrowser').modal('show');
				}
				
			});
		});
		
		$('#iphoneOk').click(function(){
			$('#iphonePrivateBrowser').modal('hide');
		});

		$t("#createPostForm, #mcreatePostForm").validate({
				// Trim the value of every element
				normalizer: function(value) {
					return $t.trim(value);
				},	
				rules : {
					crptitle : {
						required: true,
						normalizer: function(value) {
							return $.trim(value);
      					}
					},
					mcrptitle : {
						required: true
					},
					"subcat[]" : {
						required: true,
						minlength: 1
					}
				},
				errorLabelContainer: ".js-errors",
				errorElement: "li",
				messages: {
					crptitle: ($t("#crptitle").data("title"))?$t("#crptitle").data("title"):$t("#mcrptitle").data("title"),
					mcrptitle: ($t("#crptitle").data("title"))?$t("#crptitle").data("title"):$t("#mcrptitle").data("title"),
					"subcat[]": {
						required:$t("#CreatePostCatelist").data("category")
					}
				}
		});

		function publishForm(e,_callback){
			if($('.js-errors .error').length > 0 && $('.js-errors .error').text() !== '') {
				$(window).scrollTop(parseInt(($('.bg-danger').offset().top)-10));
			}
			$('.js-errors .error').remove();			
			if($("#createPostForm, #mcreatePostForm").valid()){
				Categories=[];
				$('div.post-sub-category ul').each(function(){
					$(this).find('li').each(function(index,obj){
						if($(this).find('input:checkbox').is(':checked')){
							SubCategories.push({
								"Id": $(this).attr('data-subCategoryId'),
								"Name": $(this).text(),
								"Link": null,
								"ViewAll": null
							});
						}
					});
					Categories.push({
						"Id": $(this).prev('h5').attr('data-subCategory'),
						"Name": $(this).prev('h5').text(),
						"Description": $(this).prev('h5').text(),
						"Image": null,
						"Link": null,
						"SubCategories":SubCategories
					});
					SubCategories = [];
				});
				jsonObject.ArticleMediaId = $('.addmedia').attr('data-article');
				jsonObject.IsUpdate = $('#createPostPublish').attr('data-isupdate');
				jsonObject.Categories = Categories;
				let PostDesc = (e.target.id === "createPostPublish") ? encodeURIComponent($("#createPostForm .Editor-editor").html()) : $("#createPostForm .Editor-editor").html();
				let externalLink = ($('#crpWebsiteUrl').val() !== '')?$('#crpWebsiteUrl').val():(getEditorText($("#postEditor").Editor("getText")))?getEditorText($("#postEditor").Editor("getText")):'';
				jsonObject.Article = { "Name":($('#crptitle').val())?$('#crptitle').val():$('#mcrptitle').val(), "Image":  ($('#createPostForm .dropify-render img').attr('src'))?$('#createPostForm .dropify-render img').attr('src'):'', "Body":PostDesc, "ExternalUrl":externalLink, "ButtonName":(jQuery('#crpWebsiteUrl').attr('data-btnName')===undefined)?'Access website': jQuery('#crpWebsiteUrl').attr('data-btnName')};
				//console.log('JSON-OBJECT>>>>', JSON.stringify(jsonObject));
				_callback(jsonObject);
			} else {
				$(window).scrollTop(parseInt(($('.bg-danger').offset().top)-10));
			}

		}

		function getEditorText(content) {
			let path='';
			var $el = $("<div/>").append($.parseHTML(content));
			$el.each(function() {
				if( $(this).attr('data-fileextension') !== 'mp4' && 
					$(this).attr('data-fileextension') !== 'webm' && 
					$(this).attr('data-fileextension') !== 'ogv' && 
					$(this).attr('data-fileextension') !== 'jpg' && 
					$(this).attr('data-fileextension') !== 'png') {
					path = $(this).attr('download');
					jQuery('#crpWebsiteUrl').attr('data-btnName','Download');
				}
			});
			return path;
		}

		$(document).on('click','.addmedia-delete',function(e){
			//e.preventDefault();
			$(this).parent('div.file-wrapper').remove();
			var currentImgId = $(this).parent('div.file-wrapper').attr('data-imageindex');
    		$(insImgContainer).each(function(key,obj){
    			if(obj.index === "file_"+currentImgId){
					$('#file_1').val('');
    				insImgContainer.splice(key, 1);
    			}
    			
    		});
    		if((insImgContainer && !insImgContainer.length) || $('.file-wrapper').length === 0){
    			$('#uploadFiles').hide();
    		}
		});

		//For add media upload event with terms and condition confirmation popup
		$('#uploadFiles').click(function(e){
			e.preventDefault();
			var copyRightIssue = true;
			$('#plagiarismModal').modal('show');
			if( !copyRightIssue ) {
				uploadingDocuments();
			}
		});
		$('#btn_download').click(function(e){
			e.preventDefault();
			var copyRightIssue = true;
			$('#plagiarismModal').modal('show');
		});
		
		//For add media terms and condition confirmation popup ok event
		$('#plagiarismOk').click(function(e){
			e.preventDefault();
			$('#plagiarismModal').modal('hide');
			uploadingDocuments();
		});
		
		$('#attachment_i_agree').on('change', function(e){  
			if($(this).is(':checked')) {
				$('#plagiarismModal #plagiarismOk, #plagiarismModal #articleDownloadOk').removeClass('disabled');
			} else {
				$('#plagiarismModal #plagiarismOk, #plagiarismModal #articleDownloadOk').addClass('disabled'); 
			}
		});
		
		
		//For article details download event with terms and condition confirmation popup 
		$('.Adetails-page #articleDownload').on('click', function(e){
			$('#articleDownloadModal').modal('show');
		});	
		
		//For article details download ok event
		$('#articleDownloadOk').click(function(e){
			e.preventDefault();
			$('#plagiarismModal').modal('hide');
			window.location.href = $('#btn_download').attr('data-download-url');
		});

		function uploadingDocuments() {

			$('#loader-overlay').show();
			let data = new FormData();
			let frag='';
    		let uploadURL = $('div.create-post').attr('data-mediaUrl');
    
			$(insImgContainer).each(function(key,obj){
    			data.append("mediaFiles",obj.value);
    		});
    		data.append("articleName",($('#crptitle').val())?$('#crptitle').val():$('#mcrptitle').val());
			data.append('ArticleMediaId',$('.addmedia').attr('data-article'));

    		//Focus the cursor on rich text editor if the range is not selected
    		jQuery("#postEditor").Editor("richTextEditorFocus",true);
			
	        $t.ajax({
	            url: uploadURL,
	            type: "POST",
	            data: data,
	            cache: false,
	            processData: false,
	            contentType: false,
	            success: function (result) {
					$('.addmedia').attr('data-article',result.ArticleMediaId);
					if($('#createPostForm .Editor-editor').text() === ""){
						$("#postEditor").Editor("richTextEditorFocus",true);
					}
					var mediaList = result.MediaList.reverse();
					$.each(mediaList, function(){
		                var Extension = this.Extension.split('.');
						var fileExtension = Extension[Extension.length-1];
						if(fileExtension === 'mp4' || fileExtension === 'webm' || fileExtension === 'ogv') {
			            	frag = '<video src="'+this.Path+'" controls="true" data-fileextension="'+fileExtension+'"/><br/>';
			            } else {
							 let thumbImgClass = (ext === 'pdf' || ext === 'txt' || ext === 'docx' || ext === 'ppt') ? 'file-thumbnail' : '';
							 let documentClass = (fileExtension === 'pdf' || fileExtension === 'txt' || fileExtension === 'docx' || fileExtension === 'ppt') ? 'file-upload' : '';
							 var imgUrl = (extensionImageUrl[fileExtension] !== undefined)?''+extensionImageUrl[fileExtension]+'':''+this.Path+'';
							 if( documentClass !== ''){
							 	frag = '<a download="'+this.Path+'" href="'+this.Path+'" data-fileextension="'+fileExtension+'" class="'+documentClass+'"><img src="'+imgUrl+'" class="'+thumbImgClass+'"/><span>'+this.Name+'</span></a>';	
							 } else {
							 	frag = '<a download="'+this.Path+'" href="'+this.Path+'" data-fileextension="'+fileExtension+'"><img src="'+imgUrl+'" class="'+thumbImgClass+'"/></a>';
							 }
							 
						}
			            jQuery("#postEditor").Editor("customRestoreSelection",[frag],"html");
			            jQuery('#crpWebsiteUrl').attr('data-btnName','Access website');
			            
					});
					$('#loader-overlay').hide()
					$("#file_1").val("");
					insImgContainer = [];
					$('#uploadFiles').hide();
					$('#mediaList .file-wrapper').remove();
	            },
	            error: function (result) {
	                //console.log(JSON.stringify(result));
	            }
			});
		}

		$(window).on("unload", function(e) {
			let url = window.location.href;
			url = url.substring(url.lastIndexOf('/') + 1);
			if(url === "create-post-preview"){
				localStorage.removeItem('previewObject');
				//document.cookie = "publishObject=;";
			}
		});

		$('.add-answer-input input').keyup(function() {
			var $answerBox = $(this)
							.parent()
							.next()
							.find('input[type="checkbox"]')
							.parent();
			if($(this).val() !== '') {
				$answerBox.removeClass('disabled');
			} else {
				$answerBox.children().prop('checked', false);
				$answerBox.addClass('disabled');
				$answerBox.removeClass('active');
			}

		});

		$('.add-answer-correct input[type="checkbox"]').each(function() {
			$(this).parent().addClass('disabled');
		});

		$('#publishOk').click(function(){
			//location.reload();
			location.href=location.origin+location.pathname.replace(/\/$/, "");
		});

		function httpGet(theUrl)
		{
			var url = ''+$('.create-post').attr('data-webclipurl')+'?post_url='+theUrl+'';
			//var url = '/api/sitecore/WebClipper/SiteContent?post_url='+theUrl+'';
			$('#loader-overlay').show();
		    $.ajax({
				url:url,
				type:"GET",
				success:function(result){
					var title,description,image,ogUrl;
					var $el = $("<div/>").append($.parseHTML(result));
					$('#loader-overlay').hide();
					$('#crpWebsiteUrl').val(theUrl);
					  	$el.find('meta').each(function(){
						   if($(this).attr('name') !== undefined || $(this).attr('property') !== undefined ){
						   		if($(this).attr('property') === 'og:title' || $(this).attr('name') === 'twitter:title'){
						   			title = $(this).attr('content');
						   		}
						   		if($(this).attr('property') === 'og:description' || $(this).attr('name') === 'twitter:description' || $(this).attr('name') === 'description'){
						   			description = $(this).attr('content');
						   		}
						   		if($(this).attr('name') === 'Image' || $(this).attr('property') === 'og:image' || $(this).attr('name') === 'twitter:image'){
						   			image = $(this).attr('content');
						   		}
						   		if($(this).attr('property') === 'og:url') {
						   			ogUrl = $(this).attr('content');
						   		}
						   }
						});
						if(title === undefined) {
							$el.find('title').each(function() {
							  title = $(this).text();
							});
						}
						if(image === undefined) {
							var element = document.createElement('div'); 
							element.innerHTML = result; 
							var imgSrcUrls = element.getElementsByTagName("img");
							for (var i = 0; i < imgSrcUrls.length; i++) {
							    image = imgSrcUrls[i].getAttribute("src"); 
							    if(image !== undefined) {
					              break;
					            }
							}
							/*if($el.find('img').length){
								$el.find('img').each(function() {
									image = $(this).attr('src');
									if(image !== undefined) {
										//console.log('image undefined');
										return;	
									}
								});
							}*/
						}
						if(title !== undefined) {
							$('#crptitle').val(title);
						}
						if(description !== undefined) {
							// let rangeExist = window.getSelection();
							// if(rangeExist.rangeCount === 0){
							// 	jQuery("#postEditor").Editor("richTextEditorFocus",true);
							// } else {
							// 	jQuery("#postEditor").Editor("richTextEditorFocus",false);
							// }
							jQuery("#postEditor").Editor("richTextEditorFocus",true);
							jQuery("#postEditor").Editor("customRestoreSelection",[''+description+''],"");
						}
						if(image !== undefined) {
							$('<img/>',{
								src:image,
							}).on('error',function(){
								$(this).attr('src',''+ogUrl+image+'');
							}).on('load', function() { 
								$('.dropify-wrapper #input-file-now').hide();
								$('.dropify-wrapper .dropify-infos-inner').hide();
							}).appendTo($('.dropify-preview .dropify-render'));
							$('.dropify-preview').show();
						}
						
				},
				error:function(result){
					$('#loader-overlay').hide();
					//console.log("Error="+JSON.stringify(result));
				}
			});
		}

		function getUrlParameters(parameter, staticURL, decode) {
	         const currLocation = window.location.search;
	         var parArr = currLocation.split("?")[1].split("&");
	         var returnValue = undefined;
	         var parr;

	         for(var i = 0; i < parArr.length; i++){
	            parr = parArr[i].split("=");
	            if(parr[0] === parameter){
	                returnValue = (decode) ? decodeURIComponent(parr[1]) : parr[1];
	            }
	        }

	        return returnValue;
	    }

		if(window.location.search.indexOf('?') > -1) {
		    var postUrl = getUrlParameters("post_url");
		    var editPostId = getUrlParameters("id");

			//For web-clipping functionality
		    if (postUrl !== undefined) {
		        httpGet(postUrl);
		    }
			
			//For edit create post
			if (editPostId !== undefined) {
		        editQuestionaire(editPostId);
		    }
		}
		
		
		function editQuestionaire(postId){
			
			let Questionaire = JSON.parse($t('#QuestionJsonString').val());
			let postSummaryIsExit =  $t("#postEditor").text();
			if (postSummaryIsExit != null) {
				$t('.Editor-editor').append(postSummaryIsExit);
			}
			//To assign the image preview for edit profile dropify
			$t('#createPostForm .dropify-render').append('<img src='+ $('#input-file-now').data('default-file') +'></img>');
			$t('#createPostForm .dropify-preview').css('display','block'); 
			let index, listHmtl;
			$t(Questionaire).each(function(index,item){
				listHmtl = '<li data-index="'+index+'" data-qid="'+item.ID+'">'+item.Question+'<i class="fa fa-times" aria-hidden="true" ></i></li>';
				$t('ul.saved-questions-list').append(listHmtl); 
			});
			
			if(Questionaire.length > 0){
				$t('div.saved-questions').show();
				if($t('.saved-questions-list li').length > 0){
					$t('.saved-questions').addClass('is-question-exist');
				}	
			}
						
			jsonObject.Questionaire = Questionaire;
		}
		
		
		 $(".question-footer input:file").change(function (){
			var file = this.files[0];
			var reader = new FileReader();
			// Set preview image into the popover data-content
			reader.onload = function (e) {
				var ext = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();
				if(ext === 'png' || ext === 'jpg' || ext === 'jpeg'){
					$(".image-preview-filename").val(file.name);
					img.attr('src', e.target.result);
					$(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
				} 
			}
			reader.readAsDataURL(file);
		});
		
		
		 //For Questionaire Image upload
		$(".question-footer input:file").change(function (){
			let file = this.files[0];
			let fileReader = new FileReader();
			// Set preview image into the popover data-content
			let ext = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();
			if(ext !== 'png' && ext !== 'jpg' && ext !== 'jpeg'){
				$('#file_2').val('');
				return;
			}
			fileReader.onload = function (e) {
				$(".form-hint").text(file.name);
			}
			fileReader.readAsDataURL(file);
		});
		
		
		
		
		
	});
})($t);
