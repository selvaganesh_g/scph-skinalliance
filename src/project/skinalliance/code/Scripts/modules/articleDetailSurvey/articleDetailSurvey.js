$(document).ready(function(){
	$('.survey-detail .chkbox-errmsg').hide();
	$('.questionnaire-btn').click(function(e){		
		$('#loader-overlay').show();
		$('.chkbox-errmsg').text('');
		let surveyApiUrl = ($('#accordion').attr('data-postquizurl'))?$('#accordion').attr('data-postquizurl'):'http://www.mocky.io/v2/59788c971300009105c0ff99';
		var obj = $(this).attr('data-subPanelId');
		//Validate the survey answers
		let ansValidErrMsg =  $('.survey-detail').attr('data-validate-answer');
		if($('#'+obj+' input[type=checkbox]:checked').length === 0) {
			$('#'+obj).find('.chkbox-errmsg').text(ansValidErrMsg).css('display','block');
			return;
		}
		window.QuestionId = $(this).attr('data-subPanelId');
		//let index = obj.split('-')[1];
		let ArticleId = $('#'+obj).attr('data-articleid');
		let QuizId = $('#'+obj).attr('data-quizid');
		let correctArr = $('#'+obj).attr('data-correct').split(",");
		const options = ['A','B','C','D']; 
		let ChkOption = [];
		$(options).each(function(index,value){
			ChkOption[index] = $($('#'+obj).find('input[type="checkbox"]')[index]).attr('id');
			if($('#'+obj).find("#"+ChkOption[index]).is(":checked")){ 
				ChkOption[index] = $.inArray(value,correctArr) !== -1 ? 'on' : 'off';
			}else{
				ChkOption[index]=null;
			}
		});
		
		let objArr = {
			"QuestionId":obj,
			"ArticleId":ArticleId,
			"QuizId": QuizId,
			"ChkOptionA": ChkOption[0],
			"ChkOptionB": ChkOption[1],
			"ChkOptionC": ChkOption[2],	
			"ChkOptionD": ChkOption[3]
		}
		//console.log(options," >>>> ",JSON.stringify(ChkOption)," >>>> ",correctArr," >>>> ",JSON.stringify(objArr));

		let jsonData = JSON.stringify(objArr);
		
		$.ajax({
			url: surveyApiUrl,
			type: "POST",
			data: jsonData,
			contentType: 'application/json; charset=utf-8',
			cache:false,
			processData:false,
			success:function(result){
				$('#'+obj+' .questionnaire-btn').attr('disabled',true);
				let correctAns = result.Correct.toLowerCase();
				correctAns = 'option-'+correctAns;
				$('#'+QuestionId).find('.results-chart .bar').css('backgroundColor','#EC8180');
				$('#'+QuestionId).find('.results-chart .option-a .result-option').text('A').css('text-align','center');
				$('#'+QuestionId).find('.results-chart .option-b .result-option').text('B').css('text-align','center');
				$('#'+QuestionId).find('.results-chart .option-c .result-option').text('C').css('text-align','center');
				$('#'+QuestionId).find('.results-chart .option-d .result-option').text('D').css('text-align','center');
				$('#'+QuestionId).find('.results-chart .option-a .bar').css('height',+result.A+'px');
				$('#'+QuestionId).find('.results-chart .option-b .bar').css('height',+result.B+'px');
				$('#'+QuestionId).find('.results-chart .option-c .bar').css('height',+result.C+'px');
				$('#'+QuestionId).find('.results-chart .option-d .bar').css('height',+result.D+'px');
				$('#'+QuestionId).find('.results-chart .option-a .result-percent').text(result.A+'%');
				$('#'+QuestionId).find('.results-chart .option-b .result-percent').text(result.B+'%');
				$('#'+QuestionId).find('.results-chart .option-c .result-percent').text(result.C+'%');
				$('#'+QuestionId).find('.results-chart .option-d .result-percent').text(result.D+'%');
				$('#'+QuestionId).find('.results-chart .bar').css('backgroundColor','#EC8180');
				$('#'+QuestionId).find('.results-chart .'+correctAns+' .bar').css('backgroundColor','#75CD82');
				$('#loader-overlay').hide();
				$(".survey-question .feedback").show();
			},
			error:function(result){
				$('#loader-overlay').hide();
				$('.questionnaire-btn').attr('disabled',false);
				//console.log("Error : ",result);
			}
		});
	});
});