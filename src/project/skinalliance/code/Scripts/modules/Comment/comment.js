/**
 * @module Modules/Comment
 * @desc Comment page validation
 */

export default class Comment {
    /**
     * Comment
     * @constructor
     */
    constructor() {
        this.init();
    }

    /**
     * Init the validation methods and bind the events
     * @return {object} The validation class and methods
     */
    init() {this.postRender();}

	postRender() {
        /**
         * @function document ready
         * @desc Once document loaded then this function would be triggred
         */
	var jqry = jQuery.noConflict();
	(function(jqry){
		jqry(document).ready(function(){		
			jqry('#commentformbutton').on('click', () => {					
				jqry("#commentform").validate({
					// Trim the value of every element
					normalizer: function(value) {
						return $.trim(value);
					},
					rules : {						
						Message: {
							required: true							
						}					
					},
					messages: {
						Message: jqry("#Message").data("requiredmessage")					
					}
				});
			});
		});
	})(jqry);}
}