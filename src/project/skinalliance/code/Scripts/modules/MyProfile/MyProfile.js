/**
 * @module Modules/MyProfile
 * @desc MyProfile page validation
 */ 
var $j = jQuery.noConflict();
(function($j){
	$j(document).ready(function(){	
	
		//For header form search
		$j('.search-form .fa-search').click(function() {
			if($j('#q').val() !== ""){
				$j('form.search-form').submit();
			} else {
				//console.log('Else Part');
				return;
			}
		});

		$j('#q').keypress(function(e) {
			 var key = e.which;
			 // the enter key code
			 if(key === 13 && $j('#q').val() === "") {
				//console.log('Else'); 
				e.preventDefault();
			 } 
		});
		
		//To remove the filename while mouse over on dropify
		$j('#editProfileForm .dropify').dropify({
		    tpl: {
		        'message':  '<div class="dropify-message"><span class="fa fa-plus fa-2x" /> <p>{{ default }}</p></div>',
				'filename': ''
		    }
		});

		//Implement rich text editor for edit profile page
		$j("#txtEditor").Editor({
			'fonts':false,
			'styles':false,
			'font_size':false,
			'block_quote':false,
			'source':false
		});

		//For password validation message getting from data attribute
		let profilePwdStrMsg = $j("#new_password").attr("data_pwdstr");


		//For Menu Selection Highlight
		$('#mainMenu li').click(function() {
			var id = $(this).attr("id");
			$('#mainMenu li a').removeClass("active");
			$('#' + id).children().addClass("active");
			sessionStorage.setItem("selectedMenu", id);		
		});
		
		//Hide flag for upcomming congress 
		$('.upcoming-congress a').click(function() {
			sessionStorage.setItem("selectedMenu", 'congresses');		
		});
		
		//Remove menu highlight for static footer pages and myprofile pages
		$('.UsrPicDropdown .dropdown-cart li a, .footer .fmenu ul li a').click(function() {
			sessionStorage.setItem("selectedMenu", 'none');		
		});
		
		$('.AllianceMember .board-member-profile-pic, .AllianceMember .readmore').click(function() {
			var selectedId = $(this).attr("data-navigation");
			//console.log('Alliance Member',selectedId);
			$('#mainMenu li a').removeClass("active");
			$('#' + selectedId).children().addClass("active");
			sessionStorage.setItem("selectedMenu", selectedId);		
		});
		
		var selectedMenuItem = sessionStorage.getItem('selectedMenu');

		//For Home menu highlight when navigate to home page by clicking logo 
		$('header .Logo a').click(function() {
			sessionStorage.setItem("selectedMenu", "Home");
        	selectedMenuItem = sessionStorage.getItem('selectedMenu');
		});
		
        if (selectedMenuItem === null) {
        	sessionStorage.setItem("selectedMenu", "Home");
        	selectedMenuItem = sessionStorage.getItem('selectedMenu');
        } else { 
			//Disable flag for congresses page
			if(selectedMenuItem === 'congresses'){
				//$('.toprtmenu .Language button').css('display', 'none');
			} else {
				//$('.toprtmenu .Language button').css('display', 'block');
			}
		}
        //$('.toprtmenu .Language button').css('display', 'none');
        $('#mainMenu li a').removeClass("active");
        $('#'+selectedMenuItem).children().addClass("active");
		
		//For password special character check
		$j.validator.addMethod("validPassword",function(value, element) {			
			if (/^(?=.*[a-z])(?=.*[A-Z])((?=.*[a-z])(?=.*[0-9]))(?=.*\d)[A-Za-z\d#$@!%&*?]{8,16}$/.test(value)) {
                return true;
            } else {
                return false;
            }
	   },profilePwdStrMsg);

		//Change Password form validation
		$j('#changePassword').on('click', () => {
			$j("#changePasswordForm").validate({
				// Trim the value of every element
				normalizer: function(value) {
					return $.trim(value);
				},
				rules : {
					newpassword : {
						required: true,
						notEqualTo: "#old_password",
						validPassword: true
					},

					confirmPassword : {
						equalTo : "#new_password",
						required: true,
						notEqualTo: "#old_password",
						validPassword: true
					}
				},
				messages: {
					password: $j("#old_password").attr("data_oldpwd"),
					newpassword: {
						required: $j("#new_password").attr("data_newpwd"), 
						notEqualTo: $j("#new_password").attr("data_oldasnew"),
						remote: true
					},
					confirmPassword: {
						required: $j("#confirm_password").attr("data_confirmpwd"),
						equalTo: $j("#confirm_password").attr("data_equalTo"),
						notEqualTo: $j("#new_password").attr("data_oldasnew")
					}
				},
				submitHandler: function(){
					 $.ajax({
						url: $(".change-password-modal").attr("data-api-url"),
						type: "POST",
						data: ({ "password": $j("#old_password").val(), "newPassword": $j("#new_password").val()}),
						success: function (resp) {
							if(resp.Success === true){
								$j('#change-pwd .close').trigger('click');
							}else {
								$j("#invalidOldPwd").text($j("#old_password").attr("data_invpwd"));
							}
						},
						error: function (error) {
							$j("#invalidOldPwd").text(error);
						}
					});
				}
			});
		});

		//Edit Profile form validation
		$j('#editProfileSubmit').on('click', () => {
			
			let invalidPhoneErrMsg = $j("#profile_phonenumber").attr("data-invalidphonenumber");
			//For complete email validation
			$j.validator.addMethod("validateEmail",function(value, element) {
				if(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test( value )){
				   return true;
				} else {
				  return false;
				}
		   });
		   
		   
		    $j.validator.addMethod("validPhone", function(phoneNumber, element) {
					phoneNumber = phoneNumber.replace( /\s+/g, "" );
				return this.optional( element ) || phoneNumber.length > 9 &&
					phoneNumber.match(/^[0-9-+()]{7,18}$/);
			},invalidPhoneErrMsg);

		   $j("#editProfileForm").validate({
			   // Trim the value of every element
			    rules : {
					FirstName: {
						required: true,
				    	normalizer: function(value) {
							return $.trim(value);
						}
					},
					LastName: {
						required: true,
				    	normalizer: function(value) {
							return $.trim(value);
						}
					},
					PhoneNumber: {
						validPhone: true,
				    	normalizer: function(value) {
							return $.trim(value);
						}
					},
					pemail: {
						required: true,
						validateEmail: true,
				    	normalizer: function(value) {
							return $.trim(value);
						}
					},
					PhysicianLicenseNumber: {
						required: true,
				    	normalizer: function(value) {
							return $.trim(value);
						}
					}
				},
				messages: {
				    FirstName: $j("#profile_firstname").data("pfname"),
					LastName: $j("#profile_lastname").data("plname"),
					Country: $j("#profile_country").data("country"),
					PhysicianLicenseNumber: $j("#profile_physiciallicensenumber").data("plicence"),
					MedicalSpecialty: $j("#profile_medicalspecialty").data("pmedical"),
					PhoneNumber:{
						remote: true
					}
				}
			});

			//Convert summary text into encoded text
			if($j(".Editor-editor").html() !== ''){
				let encodedValue = encodeURIComponent($j(".Editor-editor").html());
				$j("#txtEditor").text(encodedValue);
			}

			$j(".popover").css("display","none");

		});

		//Change password close event
		$j('#change-pwd .close').click(function() {
			 $j('#changePasswordForm').find("input").val('');
			 $j('#changePasswordForm').find('label.error').hide();
			 $j('.image-preview-clear').trigger('click');
		});

		//Edit Profile close event
		$j('#edit-profile .close').click(function() {
			$j(".Editor-editor").html('');
			$j("#editProfileForm")
			.find("input,textarea,select").val('')
			.end()
			.find("input[type=checkbox], input[type=radio]")
			.prop("checked", false)
			.parent('label')
			.removeClass('active')
			.attr("checked", false)
			.parent('label')
			.removeClass('active')
			.end()
			.find('label.error')
			.hide();
			$j('div.Editor-container #statusbar').html('<div class="label">Words : 0</div><div class="label">Characters : 0</div>');
			$j("#upload-resume").val('');
			$j(".upload-resume").find('.form-hint').text('(Upload an external Document or Presentation)');
			window.location = window.location.pathname;
		});


		$j("#editProfileForm :input").change(function() {
			$(this).data("changed",true);
		});

		//For summary rich text editor event
		$j(".Editor-container").on("focusout",function(){
			$j("#txtEditor").text('');
			let editorValue = $j(".Editor-editor").html();
			$j("#txtEditor").text(editorValue);
		});

		//To show the textarea value for edit profile
		$j('#edit-profile').on('shown.bs.modal', function() {
			let summaryIsExit =  $("#txtEditor").attr('value');
			if (summaryIsExit != null) {
				$j('.Editor-editor').append(summaryIsExit);
			}
			//To assign the image preview for edit profile dropify
			$j('#edit-profile .dropify-render').append('<img src='+ $('#edit-profile .dropify-wrapper').find("input[name='preview']").attr('data-default-file') +'></img>');
		});

 	   //For Upload Resume in my profile page
		$(".upload-resume input:file").change(function (){
			let file = this.files[0];
			let fileReader = new FileReader();
			// Set preview image into the popover data-content
			let ext = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();
			if(ext !== 'pdf' && ext !== 'doc' && ext !== 'ppt' && ext !== 'pptx'){
				$('#resume').val('');
				return;
			}
			fileReader.onload = function (e) {
				$(".form-hint").text(file.name);
			}
			fileReader.readAsDataURL(file);
		});

		$j('.logout-link').click(function(e){
			e.preventDefault();
			sessionStorage.removeItem("selectedMenu");
			window.location.href = $j('.logout-link').data('logout-url');
		});

		$j('select').change(function (e) {
			if(e.currentTarget.value !== ''){
				$j('#'+e.currentTarget.id).css('color','#555');
			} else {
				$j('#'+e.currentTarget.id).css('color','#999');
			}
		});
		
		//Welcome pop-up checkbox check issue fix
		$j('#termsandconditionform input[type="checkbox"]').change(function() {
			 let timer = setInterval(function(){ 
				if($j('#termsAccept').hasClass('active')){
					setTimeout(function(){
						$j('#termsandconditionform').submit();
						clearInterval(timer);
					},1000);
				}
			 }, 200);			
		});
		
		$('#registerForm select, #editProfileForm select').each(function(index,item){
			if($('#'+item.id) !== ''){
				if($('#'+item.id).val() !== ''){
					$('#'+item.id).css('color','#555555');
				//console.log('item-id',item.id);
				} else {
					$('#'+item.id).css('color','#999');
				}	
			}
		});
		
		/* if(/Android|webOS|iPhone/i.test(navigator.userAgent)){
			$j('.DermoCosmetic .subcategory').each(function(index,item){
				var contentHeight = $j(this).find('ul').height();
				if(contentHeight < 100) {
					$(this).height(contentHeight);	
				}
			});
		} */
				
	});
})($j);
