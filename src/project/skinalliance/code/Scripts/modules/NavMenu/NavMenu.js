(function($){
	$(document).ready(function(){
		$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
			event.preventDefault();
			event.stopPropagation();
			$(this).parent().siblings().removeClass('open');
			$(this).parent().toggleClass('open');
		});
	});

	//To show user terms and condition popup
	$(document).ready(function(){

		var compro = GetQueryStringParams("pcom");
		if (compro !== undefined && compro === 'true' && jQuery('#termsandcondition').val() === undefined)
		{
		  jQuery('#edit-profile').modal({backdrop: 'static', keyboard: false});
		  setTimeout(function(){
			$('#edit-profile .dropify-render').append('<img src='+ $('#edit-profile .dropify-wrapper').find("input[name='preview']").attr('data-default-file') +'></img>');
	      }, 500);
		}
		jQuery('#termsandcondition').modal({backdrop: 'static', keyboard: false});
	});

	 $(document).ready(function() {
		let isIphone = (navigator.userAgent.match(/iPhone/i) != null) || (navigator.userAgent.match(/iPod/i) != null);
		if (isIphone) {
			// $('.navigation .navbar').on("click touchstart", function(){
			// 	$('#SA-navbar-collapse').collapse('show');
			// });
		}

		$.each($('.navbar').find('li'), function() {
			 $(this).toggleClass('active',
				'/' + $(this).find('a').attr('href') === window.location.pathname);
			 });
		});


	  $(document).ready(function() {
			


			let windowWidth = $(window).width();
			if(windowWidth <= 1024 ){
				jQuery('.navbar-nav > li.dropdown .fa').click(function(){
					jQuery(this).toggleClass('icon-rotate');
					jQuery(this).next('ul.dropdown-menu').toggleClass('open');
				});


				jQuery('ul.dropdown-menu.open > li.dropdown-submenu .fa').click(function(){
					jQuery(this).toggleClass('icon-rotate');
					jQuery(this).next('ul.dropdown-menu').toggleClass('open');
				});}

			$(".moderate-article").on("click",function(){
				var apiUrl = $(this).attr("data-apiUrl")+'?delete='+ $(this).attr("data-type");
				$.ajax({
						url: apiUrl,
						dataType: "json",
						type:"POST",
						success: function(articleslist){
							$('#ModerteModal').modal({backdrop: 'static', keyboard: false});
						}
					});

			});

			// slide from left menu 
			
			jQuery('[data-toggle="slide-collapse"]').on('click', function() {
			 var navMenuCont = jQuery(this).data('target');
			jQuery(navMenuCont).addClass('slide-open');
	 });
	 
	 // close mobile nav
			jQuery('.close-mobile-nav').click(function(){
				jQuery('#SA-navbar-collapse').removeClass('slide-open');
			});
			
			
			
			// hide logo when hover search box
			
			jQuery('.searchBox').mouseenter(function() {
				jQuery('.Logo a img').addClass('logo-hide');
			});
			jQuery('.searchBox').mouseleave(function() {
				jQuery('.Logo a img').removeClass('logo-hide');
			})

	});



function GetQueryStringParams(sParam)
{
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++)
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] === sParam)
			{
				return sParameterName[1];
			}
		}
}
})(jQuery);
