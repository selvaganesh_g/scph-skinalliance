/**
 * @module Modules/SignUp
 * @desc SignUp page validation
 */

export default class SignUp {
    /**
     * Navigation Menu
     * @constructor
     */
    constructor() {
        this.init();
    }

    /**
     * Init the validation methods and bind the events
     * @return {object} The validation class and methods
     */
    init() {this.postRender();}

	postRender() {
        /**
         * @function document ready
         * @desc Once document loaded then this function would be triggred
         */
	var jqry = jQuery.noConflict();
	(function(jqry){
		jqry(document).ready(function(){		
			jqry('.reg-form-submit').on('click', () => {
				
				let signUpPwdStrMsg = jqry("#signup_password").attr("data-pwdStr");
				let invalidEmailMsg = jqry("#signup_email").attr("data-invalidemail");
				let invalidPhoneMsg = jqry("#signup_phone").attr("data-invalidphone");
	            let requiredphysicianlicensenumber =false;
				if (jqry("#signup_physiciallicensenumber").attr('required') === 'required'){
						requiredphysicianlicensenumber =true;
					}
				//For complete email validation
				jqry.validator.addMethod("validateEmail",function(value, element) {
				    if(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test( value )){
					   return true;
					} else {
					  return false;
					}    
			   },invalidEmailMsg);
				
				//For password special character check
				jqry.validator.addMethod("validPassword",function(value, element) {
					if (/^(?=.*[a-z])(?=.*[A-Z])((?=.*[a-z])(?=.*[0-9]))(?=.*\d)[A-Za-z\d#$@!%&*?]{8,16}$/.test(value)) {
						return true;
					} else {
						return false;
					} 
			   },signUpPwdStrMsg);
			   		   
			   jqry.validator.addMethod("validPhone", function(phoneNumber, element) {
					phoneNumber = phoneNumber.replace( /\s+/g, "" );
				return this.optional( element ) || phoneNumber.length > 9 &&
					phoneNumber.match(/^[0-9-+()]{7,18}$/);
				},invalidPhoneMsg);

				
				
				jqry("#registerForm").validate({
					// Trim the value of every element
					
					
					normalizer: function(value) {
						return $.trim(value);
					},
					rules : {
						Email: {
							required: true,
							validateEmail: true,
							normalizer: function(value) {
								return $.trim(value);
							}
						}, 
						Phone: {
							validPhone: true,
							normalizer: function(value) {
								return $.trim(value);
							}
						},
						Password: {
							required: true,
							validPassword: true
						},
						ConfirmPassword : {
							equalTo : "#signup_password",
							required: true,
							validPassword: true
						},
						PhysicianLicenseNumber: {
							required: requiredphysicianlicensenumber,
							normalizer: function(value) {
								return $.trim(value);
							}
						}
					},
					messages: {
						FirstName: jqry("#signup_firstname").data("fname"),
						LastName: jqry("#signup_lastname").data("lname"),
						Email: {
							required: jqry("#signup_email").data("email"),
							remote: true	
						},
						Phone:{
							remote: true
						},
						Country: jqry("#signup_country").data("country"),
						PhysicianLicenseNumber: jqry("#signup_physiciallicensenumber").data("lnumber"),
						MedicalSpecialty: jqry("#signup_medicalspecialty").data("medical"),						
						Password: {
							required: jqry("#signup_password").data("password"),
							remote: true
						},
						ConfirmPassword: {
							required: jqry("#signup_confirmpassword").data("cpassword"),
							equalTo: jqry("#signup_confirmpassword").data("equalto")
						}
					}
				});
			});
		});
	})(jqry);}
}