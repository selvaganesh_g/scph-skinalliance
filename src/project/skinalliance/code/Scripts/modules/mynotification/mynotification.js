/**
 * @module Modules/mynotification
 * @desc mynotification page validation
 */

export default class MyNotification {
    /**
     * mynotification
     * @constructor
     */
    constructor() {
        this.init();
    }

    /**
     * Init the validation methods and bind the events
     * @return {object} The validation class and methods
     */
    init() {this.postRender();}

	postRender() {
        /**
         * @function document ready
         * @desc Once document loaded then this function would be triggred
         */
	var jqry = jQuery.noConflict();
	(function(jqry){

		function loadTimeline(apiUrl,sort){

				jqry.ajax({
						url: apiUrl,
						dataType: "json",
						type:"GET",
						beforeSend : function(){
						  $("#notificationloadmore").attr("class","load-more-txt");
						},
						success: function(timelinelist){
							var timeline = timelinelist.List;
							var html='';
							var i=0;
							while(i < timeline.length){
							var icon ='';
                            switch (timeline[i].Type)
                            {
                                case 5:
                                   icon='<div class="title-image"><i class="fa fa-user user" aria-hidden="true"></i></div>';
                                    break;
                                case 6:
                                     icon='<div class="title-image"><i class="fa fa-plus-circle user" aria-hidden="true"></i></div>';
                                    break;
                                case 0:
                                case 1:
                                     icon='<div class="title-image"><i class="fa fa-star user" aria-hidden="true"></i></div>';
                                    break;
                                case 2:
                                case 3:
                                     icon='<div class="title-image"><i class="fa fa-comment user" aria-hidden="true"></i></div>';
                                    break;
                                case 4:
                                     icon='<div class="title-image"><i class="fa fa-plus-circle user" aria-hidden="true"></i></div>';
                                    break;
                                default:
        							icon='<div class="title-image"><i class="fa fa-user user" aria-hidden="true"></i></div>';
                                    break;
                            }

							html += '<ul class="notification-container"><li> '+ icon +'</li>'+
											'<li><img src="'+timeline[i].ImageUrl+'"></li>'+
											'<li>'+
											'<p><strong>'+ timeline[i].Date + '</strong></p>'+
											'<p>'+ timeline[i].Message + '</p></li></ul>';
							   i++;
							}
							if (sort)
							{
								$("#notificationcontainer").html('');
								$("#notificationcontainer").append(html);
							}
							else
							{
								$("#notificationcontainer").append(html);
							}
						   $("#notificationloadmore").attr("class","load-more-txt hidden");
						}
					});
		}
	    jqry(document).ready(function(){

	           /* To calculate how many times infinite scroll executed */
	            let infiniteCount = 1;
	            let allowScroll = true;
	            let currentPage = 1;
	            const manageNotificValue = [];

	            /* Settings from DOM */
	            const countPerPage = 6;
	            const pageNumber = 0;

	            let filterids = "";
	            const apiUrl = "saapi/notification/list";
	            const totalRecords = parseInt(jqry("#notificationcount").val());
	            const noOfPages = Math.floor((totalRecords + countPerPage - 1) / countPerPage);

				jqry('.notification-filter input[type="checkbox"]').change(function() {
					filterids = '';
					jqry('.notification-filter input:checkbox:checked').each(function() {
						if( filterids !== '') { filterids += '|'; }
						filterids += jqry(this).val();
					});
					infiniteCount = 1;
					allowScroll = true;
					currentPage = 1;
					var API_URL = apiUrl + '?offset=' + pageNumber + '&filter=' +filterids;
					loadTimeline(API_URL,true);
					//Footer issue in notification
				    if($('.footer').length > 0){
							$('.footer').css('margin-top','0');
						}

				});

				$(window).scroll(function() {
				   if (currentPage === noOfPages || totalRecords === 0) {
				   		allowScroll = false;
				   }
				   if(allowScroll && $(window).scrollTop() + $(window).height()+10 >= $(document).height() && jqry("#notificationcount").val() !== undefined) {
				    const pageNumber = infiniteCount;
					const API_URL = apiUrl + '?offset=' + pageNumber + '&filter=' +filterids;
				       loadTimeline(API_URL,false);
				       infiniteCount++;
				       currentPage++;
				   }
				});

				jqry('#notificationform').find('select').each(function() {
					manageNotificValue.push({
						'id': jqry(this).attr('id'),
						'value': jqry(this).val()
					})
				});
				jqry('#manage-notification').on('hidden.bs.modal', function (e) {
				 	manageNotificValue.forEach(function(item) {
				 		jqry('#'+item.id+'').val(item.value);
				 	});
				});

			});
	})(jqry);}
}
