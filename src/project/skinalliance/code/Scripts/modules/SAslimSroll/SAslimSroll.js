(function($){
$(document).ready(function(){
		$('#FollowBy').slimScroll({
		    height: '250px',
		    color: '#5AB4D4',
		    opacity: '1'
		});
		$('#Following').slimScroll({
		    height: '250px',
		    color: '#5AB4D4',
		    opacity: '1'
		});

		$('#CategoryFollowed').slimScroll({
		    height: '300px',
		    color: '#5AB4D4',
		    opacity: '1'
		});
		$('#SubcateFollowed').slimScroll({
		    height: '300px',
		    color: '#5AB4D4',
		    opacity: '1'
		});

		$('#DirFollowBy').slimScroll({
		    height: '250px',
		    color: '#5AB4D4',
		    opacity: '1'
		});
		$('#DirFollowing').slimScroll({
		    height: '250px',
		    color: '#5AB4D4',
		    opacity: '1'
		});

		$('#DirCategoryFollowed').slimScroll({
		    height: '300px',
		    color: '#5AB4D4',
		    opacity: '1'
		});
		$('#DirSubcateFollowed').slimScroll({
		    height: '300px',
		    color: '#5AB4D4',
		    opacity: '1'
		});

		$('#FilterbyCountry').slimScroll({
		    height: '300px',
		    color: '#5AB4D4',
		    opacity: '1'
		});
		$('#FilterbySpeciality').slimScroll({
		    height: '300px',
		    color: '#5AB4D4',
		    opacity: '1'
		});
		$('.subcategory > .list-inline').slimScroll({
			 height:'200px',
		    color: '#5AB4D4',
		    opacity: '1'
		});
		$('#CreatePostCatelist').slimScroll({
			 height:'300px',
		    color: '#5AB4D4',
		    opacity: '1'
		});

		

	});


})(jQuery);
