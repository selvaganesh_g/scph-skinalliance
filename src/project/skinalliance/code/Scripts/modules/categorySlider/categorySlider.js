var $k = jQuery.noConflict();
(function($){
	$k(document).ready(function(){
		$k('#basicSlider').multislider({
			continuous: true,
			duration: 2000
		});
		$k('#mixedSlider').multislider({
			duration: 750,
			interval: 3000
		});
	});
})($k);