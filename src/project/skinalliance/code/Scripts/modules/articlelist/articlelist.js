/**
 * @module Modules/ArticleList
 * @desc ArticleList page validation
 */

export default class ArticleList {
    /**
     * ArticleList
     * @constructor
     */
    constructor() {
        this.init();
    }

    /**
     * Init the validation methods and bind the events
     * @return {object} The validation class and methods
     */
    init() {this.postRender();}

	postRender() {
        /**
         * @function document ready
         * @desc Once document loaded then this function would be triggred
         */
	var jqry = jQuery.noConflict();
	(function(jqry){

		function loadArticles(apiUrl,sort){

				jqry.ajax({
						url: apiUrl,
						dataType: "json",
						type:"GET",
						success: function(articleslist){
							var articles = articleslist.Articles;
							var html='';
							var i=0;
							while(i < articles.length){

							  html += '<div class="col-lg-3 col-md-4 col-sm-4 artboxbt">'+
									  '<a href='+articles[i].Link+'><img class="img-responsive" src='+articles[i].Image+' alt='+articles[i].Name+'/></a>'+
                                      '<div class="article-content">'+
                                      '<ul class="list-inline date-author hidden-xs">'+
                                      '<li>'+articles[i].CreatedDate+'</li>'+
                                      '<li>'+articles[i].CreatedBy+'</li>'+
									                     '</ul>'+
                                      '<a href='+articles[i].Link+'><p class="txtdot">'+articles[i].Name+'</p></a>'+
                                      '<a href='+articles[i].Link+'>'+
                                      '<ul class="list-inline date-author visible-xs">'+
                                      '<li>'+articles[i].CreatedDate+'</li>'+
                                      '<li>'+articles[i].CreatedBy+'</li>'+
									                    '</ul>'+
                                      '<ul class="list-inline btn-social">'+
                                      '<li><i class="fa fa-star"> </i>'+articles[i].Like+'</li>'+
                                      '<li><i class="fa fa-comment"> </i>'+articles[i].Comments+'</li>'+
                                    '</ul>'+
                                    '</div>'+
                                    '</a></div>';

							   i++;
							}
							if (sort)
							{
								$("#articlelistcontainer").html('');
								$(".article-listing .artlistwid").append(html);
							}
							else
							{
								$(".article-listing .artlistwid").append(html);
							}

						}
					});
		}
	    jqry(document).ready(function(){

	           /* To calculate how many times infinite scroll executed */
	            let infiniteCount = 1;
	            let allowScroll = true;
	            let currentPage = 0;

	            /* Settings from DOM */
	            const countPerPage = 12;
	            const categoryId = jqry("#listpageid").val();
	            const apiUrl = jqry("#listurl").val();
	            const totalRecords = jqry("#listcount").val();
	            const noOfPages = Math.round(totalRecords / countPerPage);

				jqry("#articlelistsortby").change(function(){
				    infiniteCount = 1;
					allowScroll = true;
					currentPage = 0;
					var API_URL = jqry("#listurl").val()+ '?id='+$("#listpageid").val() +'&sortby='+$(this).val()+'&offset=0';
					loadArticles(API_URL,true);
				});

				$(window).scroll(function() {
				   if (currentPage === noOfPages) {
				   		allowScroll = false;
				   }
				   if(allowScroll && $(window).scrollTop() + $(window).height()+10 >= $(document).height() && categoryId !== undefined) {
				    const pageNumber = infiniteCount;
					const API_URL = apiUrl + '?id=' + categoryId +'&sortby='+ $("#articlelistsortby").val() +'&offset=' + pageNumber;
				       loadArticles(API_URL,false);
				       infiniteCount++;
				       currentPage++;
				   }
				});

			});
	})(jqry);}
}
