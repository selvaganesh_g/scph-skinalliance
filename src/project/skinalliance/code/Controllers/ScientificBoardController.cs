﻿using Loreal.SkinAlliance.Web.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System.Web.Mvc;

namespace Loreal.SkinAlliance.Web.Controllers
{

    public class ScientificBoardController : StandardController
    {
        public IScientificBoardRepository _iScientificBoardRepository;
        public ScientificBoardController(IScientificBoardRepository iScientificBoardRepository)
        {
            _iScientificBoardRepository = iScientificBoardRepository;
        }
        public ActionResult Banner()
        {
            return PartialView("~/Views/SkinAlliance/Feature/ScientificBoard/ScientificBoardBanner.cshtml", _iScientificBoardRepository.GetModel());
        }
        protected override object GetModel()
        {
            return _iScientificBoardRepository.GetModel();
        }
        protected override string GetIndexViewName()
        {
            return "~/Views/SkinAlliance/Feature/ScientificBoard/ScientificBoard.cshtml";
        }
    }
}