Function Join-Project {
    Param (
        [string]$projectName = $(throw "$projectName required.")

    )
    $Solution = new-Object PsObject -property @{Name = 'SalonSecret'}

    $sourceFolder = (Get-Item -Path ".\" -Verbose).FullName


    # 0. Set up global parameters.
    $websiteFolder = (Get-Item -Path ".\build\Website" -Verbose).FullName

    $rootMachine = "salonsecret"
    $rootHostName = "dev.local"

    $IisName = "{0}.{1}" -f $rootMachine, $rootHostName
    $IisAppPoolName= "{0}.appPool" -f $IisName
    $IisBindings="http/*:80:{0}" -f $IisName





    # Create IIS website pointing to the folder
    #     \build\Website
    # with dedicated application pool.
    Install-Module -Name 'Carbon' -AllowClobber

    Import-Module 'Carbon'
    Install-IisAppPool -Name $IisAppPoolName -ServiceAccount NetworkService
    Install-IisWebsite -Name $IisName -PhysicalPath $buildFolderPath -AppPoolName $IisAppPoolName -Binding $IisBindings  -Verbose


    # Update machine HostName
    Set-HostsEntry -IPAddress 127.0.0.1 -HostName $IisName -Description $IisName




    # 1. Update configuration files in solution
    # foreach _D*.config file in Valtech.Common.Project update property "condition-machineName" with current Machine name.
    Get-ChildItem ".\" -Recurse -Filter _D*.config |
        Foreach-Object {
        $Config = [XML](Get-Content $_.FullName)

        $currentMachine = $env:COMPUTERNAME
        $conditionMachineNames = Select-XML -XML $Config  -XPath '//@condition-machineName'

        Foreach ( $conditionMachineName in $conditionMachineNames) {
            $existingMachineNames = $conditionMachineName.Node.value.Split('|')

            if (-not $existingMachineNames.Contains($currentMachine)) {
                $conditionMachineName.Node.value = '{0}|{1}' -f $conditionMachineName.Node.value, $currentMachine
                Write-Host $conditionMachineName.Node.value
            }
        }
        $Config.Save($_.FullName)
    }



}
