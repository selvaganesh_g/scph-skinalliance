/**
 @module Unicorn/Helper
 @desc Helper Module read the configuration setting from xml file
*/

"use strict";
var xml2js = require("xml2js");
var fs = require("fs");

var habitat = {};


habitat.getConfiguration = function getConfigFile(filename) {
    var data = fs.readFileSync(filename);

    var parser = new xml2js.Parser();
    var content;
    parser.parseString(data, function (err, result) {
        if (err !== null) throw err;

        content = result;
    });
    return content;
};

habitat.getSiteUrl = function getSiteUrl(options) {
    if (!options) options = {};

    var publishFile = options.publishingSettingsFile ? options.publishingSettingsFile : "./publishsettings.targets";
    try {
        var configuration = habitat.getConfiguration(publishFile);
        return configuration.Project.PropertyGroup[0].publishUrl[0];
    } catch (error) {
        error.message = "Could not get the site URL from '" + publishFile + "'. Error:" + error.message;
        throw (error);
    }
};

module.exports = {
    /**
   @funcation getConfiguration
   @desc Helper funcation to transform the xml content to js
   @param {string} filename - configuration file path
   @example <caption>Code usage </caption>
   var publishFile ='./src/publishsetting.targets'
   getConfiguration(publishFile)
   @return {Array} Returns the list of configuration
  */
    getConfiguration: habitat.getConfiguration,
    /**
   @funcation getSiteUrl
   @desc Helper funcation to get Sitecore site url
   @param {Array} filename - configuration file path
   @example <caption>Code usage </caption>
   var publishFile ='./src/publishsetting.targets'
   var config= getConfiguration(publishFile)
   getSiteUrl(config)
   @return {string} Returns the site url
  */
    getSiteUrl: habitat.getSiteUrl
};