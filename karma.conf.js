module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['browserify', 'jasmine', 'mocha'],
    preprocessors: {
      'src/Foundation/**/*.spec.js': ['browserify'],
      //'src/Foundation/**/entry.js': ['browserify'],
      'src/Feature/**/*.spec.js': ['browserify'],
      //'src/Feature/**/entry.js': ['browserify'],
      'src/Project/**/*.spec.js': ['browserify'],
      //'src/Project/**/entry.js': ['browserify'],
    },
    files: [
      'node_modules/babel-polyfill/dist/polyfill.js',
      'src/Foundation/**/*.spec.js',
      'src/Feature/**/*.spec.js',
      //'src/Feature/**/entry.js',
      'src/Project/**/*.spec.js',
      //'src/Project/**/entry.js',
    ],
    plugins: [
      'karma-browserify',
      'karma-jasmine',
      'karma-phantomjs-launcher',
      'karma-mocha',
      'karma-mocha-reporter',
      'karma-coverage',
      'karma-junit-reporter'
    ],
    reporters: ['progress', 'junit','coverage', 'mocha'],
    junitReporter: {
      outputFile: 'reports/junit/TESTS-xunit.xml'
    }, 
    exclude: [],
    port: 7575,
    logLevel: config.LOG_INFO,
    browsers: ['PhantomJS'],
    coverageReporter: {  
    type: 'lcov',
    dir: 'reports',
    subdir: 'coverage'
  }
  });
};
