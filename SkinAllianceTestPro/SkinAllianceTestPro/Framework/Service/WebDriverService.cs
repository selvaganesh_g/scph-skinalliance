﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Appium;
using SkinAllianceTestPro.Framework.Common.EnumType;
using SkinAllianceTestPro.Blocks.PageBlocks;
using SkinAllianceTestPro.Framework.Model;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Android;
using SkinAllianceTestPro.Framework.Common;
using log4net;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;
using System.Drawing;

namespace SkinAllianceTestPro.Service
{
    public class WebDriverService
    {
        private RemoteWebDriver driver;
        private IWebElement highlightedWebElement;
        private string hightlightedWebElementStyle;
        public string BaseUrl = string.Empty;
        public WebDriverType DriverType;
        public ResponsiveType ResponsiveType;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public WebDriverService(RemoteWebDriver webDriver)
        {
            this.driver = webDriver;

            BaseUrl = Config.GetBaseUrl();
            DriverType = Config.GetBrowser();
            ResponsiveType = Config.GetResponsiveType();

        }

        #region "Public Methods"

        #region "Assertion Included Methods"

        public void NavigateTo(string pageName)
        {
            string url = string.Empty;

            logger.Debug("Navigate to base Url " + BaseUrl + "Page: " + pageName);

            try
            {
                url = BaseUrl + PageBlock.GetPage(pageName);
                logger.Debug("Navigate to Url: " + url);
                driver.Navigate().GoToUrl(url);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to navigate to Url:" + url + "; Error: " + ex.Message);
                Assert.Fail("Unable to navigate to Url:" + url + "; Error: " + ex.Message);
            }
        }
        public void NavigateTo(string baseUrl, string pageName)
        {
            string url = string.Empty;
            BaseUrl = baseUrl;

            logger.Debug("Navigate to base Url " + BaseUrl + "Page: " + pageName);

            try
            {
                url = BaseUrl + PageBlock.GetPage(pageName);
                logger.Debug("Navigate to Url: " + url);
                driver.Navigate().GoToUrl(url);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to navigate to Url:" + url + "; Error: " + ex.Message);
                Assert.Fail("Unable to navigate to Url:" + url + "; Error: " + ex.Message);
            }

        }
        public IWebElement GetElement(By locator)
        {
            IWebElement element = null;

            logger.Debug("Get Element for " + locator.ToString());

            try
            {
                element = getElementFromParent(locator, null);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to get element for locator " + locator.ToString() + "Error: " + ex.Message);
                Assert.Fail("Unable to get element for locator " + locator.ToString() + "Error: " + ex.Message);
            }

            return element;
        }
        public IWebElement GetElement(string name)
        {
            IWebElement element = null;

            logger.Debug("Get Element for " + name);

            try
            {
                LocatorDetails locatorDetails = ElementUtil.GetLocatorDetails(name);

                foreach (String key in locatorDetails.ElementIdentifier)
                {
                    element = findElement(locatorDetails.LocatorType, key, element);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Unable to get element " + name + "Error: " + ex.Message);
                Assert.Fail("Unable to get element  " + name + "Error: " + ex.Message);
            }

            return element;

        }

        public IList<IWebElement> GetElements(string name)
        {
            IList<IWebElement> elementList = null;

            logger.Debug("Get Elements for " + name);
            try
            {
                LocatorDetails locatorDetails = ElementUtil.GetLocatorDetails(name);
                elementList = findElements(locatorDetails.LocatorType, locatorDetails.ElementIdentifier[0]);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to get elements for " + name + "Error: " + ex.Message);
                Assert.Fail("Unable to get elements for " + name + "Error: " + ex.Message);
            }

            return elementList;
        }

        public IList<IWebElement> GetElements(By locator)
        {

            IList<IWebElement> elementList = null;

            logger.Debug("Get Elements for " + locator.ToString());
            try
            {
                elementList = findElements(locator);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to get elements for " + locator.ToString() + "Error: " + ex.Message);
                Assert.Fail("Unable to get elements for " + locator.ToString() + "Error: " + ex.Message);
            }

            return elementList;

        }

        public void ScrollTo(IWebElement element, bool topFlag = true)
        {
            try
            {
                logger.Debug("Scroll to " + element.TagName);
                scrollTo(element, topFlag);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to scroll to " + element + "Error: " + ex.Message);
                Assert.Fail("Unable to scroll to " + element + "Error: " + ex.Message);
            }
        }
        public void ScrollTo(string name, bool topFlag = true)
        {
            try
            {
                logger.Debug("Scroll to element " + name);
                scrollTo(GetElement(name), topFlag);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to scroll to element " + name + "Error: " + ex.Message);
                Assert.Fail("Unable to scroll to element " + name + "Error: " + ex.Message);
            }


        }
        public void ScrollAndClick(string name, bool topFlag = true)
        {
            IWebElement element = null;

            try
            {
                logger.Debug("Scroll to click on element " + name);
                element = getElement(name);
                scrollTo(element, topFlag);
                clickOnElement(element);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to scroll and click on element " + name + "Error: " + ex.Message);
                Assert.Fail("Unable to scroll and click on element " + name + "Error: " + ex.Message);
            }

        }
        public void ScrollAndClick(IWebElement element, bool topFlag = true)
        {
            try
            {
                logger.Debug("Scroll to click on element " + element.TagName);
                scrollTo(element, topFlag);
                clickOnElement(element);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to scroll and click on element " + element.TagName + "Error: " + ex.Message);
                Assert.Fail("Unable to scroll and click on element " + element.TagName + "Error: " + ex.Message);
            }

        }
        public void ScrollAndJsClick(string name, bool topFlag = true)
        {
            IWebElement element = null;

            try
            {
                logger.Debug("Scroll to click on element " + name);
                element = getElement(name);
                scrollAndJsClick(element, topFlag);

            }
            catch (Exception ex)
            {
                logger.Error("Unable to scroll and click on element " + name + "Error: " + ex.Message);
                Assert.Fail("Unable to scroll and click on element " + name + "Error: " + ex.Message);
            }

        }
        public void ScrollAndJsClick(IWebElement element, bool topFlag = true)
        {
            try
            {
                logger.Debug("Scroll to click on element " + element.TagName);
                scrollAndJsClick(element, topFlag);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to scroll and click on element " + element.TagName + "Error: " + ex.Message);
                Assert.Fail("Unable to scroll and click on element " + element.TagName + "Error: " + ex.Message);
            }

        }
        public void Click(string name)
        {
            try
            {
                logger.Debug("Click on element " + name);
                clickOnElement(GetElement(name));
            }
            catch (Exception ex)
            {
                logger.Error("Unable to click on element " + name + "Error: " + ex.Message);
                Assert.Fail("Unable to click on element " + name + "Error: " + ex.Message);
            }
        }
        public void Click(By by)
        {
            try
            {
                logger.Debug("Click on element " + by.ToString());
                clickOnElement(GetElement(by));
            }
            catch (Exception ex)
            {
                logger.Error("Unable to click on element " + by.ToString() + "Error: " + ex.Message);
                Assert.Fail("Unable to click on element " + by.ToString() + "Error: " + ex.Message);
            }


        }
        public void Click(IWebElement element)
        {
            try
            {
                logger.Debug("Click on element " + element.TagName);
                clickOnElement(element);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to click on element " + element.TagName + "Error: " + ex.Message);
                Assert.Fail("Unable to click on element " + element.TagName + "Error: " + ex.Message);
            }


        }

        public void EnterText(string elementName, string text)
        {
            IWebElement element = null;

            try
            {
                logger.Debug("Enter text as " + text + " in " + elementName);
                element = GetElement(elementName);
                element.SendKeys(text);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to enter text as " + text + "  in element " + elementName + "Error: " + ex.Message);
                Assert.Fail("Unable to enter text as " + text + "  in element " + elementName + "Error: " + ex.Message);
            }

        }

        public void ClearAndEnterText(string elementName, string text)
        {
            IWebElement element = null;

            try
            {
                logger.Debug("Clear and enter text as " + text + " in " + elementName);
                element = GetElement(elementName);
                element.Clear();
                element.SendKeys(text);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to enter text as " + text + "  in element " + elementName + "Error: " + ex.Message);
                Assert.Fail("Unable to enter text as " + text + "  in element " + elementName + "Error: " + ex.Message);
            }

        }

        public void SwitchToBrowserTab()
        {
            logger.Debug("Switch to browser tab");

            try
            {
                var browserTabs = driver.WindowHandles;
                driver.SwitchTo().Window(browserTabs[1]);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to switch to tab. Error: " + ex.Message);
                Assert.Fail("Unable to switch to tab. Error: " + ex.Message);
            }

        }

        public void SwitchToParentTab()
        {
            logger.Debug("Switch to parent tab");

            try
            {
                var browserTabs = driver.WindowHandles;
                driver.SwitchTo().Window(browserTabs[0]);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to switch to parent tab. Error: " + ex.Message);
                Assert.Fail("Unable to switch to parent tab. Error: " + ex.Message);
            }

        }
        public void HoverElement(IWebElement element)
        {
            logger.Debug("Hover the element");

            try
            {
                Actions action = new Actions(driver);
                action.MoveToElement(element).Perform();
            }
            catch (Exception ex)
            {
                logger.Error("Unable to hover the element. Error: " + ex.Message);
                Assert.Fail("Unable to hover the element. Error: " + ex.Message);
            }

            

        }

        

        public Point GetImageLocation(string name)
        {
            IWebElement element = null;
            Point imageLocation = new Point();

            logger.Debug("Get image position for " + name);

            try
            {
                element = getElement(name);
                imageLocation = element.Location;
                logger.Debug("Image X Co-ordinate: " + imageLocation.X + "; Y Co-ordinate: " + imageLocation.Y);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to get image location for the element. Error: " + ex.Message);
                Assert.Fail("Unable to get image location for the element. Error: " + ex.Message);
            }

            return imageLocation;

        }

        public Point GetImageLocation(IWebElement element)
        {
            Point imageLocation = new Point();

            logger.Debug("Get image position for " + element.TagName);

            try
            {
                imageLocation = element.Location;
                logger.Debug("Image X Co-ordinate: " + imageLocation.X + "; Y Co-ordinate: " + imageLocation.Y);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to get image location for the element. Error: " + ex.Message);
                Assert.Fail("Unable to get image location for the element. Error: " + ex.Message);
            }

            return imageLocation;

        }

        public Size GetImageSize(string name)
        {
            IWebElement element = null;
            Size imageSize = new Size();

            logger.Debug("Get image size for " + name);

            try
            {
                element = getElement(name);
                imageSize = element.Size;
                logger.Debug("Image Height: " + imageSize.Height + "; Width: " + imageSize.Width);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to get image size for the element. Error: " + ex.Message);
                Assert.Fail("Unable to get the image size for the element. Error: " + ex.Message);
            }

            return imageSize;

        }

        public Size GetImageSize(IWebElement element)
        {
            Size imageSize = new Size();

            logger.Debug("Get image size for " + element.TagName);

            try
            {
                imageSize = element.Size;
                logger.Debug("Image Height: " + imageSize.Height + "; Width: " + imageSize.Width);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to get image size for the element. Error: " + ex.Message);
                Assert.Fail("Unable to get the image size for the element. Error: " + ex.Message);
            }

            return imageSize;

        }

        #endregion

        public void SetWebDriver(RemoteWebDriver webDriver)
        {
            driver = webDriver;

        }
        public RemoteWebDriver GetWebDriver()
        {
            return driver;
        }
        public void MaximizeWindow()
        {
            if (ResponsiveType == ResponsiveType.DESKTOP)
                driver.Manage().Window.Maximize();
        }
        public void DeleteAllCookies()
        {
            driver.Manage().Cookies.DeleteAllCookies();
        }
        public string GetCurrentUrl()
        {
            return driver.Url;
        }
        public String GetPageTitle()
        {
            return driver.Title;
        }
        public void HighLightElement(IWebElement element)
        {
            if (highlightedWebElement != null)
            {
                try
                {
                    driver.ExecuteScript("arguments[0].style.border='" + hightlightedWebElementStyle + "';", highlightedWebElement);
                }
                catch { }

            }

            try
            {
                highlightedWebElement = element;
                hightlightedWebElementStyle = (String)driver.ExecuteScript("return arguments[0].style.border;", element);
                driver.ExecuteScript("arguments[0].style.border='3px dotted red';", element);
            }
            catch { }

        }
        public void ScrollBy(int pixels)
        {
            driver.ExecuteScript("window.scrollBy(0," + pixels + ")", "");
        }

        #endregion

        #region "Private Methods"
        private IWebElement getElement(By locator)
        {
            return getElementFromParent(locator, null);
        }
        private IWebElement getElement(string name)
        {
            IWebElement element = null;
            LocatorDetails locatorDetails = ElementUtil.GetLocatorDetails(name);

            foreach (String key in locatorDetails.ElementIdentifier)
            {
                element = findElement(locatorDetails.LocatorType, key, element);
            }

            return element;

        }
        private IWebElement findElement(LocatorType selectorType, string name, IWebElement parent)
        {
            IWebElement element = null;
            By locator = ElementUtil.GetLocator(selectorType, name);
            element = getElementFromParent(locator, parent);
            return element;

        }
        private IWebElement getElementFromParent(By locator, IWebElement parent)
        {
            if (parent == null)
            {
                return driver.FindElement(locator);
            }
            else
            {
                return parent.FindElement(locator);
            }
        }

        private IList<IWebElement> findElements(LocatorType selectorType, string name)
        {
            By locator = ElementUtil.GetLocator(selectorType, name);
            return findElements(locator);
        }
        private IList<IWebElement> findElements(By locator)
        {
            return driver.FindElements(locator);
        }
        private void scrollTo(IWebElement element, bool topFlag = true)
        {
            driver.ExecuteScript("arguments[0].scrollIntoView(" + topFlag.ToString().ToLower() + ");", element);
        }
        private void scrollAndJsClick(IWebElement element, bool topFlag = true)
        {
            scrollTo(element, topFlag);
            driver.ExecuteScript("arguments[0].click();", element);
        }
        private void clickOnElement(IWebElement element)
        {
            HighLightElement(element);
            element.Click();
        }

        private void enterTextOnElement(IWebElement element, string text)
        {
            HighLightElement(element);
            element.SendKeys(text);
        }

        #endregion

    }
}
