﻿using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common.EnumType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Framework.Common
{
    public class Util
    {
        //public static ResponsiveType GetResponsiveType(WebDriverType webDriverType)
        //{
        //    ResponsiveType responsiveType;

        //    switch (webDriverType)
        //    {
        //        case WebDriverType.IE:
        //        case WebDriverType.FIREFOX:
        //        case WebDriverType.CHROME:
        //            responsiveType = ResponsiveType.DESKTOP;
        //            break;
        //        case WebDriverType.ANDROID:
        //        case WebDriverType.IOS:
        //            responsiveType = ResponsiveType.MOBILE;
        //            break;
        //        default:
        //            responsiveType = ResponsiveType.DESKTOP;
        //            break;
        //    }

        //    return responsiveType;
        //}

        public static string CaptureScreenShot(IWebDriver driver, string screenshotName)
        {
            ITakesScreenshot takesScreenshot = (ITakesScreenshot)driver;
            Screenshot screenshot = takesScreenshot.GetScreenshot();

            string path = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string finalPath = path.Substring(0, path.LastIndexOf("bin")) + "ErrorReports\\" + screenshotName;
            string localPath = new Uri(finalPath).LocalPath;
            screenshot.SaveAsFile(localPath, ScreenshotImageFormat.Png);
            return localPath;
           
        }

        public static string FireFox_Chrome(String Firefox, String Chrome)
        {
            String Value = "";
            if (Config.GetBrowser().Equals(WebDriverType.FIREFOX))
            {
                Value = Firefox;
            }
            else
            {
                Value = Chrome;
            }
            return Value;
        }

        

        public static int GetRandomNumber(int size)
        {
           
            Random rnd = new Random();
            size = rnd.Next(1, size - 1);
            return size;

        }

    }
}

