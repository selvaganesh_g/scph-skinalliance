﻿using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common.EnumType;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SkinAllianceTestPro.Framework.Common
{
    class ElementUtil
    {
        #region "Private Variables"

        private static char NAME_SEPARATOR = '.';

        #endregion

        #region "Public Methods"

        public static string GetName(string blockName, Dictionary<string, string> elementList, string key)
        {
            String elementKey = "";

            if (elementList == null || elementList.Keys.Count < 1)
            {
                Assert.Fail("No elements are found in " + blockName + " block");
            }

            try
            {

                String[] keyList = key.Split('>');

                if (keyList != null && keyList.Length > 0)
                {

                    elementKey = elementList[keyList[0].Trim().ToString()];

                    for (int i = 1; i < keyList.Length; i++)
                    {
                        elementKey = String.Format(elementKey, keyList[i].Trim());
                    }

                }
                else
                {
                    elementKey = elementList[elementKey.ToString()];
                }

            }
            catch (Exception e)
            {
                Assert.Fail(key + " not found in the " + blockName + " block element list", e.Message);

            }

            return elementKey;

        }

        public static By GetLocator(LocatorType locatorType, string selector)
        {
            By by;

            switch (locatorType)
            {
                case LocatorType.XPATH:
                    by = By.XPath(selector);
                    break;
                case LocatorType.LINKTEXT:
                    by = By.LinkText(selector);
                    break;
                case LocatorType.ID:
                    by = By.Id(selector);
                    break;
                case LocatorType.NAME:
                    by = By.Name(selector);
                    break;
                case LocatorType.CSS:
                    by = By.CssSelector(selector);
                    break;
                case LocatorType.CLASSNAME:
                    by = By.ClassName(selector);
                    break;
                case LocatorType.PARTIAL_LINKTEXT:
                    by = By.PartialLinkText(selector);
                    break;
                case LocatorType.TAG:
                    by = By.TagName(selector);
                    break;
                default:
                    throw new Exception("The property specified is not supported. Property=" + locatorType.ToString() + ", object=" + selector);

            }

            return by;
        }

        public static LocatorDetails GetLocatorDetails(string name)
        {
            LocatorDetails locatorDetails;

            locatorDetails = new LocatorDetails();

            try
            {
                string[] elementDetail = name.Split('~');

                locatorDetails.ElementIdentifier = elementDetail[0].Split('^');
                locatorDetails.LocatorType = DriverUtil.getLocatorType(elementDetail[1]);

            }
            catch (Exception ex)
            {
                Assert.Fail("Unable to get locator detail for " + name + " Error: " + ex.Message);
            }

            return locatorDetails;

        }

        public static string GetName(string name)
        {
            Dictionary<string, string> identifierList = new Dictionary<string, string>();
            string[] splitName = name.Split(NAME_SEPARATOR);
            string key = name;
            string value = string.Empty;
            List<string> blockList = null;

            try
            {

                if (splitName == null || splitName.Length < 2)
                {
                    blockList = GetBlockList();

                    foreach (string block in blockList)
                    {
                        identifierList = GetIdentifierList(block.ToUpper());
                        if (identifierList.ContainsKey(key))
                        {
                            break;
                        }
                    }
                }
                else
                {
                    string blockName = splitName[0].ToUpper();
                    key = splitName[1];

                    identifierList = GetIdentifierList(blockName);

                    value = identifierList[key];
                }

                value = identifierList[key];
            }
            catch (Exception ex)
            {
                Assert.Fail("Unable to get locator detail for " + name + " Error: " + ex.Message);
            }
            return value;
        }

        public static List<string> GetBlockList()
        {
            List<string> blockList = new List<string>();
            blockList.Add(HeaderElements.BLOCK_NAME);
            blockList.Add(CommonElements.BLOCK_NAME);

            return blockList;
        }

        public static Dictionary<string, string> GetIdentifierList(string blockName)
        {
            Dictionary<string, string> identifierList = new Dictionary<string, string>();

            if (HeaderElements.BLOCK_NAME.ToUpper() == blockName)
            {
                identifierList = HeaderElements.GetElementList();
            }
            else if (CommonElements.BLOCK_NAME.ToUpper() == blockName)
            {
                identifierList = CommonElements.GetElementList();
            }

            return identifierList;
        }

        #endregion

    }
}
