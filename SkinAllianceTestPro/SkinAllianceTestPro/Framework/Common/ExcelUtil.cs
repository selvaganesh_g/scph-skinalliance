﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPOI;
using NPOI.HSSF.UserModel;
using System.IO;
using NPOI.SS.UserModel;
using log4net;
using log4net.Config;
using System.Data;

namespace SkinAllianceTestPro.Framework.Common
{
    public class ExcelUtil
    {

        public static string GetCellValue(string fileName, string sheetName, int rowIndex, int columnIndex)
        {

            ISheet worksheet = getSheet(fileName, sheetName);
            IRow row = worksheet.GetRow(rowIndex);
            ICell cell = row.GetCell(columnIndex);

            return cell.StringCellValue;

        }

        public static string GetCellValue(string fileName, string sheetName, string rowId, string columnId)
        {

            ISheet worksheet = getSheet(fileName, sheetName);

            int rowIndex = 0;
            int colIndex = 0;

            //Get Column Name
            IRow firstRow = worksheet.GetRow(0);
            for (int colCount = 1; colCount <= firstRow.LastCellNum; colCount++)
            {
                string cellValue = firstRow.GetCell(colCount).StringCellValue.Trim();
                if (!string.IsNullOrEmpty(cellValue) && columnId.ToUpper() == cellValue.ToUpper())
                {
                    colIndex = colCount;
                    break;
                }
            }

            //Get Row Name
            for (int rowCount = 1; rowCount <= worksheet.LastRowNum; rowCount++)
            {
                string cellValue = worksheet.GetRow(rowCount).GetCell(0).StringCellValue.Trim();
                if (!string.IsNullOrEmpty(cellValue) && rowId.ToUpper() == cellValue.ToUpper())
                {
                    rowIndex = rowCount;
                    break;
                }
            }

            return worksheet.GetRow(rowIndex).GetCell(colIndex).StringCellValue;

        }
        public static DataTable GetRecordsFromExcel(string fileName, string sheetName, List<TableContent> rowList, List<TableContent> columnList)
        {
            ISheet worksheet = getSheet(fileName, sheetName);

            //Get Row and Colmun Index
            getRowIndexList(worksheet, rowList, false);
            getColumnIndexList(worksheet, columnList);

            return getRecords(worksheet, rowList, columnList);

        }

        public static DataTable GetRecordsFromExcel(string fileName, string sheetName, int fromRowId, int toRowId, List<TableContent> columnList)
        {
            ISheet worksheet = getSheet(fileName, sheetName);

            //Get Row and Colmun Index
            getColumnIndexList(worksheet, columnList);

            return getRecords(worksheet, getRowListwithIndex(fromRowId, toRowId), columnList);
        }

        public static DataTable GetRecordsFromExcel(string fileName, string sheetName, List<TableContent> rowList, string columnNames)
        {
            ISheet worksheet = getSheet(fileName, sheetName);

            //Get Row and Colmun Index
            getRowIndexList(worksheet, rowList, false);

            List<TableContent> columnList = getColumnListwithName(columnNames);
            getColumnIndexList(worksheet, columnList);

            return getRecords(worksheet, rowList, columnList);

        }

        public static DataTable GetRecordsFromExcel(string fileName, string sheetName, string columnNames)
        {
            ISheet worksheet = getSheet(fileName, sheetName);

            List<TableContent> columnList = getColumnListwithName(columnNames);
            getColumnIndexList(worksheet, columnList);

            return getRecords(worksheet, getRowListwithIndex(worksheet), columnList);

        }

        public static DataTable GetRecordsFromExcel(string fileName, string sheetName, int fromRowId, int toRowId, string columnNames)
        {
            ISheet worksheet = getSheet(fileName, sheetName);

            //Get Row and Colmun Index
            List<TableContent> rowList = getRowListwithIndex(fromRowId, toRowId);
            List<TableContent> columnList = getColumnListwithName(columnNames);

            getColumnIndexList(worksheet, columnList);

            return getRecords(worksheet, rowList, columnList);

        }

        private static void getRowIndexList(ISheet worksheet, List<TableContent> rowList, bool checkEnd)
        {
            int checkColCount = 0;

            if (checkEnd)
            {
                for (int rowCount = 1; rowCount <= worksheet.LastRowNum; rowCount++)
                {
                    string cellValue = worksheet.GetRow(rowCount).GetCell(0).StringCellValue.Trim();

                    rowList[rowCount].Index = rowCount;
                    if (cellValue.Trim().ToUpper() == "END") break;

                }
            }
            else
            {

                //Get Rows
                for (int rowCount = 1; rowCount <= worksheet.LastRowNum; rowCount++)
                {
                    string cellValue = worksheet.GetRow(rowCount).GetCell(0).StringCellValue.Trim();

                    // Compare from given column list
                    for (int rowListCount = 0; rowListCount < rowList.Count; rowListCount++)
                    {
                        //Check if already contains row index (-1 - means don't have row index)
                        if (rowList[rowListCount].Index < 0)
                        {
                            if (!string.IsNullOrEmpty(cellValue) && rowList[rowListCount].Name.ToUpper() == cellValue.ToUpper())
                            {
                                rowList[rowListCount].Index = rowCount;
                                checkColCount++;
                            }
                        }
                    }

                    if (checkColCount == rowList.Count) break;
                }
            }

        }

        private static List<TableContent> getRowListwithIndex(int fromRowId, int toRowId)
        {
            List<TableContent> rowList = new List<TableContent>();
            for (int row = fromRowId; row <= toRowId; row++)
            {
                rowList.Add(new TableContent { Index = row });
            }

            return rowList;

        }

        private static List<TableContent> getRowListwithIndex(ISheet worksheet)
        {
            List<TableContent> rowList = new List<TableContent>();

            for (int rowCount = 1; rowCount <= worksheet.LastRowNum; rowCount++)
            {
                string cellValue = worksheet.GetRow(rowCount).GetCell(0).StringCellValue.Trim();

                if (cellValue.Trim().ToUpper() == "END") break;

                rowList.Add(new TableContent { Index = rowCount });
                rowList[rowCount - 1].Index = rowCount;

            }

            return rowList;
        }
        private static void getColumnIndexList(ISheet worksheet, List<TableContent> columnList)
        {
            //Get first row to identify the column name

            int checkColCount = 0;
            IRow firstRow = worksheet.GetRow(0);

            for (int colIndex = 1; colIndex <= firstRow.LastCellNum; colIndex++)
            {
                //Get the cell value
                string cellValue = firstRow.GetCell(colIndex).StringCellValue.Trim();

                // Compare from given column list
                for (int colListIndex = 0; colListIndex < columnList.Count; colListIndex++)
                {
                    //Check if already contains column index (-1 - means don't have column index)
                    if (columnList[colListIndex].Index < 0)
                    {
                        if (!string.IsNullOrEmpty(cellValue) && columnList[colListIndex].Name.ToUpper() == cellValue.ToUpper())
                        {
                            columnList[colListIndex].Index = colIndex;
                            checkColCount++;
                        }
                    }
                }

                if (checkColCount == columnList.Count) break;
            }
        }
        private static List<TableContent> getColumnListwithName(string colList)
        {
            List<TableContent> columnList = new List<TableContent>();

            string[] colNames = colList.Split(',');

            for (int col = 0; col < colNames.Length; col++)
            {
                columnList.Add(new TableContent { Name = colNames[col] });
            }

            return columnList;
        }
        private static DataTable getRecords(ISheet worksheet, List<TableContent> rowList, List<TableContent> columnList)
        {
            DataTable dt = new DataTable();

            //Add column
            foreach (TableContent col in columnList)
            {
                dt.Columns.Add(col.Name);
            }

            for (int row = 0; row < rowList.Count; row++)
            {
                DataRow dRow = dt.NewRow();

                for (int compareCol = 0; compareCol < columnList.Count; compareCol++)
                {
                    dRow[columnList[compareCol].Name] = worksheet.GetRow(rowList[row].Index).GetCell(columnList[compareCol].Index).StringCellValue;
                }

                dt.Rows.Add(dRow);
            }

            return dt;

        }
        private static ISheet getSheet(string fileName, string sheetName)
        {
            IWorkbook workbook = null;
            ISheet worksheet = null;

            string filePath = fileName;

            if (!System.IO.File.Exists(filePath))
            {
                throw new Exception("File not found. " + filePath);
            }

            using (FileStream fileStream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Read))
            {
                workbook = WorkbookFactory.Create(fileStream);
                worksheet = workbook.GetSheet(sheetName);

                if (worksheet == null)
                {
                    throw new Exception("Worksheet not found. " + sheetName);
                }

            }

            return worksheet;

        }


        //public static DataTable GetRecordsFromExcel(string fileName, string sheetName, List<TableContent> rowList, List<TableContent> colList)

        //{
        //    DataTable dt = new DataTable();

        //    ISheet worksheet = getSheet(fileName, sheetName);

        //    //Get first row to identify the column name
        //    IRow firstRow = worksheet.GetRow(0);
        //    for (int colCount = 1; colCount <= firstRow.LastCellNum; colCount++)
        //    {
        //        //Get the cell value
        //        string cellValue = firstRow.GetCell(colCount).StringCellValue.Trim();

        //        // Compare from given column list
        //        for (int compareCol = 0; compareCol < colList.Count; compareCol++)
        //        {
        //            //Check if already contains column index (-1 - means don't have column index)
        //            if (colList[compareCol].Index < 0)
        //            {
        //                if (!string.IsNullOrEmpty(cellValue) && colList[compareCol].Name.ToUpper() == cellValue.ToUpper())
        //                {
        //                    colList[compareCol].Index = colCount;
        //                }
        //            }
        //        }
        //    }

        //    //Get Rows
        //    for (int rowCount = 1; rowCount <= worksheet.LastRowNum; rowCount++)
        //    {
        //        string cellValue = worksheet.GetRow(rowCount).GetCell(0).StringCellValue.Trim();

        //        // Compare from given column list
        //        for (int compareRow = 0; compareRow < rowList.Count; compareRow++)
        //        {
        //            //Check if already contains row index (-1 - means don't have row index)
        //            if (rowList[compareRow].Index < 0)
        //            {
        //                if (!string.IsNullOrEmpty(cellValue) && rowList[compareRow].Name.ToUpper() == cellValue.ToUpper())
        //                {
        //                    rowList[compareRow].Index = rowCount;
        //                }
        //            }
        //        }
        //    }


        //    //Get the data

        //    //Add column
        //    foreach (TableContent col in colList)
        //    {
        //        dt.Columns.Add(col.Name);
        //    }

        //    for (int compareRow = 0; compareRow < rowList.Count; compareRow++)
        //    {
        //        DataRow dRow = dt.NewRow();

        //        for (int compareCol = 0; compareCol < colList.Count; compareCol++)
        //        {
        //            dRow[colList[compareCol].Name] = worksheet.GetRow(rowList[compareRow].Index).GetCell(rowList[compareCol].Index).StringCellValue;
        //        }

        //        dt.Rows.Add(dRow);
        //    }

        //    return dt;
        //}


    }

    public class TableContent
    {
        public string Name;
        public int Index;

        public TableContent()
        {
            Name = string.Empty;
            Index = -1;
        }

    }
}
