﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Framework.Common
{
    public class UiValidationUtil
    {
        public void ValidateImagePosition(string expectedImageLocation, Point actualImageLocation)
        {

            try
            {

                String[] imgCordinates = expectedImageLocation.Split('X');
                int Xcoordinate = Int32.Parse(imgCordinates[0]);
                int Ycoordinate = Int32.Parse(imgCordinates[1]);

                Assert.True((Xcoordinate == actualImageLocation.X && Ycoordinate == actualImageLocation.Y), "Mismatch image position. Expected X and Y Co-Ordinates: " +
                      expectedImageLocation + "; Actual: X and Y Co-Ordinates: " + actualImageLocation.X + "X" + actualImageLocation.Y);
            }
            catch (Exception ex)
            {
                Assert.Fail("Failed to check image position; Error" + ex.Message);
            }

        }

        public void ValidateImageSize(string expectedImageSize, Size actualImageSize)
        {

            try
            {
                String[] imgSize = expectedImageSize.Split('X');
                int height = Int32.Parse(imgSize[0]);
                int width = Int32.Parse(imgSize[1]);

                Assert.True((height == actualImageSize.Height && width == actualImageSize.Width), "Mismatch image size. Expected Height and Width: " +
                      expectedImageSize + "; Actual: Height and Width: " + actualImageSize.Height + "X" + actualImageSize.Width);
            }
            catch (Exception ex)
            {
                Assert.Fail("Failed to check image size; Error" + ex.Message);
            }

        }
    }
}
