﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Framework.Common.EnumType
{
    public enum LocatorType
    {
        XPATH,
        ID,
        LINKTEXT,
        CSS,
        PARTIAL_LINKTEXT,
        CLASSNAME,
        NAME,
        TAG

    }
}
