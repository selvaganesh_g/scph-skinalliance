﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Framework.Common.EnumType
{
    public enum WebDriverType
    {
        FIREFOX,
        CHROME,
        IE,
        SAFARI,
        IOS,
        ANDROID
    }
}
