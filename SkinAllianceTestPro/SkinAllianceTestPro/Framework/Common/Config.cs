﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using SkinAllianceTestPro.Framework.Common.EnumType;
using System.IO;

namespace SkinAllianceTestPro.Framework.Common
{
    public class Config
    {
        #region "Public Method"

        /// <summary>
        /// Reads setting from app.config
        /// </summary>
        /// <param name="key">Key Name</param>
        /// <returns>Key value</returns>
        public static string Read(string key)
        {
            string value = string.Empty;

            // load config document for current assembly
            XmlDocument doc = loadConfigDocument();

            // retrieve appSettings node
            XmlNode node = doc.SelectSingleNode("//appSettings");

            if (node != null)
            {
                try
                {
                    // select the 'add' element that contains the key
                    XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));

                    if (elem != null)
                    {
                        value = elem.GetAttribute("value");
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Configuration key (" + key + ") not found.", e);
                }
            }

            return value;
        }

        public static WebDriverType GetBrowser()
        {
            WebDriverType browserType;

            string value = Read("BROWSER");

            switch (value.ToUpper())
            {
                case "FIREFOX":
                    browserType = WebDriverType.FIREFOX;
                    break;
                case "CHROME":
                    browserType = WebDriverType.CHROME;
                    break;
                case "IE":
                case "INTERNETEXPLORER":
                    browserType = WebDriverType.IE;
                    break;
                case "ANDROID":
                    browserType = WebDriverType.ANDROID;
                    break;

                default:
                    browserType = WebDriverType.FIREFOX;
                    break;
            }

            return browserType;
        }

        public static ResponsiveType GetResponsiveType()
        {
            ResponsiveType responsiveType;
            WebDriverType webDriverType = GetBrowser();

            switch (webDriverType)
            {
                case WebDriverType.IE:
                case WebDriverType.FIREFOX:
                case WebDriverType.CHROME:
                    responsiveType = ResponsiveType.DESKTOP;
                    break;
                case WebDriverType.ANDROID:
                case WebDriverType.IOS:
                    responsiveType = ResponsiveType.MOBILE;
                    break;
                default:
                    responsiveType = ResponsiveType.DESKTOP;
                    break;
            }

            return responsiveType;
        }

        public static string GetBaseUrl()
        {
            return Read("BASE_URL");
        }

        public static string GetLogDir()
        {
            string logPath = getResourceLocation() + "\\Logs";

            if (!Directory.Exists(logPath))
                Directory.CreateDirectory(logPath);

            return getResourceLocation() + "\\Logs";
        }

        public static string GetReportDir()
        {
            return getResourceLocation() + "\\Reports";
        }

        public static string GetTestDataDir()
        {
            return getResourceLocation() + "\\TestData";
        }

        public static string GetTestDataFile()
        {
            return GetTestDataDir() +  "\\" + Read("TEST_DATA");
        }

        public static string GetDriverDir()
        {
            return getResourceLocation() + "\\Driver";
        }

        public static string GetReportFile(string key)
        {
            return GetReportDir() + "\\" + Read(key);
        }



        #endregion

        #region "Private Method"

        private static XmlDocument loadConfigDocument()
        {
            XmlDocument doc = null;
            try
            {
                doc = new XmlDocument();
                doc.Load(getConfigFilePath());
                return doc;
            }
            catch (System.IO.FileNotFoundException e)
            {
                throw new Exception("No configuration file found.", e);
            }
        }

        private static string getConfigFilePath()
        {
            return Assembly.GetExecutingAssembly().Location + ".config";
        }

        private static string getResourceLocation()
        {
            string filePath = string.Empty;

            try
            {
                filePath = Assembly.GetExecutingAssembly().Location;
                DirectoryInfo logDir = new DirectoryInfo(filePath);

                if (logDir != null)
                {
                    filePath = logDir.Parent.Parent.Parent.Parent.FullName + "\\Resources";
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Unable to find the Log folder to write the Log.");
            }

            return filePath;
        }

        private static string getAssemplyLocation()
        {
            return Assembly.GetExecutingAssembly().Location;
        }

        #endregion
    }
}
