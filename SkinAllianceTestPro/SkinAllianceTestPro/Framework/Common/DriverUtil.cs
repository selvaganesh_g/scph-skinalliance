﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using SkinAllianceTestPro.Framework.Common.EnumType;
using SkinAllianceTestPro.Framework.Common;

namespace SkinAllianceTestPro.Framework.Common
{
    public class DriverUtil
    {
        private static string DRIVER_KEY = "WEB_DRIVER";
        public static LocatorType getLocatorType(string locator)
        {
            LocatorType locatorType;

            switch (locator.ToUpper())
            {
                case "XPATH":
                    locatorType = LocatorType.XPATH;
                    break;
                case "LINKTEXT":
                    locatorType = LocatorType.LINKTEXT;
                    break;
                case "NAME":
                    locatorType = LocatorType.NAME;
                    break;
                case "ID":
                    locatorType = LocatorType.ID;
                    break;
                case "CSS":
                    locatorType = LocatorType.CSS;
                    break;
                case "CLASSNAME":
                    locatorType = LocatorType.CLASSNAME;
                    break;
                case "PARTIAL_LINKTEXT":
                    locatorType = LocatorType.PARTIAL_LINKTEXT;
                    break;
                case "TAG":
                    locatorType = LocatorType.TAG;
                    break;
                default:
                    locatorType = LocatorType.XPATH;
                    break;
            }

            return locatorType;
        }
        public static RemoteWebDriver InitializeWebDriver()
        {
            RemoteWebDriver driver = GetWebDriver(Config.GetBrowser());

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(Convert.ToDouble(Config.Read("IMPLICIT_WAIT_IN_SECONDS")));
            driver.Manage().Window.Maximize();
            return driver;
        }
        public static void QuitWebDriver(RemoteWebDriver driver)
        {
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Quit();
            driver = null;
        }
        public static RemoteWebDriver GetWebDriver(WebDriverType webDriverType)
        {
            RemoteWebDriver webDriver;
            AppiumDriver<IWebElement> _driver;

            switch (webDriverType)
            {
                case WebDriverType.FIREFOX:
                    webDriver = new FirefoxDriver();
                    break;
                case WebDriverType.CHROME:
                    webDriver = new ChromeDriver();

                    break;
                case WebDriverType.IE:

                    InternetExplorerOptions ieOpt = new InternetExplorerOptions();
                    ieOpt.EnableNativeEvents = false;
                    ieOpt.EnsureCleanSession = true;
                    ieOpt.IgnoreZoomLevel = true;
                    ieOpt.ElementScrollBehavior = InternetExplorerElementScrollBehavior.Bottom;

                    webDriver = new InternetExplorerDriver(ieOpt);
                    break;
                case WebDriverType.ANDROID:

                    DesiredCapabilities cap = DesiredCapabilities.Android();
                    cap.SetCapability(MobileCapabilityType.BrowserName, MobileBrowserType.Chrome);
                    cap.SetCapability(MobileCapabilityType.PlatformName, PlatformType.Android);
                    cap.SetCapability(MobileCapabilityType.PlatformVersion, "6.0");
                    cap.SetCapability(MobileCapabilityType.DeviceName, Config.Read("ANDROID_DEVICE_ID"));
                    // cap.SetCapability(MobileCapabilityType.App, "chrome");
                    cap.SetCapability("appPackage", "com.android.chrome");
                    cap.SetCapability("appActivity", "com.google.android.apps.chrome.Main");
                    cap.SetCapability("chromedriverExecutable", Config.Read("ANDROID_CHROME_DRIVER"));

                    Uri appiumAndroidUri = new Uri("http://127.0.0.1:4723/wd/hub");
                    _driver = new AndroidDriver<IWebElement>(appiumAndroidUri, cap, TimeSpan.FromSeconds(15));
                    webDriver = _driver;
                    break;
                case WebDriverType.IOS:
                    Uri uri2 = new Uri("");
                    webDriver = new RemoteWebDriver(uri2, new DesiredCapabilities());
                    break;

                default:
                    throw new Exception("The WebDriverType " + webDriverType.ToString() + " is not supported");

            }

            return webDriver;

        }
        public static string CaptureScreenShot(RemoteWebDriver driver, string screenshotName)
        {
            ITakesScreenshot takesScreenshot = (ITakesScreenshot)driver;
            Screenshot screenshot = takesScreenshot.GetScreenshot();

            string path = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string finalPath = path.Substring(0, path.LastIndexOf("bin")) + "ErrorReports\\" + screenshotName;
            string localPath = new Uri(finalPath).LocalPath;
            screenshot.SaveAsFile(localPath, ScreenshotImageFormat.Png);
            return localPath;

        }
        public static RemoteWebDriver GetWebDriverFromFeatureContext(ScenarioContext scenarioContext)
        {
            if (scenarioContext == null) throw new ArgumentNullException("scenarioContext");
            if (!scenarioContext.ContainsKey(DRIVER_KEY)) throw new Exception(DRIVER_KEY + " key not found");

            return (RemoteWebDriver)scenarioContext[DRIVER_KEY];
        }
        public static void LaunchBrowser(ScenarioContext scenarioContext)
        {
            if (scenarioContext == null) throw new ArgumentNullException("scenarioContext");

            if (scenarioContext.ContainsKey(DRIVER_KEY))
            {
                scenarioContext[DRIVER_KEY] = InitializeWebDriver();
            }
            else
            {
                scenarioContext.Add(DRIVER_KEY, InitializeWebDriver());
            }


        }
        public static void LaunchBrowserAtFeatureLevel(ScenarioContext scenarioContext)
        {
            if (scenarioContext == null) throw new ArgumentNullException("scenarioContext");

            if (!scenarioContext.ContainsKey(DRIVER_KEY))
            {
                scenarioContext.Add(DRIVER_KEY, InitializeWebDriver());
            }

        }
        public static void KillBrowser(ScenarioContext scenarioContext)
        {
            if (scenarioContext == null) throw new ArgumentNullException("featureContext");
            if (!scenarioContext.ContainsKey(DRIVER_KEY)) throw new Exception(DRIVER_KEY + " not found");

            QuitWebDriver((RemoteWebDriver)scenarioContext[DRIVER_KEY]);

            scenarioContext.Remove(DRIVER_KEY);
        }

    }
}

