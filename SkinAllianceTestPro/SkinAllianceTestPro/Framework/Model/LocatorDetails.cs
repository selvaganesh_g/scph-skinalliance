﻿using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkinAllianceTestPro.Framework.Common.EnumType;

namespace SkinAllianceTestPro.Framework.Model
{
    public class LocatorDetails
    {
        public string[] ElementIdentifier;
        public LocatorType LocatorType;
    }
}
