﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.PageBlocks

{

    public class PageBlock
    {
        #region "Public and Private Variables"

        private static PageBlock pageBlock;
        public static Dictionary<string, string> pageList = null;

        public static String SKIN_HOME = "/";
        private static String K_SKIN_HOME = "Skin Home";

        public static String DIRECTORY_PAGE = "/community/directory";
        private static String K_DIRECTORY_PAGE = "Directory";

        public static String ABOUT_US_PAGE = "/community/about-us";
        private static String K_ABOUT_US_PAGE = "About us";

        public static String CONTACT_US_PAGE = "/community/contact-us";
        private static String K_CONTACT_US_PAGE = "Contact us";

        #endregion

        private PageBlock()
        {

        }

        public static PageBlock getInstance()
        {
            if (pageBlock == null)
            {
                pageBlock = new PageBlock();
            }
            return pageBlock;
        }
        private static void addPages()
        {
            pageList = new Dictionary<string, string>();

            pageList.Add(K_SKIN_HOME, SKIN_HOME);
            pageList.Add(K_DIRECTORY_PAGE, DIRECTORY_PAGE);
            pageList.Add(K_ABOUT_US_PAGE, ABOUT_US_PAGE);
            pageList.Add(K_CONTACT_US_PAGE, CONTACT_US_PAGE);

        }
        public static string GetPage(string key)
        {
            if (pageList == null || pageList.Keys.Count < 1)
            {
                addPages();
            }

            try
            {
                return pageList[key];
            }
            catch (Exception e)
            {
                throw new Exception(key + " not found the Element list", e);

            }

        }

    }
}
