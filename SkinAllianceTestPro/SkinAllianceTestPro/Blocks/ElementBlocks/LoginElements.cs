﻿using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
    public class LoginElements

    {
        public static string BLOCK_NAME = "LOGIN";

        private static Dictionary<string, string> elementList;

        // public static string LOGIN_HEADING = ".//div[@id='content']/div/div[3]/div[2]/h1[@class='pull-left']~xpath";

        public static string LOGIN_HEADING = "pull-left~classname";

        public static string WELCOME_MESSAGE = "Signin_welcome_msg~id";

        public static string USERNAME_BOX = "login_email~id";

        public static string PWD_BOX = "login_password~id";

        public static string LOGIN_BUTTON = "login_submit~id";

        public static string REMEMBER_ME_CHECKBOX = "fa-check~classname";

        public static string SKIN_LOGO = "Logo~classname";

        public static string FIELD_VALIDATION_MESSAGE = "field-validation-error~classname";

        public static string EMAIL_EMPTY_MESSAGE = "login_email-error~id";

        public static string PASSWORD_VALIDATION_MESSAGE = "error-message~classname";

        public static string PASSWORD_EMPTY_MESSAGE = "login_password-error~id";

        public static string COUNTRY_FLAG = "Country_icon~id";

        public static string FORGOT_PASSWORD_LINK = "Forgot password?~linktext";

        public static string EMAIL_TEXTFIELD_FORGOT_PWD = "email~id";

        public static string SEND_BUTTON_FORGOT_PWD = "send-btn~id";

        public static string FORGOT_PWD_SUCCESS_MSG = "success-message~classname";

        public static string ERROR_MESSAGE_FORGOT_PWD = "email-error~id";

        public static string BACK_TO_HOMEPAGE_LINK = "BACK TO LOGIN~linktext";

        public static string SIGNUP_LINK = "Register Now~linktext";

       




        public static string K_SIGNUP_LINK = "REGISTER ACCOUNT LINK";

        public static string WELCOME_MESSAGE_POPUP = "modal-content~classname";

        public static string WELCOME_MESSAGE_CHECKBOX = "fa-check~classname";

        public static string WELCOME_MESSAGE_CLOSE_BUTTON = "message_close-btn~id";

        public static string TWITTER_SIGNIN = "twiiter_signin~id";

        public static string FACEBOOK_SIGNIN = "facebook_signin~id";

        public static string LINKEDIN_SIGNIN = "linkedIn_signin~id";

        public static string FACEBOOK_LOGO = "fb_logo~classname";

        public static string FACEBOOK_EMAIL_FIELD = "email~id";

        public static string FACEBOOK_PASSWORD_FIELD = "pass~id";

        public static string FACEBOOK_LOGIN_BUTTON = "loginbutton~id";

        public static string FACEBOOK_CONTINUE_BUTTON = "__CONFIRM__~name";

        public static string TWITTER_EMAIL_FIELD = "username_or_email~id";

        public static string TWITTER_PWD_FIELD = "password~id";

        public static string TWITTER_ALLOW = "allow~id";

        public static string LANGUAGE_DROPDOWN = "Language~classname";


        public static string POPUP_CLOSE_BUTTON = "close~classname";

        public static string CHECKBOX_POPUP = "Accepted~id";

        public static string FOOTER_POPUP = "redirectConfirmModal~id";

        #region "Public Methods"
        public static Dictionary<string, string> GetElementList()
        {
            if (elementList == null || elementList.Keys.Count < 1)
            {
                addElements();
            }

            return elementList;
        }

        public static string GetName(string key)
        {
            return ElementUtil.GetName(BLOCK_NAME, GetElementList(), key);

        }

#endregion

        #region "Private Methods"
        private static void addElements()
        {
            elementList = new Dictionary<string, string>();
            elementList.Add(K_SIGNUP_LINK, SIGNUP_LINK);
            //elementList.Add(K_MIXA_LOGO, MIXA_LOGO);
            //elementList.Add(K_HAMBUERGER_MENU, M_HAMBUERGER_MENU);
            //elementList.Add(K_PRODUCT_SELECTION, PRODUCT_SELECTION);
            //elementList.Add(K_SKIN_LOGO, SKIN_LOGO);
        }

        #endregion


    }
}
