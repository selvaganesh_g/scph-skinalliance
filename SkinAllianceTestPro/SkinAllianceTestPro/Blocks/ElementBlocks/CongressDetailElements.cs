﻿using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Framework.Common.EnumType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
    public class CongressDetailElements
    {

        #region "Public and Private Variables"


        public static string CONGRESSES_PAGE_TITLE = "//div[@class='container']/div/div/h1~xpath";

        public static string CONGRESSES_PAGE_IMAGE = "banner-title-image~classname";

        public static string CONGRESSES_CATEGORY_NAME = "//div[@id='header']/div/div[3]/div/div/div[1]/h5~xpath";

        public static string CONGRESSES_CATEGORY_DESCRIPTION = "congress-detail-info~classname";

        public static string CONGRESSES_REGISTER_BUTTON = Util.FireFox_Chrome("Register~linktext", "REGISTER~linktext");

        public static string CONGRESSES_DATE = "congress-schedule~classname";

        public static string OTHER_CONGRESSES_SECTION = "other-congresses~classname";

        public static string OTHER_CONGRESSES_DATE = "congress-schedule-big~classname";

        public static string OTHER_CONGRESSES_DESCRIPTION = "congress-info~classname";




        #endregion


    }
}
