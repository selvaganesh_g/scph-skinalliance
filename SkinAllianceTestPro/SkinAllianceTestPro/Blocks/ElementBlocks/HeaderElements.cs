﻿using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Framework.Common.EnumType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
    public class HeaderElements
    {

        #region "Public and Private Variables"

        public static string BLOCK_NAME = "HEADER";

        private static Dictionary<string, string> elementList;
       
        

        public static string M_HAMBUERGER_MENU = ".//button[@class='navbar-toggle collapsed']~xpath";
        private static string K_HAMBUERGER_MENU = "Mixa logo";

        public static string MIXA_MENU = "//div[@id='site-menu']/div/div/div/ul/li/a[contains(@href,'/products')]~xpath";
        private static string K_MIXA_MENU = "Mixa Menu";

        public static string PRODUCT_SELECTION = ".//div[@class='row']/span/div/div/div[@class='product-name' and contains (text(), '{0}')]/div[contains(text(), '{1}')]/../following-sibling::div/a~xpath";
        private static string K_PRODUCT_SELECTION = "Top Link";

        public static string SKIN_LOGO = "Logo~classname";
        private static string K_SKIN_LOGO = "SKIN ALLIANCE LOGO";

        public static string LOGO_PARENT = "row~class";
        public static string HEADER_SECTION = "header~id";
        public static string NAV_MENU = "SA-navbar-collapse~id";
        public static string NAV_MENUS = "dropdown~classname";
        public static string MENU_OPTIONS = "popover-content~classname";
        public static string MENU_HOME = Util.FireFox_Chrome("Home~linktext", "HOME~linktext");
        public static string MENU_KNOWLEDGE = Util.FireFox_Chrome("Dermocosmetics Knowledge~linktext", "DERMOCOSMETICS KNOWLEDGE~linktext");
        public static string MENU_EXPERT_COMMENTS = Util.FireFox_Chrome("Experts Comments~linktext", "EXPERTS COMMENTS~linktext");
        public static string MENU_WEBCASTS = Util.FireFox_Chrome("Webcasts~linktext","WEBCASTS~linktext");
        public static string MENU_CONGRESSES = Util.FireFox_Chrome("Congresses~linktext","CONGRESSES~linktext");
        public static string MENU_CLINICAL = Util.FireFox_Chrome("Clinical Cases~linktext", "CLINICAL CASES~linktext");
        public static string MENU_COMMUNITY = Util.FireFox_Chrome("Community~linktext", "COMMUNITY~linktext");
        public static string MENU_CREATE_POST = Util.FireFox_Chrome("Create Post~linktext", "CREATE POST~linktext");
        public static string DIRECTORY_LINK = Util.FireFox_Chrome("Directory~linktext", "DIRECTORY~linktext");
        public static string CONTACTUS_LINK = Util.FireFox_Chrome("Contact Us~linktext", "CONTACT US~linktext");
        public static string SCIENTIFIC_BOARD_LINK = Util.FireFox_Chrome("Scientific Board~linktext", "SCIENTIFIC BOARD~linktext");


        public static string AESTHETICS_SUBMENU = Util.FireFox_Chrome("Aesthetics~linktext", "AESTHETICS~linktext");


        public static string DROPDOWN_MENUS = "//li[@class='dropdown']/ul~xpath";

        public static string COMMUNITY_MENU = "id('SA-navbar-collapse')/ul/li[7]~xpath";

        public static string ABOUT_US_LINK = "ABOUT US~linktext";

        public static string USER_PICTURE_IMAGE = "userpic~classname";

        public static string USER_COUNTRY_ICON = "Language~classname";

        public static string USER_PICTURE_DROPDOWN = "dropdown-toggle~classname";

        public static string SKIN_BOX_DROPDOWN = "skinbox_dropdown~id";

        public static string PROFILE_DROPDOWN = Util.FireFox_Chrome("My Profile~linktext", "MY PROFILE~linktext");

        //public static string PROFILE_DROPDOWN = "My Profile~linktext";


        public static string NOTIFY_DROPDOWN = Util.FireFox_Chrome("My Notification~linktext", "MY NOTIFICATION~linktext");

        //public static string NOTIFY_DROPDOWN = "My Notification~linktext";

        public static string CREATE_POST_DROPDOWN = Util.FireFox_Chrome("Create Post~linktext", "CREATE POST~linktext");

        //public static string CREATE_POST_DROPDOWN = "Create Post~linktext";

        public static string LOGOUT_DROPDOWN = Util.FireFox_Chrome("Logout~linktext", "LOGOUT~linktext");

        //public static string LOGOUT_DROPDOWN = "Logout~linktext";

        public static string WEBCLIPPER_DROPDOWN = Util.FireFox_Chrome("Get Webclipper~linktext", "GET WEBCLIPPER~linktext");

        //public static string WEBCLIPPER_DROPDOWN = "Get Webclipper~linktext";

        public static string SEARCH_TEXT = "q~id";

        public static string SEARCH_MAGNIFIER_ICON = "fa-search~classname";

        public static string PAGE_HEADING = "Inner-Title~classname";



        //Image Carousel section//////////////

        public static string IMAGE_CAROUSEL_SECTION = "SAbanner~classname";

        public static string ARTICLE_IMAGE = "thumbnail~classname";

        public static string ARTICLE_TITLE_IMAGE = ".//div[@class='caption']/h1~xpath";

        public static string ARTICLE_DESCRIPTION_IMAGE = ".//div[@class='caption']/p~xpath";

        public static string READ_MORE_BUTTON = Util.FireFox_Chrome("Read More~linktext", "READ MORE~linktext");

        public static string LEFT_ARROW_CAROUSEL = "fa-angle-left~classname";

        public static string RIGHT_ARROW_CAROUSEL = "fa-angle-right~classname";

        public static string POST_SECTION = "SAPost~classname";

        public static string FAVOURITE_SECTION = "fav_post~id";

        public static string LATEST_POST_SECTION = "LatestPost~classname";

        public static string NEWS_SECTION = "news_post~id";

        public static string POST_DESCRIPTION = "pwrite~classname";

        public static string LATEST_POST_ARROWS = "MS-controls~classname";

        public static string POST_FULL_SECTION = "container~classname";

        public static string POST_IMAGE = "PImg~classname";

        public static string POST_LIKE = "pStar~classname";

        public static string POST_COMMENTS = "pComment~classname";

        public static string POST_VIEW_ALL_LINK = "viewall~classname";

        public static string CATEGORY_SLIDER_SECTION = "MS-content~classname";

        public static string CATEGORY_SLIDER_TITLE = "postImg~classname";



        public static string BOARD_SCIENTIFIC_SECTION = "AllianceMember~classname";

        public static string BOARD_SCIENTIFIC_IMAGE = "img-responsive~classname";

        public static string BOARD_SCIENTIFIC_DESCRIPTION = ".//div[@class='AllianceMember']/div/div/div/p~xpath";

        public static string BOARD_READ_MORE_BUTTON = "readmore~classname";


        // Create a Post page/////////

        public static string CREATE_PAGE_HEADING = "Inner-Title~classname";

        public static string ENTER_POST_TITLE = "crptitle~id";

        public static string ADD_MEDIA_BUTTON = "custom-file-upload-btn~classname";

        public static string ENTER_CONTENT_FIELD = "Editor-editor~classname";

        public static string ENTER_URL_FIELD = "crpWebsiteUrl~id";

        public static string BROWSE_BUTTON = "browse-btn~id";

        public static string UPLOAD_BUTTON = "file_1~id";

        public static string QUESTION_HEADING = "QuestionnaireSection~classname";

        public static string ADD_QUESTION_LINK = "questionnarieToggle~id";

        public static string PREVIEW_BUTTON = "createPostPreview~id";

        public static string ADDED_QUESTION_SECTION = "saved-questions~classname";

        public static string FIRST_ANSWER_CHECKBOX = "//div[@id='add-question']/div[2]/ul/li[1]/div[2]/div/label/span~xpath";

        public static string SECOND_ANSWER_CHECKBOX = "chkOptionB~id";

        public static string THIRD_ANSWER_CHECKBOX = "chkOptionC~id";

        public static string FOURTH_ANSWER_CHECKBOX = "chkOptionD~id";

        public static string RESPONSE_PERCENTAGE_CHECKBOX = "//div[@id='add-question']/div[4]/div/label~xpath";

        public static string QUESTION_SAVE_BUTTON = "saveQuestion~id";

        public static string CREATE_POST_BUTTON = "createPostPublish~id";

        public static string ADD_IMAGE_SECTION = "input-file-now~id";

        //public static string CATEGORY_CHECKBOX = "fa-check~classname";

        public static string CATEGORY_CHECKBOX = "fa-check~classname";


        public static string CATEGORY_CHECKBOX_SECTION = "post-sub-category~classname";

        public static string ARTICLE_CREATED_MSG = "modal-body~classname";

        public static string PUBLISH_OK_BUTTON = "publishOk~id";


        public static string COPYRIGHT_CONFIRMATION_MSG = "copyright-msg~id";

        public static string ENTER_QUESTION_FIELD = "question~id";

        public static string FIRST_ANSWER_FIELD = "optionA~id";

        public static string PREVIEW_POST_OVERLAY = "preview_section~id";

        public static string SECOND_ANSWER_FIELD = "optionB~id";

        public static string THIRD_ANSWER_FIELD = "optionC~id";

        public static string FOURTH_ANSWER_FIELD = "optionD~id";

        
        public static string RESPONSE_TEXT_FIELD = "response~id";


        public static string QUESTION_RECENTLY_ADDED = "recently-added~id";


        //Upcoming Congresses Section/////////////////////////////////

        public static string UPCOMING_CONGRESSES_SECTION = "upcoming-congress~classname";

        public static string CONGRESSES_CATEGORIES = "col-lg-3";

        public static string CONGRESSES_POST = "upc-post~classname";

        public static string CONGRESSES_VIEW_ALL_LINK = Util.FireFox_Chrome("View ALL >~linktext", "View ALL >~linktext");

        //public static string CONGRESSES_VIEW_ALL_LINK = "fa-angle-double-right~classname";


        #endregion

        #region "Public Methods"

        public static Dictionary<string, string> GetElementList()
        {
            if (elementList == null || elementList.Keys.Count < 1)
            {
                addElements();
            }

            return elementList;
        }

        public static string GetName(string key)
        {
            return ElementUtil.GetName(BLOCK_NAME, GetElementList(), key);

        }

        #endregion

        #region "Private Methods"
        private static void addElements()
        {
            elementList = new Dictionary<string, string>();

            
            elementList.Add(K_HAMBUERGER_MENU, M_HAMBUERGER_MENU);
            elementList.Add(K_MIXA_MENU, MIXA_MENU);
            elementList.Add(K_PRODUCT_SELECTION, PRODUCT_SELECTION);
            elementList.Add(K_SKIN_LOGO, SKIN_LOGO);
        }

        #endregion


    }
}
