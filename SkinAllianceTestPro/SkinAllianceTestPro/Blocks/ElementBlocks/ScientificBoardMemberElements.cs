﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
    public class ScientificBoardMemberElements
    {
        #region "Public and Private Variables"

        public static string PAGE_TITLE = "Inner-Title~classname";

        public static string BOARD_MEMBERS_LIST = "scientific-board-members~classname";

        public static string BOARD_MEMBERS_DESC = "board-member-desc~classname";

        public static string BOARD_MEMBERS_NAME = "board-member-name~classname";

        public static string BOARD_MEMBERS_POSITION = "board-member-position~classname";

        public static string BOARD_MEMBERS_IMAGE = "board-member-profile-pic~classname";

        public static string PAGE_DESCRIPTION = "scientific-board-desc~classname";



        #endregion
    }
}
