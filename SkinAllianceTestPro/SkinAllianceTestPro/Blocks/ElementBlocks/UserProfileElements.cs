﻿using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
   public class UserProfileElements
    {
        public static string PAGE_HEADING = "Inner-Title~classname";

        public static string PROFILE_USER_NAME = "//div[@class='dir-userinfo']/ul/h1~xpath";

        public static string PROFILE_USER_PICTURE = "dir-usr-pic~classname";

        public static string PROFILE_USER_PHONE_NO = "fa-phone~classname";

        public static string PROFILE_USER_EMAIL_ID = "user-email~classname";

        public static string USER_FOLLOW_BUTTON = "btn-primary~classname";

        public static string PROFILE_SUMMARY_SECTION = "summary~classname";

        public static string PROFILE_FOLLOWING_SECTION = "Following~classname";

        public static string FOLLOWING_USERS_LIST = "Following~id";

        public static string PROFILE_FOLLOWED_BY_SECTION = "follwed-by~classname";

        public static string FOLLOWED_BY_USERS_LIST = "FollowBy~id";

        public static string PROFILE_CATEGORIES_FOLLOWED = "catefollow-list~classname";

        public static string PROFILE_CATEGORIES_LIST = "CategoryFollowed~id";

        public static string PROFILE_SUB_CATEGORIES_LIST = "DirSubcateFollowed~id";

        public static string USER_PICTURE_FOLLOWED_BY = "follwedby-pic~classname";

        public static string USER_PICTURE_FOLLOWING = "Following-pic~classname";

        public static string BREADCRUMB_PREV_LINK = "field-navigationtitle~classname";






    }
}
