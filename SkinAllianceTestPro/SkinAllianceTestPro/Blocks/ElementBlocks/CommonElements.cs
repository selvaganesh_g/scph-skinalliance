﻿using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
    public class CommonElements
    {
        #region "Public Variables"

        public static string BLOCK_NAME = "COMMON";

        #endregion

        #region "Private Variables"

        private static Dictionary<string, string> elementList;

        #endregion

        #region "Public Methods"

        public static  Dictionary<string,string> GetElementList()
        {
            if (elementList == null || elementList.Keys.Count < 1)
            {
                addElements();
            }

            return elementList;
        } 

        public static string GetName(string key)
        {
            return ElementUtil.GetName(BLOCK_NAME, GetElementList(), key);

        }

        #endregion

        #region "Private Methods"

        private static void addElements()
        {
            elementList = new Dictionary<string, string>();

        }

        #endregion

    }
}
