﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
   public class SignUpElements
    {
        #region "Public and Private Variables"

        public static string BLOCK_NAME = "SIGN UP";

        private static Dictionary<string, string> elementList;

        public static string REGISTER_ACCOUNT_SECTION = "Register ~id";

        public static string SKIN_ALLIANCE_HEADING = "Signin_welcome_msg~id";

        public static string NEW_USER_HEADING = "//div[@class='hidden-xs']/h1~xpath";

        //public static string NEW_USER_HEADING = "pull-left~classname";

        public static string FIRST_NAME_FIELD = "signup_firstname~id";

        public static string LAST_NAME_FIELD = "signup_lastname~id";

        public static string EMAIL_FIELD = "signup_email~id";

        public static string ADDRESS_FIELD = "new_address~id";

        public static string COUNTRY_FIELD = "signup_country~id";

        public static string PHONE_NO_FIELD = "new_user_no~id";

        public static string PHYSICAL_LICENSE_FIELD = "signup_physiciallicensenumber~id";

        public static string MEDICAL_SPECIALITY_FIELD = "signup_medicalspecialty~id";

//      public static string FACEBOOK_MEDICAL_SPECIALITY_FIELD = "sel1~id";

//      public static string TWITTER_SPECIALITY_FIELD = "sel1~id";

        public static string USERNAME_FIELD = "new_user_name~id";

        public static string PASSWORD_FIELD = "signup_password~id";

        public static string PASSWORD_CONFIRMATION_FIELD = "signup_confirmpassword~id";

        public static string SUBMIT_BUTTON = "reg-form-submit~classname";

        public static string REGISTRATION_CONFIRMATION = "ThanksReg~classname";

        public static string REGISTRATION_CONFIRMATION_SECTION = "mtop~classname";

        public static string REGISTRATION_SUCCESS_MSG = "reg-title~classname";

        public static string SIGNUP_HEADER = "header_section~id";

        public static string SOCIAL_FACEBOOK_LINK = "Register with Facebook~linktext";

        public static string FACEBOOK_POPUP = "facebook-popup~id";

        public static string SOCIAL_TWITTER_LINK = "Register with Twitter~linktext";

        public static string SOCIAL_LINKEDIN_LINK = "Register with LinkedIn~linktext";

        public static string LINKEDIN_EMAIL_FIELD = "session_key-oauth2SAuthorizeForm~id";

        public static string LINKEDIN_PWD_FIELD = "session_password-oauth2SAuthorizeForm~id";

        public static string LINKEDIN_LOGIN_BUTTON = "allow~classname";

        public static string FIRST_NAME_ERROR_MESSAGE = "signup_firstname-error~id";

        public static string LAST_NAME_ERROR_MESSAGE = "signup_lastname-error~id";

        public static string COUNTRY_FIELD_ERROR_MESSAGE = "signup_country-error~id";

        public static string PHYSICIAN_FIELD_ERROR_MESSAGE = "signup_physiciallicensenumber-error~id";

        public static string SPECIALTY_ERROR_MESSAGE = "signup_medicalspecialty-error~id";

        public static string EMAIL_FIELD_ERROR_MESSAGE = "signup_email-error~id";

        public static string INVALID_PWD_ERROR_MESSAGE = "field-validation-error~classname";

        public static string PWD_ERROR_MESSAGE = "signup_password-error~id";

        public static string CONFIRM_PWD_ERROR_MESSAGE = "signup_confirmpassword-error~id";

        public static string PREFERED_LANUGAGE_DROPDOWN = "signup_preferredlanguage~id";

        public static string COMPLETE_PROFILE_BUTTON = "completeProfile~id";




        public static string BACK_TO_LOGIN_BUTTON = "BACK TO LOGIN~linktext";

        public static string ERROR_MESSAGE = "error~classname";

        #endregion
    }
}
