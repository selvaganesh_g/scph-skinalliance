﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
   public class SubCategoryElements
    {
        public static string BLOCK_NAME = "SUB_CATEGORY";

        private static Dictionary<string, string> elementList;

        public static string READ_MORE_BUTTON = "btn-more~classname";

        public static string FULL_ARTICLE_CONTENT = "col-md-4 folder_post~classname";
                    
    }
}
