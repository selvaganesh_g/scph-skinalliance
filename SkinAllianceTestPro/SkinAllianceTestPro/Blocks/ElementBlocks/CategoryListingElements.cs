﻿using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
    public class CategoryListingElements
    {
        private static Dictionary<string, string> elementList;

        public static string PAGE_TITLE = "Inner-Title~classname";

        public static string BREADCRUMB_TRAIL = "//*[@id='content']/div/div[1]/ol~xpath";

        public static string PREV_BREADCRUMB = "Directory~linktext";

        public static string CATEGORY_LIST_SECTION = "DermoCosmetic~classname";

        public static string CATEGORY_LIST_NAME = "category-title~classname";

        public static string SUB_CATEGORY_NAMES = "subcategory~classname";

        public static string CATEGORY_FOLLOW_BUTTON = "follow~classname";

        public static string TOPICAL_CATEGORY = Util.FireFox_Chrome("TOPICALS~linktext" , "Topicals~linktext");

    }
}
