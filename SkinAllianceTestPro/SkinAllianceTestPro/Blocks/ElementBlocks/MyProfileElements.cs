﻿using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
   public class MyProfileElements
    {
        private static Dictionary<string, string> elementList;

        public static string MY_PROFILE_PAGE_HEADING = "//*[@id='header']/div/div[3]/h1~xpath";

        public static string PROFILE_PICTURE = "usr-pic~classname";

        public static string USER_INFO = "//div[@class='userinfo']/ul/h1~xpath";

        public static string USER_DESIGNATION = "user_designation~id";

        public static string USER_COUNTRY = "usr-region~classname";

        public static string USER_CONTACT = "fa-phone~classname";

        public static string USER_EMAIL_ID = "fa-envelope~classname";

        public static string USER_SUMMARY_SECTION = "summary~classname";

        public static string USER_FOLLOWING_SECTION = "Following~classname";

        public static string USER_NAME_CONTENT_SECTION = "Following-pic~classname";

        public static string USER_NAME_FOLLOWING_LINK = "Following-overlay~classname";

        public static string USER_IMAGE_FOLLOWING_LINK = "follwedby-pic~classname";

        public static string USER_FOLLOWED_BY_SECTION = "follwed-by~classname";

        public static string USER_NAME_FOLLOWED_BY = "follwedby-text~classname";

        public static string USER_IMAGE_FOLLOWED_BY = "follwedby-image~classname";

        public static string CATEGORIES_SECTION = "category-followed~classname";

        public static string SUBCATEGORIES_SECTION = "subcategory-followed~classname";

        public static string CATEGORIES_NAMES = "catefollow-list~classname";

        public static string SUBCATEGORIES_NAMES = "subcatefollow-list~classname";

        public static string EDIT_BUTTON = Util.FireFox_Chrome("Edit Profile~linktext", "EDIT PROFILE~linktext");

        public static string EDIT_PROFILE_POPUP = "editProfileForm~id";

        public static string EDIT_PROFILE_CLOSE_BUTTON = "close~classname";

        //////////////////// Edit Profile //////////////////////////////////

        public static string EDIT_USER_IMAGE = "input-file-now-custom-1~id";

        public static string UPLOAD_PHOTO_BUTTON = "preview~id";

        public static string EDIT_TITLE = "profile_title~id";

        public static string EDIT_FIRST_NAME = "profile_firstname~id";

        public static string EDIT_LAST_NAME = "profile_lastname~id";

        public static string EDIT_ACTIVITY = "profile_activity~id";

        public static string EDIT_ROLE = "user_role_edit~id";

        public static string EDIT_ADDRESS = "profile_address~id";

        public static string EDIT_ZIP_CODE = "profile_zipcode~id";

        public static string EDIT_CITY = "profile_city~id";

        public static string EDIT_COUNTRY = "profile_country~id";

        public static string EDIT_LANGUAGE = "profile_preferredlanguage~id";

        public static string EDIT_PHYSICAL_NO = "profile_physiciallicensenumber~id";

        public static string EDIT_MEDICAL_SPECIALITY = "profile_medicalspecialty~id";

        public static string EDIT_PHONE = "profile_phonenumber~id";

        public static string EDIT_EMAIL = "user_email_edit~id";

        public static string EDIT_SUMMARY = "summary~classname";

        public static string EDIT_CLOSE_BUTTON = "close~classname";

        public static string SUBMIT_BUTTON_POPUP = "reg-form-submit~classname";

        public static string EDIT_RESUME = "image-preview-filename~classname";

        public static string UPLOAD_RESUME_BUTTON = "resume~id";

        public static string CONTACT_ME_EMAIL_CHECKBOX = "PreferredService1~id";

        public static string PROFILE_PUBLIC_CHECKBOX = "PreferredService2~id";

        public static string EDIT_PROFILE_CHECKBOXES = "btn-default~classname";


        /////////////////////// Change Password //////////////////////

        public static string CHANGE_PASSWORD_BUTTON = Util.FireFox_Chrome("Change Password~linktext", "CHANGE PASSWORD~linktext");

        public static string CHANGE_PASSWORD_POPUP = "change-password-modal~classname";

        public static string CURRENT_PASSWORD_FIELD = "old_password~id";

        public static string NEW_PASSWORD_FIELD = "new_password~id";

        public static string CONFIRM_PASSWORD_FIELD = "confirm_password~id";

        public static string CHANGE_PWD_SUBMIT_BUTTON = "changePassword~id";

        public static string CHANGE_PASSWORD_ERROR_MSG = "invalidOldPwd~id";

        public static string CURRENT_PASSWORD_EMPTY_MSG = "old_password-error~id";

        public static string NEW_PWD_EMPTY_MSG = "new_password-error~id";

        public static string CONFIRM_PWD_ERROR_MSG = "confirm_password-error~id";

        //public static string NEW_PASSWORD_ERROR_MSG = "new_error_msg~id";
    }
}
