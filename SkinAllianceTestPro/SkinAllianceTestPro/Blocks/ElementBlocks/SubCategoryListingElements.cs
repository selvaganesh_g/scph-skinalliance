﻿using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
   public class SubCategoryListingElements
    {
        private static Dictionary<string, string> elementList;

        public static string SUB_CATEGORY_PAGE_TITLE = "Inner-Title~classname";

        public static string SUBCATEGORY_PAGE_TITLE = "col-lg-8 col-sm-8 col-sm-push-4 inpage-banner-content~classname";

        public static string SUB_CATEGORY_SECTION = "subcategory-listing~classname";

        public static string CATEGORY_TITLE = "sub-title~classname";

        public static string SUB_CATEGORY_NAMES = "sub-categoryPost~classname";

        public static string SUB_CATEGORY_CONTENT = "subpwrite~classname";

        public static string SUB_CATEGORY_IMAGE = "subPImg~classname";

        public static string VIEW_ALL_LINK = "//*[@id='content']/div/div/div/div[5]/div/div[1]/p[1]/a/text()~xpath";

        public static string LIKE_ICON = "fa fa-star~classname";

        public static string COMMENT_ICON = "subpComment~classname";

        public static string LIKE_ICON_COUNT = "like_count~classname";

        public static string COMMENT_ICON_COUNT = "comment_count~classname";

        public static string FOLLOW_BUTTON = "follow~classname";

        public static string SUB_CATEGORY_DATE = "subpDate~classname";

        public static string SUB_CATEGORY_AUTHOR = "author_name~classname";

        public static string BREADCRUMB_PREV_LINK = "field-navigationtitle~classname";

        public static string AGING_SUBCATEGORY = "Aging~linktext";

        public static string AGING_HEADING = "//*[@id='header']/div/div[3]/h1~xpath";
    }
}
