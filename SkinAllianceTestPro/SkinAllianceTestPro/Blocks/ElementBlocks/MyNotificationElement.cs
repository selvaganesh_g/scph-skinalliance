﻿using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Block.ElementBlocks
{
    public class MyNotificationElement
    {
        #region "Public and Private Variables"

        private static Dictionary<string, string> elementList;

        public static string MY_NOTIFICATION_PAGE_HEADING = "//*[@id='header']/div/div[3]/h1~xpath";

        public static string FILTER_SECTION = "//*[@id='content']/div/div[2]/div[1]/div[1]/div[4]/h5~xpath";
                                               
        public static string TIMELINENOTIFICATION_SECTION = "notificationcontainer~id";

        public static string MANAGE_NOTIFICATION_SECTION = "manage-notification~classname";
        
        public static string MESSAGE_SECTION = "//*[@id='notificationcontainer']/ul[1]~xpath";

        public static string MANAGE_BUTTON = "manage-notification~classname";

        public static string LIKED_SECTION = "//*[@id='content']/div/div[1]/div[1]/div[1]/div[4]/div/div[1]~xpath";
        
        public static string COMMENT_SECTION = "//*[@id='content']/div/div[1]/div[1]/div[1]/div[4]/div/div[2]~xpath";

        public static string NEWPOST_FC = "//*[@id='content']/div/div[1]/div[1]/div[1]/div[4]/div/div[3]~xpath";

        public static string NEWPOST_CONTACT = "//*[@id='content']/div/div[1]/div[1]/div[1]/div[4]/div/div[4]~xpath";

        public static string CONTACT_FDUSER = "//*[@id='content']/div/div[1]/div[1]/div[1]/div[4]/div/div[5]~xpath";

        public static string NEWUSERFOLLOW_MANAGE = "//*[@id='notificationform']/div/div[1]/span~xpath";

        public static string FCATEGORY_MANAGE = "//*[@id='notificationform']/div/div[2]/span~xpath";

        public static string INTERACT_MANAGE = "//*[@id='notificationform']/div/div[3]/span~xpath";

        public static string COMMENT_MANAGE = "//*[@id='notificationform']/div/div[4]/span~xpath";

        public static string CONTACT_MANAGE = "//*[@id='notificationform']/div/div[5]/span~xpath";

        public static string MANAGE_POPUP = "popup-title~classname";

        public static string CLOSE_BUTTON = "//*[@id='manage-notification']/div/div/div/button~xpath";

        public static string RANDOM_DROPDOWN = "//*[@id='NotificationFour'~xpath]";

        public static string DROPDOWN_ONE = "NotificationOne~id";

        public static string DROPDOWN_TWO = "NotificationTwo~id";

        public static string DROPDOWN_THREE = "NotificationThree~id";

        public static string DROPDOWN_FOUR = "NotificationFour~id";

        public static string DROPDOWN_FIVE = "NotificationFive~id";

        public static string SAVE_BUTTON = "//*[@id='notificationform']/div/div[6]/input~xpath";

        public static string LIKED_FILTER = "";

        public static string COMMENT_FILTER = "";

        public static string FC_FILTER = "";

        public static string POST_FILTER = "";

        public static string CONTACT_FILTER = "";

        #endregion
    }
}
