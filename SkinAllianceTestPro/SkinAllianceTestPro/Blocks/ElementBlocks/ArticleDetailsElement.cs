﻿using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Block.ElementBlocks
{
    public class ArticleDetailsElement
    {
        #region "Public and Private Variables"

        private static Dictionary<string, string> elementList;

        public static string CATEGORY_TITLE = "postcategory~classname";

        public static string ARTICLE_TITLE = "Adetails-page~classname";

        public static string ARTICLE_CONTENT = "article-details~classname";

        public static string ARTICLE_COMMENTS = "arti-post-cmnt~classname";

        public static string POSTED_BY_SECTION = "post_section~id";

        public static string POSTED_BY_DATE = "postdate~classname";

        public static string POSTED_BY_NAME = "postby~classname";
      
        public static string ARTICLE_COMMENTS_COUNT = "comments_count~id";

        public static string POSTED_REPLY = "reply-comment~classname";

        public static string ARTICLE_IMAGE = "col-lg-3~classname";

        public static string REPLY_LINK = "Reply~linktext";

        public static string REPLY_TEXT_BOX = "Message~id";

        public static string PAGE_BUTTONS = "btn btn-primary~classname";

        public static string REPLY_SUBMIT_BUTTON = "//*[@id='replyformbutton-6']~xpath";

        public static string ARTICLE_LIKE_ICON = "post_like~id";

        public static string ARTICLEDETAIL_LIKE_ICON = "//*[@id='content']/div/div[2]/div/div/div[1]/div[5]/form/button/text()~xpath";

        public static string ARTICLE_LIKE_COUNT = "likes_count~id";

        public static string REPORT_MODERATOR_LINK = "//*[@id='content']/div/div[2]/div/div/div[2]/div[4]/a/i~xpath";

        public static string REPORT_CONFIRMATION_MSG = "//*[@id='ModerteModal']/div/div/div/p~xpath";

        public static string CLOSE_BUTTON_MODERATOR = "//*[@id='ModerteModal']/div/div/div/button~xpath";

        public static string ACCESS_WEBSITE_LINK = Util.FireFox_Chrome("ACCESS WEBSITE~linktext" , "Access Website~linktext");

        public static string DOWNLOAD_BUTTON = "Download~linktext";

        public static string COPYRIGHT_POPUP = "copyright~classsname";

        public static string WEBCASTS_SECTION = "artdet-webcast~classname";

        public static string RELATED_POST_SECTION = "Related-Posts~classname";

        public static string RELATED_POST_IMAGE = "related-post-thumb~classname";

        public static string RELATED_POST_USERNAME = "username_rel~id";

        public static string RELATED_POST_LIKE = "fa-star~classname";

        public static string RELATED_POST_LIKE_COUNT = "Rel_like_count~id";

        public static string RELATED_POST_COMMENT = "fa-comments~classname";

        public static string RELATED_POST_COMMENT_COUNT = "Rel_comment_count~id";

        public static string ARTICLE_VIEW_MORE_BUTTON = "Rel_view-btn~id";

        public static string ARTICLE_EDIT_BUTTON = "edit-btn~id";

        public static string ARTICLE_DELETE_BUTTON = "delete-btn~id";

        public static string COMMENT_SEND_BUTTON = "//div[@class='pull-right']/form/button~xpath";

        public static string POST_COMMENT_BUTTON = "commentformbutton~id";

        public static string COMMENT_SECTION = "Article-Comments~classname";

        public static string ARTICLE_READ_MORE_BUTTON = "btn-more~id";

        public static string ARTICLE_COMMENTS_FIELD = "//*[@id='Message']~xpath";
        
        public static string COMMENTS_TOOLBAR = "comment_toolbar~id";

        public static string COMMENTS_IMAGE = "comments_img~id";

        public static string COMMENTS_USER_NAME = "//div[@class='arti-post-cmnt']/ul/li[1]~xpath";

        public static string PLAY_WEBCAST_LINK = "webcast-wrap~classname";

        public static string WEBCAST_VIDEO = "player~classname";

        public static string WEBCAST_VIDEO_PLAY = "play~classname";

        public static string WEBCAST_VIDEO_PAUSE_ICON = "pause-icon~classname";

        public static string WEBCAST_POPUP = "webcast_popup~id";

        public static string WEBCAST_POPUP_HEADING = "webcast_heading~id";

        public static string WEBCAST_SLIDES = "slides~classname";

        public static string WEBCAST_CLOSE_BUTTON = "close~classname";

        

        #endregion
    }
}
