﻿using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
    public class MixaElements
    {

        #region "Public and Private Variables"

        public static string BLOCK_NAME = "HEADER";

        private static Dictionary<string, string> elementList;
       
        //public static string D_MENU_OPTION = ".//button[@class='navbar-toggle']~xpath";
        public static string M_MENU = ".//button[@class='navbar-toggle']~xpath";
        private static string K_MENU = "Menu";

        public static string M_MENU_OPTION = ".//div[@id='site-menu']/div/div/div/ul/li/a[contains(@href,'products')]~xpath";
        private static string K_MENU_OPTION = "Menu Option";

        public static string M_PRODUCT_FIRST = ".//div[@class='products-listing-head content-wrapper']/div~xpath";
        private static string K_PRODUCT_FIRST = "First Product";

        public static string M_PRODUCT_EXPAND = ".//div[@class='products-listing-head content-wrapper']/div/div[1]/div[@class='collapsible__button']~xpath";
        private static string k_PRODUCT_EXPAND = "First Product";



        #endregion

        #region "Public Methods"

        public static Dictionary<string, string> GetElementList()
        {
            if (elementList == null || elementList.Keys.Count < 1)
            {
                addElements();
            }

            return elementList;
        }

        public static string GetName(string key)
        {
            return ElementUtil.GetName(BLOCK_NAME, GetElementList(), key);
            
        }

        #endregion

        #region "Private Methods"
        private static void addElements()
        {
            elementList = new Dictionary<string, string>();

            elementList.Add(K_MENU, M_MENU);
            elementList.Add(K_MENU_OPTION, M_MENU_OPTION);
            elementList.Add(K_PRODUCT_FIRST, M_PRODUCT_FIRST);
            elementList.Add(k_PRODUCT_EXPAND, M_PRODUCT_EXPAND);

        }

        #endregion


    }
}
