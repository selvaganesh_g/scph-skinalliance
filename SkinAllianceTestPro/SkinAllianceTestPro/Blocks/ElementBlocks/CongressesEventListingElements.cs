﻿using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Block.ElementBlocks
{
    public class CongressEventListingElements
    {

        public static string PAGE_HEADING = "Inner-Title~classname";

        public static string CONGRESS_IMAGE = "congress-list-image~classname";

        public static string CONGRESS_DATE = "congress-schedule~classname";

        public static string CONGRESS_DESC = "congress-list-info~classname";


    }
}