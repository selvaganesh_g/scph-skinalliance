﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkinAllianceTestPro.Framework.Common;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
   public class FooterElements
    {
        #region "Public and Private Variables"

        public static string BLOCK_NAME = "FOOTER";

        public static string FOOTER_SECTION = ".//div[@class='fmenu']/ul~xpath";

        public static string FOOTER_TERMS_CONDITIONS = Util.FireFox_Chrome("TERMS OF USE~linktext" , "Terms of Use~linktext");

        //public static string FOOTER_TERMS_CONDITIONS = "Terms of Use~linktext";

        public static string FOOTER_PRIVACY_POLICY = Util.FireFox_Chrome("COOKIE AND PRIVACY POLICY~linktext" , "Cookie and Privacy Policy~linktext");

        //public static string FOOTER_PRIVACY_POLICY = "Privacy Policy~linktext";

        public static string FOOTER_LEGAL = Util.FireFox_Chrome("LEGAL~linktext" , "Legal~linktext");
                                            
        //public static string FOOTER_LEGAL = "Legal~linktext";

        public static string FOOTER_CONTACT_US = "contactus_footer~id";

        //public static string PARTNER_LOGO_SECTION = ".//footer[@class='footer ']/div[@class='container']/div/div/div[@class='footerlogo']/ul/li~xpath";

        public static string PARTNER_LOGO_SECTION = "fLogo~classname";

        public static string TERMS_CONDITIONS_HEADING = "//*[@id='header']/div/div[3]/h1~xpath";

        public static string LEGAL_HEADING = "//*[@id='header']/div/div[3]/h1~xpath";

        public static string STATIC_PAGE_HEADING = "Inner-Title~classname";

        public static string PRIVACY_POLICY_HEADING = "//*[@id='header']/div/div[3]/h1~xpath";

        public static string IMAGE_ABOUT_US = "col-lg-3~classname";

        public static string CONTACT_FIRST_NAME = "contact_firstname~id";

        public static string CONTACT_LAST_NAME = "contact_lastname~id";

        public static string CONTACT_EMAIL_ADDRESS = "contact_email~id";

        public static string CONTACT_PHONE_NO = "contact_phonenumber~id";

        public static string CONTACT_MESSAGE = "contact_message~id";

        public static string PRIVACY_POLICY_CONTENT = "privacy~classname";

        public static string LEGAL_PAGE_CONTENT = "legal~classname";

        public static string TERMS_OF_USE_CONTENT = "termsofuse~classname";

        public static string CONTACT_SUBMIT_BUTTON = "contactus_submit~id";

        public static string ERROR_MESSAGE = "error~classname";

        public static string FIRST_NAME_FIELD_MSG = "contact_firstname-error~id";

        public static string LAST_NAME_FIELD_MSG = "contact_lastname-error~id";

        public static string EMAIL_FIELD_MSG = "contact_email-error~id";

        public static string MESSAGE_FIELD_MSG = "contact_message-error~id";

        public static string PHONENO_FIELD_MSG = "contact_phoneno-error~id";

        public static string SUCCESS_MESSAGE = "success-message~classname";

        public static string EMAIL_INFO = "mail~classname";

        public static string PHONE_NO_INFO = "phone~classname";

        public static string ADDRESS_INFO = "address~classname";

        public static string WEBSITE_INFO = "web~classname";



    }
}

#endregion

