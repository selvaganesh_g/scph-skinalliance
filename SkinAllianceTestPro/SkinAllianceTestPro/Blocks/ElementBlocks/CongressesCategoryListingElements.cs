﻿using SkinAllianceTestPro.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Block.ElementBlocks
{
    public class CongressesCategoryListingElements
    {
        #region "Public and Private Variables"

        private static Dictionary<string, string> elementList;

        public static string DERMATOLOGY_DROPDOWN = Util.FireFox_Chrome("Dermatology~linktext", "DERMATOLOGY~linktext");
             

        public static string ESTHETIC_DROPDOWN = Util.FireFox_Chrome("Esthetic~linktext", "ESTHETIC~linktext");

        public static string RESEARCH_DROPDOWN = Util.FireFox_Chrome("Research~linktext", "RESEARCH~linktext");

        public static string CONGRESSES_CATEGORY_TITLE = "Inner-Title~classname";

        public static string ESTHETIC_CATEGORY = "//*[@id='content']/div/div[2]/div[2]/div/div[1]/h1~xpath";

        public static string DERMATOLOGY_CATEGORY = "//*[@id='content']/div/div[2]/div[1]/div/div[1]/h1~xpath";

        public static string RESEARCH_CATEGORY = "//*[@id='content']/div/div[2]/div[3]/div/div[1]/h1~xpath";

        public static string CONGRESSES_DATE = "cong-ddmm~classname";

        public static string CONGRESSES_DESC = "cong-post~classname";

        public static string CONGRESSES_VIEW_ALL_LINK = Util.FireFox_Chrome("View ALL >~linktext", "View ALL >~linktext");

        //public static string CONGRESSES_VIEW_ALL_LINK = "View ALL >~linktext";


        #endregion
    }
}