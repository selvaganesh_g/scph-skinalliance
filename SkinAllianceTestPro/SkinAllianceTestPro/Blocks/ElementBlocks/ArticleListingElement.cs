﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
   public class ArticleListingElement
    {
        #region "Public and Private Variables"

        //public static string BLOCK_NAME = "HEADER";

        private static Dictionary<string, string> elementList;

        public static string ARTICLE_LISTING_SECTION = "article-listing~classname";

        public static string ARTICLES_LIST = "articlelistcontainer~id";

        public static string PAGE_HEADING = "Inner-Title~classname";

        public static string ARTICLE_IMAGE = "img-responsive~classname";

        public static string ARTICLE_CONTENT = "content_article~id";

        public static string ARTICLE_COMMENT_ICON = "fa-comment~classname";

        public static string ARTICLE_STAR_ICON = "fa-star~classname";

        public static string DATE_AUTHOR_CONTENT = "//div[@class='article-listing']/div/div/ul[1]/li[2]~xpath";

        public static string BREADCRUMB_PREV_LINK = "field-title~classname";

        public static string SORT_DROPDOWN = "articlelistsortby~id";




        #endregion
    }
}
