﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkinAllianceTestPro.Blocks.ElementBlocks
{
   public class DirectoryElements
    {
        public static string PAGE_HEADING = "Inner-Title~classname";

        public static string PROFILE_PICTURES = "Dir-List-image~classname";

        public static string USER_DETAIL_SECTION = "Dir-List~classname";

        public static string USER_NAMES = "ccol-lg-8~classname";

        public static string USER_SPECIALTY = "user-specialty~classname";

        public static string USER_COUNTRY = "user-country~classname";

        public static string FILTER_COUNTRY_CHECKBOXES = "FilterbyCountry~id";

        public static string FILTER_SPECIALTY_CHECKBOEXS = "FilterbySpeciality~id";

        public static string FILTER_COUNTRY_SECTION = "Filterby-Country~classname";

        public static string FILTER_SPECIALTY_SECTION = "Filterby-speciality~classname";

        public static string EXPAND_ALL_BUTTON = "dir-expand~classname";

        public static string COLLAPSE_ALL_BUTTON = "dir-collapse~classname";

        public static string USERS_LIST_DIRECTORY = "user-directory-list~classname";
    }
}
