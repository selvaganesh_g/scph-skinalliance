Feature: Article Listing page
 
 Background: 
 Given I launch Skin Home page
  When I enter "user31@3107.com" in email text field
   And I enter "Photon@123" in password field
 
  
  @ArticleListing
Scenario Outline: Verify if the user is able to view the article listing page and its attributes
When I click "<Nav_Menu>" menu from navigation bar
When I click on "Aging" subcategory name
Then I verify corresponding "Aging" sub category listing page is displayed
 When I verify articles are displayed
 Then I verify "Image" of the articles in article listing page 
 Then I verify "short description" of the articles in article listing page 
 Then I verify "Comment icon" of the articles in article listing page 
 Then I verify "Like icon" of the articles in article listing page 
 #Then I verify "No of likes" of the articles in article listing page 
 #Then I verify "No of comments" of the articles in article listing page 
 Then I verify "Posted date" of the articles in article listing page 
 Then I verify "Author Name" of the articles in article listing page 
 Then I verify header and footer navigations are displayed
  Then I should see navigaton menu bar in page
   Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
 #| EXPERTS COMMENTS         |

 

 @ArticleListing 
 Scenario Outline: Verify if the user is able to navigate to article detail page from article details page in dermocosmetics
 When I click "DERMOCOSMETICS KNOWLEDGE" menu from navigation bar
 Then I click Topical subcategory in categorylisting page
 When I click a random "<Article_Link>" in article listing page
 Then I verify Article details page is displayed
  Examples: 
  | Article_Link |
  | Image        |
  | Content text |
  | Comment icon |
  | Like icon    |

  @ArticleListing
Scenario Outline: Verify if the user is able to navigate to article detail page from article details page in Experts Comments
 When I click "EXPERTS COMMENTS" menu from navigation bar
 Then I click Topical subcategory in categorylisting page
 When I click a random "<Article_Link>" in article listing page
 Then I verify Article details page is displayed
  Examples: 
  | Article_Link |
  | Image        |
  | Content text |
  | Comment icon |
  | Like icon    |

 
  @ArticleListing
Scenario Outline: Verify if the user is able to follow a category
When I click "<Nav_Menu>" menu from navigation bar
When I click on a random sub category name
 When I click on follow button
 Then I verify "UNFOLLOW" as button name
 Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS         |

  @ArticleListing
Scenario Outline: Verify if the user is able to sort the articles based on by date and popularity
When I click "DERMOCOSMETICS KNOWLEDGE" menu from navigation bar
Then I click Topical subcategory in categorylisting page
 When I click on sort option in article listing page
 Then I sort articles based on "<sort_option>"
 When I verify articles are displayed
 Examples: 
 | sort_option |
 | Date        |
 | Popularity  |


 @ArticleListing
  Scenario Outline: Verify if the user is able to view and access breadcrumb trail
  When I click "<Nav_Menu>" menu from navigation bar
  When I click on a random sub category name
  Then I verify breadcrumb trail is displayed
   And I click on previous link in breadcrumb in article listing page
  Then I verify Category listing page with title is displayed
  Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS         |
   


 		 
