﻿Feature: Verify the UI and functionality of Congresses Event Detail page

Background: 
Given I launch Skin Home page
 When I enter "testloreal@test.com" in email text field
 And I enter "Testuser123$" in password field


  @CongressesEventDetail 
Scenario Outline: Verify if the user is able to view Congresses Event Detail page with its attributes
 When I click "CONGRESSES" menu from navigation bar
 When I click on View all link of "<Sub_Category>"
 When I click on a random congresses from event listing page
 Then I verify respective congress detail page displayed
 Then I verify header and footer navigations are displayed
 Then I verify "Title" in event detail page
 Then I verify "Category Name" in event detail page
 Then I verify "Description" in event detail page
 Then I verify "Register button" in event detail page
 Then I verify "Date" in event detail page
 Examples: 
 | Sub_Category |
 | DERMATOLOGY  |
 | ESTHETIC     |
 | RESEARCH     |

 
  @CongressesEventDetail
Scenario Outline: Verify if the user is able to view Other Congresses section
When I click "CONGRESSES" menu from navigation bar
When I click on View all link of "<Sub_Category>"
 When I click on a random congresses from event listing page
 Then I verify other congresses section is displayed
 Then I verify "Date" displayed in other congresses section
 Then I verify "Small Description" displayed in other congresses section
  Examples: 
 | Sub_Category |
 | DERMATOLOGY  |
 | ESTHETIC     |
 | RESEARCH     |

 
  @CongressesEventDetail 
Scenario Outline: Verify if the user is able to navigate to a detail page on clicking a congresses from other congresses section
When I click "CONGRESSES" menu from navigation bar
When I click on View all link of "<Sub_Category>"
 When I click on a random congresses from event listing page
 Then I verify other congresses section is displayed
 When I click on a random congresses in other congresses
 Then I verify respective congress detail page displayed
 Examples: 
 | Sub_Category |
 | DERMATOLOGY  |
 | ESTHETIC     |
 | RESEARCH     |


 @CongressesEventDetail
Scenario Outline: Verify if  the user is able to navigate to a external registration site on clicking register button
When I click "CONGRESSES" menu from navigation bar
When I click on View all link of "<Sub_Category>"
 When I click on a random congresses from event listing page
  Then I verify respective congress detail page displayed
 When I click on register button in detail page
 Then I verify registration external site is displayed
 Examples: 
 | Sub_Category |
 | DERMATOLOGY  |
 | ESTHETIC     |
 | RESEARCH     |
