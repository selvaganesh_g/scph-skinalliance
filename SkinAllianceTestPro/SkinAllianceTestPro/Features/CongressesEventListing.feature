﻿Feature: Verify the UI and functionality of Congresses Event listing page

Background: 
Given I launch Skin Home page
 When I enter "testloreal@test.com" in email text field
  And I enter "Testuser123$" in password field
  
 

 @CongressEventListing 
Scenario Outline: Verify if the user is able to view Congresses Event listing page with its attributes
  When I click "CONGRESSES" menu from navigation bar
  When I click on View all link of "<Sub_Category>"
 #When I click on "<Sub_Category>" from dropdown
 Then I verify "<Sub_Category>" congresses event listing page
 Then I verify "Category Heading" in event listing page
 Then I verify "Image" in event listing page
 Then I verify "Date" in event listing page
 Then I verify "Small description" in event listing page
Examples: 
 | Sub_Category |
 | DERMATOLOGY  |
 | ESTHETIC     |
 | RESEARCH     |

 
 @CongressEventListing
Scenario Outline: Verify if the user is able to navigate to congresses detail page from event listing page
 When I click "CONGRESSES" menu from navigation bar
 When I click on View all link of "<Sub_Category>"
 Then I verify "<Sub_Category>" congresses event listing page
 When I click on a random congresses from event listing page
 Then I verify respective congress detail page displayed
 Then I verify header and footer navigations are displayed
 Examples: 
 | Sub_Category |
 | DERMATOLOGY  |
 | ESTHETIC     |
 | RESEARCH     |

