Feature: Sign In page
 
 Background: 
 Given I launch Skin Home page
 
 @Signin
 Scenario: Verify if user is able to view and login into the application from Sign In page
   Then I verify Skin Alliance logo
   Then I verify Enter your Email text field
   Then I verify Enter your Password text field
   Then I verify Login button is displayed
   #Then I verify "Are you a new user?" text is displayed
   When I enter "testloreal1@test.com" in email text field
   And I enter "Testuser123$" in password field
   Then I verify welcome message popup and close it
   Then I verify home page is displayed
	

@Signin
Scenario: Verify if user is able to view "Welcome message" popup
   Then I verify Skin Alliance logo
   When I click on the "Register for account" link
   When I enter "test2" in First name field
  And I enter "user2" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
  And I click on Submit button       
  Then I should see Registration confirmation page
  And I click on Back to Login button
   Then I verify Sign in page is displayed
   When I enter user mail id in email text field
   And I enter "Testuser123$" in password field
   Then I verify "Welcome message" popup is displayed
    
    

 @Signin
Scenario: Verify if user does not see "Welcome message" in next sign in on clicking checkbox
   Then I verify Skin Alliance logo
   When I click on the "Register for account" link
   When I enter "test3" in First name field
  And I enter "user3" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
  And I click on Submit button       
  Then I should see Registration confirmation page
  And I click on Back to Login button
  Then I verify Sign in page is displayed
   When I enter user mail id in email text field
   And I enter "Testuser123$" in password field
	Then I verify "Welcome message" popup is displayed
    And I click on no longer wish to see these messages checkbox
	When I click on User picture in header
   Then I click on "LOGOUT" link from user picture
   Then I verify Sign in page is displayed
    When I enter user mail id in email text field
    And I enter "Testuser123$" in password field
   Then I verify welcome message popup is not displayed
   

 
  @Signin
Scenario: Verify if user is able to login into the application using facebook account
  Then I verify Skin Alliance logo
  Then I verify Facebook button in sign in page
   And I click on facebook button
  Then I verify "facebook" login page is displayed
  When I enter "testuserskinalliance@gmail.com" in facebook email field
   And I enter "Testinguser123$" in facebook password field
   And I click facebook login button
   And I select a random country from country dropdown
   And I enter "1224123123" as Physical License number
   And I select medical speciality
   And I click on Submit button
   Then I verify welcome message popup and close it
   Then I should see navigaton menu bar in page

   @Signin
Scenario: Verify if user is able to login into the application using Linkedin account
   Then I verify Skin Alliance logo
   Then I verify Linkedln button in sign in page
   And I click on Linkedln button
  Then I verify "Linkedin" login page is displayed
   And I enter "testuserskinalliance@gmail.com" as username in linkedin
   And I enter "Testinguser123$" as password in linkedin
   And I click Authorizeapp button in linkedin
   When I select a random country from country dropdown
   And I enter "1224123123" as Physical License number
   And I select medical speciality
   And I click on Submit button
   Then I verify welcome message popup and close it
  Then I should see navigaton menu bar in page


  @Signin
Scenario: Verify if user is able to check Remember me and login into the application
 Then I verify Skin Alliance logo
 When I enter "testloreal1@test.com" in email text field
 And I click Remember me checkbox
 And I enter "Testuser123$" in password field
  Then I verify home page is displayed 
   

 @Signin
Scenario: Verify if user is able to login into the application using twitter account
  Then I verify Skin Alliance logo
  Then I verify twitter button in sign in page
   And I click on twitter button
  Then I verify "twitter" login page is displayed
   And I enter "TesterAlliance" as username in twitter
   And I enter "Testuser123$" as password in twitter
   And I click Authorizeapp button in twitter
   When I select a random country from country dropdown
   And I enter "1224123123" as Physical License number
   And I select medical speciality in twitter registration page
   And I click on Submit button
   Then I verify welcome message popup and close it
  Then I should see navigaton menu bar in page
   
@Signin
 Scenario: Verify if user is able to view the error message if email id authentication are invalid
   Then I verify Skin Alliance logo
   Then I verify Enter your Email text field
   Then I verify Enter your Password text field
   Then I verify Login button is displayed
   When I enter "test1@test$com" in email text field
    And I enter "Testuser123$" in password field
   Then I verify "Please enter email address" email field error message is displayed   
     
@Signin
Scenario: Verif if user is able to view the error message if email id field is empty
 Then I verify Skin Alliance logo
   Then I verify Enter your Email text field
   Then I verify Enter your Password text field
   Then I verify Login button is displayed
   When I enter "Testuser123$" in password field
   Then I verify "Please enter email address" as email empty error message

   
 @Signin     
Scenario: Verify if user is able to view the error message if password authentication is invalid
   Then I verify Skin Alliance logo
   Then I verify Enter your Email text field
   Then I verify Enter your Password text field
   Then I verify Login button is displayed
   When I enter "testloreal@test.com" in email text field
    And I enter "Test#say" in password field
  Then I verify "Incorrect Email Address or Password" password field error message is displayed
	

@Signin
Scenario: Verify if user is able to view the error message if password field is empty
   Then I verify Skin Alliance logo
   Then I verify Enter your Email text field
   Then I verify Enter your Password text field
   Then I verify Login button is displayed
   When I enter "testloreal@test.com" in email text field
    And I click login button in signin page
   Then I verify "Please enter the Password" as error message in signin page
    

  
 @Signin
Scenario Outline: Verify if user is able to view the error message if email id/user name is invalid in forgot password page 
 When I click on Forgot your Password link in sign In page
 Then I should see enter registered email text field in forgot password page
  And I enter "<Email_id>" in enter registered email field
 Then I verify "Please enter a valid email address" as error message
 Examples: 
 | Email_id       |
 | test@test@.com |
 |                |

 
  
@Signin
Scenario: Verify if user is able to navigate back to sign in page on clicking Back to homepage link
 When I click on Forgot your Password link in sign In page
 Then I should see enter registered email text field in forgot password page
  And I click Back to homepage link
 Then I verify Sign in page is displayed

 @Signin
Scenario: Verify if user is able to view the success message for forgot password
 When I click on Forgot your Password link in sign In page
 Then I should see enter registered email text field in forgot password page
  And I enter "testuser@test.com" in enter registered email field
 Then I verify "An email sent with password, login the application using new password!" as success message
  
   

@Signin
Scenario: Verify if user is able to navigate to signup page on clicking Register for account link
   Then I verify Sign in page is displayed
   When I click on Register Account link
   Then I verify "WELCOME TO SKINALLIANCE" as heading in signup page
   Then I verify "New User Registration" as heading in signup page

 @Signin   
Scenario Outline: Verify if user is not able to access footer links and popup is displayed
 Then I verify Sign in page is displayed
  When I click "<Footer_link>" link from footer
  Then I verify a login to access popup is displayed
  Then I should see close button in popup
  Examples: 
  |Footer_link|
  |Terms of Use|
  |Legal|
  |Cookie and Privacy Policy|


Scenario: Verify if user is able to change country
 When I click language dropdown
  And I select a country from dropdown
 Then I verify the country flag is changed
    
