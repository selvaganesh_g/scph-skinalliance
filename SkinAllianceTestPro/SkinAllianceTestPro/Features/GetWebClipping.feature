﻿Feature: Login and Home Page
 
 Background: 
 Given I launch Skin Home page
  When I enter "testuser@test.com" in email text field
   And I enter "testuser#say" in password field
  When I click on User picture in header
  Then I verify "GET-WEBCLIPPER" menu in dropdown
 When I click on "GET-WEBCLIPPER" in Image logo dropdown

  
@WebClipping
Scenario: Verify if the user is able to navigate and view video Web clipping page
 Then I verify "GET-WEBCLIPPER" page
 Then I verify page heading is displayed
 Then I should see webclipper video displayed
 
@WebClipping
Scenario: Verify if the user is able to view and click Download button
 Then I verify Download button in webclipping page
 When I click on the download link in webclipping page
 Then I should see chrome extension in new tab

 @WebClipping
Scenario: Verify if user is able to view and functionality of Related Post section in webclipping page
   Then I verify Related Post section is displayed
   Then I verify "Profile Pic" is displayed in related post
   Then I verify "User Name" is displayed in related post
   Then I verify "Likes" is displayed in related post
   Then I verify "Comments" is displayed in related post
   Then I verify "No of likes" is displayed in related post
   Then I verify "No of comments" is displayed in related post
    And I click on a User Name link in related post

@WebClipping
Scenario: Verify if the user is able to view the Posted by section in webclipping page
   Then I verify "Comments Posted by other users" in article details page
   Then I verify "Image" of comments posted by other users
   Then I verify "Username" of comments posted by other users
   Then I verify "Posted Date" posted by other users
    And I Post a comment in article details page
   Then I verify the corresponding comment is posted

  @WebClipping  
Scenario: Verify if the user is able to view and click the Like link in webclipping page
   Then I verify Like icon is displayed
    And I click the Like icon of the article
   Then I verify the no of likes for the article