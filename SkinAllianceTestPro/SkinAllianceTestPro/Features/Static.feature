Feature: Verify UI and functionality of Static footer Pages
 
  Background: 
 Given I launch Skin Home page
  When I enter "testloreal1@test.com" in email text field
   And I enter "Testuser123$" in password field
 
 @static
Scenario: Verify if the user is able to view About Us page and its contents
   Given I launch About us page
   Then I verify "ABOUT US" page is displayed
   Then I verify header and footer navigations are displayed
   Then I verify "Navigation menu" in header
    

@static
Scenario: Verify if the user is able to view Terms of use page and its contents
 When I click "Terms of Use" link from footer
  Then I verify "TERMS OF USE" page is displayed
  Then I verify description is displayed in "TERMS OF USE" page
  Then I verify header and footer navigations are displayed
  Then I verify "Navigation menu" in header
   
@static
Scenario: Verify if the user is able to view Privacy Policy page and its contents
 When I click "Cookie and Privacy Policy" link from footer
  Then I verify "COOKIE AND PRIVACY POLICY" page is displayed
  Then I verify description is displayed in "COOKIE AND PRIVACY POLICY" page
  Then I verify header and footer navigations are displayed
  Then I verify "Navigation menu" in header
   

  @static 
Scenario: Verify if the user is able to view Legal page and its contents
Then I verify "Navigation menu" in header
When I click "Legal" link from footer
  Then I verify "LEGAL" page is displayed
  Then I verify description is displayed in "LEGAL" page
  Then I verify header and footer navigations are displayed
  Then I verify "Navigation menu" in header
   
	
@static 
Scenario: Verify if the user is able to view Contact us page and its fields
Then I verify "Navigation menu" in header
  When I hover on a "Community" in navigation bar
  And I select "Contact us" from community dropdown
  Then I verify "CONTACT US" page is displayed
 Then I verify the following fields
 | Attributes    |
 | First Name    |
 | Last Name     |
 | Email         |
 | Phone Number  |
 | Message       |
 | Submit Button |
  
  @static 
 Scenario: Verify if the user is able to view first name field empty error message in contact us page
  When I hover on a "Community" in navigation bar
  And I select "Contact us" from community dropdown
   Then I verify "CONTACT US" page is displayed
   When I enter "user" in last name field
   And I enter "1234567" in contact us phone number field
   And I enter "testloreal@test.com" in email field
   And I enter "message" in message field
   And I click Submit button in contact us page
  Then I verify "Please enter the First Name" in contact us page

  @static 
  Scenario: Verify if the user is able to view last name field empty error message in contact us page
  When I hover on a "Community" in navigation bar
  And I select "Contact us" from community dropdown
   Then I verify "CONTACT US" page is displayed
  When I enter "test" in first name field
   And I enter "1234567" in contact us phone number field
   And I enter "testloreal@test.com" in email field
   And I enter "message" in message field
   And I click Submit button in contact us page
  Then I verify "Please enter the Last Name" in contact us page

  @static 
  Scenario: Verify if the user is able to view Email field empty error message in contact us page
  When I hover on a "Community" in navigation bar
  And I select "Contact us" from community dropdown
   Then I verify "CONTACT US" page is displayed
  When I enter "test" in first name field
   And I enter "user" in last name field
   And I enter "1234567" in contact us phone number field
   And I enter "message" in message field
   And I click Submit button in contact us page
  Then I verify "Please enter the Email" in contact us page

  @static 
  Scenario: Verify if the user is able to view Message field empty error message in contact us page
  When I hover on a "Community" in navigation bar
  And I select "Contact us" from community dropdown
   Then I verify "CONTACT US" page is displayed
  When I enter "test" in first name field
   And I enter "user" in last name field
   And I enter "1234567" in contact us phone number field
   And I enter "testloreal@test.com" in email field
   And I click Submit button in contact us page
  Then I verify "Please enter the Message" in contact us page

  @static 
  Scenario: Verify if the user is able to view invalid email id field error message in contact us page
  When I hover on a "Community" in navigation bar
  And I select "Contact us" from community dropdown
   Then I verify "CONTACT US" page is displayed
  When I enter "test" in first name field
   And I enter "user" in last name field
   And I enter "1234567" in contact us phone number field
   And I enter "testloreal@test@com" in email field
   And I enter "message" in message field
   And I click Submit button in contact us page
  Then I verify "Please enter a valid Email Address" in contact us page

 
  Scenario: Verify if the user is able to view Phone number field empty error message in contact us page
  When I hover on a "Community" in navigation bar
  And I select "Contact us" from community dropdown
   Then I verify "CONTACT US" page is displayed
  When I enter "test" in first name field
   And I enter "user" in last name field
   And I enter "testloreal@test.com" in email field
   And I enter "message" in message field
   And I click Submit button in contact us page
  Then I verify "Please enter Phone Number" in contact us page

  
  Scenario: Verify if the user is able to view Invalid phone number error message in contact us page
  When I hover on a "Community" in navigation bar
  And I select "Contact us" from community dropdown
   Then I verify "CONTACT US" page is displayed
  When I enter "test" in first name field
   And I enter "user" in last name field
   And I enter "testloreal@test.com" in email field
   And I enter "@@@" in contact us phone number field
   And I enter "message" in message field
   And I click Submit button in contact us page
  Then I verify "Please enter valid Phone Number" in contact us page
   
 
 @static
 Scenario: Verify if the user is able to submit message and view the success message
  When I hover on a "Community" in navigation bar
  And I select "Contact us" from community dropdown
  Then I verify "CONTACT US" page is displayed
  When I enter "test" in first name field
   And I enter "user" in last name field
   And I enter "test1@test.com" in email field
   And I enter "1234567890" in contact us phone number field
   And I enter "testmessage" in message field
   And I click Submit button in contact us page
  Then I should see "Message Submitted Successfully" message is displayed
  