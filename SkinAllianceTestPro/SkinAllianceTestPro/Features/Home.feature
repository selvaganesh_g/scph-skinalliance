﻿ Feature: Verify UI and Functionality of Home Page
 
 Background: 
 Given I launch Skin Home page
  When I enter "testloreal@test.com" in email text field
    And I enter "Testuser123$" in password field
 
@home 
 Scenario: Verify if the user is able to login to the application and view the homepage contents
	Then I verify "Skin Alliance logo" in header
	 And I verify the following contents in homepage
	   |Attributes              |
	   | Header                |
	   | Footer                |
	   | Navigation menu       |
	   | Image carousel slider |
	   | Latest Posts          |
	   | Favourite Post        |
	   | News                  |
       | Board Members          |
       | Image Logo             |
	   | Search                 |
	

############ Header ###############################################
 

@home 
Scenario: Verify if the user is able to view the header and its components
   Then I verify "Userpicture" in header
   Then I verify "Navigation menu" in header
   Then I verify "Skin Alliance logo" in header
   Then I verify "Search" in header
   Then I verify "Country flag" in header
   
   
@home
 Scenario: Verify if the user is redirected to home page on clicking Skin Alliance logo
    When I click on "Skin Alliance Logo" in header
    Then I verify home page is displayed
	
 
 
    

############ Footer ###############################################



   

 @home
Scenario Outline: Verify the footer pages are displayed on clicking the corresponding footer link
	When I click "<Footer_link>" link from footer
	#Then I verify "<Footer_link>" page is displayed
	Then I verify corresponding "<Footer_link>" page is displayed
	 Examples:
	  |Footer_link|
	  |Terms of Use|
	  |Legal|
	  |Privacy Policy|

@home 
Scenario: Verify if the user redirects to external site on clicking the "Skin Alliance" partner's logo or names
   When I verify Skin Alliance partner names with logos
    When I click on a random Skin Alliance Partner logo
   Then I verify external site is displayed
    

######### Navigation Menu #######################################################################

	 
 @home
 Scenario: Verify if the logged in user is able to view the navigation menu bar in home page
   Then I should see navigaton menu bar in page
   Then I verify categories in navigation bar
          |Attributes|
          | Home                     |
          | Dermocosmetics Knowledge |
          | Experts Comments          |
          | Webcasts                 |
          | Congresses               |
          | Clinical Cases           |
          | Community                |
          | +Create a Post           |
    

	@home
 Scenario Outline: Verify if the user navigate to respective landing page on clicking the menu in navigation bar
   When I hover on a "<Navigation_menu>" in navigation bar
   Then I verify "<Navigation_menu>" menu is expanded
   When I click "<Navigation_menu>" menu from navigation bar
   Then I verify "<Navigation_menu>" menu page
   Then I should see navigaton menu bar in page 
    And I click on "Skin Alliance Logo" in header
   Then I verify home page is displayed 
   When I hover on a "<Navigation_menu>" in navigation bar
   Then I verify "<Navigation_menu>" menu is expanded
    Examples:
      |Navigation_menu           |
      | DERMOCOSMETICS KNOWLEDGE |
      | WEBCASTS                 |
      | CONGRESSES               |
    # | COMMUNITY                |
      



################# Upcoming Congresses ####################################################################################################
   @home  
Scenario: Verify that user view the popular category widet and redirected to article listing page on clicking a category displayed
  Then I verify Upcoming Congresses in homepage
  Then I verify categories in Upcoming Congresses
  When I click on a random congresses from Upcoming Congresses
  Then I verify respective congress detail page displayed

  @home
Scenario: Verify that user view the View all button and congress listing page on clicking the button
  Then I verify Upcoming Congresses in homepage
  Then I verify "View ALL" button in the widget
  And I click on "View ALL" button in congresses section
  Then I verify congresses listing page is displayed


################### Image Logo ########################################################################################################## 
 @home 
Scenario: Verify if the user is able to view "My Skin Box" in home page
   When I click on User picture in header
   Then I verify "MY PROFILE" menu in dropdown
   Then I verify "MY NOTIFICATION" menu in dropdown
   Then I verify "CREATE POST" menu in dropdown
   Then I verify "GET WEBCLIPPER" menu in dropdown
   Then I verify "LOGOUT" menu in dropdown
   


  @home  
Scenario Outline: Verify the user is redirected to respective detail page on clicking menu from "My Skin Box" dropdown  
   When I click on User picture in header   
  When I click on "<menu_options>" in Image logo dropdown
  Then I verify "<menu_options>" page
   
     Examples:
      | menu_options   |
      | MY PROFILE     |
      |MY NOTIFICATION |
      |CREATE POST     |
      #|GET-WEBCLIPPER |
      |LOGOUT    |
   

################# Search ################################################################################################################
 @home
Scenario: Verify if the user is able to view the search box
    Then I verify the search field is displayed
    When I search for keyword "Why Skin Care is Important"
    Then I verify search results page is displayed
    When I click a random "Image" in article listing page
    Then I verify Article details page is displayed	 
    Then I verify header and footer navigations are displayed
	Then I verify the search field is displayed
     

##################Scientific Board ###############################################################################

@home
Scenario: Verify if the user is able to view and navigate to Scientific board page
 Then I verify Scientific board widget in home page
 Then I verify Image in scientific board widget
 Then I verify description in scientific board widget
 Then I verify Read more button in scientific board widget
 When I click on Read more button in scientific board
 Then I should see "SCIENTIFIC BOARD" detail page
 Then I verify members with details listed

############## Image Slider #############################################################################################################
@home 
Scenario: Verify if the user is able to view Image carousel and redirect to article details page on clicking it
  Then I verify image carousel in home page
  Then I verify image of articles in image carousel
  Then I verify article title in image carousel
  Then I verify description about the articles in image carousel
   

  @home 
Scenario:  Verify if the user is able to navigate to article details page on clicking read more link
  Then I verify image carousel in home page
  Then I verify Read More button is displayed
  Then I click on Read More button
  Then I verify Article details page is displayed

  @home
Scenario: Verify if the user is able to navigate betwen three posts shown in the carousel
  Then I verify image carousel in home page
  When I click on the left arrow in the carousel
  Then I verify different article in the image carousel
  

########### News, Latest Posts & Favourite Posts #############################################################################################################

@home 
Scenario: Verify if the user is able to view category listing and the Posts/articles
  Then I verify Latest Posts and Favourite Posts widget in homepage
  Then I verify "short description" related to post in category listing
  Then I verify "image" related to post in category listing
  Then I verify "no of likes" related to post in category listing
  Then I verify "no of comments" related to post in category listing
  Then I verify sliding arrows in Popular category heading
  Then I verify View all button in the post section

  @home 
Scenario Outline: Verify if the user is able to redirect to article details page on clicking it
  Then I verify Latest Posts and Favourite Posts widget in homepage
   And I click on a random post in "<section>" post
   Then I verify Article details page is displayed
  Examples: 
  | section  |
  | Latest   |
  | Favorite |
  | News     |

@home
Scenario Outline: Verify if the user is able to redirect to  “News, Latest Posts & Favourite Posts” page on clicking it
  Then I verify Latest Posts and Favourite Posts widget in homepage
   And I click on a view all button of "<section>"
  When I verify articles are in "<section>" page
  Examples: 
  | section  |
  | Latest   |
  | Favorite |
  | News     |


  @home
Scenario: Verify if the user is able to view Categories listing page
 Then I verify Latest Posts and Favourite Posts widget in homepage
  And I click on a random category in latest posts
 Then I should see corresponding category listing page


@home
 Scenario: Verify if the user is able to view the logo and title of cateogries in popular categories section
 Then I verify "name" of the categories in popular categories section
  Then I verify "logo" of the categories in popular categories section
  



  ########Create a Post ############################################


 @home
Scenario: Verify if the user is able to view the “Create a Post/ AddContent” in “My Skin Box” widget in homepage
    When I click "CREATE POST" menu from navigation bar
   Then I verify "CREATE POST" as heading in the page
  

  @home 
Scenario: Verify if the user is able to view the Fields and contents in "Create a Post" page
  When I click "CREATE POST" menu from navigation bar
 Then I verify the following contents in create a post page
    | Fields             |
    | Enter title field  |
    | Text input section |
    | Website URL field  |
    | File Upload field  |
    | Add Media button   |
    | Browse button      |
    | Questionnaire      |

@home
Scenario: Verify if the user is able to add a new post by entering all contents
  When I click "CREATE POST" menu from navigation bar
   Then I verify "CREATE POST" as heading in the page
  And I enter "OCT17 Congress" as title for the post
  And I add content in the Text input section
  And I enter "https://www.skin-alliance.com" as URL 
  And I upload a file in file upload field
  And I add a image in Add featured image section
  And I select a random category in create a post page
  When I click on "Create post" button in create new post
  Then I should see new post popup
   And I click on Ok button in new post popup
  Then I verify "CREATE POST" as heading in the page
  

  @home
Scenario: Verify if the user is able to add a new post without a file uploaded
  When I click "CREATE POST" menu from navigation bar
  Then I verify "CREATE POST" as heading in the page
  And I enter "OCT17 Congress" as title for the post
  And I add content in the Text input section
  And I enter "https://www.skin-alliance.com" as URL 
  And I add a image in Add featured image section
  And I select a random category in create a post page
  When I click on "Create post" button in create new post
  Then I should see new post popup
   And I click on Ok button in new post popup
  Then I verify "CREATE POST" as heading in the page
  

  @home 
 Scenario: Verify if the user is able to add a new post without entering a URL
  When I click "CREATE POST" menu from navigation bar
   Then I verify "CREATE POST" as heading in the page
  And I enter "OCT17 Congress" as title for the post
  And I add content in the Text input section
  And I upload a file in file upload field
  And I add a image in Add featured image section
  And I select a random category in create a post page
  When I click on "Create post" button in create new post
  Then I should see new post popup
   And I click on Ok button in new post popup
  Then I verify "CREATE POST" as heading in the page
  



  @home 
Scenario: Verify if the user is able to add a new post without both URL and a file uploaded 
 When I click "CREATE POST" menu from navigation bar
   Then I verify "CREATE POST" as heading in the page
  And I enter "OCT17 Congress" as title for the post
  And I add content in the Text input section
  And I add a image in Add featured image section
  And I select a random category in create a post page
  When I click on "Create post" button in create new post
  Then I should see new post popup
   And I click on Ok button in new post popup
  Then I verify "CREATE POST" as heading in the page

  

  
Scenario: Verify if the user is able to view confirmation message on uploading a document
  When I click "CREATE POST" menu from navigation bar
  Then I verify "CREATE POST" as heading in the page
  And I enter "OCT17 Congress" as title for the post
  And I add content in the Text input section 
  And I upload a file in file upload field
 Then I verify "Please confirm there is no plagiarism" or There is no Copyright" message in a popup


Scenario: Verify if the user is able to add multiple images in create a post page
  When I click "CREATE POST" menu from navigation bar
Then I verify "CREATE A POST" as heading in the page
  When I add more than one image in featured image section
 Then I verify multiple images are displayed in featured image section

 @home 
Scenario: Verify if the user is able to view questionnaire section
  When I click "CREATE POST" menu from navigation bar
 Then I verify Questionnaire section is displayed
  

  @home 
Scenario: Verify if the user is able to view "Preview" and "Submit" button in create a post page 
 When I click "CREATE POST" menu from navigation bar
  Then I verify Add Question link in create a post
  Then I verify "Preview post" button in create a post
  Then I verify "Submit post" button in create a post
  


  @home 
Scenario: Verify if the user is able to add a question in questionnaire section
   When I click "CREATE POST" menu from navigation bar
   Then I enter "OCT17 Congress" as title for the post
  And I add content in the Text input section
  And I select a random category in create a post page
  When I click on "Add Question" link
 When I add question in Enter question text field
 And I enter answers related question in Answers text fields
 And I enter comments in Enter Response text field
 And I click on a answer checkbox
 Then I verify display user response percentage checkbox
 And I click display user response percentage checkbox 
 And I click Save button in questionnaire section
 Then I verify added question is displayed in questionnaire section
 And I click on preview button 
 Then I verify preview of the post is displayed
 When I click on "Create post" button in create new post
 Then I should see new post popup
  And I click on Ok button in new post popup
 Then I verify "CREATE POST" as heading in the page



 
   
   
 