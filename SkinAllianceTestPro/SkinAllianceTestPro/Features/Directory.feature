Feature: Directory page
 
 Background: 
 Given I launch Skin Home page
  When I enter "testloreal@test.com" in email text field
   And I enter "Testuser123$" in password field
 When I hover on a "Community" in navigation bar
   And I select "Directory" from community dropdown
   #Given I launch Directory page
 
 
 @directory
 Scenario: Verify if the user is able to view the details of various users
 Then I verify "DIRECTORY" listing page is displayed
 #Then I verify users are listed on alphabetic order 
 Then I verify "Profile pic" of the users
 Then I verify "UserName" of the users
 Then I verify header and footer navigations are displayed
  Then I verify "Navigation menu" in header

	@directory
 Scenario: Verify if the user is able to view and functionality of the "Filter by Country" section
  Then I verify "DIRECTORY" listing page is displayed
  Then I verify "Filter by Country" section is displayed in directory page
  When I select a "random Country" from the filter by list
  Then I verify user list displayed based on "selected country"
	
 @directory  
 Scenario: Verify if the user is able to view and functionality of the "Filter By Speciality" section
  Then I verify "DIRECTORY" listing page is displayed
  Then I verify "Filter by speciality" section is displayed in directory page
  When I select a "random specialty" from the filter by list
   Then I verify user list displayed based on "selected speciality"

   @directory 
  Scenario: Verify if the user is able to navigate to user profile page on clicking a user name
   When I click a random user name in directory page
  Then I verify corresponding userprofile page is displayed
   
   
   @directory 
Scenario: Verify if the user is able to view and access the Expand and Collapse All button
 Then I verify "DIRECTORY" listing page is displayed
 When I click "Collapse All" button in directory page
 Then I verify the list of users "not displayed" in directory
 When I click "Expand All" button in directory page
 Then I verify the list of users "displayed" in directory

