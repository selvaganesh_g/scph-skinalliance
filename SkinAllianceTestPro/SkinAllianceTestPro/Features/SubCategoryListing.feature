﻿Feature: SubCategory Listing Page
	
Background: 
 Given I launch Skin Home page
  When I enter "user31@3107.com" in email text field
  And I enter "Photon@123" in password field
  

  @subcategorylisting
Scenario Outline: Verify if the user is able to view Category name title and sub category
 When I click "<Nav_Menu>" menu from navigation bar
 When I click on a random category name
 Then I verify corresponding sub category listing page is displayed
  And I verify subcategories are displayed
  #And I verify two articles per subcategory
   Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS         |

  @subcategorylisting 
Scenario Outline: Verify if the user is able to view Subcategory contents
 When I click "<Nav_Menu>" menu from navigation bar
 When I click on "Aging" subcategory name
 Then I verify corresponding "Aging" sub category listing page is displayed
 Then I verify "Image" of the articles
 Then I verify "short description" of the articles
 Then I verify "Comment icon" of the articles
 Then I verify "Like icon" of the articles
 #Then I verify "No of likes" of the articles
 #Then I verify "No of comments" of the articles
 Then I verify "Posted date" of the articles
 Then I verify "Author Name" of the articles
 Then I verify header and footer navigations are displayed
 Then I should see navigaton menu bar in page
 Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS         |
  
  

 @subcategorylisting @Edit
 Scenario Outline: Verify if the user is able to navigate to article detail page from dermocosmetics knowledge category listing page
 When I click "DERMOCOSMETICS KNOWLEDGE" menu from navigation bar
 When I click on a random category name
 Then I verify corresponding sub category listing page is displayed
 When I click "<Article_Link>" of a random article
 Then I verify Article details page is displayed
  Examples: 
  | Article_Link |
  | Image        |
  | Content text |
  | Comment icon |
  | Like icon    |

  @subcategorylisting 
Scenario Outline: Verify if the user is able to navigate to article detail page from Experts Comments category listing page
 When I click "DERMOCOSMETICS KNOWLEDGE" menu from navigation bar
 Then I click Topical subcategory in categorylisting page
 Then I verify corresponding sub category listing page is displayed
 When I click "<Article_Link>" of a random article
 Then I verify Article details page is displayed
  Examples: 
  | Article_Link |
  | Image        |
  | Content text |
  | Comment icon |
  | Like icon    |

  @subcategorylisting
Scenario Outline: Verify if the user is able to navigate to sub category page
When I click "<Nav_Menu>" menu from navigation bar
 When I click on a random category name
 Then I verify corresponding sub category listing page is displayed
 #When I click on a random View All button
 #When I verify articles are displayed
  Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS         |
 

 @subcategorylisting
Scenario Outline: Verify if the user is able to follow a category
When I click "<Nav_Menu>" menu from navigation bar
 When I click on a random category name
 Then I verify corresponding sub category listing page is displayed
 When I click on follow button
 Then I verify "UNFOLLOW" as button name
  Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS         |
  

 @subcategorylisting
 Scenario Outline: Verify if the user is able to view and access breadcrumb trail
 When I click "<Nav_Menu>" menu from navigation bar
 When I click on a random category name
 Then I verify corresponding sub category listing page is displayed
  Then I verify breadcrumb trail is displayed
   And I click on previous breadcrumb link in subcategory listing page
  Then I verify Category listing page with title is displayed
  Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS         |
   
 


 
