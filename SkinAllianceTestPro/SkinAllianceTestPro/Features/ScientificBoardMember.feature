﻿Feature: Verify the UI and Functionality of Scientific board member page
	
Background: 
 Given I launch Skin Home page
  When I enter "testloreal@test.com" in email text field
   And I enter "Testuser123$" in password field
   When I hover on a "Community" in navigation bar
   And I select "Scientific Board" from community dropdown


 @scientificboardmember
Scenario: Verify if  the user is able to view navigate to scientific board member listing page
 Then I should see "SCIENTIFIC BOARD" detail page
 Then I verify header and footer navigations are displayed
 Then I verify "Navigation menu" in header

 @scientificboardmember
Scenario: Verify if the user is able to view Scientifc board members details
 Then I should see "SCIENTIFIC BOARD" detail page
 Then I verify the scientific board members details
 | Attributes   |
 | Description  |
 | Image        |
 | Name         |
 #| Specialty    |
 | Introduction |

 @scientificboardmember
Scenario Outline: Verify if the user is able to navigate to User profile page from scientific board member page
 When I click on a "<Element>" random board member name
 Then I verify User Profile page is displayed
 Examples: 
 | Element |
 | Name    |
 | Image   |