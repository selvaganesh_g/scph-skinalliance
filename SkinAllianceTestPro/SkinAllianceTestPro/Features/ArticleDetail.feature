Feature: Article Detail page
 
 Background: 
  Given I launch Skin Home page
  When I enter "user31@3107.com" in email text field
   And I enter "Photon@123" in password field
  

  @ArticleDetails
 Scenario: Verify if the user is able to view the attributes of Article details page
  When I click "EXPERTS COMMENTS" menu from navigation bar
  Then I click Topical subcategory in categorylisting page
  When I click a random "Image" in article listing page
  Then I verify Article details page is displayed
 # Then I verify "Posted by : " in article details page
 # Then I verify "Posted" in article details page
 # Then I verify "Category" in article details page
 # Then I verify "image" in article details page
 # Then I verify "description" in article details page
   
   

   @ArticleDetails
Scenario: Verify if the user is able to navigate back to previous page on clicking link in breadcrumb trail
 When I click "DERMOCOSMETICS KNOWLEDGE" menu from navigation bar
  When I click on a random sub category name
  When I click a random "Image" in article listing page
  Then I verify breadcrumb trail is displayed
   And I click on previous link in breadcrumb trail in article details page
  Then I verify Sub Category listing page with title is displayed
   
    
	@ArticleDetails
 Scenario: Verify if the user is able to view the Posted by section
 When I click "DERMOCOSMETICS KNOWLEDGE" menu from navigation bar
  When I click on a random sub category name
  When I click a random "Image" in article listing page
  Then I verify Article details page is displayed
  Then I Post a comment in article details page
   #Then I verify the corresponding comment is posted
   Then I verify Comments Posted by other users in article details page
   Then I verify "Image" of comments posted by other users
   Then I verify "Username" of comments posted by other users
   Then I verify Posted Date posted by other users

  @ArticleDetails
Scenario: Verify if the user is able to Reply to a comment
 When I click "DERMOCOSMETICS KNOWLEDGE" menu from navigation bar
  Then I click Topical subcategory in categorylisting page
  When I click a random "Image" in article listing page
  Then I verify Article details page is displayed
  #Then I Post a comment in article details page
  #And I Enter a reply for a random comment
  #Then I verify the reply is posted

  
   
	@ArticleDetails 
 Scenario: Verify if the user is able to view and click the Like link
 When I click "DERMOCOSMETICS KNOWLEDGE" menu from navigation bar
 Then I click Topical subcategory in categorylisting page
  When I click a random "Image" in article listing page
  Then I verify Article details page is displayed
  # Then I verify Like icon is displayed
  # And I click the Like icon of the article
  # Then I verify the no of likes for the article

   @ArticleDetails
Scenario: Verify if the user is able to view and click the Report to Moderator link
   Then I verify the search field is displayed
  # When I search for keyword "Skin Care"
  # Then I verify search results page is displayed
   When I click "DERMOCOSMETICS KNOWLEDGE" menu from navigation bar
  Then I click Topical subcategory in categorylisting page
   When I click a random "Image" in article listing page
   Then I verify Article details page is displayed	
  Then I verify Report to Moderator link is displayed
   And I click on Report to Moderator link of article
 # Then I verify message "Thank you an email has been sent to the moderator." is displayed in popup
 # Then I verify close button is displayed in popup
 #  And I click close button in report to moderator popup
#Then I verify Article details page is displayed
    
@ArticleDetails	
 Scenario Outline: Verify if user is able to view and functionality of Related Post section
   When I click "<Nav_menu>" menu from navigation bar
   Then I click Topical subcategory in categorylisting page
 # When I click on a random sub category name
   When I click a random "Image" in article listing page
  Then I verify Article details page is displayed
   Then I verify Related Post section is displayed
   Then I verify "Profile Pic" is displayed in related post
   Then I verify "User Name" is displayed in related post
   Then I verify "Likes" is displayed in related post
   Then I verify "Comments" is displayed in related post
   #Then I verify "No of likes" is displayed in related post
   #Then I verify "No of comments" is displayed in related post
    And I click on a User Name link in related post
   Then I verify Article details page is displayed
   Examples: 
   | Nav_menu                 |
   | DERMOCOSMETICS KNOWLEDGE |
   | EXPERTS COMMENTS         |
    

 @ArticleDetails
 Scenario: Verify if user is able to view Access the website sections
 When I click "DERMOCOSMETICS KNOWLEDGE" menu from navigation bar
  Then I click Topical subcategory in categorylisting page
  When I click a random "Image" in article listing page
  # Then I should see "ACCESS WEBSITE" button
  #   And I click on "ACCESS WEBSITE" button  
   # Then I verify video player page in new tab
     


Scenario: Verify if user is able to view "Edit" button and Edit in Article details page
  Then I verify "Edit" button in article details page
   And I click "Edit" button
  Then I verify edit page is displayed
  Then I verify Admin user/post owner update the page
   And I close the browser

 
Scenario: Verify if user is able to view "Delete" button and delete posts in Article details page
  Then I verify "Delete" button in article details page
  Then I verify "Delete" button not displayed for article by another user
   And I click "Delete" button
  Then I verify edit page is displayed
  Then I verify Admin user/post owner update the page
   And I close the browser


	
Scenario: Verify the functionality of WebCast in Article Details Page
  When I hover on a "Webcasts" in navigation bar
 And I click on "Aesthetics" in submenu
 Then I click on a random Webcast article
 Then I verify Webcast details page is displayed
  And I click on Play the Webcast link
  And I click on Play button in popup
  Then I verify video is displayed in a window popup
  #Then I verify the heading in the popup window
   And I click on the slides to select the topic
   And I click the close link in webcast popup
   Then I verify the popup window is closed
   And I click download button in webcast details page
 Then I should see Copyright alert message
  
  

  
Scenario: Verify if user is able to view "Play the Webcast" button and "download" button
 When I hover on a "Webcasts" in navigation bar
 And I click on "Aesthetics" in submenu
 Then I click on a random Webcast article
 Then I verify Webcast details page is displayed
  Then I should see download button
  #Then I should not see Access the Website button
   And I click download button
  Then I verify a copyright popup is displayed

  
Scenario: Verify the user is able to view article details page with questions
   And I select answer checkbox
   And I click "Submit my answer" button
  Then I should see percentage of user response in a table
   And I enter comment in comment text box
   And I close the browser



 
	
 
	
	
 
 