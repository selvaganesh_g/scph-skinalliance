Feature: User Profile page
 
 Background: 
 Given I launch Skin Home page
  When I enter "testloreal@test.com" in email text field
   And I enter "Testuser123$" in password field
   When I hover on a "Community" in navigation bar
   And I select "Directory" from community dropdown
  When I click a random user name in directory page
 
 @userprofile 
 Scenario: Verify if the user is able to view a user profile and its attributes in user profile page
   Then I verify corresponding userprofile page is displayed
	Then I verify the attributes of user Profile page
       | Attributes        |
       | Profile Picture   |
       | Profile Name      |
       #| LinkedIn         |
       #| Phone Number     |
       #| Email Id         |
       | Follow Button     |
       | Summary Section   |
       | Following Section |
	   |Followed By        | 
	Then I verify header and footer navigations are displayed
	

	@userprofile 
Scenario: Verify if the user is able to follow a user
 When I click on follow button in user profile page
 Then I verify "UNFOLLOW" as button name in user profile page
  
	@userprofile 
 Scenario Outline: Verify if the user is able to view followed by and following sections and its attributes
	Then I verify "<Section>" section in userprofile page
	Then I verify profile picture is displayed in "<Section>"
	Then I verify Profile name is displayed in "<Section>"
	 Then I click on a random user name in "<Section>" section
	Then I verify User Profile page is displayed
	#Then I verify the following users count in "<Section>" section
	 Examples:
	 | Section     |
	 | Followed By: |
	 | Following:   |

	 @userprofile 
 Scenario: Verify if the user is able to view the Categories followed and subcategories followed section
  Then I verify "Categories Followed:" section in userprofile page
  Then I verify "SubCategories Followed:" section in userprofile page
  Then I verify Name of "Categories Followed:" section
  Then I verify Name of "SubCategories Followed:" section
	
	@userprofile 
  Scenario: Verify if the user is able to view breadcrumb and access it in Userprofile page
 Then I verify breadcrumb trail is displayed
  And I click on previous link in breadcrumb in Userprofile page
 Then I verify "DIRECTORY" listing page is displayed