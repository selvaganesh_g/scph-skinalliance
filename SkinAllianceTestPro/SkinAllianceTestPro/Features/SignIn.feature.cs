﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.2.0.0
//      SpecFlow Generator Version:2.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace SkinAllianceTestPro.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.2.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Sign In page")]
    public partial class SignInPageFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "SignIn.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "Sign In page", null, ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 3
 #line 4
 testRunner.Given("I launch Skin Home page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to view and login into the application from Sign In page")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToViewAndLoginIntoTheApplicationFromSignInPage()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to view and login into the application from Sign In page", new string[] {
                        "Signin"});
#line 7
 this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 8
   testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 9
   testRunner.Then("I verify Enter your Email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 10
   testRunner.Then("I verify Enter your Password text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 11
   testRunner.Then("I verify Login button is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 13
   testRunner.When("I enter \"testloreal1@test.com\" in email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 14
   testRunner.And("I enter \"Testuser123$\" in password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 15
   testRunner.Then("I verify welcome message popup and close it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 16
   testRunner.Then("I verify home page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to view \"Welcome message\" popup")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToViewWelcomeMessagePopup()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to view \"Welcome message\" popup", new string[] {
                        "Signin"});
#line 20
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 21
   testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 22
   testRunner.When("I click on the \"Register for account\" link", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 23
   testRunner.When("I enter \"test2\" in First name field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 24
  testRunner.And("I enter \"user2\" as last name in signup page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 25
  testRunner.And("I enter email address in email address field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 26
  testRunner.And("I select a random country from country dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 27
  testRunner.And("I enter \"1224123123\" as Physical License number", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 28
  testRunner.And("I select medical speciality", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 29
  testRunner.And("I select preferred language", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 30
  testRunner.And("I enter \"Testuser123$\" as password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 31
  testRunner.And("I enter \"Testuser123$\" in confirm password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 32
  testRunner.And("I click on Submit button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 33
  testRunner.Then("I should see Registration confirmation page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 34
  testRunner.And("I click on Back to Login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 35
   testRunner.Then("I verify Sign in page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 36
   testRunner.When("I enter user mail id in email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 37
   testRunner.And("I enter \"Testuser123$\" in password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 38
   testRunner.Then("I verify \"Welcome message\" popup is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user does not see \"Welcome message\" in next sign in on clicking checkbo" +
            "x")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserDoesNotSeeWelcomeMessageInNextSignInOnClickingCheckbox()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user does not see \"Welcome message\" in next sign in on clicking checkbo" +
                    "x", new string[] {
                        "Signin"});
#line 43
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 44
   testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 45
   testRunner.When("I click on the \"Register for account\" link", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 46
   testRunner.When("I enter \"test3\" in First name field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 47
  testRunner.And("I enter \"user3\" as last name in signup page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 48
  testRunner.And("I enter email address in email address field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 49
  testRunner.And("I select a random country from country dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 50
  testRunner.And("I enter \"1224123123\" as Physical License number", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 51
  testRunner.And("I select medical speciality", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 52
  testRunner.And("I select preferred language", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 53
  testRunner.And("I enter \"Testuser123$\" as password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 54
  testRunner.And("I enter \"Testuser123$\" in confirm password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 55
  testRunner.And("I click on Submit button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 56
  testRunner.Then("I should see Registration confirmation page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 57
  testRunner.And("I click on Back to Login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 58
  testRunner.Then("I verify Sign in page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 59
   testRunner.When("I enter user mail id in email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 60
   testRunner.And("I enter \"Testuser123$\" in password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 61
 testRunner.Then("I verify \"Welcome message\" popup is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 62
    testRunner.And("I click on no longer wish to see these messages checkbox", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 63
 testRunner.When("I click on User picture in header", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 64
   testRunner.Then("I click on \"LOGOUT\" link from user picture", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 65
   testRunner.Then("I verify Sign in page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 66
    testRunner.When("I enter user mail id in email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 67
    testRunner.And("I enter \"Testuser123$\" in password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 68
   testRunner.Then("I verify welcome message popup is not displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to login into the application using facebook account")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToLoginIntoTheApplicationUsingFacebookAccount()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to login into the application using facebook account", new string[] {
                        "Signin"});
#line 73
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 74
  testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 75
  testRunner.Then("I verify Facebook button in sign in page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 76
   testRunner.And("I click on facebook button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 77
  testRunner.Then("I verify \"facebook\" login page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 78
  testRunner.When("I enter \"testuserskinalliance@gmail.com\" in facebook email field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 79
   testRunner.And("I enter \"Testinguser123$\" in facebook password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 80
   testRunner.And("I click facebook login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 81
   testRunner.And("I select a random country from country dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 82
   testRunner.And("I enter \"1224123123\" as Physical License number", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 83
   testRunner.And("I select medical speciality", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 84
   testRunner.And("I click on Submit button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 85
   testRunner.Then("I verify welcome message popup and close it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 86
   testRunner.Then("I should see navigaton menu bar in page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to login into the application using Linkedin account")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToLoginIntoTheApplicationUsingLinkedinAccount()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to login into the application using Linkedin account", new string[] {
                        "Signin"});
#line 89
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 90
   testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 91
   testRunner.Then("I verify Linkedln button in sign in page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 92
   testRunner.And("I click on Linkedln button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 93
  testRunner.Then("I verify \"Linkedin\" login page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 94
   testRunner.And("I enter \"testuserskinalliance@gmail.com\" as username in linkedin", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 95
   testRunner.And("I enter \"Testinguser123$\" as password in linkedin", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 96
   testRunner.And("I click Authorizeapp button in linkedin", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 97
   testRunner.When("I select a random country from country dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 98
   testRunner.And("I enter \"1224123123\" as Physical License number", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 99
   testRunner.And("I select medical speciality", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 100
   testRunner.And("I click on Submit button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 101
   testRunner.Then("I verify welcome message popup and close it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 102
  testRunner.Then("I should see navigaton menu bar in page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to check Remember me and login into the application")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToCheckRememberMeAndLoginIntoTheApplication()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to check Remember me and login into the application", new string[] {
                        "Signin"});
#line 106
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 107
 testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 108
 testRunner.When("I enter \"testloreal1@test.com\" in email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 109
 testRunner.And("I click Remember me checkbox", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 110
 testRunner.And("I enter \"Testuser123$\" in password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 111
  testRunner.Then("I verify home page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to login into the application using twitter account")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToLoginIntoTheApplicationUsingTwitterAccount()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to login into the application using twitter account", new string[] {
                        "Signin"});
#line 115
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 116
  testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 117
  testRunner.Then("I verify twitter button in sign in page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 118
   testRunner.And("I click on twitter button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 119
  testRunner.Then("I verify \"twitter\" login page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 120
   testRunner.And("I enter \"TesterAlliance\" as username in twitter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 121
   testRunner.And("I enter \"Testuser123$\" as password in twitter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 122
   testRunner.And("I click Authorizeapp button in twitter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 123
   testRunner.When("I select a random country from country dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 124
   testRunner.And("I enter \"1224123123\" as Physical License number", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 125
   testRunner.And("I select medical speciality in twitter registration page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 126
   testRunner.And("I click on Submit button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 127
   testRunner.Then("I verify welcome message popup and close it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 128
  testRunner.Then("I should see navigaton menu bar in page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to view the error message if email id authentication are i" +
            "nvalid")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToViewTheErrorMessageIfEmailIdAuthenticationAreInvalid()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to view the error message if email id authentication are i" +
                    "nvalid", new string[] {
                        "Signin"});
#line 131
 this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 132
   testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 133
   testRunner.Then("I verify Enter your Email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 134
   testRunner.Then("I verify Enter your Password text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 135
   testRunner.Then("I verify Login button is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 136
   testRunner.When("I enter \"test1@test$com\" in email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 137
    testRunner.And("I enter \"Testuser123$\" in password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 138
   testRunner.Then("I verify \"Please enter email address\" email field error message is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verif if user is able to view the error message if email id field is empty")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifIfUserIsAbleToViewTheErrorMessageIfEmailIdFieldIsEmpty()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verif if user is able to view the error message if email id field is empty", new string[] {
                        "Signin"});
#line 141
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 142
 testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 143
   testRunner.Then("I verify Enter your Email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 144
   testRunner.Then("I verify Enter your Password text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 145
   testRunner.Then("I verify Login button is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 146
   testRunner.When("I enter \"Testuser123$\" in password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 147
   testRunner.Then("I verify \"Please enter email address\" as email empty error message", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to view the error message if password authentication is in" +
            "valid")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToViewTheErrorMessageIfPasswordAuthenticationIsInvalid()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to view the error message if password authentication is in" +
                    "valid", new string[] {
                        "Signin"});
#line 151
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 152
   testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 153
   testRunner.Then("I verify Enter your Email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 154
   testRunner.Then("I verify Enter your Password text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 155
   testRunner.Then("I verify Login button is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 156
   testRunner.When("I enter \"testloreal@test.com\" in email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 157
    testRunner.And("I enter \"Test#say\" in password field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 158
  testRunner.Then("I verify \"Incorrect Email Address or Password\" password field error message is di" +
                    "splayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to view the error message if password field is empty")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToViewTheErrorMessageIfPasswordFieldIsEmpty()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to view the error message if password field is empty", new string[] {
                        "Signin"});
#line 162
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 163
   testRunner.Then("I verify Skin Alliance logo", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 164
   testRunner.Then("I verify Enter your Email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 165
   testRunner.Then("I verify Enter your Password text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 166
   testRunner.Then("I verify Login button is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 167
   testRunner.When("I enter \"testloreal@test.com\" in email text field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 168
    testRunner.And("I click login button in signin page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 169
   testRunner.Then("I verify \"Please enter the Password\" as error message in signin page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to view the error message if email id/user name is invalid" +
            " in forgot password page")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        [NUnit.Framework.TestCaseAttribute("test@test@.com", null)]
        [NUnit.Framework.TestCaseAttribute("", null)]
        public virtual void VerifyIfUserIsAbleToViewTheErrorMessageIfEmailIdUserNameIsInvalidInForgotPasswordPage(string email_Id, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "Signin"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to view the error message if email id/user name is invalid" +
                    " in forgot password page", @__tags);
#line 174
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 175
 testRunner.When("I click on Forgot your Password link in sign In page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 176
 testRunner.Then("I should see enter registered email text field in forgot password page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 177
  testRunner.And(string.Format("I enter \"{0}\" in enter registered email field", email_Id), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 178
 testRunner.Then("I verify \"Please enter a valid email address\" as error message", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to navigate back to sign in page on clicking Back to homep" +
            "age link")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToNavigateBackToSignInPageOnClickingBackToHomepageLink()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to navigate back to sign in page on clicking Back to homep" +
                    "age link", new string[] {
                        "Signin"});
#line 187
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 188
 testRunner.When("I click on Forgot your Password link in sign In page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 189
 testRunner.Then("I should see enter registered email text field in forgot password page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 190
  testRunner.And("I click Back to homepage link", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 191
 testRunner.Then("I verify Sign in page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to view the success message for forgot password")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToViewTheSuccessMessageForForgotPassword()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to view the success message for forgot password", new string[] {
                        "Signin"});
#line 194
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 195
 testRunner.When("I click on Forgot your Password link in sign In page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 196
 testRunner.Then("I should see enter registered email text field in forgot password page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 197
  testRunner.And("I enter \"testuser@test.com\" in enter registered email field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 198
 testRunner.Then("I verify \"An email sent with password, login the application using new password!\"" +
                    " as success message", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to navigate to signup page on clicking Register for accoun" +
            "t link")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        public virtual void VerifyIfUserIsAbleToNavigateToSignupPageOnClickingRegisterForAccountLink()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to navigate to signup page on clicking Register for accoun" +
                    "t link", new string[] {
                        "Signin"});
#line 203
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 204
   testRunner.Then("I verify Sign in page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 205
   testRunner.When("I click on Register Account link", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 206
   testRunner.Then("I verify \"WELCOME TO SKINALLIANCE\" as heading in signup page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 207
   testRunner.Then("I verify \"New User Registration\" as heading in signup page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is not able to access footer links and popup is displayed")]
        [NUnit.Framework.CategoryAttribute("Signin")]
        [NUnit.Framework.TestCaseAttribute("Terms of Use", null)]
        [NUnit.Framework.TestCaseAttribute("Legal", null)]
        [NUnit.Framework.TestCaseAttribute("Cookie and Privacy Policy", null)]
        public virtual void VerifyIfUserIsNotAbleToAccessFooterLinksAndPopupIsDisplayed(string footer_Link, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "Signin"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is not able to access footer links and popup is displayed", @__tags);
#line 210
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 211
 testRunner.Then("I verify Sign in page is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 212
  testRunner.When(string.Format("I click \"{0}\" link from footer", footer_Link), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 213
  testRunner.Then("I verify a login to access popup is displayed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 214
  testRunner.Then("I should see close button in popup", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify if user is able to change country")]
        public virtual void VerifyIfUserIsAbleToChangeCountry()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify if user is able to change country", ((string[])(null)));
#line 222
this.ScenarioSetup(scenarioInfo);
#line 3
 this.FeatureBackground();
#line 223
 testRunner.When("I click language dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 224
  testRunner.And("I select a country from dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 225
 testRunner.Then("I verify the country flag is changed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
