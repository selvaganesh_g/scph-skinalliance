﻿Feature: Verify the UI and functionality of Congresses Category Listing page

Background: 
Given I launch Skin Home page
  When I enter "testloreal@test.com" in email text field
   And I enter "Testuser123$" in password field
   


 @congressCategoryListing 
Scenario: Verify if the user is able to view the categories in Congresses dropdown Congresses Category listing page 
 When I hover on a "CONGRESSES" in navigation bar
 Then I verify "DERMATOLOGY" subcategory in dropdown
 Then I verify "ESTHETIC" subcategory in dropdown
 Then I verify "RESEARCH" subcategory in dropdown
 
@congressCategoryListing 
Scenario Outline: Verify if the user is able to view the congresses event listing page
 When I click "CONGRESSES" menu from navigation bar
 When I click on View all link of "<Sub_Category>"
 #When I click on "<Sub_Category>" from dropdown 
 Then I verify "<Sub_Category>" congresses event listing page
 Examples: 
 | Sub_Category |
 | DERMATOLOGY  |
 | ESTHETIC     |
 | RESEARCH     |
 
 
 @congressCategoryListing 
Scenario: Verify if the user is able to navigate to congresses category listing page
 When I click "CONGRESSES" menu from navigation bar
 Then I verify congresses listing page is displayed
 Then I verify "DERMATOLOGY" category in category listing page
 Then I verify "ESTHETIC" category in category listing page
 Then I verify "RESEARCH" category in category listing page
 
 
 @congressCategoryListing 
Scenario: Verify if the user is able to view four congresses per category in category listing page
 When I click "CONGRESSES" menu from navigation bar
 Then I verify congresses listing page is displayed
 #Then I verify four congresses per category
 Then I verify date of congresses displayed
 Then I verify description of congresses displayed
 Then I verify View all button in category listing page

@congressCategoryListing 
Scenario: Verify if the user is able to navigate to Event listing page on clicking View all button
 When I click "CONGRESSES" menu from navigation bar
 Then I verify congresses listing page is displayed
 Then I verify View all button in category listing page
 And I click on a random view all link
 Then I should see Congresses event listing page

 @congressCategoryListing 
Scenario: Verify if the user is able to navigate to Congresses detail page on clicking a congresses
When I click "CONGRESSES" menu from navigation bar
 Then I verify congresses listing page is displayed
 When I click on a random congresses in category listing page
 Then I verify respective congress detail page displayed
 Then I verify header and footer navigations are displayed