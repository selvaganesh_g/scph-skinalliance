 Feature: Verify UI and Functionality of My Profile Page
 
 Background: 
 Given I launch Skin Home page
  When I enter "testloreal1@test.com" in email text field
    And I enter "Testuser123$" in password field
 
 @MyProfile 
 Scenario: Verify if the user is able to navigate to "My Profile" page
    Then I verify "Userpicture" in header
	When I click on User picture in header
	Then I verify "MY PROFILE" menu in dropdown
	When I click on "MY PROFILE" in Image logo dropdown
	Then I verify "MY PROFILE" page
	
 
  @MyProfile
 Scenario: Verify if the user is able to view attributes of "My Profile" page
  When I click on User picture in header
   When I click on "MY PROFILE" in Image logo dropdown
   Then I verify "MY PROFILE" page
   Then I verify the attributes of My Profile page
       | Attributes      |
       | Profile Picture |
       | Profile Name    |
       #| Designation    |
       | Country         |
       #| Contact |
       | Email Id        |
       | Summary Section |
       | Following Section|
       | Followed By      |
       | EDIT PROFILE   |
       #|  Resume     |
  Then I verify header and footer navigations are displayed
   

 @MyProfile 
Scenario: Verify if user is able to view the Followed By section and its contents
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
 Then I verify "Following" section is displayed
 Then I verify list of users "following" the user
 Then I verify the "name" of the user following
 Then I verify the "Image" of the user following
  

  @MyProfile
Scenario: Verify if user is able to view the Following section and its contents
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
 Then I verify "Followed By" section is displayed
 Then I verify list of users "followed by" the user
 Then I verify the "name of the users" followed by the user
 Then I verify the "Image of the users" followed by the user

 
 Scenario: Verify if user is able to view Confirmation message popup to unfollow a user
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
 Then I verify "Following" section is displayed
  And I hover on a user image
 Then I verify x icon is displayed
 When I click on the x icon
 Then I verify confirmation message popup is displayed
 Then I verify "Confirm" button displayed in popup
 Then I verify "Cancel" butotn displayed in popup

 
 Scenario: Verify if user is able to click confirm button to unfollow a user
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
 Then I verify "Following" section is displayed
  And I hover on a user image
 Then I verify x icon is displayed
 When I click on the x icon
 Then I verify a confirmation message popup is displayed
 When I click "Confirm" button in popup
 Then I verify corresponding user is unfollowed

 
 Scenario: Verify if user is able to close the confirmation message popup
  When I click on User picture in header
  And I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
 Then I verify "Following" section is displayed
  And I hover on a user image
 Then I verify x icon is displayed
 When I click on the x icon
 Then I verify confirmation message popup is displayed
 When I click "Cancel" button in popup
 Then I verify confirmation message poup is not displayed 


  

 @MyProfile 
Scenario: Verify if user is able to view the Followed categories section and its contents
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
 Then I verify "Followed Categories" section is displayed
 Then I verify the "categories" followed by the user

 @MyProfile 
Scenario: Verify if user is able to view the Followed subcategories section and its contents
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
 Then I verify "Followed Sub Categories" section is displayed
 Then I verify the "Sub categories" followed by the user
  

 ########### Edit Profile ###############################
     
	 @MyProfile
 Scenario: Verify if user is view edit profile popup and user details
   When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
   When I click on "Edit" button in my profile page
   Then I verify the Edit profile popup is displayed
   Then I verify the attributes of Edit Profile page
      | Attributes |
      | User Image |
    # | Title      |
      | First Name              |
      | Last Name               |
      | Medical Speciality      |
      | Address                 |
      | Physical License Number |
      | Prefered Language       |
      | City                    |
      | Zip Code                |
      | Activity                |
      | Upload Resume           |
      | Country                 |
      | Phone                   |
      | Summary                 |
	  | Close X button          |
     
@MyProfile 
Scenario: Verify if the user is able to view the two checkboxes in edit profile page
  When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
   When I click on "Edit" button in my profile page
   Then I verify "Contact me through mail" checkbox
   Then I verify "Profile made public to SkinAlliance commmunity" checkbox
	

 @MyProfile 
Scenario: Verify if user is able to edit the user profile
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
  When I click on "Edit" button in my profile page
 Then I verify the Edit profile popup is displayed
  And I edit the fields in edit profile popup
  And I select a "lanugage" from dropdown
  And I enter a "Medical specialist" in activity field
  And I add a resume to my profile
 When I click on Submit button in edit profile popup
 Then I verify "MY PROFILE" page
  
  @Myprofile
Scenario: Verify if User is able to edit User profile picture
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
  When I click on "Edit" button in my profile page
 Then I verify the Edit profile popup is displayed
 Then I upload a new profile picture
  When I click on Submit button in edit profile popup
   Then I verify "MY PROFILE" page

   @MyProfile
Scenario: Verify if the user is able to view Validation messages on entering incorrect password
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
 Then I verify change password button
 When I click on change password button
 Then I verify Change Password popup is displayed
  And I enter "Testuser234$" in old password field
  And I enter "Testuser123@" in new password text field
  And I enter "Testuser123@" in confirm password field
  When I click on Submit button in change password popup
 Then I should see "Please enter valid Old Password" error message

 @MyProfile 
 Scenario: Verify if the user is able to view Validation message on leaving current password field empty
  When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
 Then I verify change password button
 When I click on change password button
 Then I verify Change Password popup is displayed
  And I enter "Testuser123@" in new password text field
  And I enter "Testuser123@" in confirm password field
  When I click on Submit button in change password popup
 Then I should see "Old Password Required" error message
  

  @MyProfile
 Scenario: Verify if the user is able to view Validation error message on leaving new password field blank
  When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
  Then I verify change password button
  When I click on change password button
  Then I verify Change Password popup is displayed
  And I enter "Testuser123@" in old password field
  And I enter "Testuser123$" in confirm password field
  When I click on Submit button in change password popup
  Then I should see "Please enter New Password" new password error message

  @MyProfile
Scenario: Verify if the user is able to view Validation error message on leaving confirm password field blank
  When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
  Then I verify change password button
  When I click on change password button
  Then I verify Change Password popup is displayed
  And I enter "Testuser123$" in old password field
  And I enter "Testuser12@" in new password text field
  When I click on Submit button in change password popup
  Then I wait for 5 seconds
  Then I should see "Please enter Confirm Password" confirm password error message

  @MyProfile
Scenario: Verify if the user is able to view Validation error message for password mismatch
  When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify change password button
  When I click on change password button
  Then I verify Change Password popup is displayed
  And I enter "Testuser123$" in old password field
  And I enter "Testuser123@" in new password text field
  And I enter "Test1234$" in confirm password field
  When I click on Submit button in change password popup
  Then I should see "Password and Confirm Password are not matching" confirm password error message
  

@MyProfile
Scenario: Verify if the user is able to view and change the current password
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  Then I verify "MY PROFILE" page
 Then I verify change password button
 When I click on change password button
 Then I verify Change Password popup is displayed
 Then I should see "Current Password" text field
 Then I should see "New Password" text field
 Then I should see "Confirm Password" text field
  And I enter "Testuser123$" in old password field
  And I enter "Testuser123@" in new password text field
  And I enter "Testuser123@" in confirm password field
  When I click on Submit button in change password popup
 When I click on User picture in header
  And I click on "LOGOUT" in Image logo dropdown
 Then I verify Sign in page is displayed
  When I enter "testloreal@test.com" in email text field
    And I enter "Testuser123@" in password field
 Then I verify "Navigation menu" in header
 When I click on User picture in header
  When I click on "MY PROFILE" in Image logo dropdown
  And I click on change password button
  Then I verify Change Password popup is displayed
  And I enter "Testuser123@" in old password field
  And I enter "Testuser123$" in new password text field
  And I enter "Testuser123$" in confirm password field
  When I click on Submit button in change password popup
  Then I verify "MY PROFILE" page

 
   

 
   
  
	