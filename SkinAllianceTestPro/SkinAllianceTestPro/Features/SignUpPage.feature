Feature: Sign Up page
 
 Background: 
   Given I launch Skin Home page
    When I click on the "Register for account" link
	Then I verify Skin Alliance header
 
  @signup
 Scenario: Verify if the user is able to signup for accessing the site successfully
   Then I verify "WELCOME TO SKINALLIANCE" as heading in signup page
   Then I verify "New User Registration" as heading in signup page
   Then I verify signup fields in signup page
     | Attributes                |
     | First Name                |
     | Last Name                 |
     | Country                   |
     | Select Medical Speciality |
     | Physical License number   |
     | Preferred Language        |
     | Password                  |
     | Password Confirmation     |
     | Submit Button             |
  

   @signup
Scenario: Verify if the user redirects to external site on clicking the "Skin Alliance" partner's logo or names
    When I click on a random Skin Alliance Partner logo
   Then I verify a login to access popup is displayed
    

  @signup 
  Scenario: Verify if the user is able to view Social login icons
   Then I verify "Register with Facebook" link is displayed
   Then I verify "Register with Twitter" link is displayed
   Then I verify "Register with LinkedIn" link is displayed
    
	@signup
 Scenario: Verify if the user is able to view First name field validation message
  When I enter "user" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
    And I click on Submit button
   Then I verify "Please enter First Name" as error message in signup page
    
	@signup
 Scenario: Verify if the user is able to view Last name field validation message
  When I enter "test" in First name field
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
    And I click on Submit button
   Then I verify "Please enter Last Name" as error message in signup page
    

@signup
Scenario: Verify if the user is able to view Country field validation message
   When I enter "test" in First name field
  And I enter "user" as last name in signup page
  And I enter email address in email address field
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
    And I click on Submit button
   Then I verify "Please select Country" as error message in signup page

   @signup
Scenario: Verify if the user is able to view Empty Email field validation message
   When I enter "test" in First name field
  And I enter "user" as last name in signup page
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
    And I click on Submit button
   Then I verify "Please enter email address" as error message in signup page
    

@signup
Scenario: Verify if the user is able to view invalid email id field validation message
   When I enter "test" in First name field
  And I enter "user" as last name in signup page
  And I enter "test@test@com" in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
    And I click on Submit button
   Then I verify "Please enter valid email address" as error message in signup page
    

   
 Scenario: Verify if the user is able to view Physician license field validation message
   When I enter "test" in First name field
  And I enter "user" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
    And I click on Submit button
   Then I verify "Please enter Physician License" as error message in signup page
    

	@signup 
 Scenario: Verify if the user is able to view Medical Speciality field validation message
   When I enter "test" in First name field
  And I enter "user" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1213534" as Physical License number
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
    And I click on Submit button
   Then I verify "Please Select Medical Specialty" as error message in signup page
    


  @signup
Scenario: Verify if the user is able to view Password field validation message
   When I enter "test" in First name field
  And I enter "user" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" in confirm password field
    And I click on Submit button
   Then I verify "Please enter password" as error message in signup page
    

	@signup
Scenario: Verify if the user is able to view Confirm Password field validation message
   When I enter "test" in First name field
  And I enter "user" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I click on Submit button
   Then I verify "Please enter Confirm password" as error message in signup page
    

	@signup
Scenario: Verify if the user is able to view Password mismatch error message
 When I enter "test" in First name field
  And I enter "user" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "test#say" in confirm password field
  And I click on Submit button
 Then I verify "Confirm password is not matching the password" as error message in signup page

   @signup
 Scenario: Verify if the user is able to view invalid Password error message
 When I enter "test" in First name field
  And I enter "user" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "testuser" as password
  And I enter "testuser" in confirm password field
  And I click on Submit button
 Then I verify "Password must be contain at least eight characters, at least one number and both lower and uppercase letters and special characters." as error message in signup page
  
   

   @signup
Scenario: Verify if the user is able to create a user account successfully
 When I enter "test" in First name field
  And I enter "user" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
  And I click on Submit button
 Then I should see Registration confirmation page
   


  @signup
 Scenario: Verify the Registration Confirmation page on successful user creation
   Then I verify Skin Alliance header
   When I enter "test1" in First name field
  And I enter "user2" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
  And I click on Submit button       
  Then I should see Registration confirmation page
  Then I verify the "Thank You for Registering!" success message
  Then I verify Back to Login button in confirmation page
   Then I verify Skin Alliance logo
   Then I verify header and footer navigations are displayed
    And I click on Back to Login button
   Then I verify Sign in page is displayed

   @signup
 Scenario: Verify the Edit profile displayed on clicking complete profile button in Registration confirmation page
  Then I verify Skin Alliance header
   When I enter "test1" in First name field
  And I enter "user2" as last name in signup page
  And I enter email address in email address field
  And I select a random country from country dropdown
  And I enter "1224123123" as Physical License number
  And I select medical speciality
  And I select preferred language
  And I enter "Testuser123$" as password
  And I enter "Testuser123$" in confirm password field
  And I click on Submit button       
  Then I should see Registration confirmation page
  Then I verify the "Thank You for Registering!" success message
  Then I verify Back to Login button in confirmation page
   Then I verify Skin Alliance logo
   Then I verify header and footer navigations are displayed
   And I click on Complete profile button
   Then I verify welcome message popup and close it
   Then I verify "MY PROFILE" page
   And I click on no longer wish to see these messages checkbox
   And I edit the fields in edit profile popup
  And I select a "lanugage" from dropdown
  And I enter a "Medical specialist" in activity field
  And I add a resume to my profile
 When I click on Submit button in edit profile popup
 Then I verify "MY PROFILE" page
  
    

	@signup 
 Scenario: Verify if user is able to create a user using facebook account
   Then I verify "Register with Facebook" link is displayed
    And I click on facebook button
   Then I verify "facebook" login page is displayed
  When I enter "testuserskinalliance@gmail.com" in facebook email field
   And I enter "Testinguser123$" in facebook password field
   And I click facebook login button
   And I select a random country from country dropdown
   And I enter "1224123123" as Physical License number
   And I select medical speciality
   And I select preferred language
   And I click on Submit button
   Then I should see navigaton menu bar in page
   
 
    @signup
 Scenario: Verify if user is able to create a user using twitter account
   Then I verify "Register with Twitter" link is displayed
    And I click on twitter button
  Then I verify "twitter" login page is displayed
   And I enter "TesterAlliance" as username in twitter
   And I enter "Testuser123$" as password in twitter
   And I click Authorizeapp button in twitter
   When I select a random country from country dropdown
   And I enter "1224123123" as Physical License number
   And I select medical speciality in twitter registration page
   And I click on Submit button
   Then I verify welcome message popup and close it
  Then I should see navigaton menu bar in page

  @signup
  Scenario: Verify if user is able to create a user using Linkedin account
   Then I verify "Register with LinkedIn" link is displayed
    And I click on Linkedin button
  Then I verify "Linkedin" login page is displayed
   And I enter "testuserskinalliance@gmail.com" as username in linkedin
   And I enter "Testinguser123$" as password in linkedin
   And I click Authorizeapp button in linkedin
   When I select a random country from country dropdown
   And I enter "1224123123" as Physical License number
   And I select medical speciality
   And I click on Submit button
   Then I verify welcome message popup and close it
  Then I should see navigaton menu bar in page
 

 @signup
 Scenario Outline: Verify if user is not able to access footer links and popup is displayed in signup page
  When I click "<Footer_link>" link from footer
  Then I verify a login to access popup is displayed
  Then I should see close button in popup
  Examples: 
  |Footer_link|
  |Terms of Use|
  |Legal|
  |Cookie and Privacy Policy|