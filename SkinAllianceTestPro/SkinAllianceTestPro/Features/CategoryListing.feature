﻿Feature: Verify UI and Functionality of CategoryListing Page
 
 Background: 
 Given I launch Skin Home page
  When I enter "testloreal@test.com" in email text field
   And I enter "Testuser123$" in password field
   
 

@categorylisting
Scenario Outline: Verify if the user is able to view the Category Listing page and its contents
Then I verify "Navigation menu" in header
When I click "<Nav_Menu>" menu from navigation bar
 Then I verify Category listing page with title is displayed
 Then I verify Category names are displayed
  And I verify Sub categories are displayed
  Then I verify header and footer navigations are displayed
  Then I verify "Navigation menu" in header
  Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS        |

  
 @categorylisting 
Scenario Outline: Verify if the user is able to navigate to Sub category listing page
Then I verify "Navigation menu" in header
When I click "<Nav_Menu>" menu from navigation bar
Then I verify "Navigation menu" in header
 When I click on a random category name
 Then I verify corresponding sub category listing page is displayed
 Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS        |
  

 @categorylisting
Scenario Outline: Verify if the user is able to navigate to Article listing page
Then I verify "Navigation menu" in header
When I click "<Nav_Menu>" menu from navigation bar
Then I verify "Navigation menu" in header
When I click on a random sub category name
And I verify sub category heading in article listing page
And I verify articles are displayed
  Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS         |
     
 @categorylisting
Scenario Outline: Verify if the user is able to view breadcrumb and access it
Then I verify "Navigation menu" in header
When I click "<Nav_Menu>" menu from navigation bar
Then I verify "Navigation menu" in header
 Then I verify breadcrumb trail is displayed
  And I click on previous link in breadcrumb trail in Category listing page
 Then I verify home page is displayed
 Examples: 
  | Nav_Menu                 |
  | DERMOCOSMETICS KNOWLEDGE |
  | EXPERTS COMMENTS        |
  

 