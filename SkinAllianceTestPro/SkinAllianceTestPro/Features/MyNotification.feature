Feature: Verify UI and Functionality of My Dashboard Page
 
  Background: 
 Given I launch Skin Home page
  When I enter "saqatest@gmail.com" in email text field
   And I enter "Saqatest@123" in password field

@MyNotification 
 Scenario: Verify if the user is able to navigate to "My Notification" page 
	 When I click on User picture in header
    And I click on "MY NOTIFICATION" in Image logo dropdown
	Then I verify "MY NOTIFICATIONS" as heading in the page
	
@MyNotification 
Scenario: Verify if the user is able to view Timeline notifications and Manage my notification button
 When I click on User picture in header
  And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify "MY NOTIFICATIONS" as heading in the page
 Then I verify timeline notifications section in my notification page
 Then I verify Manage my notification button is displayed

 @MyNotification  
Scenario: Verify if the user is able to view list of timeline notification messages in My Notification page
 When I click on User picture in header
  And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify the timeline notifications messages

 
Scenario: Verify if the user is able to view 10 timeline notifications in timeline notification section
 When I click on User picture in header
  And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify "10" notifications are displayed

 
Scenario: Verify if the user is able to view and click Loading more button
 When I click on User picture in header
  And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify "10" notifications are displayed
  And I click Loading more button
 Then I verify more 8 notifications are displayed
 Then I verify 18 notifications are displayed in total


Scenario Outline: Verify if the user is able to view  Filter notification section and its various options
 When I click on User picture in header
 And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify "Filter Notifications:" section in my notifcation page
 Then I verify "<filter_options>" in filter notification
Examples:
   | filter_options                |
   | Like                          |
   | Comment                       |
   | New Post in Followed Category |
   | New post by Contact           |
   | Contact Followed a User       |

 
Scenario Outline: Verify if the user is able to filter the notificaton based on selecting different options
 When I click on User picture in header
  And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify "Filter Notifications:" section in my notifcation page
 When I click "<filter_options>" in filter notification
 Then I should see notifications based on "<filter_options>" selection
 Examples: 
 | filter_options                |
 | Like                          |
 | Comment                       |
 | New Post in Followed Category |
 | New post by Contact           |
 | Contact Followed a User       |

  @MyNotification
 Scenario Outline: Verify if the user is able to view and manage my notification section
 When I click on User picture in header
 And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify Manage my notification button is displayed
 When I click on "MANAGE NOTIFICATIONS" in my notification page
 Then I verify the manage notification popup is displayed
 Then I verify close button in manage my notification popup
 Then I verify "<Attributes>"notifications in the popup
 Examples: 
        | Attributes                                       |
        | One of your contacts is now following a new user |
        | New content in a followed category               |
        | Someone interacts with your post                 |
        | New comment in a post shared,liked or commented  |
        | One of your contacts add a new post              |
    

@MyNotification 
Scenario: Verify if the user is able to select the dropdown options immediate in manage my notification popup
 When I click on User picture in header
 And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify Manage my notification button is displayed
 When I click on "MANAGE NOTIFICATIONS" in my notification page
 Then I verify the manage notification popup is displayed
 Then I click Immediate dropdown in the popup
 Then I click on Save button

@MyNotification 
Scenario: Verify if the user is able to select the dropdown options daily in manage my notification popup
 When I click on User picture in header
 And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify Manage my notification button is displayed
 When I click on "MANAGE NOTIFICATIONS" in my notification page
 Then I verify the manage notification popup is displayed
 Then I click Daily dropdown in the popup
 Then I click on Save button

@MyNotification 
Scenario: Verify if the user is able to select the dropdown options weekly in manage my notification popup
 When I click on User picture in header
 And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify Manage my notification button is displayed
 When I click on "MANAGE NOTIFICATIONS" in my notification page
 Then I verify the manage notification popup is displayed
 Then I click Weekly dropdown in the popup
 Then I click on Save button

 @MyNotification
Scenario: Verify if the user is able to select the dropdown options never in manage my notification popup
 When I click on User picture in header
 And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify Manage my notification button is displayed
 When I click on "MANAGE NOTIFICATIONS" in my notification page
 Then I verify the manage notification popup is displayed
 Then I click Never dropdown in the popup
 Then I click on Save button


@MyNotification
Scenario: Verify One of your contacts is now following a new user
 When I click on User picture in header
 And I click on "LOGOUT" in Image logo dropdown
 When I enter "testloreal@test.com" in email text field
 And I enter "Testuser123$" in password field
 When I hover on a "Community" in navigation bar
  And I select "Directory" from community dropdown
  When I click a random user name in directory page
  Then I verify corresponding userprofile page is displayed
 When I click on follow button in user profile page
 Then I verify "UNFOLLOW" as button name in user profile page
  When I click on User picture in header
 And I click on "LOGOUT" in Image logo dropdown
  When I enter "saqatest@gmail.com" in email text field
 And I enter "Saqatest@123" in password field
 When I click on User picture in header
  And I click on "MY NOTIFICATION" in Image logo dropdown
 Then I verify the timeline notifications messages





   
   
 
  
 
   
   
  