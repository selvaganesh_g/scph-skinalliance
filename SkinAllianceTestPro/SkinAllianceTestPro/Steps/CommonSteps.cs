﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using SkinAllianceTestPro.Pages;
using System.Threading;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Service;
using System.Data;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Framework.Common;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class CommonSteps
    {
        
        private readonly ScenarioContext scenarioContext;
        private CommonPage commonPage;
        //public LoginPage loginPage;
        

        public CommonSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            commonPage = new CommonPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }


        //[Given(@"I launch (.+) page")]
        //public void LaunchPage(string pageName)
        //{
            
        //    commonPage.Browser.NavigateTo(pageName);
        //    System.Threading.Thread.Sleep(10000);

        //}

        [When(@"I click on Menu")]
        public void ClickOnMenu()
        {
            commonPage.Browser.Click(ElementUtil.GetName(""));

        }

        [When(@"I click on button (.+)")]
        public void ClickOnButton(string buttonName)
        {
            commonPage.Browser.Click(ElementUtil.GetName(buttonName));
        }

        [When(@"I click on link (.+)")]
        public void ClickOnLink(string linkName)
        {
            commonPage.Browser.Click(ElementUtil.GetName(linkName));

        }

        [When(@"I click on image (.+)")]
        public void ClickOnImage(string imageName)
        {
            commonPage.Browser.Click(ElementUtil.GetName(imageName));

        }

        [Then(@"I wait for (.+) seconds")]
        public void WaitForSeconds(int seconds)
        {
            Thread.Sleep(seconds);

        }

        [Then(@"I expect the page title as (.+)")]
        public void verifyPageTitle(string title)
        {
            commonPage.validatePageTitle(title);

        }

        [Then(@"I clear all cookies")]
        public void clearAllCookies()
        {
            commonPage.Browser.DeleteAllCookies();

        }





        //[Then(@"(I expect the page url contains (.+)")]
        //public void verifyPageUrlContains(string partialUrl)
        //{


        //}


        //[Then(@"I expect the (.+) block contains (.+)")]
        //public void verifyBlockContains(string partialUrl)
        //{


    }
}
