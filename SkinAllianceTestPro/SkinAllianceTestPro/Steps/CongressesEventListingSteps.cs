﻿using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using NUnit.Framework;
using System.Threading;
using SkinAllianceTestPro.Block.ElementBlocks;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class CongressesEventListingSteps
    {
        private readonly ScenarioContext scenarioContext;

        private CongressesEventListingPage congressesEventListPage;

        private HomePage homePage;

        private ArticleListingPage articleListing;


        public static string SelectedCategoryName;

        public static string SelectedSubCategoryName;

        public CongressesEventListingSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            congressesEventListPage = new CongressesEventListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            homePage = new HomePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            articleListing = new ArticleListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }


        [Then(@"I verify ""(.*)"" in event listing page")]
        public void ThenIVerifyInEventListingPage(string Element)
        {
            congressesEventListPage.VerifyElementsinPage(Element);
        }

        [When(@"I click on a random congresses from event listing page")]
        public void WhenIClickOnARandomCongressesFromEventListingPage()
        {
            congressesEventListPage.ClickRandomCongressName();
        }

        



    }
}