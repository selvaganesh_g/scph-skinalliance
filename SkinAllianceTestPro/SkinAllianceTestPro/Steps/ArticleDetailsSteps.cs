﻿using NUnit.Framework;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Block.ElementBlocks;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class ArticleDetailsSteps
    {
        private readonly ScenarioContext scenarioContext;
        private ArticleDetailsPage articleDetails;
        private FooterPage footerPage;
        private HomePage homepage;
        private ArticleListingPage articleListing;

        public ArticleDetailsSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            articleDetails = new ArticleDetailsPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            articleListing = new ArticleListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }

        [Then(@"I click on previous link in breadcrumb trail in article details page")]
        public void ThenIClickOnPreviousLinkInBreadcrumbTrailInArticleDetailsPage()
        {
            articleListing.ClickPreviousBreadcrumb();   
        }



        [Then(@"I verify ""(.*)"" in article details page")]
        public void ThenIVerifyInArticleDetailsPage(string Attributes)
        {
            articleDetails.GetArticleDetailContents(Attributes);
        }

        [Then(@"I verify Comments Posted by other users in article details page")]
        public void ThenIVerifyCommentsPostedByOtherUsersInArticleDetailsPage()
        {
            Thread.Sleep(3000);
            Assert.IsTrue(articleDetails.Browser.GetElements(ArticleDetailsElement.ARTICLE_COMMENTS).Count >= 1, "Comments posted by other users are not displayed");
        }

        [Then(@"I Enter a reply for a random comment")]
        public void ThenIEnterAReplyForARandomComment()
        {
            articleDetails.PostReplyForComment();
        }

        [Then(@"I verify the reply is posted")]
        public void ThenIVerifyTheReplyIsPosted()
        {
            articleDetails.VerifyPostedReply();
        }



        [Then(@"I verify ""(.*)"" button in article details page")]
        public void ThenIVerifyButtonInArticleDetailsPage(string button)
        {
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.DOWNLOAD_BUTTON).Displayed, button+ "is not displayed");
        }

        [Then(@"I verify ""(.*)"" of comments posted by other users")]
        public void ThenIVerifyOfCommentsPostedByOtherUsers(string Element)
        {
            {
                if (Element == "Image")
                {
                    articleDetails.GetCommentsImage();
                }
                else if (Element == "Username")
                {
                    articleDetails.GetCommentsUserName();
                }
                
            }

        }

        [Then(@"I verify Posted Date posted by other users")]
        public void ThenIVerifyPostedDatePostedByOtherUsers()
        {
            articleDetails.GetCommentsPostedDate();
        }


        [Then(@"I Post a comment in article details page")]
        public void ThenIPostACommentInArticleDetailsPage()
        {
            articleDetails.PostComment();
        }

        [Then(@"I verify the corresponding comment is posted")]
        public void ThenIVerifyTheCorrespondingCommentIsPosted()
        {
            
        }


        [Then(@"I verify Like icon is displayed")]
        public void ThenIVerifyLikeIconIsDisplayed()
        {
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.ARTICLEDETAIL_LIKE_ICON).Displayed, "Like icon is not displayed");
        }

        [Then(@"I click the Like icon of the article")]
        public void ThenIClickTheLikeIconOfTheArticle()
        {
            articleDetails.Browser.GetElement(ArticleDetailsElement.ARTICLEDETAIL_LIKE_ICON).Click();
            articleDetails.Browser.GetElement(ArticleDetailsElement.ARTICLEDETAIL_LIKE_ICON).GetAttribute("i");
        }

        [Then(@"I verify the no of likes for the article")]
        public void ThenIVerifyTheNoOfLikesForTheArticle()
        {
            Thread.Sleep(4000);
            var LikesCount = articleDetails.Browser.GetElements(ArticleDetailsElement.COMMENT_SEND_BUTTON)[0].Text.Split(' ');
            Assert.IsTrue(LikesCount[1].Contains(")"), LikesCount + "is not displayed");
            
        }

        [Then(@"I click on a random Webcast article")]
        public void ThenIClickOnARandomWebcastArticle()
        {
            articleDetails.ClickRandomWebcast();
        }

        [Then(@"I should see download button")]
        public void ThenIShouldSeeDownloadButton()
        {
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.DOWNLOAD_BUTTON).Displayed, "Download button is not displayed");
        }

        [Then(@"I click download button")]
        public void ThenIClickDownloadButton()
        {
            articleDetails.Browser.Click(ArticleDetailsElement.DOWNLOAD_BUTTON);
        }

        [Then(@"I verify a copyright popup is displayed")]
        public void ThenIVerifyACopyrightPopupIsDisplayed()
        {
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.COPYRIGHT_POPUP).Displayed, "Copyright popup is not displayed");
        }

        [Then(@"I verify Webcast details page is displayed")]
        public void ThenIVerifyWebcastDetailsPageIsDisplayed()
        {
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.WEBCASTS_SECTION).Displayed, "webcasts detail page is not displayed");
        }




        [Then(@"I should not see Access the Website button")]
        public void ThenIShouldNotSeeAccessTheWebsiteButton()
        {
            Assert.False(articleDetails.Browser.GetElement(ArticleDetailsElement.ACCESS_WEBSITE_LINK).Displayed, "Access the website button is not displayed");
        }




        [Then(@"I verify Report to Moderator link is displayed")]
        public void ThenIVerifyReportToModeratorLinkIsDisplayed()
        {
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.REPORT_MODERATOR_LINK).Displayed, "Report to moderator link is not displayed");
        }


        [Then(@"I click on Report to Moderator link of article")]
        public void ThenIClickOnReportToModeratorLinkOfArticle()
        {
            articleDetails.Browser.Click(ArticleDetailsElement.REPORT_MODERATOR_LINK);
        }

        [Then(@"I click on Play the Webcast link")]
        public void ThenIClickOnPlayTheWebcastLink()
        {
            Thread.Sleep(4000);
            articleDetails.Browser.Click(ArticleDetailsElement.PLAY_WEBCAST_LINK);
        }


        [Then(@"I click on Play button in popup")]
        public void ThenIClickOnPlayButtonInPopup()
        {
            Thread.Sleep(3000);
            articleDetails.Browser.Click(ArticleDetailsElement.WEBCAST_VIDEO_PLAY);
        }

        [Then(@"I click on the slides to select the topic")]
        public void ThenIClickOnTheSlidesToSelectTheTopic()
        {
            articleDetails.SelectRandomSlides();
        }

        [Then(@"I click the close link in webcast popup")]
        public void ThenIClickTheCloseLinkInWebcastPopup()
        {
            articleDetails.Browser.Click(ArticleDetailsElement.WEBCAST_CLOSE_BUTTON);
        }

        [Then(@"I click download button in webcast details page")]
        public void ThenIClickDownloadButtonInWebcastDetailsPage()
        {
            articleDetails.Browser.Click(ArticleDetailsElement.DOWNLOAD_BUTTON);
        }

        [Then(@"I should see Copyright alert message")]
        public void ThenIShouldSeeCopyrightAlertMessage()
        {
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.COPYRIGHT_POPUP).Displayed, "Copyright message popup is not displayed");
        }

        [Then(@"I verify the popup window is closed")]
        public void ThenIVerifyThePopupWindowIsClosed()
        {
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.PLAY_WEBCAST_LINK).Displayed, "Popup window is not closed");
        }




        [Then(@"I verify video is displayed in a window popup")]
        public void ThenIVerifyVideoIsDisplayedInAWindowPopup()
        {
           string VideoStatus = articleDetails.Browser.GetElement(ArticleDetailsElement.WEBCAST_VIDEO_PLAY).GetAttribute("title");
            //Thread.Sleep(5000);
            //articleDetails.Browser.HoverElement(ArticleDetailsElement.WEBCAST_VIDEO_PLAY);
            Assert.IsTrue(VideoStatus.Contains("Pause"), "Expected is:" +VideoStatus+ "Actual is: Play"); 
        }



        [Then(@"I verify message ""(.*)"" is displayed in popup")]
        public void ThenIVerifyMessageIsDisplayedInPopup(string errorMessage)
        {
            articleDetails.VerifyReportModeratorMessage(errorMessage);
        }

        [Then(@"I verify close button is displayed in popup")]
        public void ThenIVerifyCloseButtonIsDisplayedInPopup()
        {
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.CLOSE_BUTTON_MODERATOR).Displayed,
                "Close button is not displayed in the Report to moderator popup");
        }

        [Then(@"I click close button in report to moderator popup")]
        public void ThenIClickCloseButtonInReportToModeratorPopup()
        {
            articleDetails.Browser.Click(ArticleDetailsElement.CLOSE_BUTTON_MODERATOR);
        }

        [Then(@"I verify Related Post section is displayed")]
        public void ThenIVerifyRelatedPostSectionIsDisplayed()
        {
            Assert.IsTrue(articleDetails.VerifyRelatedPost(), "Related Post section is displayed");
        }

        [Then(@"I should see ""(.*)"" button")]
        public void ThenIShouldSeeButton(string button)
        {
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.ACCESS_WEBSITE_LINK).Displayed, button + "is not displayed");
        }

        [Then(@"I click on ""(.*)"" button")]
        public void ThenIClickOnButton(string button)
        {
            articleDetails.Browser.Click(ArticleDetailsElement.ACCESS_WEBSITE_LINK);
        }

        [Then(@"I verify video player page in new tab")]
        public void ThenIVerifyVideoPlayerPageInNewTab()
        {
            articleDetails.VerifyExternalVideoTab();
        }




        [Then(@"I verify ""(.*)"" is displayed in related post")]
        public void ThenIVerifyIsDisplayedInRelatedPost(string Element)
        {
            for (int i = 0; i < articleDetails.GetRelatedPostComments().Count; i++)
            {
                if(Element == "Profile Pic")
                {
                    Assert.IsTrue(articleDetails.GetRelatedPostPicture()[i].Displayed, Element + "is not displayed");
                }
                else if(Element == "User Name")
                {
                    articleDetails.GetRelatedPostUser();
                }
                else if(Element == "Likes")
                {
                    Assert.IsTrue(articleDetails.GetRelatedPostLikes()[i].Displayed, Element + "is not displayed");
                }
                else if(Element == "Comments")
                {
                    Assert.IsTrue(articleDetails.GetRelatedPostComments()[i].Displayed, Element + "is not displayed");
                }
                else if(Element == "No of likes")
                {
                    Assert.IsTrue(articleDetails.GetRelatedPostLikesCount()[i].Displayed, Element + "is not displayed");
                }
                else if(Element == "No of comments")
                {
                    Assert.IsTrue(articleDetails.GetRelatedPostCommentsCount()[i].Displayed, Element + "is not displayed");
                }
                else
                {
                    Assert.Fail("Related post section is not displayed");
                }

            }
        }

        [Then(@"I click on a User Name link in related post")]
        public void ThenIClickOnAUserNameLinkInRelatedPost()
        {
            articleDetails.ClickRandomRelatedPost();
        }


    }
}
