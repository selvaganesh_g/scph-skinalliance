﻿using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using NUnit.Framework;
using System.Threading;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class CateogryListingSteps
    {
        private readonly ScenarioContext scenarioContext;

        private CategoryListingPage catListingPage;

        private HomePage homePage;

        private ArticleListingPage articleListing;

       
        public static string SelectedCategoryName;

        public static string SelectedSubCategoryName;

        public CateogryListingSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            catListingPage = new CategoryListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            homePage = new HomePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            articleListing = new ArticleListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }

        [When(@"I navigate to a random category listing page")]
        public void WhenINavigateToARandomCategoryListingPage()
        {
            homePage.SelectRandomCategory();
        }

        [Then(@"I verify Category listing page with title is displayed")]
        public void ThenIVerifyCategoryTitleIsDisplayed()
        {
            Thread.Sleep(3000);
            IWebElement ele = catListingPage.Browser.GetElement(CategoryListingElements.PAGE_TITLE);

            Assert.IsTrue(ele.Displayed, "Category title is not displayed");
        }

        [Then(@"I verify breadcrumb trail is displayed")]
        public void ThenIVerifyBreadcrumbTrailIsDisplayed()
        {
            IWebElement breadcrumb = catListingPage.Browser.GetElement(CategoryListingElements.BREADCRUMB_TRAIL);
            Assert.IsTrue(breadcrumb.Displayed, "Breadcrumb trail is not displayed");
        }

        [Then(@"I click on previous link in breadcrumb trail in Category listing page")]
        public void ThenIClickOnPreviousLinkInBreadcrumbTrailInCategoryListingPage()
        {
            catListingPage.Browser.Click(CategoryListingElements.PREV_BREADCRUMB);
        }


        [When(@"I click on follow button in category listing page")]
        public void WhenIClickOnFollowButtonInCategoryListingPage()
        {
            
            catListingPage.Browser.Click(CategoryListingElements.CATEGORY_FOLLOW_BUTTON);
            
        }

        [Then(@"I verify ""(.*)"" as button name in category listing page")]
        public void ThenIVerifyAsButtonNameInCategoryListingPage(string buttonName)
        {
            string ButtonDisplayed = catListingPage.Browser.GetElement(CategoryListingElements.CATEGORY_FOLLOW_BUTTON).Text;
            if (buttonName == "UNFOLLOW")
            {
                Assert.IsTrue(ButtonDisplayed.Contains("UNFOLLOW"), buttonName + "is not displayed");
            }
            else
            {
                Assert.IsTrue(ButtonDisplayed.Contains("FOLLOW"), buttonName + "is not displayed");
            }
        }



        [Then(@"I verify Category names are displayed")]
        public void ThenIVerifyCategoryNamesAreDisplayed()
        {
            Assert.IsTrue(catListingPage.GetCategoryNames().Count >= 1, "Category Names are not displayed");
        }

        [Then(@"I verify Sub categories are displayed")]
        public void ThenIVerifySubCategoriesAreDisplayed()
        {
            Assert.IsTrue(catListingPage.GetSubCategoryNames().Count >= 1, "Sub Category Names are not displayed");
        }

        [When(@"I click on a random category name")]
        public void WhenIClickOnARandomCategoryName()
        {
            Thread.Sleep(5000);
             catListingPage.ClickRandomCategoryName();
            
        }

        [Then(@"I click Topical subcategory in categorylisting page")]
        public void ThenIClickTopicalSubcategoryInCategorylistingPage()
        {
            catListingPage.Browser.Click(CategoryListingElements.TOPICAL_CATEGORY);
        }


        [When(@"I click on a random sub category name")]
        public void WhenIClickOnARandomSubCategoryName()
        {
            Thread.Sleep(5000);
            SelectedSubCategoryName = catListingPage.ClickRandomSubCategory();
        }

        [Then(@"I verify corresponding sub category listing page is displayed")]
        public void ThenIVerifyCorrespondingSubCategoryListingPageIsDisplayed()
        {
            Thread.Sleep(4000);
            catListingPage.VerifySubCategoryName();
            
        }

        [When(@"I verify sub category heading in article listing page")]
        public void WhenIVerifySubCategoryHeadingInArticleListingPage()
        {
            Thread.Sleep(4000);
          string pageTitle =  articleListing.GetArticleTitle();
            Assert.IsTrue(SelectedSubCategoryName.Equals(pageTitle, StringComparison.InvariantCultureIgnoreCase), "Expected is:" +pageTitle+ "Actual is: " +SelectedCategoryName);
        }

        [When(@"I verify articles are displayed")]
        public void WhenIVerifyArticlesAreDisplayed()
        {
            articleListing.GetArticleContents();


        }

        [Then(@"I click on previous link in breadcrumb trail")]
        public void ThenIClickOnHomeIconInBreadcrumbTrail()
        {
            catListingPage.Browser.Click(CategoryListingElements.PREV_BREADCRUMB);
        }



    }

}