﻿using NUnit.Framework;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Block.ElementBlocks;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class MyNotificationSteps
    {

        private readonly ScenarioContext scenarioContext;
        private MyNotificationPage myNotificationpage;
        

        public MyNotificationSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            myNotificationpage = new MyNotificationPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }

  
        [Then(@"I verify timeline notifications section in my notification page")]
        public void ThenIVerifytimelinenotificationssectioninmynotificationpage()
        {
            myNotificationpage.VerifyTimeLineNotification();
        }

        [Then(@"I verify Manage my notification button is displayed")]
        public void ThenIVerifyManageMyNotificationButtonIsDisplayed()
        {
            Assert.IsTrue(myNotificationpage.Browser.GetElement(MyNotificationElement.MANAGE_NOTIFICATION_SECTION).Displayed, "Manage Notification is not displayed");
        }
       


    [Then(@"I verify the timeline notifications messages")]
        public void ThenIVerifyTheTimelineNotificationsMessages()
        {
            myNotificationpage.VerifyTimeLineMessages();

        }

        [Then(@"I verify ""(.*)"" section in my notifcation page")]
        public void ThenIVerifySectionInMyNotifcationPage(string Section)
        {
                Assert.IsTrue(myNotificationpage.Browser.GetElement(MyNotificationElement.FILTER_SECTION).Displayed, "FILTER Section is not displayed");
        }


        [Then(@"I verify manage notification button")]
        public void ThenIVerifyManageNotificationButton(string button)
        {
            Assert.IsTrue(myNotificationpage.Browser.GetElement(MyNotificationElement.MANAGE_BUTTON).Displayed, "Manage button is not displayed");

        }
        
        [When(@"I click on ""(.*)"" in my notification page")]
        public void WhenIClickOnInMyNotificationPage(string button)
        {
            Thread.Sleep(5000);
            myNotificationpage.Browser.Click(MyNotificationElement.MANAGE_BUTTON);
            Console.WriteLine("Successfully clicked Manage button");
        }

        [Then(@"I verify below options in filter notification section")]
        public void ThenIVerifyBelowOptionsInFilterNotificationSection(string sectionDisplayed)
        {
            Thread.Sleep(5000);
            myNotificationpage.VerifyFilterNotificationOption(sectionDisplayed);
            // Console.WriteLine("Filter notification Section is displayed properly");
        }



        [Then(@"I verify the manage notification popup is displayed")]
        public void ThenIVerifyTheMangeNotificationPopupIsDisplayed()
        {
            Thread.Sleep(5000);
            Assert.IsTrue(myNotificationpage.Browser.GetElement(MyNotificationElement.MANAGE_POPUP).Displayed, "Manage Notificaiton popup is Not displayed");

        }

        [Then(@"I verify close button in manage my notification popup")]
        public void ThenIVerifyCloseButtonInManageMyNotificaitonPopup()
        {
            myNotificationpage.VerifyClosePopup();

        }

        
        [Then(@"I verify notifications in the popup")]
        public void ThenIVerifyNotificationsInThePopup(string sectionDisplayed)
        {
            myNotificationpage.VerifyNotificationPopup(sectionDisplayed);

        }

        [Then(@"I click Immediate dropdown in the popup")]
        public void ThenIClickImmediateDropDownInThePopup()
        {
            myNotificationpage.SelectDropDownImmediatly();

        }

        [Then(@"I click Daily dropdown in the popup")]
        public void ThenIClickDailyDropdownInThePopup()
        {
            myNotificationpage.SelectDropDownDaily();
        }

        [Then(@"I click Weekly dropdown in the popup")]
        public void ThenIClickWeeklyDropdownInThePopup()
        {
            myNotificationpage.SelectDropDownWeekly();
        }

        [Then(@"I click Never dropdown in the popup")]
        public void ThenIClickNeverDropdownInThePopup()
        {
            myNotificationpage.SelectDropDownNever();
        }

        [Then(@"I verify filter notification section in my notification page")]
        public void ThenIVerifyFilterNotificationSectionInMyNotificationPage()
        {
            Thread.Sleep(5000);
            Assert.IsTrue(myNotificationpage.Browser.GetElement(MyNotificationElement.FILTER_SECTION).Displayed, "Filter Section is not displayed");
        }

        [Then(@"I click on Save button")]
        public void ThenIClickOnSaveButton()
        {
            Thread.Sleep(5000);
            myNotificationpage.Browser.Click(MyNotificationElement.SAVE_BUTTON);
        }

        [Then(@"I verify ""(.*)""notifications in the popup")]
        public void VerifyNotificationPopup(string Element)
        {
            Thread.Sleep(5000);
            myNotificationpage.VerifyNotificationPopup(Element);
           
        }
        
        [Then(@"I verify ""(.*)"" in filter notification")]
        public void ThenIVerifyInFilterNotification(string filterList)
        {
            myNotificationpage.VerifyFilterNotificationOption(filterList);
        }

        [When(@"I click ""(.*)"" in filter notification")]
        public void WhenIClickInFilterNotification(string filterList)
        {
            myNotificationpage.ClickFilterNotificationOption(filterList);
        }

        [Then(@"I should see notifications based on ""(.*)"" selection")]
        public void ThenIShouldSeeNotificationsBasedOnSelection(string filterList)
        {
            myNotificationpage.VerifyFilteredOption(filterList);
        }

    }
}
