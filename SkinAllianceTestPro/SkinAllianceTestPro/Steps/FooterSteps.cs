﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class FooterSteps : FooterElements
    {
        private readonly ScenarioContext scenarioContext;
        private FooterPage footerPage;
        private HomePage homepage;

        public static string Title;


        public FooterSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            footerPage = new FooterPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            homepage = new HomePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }



        [Then(@"I verify the footer elements")]
        public void ThenIVerifyTheFooterElements(Table table)
        {
            footerPage.VerifyFooterLinks(table);
            Console.WriteLine("Footer links are verified successfully");
        }

        [Then(@"I verify header and footer navigations are displayed")]
        public void ThenIVerifyHeaderAndFooterNavigationsAreDisplayed()
        {
            bool FooterSection = footerPage.Browser.GetElement(FooterElements.FOOTER_SECTION).Displayed;
            Assert.IsTrue(FooterSection, "footer section is not displayed");
            bool HeaderSection = homepage.Browser.GetElement(HeaderElements.HEADER_SECTION).Displayed;
            Assert.IsTrue(HeaderSection, "header section is not displayed");
        }



        [Then(@"I verify ""(.*)"" page is displayed")]
        public void ThenIVerifyPageIsDisplayed(string pageHeading)
        {
            Thread.Sleep(5000);
            Title = footerPage.Browser.GetElement(FooterElements.STATIC_PAGE_HEADING).Text;
  
            Assert.IsTrue(Title.Contains(pageHeading), pageHeading + "footer page is not displayed");
        }



        [When(@"I click ""(.*)"" link from footer")]
        public void WhenIClickLinkFromFooter(string footerLinks)
        {
            Thread.Sleep(5000);
            footerPage.ClickFooterLinks(footerLinks);
            //Console.WriteLine("Successfully navigated to" + footerLinks + "is displayed");
        }

        [Then(@"I verify Image related to skin alliance")]
        public void ThenIVerifyImageRelatedToSkinAlliance()
        {
            IWebElement Image = footerPage.Browser.GetElement(FooterElements.IMAGE_ABOUT_US);

            Assert.IsTrue(Image.Displayed, "Image is not displayed in About us page");
        }

        [Then(@"I verify the following fields")]
        public void ThenIVerifyTheFollowingFields(Table table)
        {
            footerPage.VerifyContactUsFields(table);
        }

        [When(@"I enter ""(.*)"" in first name field")]
        public void WhenIEnterInFirstNameField(string firstName)
        {
            footerPage.Browser.EnterText(FooterElements.CONTACT_FIRST_NAME, firstName);

        }

        [When(@"I enter ""(.*)"" in phone field")]
        public void WhenIEnterInPhoneField(string PhoneNo)
        {
            footerPage.Browser.EnterText(FooterElements.CONTACT_PHONE_NO, PhoneNo);
        }


        [When(@"I enter ""(.*)"" in last name field")]
        public void WhenIEnterInLastNameField(string lastName)
        {
            footerPage.Browser.EnterText(FooterElements.CONTACT_LAST_NAME, lastName);
        }

        [When(@"I enter ""(.*)"" in email field")]
        public void WhenIEnterInEmailField(string Email)
        {
           footerPage.Browser.EnterText(FooterElements.CONTACT_EMAIL_ADDRESS, Email);
        }

        [When(@"I enter ""(.*)"" in message field")]
        public void WhenIEnterInMessageField(string Message)
        {
           footerPage.Browser.EnterText(FooterElements.CONTACT_MESSAGE, Message);
        }

        [Then(@"I verify ""(.*)"" in contact us page")]
        public void ThenIVerifyInContactUsPage(string errorMessage)
        {
            footerPage.GetErrorMessage(errorMessage);
        }

        [When(@"I click Submit button in contact us page")]
        public void WhenIClickSubmitButtonInContactUsPage()
        {
            footerPage.Browser.Click(FooterElements.CONTACT_SUBMIT_BUTTON);
        }

        [Then(@"I should see ""(.*)"" message is displayed")]
        public void ThenIShouldSeeMessageIsDisplayed(string successMessage)
        {
            Thread.Sleep(3000);
            string message = footerPage.Browser.GetElement(FooterElements.SUCCESS_MESSAGE).Text;
            Assert.AreEqual(successMessage, message, "Expected is:" + successMessage + "Actual is:" + message);
        }

        [When(@"I enter ""(.*)"" in contact us phone number field")]
        public void WhenIEnterInContactUsPhoneNumberField(string PhoneNumber)
        {
            footerPage.Browser.EnterText(FooterElements.CONTACT_PHONE_NO, PhoneNumber);
        }

        [When(@"I verify Skin Alliance partner names with logos")]
        public void WhenIVerifySkinAlliancePartnerNamesWithLogos()
        {
            Thread.Sleep(3000);
            Assert.IsTrue(footerPage.Browser.GetElement(FooterElements.PARTNER_LOGO_SECTION).Displayed, "Skin Alliance partner names and logos are not displayed");
        }

        [Then(@"I verify description is displayed in ""(.*)"" page")]
        public void ThenIVerifyDescriptionIsDisplayedInPage(string page)
        {
            if(page == "PRIVACY POLICY")
            {
                Assert.IsTrue(footerPage.Browser.GetElement(PRIVACY_POLICY_CONTENT).Displayed, page + "content is not displayed");
            }
            else if(page == "LEGAL")
            {
                Assert.IsTrue(footerPage.Browser.GetElement(LEGAL_PAGE_CONTENT).Displayed, page + "content is not displayed");
            }
            else if(page == "TERMS OF USE")
            {
                Assert.IsTrue(footerPage.Browser.GetElement(TERMS_OF_USE_CONTENT).Displayed, page + "content is not displayed");
            }
        }



    }
}
