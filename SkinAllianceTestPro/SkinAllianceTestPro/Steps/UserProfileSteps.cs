﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class UserProfileSteps
    {
        private readonly ScenarioContext scenarioContext;
        private UserProfilePage userProfilePage;

        public UserProfileSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            userProfilePage = new UserProfilePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            
        }

        [Then(@"I verify the attributes of user Profile page")]
        public void ThenIVerifyTheAttributesOfUserProfilePage(Table table)
        {
            userProfilePage.VerifyUserProfileContents(table);
        }

        [When(@"I click on follow button in user profile page")]
        public void WhenIClickOnFollowButtonInUserProfilePage()
        {
            Thread.Sleep(5000);
            userProfilePage.Browser.Click(UserProfileElements.USER_FOLLOW_BUTTON);
        }

        [Then(@"I verify ""(.*)"" as button name in user profile page")]
        public void ThenIVerifyAsButtonNameInUserProfilePage(string buttonName)
        {
            string button = userProfilePage.Browser.GetElement(UserProfileElements.USER_FOLLOW_BUTTON).Text;
            if (button == "UNFOLLOW")
            {

                Assert.AreEqual(buttonName, button, "Expected button name:" + buttonName + "Displayed button name:" + button);
            }
            else
            {
                Assert.IsTrue(button.Equals("FOLLOW"), "Expected button name:" + buttonName + "Displayed button name:" + button);
            }
        }

        [Then(@"I verify ""(.*)"" section in userprofile page")]
        public void ThenIVerifySectionInUserprofilePage(string Section)
        {
            userProfilePage.VerifyUserSection(Section);
        }

        [Then(@"I verify Name of ""(.*)"" section")]
        public void ThenIVerifyNameOfSection(string CategoryList)
        {
            userProfilePage.VerifyCategoriesList(CategoryList);
        }

        [Then(@"I verify profile picture is displayed in ""(.*)""")]
        public void ThenIVerifyProfilePictureIsDisplayedIn(string Section)
        {
            userProfilePage.VerifyOtherUserProfilePictures(Section);
        }

        [Then(@"I click on previous link in breadcrumb in Userprofile page")]
        public void ThenIClickOnPreviousLinkInBreadcrumbInUserprofilePage()
        {
            userProfilePage.Browser.GetElements(UserProfileElements.BREADCRUMB_PREV_LINK)[1].Click();
        }


        [Then(@"I verify Profile name is displayed in ""(.*)""")]
        public void ThenIVerifyProfileNameIsDisplayedIn(string Section)
        {
            Thread.Sleep(4000);
            userProfilePage.VerifyOtherUserProfileNames(Section);
        }

        [Then(@"I click on a random user name in ""(.*)"" section")]
        public void ThenIClickOnARandomUserNameInSection(string UserName)
        {
            //Thread.Sleep(4000);
            userProfilePage.ClickRandomUserName(UserName);
        }


        [Then(@"I verify User Profile page is displayed")]
        public void ThenIVerifyUserProfilePageIsDisplayed()
        {
           Assert.IsTrue(userProfilePage.Browser.GetElement(UserProfileElements.PAGE_HEADING).Displayed, "User profile page is not displayed");
        }

    }
}
