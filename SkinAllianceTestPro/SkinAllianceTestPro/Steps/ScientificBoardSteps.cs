﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class ScientificBoardSteps 
    {
        private readonly ScenarioContext scenarioContext;
        private ScientificBoardDetailPage scientificPage;
       

        public static string Title;


        public ScientificBoardSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            scientificPage = new ScientificBoardDetailPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
           
        }


        [Then(@"I should see ""(.*)"" detail page")]
        public void ThenIShouldSeeDetailPage(string pageTitle)
        {
            Title = scientificPage.Browser.GetElement(ScientificBoardMemberElements.PAGE_TITLE).Text;
            Assert.IsTrue(pageTitle.Equals(Title, StringComparison.InvariantCultureIgnoreCase), "Incorrect title is displayed");
        }

        [Then(@"I verify members with details listed")]
        public void ThenIVerifyMembersWithDetailsListed()
        {
            Assert.IsTrue(scientificPage.Browser.GetElement(ScientificBoardMemberElements.BOARD_MEMBERS_LIST).Displayed, "Members with details are not displayed");
        }

        

        [When(@"I click on a ""(.*)"" random board member name")]
        public void WhenIClickOnARandomBoardMemberName(string Element)
        {
            scientificPage.ClickRandomScientificMember(Element);
        }



        [Then(@"I verify the scientific board members details")]
        public void ThenIVerifyTheScientificBoardMembersDetails(Table table)
        {
            scientificPage.VerifyScientificBoardItems(table);
        }



    }
}
