﻿using NUnit.Framework;
using OpenQA.Selenium;
using SkinAllianceTestPro.Block.ElementBlocks;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
   public class SubCategoryListingSteps
    {
        private readonly ScenarioContext scenarioContext;
        private SubCategoryListingPage subCatListingPage;
        private ArticleDetailsPage articleDetails;
        private ArticleListingPage articleListing;

        public SubCategoryListingSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            subCatListingPage = new SubCategoryListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            articleDetails = new ArticleDetailsPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));

            // homePage = new HomePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            articleListing = new ArticleListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }


        [Then(@"I verify subcategories are displayed")]
        public void ThenIVerifySubcategoriesAreDisplayed()
        {
            Assert.IsTrue(subCatListingPage.Browser.GetElement(SubCategoryListingElements.SUB_CATEGORY_SECTION).Displayed, "Subcategories are not displayed");
        }

        [Then(@"I verify ""(.*)"" of the articles")]
        public void ThenIVerifyOfTheArticles(string Element)
        {
            for (int i = 0; i< subCatListingPage.GetArticlesDescription().Count; i++) {
                if (Element == "Image")
                {
                    Assert.IsTrue(subCatListingPage.GetArticlesImage()[i].Displayed, Element + "is not displayed");
                }
                else if (Element == "short description")
                {
                    Assert.IsTrue(subCatListingPage.GetArticlesDescription()[i].Displayed, Element + "is not displayed");
                }
                else if (Element == "Comment icon")
                {
                    Assert.IsTrue(subCatListingPage.GetArticlesCommentIcon()[i].Displayed, Element + "is not displayed");
                }
                else if (Element == "Like icon")
                {
                    Assert.IsTrue(subCatListingPage.GetArticlesLikeIcon()[i].Displayed, Element + "is not displayed");
                }

                else if (Element == "Posted date")
                {
                    Assert.IsTrue(subCatListingPage.getArticlesPostedDate()[i].Displayed, Element + "is not displayed");
                }
                else if (Element == "Author Name")
                {
                    Assert.IsTrue(subCatListingPage.getArticlesAuthorName()[i].Displayed, Element + "is not displayed");
                }
                else if(Element == "No of likes")
                {
                    Assert.IsTrue(subCatListingPage.GetLikesCount()[i].Displayed, Element + "is not displayed");
                }
                else if(Element == "No of comments")
                {
                    Assert.IsTrue(subCatListingPage.GetCommentsCount()[i].Displayed, Element + "is not displayed");
                }
                
            }
        }

        [When(@"I click ""(.*)"" of a random article")]
        public void WhenIClickOfARandomArticle(string Element)
        {
            if(Element == "Image")
            {
                subCatListingPage.ClickRandomArticleImage();
            }
           else if(Element == "Content text")
            {
                subCatListingPage.ClickRandomArticleContent();
            }
         else if(Element == "Comment icon")
            {
                subCatListingPage.ClickRandomCommentIcon();
            }

        else if(Element == "Like icon")
            {
                subCatListingPage.ClickRandomLikeIcon();
            }
        else
            {
                Assert.Fail(Element + "link is not clickable");
            }
            
        }

        [Then(@"I verify Sub Category listing page with title is displayed")]
        public void ThenIVerifySubCategoryListingPageWithTitleIsDisplayed()
        {
            Thread.Sleep(3000);
            IWebElement title = subCatListingPage.Browser.GetElement(SubCategoryListingElements.SUB_CATEGORY_PAGE_TITLE);
            Assert.IsTrue(title.Displayed, "Sub Category listing page is not displayed");
        }


        [Then(@"I verify Article details page is displayed")]
        public void ThenIVerifyArticleDetailsPageIsDisplayed()
        {
            Thread.Sleep(4000);
            articleDetails.Browser.GetElement(ArticleDetailsElement.ARTICLE_CONTENT);
            Assert.IsTrue(articleDetails.Browser.GetElement(ArticleDetailsElement.ARTICLE_CONTENT).Displayed, "Article details page is not displayed");
        }

        [When(@"I click on a random View All button")]
        public void WhenIClickOnARandomViewAllButton()
        {
            Thread.Sleep(5000);
            subCatListingPage.ClickRandomViewAllLink();
            
        }

        [Then(@"I verify coressponding sub category page is displayed")]
        public void ThenIVerifyCoresspondingSubCategoryPageIsDisplayed()
        {
            string Title = articleListing.Browser.GetElement(ArticleListingElement.PAGE_HEADING).Text;
            Assert.IsTrue(subCatListingPage.Browser.GetElement(SubCategoryListingElements.SUB_CATEGORY_PAGE_TITLE).Displayed, "Corresponding sub category page is not displayed");
        }


        [When(@"I click on follow button")]
        public void WhenIClickOnFollowButton()
        {
            subCatListingPage.Browser.Click(SubCategoryListingElements.FOLLOW_BUTTON);
        }

        [Then(@"I verify ""(.*)"" as button name")]
        public void ThenIVerifyAsButtonName(string buttonName)
        {

            subCatListingPage.GetButtonName(buttonName);
        }

        [Then(@"I click on previous breadcrumb link in subcategory listing page")]
        public void ThenIClickOnPreviousBreadcrumbLinkInSubcategoryListingPage()
        {
            subCatListingPage.Browser.Click(SubCategoryListingElements.BREADCRUMB_PREV_LINK);
        }

        [When(@"I click on ""(.*)"" subcategory name")]
        public void WhenIClickOnSubcategoryName(string Element)
        {
            if (Element == "Aging")
            {
                subCatListingPage.Browser.Click(SubCategoryListingElements.AGING_SUBCATEGORY);
            }

        }
        [Then(@"I verify corresponding ""(.*)"" sub category listing page is displayed")]
        public void ThenIVerifyCorrespondingSubCategoryListingPageIsDisplayed(string Element)
        {
            if (Element == "AGING")
            {
                string Title = subCatListingPage.Browser.GetElement(SubCategoryListingElements.AGING_HEADING).Text;
            }
        }

    }
}
