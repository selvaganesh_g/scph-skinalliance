﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using SkinAllianceTestPro.Steps;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Block.ElementBlocks;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class HomeSteps : HeaderElements
    {
        private readonly ScenarioContext scenarioContext;
        //private static RemoteWebDriver driverContext;
        private HomePage homePage;

        private LoginPage loginPage;

        private CommonPage commonPage;

        private ArticleListingPage articleListing;

        private CategoryListingPage catListingPage;

        private MyProfilePage profilePage;

        private MyNotificationPage myNotificationpage;

        public static string QuestionText;

        //private FooterPage footerPage = new FooterPage(driverContext);

        public static string category;

        public static string ActualURL;

        public static string EnteredText;

        public static string DisplayedArticle;

        public static string SelectedSubCategoryName;

        
        public HomeSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            homePage = new HomePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            articleListing = new ArticleListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            catListingPage = new CategoryListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            loginPage = new LoginPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            profilePage = new MyProfilePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            myNotificationpage = new MyNotificationPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }


        


        [Then(@"I click on ""(.*)"" link from user picture")]
        public void ThenIClickOnLinkFromUserPicture(string MenuOption)
        {
            homePage.NavigateToSkinBoxPages(MenuOption);
        }

        [When(@"I select (.+) from mega menu")]
        public void MenuSelection(string menuOption)
        {
            homePage.MenuSelection(menuOption);
        }

        [When(@"I select product (.+) from (.+)")]
        public void ProductSelection(string productCat, string productName)
        {
            homePage.ProductSelection(productName, productCat);

        }

        [When(@"I select a product from sheet (.+)")]
        public void SelectProduct(string rowId, Table productTable)
        {
            homePage.SelectProduct(rowId, productTable);
        }

        [Then(@"I should see page title as (.+)")]
        public void CheckPageTitle(string pageTitle)
        {
            string actualPageTitle = homePage.Browser.GetPageTitle();
           
           
            //Assert.AreEqual(pageTitle, actualPageTitle.Contains(pageTitle), "The expected page title not found. Expected: " + pageTitle + "; but actual: " + actualPageTitle) ;
            Assert.IsTrue(actualPageTitle.Contains((pageTitle)), "The expected page title not found. Expected: " + pageTitle + "; but actual: " + actualPageTitle);
            
        }

        [When(@"I select column '(.+)' from sheet (.+) row from (.*) to (.*)")]
        public void GetfromExcel(string columnNames, string sheetName, int fromRowId, int toRowId)
        {
            homePage.selectProducts(sheetName, fromRowId, toRowId, columnNames);

        }

        [When(@"I select column '(.+)' from sheet (.+)")]
        public void GetfromExcel(string columnNames, string sheetName, Table table)
        {
            IList<dynamic> productTable = table.CreateDynamicSet().ToList();

            List<TableContent> rowList = new List<TableContent>();

            foreach (var row in productTable)
            {
                rowList.Add(new TableContent { Name = row.RowId });
            }

            homePage.selectProducts(sheetName, rowList, columnNames);

        }

        [When(@"I click on a random sub category from dropdown")]
        public void WhenIClickOnARandomSubCategoryFromDropdown()
        {
            homePage.ClickRandomDropdown();
        }



        [Then(@"I verify the following contents in homepage")]
        public void ThenIVerifyTheFollowingContentsInHomepage(Table table)
        {
            homePage.VerifyHomeComponents(table);
        }

        [When(@"I click on ""(.*)"" in header")]
        public void WhenIClickOnInHeader(string Logo)
        {
            homePage.ClickPageLogo();
            Console.WriteLine("Successfully clicked" + Logo);
        }

        [Then(@"I click on ""(.*)"" in header")]
        public void ThenIClickOnInHeader(string logo)
        {
            homePage.Browser.Click(SKIN_LOGO);
        }

        [Then(@"I verify home page is displayed")]
        public void ThenIVerifyHomePageIsDisplayed()
        {
            Thread.Sleep(4000);
            Assert.IsTrue(homePage.Browser.GetElement(SKIN_LOGO).Displayed, "Home page is not displayed");
        }

        

        [When(@"I Navigate to (.*) from home page")]
        public void WhenINavigateToInHomePage(string PageNavigation)
            
        {
            if (PageNavigation == "Article Listing")
            {
                homePage.SelectRandomCategory();
                catListingPage.ClickRandomSubCategory();
                SelectedSubCategoryName = catListingPage.ClickRandomSubCategory();

            }

            else if (PageNavigation == "Article details")
            {
                 homePage.SelectRandomCategory();
                 category = articleListing.ClickRandomArticleContent();
               
            }

            else if (PageNavigation == "Search results")
            {
                EnteredText = "Skin Care";
                homePage.EnterSearchText(EnteredText);
            }
            else if (PageNavigation == "My Profile")
            {
                homePage.ClickUserPicture();
                homePage.NavigateToSkinBoxPages(PageNavigation);
            }

            else if(PageNavigation == "User Profile")
            {
                
            }
            
        }

        [Then(@"I verify the (.*) page is displayed")]
        public void ThenIVerifyThePageIsDisplayed(string Page)
        {
            if (Page == "Article Listing")
            {
                string pageTitle = articleListing.GetArticleTitle();
                Assert.IsTrue(pageTitle.Contains(SelectedSubCategoryName), Page + "is not displayed");
            }

            if (Page == "Article details")
            {
                
            }

            if(Page == "Search results")
            {
              
            }
           }



        [Then(@"I verify ""(.*)"" in header")]
        public void ThenIVerifyInHeader(string HeaderElements)
        {
            Thread.Sleep(5000);
            if (HeaderElements == "Userpicture")
            {
                Assert.IsTrue(homePage.Browser.GetElement(USER_PICTURE_IMAGE).Displayed, HeaderElements+ "is not displayed");
            }
            else if(HeaderElements == "Navigation menu")
            {
                Assert.IsTrue(homePage.Browser.GetElement(NAV_MENU).Displayed, HeaderElements + "is not displayed");
            }
            else if(HeaderElements == "Skin Alliance logo")
            {
                Assert.IsTrue(homePage.Browser.GetElement(SKIN_LOGO).Displayed, HeaderElements + "is not displayed");
            }
            else if(HeaderElements == "Search")
            {
                Assert.IsTrue(homePage.Browser.GetElement(SEARCH_TEXT).Displayed, HeaderElements + "is not displayed");
            }
            else if(HeaderElements == "Country flag")
            {
                Assert.IsTrue(homePage.Browser.GetElement(USER_COUNTRY_ICON).Displayed, HeaderElements + "is not displayed");
            }
        }
        [When(@"I click on a random Skin Alliance Partner logo")]
        public void WhenIClickOnSkinAlliancePartnerLogo()
        {
            ActualURL = homePage.Browser.GetCurrentUrl();
            homePage.ClickRandomPartnerLogo();
        
        }

        [Then(@"I verify external site is displayed")]
        public void ThenIVerifyExternalSiteIsDisplayed()
        {
            Thread.Sleep(4000);
            homePage.Browser.SwitchToBrowserTab();
            string currentURL = homePage.VerifyExternalBrowserURL();
            homePage.Browser.SwitchToParentTab();
            Assert.AreNotEqual(currentURL, ActualURL, "External site URL is matching with the Actual URL");
            //Console.WriteLine("External site is displayed");
           }

        [Then(@"I verify the search field is displayed")]
        public void ThenIVerifyTheSearchFieldIsDisplayed()
        {
            Assert.IsTrue((homePage.Browser.GetElement(SEARCH_TEXT).Displayed), "Search field is not displayed");
        }

        [When(@"I search for keyword ""(.*)""")]
        public void WhenISearchForKeyword(string Keyword)
        {
          
            homePage.EnterSearchText(Keyword);
            Console.WriteLine("Search keyword" + Keyword + "is entered successfully");
        }

        [Then(@"I should see corresponding article list page")]
        public void ThenIShouldSeeCorrespondingArticlesArePopulated()
        {
            Thread.Sleep(3000);
            Assert.AreEqual(EnteredText, articleListing.GetArticleTitle(), "Coressponding Article" +EnteredText+ "is not displayed");
            Console.WriteLine(EnteredText + "Article Listing page is displayed");
            
        }

        [Then(@"I verify search results page is displayed")]
        public void ThenIVerifySearchResultsPageIsDisplayed()
        {
            Thread.Sleep(3000);
            homePage.VerifySearchPageHeading();
        }




        [Then(@"I verify Scientific board widget in home page")]
        public void ThenIVerifyScientificBoardWidgetInHomePage()
        {
            Assert.IsTrue(homePage.Browser.GetElement(HeaderElements.BOARD_SCIENTIFIC_SECTION).Displayed, "Scientific board widget is not displayed");
        }

        [Then(@"I verify Image in scientific board widget")]
        public void ThenIVerifyImageInScientificBoardWidget()
        {
            Assert.IsTrue(homePage.Browser.GetElement(HeaderElements.BOARD_SCIENTIFIC_IMAGE).Displayed, "Scientific board image is not displayed");
        }

        [Then(@"I verify description in scientific board widget")]
        public void ThenIVerifyDescriptionInScientificBoardWidget()
        {
            Assert.IsTrue(homePage.Browser.GetElement(HeaderElements.BOARD_SCIENTIFIC_DESCRIPTION).Displayed, "Description is not displayed in scientific board");
        }

        [Then(@"I verify Read more button in scientific board widget")]
        public void ThenIVerifyReadMoreButtonInScientificBoardWidget()
        {
            Assert.IsTrue(homePage.Browser.GetElement(HeaderElements.BOARD_READ_MORE_BUTTON).Displayed, "Read more button is not displayed");
        }

        [When(@"I click on Read more button in scientific board")]
        public void WhenIClickOnReadMoreButtonInScientificBoard()
        {
            homePage.Browser.Click(HeaderElements.BOARD_READ_MORE_BUTTON);
        }




        [Then(@"I verify image carousel in home page")]
        public void ThenIVerifyImageCarouselInHomePage()
        {
            Thread.Sleep(5000);
            Assert.IsTrue(homePage.VerifyImageCarouselSlider(), "Image carousel section is not displayed");   
        }

        [Then(@"I verify image of articles in image carousel")]
        public void ThenIVerifyImageOfArticlesInImageCarousel()
        {
            Assert.IsTrue(homePage.VerifyImageCarousel().Count == 3, "Article Image is not displayed");
        }

        [Then(@"I verify article title in image carousel")]
        public void ThenIVerifyArticleTitlesInImageCarousel()
        {
            //homePage.Browser.GetElement(ARTICLE_TITLE_IMAGE).Text;
            Assert.IsTrue(homePage.VerifyImageTitle().Count == 3, "Article title is not displayed");
        }

        [Then(@"I verify description about the articles in image carousel")]
        public void ThenIVerifyDescriptionAboutTheArticlesInImageCarousel()
        {
            Assert.IsTrue(homePage.VerifyImageDescription().Count == 3, "Article description is not displayed");
        }

        [Then(@"I verify Read More button is displayed")]
        public void ThenIVerifyReadMoreButtonIsDisplayed()
        {
            Assert.IsTrue(homePage.Browser.GetElement(HeaderElements.READ_MORE_BUTTON).Displayed, "Read more button is not displayed");
        }


        [Then(@"I click on Read More button")]
        public void ThenIClickOnReadMoreButton()
        {
            Thread.Sleep(3000);
            homePage.Browser.Click(READ_MORE_BUTTON);
            Console.WriteLine("Successfully clicked Read more button");
        }

        [When(@"I click on the left arrow in the carousel")]
        public void WhenIClickOnTheLeftArrowInTheCarousel()
        {
            DisplayedArticle = homePage.Browser.GetElement(ARTICLE_DESCRIPTION_IMAGE).Text;
            Console.WriteLine(DisplayedArticle);
            homePage.Browser.Click(LEFT_ARROW_CAROUSEL);
           // Console.WriteLine("Successfully clicked on left arrow in the carousel");
        }

        [Then(@"I verify different article in the image carousel")]
        public void ThenIVerifyDifferentArticleInTheImageCarousel()
        {
            Thread.Sleep(3000);
            string CurrentArticle = homePage.Browser.GetElement(ARTICLE_DESCRIPTION_IMAGE).Text;
            Assert.IsTrue(!CurrentArticle.Equals(DisplayedArticle), "Displayed is:" +DisplayedArticle+ "ActualTitle is:" +CurrentArticle);
            Console.WriteLine("Different article image is displayed");
        }

        [Then(@"I verify sliding arrows in Popular category heading")]
        public void ThenIVerifySlidingArrowsInPopularCategoryHeading()
        {
            Assert.IsTrue(homePage.Browser.GetElement(LATEST_POST_ARROWS).Displayed, "Sliding arrows is not displayed in popular category heading");
        }


        [Then(@"I verify Latest Posts and Favourite Posts widget in homepage")]
        public void ThenIVerifyLatestPostsFavouritePostsWidgetInHomepage()
        {
            Assert.IsTrue(homePage.Browser.GetElement(POST_FULL_SECTION).Displayed, "Latest post and favourite posts widget are not displayed in home page");
        }

        [Then(@"I verify ""(.*)"" related to post in category listing")]
        public void ThenIVerifyRelatedToPostInCategoryListing(string element)
        {
            if(element == "short description")
            {
                Assert.IsTrue(homePage.Browser.GetElements(POST_DESCRIPTION).Count > 0, element + "is not displayed");
            }

            else if(element == "image")
            {
                Assert.IsTrue(homePage.Browser.GetElements(POST_IMAGE).Count > 0, element + "is not displayed");
            }

            else if(element == "no of likes")
            {
                Assert.IsTrue(homePage.Browser.GetElements(POST_LIKE).Count > 0, element + "is not displayed");
            }

            else if(element == "no of comments")
            {
                Assert.IsTrue(homePage.Browser.GetElements(POST_COMMENTS).Count > 0, element + "is not displayed");
            }
        }

        [Then(@"I verify ""(.*)"" button in the widget")]
        public void ThenIVerifyButtonInTheWidget(string button)
        {
            Assert.IsTrue(homePage.Browser.GetElement(HeaderElements.CONGRESSES_VIEW_ALL_LINK).Displayed, button + "is not displayed");
        }

        [Then(@"I verify View all button in the post section")]
        public void ThenIVerifyViewAllButtonInThePostSection()
        {
            Assert.IsTrue(homePage.Browser.GetElements(POST_VIEW_ALL_LINK).Count > 0, "View all is displayed in the latest post section");

        }

        [Then(@"I click on a random post in ""(.*)"" post")]
        public void ThenIClickOnARandomPostInPost(string section)
        {
            Thread.Sleep(3000);
            homePage.ClickRandomPost(section);
        }

        [Then(@"I click on a view all button of ""(.*)""")]
        public void ThenIClickOnAViewAllButtonOf(string section)
        {
            Thread.Sleep(3000);
            homePage.ClickRandomViewAllLink(section);
        }

        [Then(@"I click on a random category in latest posts")]
        public void ThenIClickOnARandomCategoryInLatestPosts()
        {
            homePage.ClickRandomCategory();
        }

        [Then(@"I verify ""(.*)"" of the categories in popular categories section")]
        public void ThenIVerifyOfTheCategoriesInPopularCategoriesSection(string element)
        {
            homePage.VerifyPopularCategoriesElements(element);
        }


        [Then(@"I should see corresponding category listing page")]
        public void ThenIShouldSeeCorrespondingCategoryListingPage()
        {
            homePage.VerifySubCategoryPageTitle();
        }



        [Then(@"I click on ""(.*)"" button in congresses section")]
        public void ThenIClickOnButtonInCongressesSection(string button)
        {
            homePage.Browser.Click(HeaderElements.CONGRESSES_VIEW_ALL_LINK);
        }



        [When(@"I click on User picture in header")]
        public void WhenIClickOnUserPictureInHeader()
        {
            Thread.Sleep(5000);
            homePage.ClickUserPicture();
            Console.WriteLine("Successfully user picture image");
        }

        [When(@"I click on ""(.*)"" in my skin box widget")]
        public void WhenIClickOnInMySkinBoxWidget(string Menu)
        {
            if(Menu == "My Profile")
                {
               
            }
        }


        [Then(@"I verify ""(.*)"" menu in dropdown")]
        public void ThenIVerifyMenuInDropdown(string dropdownMenu)
        {
            
            homePage.VerifySkinBoxDropdownMenus(dropdownMenu);
        }

        [Then(@"I should see navigaton menu bar in page")]
        public void ThenIShouldSeeNavigatonMenuBarInHomePage()
        {
            Thread.Sleep(5000);
            Assert.IsTrue(homePage.VerifyNavigationMenu(), "Navigation menu bar is not displayed");

        }

        [Then(@"I verify categories in navigation bar")]
        public void ThenIVerifyCategoriesInNavigationBar(Table table)
        {
            homePage.VerifyMenuCategories(table);
            Console.WriteLine("Menu categories are displayed in navigation bar");
        }

        [When(@"I hover on a ""(.*)"" in navigation bar")]
        public void WhenIHoverOnAInNavigationBar(string Menu)
        {
            Thread.Sleep(5000);
            homePage.HighlightCategoryMenu(Menu);
            
            //Console.WriteLine(Menu_Options + "is not hovered and highlighted");
        }

        [When(@"I click on ""(.*)"" in submenu")]
        public void WhenIHoverOnAInSubmenu(string Sub_Menu)
        {
            if(Sub_Menu == "Aesthetics")
            {
                homePage.Browser.Click(HeaderElements.AESTHETICS_SUBMENU);
            }
        }

        [When(@"I select ""(.*)"" from community dropdown")]
        public void WhenISelectFromCommunityDropdown(string Dropdown_Option)
        {
            Thread.Sleep(3000);
           if(Dropdown_Option == "Directory")
            {
                homePage.Browser.Click(HeaderElements.DIRECTORY_LINK);
            }
           else if(Dropdown_Option == "Contact us")
            {
                homePage.Browser.Click(HeaderElements.CONTACTUS_LINK);
            }
           else if(Dropdown_Option == "Scientific Board")
            {
                homePage.Browser.Click(HeaderElements.SCIENTIFIC_BOARD_LINK);
            }
        }


        [Then(@"I verify ""(.*)"" menu is expanded")]
        public void ThenIVerifyMenuIsExpanded(string Menu_Expanded)
        {
            Thread.Sleep(3000);
            homePage.VerifyCategoryMenuState();
        }

        [Then(@"I verify ""(.*)"" menu page")]
        public void ThenIVerifyMenuPage(string PageTitle)
        {
            Thread.Sleep(3000);
            string title = homePage.Browser.GetElement(CategoryListingElements.PAGE_TITLE).Text;
                Assert.IsTrue(PageTitle.Equals(title, StringComparison.InvariantCultureIgnoreCase), "Expected is:" + PageTitle + "Actual is:" + title);
        }



        [When(@"I click ""(.*)"" menu from navigation bar")]
        public void WhenIClickMenuFromNavigationBar(string Select_Menu)
        {
            Thread.Sleep(3000);
            homePage.ClickCategoryMenu(Select_Menu);
            Console.WriteLine("Successfully Navigated to" + Select_Menu + "Page");
        }

        [Then(@"I should see navigaton menu bar in ""(.*)"" page")]
        public void ThenIShouldSeeNavigatonMenuBarInPage(string Page)
        {
            Assert.IsTrue(homePage.Browser.GetElement(NAV_MENU).Displayed, "Navigation menu bar is not displayed in" +Page+ "page");

        }

        [When(@"I click on ""(.*)"" in Image logo dropdown")]
        public void WhenIClickOnInImageLogoDropdown(string dropdown_menu)
        {
            Thread.Sleep(5000);
            homePage.NavigateToSkinBoxPages(dropdown_menu);
        }

       [Then(@"I verify ""(.*)"" page")]
        public void ThenIVerifyPage(string Page)
        {
            if (Page == "MY PROFILE")
            {
                Thread.Sleep(10000);
                string pageHeading = profilePage.Browser.GetElement(MyProfileElements.MY_PROFILE_PAGE_HEADING).Text;
                Assert.IsTrue(Page.Equals(pageHeading, StringComparison.InvariantCultureIgnoreCase), "Expected is:" + Page + "Actual is" + pageHeading);
            }
            else if (Page == "MY NOTIFICATION")
            {
                Thread.Sleep(5000);
                string pageHeading = myNotificationpage.Browser.GetElement(MyNotificationElement.MY_NOTIFICATION_PAGE_HEADING).Text;
                Assert.IsTrue(Page.Equals(pageHeading, StringComparison.InvariantCultureIgnoreCase), "Expected is:" + Page + "Actual is" + pageHeading);
            }
            else if (Page == "LOG OUT")
            {
                loginPage.Browser.Click(LoginElements.LOGIN_HEADING);
            }
            else if (Page == "CREATE POST")
            {
                string pageHeading = homePage.Browser.GetElement(HeaderElements.PAGE_HEADING).Text;
                Assert.IsTrue(Page.Equals(pageHeading, StringComparison.InvariantCultureIgnoreCase), "Expected is:" + Page + "Actual is" + pageHeading);
            }

        }


        [Then(@"I verify ""(.*)"" as heading in the page")]
        public void ThenIVerifyAsHeadingInThePage(string pageHeading)
        {
            string heading_displayed = homePage.Browser.GetElement(CREATE_PAGE_HEADING).Text;
            Assert.IsTrue(pageHeading.Equals(heading_displayed, StringComparison.InvariantCultureIgnoreCase), pageHeading + "is not displayed as the page heading");
        }

        [Then(@"I verify the following contents in create a post page")]
        public void ThenIVerifyTheFollowingContentsInCreateAPostPage(Table table)
        {
            homePage.VerifyCreatePostComponents(table);
        }

        [Then(@"I verify corresponding ""(.*)"" page is displayed")]
        public void ThenIVerifyCorrespondingPageIsDisplayed(string Element)
        {
                if (Element == "TERMS OF USE")
                {
                    string Title = homePage.Browser.GetElement(FooterElements.TERMS_CONDITIONS_HEADING).Text;
                }

                else if (Element == "LEGAL")
                {
                     string Title = homePage.Browser.GetElement(FooterElements.LEGAL_HEADING).Text;
                }

                else if (Element == "COOKIE AND PRIVACY POLICY")
                {

                    string Title = homePage.Browser.GetElement(FooterElements.PRIVACY_POLICY_HEADING).Text;
                }     
        }


        [Then(@"I enter ""(.*)"" as title for the post")]
        public void ThenIEnterAsTitleForThePost(string Title)
        {
            homePage.Browser.EnterText(ENTER_POST_TITLE, Title);
        }



        [Then(@"I add content in the Text input section")]
        public void ThenIAddContentInTheTextInputSection()
        {
            homePage.Browser.EnterText(ENTER_CONTENT_FIELD, "Skin care product");
        }

        

        [Then(@"I enter ""(.*)"" as URL")]
        public void ThenIEnterAsURL(string URL)
        {
            homePage.Browser.EnterText(ENTER_URL_FIELD, URL);
        }

        [Then(@"I upload a file in file upload field")]
        public void ThenIUploadAFileInFileUploadField()
        {
            homePage.UploadFile();
        }

        [Then(@"I add a image in Add featured image section")]
        public void ThenIAddAImageInAddFeaturedImageSection()
        {
            homePage.UploadPostImage();
        }



        [Then(@"I select a random category in create a post page")]
        public void ThenISelectARandomCategoryInCreateAPostPage()
        {
            homePage.SelectRandomCategoryinPost();
            Console.WriteLine("Successfully selected a category checkbox");
        }


        [When(@"I click on ""(.*)"" button in create new post")]
        public void WhenIClickOnButtonInCreateNewPost(string Button)
        {
            homePage.Browser.Click(CREATE_POST_BUTTON);
            Console.WriteLine("Successfully clicked" + Button + "button");
        }

        [Then(@"I should see new post popup")]
        public void ThenIShouldSeeNewPostIsCreated()
        {
            Thread.Sleep(3000);
            Assert.IsTrue(homePage.Browser.GetElement(HeaderElements.ARTICLE_CREATED_MSG).Displayed, "New post is not created");
        }

        [Then(@"I click on Ok button in new post popup")]
        public void ThenIClickOnOkButtonInNewPostPopup()
        {
            homePage.Browser.Click(HeaderElements.PUBLISH_OK_BUTTON);
        }


        [Then(@"I verify ""(.*)"" or There is no Copyright"" message in a popup")]
        public void ThenIVerifyOrThereIsNoCopyrightMessageInAPopup(string Confirmation_Message)
        {
            string message = homePage.Browser.GetElement(COPYRIGHT_CONFIRMATION_MSG).Text;
            Console.WriteLine(message);
          Assert.AreEqual(Confirmation_Message, message, "Incorrect Confirmation message is displayed");
        }

        [Then(@"I verify Questionnaire section is displayed")]
        public void ThenIVerifyQuestionnaireSectionIsDisplayed()
        {
            Assert.IsTrue(homePage.Browser.GetElement(QUESTION_HEADING).Displayed, "Questionnaire section is not displayed");
        }

        [Then(@"I verify Add Question link in create a post")]
        public void ThenIVerifyAddQuestionLinkInCreateAPost()
        {
            homePage.Browser.Click(HeaderElements.ADD_QUESTION_LINK);
        }



        [Then(@"I verify ""(.*)"" button in create a post")]
        public void ThenIVerifyButtonInCreateAPost(string button)
        {
            if(button == "Preview post")
            {
                Assert.IsTrue(homePage.Browser.GetElement(PREVIEW_BUTTON).Displayed, button + "button is not displayed");

            }
            else if(button == "Submit post")
            {
                Assert.IsTrue(homePage.Browser.GetElement(CREATE_POST_BUTTON).Displayed, button + " button is not displayed");
            }
            else
            {
                Assert.Fail("Create post page is not displayed properly");
            }
        }

        [When(@"I click on ""(.*)"" link")]
        public void WhenIClickOnLink(string QuestionLink)
        {
            homePage.Browser.Click(ADD_QUESTION_LINK);
        }

        [When(@"I add question in Enter question text field")]
        public void WhenIAddQuestionInEnterQuestionTextField()
        {
            homePage.Browser.EnterText(ENTER_QUESTION_FIELD, "question");
           
        }

        [When(@"I enter answers related question in Answers text fields")]
        public void WhenIEnterAnswersRelatedQuestionInAnswersTextFields()
        {
            homePage.Browser.EnterText(FIRST_ANSWER_FIELD, "answer1");
            homePage.Browser.EnterText(SECOND_ANSWER_FIELD, "answer2");
            homePage.Browser.EnterText(THIRD_ANSWER_FIELD, "answer3");
            homePage.Browser.EnterText(FOURTH_ANSWER_FIELD, "answer4");
                
         }

        [When(@"I enter comments in Enter Response text field")]
        public void WhenIEnterCommentsInEnterResponseTextField()
        {
            homePage.Browser.EnterText(RESPONSE_TEXT_FIELD, "comments section");
        }

        [When(@"I click on a answer checkbox")]
        public void WhenIClickOnAAnswerCheckbox()
        {
            Thread.Sleep(3000);
            homePage.Browser.Click(HeaderElements.FIRST_ANSWER_CHECKBOX);
        }

        [Then(@"I verify display user response percentage checkbox")]
        public void ThenIVerifyDisplayUserResponsePercentageCheckbox()
        {
            Assert.IsTrue(homePage.Browser.GetElement(HeaderElements.RESPONSE_PERCENTAGE_CHECKBOX).Displayed, "User response percentage checkbox is not displayed");
        }

        [Then(@"I click display user response percentage checkbox")]
        public void ThenIClickDisplayUserResponsePercentageCheckbox()
        {
            homePage.Browser.Click(HeaderElements.RESPONSE_PERCENTAGE_CHECKBOX);
        }

        [Then(@"I click Save button in questionnaire section")]
        public void ThenIClickSaveButtonInQuestionnaireSection()
        {
            homePage.Browser.Click(HeaderElements.QUESTION_SAVE_BUTTON);
        }

        [Then(@"I click on preview button")]
        public void ThenIClickOnPreviewButton()
        {
            homePage.Browser.Click(HeaderElements.PREVIEW_BUTTON);
        }







        [When(@"I click Save button in questionnaire section")]
        public void WhenIClickSaveButtonInQuestionnaireSection()
        {
            homePage.Browser.Click(QUESTION_SAVE_BUTTON);
                
        }

        [When(@"I click on preview button")]
        public void WhenIClickOnPreviewButton()
        {
            homePage.Browser.Click(PREVIEW_BUTTON);
            
        }

        [Then(@"I verify preview of the post is displayed")]
        public void ThenIVerifyPreviewOfThePostIsDisplayed()
        {
            Thread.Sleep(3000);
            homePage.VerifyPreviewPostTab();
        }

        [Then(@"I verify added question is displayed in questionnaire section")]
        public void ThenIVerifyAddedQuestionIsDisplayedInQuestionnaireSection()
        {
            Assert.IsTrue(homePage.Browser.GetElement(ADDED_QUESTION_SECTION).Displayed, "Added question section is not displayed");   
        }


        [Then(@"I click on Create post button")]
        public void ThenIClickOnCreatePostButton()
        {
            homePage.Browser.Click(CREATE_POST_BUTTON);
        }

        [When(@"I click ""(.*)"" from Community navigation menu")]
        public void WhenIClickFromCommunityNavigationMenu(string StaticLink)
        {
            
        }

        [Then(@"I verify Upcoming Congresses in homepage")]
        public void ThenIVerifyUpcomingCongressesInHomepage()
        {
            Assert.IsTrue(homePage.Browser.GetElement(UPCOMING_CONGRESSES_SECTION).Displayed, "Upcoming Congresses section is not displayed");   
        }

        [Then(@"I verify categories in Upcoming Congresses")]
        public void ThenIVerifyCategoriesInUpcomingCongresses()
        {
            Assert.IsTrue(homePage.Browser.GetElements(UPCOMING_CONGRESSES_SECTION).Count > 0, "categories are not displayed in upcoming congresses");
        }

        [When(@"I click on a random congresses from Upcoming Congresses")]
        public string WhenIClickOnARandomCongressesFromUpcomingCongresses()
        {
            return homePage.ClickRandomCongresses();
        }



    }
}
