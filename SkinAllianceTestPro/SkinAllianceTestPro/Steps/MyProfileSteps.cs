﻿using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using OpenQA.Selenium;
using NUnit.Framework;
using System.Threading;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
   public class MyProfileSteps
    {
        private readonly ScenarioContext scenarioContext;
        private MyProfilePage profilePage;
        private FooterPage footerPage;
        private HomePage homePage;

        public static string ErrorText;
        public MyProfileSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            profilePage = new MyProfilePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            //footerPage = new FooterPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            homePage = new HomePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }

        [Then(@"I verify the attributes of My Profile page")]
        public void ThenIVerifyTheAttributesOfMyProfilePage(Table table)
        {
            Thread.Sleep(5000);
            profilePage.VerifyMyProfileContents(table);
           // Console.WriteLine("My Profile page is displayed properly");
        }

        [Then(@"I verify the attributes of Edit Profile page")]
        public void ThenIVerifyTheAttributesOfEditProfilePage(Table table)
        {
            Thread.Sleep(5000);
            profilePage.VerifyEditProfileContents(table);
            // Console.WriteLine("Edit Profile page is displayed properly");
        }

        [Then(@"I verify ""(.*)"" section is displayed")]
        public void ThenIVerifySectionIsDisplayed(string section)
        {
            profilePage.VerifyUserSection(section);
            Console.WriteLine(section + "section is displayed properly");
        }

        [Then(@"I verify list of users ""(.*)"" the user")]
        public void ThenIVerifyListOfUsersTheUser(string section)
        {

            profilePage.VerifyUsersList(section);
            Console.WriteLine(section + "is displayed properly");
        }

        [Then(@"I verify the ""(.*)"" of the user following")]
        public void ThenIVerifyTheOfTheUserFollowing(string UserContent)
        {
            profilePage.VerifyFollowingContents(UserContent);
           // Console.WriteLine(UserContent + "is displayed properly");
        }

        [Then(@"I verify the ""(.*)"" followed by the user")]
        public void ThenIVerifyTheFollowedByTheUser(string UserContent)
        {
            profilePage.VerifyFollowedByContents(UserContent);
            Console.WriteLine(UserContent + "is displayed properly");
        }

        [When(@"I click on ""(.*)"" button in my profile page")]
        public void WhenIClickOnButtonInMyProfilePage(string button)
        {
            Thread.Sleep(5000);
            profilePage.Browser.Click(MyProfileElements.EDIT_BUTTON);
            Console.WriteLine("Successfully clicked Edit button");
        }

        [Then(@"I verify the Edit profile popup is displayed")]
        public void ThenIVerifyTheEditProfilePopupIsDisplayed()
        {
            Thread.Sleep(5000);
            Assert.IsTrue(profilePage.Browser.GetElement(MyProfileElements.EDIT_PROFILE_POPUP).Displayed, "Edit Profile popup is NotFiniteNumberException displayed");
            
        }

        [Then(@"I verify following fields in edit profile popup")]
        public void ThenIVerifyFollowingFieldsInEditProfilePopup(Table table)
        {
            Thread.Sleep(5000);
            profilePage.VerifyEditProfileContents(table);
            Console.WriteLine("Edit profile popup contents are displayed properly");
        }

        [Then(@"I verify ""(.*)"" checkbox")]
        public void ThenIVerifyCheckbox(string checkboxName)
        {
            Thread.Sleep(4000);
           if(checkboxName == "Contact me through mail")
            {
                Assert.IsTrue(profilePage.Browser.GetElements(MyProfileElements.CONTACT_ME_EMAIL_CHECKBOX)[0].Displayed, checkboxName + "is not displayed");
            }
           else if(checkboxName == "Profile made public to SkinAlliance commmunity")
            {
                Assert.IsTrue(profilePage.Browser.GetElements(MyProfileElements.PROFILE_PUBLIC_CHECKBOX)[0].Displayed, checkboxName + "is not displayed");
            }
        }


        [Then(@"I edit the fields in edit profile popup")]
        public void ThenIEditTheFieldsInEditProfilePopup()
        {
            profilePage.EditUserDetails();
            Console.WriteLine("Profile has been updated successfully");
        }

        [Then(@"I enter ""(.*)"" in Zipcode field")]
        public void ThenIEnterInZipcodeField(string zipCode)
        {
            profilePage.Browser.EnterText(MyProfileElements.EDIT_ZIP_CODE, zipCode);
        }

        [Then(@"I enter a ""(.*)"" in activity field")]
        public void ThenIEnterAInActivityField(string ActivityText)
        {
            profilePage.Browser.EnterText(MyProfileElements.EDIT_ACTIVITY, ActivityText);
        }

        [Then(@"I add a resume to my profile")]
        public void ThenIAddAResumeToMyProfile()
        {
            profilePage.SelectResume();
        }

        [Then(@"I upload a new profile picture")]
        public void ThenIUploadANewProfilePicture()
        {
            profilePage.SelectProfilePicture();
        }



        [Then(@"I select a ""(.*)"" from dropdown")]
        public void ThenISelectAFromDropdown(string Option)
        {
            if(Option == "city")
            {
                profilePage.SelectCity();
            }
            else if(Option == "lanugage")
            {
                profilePage.SelectLanguage();
            }
        }



        [When(@"I click on Submit button in edit profile popup")]
        public void WhenIClickOnSubmitButtonInEditProfilePopup()
        {
            profilePage.Browser.Click(MyProfileElements.SUBMIT_BUTTON_POPUP);
           
            
        }

        [When(@"I click on Submit button in change password popup")]
        public void WhenIClickOnSubmitButtonInChangePasswordPopup()
        {
            profilePage.Browser.Click(MyProfileElements.CHANGE_PWD_SUBMIT_BUTTON);
        }


        [Then(@"I verify change password button")]
        public void ThenIVerifyChangePasswordButton()
        {
            Assert.IsTrue(profilePage.Browser.GetElement(MyProfileElements.CHANGE_PASSWORD_BUTTON).Displayed, "Change password button is not displayed");
            
        }

        [Then(@"I verify Change Password popup is displayed")]
        public void ThenIVerifyChangePasswordPopupIsDisplayed()
        {
            Thread.Sleep(5000);
            IWebElement Popup = profilePage.Browser.GetElement(MyProfileElements.CHANGE_PASSWORD_POPUP);
            Assert.IsTrue(Popup.Displayed, "Change password popup is not displayed");
        }

        [Then(@"I should see ""(.*)"" text field")]
        public void ThenIShouldSeeTextField(string PasswordField)
        {
            profilePage.VerifyChangePasswordField(PasswordField);
        }

        [Then(@"I change password in change password popup")]
        public void ThenIChangePasswordInChangePasswordPopup()
        {
            profilePage.ChangeUserPassword();
        }

        [Then(@"I enter ""(.*)"" in old password field")]
        public void ThenIEnterInOldPasswordField(string PassWord)
        {
           
            profilePage.Browser.EnterText(MyProfileElements.CURRENT_PASSWORD_FIELD, PassWord);
        }

        [Then(@"I enter ""(.*)"" in new password text field")]
        public void ThenIEnterInNewPasswordTextField(string Password)
        {
            profilePage.Browser.EnterText(MyProfileElements.NEW_PASSWORD_FIELD, Password);
        }

        [Then(@"I enter ""(.*)"" in confirm password field")]
        public void ThenIEnterInConfirmPasswordField(string Password)
        {
            profilePage.Browser.EnterText(MyProfileElements.CONFIRM_PASSWORD_FIELD, Password);
        }

        [Then(@"I click submit button in popup")]
        public void ThenIClickSubmitButtonInPopup()
        {
            profilePage.Browser.Click(MyProfileElements.CHANGE_PWD_SUBMIT_BUTTON);
        }

       
        
        [Then(@"I enter incorrect password in Current password text field")]
        public void ThenIEnterIncorrectPasswordInCurrentPasswordTextField()
        {
            profilePage.EnterIncorrectCurrentPassword();
        }

        [Then(@"I should see ""(.*)"" error message")]
        public void ThenIShouldSeeErrorMessage(string errorMessage)
        {
            Thread.Sleep(4000);
            if (errorMessage == "Please enter valid Old Password")
            {
                 ErrorText = profilePage.Browser.GetElement(MyProfileElements.CHANGE_PASSWORD_ERROR_MSG).Text;
            }
            else if(errorMessage == "Old Password Required")
            {
                 ErrorText = profilePage.Browser.GetElement(MyProfileElements.CURRENT_PASSWORD_EMPTY_MSG).Text;
            }
            Assert.AreEqual(errorMessage, ErrorText, "Incorrect error message is displayed");
        }

        [Then(@"I should see ""(.*)"" new password error message")]
        public void ThenIShouldSeeNewPasswordErrorMessage(string errorMessage)
        {
            Thread.Sleep(5000);
            string ErrorText = profilePage.Browser.GetElement(MyProfileElements.NEW_PWD_EMPTY_MSG).Text;
            Assert.AreEqual(errorMessage, ErrorText, "Incorrect error message is displayed");
        }

        [Then(@"I should see ""(.*)"" confirm password error message")]
        public void ThenIShouldSeeConfirmPasswordErrorMessage(string errorMessage)
        {
            
            string ErrorText = profilePage.Browser.GetElement(MyProfileElements.CONFIRM_PWD_ERROR_MSG).Text;
            Assert.AreEqual(errorMessage, ErrorText, "Incorrect error message is displayed");
        }



        [When(@"I leave new password field empty")]
        public void WhenILeaveNewPasswordFieldEmpty()
        {
            profilePage.EnterOnlyCurrentPassword();
        }




        [When(@"I click on change password button")]
        public void WhenIClickOnChangePasswordButton()
        {
            Thread.Sleep(3000);
            profilePage.Browser.Click(MyProfileElements.CHANGE_PASSWORD_BUTTON);
        }


    }
}
