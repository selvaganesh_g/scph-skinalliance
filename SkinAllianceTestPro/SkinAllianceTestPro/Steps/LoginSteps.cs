﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using SkinAllianceTestPro.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;


namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class LoginSteps: FooterElements
    {
        private readonly ScenarioContext scenarioContext;
        private LoginPage loginPage;
        private FooterPage footerPage;
        private SignUpPage signupPage;
        private HomePage   homePage;

        public LoginSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            loginPage = new LoginPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            footerPage = new FooterPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            signupPage = new SignUpPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            homePage = new HomePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));

        }

        [When(@"I enter ""(.*)"" in email text field")]
        public void WhenIEnterAsUsername(string UserNameText)
        {
            loginPage.EnterUserCredentials(UserNameText);
        }
    

    [When(@"I enter ""(.*)"" in password field")]
    public void WhenIEnterAsPassword(string PassWord)
    {
            loginPage.EnterPassword(PassWord);
            loginPage.ClickLoginButton();
            Thread.Sleep(3000);
        }


        [When(@"I click Sign in button")]
        public void WhenIClickLoginButton()
        {
            loginPage.ClickLoginButton();
        }

        [Then(@"I verify ""(.*)"" email field error message is displayed")]
        public void ThenIVerifyIsDisplayed(string errorMessage)
        {
            Thread.Sleep(3000);
            loginPage.VerifyEmptyEmailFieldErrorMessage(errorMessage);

        }

        [Then(@"I verify ""(.*)"" as email empty error message")]
        public void ThenIVerifyAsEmailEmptyErrorMessage(string errorMessage)
        {
            Thread.Sleep(3000);
            loginPage.VerifyEmptyEmailFieldErrorMessage(errorMessage);
        }


        [Then(@"I verify ""(.*)"" password field error message is displayed")]
        public void ThenIVerifyPasswordFieldErrorMessageIsDisplayed(string errorMessage)
        {
            Thread.Sleep(3000);
            loginPage.VerifyPasswordFieldErrorMessage(errorMessage);
        }

        [Then(@"I verify ""(.*)"" as error message in signin page")]
        public void ThenIVerifyAsErrorMessageInSigninPage(string errorMessage)
        {
            Thread.Sleep(3000);
            loginPage.VerifyEmptyPasswordFieldErrorMessage( errorMessage);
        }



        [Then(@"I verify Facebook button in sign in page")]
        public void ThenIVerifyFacebookButtonInSignInPage()
        {
            Assert.IsTrue(loginPage.Browser.GetElement(LoginElements.FACEBOOK_SIGNIN).Displayed, "Login with Facebook button is not displayed");
        }

        [Then(@"I click on facebook button")]
        public void ThenIClickOnFacebookButton()
        {
            loginPage.Browser.Click(LoginElements.FACEBOOK_SIGNIN);
        }

        [Then(@"I verify Linkedln button in sign in page")]
        public void ThenIVerifyLinkedlnButtonInSignInPage()
        {
            Assert.IsTrue(loginPage.Browser.GetElement(LoginElements.LINKEDIN_SIGNIN).Displayed, "Login with LinkedIn button is not displayed");
        }

        [Then(@"I click on Linkedln button")]
        public void ThenIClickOnLinkedlnButton()
        {
            loginPage.Browser.Click(LoginElements.LINKEDIN_SIGNIN);
        }



        [Then(@"I verify twitter button in sign in page")]
        public void ThenIVerifyTwitterButtonInSignInPage()
        {
            Assert.IsTrue(loginPage.Browser.GetElement(LoginElements.TWITTER_SIGNIN).Displayed, "Login with twitter button is not displayed");
        }

        [Then(@"I click on twitter button")]
        public void ThenIClickOnTwitterButton()
        {
            loginPage.Browser.Click(LoginElements.TWITTER_SIGNIN);
        }

        [Then(@"I verify ""(.*)"" login page is displayed")]
        public void ThenIVerifyLoginPageIsDisplayed(string SocialPage)
        {
            Thread.Sleep(3000);
            loginPage.VerifySocialLoginPage(SocialPage);
        }

        [When(@"I enter ""(.*)"" in facebook email field")]
        public void WhenIEnterInFacebookEmailField(string EmailField)
        {
            loginPage.Browser.EnterText(LoginElements.FACEBOOK_EMAIL_FIELD, EmailField);
        }

        [Then(@"I enter ""(.*)"" as username in linkedin")]
        public void ThenIEnterAsUsernameInLinkedin(string EmailField)
        {
            loginPage.Browser.EnterText(SignUpElements.LINKEDIN_EMAIL_FIELD, EmailField);
        }

        [Then(@"I enter ""(.*)"" as password in linkedin")]
        public void ThenIEnterAsPasswordInLinkedin(string Password)
        {
            loginPage.Browser.EnterText(SignUpElements.LINKEDIN_PWD_FIELD, Password);
        }


        [When(@"I enter ""(.*)"" in facebook password field")]
        public void WhenIEnterInFacebookPasswordField(string PasswordField)
        {
            loginPage.Browser.EnterText(LoginElements.FACEBOOK_PASSWORD_FIELD, PasswordField);
        }

        [Then(@"I click Authorizeapp button in linkedin")]
        public void ThenIClickAuthorizeappButtonInLinkedin()
        {
            loginPage.Browser.Click(SignUpElements.LINKEDIN_LOGIN_BUTTON);
        }


        [When(@"I click facebook login button")]
        public void WhenIClickFacebookLoginButton()
        {
            
            loginPage.Browser.Click(LoginElements.FACEBOOK_LOGIN_BUTTON);
            Thread.Sleep(5000);
            string URL = loginPage.Browser.GetCurrentUrl();

              if(URL.Contains("social-signup#"))
            {
                Assert.IsTrue(signupPage.Browser.GetElement(SignUpElements.COUNTRY_FIELD).Displayed, "Signup page is not displayed");
            }
                else 
            {
                
                Assert.IsTrue(homePage.Browser.GetElement(HeaderElements.NAV_MENU).Displayed, "Home page is not displayed");
            }

        }



        [Then(@"I enter ""(.*)"" as username in twitter")]
        public void ThenIEnterAsUsernameInTwitter(string UserName)
        {
            loginPage.Browser.EnterText(LoginElements.TWITTER_EMAIL_FIELD, UserName);
        }


        [Then(@"I enter ""(.*)"" as password in twitter")]
        public void ThenIEnterAsPasswordInTwitter(string Password)
        {
            loginPage.Browser.EnterText(LoginElements.TWITTER_PWD_FIELD, Password);
        }

        [Then(@"I click Authorizeapp button in twitter")]
        public void ThenIClickAuthorizeappButtonInTwitter()
        {
            loginPage.Browser.Click(LoginElements.TWITTER_ALLOW);
        }





        [Then(@"I verify ""(.*)"" logo")]
        public void ThenIVerifyLogo()
        {
            // Assert.IsTrue((loginPage.VerifySkinAllianceLogo).PageLogo, "Logo is not displayed" );
            

            //Assert.IsTrue(LoginElements.SKIN_LOGO.), "Logo is not displayed");
        }

        [Then(@"I verify Skin Alliance logo")]
        public void ThenIVerifySkinAllianceLogo()
        {

            loginPage.VerifySkinAllianceLogo();
        }


        [Then(@"I verify Enter your Email text field")]
        public void ThenIVerifyEnterYourEmailTextField()
        {
            loginPage.VerifyEmailTextField();
        }


        [Then(@"I verify Enter your Password text field")]
        public void ThenIVerifyEnterYourPasswordTextField()
        {
            loginPage.VerifyPasswordTextField();
        }



        [Then(@"I verify Login button is displayed")]
        public void ThenIVerifyLoginButtonIsDisplayed()
        {
           loginPage.VerifyLoginButton();
        }

        [When(@"I click login button in signin page")]
        public void WhenIClickLoginButtonInSigninPage()
        {
            loginPage.Browser.Click(LoginElements.LOGIN_BUTTON);
        }

        [Then(@"I click on no longer wish to see these messages checkbox")]
        public void ThenIClickOnNoLongerWishToSeeTheseMessagesCheckbox()
        {
            loginPage.Browser.Click(LoginElements.WELCOME_MESSAGE_CHECKBOX);
        }

        

        [When(@"I click on Forgot your Password link in sign In page")]
        public void WhenIClickOnLinkInSignInPage()
        {
            loginPage.ClickPasswordLink();
        }

        



        [Then(@"I verify country logo in page header")]
        public void ThenIVerifyCountryLogoInPageHeader()
        {
            ScenarioContext.Current.Pending();
        }

        //Forgot Password Steps//

        [Then(@"I verify the footer links are displayed")]
        public void ThenIVerifyTheFooterLinksAreDisplayed()
        {
            Thread.Sleep(4000);
            footerPage.FooterLinkVerification();
            Console.WriteLine("footer links are displayed");
        }


        [Then(@"I should see enter registered email text field")]
        public void ThenIShouldSeeEnterRegisteredEmailTextField()
        {
            Assert.IsTrue(loginPage.VerifyEmailTextField(), "Email Text Field is not displayed");
            Console.WriteLine("Email Text field is displayed");
        }

        [Then(@"I should see enter registered email text field in forgot password page")]
        public void ThenIShouldSeeEnterRegisteredEmailTextFieldInForgotPasswordPage()
        {
            Thread.Sleep(5000);
            Assert.IsTrue(loginPage.Browser.GetElement(LoginElements.EMAIL_TEXTFIELD_FORGOT_PWD).Displayed, "Enter email text field is not displayed");
        }


        [Then(@"I enter ""(.*)"" in enter registered email field")]
        public void ThenIEnterInEnterRegisteredEmailField(string IncorrectMailID)
        {
            loginPage.EnterForgotPasswordEmail(IncorrectMailID);
            Console.WriteLine(" Entered"+ IncorrectMailID + "is entered in email field");
        }

        [When(@"I click on Register Account link")]
        public void WhenIClickOnRegisterAccountLink()
        {
            loginPage.Browser.Click(LoginElements.SIGNUP_LINK);
        }

        [Then(@"I verify ""(.*)"" as success message")]
        public void ThenIVerifyAsSuccessMessage(string Message)
        {
            loginPage.VerifyPasswordResetMessage(Message);
        }


        [Then(@"I verify ""(.*)"" as error message")]
        public void ThenIVerifyAsErrorMessage(string ErrorMessage)
        {
            string Err_Message_Text = loginPage.VerifyErrorMessageForgotPWD();
            Assert.AreEqual(ErrorMessage, Err_Message_Text, "Incorrect Email Address error message is verified successfully");  
        }

        [Then(@"I click Back to homepage link")]
        public void ThenIClickBackToHomepageLink()
        {
            loginPage.ClickBackToHomePageLink();
            Console.WriteLine("Successfully clicked Back to Homepage link");   
        }

        [Then(@"I verify Sign in page is displayed")]
        public void ThenIVerifySignInPageIsDisplayed()
        {
            loginPage.VerifyEmailTextField();
        }

        
        [When(@"I select a country from dropdown")]
        public void WhenISelectACountryFromDropdown()
        {
            loginPage.SelectCountry();
        }


        [When(@"I click language dropdown")]
        public void WhenIClickLanguageDropdown()
        {
            loginPage.Browser.Click(LoginElements.LANGUAGE_DROPDOWN);
        }


        [Then(@"I verify the country flag is changed")]
        public void ThenIVerifyTheCountryFlagIsChanged()
        {
            loginPage.VerifyCountryFlag();
        }



        [Then(@"I verify a login to access popup is displayed")]
        public void ThenIVerifyALoginToAccessPopupIsDisplayed()
        {
            Thread.Sleep(3000);
            Assert.IsTrue(loginPage.Browser.GetElement(LoginElements.FOOTER_POPUP).Displayed, "Login to access popup is not displayed");
        }


        [Then(@"I verify welcome message popup and close it")]
        public void ThenIVerifyWelcomeMessagePopupAndCloseIt()
        {
            
           if(homePage.Browser.GetElement(HeaderElements.NAV_MENU).Displayed)
            {
                Assert.IsTrue(homePage.Browser.GetElement(HeaderElements.HEADER_SECTION).Displayed);
            }
           else
            {
                loginPage.Browser.Click(LoginElements.WELCOME_MESSAGE_CHECKBOX);
            }
        }


        [Then(@"I verify ""(.*)"" popup is displayed")]
        public void ThenIVerifyPopupIsDisplayed(string welcomePopup)
        {
            Thread.Sleep(3000);         
             Assert.IsTrue(loginPage.Browser.GetElement(LoginElements.WELCOME_MESSAGE_POPUP).Displayed, welcomePopup + "is not displayed");            
        }

        [When(@"I click on ""(.*)"" checkbox")]
        public void WhenIClickOnCheckbox(string checkboxText)
        {
            Thread.Sleep(3000);
            loginPage.Browser.Click(LoginElements.CHECKBOX_POPUP);
        }



        [Then(@"I should see close button in popup")]
        public void ThenIShouldSeeCloseButtonInPopup()
        {
         var button = loginPage.Browser.GetElement(LoginElements.POPUP_CLOSE_BUTTON).Displayed;
            Assert.IsTrue(button, "close button is not displayed in the popup");
        }

        [Then(@"I verify welcome message popup is not displayed")]
        public void ThenIVerifyWelcomeMessagePopupIsNotDisplayed()
        { 
             Assert.IsFalse(loginPage.Browser.GetElement(LoginElements.WELCOME_MESSAGE_POPUP).Displayed, "welcome message popup is displayed");
            
        }

        [When(@"I click on the ""(.*)"" link")]
        public void WhenIClickOnTheLink(string registerUserLink)
        {
            loginPage.ClickRegisterUser();
            
        }

        [When(@"I click Remember me checkbox")]
        public void WhenIClickRememberMeCheckbox()
        {
            Thread.Sleep(3000);
            loginPage.Browser.Click(LoginElements.REMEMBER_ME_CHECKBOX);
        }

        [Then(@"I close the application")]
        public void ThenICloseTheApplication()
        {
            ScenarioContext.Current.Pending();
        }

    }
}

