﻿using NUnit.Framework;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class SignUpSteps
    {
        private readonly ScenarioContext scenarioContext;
        private SignUpPage signupPage ;

        public SignUpSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            signupPage = new SignUpPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }

        [When(@"I enter all fields in signup page")]
        public void WhenIEnterAllFieldsInSignupPage()
        {
            //signupPage.CreateNewUser();
            
        }

        [When(@"I click on Submit button")]
        public void WhenIClickOnSubmitButton()
        {
            string URL = signupPage.Browser.GetCurrentUrl();
            if (URL.Contains("social-signup") || URL.Contains("register"))
            {
                signupPage.Browser.Click(SignUpElements.SUBMIT_BUTTON);
            }
            else
            {
                Assert.IsTrue(signupPage.Browser.GetElement(HomePage.NAV_MENU).Displayed, "Home page is not displayed");
            }
        }

        [Then(@"I should see Registration confirmation page")]
        public void ThenIShouldSeeRegistrationConfirmationPage()
        {
            Thread.Sleep(5000);
            Assert.IsTrue(signupPage.VerifyRegistrationConfirmationMsg(), "Registration confirmation message is not displayed");
        }

        [Then(@"I verify Skin Alliance header")]
        public void ThenIVerifySkinAllianceHeader()
        {   
            Thread.Sleep(5000);
            var header = signupPage.Browser.GetElement(HeaderElements.HEADER_SECTION);
            Assert.IsTrue(header.Displayed, "Skin Alliance header section is not displayed");
        }

        [Then(@"I verify ""(.*)"" as heading in signup page")]
        public void ThenIVerifyAsHeadingInSignupPage(string pageHeading)
        {
            signupPage.VerifySignUpPageHeading(pageHeading);
            
        }

        [Then(@"I verify signup fields in signup page")]
        public void ThenIVerifySignupFieldsInSignupPage(Table table)
        {
            signupPage.VerifyCreateUserFields(table);
            Console.WriteLine(table + "field is displayed");
        }

        [Then(@"I verify ""(.*)"" link is displayed")]
        public void ThenIVerifyLinkIsDisplayed(string socialIcon)
        {
            signupPage.VerifySocialIconLinks(socialIcon);
            Console.WriteLine(socialIcon + "link is displayed");
        }

        [Then(@"I click on Linkedin button")]
        public void ThenIClickOnLinkedinButton()
        {
            signupPage.Browser.Click(SignUpElements.SOCIAL_LINKEDIN_LINK);
        }
        
        [When(@"I enter junk characters in ""(.*)"" field")]
        public void WhenIEnterJunkCharactersInField(string fieldName)
        {
            //signupPage.EnterJunkCharactersInField(fieldName);
        }

        [When(@"I enter ""(.*)"" in email address field")]
        public void WhenIEnterInEmailAddressField(string emailAddress)
        {
            signupPage.Browser.EnterText(SignUpElements.EMAIL_FIELD, emailAddress);
        }


        [Then(@"I click on close button in popup")]
        public void ThenIClickOnCloseButtonInPopup()
        {
            signupPage.Browser.Click(LoginElements.WELCOME_MESSAGE_CLOSE_BUTTON);
        }

        



        [Then(@"I click on Register Using Facebook link")]
        public void ThenIClickOnRegisterUsingFacebookLink()
        {
            signupPage.Browser.Click(SignUpElements.SOCIAL_FACEBOOK_LINK);
        }

        [Then(@"I verify facebook popup is displayed")]
        public void ThenIVerifyFacebookPopupIsDisplayed()
        {
           Assert.IsTrue(signupPage.Browser.GetElement(SignUpElements.FACEBOOK_POPUP).Displayed, "Facebook popup is NotFiniteNumberException displayed");
        }


        [When(@"I enter all fields without ""(.*)"" field")]
        public void WhenIEnterAllFieldsWithoutField(string Field)
        {
            //signupPage.EnterSignupField(Field);
            Console.WriteLine(Field + "field is left blank");
        }

        [Then(@"I verify ""(.*)"" as error message in signup page")]
        public void ThenIVerifyAsErrorMessageInSignupPage(string errorMessage)
        {
            
            signupPage.VerifyFieldValidationMessage(errorMessage);
            //Console.WriteLine(errorMessage + "is displayed as the error message");
        }

        [Then(@"I click on Complete profile button")]
        public void ThenIClickOnCompleteProfileButton()
        {
            signupPage.Browser.Click(SignUpElements.COMPLETE_PROFILE_BUTTON);
        }


        [When(@"I enter different password in each password field")]
        public void WhenIEnterDifferentPasswordInEachPasswordField()
        {
           // signupPage.EnterName();
            //signupPage.EnterLastName();
            signupPage.EnterEmailID();
            //signupPage.EnterPhoneNo();
            signupPage.EnterAddressField();
            signupPage.EnterUserCountry();
            //signupPage.EnterPhysicianNo();
            //signupPage.EnterUserName();
            signupPage.EnterIncorrectPasswords();
        }

        [Then(@"I verify the ""(.*)"" success message")]
        public void ThenIVerifyTheSuccessMessage(string successMessage)
        {
            //string message = signupPage.Browser.GetElement(SignUpElements.REGISTRATION_CONFIRMATION_SECTION).FindElements(By.TagName("strong"))[0].Text;
            string message = signupPage.Browser.GetElement(SignUpElements.REGISTRATION_SUCCESS_MSG).Text;
            Assert.AreEqual(successMessage, message, message+ "is displayed");
        }

        [Then(@"I verify Back to Login button in confirmation page")]
        public void ThenIVerifyBackToLoginButtonInConfirmationPage()
        {
            Assert.IsTrue(signupPage.Browser.GetElement(SignUpElements.BACK_TO_LOGIN_BUTTON).Displayed, "Back to login button is not displayed");
        }

        [Then(@"I click on Back to Login button")]
        public void ThenIClickOnBackToLoginButton()
        {
            signupPage.Browser.Click(SignUpElements.BACK_TO_LOGIN_BUTTON);
        }

        [Then(@"I click on ""(.*)"" link")]
  public void ThenIClickOnLink(string link)
{
    if(link == "Register Using Twitter")
            {
                signupPage.Browser.Click(SignUpElements.SOCIAL_TWITTER_LINK);
            }
    else if(link == "Register Using Facebook")
            {
                signupPage.Browser.Click(SignUpElements.SOCIAL_FACEBOOK_LINK);
            }
    else if(link == "Add Question")
            {
                signupPage.Browser.Click(HeaderElements.ADD_QUESTION_LINK);
            }
}


       // [When(@"I enter a invalid email id in email text field")]
       // public void WhenIEnterAInvalidEmailIdInEmailTextField()
        //{
          //  signupPage.EnterInvalidEmailID();
        //}
        

        //////////////// Registration Fields/////////////////////////////

        [When(@"I enter ""(.*)"" in First name field")]
        public void WhenIEnterInFirstNameField(string Name)
        {
            Thread.Sleep(4000);
            signupPage.EnterFirstName(Name);
        }

        [When(@"I enter ""(.*)"" as last name in signup page")]
        public void WhenIEnterAsLastNameInSignupPage(string LastName)
        {
            signupPage.EnterLastName(LastName);
        }

        [When(@"I enter email address in email address field")]
        public void WhenIEnterEmailAddressInEmailAddressField()
        {
            signupPage.EnterEmailID();
        }

        [When(@"I enter user mail id in email text field")]
        public void WhenIEnterUserMailIdInEmailTextField()
        {
            signupPage.EnterStoredEmailID();
        }

        [When(@"I select a random country from country dropdown")]
        public void WhenISelectARandomCountryFromCountryDropdown()
        {
            Thread.Sleep(4000);
            signupPage.EnterUserCountry();
        }

        [When(@"I select ""(.*)"" in country dropdown")]
        public void WhenISelectInCountryDropdown(string p0)
        {
            ScenarioContext.Current.Pending();
        }


        [When(@"I enter ""(.*)"" in phone number field")]
        public void WhenIEnterInPhoneNumberField(string number)
        {
            signupPage.EnterPhoneNo(number);
        }

        [When(@"I enter address in address field")]
        public void WhenIEnterAddressInAddressField()
        {
            signupPage.EnterAddressField();
        }

        [When(@"I enter ""(.*)"" as Physical License number")]
        public void WhenIEnterAsPhysicalLicenseNumber(string licenseNo)
        {
            signupPage.EnterPhysicianNo(licenseNo);
        }

        [When(@"I select medical speciality")]
        public void WhenISelectMedicalSpeciality()
        {
            
            signupPage.EnterMedicalSpeciality();
        }

        [When(@"I select preferred language")]
        public void WhenISelectPreferredLanguage()
        {
            signupPage.SelectPreferredLanugage();
        }


        [When(@"I select medical speciality in twitter registration page")]
        public void WhenISelectMedicalSpecialityInTwitterRegistrationPage()
        {
            signupPage.EnterMedicalSpecialityinTwitter();
        }


        [When(@"I enter ""(.*)"" as username")]
        public void WhenIEnterAsUsername(string userName)
        {
            signupPage.EnterUserName(userName);
        }

        [When(@"I enter ""(.*)"" as password")]
        public void WhenIEnterAsPassword(string password)
        {
            signupPage.EnterUserPassword(password);
        }

        [When(@"I enter ""(.*)"" in confirm password field")]
        public void WhenIEnterInConfirmPasswordField(string ConfirmPassword)
        {
            signupPage.EnterUserConfirmPassword(ConfirmPassword);
        }
                      

    }
}
