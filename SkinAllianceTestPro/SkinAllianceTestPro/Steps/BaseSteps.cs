﻿using log4net;
using NUnit.Framework;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using SkinAllianceTestPro.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace SkinAllianceTestPro.Steps
{

    [TestFixture]
    [Binding]
    public class BaseSteps
    {
        private readonly ScenarioContext scenarioContext;

        public BaseSteps(ScenarioContext scenarioContext)
        {
            if (scenarioContext == null) throw new ArgumentNullException("scenarioContext");
            this.scenarioContext = scenarioContext;

        }

        [BeforeTestRun]
        public static void BeforeTestStart()
        {
            // Logger details

            string logDir = Config.GetLogDir();
            DirectoryInfo dir = new DirectoryInfo(logDir);
            foreach (FileInfo file in dir.GetFiles())
            {
                file.Delete();
            }

            log4net.GlobalContext.Properties["LOG_FILE_NAME"] = logDir + "\\TestCases_" + DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss");
            log4net.Config.XmlConfigurator.Configure();
            log4net.Config.BasicConfigurator.Configure();

        }

        [BeforeScenario]
        public void BeforeEachScenario()
        {
            DriverUtil.LaunchBrowser(scenarioContext);
        }

        [AfterScenario]
        public void AfterEachScenario()
        {
            DriverUtil.KillBrowser(scenarioContext);
        }

        [Given(@"I launch (.+) page")]
        public void LaunchPage(string pageName)
        {
            //  DriverUtil.LaunchBrowser(scenarioContext);

            CommonPage commonPage = new CommonPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            commonPage.Browser.NavigateTo(pageName);
            //  commonPage.Browser.MaximizeWindow();


        }

    }
}
