﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class DirectorySteps
    {
            private readonly ScenarioContext scenarioContext;
            private DirectoryPage directoryPage;
            private HomePage homePage;

            public DirectorySteps(ScenarioContext scenarioContext)
            {
                this.scenarioContext = scenarioContext;
                directoryPage = new DirectoryPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
                homePage = new HomePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));

            }

        [Then(@"I verify ""(.*)"" listing page is displayed")]
        public void ThenIVerifyListingPageIsDisplayed(string Heading)
        {
            string Title = directoryPage.Browser.GetElement(DirectoryElements.PAGE_HEADING).Text;
            //Assert.IsTrue(Heading, Title, "Title displayed is:" + Title);
            Assert.AreEqual(Heading, Title, "Title displayed is:" + Title);
        }

        [Then(@"I verify ""(.*)"" of the users")]
        public void ThenIVerifyOfTheUsers(string Element)
        {
            directoryPage.VerifyUserDetails(Element);
        }

        [Then(@"I verify user list displayed based on ""(.*)""")]
        public void ThenIVerifyUserListDisplayedBasedOn(string SelectedOption)
        {
            Thread.Sleep(4000);
            directoryPage.VerifyFilteredUserList(SelectedOption);
        }



        [Then(@"I verify ""(.*)"" section is displayed in directory page")]
        public void ThenIVerifySectionIsDisplayedInDirectoryPage(string Section)
        {
            if (Section == "Filter by Country")
            {
                Assert.IsTrue(directoryPage.Browser.GetElement(DirectoryElements.FILTER_COUNTRY_SECTION).Displayed, Section + "is not displayed");
            }
            else if(Section == "Filter by speciality")
            {
                Assert.IsTrue(directoryPage.Browser.GetElement(DirectoryElements.FILTER_SPECIALTY_SECTION).Displayed, Section + "is not displayed");
            }

        }

        [When(@"I select a ""(.*)"" from the filter by list")]
        public void WhenISelectAFromTheFilterByList(string Option)
        {
            Thread.Sleep(3000);
                directoryPage.ClickRandomFilterOption(Option);
                  
        }

       

        [When(@"I click a random user name in directory page")]
        public void WhenIClickARandomUserNameInDirectoryPage()
        {
            directoryPage.ClickRandomUserProfile();
        }


        [Then(@"I verify corresponding userprofile page is displayed")]
        public void ThenIVerifyCorrespondingUserprofilePageIsDisplayed()
        {
            directoryPage.VerifyNameInUserProfile();
        }

        [When(@"I click ""(.*)"" button in directory page")]
        public void WhenIClickButtonInDirectoryPage(string button)
        {
            directoryPage.ClickUserDisplayButton(button);
        }

        [Then(@"I verify the list of users ""(.*)"" in directory")]
        public void ThenIVerifyTheListOfUsersInDirectory(string UserDisplay)
        {
            Thread.Sleep(4000);
           if(UserDisplay == "not displayed")
            {
                Assert.IsFalse(directoryPage.Browser.GetElement(DirectoryElements.USERS_LIST_DIRECTORY).Displayed, "Users list is" + UserDisplay);
            }
           else
            {
                Assert.IsTrue(directoryPage.Browser.GetElement(DirectoryElements.USERS_LIST_DIRECTORY).Displayed, "Users list is" + UserDisplay);
            }
        }




    }
}
