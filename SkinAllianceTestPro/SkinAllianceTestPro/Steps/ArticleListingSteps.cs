﻿using NUnit.Framework;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class ArticleListingSteps 
    {
        private readonly ScenarioContext scenarioContext;

        private ArticleDetailsPage articleDetails;

        private ArticleListingPage articleListing;

        public static string contentText;




        public ArticleListingSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            articleListing = new ArticleListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }

        [Then(@"I click on a random article listed")]
        public void ThenIClickOnReadMoreButtonOfAArticle()
        {
            //contentText = articleListing.GetRandomArticleContent();
            articleListing.Browser.Click(contentText);
            Console.WriteLine("Successfully navigated to article details page");
        }

        [Then(@"I verify the articles are listed")]
        public void ThenIVerifyTheArticlesAreListed()
        {
            
        }

        [Given(@"I click ""(.*)"" of a random article")]
        public void GivenIClickOfARandomArticle(string ArticleContent)
        {
            
        }


        [Then(@"I verify ""(.*)"" of the articles in article listing page")]
        public void ThenIVerifyOfTheArticlesInArticleListingPage(string Content)
        {
            if(Content == "Image")
            {
                articleListing.VerifyArticleImage();
            }

            else if(Content == "short description")
            {
                articleListing.VerifyArticleDescription();
            }

            else if(Content == "Comment icon")
            {
                articleListing.VerifyArticlesCommentIcon();
            }
            else if(Content == "Like icon")
            {
                articleListing.VerifyArticlesLikeIcon();
            }
            else if(Content == "Posted date")
            {
                articleListing.VerifyArticleDate();
            }
            else if(Content == "Author Name")
            {
                articleListing.VerifyArticleAuthorName();
            }
            }

        [When(@"I verify articles are in ""(.*)"" page")]
        public void WhenIVerifyArticlesAreInPage(string Section)
        {
            Assert.IsTrue(articleListing.Browser.GetElement(ArticleListingElement.ARTICLE_LISTING_SECTION).Displayed, Section + " Article listing page is not displayed");
        }


        [When(@"I click a random ""(.*)"" in article listing page")]
        public void WhenIClickARandomInArticleListingPage(string articleContent)
            
        {
            Thread.Sleep(4000);
            if (articleContent == "Image")
            {
                articleListing.ClickArticleImage();
            }
           else if(articleContent == "Content text")
            {
                articleListing.ClickArticleDescription();
            }

            else if(articleContent == "Comment icon")
            {
                articleListing.ClickArticleComment();
            }
            else if(articleContent == "Like icon")
            {
                articleListing.ClickArticleImage();
            }
        }

        [Then(@"I click on previous link in breadcrumb in article listing page")]
        public void ThenIClickOnPreviousLinkInBreadcrumbInArticleListingPage()
        {
            articleListing.ClickPreviousBreadcrumb();   
        }

        [When(@"I click on sort option in article listing page")]
        public void WhenIClickOnSortOptionInArticleListingPage()
        {
            articleListing.ClickSortDropdown();
        }

        [Then(@"I sort articles based on ""(.*)""")]
        public void ThenISortArticlesBasedOn(string Sort_Option)
        {
            articleListing.SelectSortOption(Sort_Option);
        }



        [Then(@"I verify the corresponding article detail page")]
        public void ThenIVerifyTheCorrespondingArticleDetailPage()
        {
            Assert.IsTrue((articleDetails.GetArticleDetailPageTitle().Contains(contentText)), "Corresponding Article details page is not displayed");
            Console.WriteLine("Corresponding article details page is displayed");
        }
    }
}
