﻿using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using NUnit.Framework;
using System.Threading;
using SkinAllianceTestPro.Block.ElementBlocks;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class CongressesCategoryListingSteps
    {
        private readonly ScenarioContext scenarioContext;

        private CongressesCategoryListingPage congressescatListPage;

        private HomePage homePage;

        private ArticleListingPage articleListing;


        public static string SelectedCategoryName;

        public static string SelectedSubCategoryName;

        public CongressesCategoryListingSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            congressescatListPage = new CongressesCategoryListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            homePage = new HomePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            articleListing = new ArticleListingPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }


        [When(@"I click on ""(.*)"" from dropdown")]
        public string WhenIClickOnFromDropdown(string subCategory)
        {
            return congressescatListPage.ClickSubCategory(subCategory);
        }

        [Then(@"I verify ""(.*)"" congresses event listing page")]
        public void ThenIVerifyCongressesEventListingPage(string subCategory)
        {
            Thread.Sleep(4000);
            congressescatListPage.VerifyCatEventListingPageTitle(subCategory);
        }

        [When(@"I click on View all link of ""(.*)""")]
        public void WhenIClickOnViewAllLinkOf(string Category)
        {
            congressescatListPage.ClickViewAllLink(Category);
        }



        [Then(@"I verify ""(.*)"" subcategory in dropdown")]
        public void ThenIVerifySubcategoryInDropdown(string Options)
        {
            congressescatListPage.VerifySubCategoryinDropdown(Options);   
        }



        [Then(@"I verify congresses listing page is displayed")]
        public void ThenIVerifyCongressesListingPageIsDisplayed()
        {
            Thread.Sleep(4000);
            string Title = congressescatListPage.Browser.GetElement(CongressesCategoryListingElements.CONGRESSES_CATEGORY_TITLE).Text;
            Assert.IsTrue(Title.Equals("CONGRESSES"), "Congresses listing page is not displayed");
            
        }


        [Then(@"I verify categories in category listing page")]
        public void ThenIVerifyCategoriesInCategoryListingPage(Table table)
        {
            Thread.Sleep(4000);
            congressescatListPage.VerifyCategoriesNames(table);
        }

        [Then(@"I verify ""(.*)"" category in category listing page")]
        public void ThenIVerifyCategoryInCategoryListingPage(string category)
        {
            congressescatListPage.VerifyCategoriesName(category);
        }

        [Then(@"I click on a random view all link")]
        public void ThenIClickOnARandomViewAllLink()
        {
            congressescatListPage.ClickRandomViewAllLink();
        }


        [Then(@"I should see Congresses event listing page")]
        public void ThenIShouldSeeCongressesEventListingPage()
        {
            Thread.Sleep(4000);
            Assert.IsTrue(congressescatListPage.Browser.GetElement(CongressesCategoryListingElements.CONGRESSES_CATEGORY_TITLE).Displayed);
        }

        [When(@"I click on a random congresses in category listing page")]
        public void WhenIClickOnARandomCongressesInCategoryListingPage()
        {
            congressescatListPage.ClickRandomCongresses();
        }


        [Then(@"I verify date of congresses displayed")]
        public void ThenIVerifyDateOfCongressesDisplayed()
        {
            Assert.IsTrue(congressescatListPage.Browser.GetElements(CongressesCategoryListingElements.CONGRESSES_DATE).Count != 0,
                "congresses date is not displayed");
        }

        [Then(@"I verify description of congresses displayed")]
        public void ThenIVerifyDescriptionOfCongressesDisplayed()
        {
            Assert.IsTrue(congressescatListPage.Browser.GetElements(CongressesCategoryListingElements.CONGRESSES_DESC).Count != 0,
                "congresses description is not displayed");
        }

        [Then(@"I verify View all button in category listing page")]
        public void ThenIVerifyViewAllButtonInCategoryListingPage()
        {
            Assert.IsTrue(congressescatListPage.Browser.GetElements(CongressesCategoryListingElements.CONGRESSES_VIEW_ALL_LINK).Count != 0,
                "View all button is not displayed");
        }



    }
}