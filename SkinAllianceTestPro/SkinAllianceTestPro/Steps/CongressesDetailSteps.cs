﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using SkinAllianceTestPro.Steps;
using SkinAllianceTestPro.Framework.Common;

namespace SkinAllianceTestPro.Steps
{
    [Binding]
    public class CongressesDetailSteps 
    {
        private readonly ScenarioContext scenarioContext;
        //private static RemoteWebDriver driverContext;
        private CongressesDetailPage congressesdetailPage;
        //private FooterPage footerPage = new FooterPage(driverContext);
        private HomePage homePage;

        public static string category;

        public static string ActualURL;

        public static string EnteredText;

        public static string DisplayedArticle;

        public static string SelectedSubCategoryName;




        public CongressesDetailSteps(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;

            congressesdetailPage = new CongressesDetailPage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
            homePage = new  HomePage(DriverUtil.GetWebDriverFromFeatureContext(scenarioContext));
        }



        [Then(@"I verify respective congress detail page displayed")]
        public void ThenIVerifyRespectiveCongressDetailPageDisplayed()
        {
            Thread.Sleep(4000);
            Assert.IsTrue(congressesdetailPage.Browser.GetElement(CongressDetailElements.CONGRESSES_PAGE_IMAGE).Displayed, "Congresses detail page is not displayed");
        }


        [Then(@"I verify ""(.*)"" in event detail page")]
        public void ThenIVerifyInEventDetailPage(string Attribute)
        {
            congressesdetailPage.VerifyDetailPage(Attribute);
        }

        [Then(@"I verify other congresses section is displayed")]
        public void ThenIVerifyOtherCongressesSectionIsDisplayed()
        {
            Assert.IsTrue(congressesdetailPage.Browser.GetElement(CongressDetailElements.OTHER_CONGRESSES_SECTION).Displayed, "Other congresses section is not displayed");
        }

        [Then(@"I verify ""(.*)"" displayed in other congresses section")]
        public void ThenIVerifyDisplayedInOtherCongressesSection(string element)
        {
           if(element == "Date")
            {
                Assert.IsTrue(congressesdetailPage.Browser.GetElements(CongressDetailElements.OTHER_CONGRESSES_DATE).Count > 0, element + "is not displayed");
            }
           else if(element == "Small Description")
            {
                Assert.IsTrue(congressesdetailPage.Browser.GetElements(CongressDetailElements.OTHER_CONGRESSES_DESCRIPTION).Count > 0, element + "is not displayed");
            }
        }

        [When(@"I click on a random congresses in other congresses")]
        public void WhenIClickOnARandomCongressesInOtherCongresses()
        {
            congressesdetailPage.ClickRandomCongresses();
        }

        [When(@"I click on register button in detail page")]
        public void WhenIClickOnRegisterButtonInDetailPage()
        {
            congressesdetailPage.Browser.Click(CongressDetailElements.CONGRESSES_REGISTER_BUTTON);
        }

        [Then(@"I verify registration external site is displayed")]
        public void ThenIVerifyRegistrationExternalSiteIsDisplayed()
        {
            congressesdetailPage.Browser.SwitchToBrowserTab();
            string currentURL = homePage.VerifyExternalBrowserURL();
            congressesdetailPage.Browser.SwitchToParentTab();
            Assert.AreNotEqual(currentURL, ActualURL, "External site URL is matching with the Actual URL");
            //Console.WriteLine("External site is displayed");
        }
    




    }
}
