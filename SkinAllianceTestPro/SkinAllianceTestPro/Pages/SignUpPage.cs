﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SkinAllianceTestPro.Pages
{
    public class SignUpPage : SignUpElements
    {
        public WebDriverService Browser;

        public static string headingDisplayed;

        public Boolean socialLink;

        public static string Error_Message_Displayed;

        public static string MailID;

        public SignUpPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
           
        }

        public void EnterFirstName(string Name)
        {
            Browser.EnterText(FIRST_NAME_FIELD, Name);
        }

        public void EnterLastName(string Name)
        {
            Browser.EnterText(LAST_NAME_FIELD, Name);
        }

        
        public string EnterEmailID()
        {
            Random rnd = new Random();
            int i = rnd.Next(0, 1000000);
            Browser.EnterText(EMAIL_FIELD, "qatestingloreal" + i + "@test.com");
             MailID = Browser.GetElement(EMAIL_FIELD).GetAttribute("value");
            return MailID;
                }

        public void EnterStoredEmailID()
        {
            Browser.EnterText(LoginElements.USERNAME_BOX, MailID);
        }


        public void EnterPhoneNo( string number)
        {
            Browser.EnterText(PHONE_NO_FIELD, number);
        }

        public void EnterAddressField()
        {
            Random rnd = new Random();
            int i = rnd.Next(0, 1000000);
            Browser.EnterText(SignUpElements.ADDRESS_FIELD, "123"+i+ "xadfs street");
        }

        public void EnterUserCountry()
        {
           string URL = Browser.GetCurrentUrl();
           if(URL.Contains("social-signup") || URL.Contains("register"))
            {

                SelectElement e = new SelectElement(Browser.GetElement(COUNTRY_FIELD));
                
                 e.SelectByIndex(Util.GetRandomNumber(e.Options.Count()));

            }
            else
            {
                Assert.IsTrue(Browser.GetElement(HomePage.NAV_MENU).Displayed, "Home page is not displayed");
            }
        }

        public void EnterPhysicianNo(string number)
        {
            string URL = Browser.GetCurrentUrl();
            if (URL.Contains("social-signup") || URL.Contains("register"))
            {
                Browser.EnterText(PHYSICAL_LICENSE_FIELD, number);
            }
            else
            {
                Assert.IsTrue(Browser.GetElement(HomePage.NAV_MENU).Displayed, "Home page is not displayed");
            }
        }

        public void EnterMedicalSpeciality()
        {
            string URL = Browser.GetCurrentUrl();
            if  (URL.Contains("register"))
            {

                SelectElement e = new SelectElement(Browser.GetElement(MEDICAL_SPECIALITY_FIELD));
                e.SelectByIndex(Util.GetRandomNumber(e.Options.Count()));

            }
            else if(URL.Contains("social-signup"))
            {
                Browser.Click(MEDICAL_SPECIALITY_FIELD);
                Browser.GetElement(MEDICAL_SPECIALITY_FIELD).SendKeys("Dermatologist");

               // SelectElement e = new SelectElement(Browser.GetElement(FACEBOOK_MEDICAL_SPECIALITY_FIELD));
               // e.SelectByIndex(Util.GetRandomNumber(e.Options.Count()));
            }    

            else
            {

                Assert.IsTrue(Browser.GetElement(HomePage.NAV_MENU).Displayed, "Home page is not displayed");
            }
           
        }

        public void SelectPreferredLanugage()
        {
            string URL = Browser.GetCurrentUrl();
            if (URL.Contains("social-signup") || URL.Contains("register"))
            {
               
                SelectElement e = new SelectElement(Browser.GetElement(PREFERED_LANUGAGE_DROPDOWN));
                e.SelectByIndex(Util.GetRandomNumber(e.Options.Count()));
            }
            else
            {

                Assert.IsTrue(Browser.GetElement(HomePage.NAV_MENU).Displayed, "Home page is not displayed");
            }
        }

        public void EnterMedicalSpecialityinTwitter()
        {
            string URL = Browser.GetCurrentUrl();
            if (URL.Contains("social-signup"))
            {
                Browser.Click(MEDICAL_SPECIALITY_FIELD);
                Browser.GetElement(MEDICAL_SPECIALITY_FIELD).SendKeys("Dermatologist");


                //SelectElement e = new SelectElement(Browser.GetElement(TWITTER_SPECIALITY_FIELD));
                //e.SelectByIndex(Util.GetRandomNumber(e.Options.Count()));
            }
            else
            {

                Assert.IsTrue(Browser.GetElement(HomePage.NAV_MENU).Displayed, "Home page is not displayed");
            }

        }
    
        public void EnterUserName(string userName)
        {
           
            Browser.EnterText(USERNAME_FIELD, userName);
        }

        public void EnterUserPassword(string password)
        {
            Browser.EnterText(SignUpElements.PASSWORD_FIELD, password);
        }

        public void EnterUserConfirmPassword(string password)
        { 
            Browser.EnterText(SignUpElements.PASSWORD_CONFIRMATION_FIELD, password);
        }

        public void EnterIncorrectPasswords()
        {
            Browser.EnterText(PASSWORD_FIELD, "Loreal#user");
            Browser.EnterText(PASSWORD_CONFIRMATION_FIELD, "Loreal@user");
        }
       

        public Boolean VerifyRegistrationConfirmationMsg()
        {
            return Browser.GetElement(SignUpElements.REGISTRATION_SUCCESS_MSG).Displayed;
        }

        public void VerifySignUpPageHeading(string Heading)
        {
            if (Heading == "WELCOME TO SKINALLIANCE")
            {
                headingDisplayed = Browser.GetElement(SKIN_ALLIANCE_HEADING).Text;   
            }

            else if(Heading == "New User Registration")
            {
               
                headingDisplayed = Browser.GetElement(NEW_USER_HEADING).Text;
            }

            else
            {
                Assert.Fail(Heading+ "heading is not displayed");
            }

            Assert.IsTrue(Heading.Equals(headingDisplayed), "Expected is:" +Heading+ "Actual is:" +headingDisplayed);

            //return headingDisplayed;
        }

        public void VerifyCreateUserFields(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps TextField in rows)
            {
                switch (TextField.Attributes)
                {
                    case "First Name":
                        Assert.IsTrue(Browser.GetElement(SignUpElements.FIRST_NAME_FIELD).Displayed, TextField + "field is not displayed");
                        break;
                    case "Last Name":
                        Assert.IsTrue(Browser.GetElement(SignUpElements.LAST_NAME_FIELD).Displayed, TextField + "field is not displayed");
                        break;
                    case "Phone Number":
                        Assert.IsTrue(Browser.GetElement(SignUpElements.PHONE_NO_FIELD).Displayed, TextField + "field is not displayed");
                        break;
                    case "Address":
                        Assert.IsTrue(Browser.GetElement(SignUpElements.ADDRESS_FIELD).Displayed, TextField + "field is not displayed");
                        break;
                    case "Country":
                        Assert.IsTrue(Browser.GetElement(SignUpElements.COUNTRY_FIELD).Displayed, TextField + "field is not displayed");
                        break;
                    case "Physical License number":
                        Assert.IsTrue(Browser.GetElement(SignUpElements.PHYSICAL_LICENSE_FIELD).Displayed, TextField + "field is not displayed");
                        break;
                    case "Username":
                        Assert.IsTrue(Browser.GetElement(SignUpElements.USERNAME_FIELD).Displayed, TextField + "field is not displayed");
                        break;
                    case "Password":
                        Assert.IsTrue(Browser.GetElement(SignUpElements.PASSWORD_FIELD).Displayed, TextField + "field is not displayed");
                        break;
                    case "Password Confirmation":
                        Assert.IsTrue(Browser.GetElement(SignUpElements.PASSWORD_CONFIRMATION_FIELD).Displayed, TextField + "field is not displayed");
                        break;
                    case "Submit Button":
                        Assert.IsTrue(Browser.GetElement(SignUpElements.SUBMIT_BUTTON).Displayed, TextField + "field is not displayed");
                        break;
                    default:
                        break;
                        //Assert.Fail(TextField + "is not displayed");
                        //break;
                }
            }
        }

        public Boolean VerifySocialIconLinks(string socialSite)
        {
            
            if (socialSite == "Register with Facebook")
            {
                socialLink = Browser.GetElement(SOCIAL_FACEBOOK_LINK).Displayed;
            }

            else if (socialSite == "Register with Twitter")
            {
                socialLink = Browser.GetElement(SOCIAL_TWITTER_LINK).Displayed;
            }

            else if(socialSite == "Register with LinkedIn")
            {
                socialLink = Browser.GetElement(SOCIAL_LINKEDIN_LINK).Displayed;
            }
            else
            {
                Assert.Fail(socialSite + "link is not displayed");
            }

            Assert.IsTrue(socialLink, "link is not displayed");
            return socialLink;

        }

        public void VerifyErrorMessage(string ErrorMessage)
        {


            string ErrorMsg = Browser.GetElement(ERROR_MESSAGE).Text;
            
            Assert.AreEqual(ErrorMessage, ErrorMsg, ErrorMsg + "is displayed as the error message");
        }
        

        public void VerifyFieldValidationMessage(string ErrorMessage)

        {
            if (ErrorMessage == "Please enter First Name")
            {
                Error_Message_Displayed = Browser.GetElement(FIRST_NAME_ERROR_MESSAGE).Text;
            }
            else if (ErrorMessage == "Please enter Last Name")
            {
                Error_Message_Displayed = Browser.GetElement(LAST_NAME_ERROR_MESSAGE).Text;
            }
            else if(ErrorMessage == "Please select Country")
            {
                Error_Message_Displayed = Browser.GetElement(COUNTRY_FIELD_ERROR_MESSAGE).Text;
            }
            else if(ErrorMessage == "Please enter valid email address")
            {
                Error_Message_Displayed = Browser.GetElement(EMAIL_FIELD_ERROR_MESSAGE).Text;

            }
            else if(ErrorMessage == "Please enter Physician License")
            {
                Error_Message_Displayed = Browser.GetElement(PHYSICIAN_FIELD_ERROR_MESSAGE).Text;
            }

            else if(ErrorMessage == "Please Select Medical Specialty")
            {
                Error_Message_Displayed = Browser.GetElement(SPECIALTY_ERROR_MESSAGE).Text;
            }
            else if(ErrorMessage == "Please enter password" || ErrorMessage == "Password must be contain at least eight characters, at least one number and both lower and uppercase letters and special characters.")
            {
                Error_Message_Displayed = Browser.GetElement(PWD_ERROR_MESSAGE).Text;
            }
            else if(ErrorMessage == "Please enter valid password")
            {
                Error_Message_Displayed = Browser.GetElement(INVALID_PWD_ERROR_MESSAGE).Text;
            }

            else if(ErrorMessage == "Please enter email address")
            {
                Error_Message_Displayed = Browser.GetElement(EMAIL_FIELD_ERROR_MESSAGE).Text;
            }
            else if(ErrorMessage == "Please enter Confirm password" || ErrorMessage == "Confirm password is not matching the password")
            {
                Error_Message_Displayed = Browser.GetElement(CONFIRM_PWD_ERROR_MESSAGE).Text;
            }


            Assert.AreEqual(ErrorMessage, Error_Message_Displayed, "Incorrect error message is displayed");

            
        }


        }
   }
    

