﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using System.Threading;
using TechTalk.SpecFlow;
using System.Collections;
using TechTalk.SpecFlow.Assist;
using System.Data;
using SkinAllianceTestPro.Steps;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Framework.Common.EnumType;
using SkinAllianceTestPro.Steps;
using OpenQA.Selenium;
using SkinAllianceTestPro.Block.ElementBlocks;

namespace SkinAllianceTestPro.Pages
{
    public class CongressesEventListingPage : CongressEventListingElements
    {
        public WebDriverService Browser;

        public static string Component;

        public static string PageTitle;



        public CongressesEventListingPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void VerifyElementsinPage(string Element)
        {
            if(Element == "Category Heading")
            {
                Assert.IsTrue(Browser.GetElement(PAGE_HEADING).Displayed, Element + "is not displayed");
            }

            else if(Element == "Image")
            {
                Assert.IsTrue(Browser.GetElements(CONGRESS_IMAGE).Count != 0, Element + "is not displayed");
            }

            else if(Element == "Date")
            {
                Assert.IsTrue(Browser.GetElements(CONGRESS_DATE).Count != 0, Element + "is not displayed");
            }

            else if(Element == "Small description")
            {
                Assert.IsTrue(Browser.GetElements(CONGRESS_DATE).Count != 0, Element + "is not displayed");
            }
        }

        public void ClickRandomCongressName()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(CONGRESS_DESC).Count);
            Browser.GetElements(CONGRESS_DESC)[i].Click();
        }


    }

}