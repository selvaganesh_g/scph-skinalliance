﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using System.Threading;
using TechTalk.SpecFlow;
using System.Collections;
using TechTalk.SpecFlow.Assist;
using System.Data;
using SkinAllianceTestPro.Steps;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Framework.Common.EnumType;
using SkinAllianceTestPro.Steps;
using OpenQA.Selenium;

namespace SkinAllianceTestPro.Pages
{
    public class ScientificBoardDetailPage :ScientificBoardMemberElements
    {
        public WebDriverService Browser;

        public static string SelectedValue;

        public static string UserDetail;

        public static string SelectedUserName;

        public ScientificBoardDetailPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void VerifyScientificBoardItems(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps BoardItem in rows)
            {
                switch (BoardItem.Attributes)
                {

                    case "Description":
                        Assert.IsTrue(Browser.GetElements(BOARD_MEMBERS_DESC).Count !=0, BoardItem + "is not displayed");
                        break;
                    case "Image":
                        Assert.IsTrue(Browser.GetElements(BOARD_MEMBERS_IMAGE).Count !=0, BoardItem + "is not displayed");
                        break;
                    case "Name":
                        Assert.IsTrue(Browser.GetElements(BOARD_MEMBERS_NAME).Count != 0, BoardItem + "is not displayed");
                        break;
                    case "Specialty":
                        Assert.IsTrue(Browser.GetElements(BOARD_MEMBERS_POSITION).Count !=0, BoardItem + "is not displayed");
                        break;
                    case "Introduction":
                        Assert.IsTrue(Browser.GetElements(PAGE_DESCRIPTION).Count !=0, BoardItem + "is not displayed");
                        break;


                }
            }
        }

        public string ClickRandomScientificMember(string Element)
        {
            if (Element == "Name")
            {
                int i = Util.GetRandomNumber(Browser.GetElements(BOARD_MEMBERS_NAME).Count);
                SelectedUserName = Browser.GetElements(BOARD_MEMBERS_NAME)[i].Text;
                Browser.GetElements(BOARD_MEMBERS_NAME)[i].Click();
                
            }
            else
            {
                int i = Util.GetRandomNumber(Browser.GetElements(BOARD_MEMBERS_IMAGE).Count);
                SelectedUserName = Browser.GetElements(BOARD_MEMBERS_NAME)[i].Text;
                Browser.GetElements(BOARD_MEMBERS_IMAGE)[i].Click();
            }
            return SelectedUserName;
        }
    }
}
