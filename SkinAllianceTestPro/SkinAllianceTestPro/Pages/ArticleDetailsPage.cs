﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using System.Threading;
using TechTalk.SpecFlow;
using SkinAllianceTestPro.Block.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using OpenQA.Selenium;

namespace SkinAllianceTestPro.Pages
{
    public class ArticleDetailsPage : ArticleDetailsElement
    {
        public WebDriverService Browser;

        public static string CommentText;

        public ArticleDetailsPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public String GetArticleDetailPageTitle()
        {
            string DetailPageTitle = Browser.GetElement(ArticleDetailsElement.ARTICLE_TITLE).Text;
            return DetailPageTitle;
        }

        public void PostReplyForComment()
        {
            var ReplyLink = Browser.GetElements(REPLY_LINK)[0];
            Browser.GetElements(REPLY_LINK)[0].Click();
            CommentText = "good article";
            Browser.EnterText(REPLY_TEXT_BOX, "good article");
            Browser.GetElements(REPLY_SUBMIT_BUTTON)[0].Click();

        }

        public void VerifyPostedReply()
        {
            string Posted_Reply = Browser.GetElements(POSTED_REPLY)[0].Text;
            Assert.IsTrue(Posted_Reply.Contains(CommentText), "Posted Reply is not displayed");
        }
        public void GetArticleDetailContents(string content)
        {
            if (content == "Posted by : ")
            {
                Assert.IsTrue(Browser.GetElement(POSTED_BY_NAME).Displayed, content + "is not displayed");

            }

            else if (content == "Posted")
            {
                Assert.IsTrue(Browser.GetElement(POSTED_BY_DATE).Displayed, content + "is not displayed");
            }

            else if (content == "image")
            {
                for (int i = 0; i < Browser.GetElements(ARTICLE_IMAGE).Count; i++)
                {
                    var element = Browser.GetElements(ARTICLE_IMAGE);
                    Assert.IsTrue(element[i].Displayed, content + "is not displayed");

                }
            }
            else if(content == "Category")
            {
                Assert.IsTrue(Browser.GetElement(CATEGORY_TITLE).Displayed, content+ "is not displayed");

            }

            else if (content == "description")
            {
                Assert.IsTrue(Browser.GetElement(ARTICLE_CONTENT).Displayed, content + "is not displayed");
            }

            else if (content == "Access Website")
            {
                Assert.IsTrue(Browser.GetElement(ACCESS_WEBSITE_LINK).Displayed, content + "is not displayed");
            }
            else if (content == "Report to Moderator")
            {
                Assert.IsTrue(Browser.GetElement(REPORT_MODERATOR_LINK).Displayed, content + "is not displayed");
            }

            else if(content == "Comments Posted by other users")
            {
                Assert.IsTrue(Browser.GetElement(ARTICLE_COMMENTS).Displayed, content + "is not displayed");
            }
            else
            {
                Assert.Fail("Article details page is not displayed");
            }
        }

        public void GetCommentsImage()
        {
            int i = Browser.GetElement(COMMENT_SECTION).FindElements(By.TagName("img")).Count;
            Assert.IsTrue(i >= 1, "Images are not displayed for comments");
        }

        public void GetCommentsUserName()
        {
             Assert.IsTrue(Browser.GetElements(COMMENTS_USER_NAME).Count >=1, "User names are not displayed");
        }

        public void GetCommentsPostedDate()
        {
            int i = Browser.GetElement(COMMENT_SECTION).FindElements(By.TagName("span")).Count;
            Assert.IsTrue(i >= 1, "Posted date is not displayed");
        }
        public IList<IWebElement> GetRelatedPostPicture()
        {
            return Browser.GetElements(RELATED_POST_IMAGE);
        }

        public void GetRelatedPostUser()
        {
              int i = Browser.GetElement(RELATED_POST_SECTION).FindElements(By.TagName("p")).Count;
            Assert.IsTrue(i >= 1, "Posted date is not displayed");
        }

        public IList<IWebElement> GetRelatedPostLikes()
        {
            return Browser.GetElements(RELATED_POST_LIKE);

        }

        public IList<IWebElement> GetRelatedPostComments()
        {
            return Browser.GetElements(RELATED_POST_COMMENT);
        }

        public IList<IWebElement> GetRelatedPostLikesCount()
        {
            return Browser.GetElements(RELATED_POST_LIKE_COUNT);
        }

        public IList<IWebElement> GetRelatedPostCommentsCount()
        {
            return Browser.GetElements(RELATED_POST_COMMENT_COUNT);
        }

        public Boolean VerifyCommentsSection()
        {
            return  Browser.GetElement(ARTICLE_COMMENTS).Displayed;
            
        }

        public string ClickRandomUserRelatedPost()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(RELATED_POST_USERNAME).Count);
            string UserName = Browser.GetElements(RELATED_POST_USERNAME)[i].Text;
            Browser.GetElements(RELATED_POST_USERNAME)[i].Click();
            return UserName;
                }

        

        public void SelectRandomSlides()
        {
            int i = Util.GetRandomNumber(Browser.GetElement(WEBCAST_SLIDES).FindElements(By.TagName("img")).Count);
            Browser.GetElement(WEBCAST_SLIDES).FindElements(By.TagName("img"))[i].Click();

        }

        public void PostComment()
        {
            CommentText = "good Article";
            Browser.ScrollTo(POST_COMMENT_BUTTON);
            Browser.EnterText(ARTICLE_COMMENTS_FIELD, CommentText);
            Assert.IsTrue(Browser.GetElement(POST_COMMENT_BUTTON).Displayed, "Send button is not displayed");
            Browser.Click(POST_COMMENT_BUTTON);
        }

        //public IList<IWebElement> VerifyPostedComment()
        //{
            //iwe i = Browser.GetElement(COMMENT_SECTION).FindElements(By.TagName("p")).Last;
            

           // List<IWebElement> comments = Browser.GetElement(COMMENT_SECTION).FindElements(By.TagName("p"));
           // List




       // }

        public void ClickRandomWebcast()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(ARTICLE_IMAGE).Count);
            Browser.GetElements(ARTICLE_IMAGE)[i].Click();
        }
       
        

        public void VerifyReportModeratorMessage(string message)
        {
            string messageDisplayed = Browser.GetElement(REPORT_CONFIRMATION_MSG).Text;
            Assert.AreEqual(message, messageDisplayed, "Confirmation message is not displayed");
            
        }

        public Boolean VerifyRelatedPost()
        {
            return Browser.GetElement(RELATED_POST_SECTION).Displayed;
        }

        public void VerifyExternalVideoTab()
        {
            Browser.SwitchToBrowserTab();
            string URL = Browser.GetCurrentUrl();
            Assert.IsTrue(URL.Contains("player.vimeo.com"), "Video is not working");
        }
        public void ClickRandomRelatedPost()
        {
            int i = Util.GetRandomNumber(Browser.GetElement(RELATED_POST_SECTION).FindElements(By.TagName("p")).Count);
            Browser.GetElement(RELATED_POST_SECTION).FindElements(By.TagName("p"))[i].Click();
        }
    }
}

