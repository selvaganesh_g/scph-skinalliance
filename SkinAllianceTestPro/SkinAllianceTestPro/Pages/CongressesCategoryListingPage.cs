﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using System.Threading;
using TechTalk.SpecFlow;
using System.Collections;
using TechTalk.SpecFlow.Assist;
using System.Data;
using SkinAllianceTestPro.Steps;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Framework.Common.EnumType;
using SkinAllianceTestPro.Steps;
using OpenQA.Selenium;
using SkinAllianceTestPro.Block.ElementBlocks;

namespace SkinAllianceTestPro.Pages
{
    public class CongressesCategoryListingPage : CongressesCategoryListingElements
    {
        public WebDriverService Browser;

        public static string Category;

        public static string PageTitle;

        public CongressesCategoryListingPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void VerifyCategoriesNames(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps BoardItem in rows)
            {
                switch (BoardItem.Attributes)
                {
                    case "DERMATOLOGY":
                        Category = Browser.GetElement(DERMATOLOGY_CATEGORY).Text;
                        Assert.IsTrue(Category.Equals("DERMATOLOGY", StringComparison.InvariantCultureIgnoreCase), BoardItem + "is not displayed");
                        break;
                    case "ESTHETIC":
                        Category = Browser.GetElement(ESTHETIC_CATEGORY).Text;
                        Assert.IsTrue((BoardItem.Equals(Category)), BoardItem + "is not displayed");
                        break;
                    case "RESEARCH":
                        Category = Browser.GetElement(RESEARCH_CATEGORY).Text;
                        Assert.IsTrue((BoardItem.Equals(Category)), BoardItem + "is not displayed");
                        break;
                }


            }
        }

        public void VerifyCategoriesName(string categoryName)
        {
            if (categoryName == "DERMATOLOGY")
            {
                Category = Browser.GetElement(DERMATOLOGY_CATEGORY).Text;

            }

            else if (categoryName == "ESTHETIC")
            {
                Category = Browser.GetElement(ESTHETIC_CATEGORY).Text;
            }

            else if (categoryName == "RESEARCH")
            {
                Category = Browser.GetElement(RESEARCH_CATEGORY).Text;
            }

            Assert.AreEqual(categoryName, Category, categoryName + "is not displayed");
        }

        public string ClickSubCategory(string category)
        {
            if (category == "DERMATOLOGY")
            {
                Category = Browser.GetElement(DERMATOLOGY_DROPDOWN).Text;
                Browser.Click(DERMATOLOGY_DROPDOWN);

            }

            else if (category == "ESTHETIC")
            {
                Category = Browser.GetElement(ESTHETIC_DROPDOWN).Text;
                Browser.Click(ESTHETIC_DROPDOWN);
            }

            else if (category == "RESEARCH")
            {
                Category = Browser.GetElement(RESEARCH_DROPDOWN).Text;
                Browser.Click(RESEARCH_DROPDOWN);
            }

            return Category;

        }

        public void VerifyCatEventListingPageTitle(string Title)
        {
            //PageTitle = Browser.GetElement(CONGRESSES_CATEGORY_TITLE).Text;
            //Assert.AreEqual(Title, Category, "Expected: "+ Title + "Actual: " + Category);
            Assert.IsTrue(Browser.GetElement(CONGRESSES_CATEGORY_TITLE).Displayed, "Congresses Event listing page is not displayed");
        }


        public void ClickViewAllLink(string Congress_category)
        {
            if(Congress_category == "DERMATOLOGY")
            {
                Browser.GetElements(CONGRESSES_VIEW_ALL_LINK)[0].Click();
            }
            else if(Congress_category == "ESTHETIC")
            {
                Browser.GetElements(CONGRESSES_VIEW_ALL_LINK)[1].Click();
            }
            else if(Congress_category == "RESEARCH")
            {
                Browser.GetElements(CONGRESSES_VIEW_ALL_LINK)[2].Click();
            }

        }
        public void VerifySubCategoryinDropdown(string subCategory)
        {
            if(subCategory == "DERMATOLOGY")
            {
                Category = Browser.GetElement(DERMATOLOGY_DROPDOWN).Text;
            }

            else if(subCategory == "ESTHETIC")
            {
                Category = Browser.GetElement(ESTHETIC_DROPDOWN).Text;
            }
            else if(subCategory == "RESEARCH")
            {
                Category = Browser.GetElement(RESEARCH_DROPDOWN).Text;
            }
            Assert.AreEqual(subCategory, Category, "Expected: " + subCategory + "Actual: " + Category);
        }
        
        public void ClickRandomViewAllLink()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(CONGRESSES_VIEW_ALL_LINK).Count);
            Browser.GetElements(CONGRESSES_VIEW_ALL_LINK)[i].Click();
        }

        public void ClickRandomCongresses()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(CONGRESSES_DESC).Count);
            Browser.GetElements(CONGRESSES_DESC)[i].Click();
        }
}

}