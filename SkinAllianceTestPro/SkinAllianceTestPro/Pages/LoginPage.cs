﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using System.Threading;
using TechTalk.SpecFlow;
using SkinAllianceTestPro.Framework.Common;
using OpenQA.Selenium;

namespace SkinAllianceTestPro.Pages
{
    public class LoginPage : LoginElements
    {
        public WebDriverService Browser;

        public static string CurrentImage;

        public LoginPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void VerifyWelcomeMessageText()
        {
            
           Assert.IsTrue((Browser.GetElement(WELCOME_MESSAGE).Displayed), "Welcome Message is dispalyed properly");
        }
        public void EnterUserCredentials(string Text)
        {
            
            Browser.EnterText(USERNAME_BOX, Text);
            
        }

        public void EnterPassword(string PassWordText)
        {
            Browser.EnterText(PWD_BOX, PassWordText);
           
            
        }

        public void VerifySkinAllianceLogo()
        {
            //bool Element;

            //Element = Browser.GetElement(LoginElements.SKIN_LOGO).Displayed;


            Assert.IsTrue(Browser.GetElement(LoginElements.SKIN_LOGO).Displayed, "Skin Alliance logo is not displayed");
            Console.WriteLine("Skin alliance logo is displayed");
        }

        public Boolean VerifyEmailTextField()
        {
            //bool Email;
            return Browser.GetElement(USERNAME_BOX).Displayed;

        }

        public void VerifyPasswordTextField()
        {
            //bool Password;

            Assert.IsTrue(Browser.GetElement(PWD_BOX).Displayed, "Password Text field is not dispalyed");
            Console.WriteLine("password text field is displayed");
        }

        public void VerifyLoginButton()
        {
            //bool Login;
            Assert.IsTrue(Browser.GetElement(LOGIN_BUTTON).Displayed, "Login button is not displayed");
            Console.WriteLine("Login button is displayed");
        }

        public  void VerifyEmailFieldErrorMessage(string ErrorMessage)
        {
            string ErrorMessageDisplayed = Browser.GetElement(FIELD_VALIDATION_MESSAGE).Text;
            Assert.AreEqual(ErrorMessage, ErrorMessageDisplayed, "Email error message is not matching");
             
        }

        public void VerifyEmptyEmailFieldErrorMessage(string ErrorMessage)
        {
            string ErrorMessageDisplayed = Browser.GetElement(EMAIL_EMPTY_MESSAGE).Text;
            Assert.AreEqual(ErrorMessage, ErrorMessageDisplayed, "Email error message is not matching");
        }

        public void VerifyPasswordFieldErrorMessage(string ErrorMessage)
        {
            string ErrorMessageDisplayed = Browser.GetElement(PASSWORD_VALIDATION_MESSAGE).Text;
            Assert.AreEqual(ErrorMessage, ErrorMessageDisplayed, "Password field error message is not matching");
        }

        public void VerifyEmptyPasswordFieldErrorMessage(string ErrorMessage)
        {
            string ErrorMessageDisplayed = Browser.GetElement(PASSWORD_EMPTY_MESSAGE).Text;
            Assert.AreEqual(ErrorMessage, ErrorMessageDisplayed, "Password field error message is not matching");
        }
        public void VerifyCountryLogo()
        {
            Assert.IsTrue(Browser.GetElement(COUNTRY_FLAG).Displayed, "Country logo is not displayed");
            Console.WriteLine("Country logo is displayed");
        }

        public void ClickPasswordLink()
        {
            Browser.Click(FORGOT_PASSWORD_LINK);
            
        }



        public void ClickLoginButton()
        {
            Browser.Click(LOGIN_BUTTON);
        }

        //Forgot Password Related Methods//

        
        public void ForgotPasswordVerifyEmailField()
        {
            Assert.IsTrue((Browser.GetElement(EMAIL_TEXTFIELD_FORGOT_PWD).Displayed), "Email text field is displayed in forgot password page");
            Console.WriteLine("Successfully verified forgot password email field");
        }

        public void EnterForgotPasswordEmail(string EmailAddress)
        {
            Browser.EnterText(EMAIL_TEXTFIELD_FORGOT_PWD, EmailAddress);
            Browser.Click(SEND_BUTTON_FORGOT_PWD);

        }
        
        public void VerifyPasswordResetMessage(string Message)
        {
           string Success_Message = Browser.GetElement(FORGOT_PWD_SUCCESS_MSG).Text;
            Assert.AreEqual(Message, Success_Message, "Expected message is: " + Message + "Actual message is: " + Success_Message);
        }
        public void ClickBackToHomePageLink()

        {
            Browser.Click(BACK_TO_HOMEPAGE_LINK);
        }

        public String VerifyErrorMessageForgotPWD()
        {
            string Forgot_Error_Msg = Browser.GetElement(ERROR_MESSAGE_FORGOT_PWD).Text;
            return Forgot_Error_Msg;
        }

        public Boolean VerifyWelcomeMessagePopup()
        {
            return Browser.GetElement(WELCOME_MESSAGE_POPUP).Displayed;
        }

        public void ClickRegisterUser()
        {
            Browser.Click(SIGNUP_LINK);
        }

       
        public void SelectCountry()
        {
            CurrentImage = Browser.GetElement(LANGUAGE_DROPDOWN).FindElement(By.TagName("img")).GetAttribute("src");
            Browser.Click(LANGUAGE_DROPDOWN);
            int i = Util.GetRandomNumber(Browser.GetElement(LANGUAGE_DROPDOWN).FindElements(By.TagName("a")).Count);
            Browser.GetElement(LANGUAGE_DROPDOWN).FindElements(By.TagName("a"))[i].Click();
        }

        public void VerifyCountryFlag()
        {
            string NewImage = Browser.GetElement(LANGUAGE_DROPDOWN).FindElement(By.TagName("img")).GetAttribute("src");
            Assert.AreNotEqual(CurrentImage, NewImage, "Expected is:" + CurrentImage + "Actual is:" + NewImage);
        }
        public void VerifySocialLoginPage(string Page)
        {
            string URL = Browser.GetCurrentUrl();

            if(Page == "facebook")
            {
                Assert.IsTrue(URL.Contains("facebook"), Page+ "login page is not displayed");
            }
            else if(Page == "twitter")
            {
                Assert.IsTrue(URL.Contains("twitter"), "Twitter Signin Page is not displayed");
            }
            else if(Page == "Linkedin")
            {
                Assert.IsTrue(URL.Contains("linkedin"), "Linkedin signin page is displayed");
            }
        }
       
    }
}
