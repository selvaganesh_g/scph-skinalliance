﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using NUnit.Framework;

namespace SkinAllianceTestPro.Pages
{
   public class CategoryListingPage : CategoryListingElements
    {
        public WebDriverService Browser;
        
        public static string CategoryName;

        public static string SubCategory;

        public CategoryListingPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void ClickRandomCategoryName()
        {
            // int i = Util.GetRandomNumber(Browser.GetElements(CATEGORY_LIST_NAME).Count);
            //string CategoryName = Browser.GetElements(CATEGORY_LIST_NAME)[i].Text;
            //Browser.GetElements(CATEGORY_LIST_NAME)[i].Click();
            int i = Util.GetRandomNumber(Browser.GetElement(CATEGORY_LIST_SECTION).FindElements(By.TagName("h2")).Count);
             CategoryName = Browser.GetElement(CATEGORY_LIST_SECTION).FindElements(By.TagName("h2"))[i].Text;
            Browser.GetElement(CATEGORY_LIST_SECTION).FindElements(By.TagName("h2"))[i].Click();
            
        }

        public void VerifySubCategoryName()
        {
            string Title = Browser.GetElement(SubCategoryListingElements.SUB_CATEGORY_PAGE_TITLE).Text;
            Assert.IsTrue(Title.Equals(CategoryName, StringComparison.InvariantCultureIgnoreCase), "Expected is:" +CategoryName+ "Actual is:" +Title);
        }
        public string ClickRandomSubCategory()
        {
            int a = Browser.GetElement(CATEGORY_LIST_SECTION).FindElements(By.TagName("a")).Count;
            int i = Util.GetRandomNumber(Browser.GetElement(CATEGORY_LIST_SECTION).FindElements(By.TagName("a")).Count);

            if (a == 1)
            {
                SubCategory = Browser.GetElement(CATEGORY_LIST_SECTION).FindElement(By.TagName("a")).Text;
                Browser.GetElement(CATEGORY_LIST_SECTION).FindElement(By.TagName("a")).Click();
            }
            else
            {
                //int i = Util.GetRandomNumber(Browser.GetElements(SUB_CATEGORY_NAMES).Count);
                SubCategory = Browser.GetElement(CATEGORY_LIST_SECTION).FindElements(By.TagName("a"))[i].Text;
                Browser.GetElement(CATEGORY_LIST_SECTION).FindElements(By.TagName("a"))[i].Click();
                
            }

            return SubCategory;
        }

        public IList<IWebElement> GetCategoryNames()
        {
          return Browser.GetElements(CATEGORY_LIST_NAME);
        }

        public IList<IWebElement> GetSubCategoryNames()
        {
            return Browser.GetElements(SUB_CATEGORY_NAMES);
        }
    }
}
