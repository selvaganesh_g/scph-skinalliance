﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using System.Threading;
using TechTalk.SpecFlow;
using System.Collections;
using TechTalk.SpecFlow.Assist;
using System.Data;
using SkinAllianceTestPro.Steps;
using SkinAllianceTestPro.Framework.Common.EnumType;

namespace SkinAllianceTestPro.Pages
{
    [Binding]
    public class FooterPage : FooterElements
    {
        public WebDriverService Browser;

        public FooterPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void ClickFooterLinks(string footerLink)
        {
            if(footerLink == "Terms of Use")
            {
                Browser.Click(FOOTER_TERMS_CONDITIONS);
            }
            else if(footerLink == "Legal")
            {
                Browser.Click(FOOTER_LEGAL);
            }
            else if(footerLink == "Cookie and Privacy Policy")
                {
                Browser.Click(FOOTER_PRIVACY_POLICY);
            }
                 
        }

        public void VerifyFooterPageHeading(string pageHeading)
        {
            if(pageHeading == "TERMS OF USE")
            {
                Browser.GetElement(FooterElements.TERMS_CONDITIONS_HEADING);
            }
            else if(pageHeading == "LEGAL")
            {
                Browser.GetElement(FooterElements.LEGAL_HEADING);
            }
            else if(pageHeading == "COOKIE AND PRIVACY POLICY")
            {
                Browser.GetElement(FooterElements.PRIVACY_POLICY_HEADING);
            }
            else
            {
                Assert.Fail("Footer links are not displayed");
            }
        }


        public void FooterLinkVerification()
        {

            Assert.IsTrue((Browser.GetElement(FOOTER_SECTION).Displayed), "Footer section is not displayed");
        }

        public void VerifyFooterLinks(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps FooterLinks in rows)
            {
                switch (FooterLinks.Attributes)
                {
                    case "Terms of Use":

                        Assert.IsTrue(Browser.GetElement(FooterElements.FOOTER_TERMS_CONDITIONS).Displayed, "Footer link" + FooterLinks + "is not displayed");
                        break;
                    case "Legal":
                        Assert.IsTrue(Browser.GetElement(FooterElements.FOOTER_LEGAL).Displayed, "Footer link" + FooterLinks + "is not displayed");
                        break;
                    case "Cookie and Privacy Policy":
                        Assert.IsTrue(Browser.GetElement(FooterElements.FOOTER_PRIVACY_POLICY).Displayed, "Footer link" + FooterLinks + "is not displayed");
                        break;
                    default:
                        Assert.Fail("Footer section is not displayed");
                        break;
                }
            }

        }

        public void VerifyContactUsFields(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps Fields in rows)
            {
                switch(Fields.Attributes)
                {
                    case "First Name":
                        Assert.IsTrue(Browser.GetElement(CONTACT_FIRST_NAME).Displayed, Fields+ "is not displayed");
                        break;
                    case "Last Name":
                        Assert.IsTrue(Browser.GetElement(CONTACT_LAST_NAME).Displayed, Fields + "is not displayed");
                        break;
                    case "Email":
                        Assert.IsTrue(Browser.GetElement(CONTACT_EMAIL_ADDRESS).Displayed, Fields + "is not displayed");
                        break;
                    case "Phone Number":
                        Assert.IsTrue(Browser.GetElement(CONTACT_PHONE_NO).Displayed, Fields + "is not displayed");
                        break;
                    case "Message":
                        Assert.IsTrue(Browser.GetElement(CONTACT_MESSAGE).Displayed, Fields + "is not displayed");
                        break;
                    case "Submit Button":
                        Assert.IsTrue(Browser.GetElement(CONTACT_SUBMIT_BUTTON).Displayed, Fields + "is not displayed");
                        break;
                    default:
                        Assert.Fail("Contact us page is not dispalyed properly");
                        break;
                }
            }
        }


       public void GetErrorMessage(string errorMessage)
        {
            if(errorMessage == "Please enter the First Name")
            {
                ERROR_MESSAGE = Browser.GetElement(FIRST_NAME_FIELD_MSG).Text;
            }

            else if(errorMessage == "Please enter the Last Name")
            {
                ERROR_MESSAGE = Browser.GetElement(LAST_NAME_FIELD_MSG).Text;
            }
            else if(errorMessage == "Please enter the Email" || errorMessage == "Please enter a valid Email Address")
            {
                ERROR_MESSAGE = Browser.GetElement(EMAIL_FIELD_MSG).Text;

            }
            else if(errorMessage == "Please enter the Message")
            {
                ERROR_MESSAGE = Browser.GetElement(MESSAGE_FIELD_MSG).Text;
            }
            else if(errorMessage == "Please enter Phone Number" || errorMessage == "Please enter valid Phone Number")
            {
                ERROR_MESSAGE = Browser.GetElement(PHONENO_FIELD_MSG).Text;
            }

            Assert.AreEqual(errorMessage, ERROR_MESSAGE, "Expected is:" + errorMessage + "Actual is: " + ERROR_MESSAGE);
        }


        public void GetSuccessMessage(string succesMessage)
        {
            string messageDisplayed = Browser.GetElement(SUCCESS_MESSAGE).Text;
            Assert.AreEqual(succesMessage, messageDisplayed, "Incorrect success message is not displayed");
        }


    }


}

