﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using System.Threading;
using TechTalk.SpecFlow;
using System.Collections;
using TechTalk.SpecFlow.Assist;
using System.Data;
using SkinAllianceTestPro.Steps;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Framework.Common.EnumType;
using OpenQA.Selenium;
using log4net;
using System.IO;
using System.Reflection;
using OpenQA.Selenium.Interactions;

namespace SkinAllianceTestPro.Pages
{
    public class HomePage : HeaderElements
    {
        public WebDriverService Browser;

        public static string  menuText;

       public static IWebElement menu;

        public static string CategoryName;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public HomePage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void MenuSelection(string menuOption)
        {
      
            if (Browser.ResponsiveType == ResponsiveType.MOBILE)
            {
                Browser.Click(M_HAMBUERGER_MENU);
            }
            Thread.Sleep(1000);
            Browser.Click(string.Format(MIXA_MENU, menuOption.ToLower()));
      

        }
        public void ProductSelection(string productName, string productCat)
        {
            string locateProduct = string.Format(PRODUCT_SELECTION, productName, productCat);
            Browser.ScrollTo(locateProduct);
            Browser.ScrollBy(-70);
            Browser.Click(locateProduct);
            Thread.Sleep(10000);

        }

        public void UploadFile()
        {
            string exeDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            exeDir = exeDir.Replace("SkinAllianceTestPro\\bin\\Debug", "");
            Browser.GetElement(UPLOAD_BUTTON).SendKeys(exeDir + "\\Resources\\Resume\\Resume.txt");
        }

        public void UploadPostImage()
        {
            string exeDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            exeDir = exeDir.Replace("SkinAllianceTestPro\\bin\\Debug", "");
            Browser.GetElement(ADD_IMAGE_SECTION).SendKeys(exeDir + "Resources\\Images\\Image.png");
            logger.Debug("Working directory --==-=-=-=-=->>>dasd>ADs>" + exeDir);
        }


        public void SelectProduct(string rowId, string sheetName)
        {
            string category = ExcelUtil.GetCellValue(Config.Read(""), sheetName, rowId, "Category");
            string productName = ExcelUtil.GetCellValue(Config.Read(""), sheetName, rowId, "ProductName");
            ProductSelection(productName, category);
        }

        public void SelectProduct(string sheetName, Table productTable)
        {
            string rowId = string.Empty;
            string filePath = Config.Read("DATA_SOURCE_FILE");
            string category = string.Empty;
            string productName = string.Empty;

            foreach (TableRow row in productTable.Rows)
            {
                rowId = row["RowId"];

                category = ExcelUtil.GetCellValue(filePath, sheetName, rowId, "Category");
                productName = ExcelUtil.GetCellValue(filePath, sheetName, rowId, "ProductName");

                MenuSelection("PRODUCTS");
                ProductSelection(productName, category);
                Thread.Sleep(5000);

            }
        }

        public void selectProducts(string sheetName, int fromRowId, int toRowId, string columnNames)
        {
            string category = string.Empty;
            string productName = string.Empty;

            DataTable dt = ExcelUtil.GetRecordsFromExcel(Config.Read("DATA_SOURCE_FILE"), sheetName, fromRowId, toRowId, columnNames);

            foreach (DataRow row in dt.Rows)
            {
                category = Convert.ToString(row["Category"]);
                productName = Convert.ToString(row["ProductName"]);

                MenuSelection("PRODUCTS");
                ProductSelection(productName, category);
                Thread.Sleep(5000);
            }

        }

        

        public void selectProducts(string sheetName, List<TableContent> rowList, string columnNames)
        {
            string category = string.Empty;
            string productName = string.Empty;
            DataTable dt = ExcelUtil.GetRecordsFromExcel(Config.Read("DATA_SOURCE_FILE"), sheetName, rowList, columnNames);

            foreach (DataRow row in dt.Rows)
            {
                category = Convert.ToString(row["Category"]);
                productName = Convert.ToString(row["ProductName"]);
               

                MenuSelection("PRODUCTS");
                ProductSelection(productName, category);
                Thread.Sleep(5000);
            }

        }

        public String SelectRandomCategory()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(NAV_MENUS).Count);
           
            string value = Browser.GetElements(NAV_MENUS)[i].Text;
            Browser.GetElements(NAV_MENUS)[i].Click();
            return value;
            //Browser.Click(MENU_BUTTON);
          //  Browser.FindElements(LocatorType.CLASSNAME, "cat_menu");
           // Browser.FindElement(LocatorType.LINKTEXT, category, Browser.GetElement(MENU_OPTIONS)).Click();
            //Thread.Sleep(1000);
            
        }

        public void ClickNavigationMenu(string Navigation)
        {
            
        }

        public String RandomCategory( string category_menu)
        {
            int i = Browser.GetElements(NAV_MENU).Count;
            Random rnd = new Random();
            i = rnd.Next(0, i - 1);
            var Item = Browser.GetElements(NAV_MENU)[i];
            string Menu = Browser.GetElements(NAV_MENU)[i].Text;
            Browser.HoverElement(Item);
            Browser.HighLightElement(Item);
            Assert.AreEqual(category_menu, Menu, "Incorrect category menu is highlighted");
            return Menu;
        }

        public void HighlightCategoryMenu(string cat_menu)
        {
            
         

            if(cat_menu == "DERMOCOSMETICS KNOWLEDGE")
            {
                menu = Browser.GetElement(MENU_KNOWLEDGE);
                  
            }

            else if (cat_menu == "Community")
            {
                menu = Browser.GetElement(MENU_COMMUNITY);
                 //Browser.Click(MENU_COMMUNITY);
                //Browser.Click(DIRECTORY_LINK);
            }
            
            else if(cat_menu == "WEBCASTS")
            {
                menu = Browser.GetElement(MENU_WEBCASTS);
            }
           
            else if(cat_menu == "CONGRESSES")
            {
                menu = Browser.GetElement(MENU_CONGRESSES);
            }
            Browser.HoverElement(menu);
        }

        public void VerifyCategoryMenuState()
        {
            Assert.IsTrue(Browser.GetElements(DROPDOWN_MENUS).Count >= 4, "Corresponding menu is not expanded");
        }

        public void ClickCategoryMenu(string Menu)
        {
            if (Menu == "HOME")
            {
                Browser.Click(MENU_HOME);
            }

            else if (Menu == "DERMOCOSMETICS KNOWLEDGE")
            {
                Browser.Click(MENU_KNOWLEDGE);
            }

            else if (Menu == "EXPERTS COMMENTS")
            {
                Browser.Click(MENU_EXPERT_COMMENTS);
            }
            else if (Menu == "WEBCASTS")
            {
                Browser.Click(MENU_WEBCASTS);

            }
            else if(Menu == "CONGRESSES")
            {
                Browser.Click(MENU_CONGRESSES);
            }
            else if (Menu == "CLINICAL CASES")
            {
                Browser.Click(MENU_CLINICAL);

            }
            else if (Menu == "COMMUNITY")
            {
                Browser.Click(MENU_COMMUNITY);

            }
            else if(Menu == "CREATE POST")
            {
                Browser.Click(MENU_CREATE_POST);
            }
            else
            {
                Assert.Fail(Menu + " is not displayed in navigation menu");
            }
        }

        public void ClickRandomPartnerLogo()
        {
            int i = Util.GetRandomNumber(Browser.GetElement(FooterElements.PARTNER_LOGO_SECTION).FindElements(By.TagName("img")).Count);
            Browser.GetElement(FooterElements.PARTNER_LOGO_SECTION).FindElements(By.TagName("img"))[i].Click();
            
        } 

        public string VerifyExternalBrowserURL()
        {
            string url = Browser.GetCurrentUrl();
            return url;
        }

        public void ClickPageLogo()
        {
            Browser.Click(SKIN_LOGO);
        }

        public void EnterSearchText( string Search_Keyword)
        {
            Browser.EnterText(SEARCH_TEXT, Search_Keyword);
            Browser.GetElement(SEARCH_TEXT).SendKeys(Keys.Enter);
           //Browser.Click(SEARCH_MAGNIFIER_ICON);
        }

        public void VerifySearchPageHeading()
        {
          string Page_Heading =  Browser.GetElement(PAGE_HEADING).Text;
            Assert.IsTrue(Page_Heading.Equals("SEARCH"), "Expected is: SEARCH Actual is: " + Page_Heading);
        }
        
      
        public Boolean VerifyImageCarouselSlider()
        {
            return Browser.GetElement(IMAGE_CAROUSEL_SECTION).Displayed; 
        }

        public IList<IWebElement> VerifyImageCarousel()
        {
            return Browser.GetElements(ARTICLE_IMAGE);
        }

        public IList<IWebElement> VerifyImageTitle()
        {
            return Browser.GetElements(ARTICLE_TITLE_IMAGE);
            //int i = Browser.GetElements(ARTICLE_TITLE_IMAGE).Count;
            //return Browser.GetElement(ARTICLE_TITLE_IMAGE).Displayed;
        }

        public IList<IWebElement> VerifyImageDescription()
        {
            return Browser.GetElements(ARTICLE_DESCRIPTION_IMAGE);
        }

        public void ClickRandomPost(string article)
        {
            if(article == "Latest")
            {
                var Latest_Post_Section = Browser.GetElements(POST_SECTION)[0];
               int i = Util.GetRandomNumber(Latest_Post_Section.FindElements(By.TagName("a")).Count);
                Latest_Post_Section.FindElements(By.TagName("a"))[i].Click();
            }
            else if(article == "Favorite")
            {
                var Favorite_Post_Section = Browser.GetElements(POST_SECTION)[1];
                int i = Util.GetRandomNumber(Favorite_Post_Section.FindElements(By.TagName("a")).Count);
                Favorite_Post_Section.FindElements(By.TagName("a"))[i].Click();
            }
            else if(article == "News")
            {
                var News_Section = Browser.GetElements(POST_SECTION)[2];
                int i = Util.GetRandomNumber(News_Section.FindElements(By.TagName("a")).Count);
                News_Section.FindElements(By.TagName("a"))[i].Click();
            }
        }

        public void ClickRandomViewAllLink(string article)
        {
            if (article == "Latest")
            {
                //var Latest_Post_Section = Browser.GetElements(POST_SECTION)[0];
                //Latest_Post_Section.FindElements(By.LinkText("View All"))
                Browser.GetElements(POST_VIEW_ALL_LINK)[0].Click();
               
            }
            else if (article == "Favorite")
            {
                Browser.GetElements(POST_VIEW_ALL_LINK)[1].Click();
            }
            else if (article == "News")
            {
                Browser.GetElements(POST_VIEW_ALL_LINK)[2].Click();
            }
        }

        public void ClickRandomCategory()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(CATEGORY_SLIDER_TITLE).Count);
            CategoryName = Browser.GetElements(CATEGORY_SLIDER_TITLE)[i].FindElement(By.TagName("a")).Text;
            logger.Debug(CategoryName);
            Browser.GetElements(CATEGORY_SLIDER_TITLE)[i].FindElement(By.TagName("a")).Click();
        }

        public void VerifyPopularCategoriesElements(string element)
        {
            if(element == "name")
            {
                Assert.IsTrue(Browser.GetElement(CATEGORY_SLIDER_TITLE).FindElements(By.TagName("a")).Count > 0, element+ "is not displayed");
            }

            else if(element == "logo")
            {
                Assert.IsTrue(Browser.GetElement(CATEGORY_SLIDER_SECTION).FindElements(By.TagName("img")).Count > 0, element + "is not displayed");
            }
        }

        public void VerifySubCategoryPageTitle()
        {
            string PageTitle = Browser.GetElement(SubCategoryListingElements.SUB_CATEGORY_PAGE_TITLE).Text;
            Assert.IsTrue(CategoryName.Equals(PageTitle, StringComparison.InvariantCultureIgnoreCase), "Expected is: " + CategoryName + "Actual is:" + PageTitle);

        }
        public void VerifyHomeComponents(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps HomeContents in rows)
            {
                switch (HomeContents.Attributes)
                {
                    case "Header":

                        Assert.IsTrue(Browser.GetElement(HeaderElements.HEADER_SECTION).Displayed, HomeContents+ "is not displayed in home page");
                        break;
                    case "Footer":
                        Assert.IsTrue(Browser.GetElement(FooterElements.FOOTER_SECTION).Displayed, HomeContents + "is not displayed in home page");
                        break;
                    case "Navigation menu":
                        Assert.IsTrue(Browser.GetElement(HeaderElements.NAV_MENU).Displayed, HomeContents + "is not displayed in home page");
                        break;
                    case "Image carousel slider":
                        Assert.IsTrue(Browser.GetElement(HeaderElements.IMAGE_CAROUSEL_SECTION).Displayed, HomeContents + "is not displayed in home page");
                        break;
                    case "Latest Posts":
                        Assert.IsTrue(Browser.GetElements(HeaderElements.LATEST_POST_SECTION)[0].Displayed, HomeContents + "is not displayed in home page");
                        break;
                    case "Favourite Post":
                        Assert.IsTrue(Browser.GetElements(HeaderElements.LATEST_POST_SECTION)[1].Displayed, HomeContents + "is not displayed in home page");
                        break;
                    case "News":
                        Assert.IsTrue(Browser.GetElements(HeaderElements.LATEST_POST_SECTION)[2].Displayed, HomeContents + "is not displayed in home page");
                        break;
                    case "Board Members":
                        Assert.IsTrue(Browser.GetElement(HeaderElements.BOARD_SCIENTIFIC_SECTION).Displayed, HomeContents + "is not displayed in home page");
                        break;
                    case "Image Logo":
                        Assert.IsTrue(Browser.GetElement(HeaderElements.USER_PICTURE_IMAGE).Displayed, HomeContents + "is not displayed in home page");
                        break;
                    case "Search":
                        Assert.IsTrue(Browser.GetElement(HeaderElements.SEARCH_TEXT).Displayed, HomeContents + "is not displayed in home page");
                        break;
                    default:
                        Assert.Fail("Page is not loaded properly");
                        break;
                }
            }

        }


        public void ClickRandomDropdown()
        {
            int i = Util.GetRandomNumber(Browser.GetElement(MENU_COMMUNITY).FindElements(By.TagName("li")).Count);
            string value = Browser.GetElement(MENU_COMMUNITY).FindElements(By.TagName("li"))[i].Text;
            Browser.GetElement(MENU_COMMUNITY).FindElements(By.TagName("li"))[i].Click();

        }

        public void NavigateToSkinBoxPages(string Page)
        {
            //Browser.Click(USER_PICTURE_IMAGE);
           if(Page == "MY PROFILE")
            {
                Browser.Click(PROFILE_DROPDOWN);
            }
           else if(Page == "MY NOTIFICATION")
                {
               
                Browser.Click(NOTIFY_DROPDOWN);
            }
            else if (Page == "GET WEBCLIPPER")
            {
                Browser.Click(WEBCLIPPER_DROPDOWN);
            }
            else if(Page == "CREATE POST")
            {
                Browser.Click(CREATE_POST_DROPDOWN);
            }
           else if(Page == "LOGOUT")
            {
                Browser.Click(LOGOUT_DROPDOWN);
            }
            
        }
        
        public void VerifySkinBoxDropdownMenus(string Menu)
        { 
            
            if (Menu == "MY PROFILE")
            {
               
                    menuText = Browser.GetElement(PROFILE_DROPDOWN).Text;
                }                      
                
           
          else if(Menu == "MY NOTIFICATION")
            {
                menuText = Browser.GetElement(NOTIFY_DROPDOWN).Text;
            }

            else if (Menu == "GET WEBCLIPPER")
            {
                menuText = Browser.GetElement(WEBCLIPPER_DROPDOWN).Text;
            }

            else if(Menu == "CREATE POST")
            {
                menuText = Browser.GetElement(CREATE_POST_DROPDOWN).Text;
            }

           else if(Menu == "LOGOUT")
            {
                menuText = Browser.GetElement(LOGOUT_DROPDOWN).Text;
            }
            else if(Menu == "GET WEBCLIPPER")
            {
                menuText = Browser.GetElement(WEBCLIPPER_DROPDOWN).Text;
            }

            Assert.IsTrue(Menu.Equals(menuText, StringComparison.InvariantCultureIgnoreCase), "Expected is:" + Menu + "Actual is:" + menuText);
        }


        public string ClickRandomCongresses()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(CONGRESSES_POST).Count);
            string CongressesTitle = Browser.GetElements(CONGRESSES_POST)[i].Text;
            Browser.GetElements(CONGRESSES_POST)[i].Click();
            return CongressesTitle;
        }
   

        public void ClickUserPicture()
        {
            Browser.Click(USER_PICTURE_DROPDOWN);
        }

        public Boolean VerifyNavigationMenu()
        {
             return Browser.GetElement(NAV_MENU).Displayed;
        }
        
        public void VerifyMenuCategories(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps menuItem in rows)
            {
                switch (menuItem.Attributes)
                {
                    case "HOME":
                        
                        Assert.IsTrue(Browser.GetElement(MENU_HOME).Displayed, menuItem + "is not displayed");
                        break;
                    case "DERMOCOSMETICS KNOWLEDGE":
                        Assert.IsTrue(Browser.GetElement(MENU_KNOWLEDGE).Displayed, menuItem + "is not displayed");
                        break;
                    case "EXPERTS COMMENTS":
                        Assert.IsTrue(Browser.GetElement(MENU_EXPERT_COMMENTS).Displayed, menuItem + "is not displayed");
                        break;
                    case "WEBCASTS":
                        Assert.IsTrue(Browser.GetElement(MENU_WEBCASTS).Displayed, menuItem + "is not displayed");
                        break;
                    case "CONGRESSES":
                        Assert.IsTrue(Browser.GetElement(MENU_CONGRESSES).Displayed, menuItem + "is not displayed");
                        break;
                    case "CLINICAL CASES":
                        Assert.IsTrue(Browser.GetElement(MENU_CLINICAL).Displayed, menuItem + " is not displayed");
                        break;
                    case "COMMUNITY":
                        Assert.IsTrue(Browser.GetElement(MENU_COMMUNITY).Displayed, menuItem + "is not displayed");
                        break;
                    case "+CREATE POST":
                        Assert.IsTrue(Browser.GetElement(MENU_CREATE_POST).Displayed, menuItem + "is not displayed");
                        break;
                    
                }
            }
        }

        public void VerifyCreatePostComponents(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps Elements in rows)
            {
                switch(Elements.Step)
                {
                    case "Enter title field":
                        Assert.IsTrue(Browser.GetElement(ENTER_POST_TITLE).Displayed, Elements + "is not displayed");
                        break;
                    case "Text input section":
                        Assert.IsTrue(Browser.GetElement(ENTER_CONTENT_FIELD).Displayed, Elements + "is not displayed");
                        break;
                    case "Website URL field":
                        Assert.IsTrue(Browser.GetElement(ENTER_URL_FIELD).Displayed, Elements + "is not displayed");
                        break;
                    case "File Upload field":
                        Assert.IsTrue(Browser.GetElement(UPLOAD_BUTTON).Displayed, Elements + "is not displayed");
                        break;
                    case "Add Media button":
                        Assert.IsTrue(Browser.GetElement(ADD_MEDIA_BUTTON).Displayed, Elements + "is not displayed");
                        break;
                    case "Browse button":
                        Assert.IsTrue(Browser.GetElement(BROWSE_BUTTON).Displayed, Elements + "is not displayed");
                        break;
                    case "Questionnaire":
                        Assert.IsTrue(Browser.GetElement(QUESTION_HEADING).Displayed, Elements + "is not displayed");
                        break;
                    
                }
            }
        }

        public void VerifyPreviewPostTab()
        {
            Browser.SwitchToBrowserTab();
            string CurrentURL = Browser.GetCurrentUrl();
            Assert.IsTrue(CurrentURL.Contains("/create-post/preview"));
            Browser.SwitchToParentTab();
        }

        public void SelectRandomCategoryinPost()
        {
            int i = Util.GetRandomNumber(Browser.GetElement(CATEGORY_CHECKBOX_SECTION).FindElements(By.TagName("span")).Count);
            
            Browser.GetElement(CATEGORY_CHECKBOX_SECTION).FindElements(By.TagName("span"))[i].Click();

          // int i = Util.GetRandomNumber(Browser.GetElements(CATEGORY_CHECKBOX_SECTION).Count);
            //string a = Browser.GetElements(CATEGORY_CHECKBOX_SECTION)[i].Text;
            //Browser.GetElements(CATEGORY_CHECKBOX_SECTION)[i].Click();

            //int Subcategory = Browser.GetElements(CATEGORY_CHECKBOX_SECTION)[i].FindElements(By.TagName("span")).Count();
            //Thread.Sleep(4000);
            
           // Browser.GetElements(CATEGORY_CHECKBOX_SECTION)[i].FindElements(By.TagName("span"))[i].Click();
            
            
           //Browser.GetElements(CATEGORY_CHECKBOX_SECTION)[i].Click();
           //var b = 
           //int a =  Browser.GetElement(CATEGORY_CHECKBOX_SECTION).FindElements(By.TagName("span")).Count();

        }



        public void SelectLinkFromCommunity(string link)
        {
            if(link == "About Us")
            {
                Browser.Click(ABOUT_US_LINK);
            }
        }

        }
    }

