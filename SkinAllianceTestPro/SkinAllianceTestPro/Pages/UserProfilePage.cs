﻿using log4net;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;


namespace SkinAllianceTestPro.Pages
{
    public class UserProfilePage : UserProfileElements
    {
        public WebDriverService Browser;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public UserProfilePage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void VerifyUserProfileContents(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps profileContent in rows)
            {
                switch (profileContent.Attributes)
                {
                    case "Profile Picture":
                        Assert.IsTrue(Browser.GetElement(PROFILE_USER_PICTURE).Displayed, profileContent + "is not displayed");
                        break;
                    case "Profile Name":
                        Assert.IsTrue(Browser.GetElement(PROFILE_USER_NAME).Displayed, profileContent + "is not displayed");
                        break;
 
                    case "Phone Number":
                        Assert.IsTrue(Browser.GetElement(PROFILE_USER_PHONE_NO).Displayed, profileContent + "is not displayed");
                        break;
                    case "Email Id":
                        Assert.IsTrue(Browser.GetElement(PROFILE_USER_EMAIL_ID).Displayed, profileContent + "is not displayed");
                        break;
                    case "Follow Button":
                        Assert.IsTrue(Browser.GetElement(USER_FOLLOW_BUTTON).Displayed, profileContent + "is not displayed");
                        break;
                    case "Summary Section":
                        Assert.IsTrue(Browser.GetElement(PROFILE_SUMMARY_SECTION).Displayed, profileContent + "is not displayed");
                        break;
                    case "Following Section":
                        Assert.IsTrue(Browser.GetElement(PROFILE_FOLLOWING_SECTION).Displayed, profileContent + "is not displayed");
                        break;
                    case "Followed By":
                        Assert.IsTrue(Browser.GetElement(PROFILE_FOLLOWED_BY_SECTION).Displayed, profileContent + "is not displayed");
                        break;
                }
            }
        }

        public void VerifyUserSection(string Section)
        {
            if(Section == "Followed By")
            {
                Assert.IsTrue(Browser.GetElement(PROFILE_FOLLOWED_BY_SECTION).Displayed, Section + "is not displayed");
            }
            else if(Section == "Following")
            {
                Assert.IsTrue(Browser.GetElement(PROFILE_FOLLOWING_SECTION).Displayed, Section + "is not displayed");
            }
            else if(Section == "Categories Followed")
            {
                Assert.IsTrue(Browser.GetElements(PROFILE_CATEGORIES_FOLLOWED)[0].FindElement(By.TagName("h1")).Displayed, Section + "is not displayed");
            }
            else if(Section == "SubCategories Followed")
            {
                Assert.IsTrue(Browser.GetElements(PROFILE_CATEGORIES_FOLLOWED)[1].FindElement(By.TagName("h1")).Displayed, Section + "is not displayed");
            }
        }

        public void VerifyCategoriesList(string Section)
        {
            if (Section == "Categories Followed")
            {
                Assert.IsTrue(Browser.GetElement(PROFILE_CATEGORIES_LIST).Displayed, Section + "is not displayed");
            }
            else if(Section == "SubCategories Followed")
            {
                Assert.IsTrue(Browser.GetElement(PROFILE_SUB_CATEGORIES_LIST).Displayed, Section + "is not displayed");
            }
        }

        public void VerifyOtherUserProfilePictures(string Section)
        {
            if (Section == "Followed By")
            {
                Assert.IsTrue(Browser.GetElements(USER_PICTURE_FOLLOWED_BY).Count != 0, Section + "is not displayed");
            }
            else if (Section == "Following")
            {
                Assert.IsTrue(Browser.GetElements(USER_PICTURE_FOLLOWING).Count != 0, Section + "is not displayed");
            }
        }

        public void VerifyOtherUserProfileNames(string Section)
        {
            if (Section == "Followed By")
            {
                Assert.IsTrue(Browser.GetElement(FOLLOWED_BY_USERS_LIST).FindElements(By.TagName("span")).Count != 0, "User names are not displayed in:" +Section);
            }
            else if (Section == "Following")
            {
                Assert.IsTrue(Browser.GetElement(FOLLOWING_USERS_LIST).FindElements(By.TagName("span")).Count != 0, "User names are not displayed in:" + Section);
            }
        }

        public void ClickRandomUserName(string Name)
        {
            
            if (Name == "Followed By")
            {
                
                int i = Util.GetRandomNumber(Browser.GetElements(USER_PICTURE_FOLLOWED_BY).Count);
                logger.Debug(i);
                var SelectedUser = Browser.GetElements(USER_PICTURE_FOLLOWED_BY)[i];
                Browser.ScrollTo(SelectedUser);
                Browser.GetElements(USER_PICTURE_FOLLOWED_BY)[i].Click();
            }
            else if (Name == "Following")
            {
               // int i = Util.GetRandomNumber(Browser.GetElement(FOLLOWING_USERS_LIST).FindElements(By.TagName("img")).Count);

                int i = Util.GetRandomNumber(Browser.GetElements(USER_PICTURE_FOLLOWING).Count);
                logger.Debug(i);
                var SelectedUser = Browser.GetElements(USER_PICTURE_FOLLOWING)[i];
                Browser.ScrollTo(SelectedUser);
                Browser.GetElements(USER_PICTURE_FOLLOWING)[i].Click();
            }
        }

    }
}
    