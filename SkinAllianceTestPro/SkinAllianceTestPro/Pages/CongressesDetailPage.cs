﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SkinAllianceTestPro.Pages
{
    public class CongressesDetailPage :CongressDetailElements
    {
        public WebDriverService Browser;

        public static string headingDisplayed;

        public Boolean socialLink;

        public static string Error_Message_Displayed;

        public CongressesDetailPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

       public void VerifyDetailPage(string Element)
        {
            if(Element == "Title")
            {
                
                Assert.IsTrue(Browser.GetElement(CONGRESSES_PAGE_TITLE).Displayed, Element + "is not displayed");
            }

           else if(Element == "Category Name")
            {
                Assert.IsTrue(Browser.GetElement(CONGRESSES_CATEGORY_NAME).Displayed, Element + "is not displayed");
            }
            else if(Element == "Description")
            {
                Assert.IsTrue(Browser.GetElement(CONGRESSES_CATEGORY_DESCRIPTION).Displayed, Element + "is not displayed");
            }
            else if(Element == "Register button")
            {
                Assert.IsTrue(Browser.GetElement(CONGRESSES_REGISTER_BUTTON).Displayed, Element + "is not displayed");
            }
            else if(Element == "Date")
            {
                Assert.IsTrue(Browser.GetElement(CONGRESSES_DATE).Displayed, Element + "is not displayed");
            }
        }

        public void ClickRandomCongresses()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(OTHER_CONGRESSES_DESCRIPTION).Count);
            Browser.GetElements(OTHER_CONGRESSES_DESCRIPTION)[i].Click();
        }


    }
}


