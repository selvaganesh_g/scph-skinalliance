﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using System.Threading;
using TechTalk.SpecFlow;
using SkinAllianceTestPro.Framework.Common.EnumType;
using OpenQA.Selenium;
using SkinAllianceTestPro.Framework.Common;
using OpenQA.Selenium.Support.UI;

namespace SkinAllianceTestPro.Pages
{
   public class ArticleListingPage : ArticleListingElement
    {
        public WebDriverService Browser;

        public ArticleListingPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public String GetRandomArticleContent()
        {
            int i = Browser.GetElements(ARTICLE_CONTENT).Count;
            Random rnd = new Random();
            i = rnd.Next(0, i - 1);
            Console.WriteLine("random value: " + i);
            string content = Browser.GetElements(ARTICLE_CONTENT)[i].Text;
            return content;
        }
        public String ClickRandomArticleContent()
        {
            int i = Browser.GetElements(ARTICLE_CONTENT).Count;
            Random rnd = new Random();
            i = rnd.Next(0, i - 1);
            Console.WriteLine("random value: " + i);
            string content = Browser.GetElements(ARTICLE_CONTENT)[i].Text;
            Browser.GetElements(ARTICLE_CONTENT)[i].Click();
            return content;
             
        }

        public String GetArticleTitle()
        {
            string PageTitle = Browser.GetElement(PAGE_HEADING).Text;
            return PageTitle;
        }
        
        public void GetArticleContents()
        {
            Assert.IsTrue(Browser.GetElement(ARTICLES_LIST).Displayed, "Articles are not displayed");
        }

        public void VerifyArticleDescription()
        {
            int i = Util.GetRandomNumber(Browser.GetElement(ARTICLE_LISTING_SECTION).FindElements(By.TagName("p")).Count);
            Assert.IsTrue(Browser.GetElement(ARTICLE_LISTING_SECTION).FindElements(By.TagName("p"))[i].Displayed, "Article Description is not displayed");
        }

        public void ClickArticleDescription()
        {
            int i = Util.GetRandomNumber(Browser.GetElement(ARTICLES_LIST).FindElements(By.TagName("p")).Count);
            Browser.GetElement(ARTICLE_LISTING_SECTION).FindElements(By.TagName("p"))[i].Click();
        }

      
        public void ClickPreviousBreadcrumb()
        {
            Thread.Sleep(3000);
            var BreadcrumbLink = Browser.GetElements(BREADCRUMB_PREV_LINK)[1];
            BreadcrumbLink.Click();
        }

        public void ClickSortDropdown()
        {
            Browser.GetElement(SORT_DROPDOWN).Click();
        }

        public void SelectRandomSortOption()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(SORT_DROPDOWN).Count);
            Browser.GetElements(SORT_DROPDOWN)[i].Click();
        }

        public void SelectSortOption(string Option)
        {
            SelectElement e = new SelectElement(Browser.GetElement(SORT_DROPDOWN));
            if (Option == "Popularity")
            {
                
                e.SelectByValue("sortbypopularity");
            }
            else if(Option == "Date")
            {
                e.SelectByValue("sortbymostrecent");
            }
        }
        public void VerifyArticleImage()
        {
            Assert.IsTrue(Browser.GetElements(ARTICLE_IMAGE).Count > 0, "Article Image is not displayed");
        }

        public void ClickArticleImage()
        {
            int a = Browser.GetElements(ARTICLE_IMAGE).Count;

            if (a == 1)
            {
                Browser.GetElement(ARTICLE_IMAGE).Click();
            }
            else
            {
                int i = Util.GetRandomNumber(Browser.GetElements(ARTICLE_IMAGE).Count);
                Browser.GetElements(ARTICLE_IMAGE)[i].Click();
            }
        }

        public void ClickArticleComment()
        {
            int a = Browser.GetElements(ARTICLE_COMMENT_ICON).Count;

            if (a == 1)
            {
                Browser.GetElement(ARTICLE_COMMENT_ICON).Click();
            }
            else
            {
                int i = Util.GetRandomNumber(Browser.GetElements(ARTICLE_COMMENT_ICON).Count);
                Browser.GetElements(ARTICLE_COMMENT_ICON)[i].Click();
            }
        }

        public void ClickArticleLikeIcon()
        {
            int a = Browser.GetElements(ARTICLE_STAR_ICON).Count;

            if (a == 1)
            {
                Browser.GetElement(ARTICLE_STAR_ICON).Click();
            }
            else
            {
                int i = Util.GetRandomNumber(Browser.GetElements(ARTICLE_STAR_ICON).Count);
                Browser.GetElements(ARTICLE_STAR_ICON)[i].Click();
            }
        }
        
        public void VerifyArticlesLikeIcon()
        {
            Assert.IsTrue(Browser.GetElements(ARTICLE_STAR_ICON).Count > 0, "Likes icon is not displayed");
        }

        public void VerifyArticlesCommentIcon()
        {
            Assert.IsTrue(Browser.GetElements(ARTICLE_COMMENT_ICON).Count > 0, "Comments icon is not displayed");
        }

        public void VerifyLikesCount()
        {
            Assert.IsTrue(Browser.GetElements(ARTICLE_STAR_ICON).Count > 0, "No of likes are not displayed");
        }

        public void VerifyCommentsCount()
        {
            Assert.IsTrue(Browser.GetElements(ARTICLE_COMMENT_ICON).Count > 0, "Comments icon is not displayed");
        }

       
        public void VerifyArticleDate()
        {
            Browser.ScrollTo(FooterElements.FOOTER_PRIVACY_POLICY);
            int i = Util.GetRandomNumber(Browser.GetElement(ARTICLE_LISTING_SECTION).FindElements(By.TagName("ul")).Count);
            string date = Browser.GetElement(ARTICLE_LISTING_SECTION).FindElements(By.TagName("ul"))[i].Text;
            string[] a = date.Split('|');
            Assert.IsTrue(a[0].Contains("2017"), a[0]+ date+ "is not displayed");
            
        }

        public void VerifyArticleAuthorName()
        {
            Assert.IsTrue(Browser.GetElements(DATE_AUTHOR_CONTENT).Count > 0, "Article Author name is not displayed");
            
        }

       

    }

}

