﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using System.Threading;
using TechTalk.SpecFlow;
using System.Collections;
using TechTalk.SpecFlow.Assist;
using System.Data;
using SkinAllianceTestPro.Steps;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Framework.Common.EnumType;
using SkinAllianceTestPro.Steps;
using OpenQA.Selenium;

namespace SkinAllianceTestPro.Pages
{
    public class DirectoryPage : DirectoryElements
    {
        public WebDriverService Browser;

        public static string SelectedValue;

        public static string UserDetail;

        public static string SelectedUserName;

        public DirectoryPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void VerifyUserDetails(string Element)
        {
            if (Element == "Profile pic")
            {
                int Picture = Browser.GetElements(PROFILE_PICTURES).Count;
                Assert.IsTrue(Picture != 0, Element + "is not displayed");
            }

            else if (Element == "UserName")
            {
                Assert.IsTrue(Browser.GetElement(USER_NAMES).FindElements(By.TagName("span")).Count != 0, Element + "is not displayed");

            }
            else if (Element == "Specialty")
            {
                Assert.IsTrue(Browser.GetElement(USER_SPECIALTY).Displayed, Element + "is not displayed");
            }
        }

        public void VerifyFilteredUserList(string SelectedOption)
        {
            if (SelectedOption == "selected country" || SelectedOption == "selected speciality")
            {

                Assert.IsTrue(Browser.GetElements(USER_DETAIL_SECTION).Count > 0, SelectedOption + "is not displayed");
            }
            else
            {
                Assert.Fail("Users list is not displayed in directory page");
            }
        }
        public String ClickRandomFilterOption(string Filter)
        {

            if (Filter == "random country")
            {
                int countryList = Browser.GetElement(FILTER_COUNTRY_CHECKBOXES).FindElements(By.TagName("span")).Count;
                Browser.GetElement(FILTER_COUNTRY_CHECKBOXES).FindElements(By.TagName("span"))[Util.GetRandomNumber(countryList)].Click();
                
               //SelectedCountry.FindElement(By.TagName("label")).Click();

            }
            else if (Filter == "random specialty")
            {
                int specialtyList = Browser.GetElement(FILTER_SPECIALTY_CHECKBOEXS).FindElements(By.TagName("span")).Count;
                Browser.GetElement(FILTER_SPECIALTY_CHECKBOEXS).FindElements(By.TagName("span"))[Util.GetRandomNumber(specialtyList)].Click();
                //SelectedValue = SelectedSpecialty.Text;
                //SelectedSpecialty.FindElement(By.TagName("label")).Click();
            }

            return SelectedValue;
        }

        public string ClickRandomUserProfile()
        {
            int i = Util.GetRandomNumber(Browser.GetElement(USER_NAMES).FindElements(By.TagName("span")).Count);
            SelectedUserName = Browser.GetElement(USER_NAMES).FindElements(By.TagName("span"))[i].Text;
            Browser.GetElement(USER_NAMES).FindElements(By.TagName("span"))[i].Click();
            return SelectedUserName;
        }

        public void VerifyNameInUserProfile()
        {
            string UserName = Browser.GetElement(UserProfileElements.PROFILE_USER_NAME).Text;
            Assert.IsTrue(UserName.Equals(SelectedUserName, StringComparison.InvariantCultureIgnoreCase), SelectedUserName + "is not displayed as the user name in User profile page");
        }

        public void ClickUserDisplayButton(string buttonName)
        {
            if (buttonName == "Expand All")
            {
                Browser.Click(EXPAND_ALL_BUTTON);
            }
            else if (buttonName == "Collapse All")
            {
                Browser.Click(COLLAPSE_ALL_BUTTON);
            }
        }
    }
}
