﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using System.Threading;
using TechTalk.SpecFlow;
using SkinAllianceTestPro.Block.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

public class MyNotificationPage : MyNotificationElement
    {
        public WebDriverService Browser;

        public static string CommentText;

        public string SelectElement;

        public string oSelection;

        public MyNotificationPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void VerifyTimeLineNotification()
        {
            Assert.IsTrue(Browser.GetElement(MyNotificationElement.TIMELINENOTIFICATION_SECTION).Displayed, "Timeline section is not displayed");
        }

        public void VerifyManageNotification()
        {
            Assert.IsTrue(Browser.GetElement(MyNotificationElement.MANAGE_NOTIFICATION_SECTION).Displayed, "Manage Notification button is not displayed");
        }

        public void VerifyTimeLineMessages()
        {
            Assert.IsTrue(Browser.GetElement(MyNotificationElement.MESSAGE_SECTION).Displayed, "Timeline messages is not displayed");
        }

        public void ClickManageButton()
        {
            Assert.IsTrue(Browser.GetElement(MyNotificationElement.MANAGE_BUTTON).Displayed, "Message button is not clickable");
        }

        public void VerifyClosePopup()
        {
            Assert.IsTrue(Browser.GetElement(MyNotificationElement.CLOSE_BUTTON).Displayed, "Close button is not display");
        }
    
       public void VerifyFilterNotificationOption(string filterList)
        {
        if (filterList == "Like")
          {
            Browser.GetElement(MyNotificationElement.LIKED_SECTION);
          }
        else if (filterList == "Comment")
          {
            Browser.GetElement(MyNotificationElement.COMMENT_SECTION);
          }
        else if (filterList == "New Post in Followed Category")
          {
            Browser.GetElement(MyNotificationElement.NEWPOST_FC);
          }
        else if (filterList == "New post by Contact")
          {
            Browser.GetElement(MyNotificationElement.NEWPOST_CONTACT);
          }
        else if (filterList == "Contact Followed a User")
          {
            Browser.GetElement(MyNotificationElement.CONTACT_FDUSER);
          }
        else
        {
            Assert.Fail("Filters are not displayed");
        }
    }

    public void ClickFilterNotificationOption(string filterList)
    {
        if (filterList == "Like")
        {
            Browser.GetElement(MyNotificationElement.LIKED_SECTION).Click();
        }
        else if (filterList == "Comment")
        {
            Browser.GetElement(MyNotificationElement.COMMENT_SECTION).Click();
        }
        else if (filterList == "New Post in Followed Category")
        {
            Browser.GetElement(MyNotificationElement.NEWPOST_FC).Click();
        }
        else if (filterList == "New post by Contact")
        {
            Browser.GetElement(MyNotificationElement.NEWPOST_CONTACT).Click();
        }
        else if (filterList == "Contact Followed a User")
        {
            Browser.GetElement(MyNotificationElement.CONTACT_FDUSER).Click();
        }
        else
        {
            Assert.Fail("Filters are not displayed");
        }
      }

    public void VerifyFilteredOption(string filterList)
    {
        if (filterList == "Like")
        {
            Browser.GetElement(MyNotificationElement.LIKED_FILTER);
        }
        else if (filterList == "Comment")
        {
            Browser.GetElement(MyNotificationElement.COMMENT_FILTER);
        }
        else if (filterList == "New Post in Followed Category")
        {
            Browser.GetElement(MyNotificationElement.FC_FILTER);
        }
        else if (filterList == "New post by Contact")
        {
            Browser.GetElement(MyNotificationElement.POST_FILTER);
        }
        else if (filterList == "Contact Followed a User")
        {
            Browser.GetElement(MyNotificationElement.CONTACT_FILTER);
        }
        else
        {
            Assert.Fail("Filters are not displayed");
        }
    }
    

    public void VerifyNotificationPopup(string sectionDisplayed)
        {
            if (sectionDisplayed == "One of your contacts is now following a new user")
            {
                Assert.IsTrue(Browser.GetElement(MyNotificationElement.NEWUSERFOLLOW_MANAGE).Displayed, sectionDisplayed + "is not displayed");
            }
            else if (sectionDisplayed == "New content in a followed category")
            {
                Assert.IsTrue(Browser.GetElement(MyNotificationElement.FCATEGORY_MANAGE).Displayed, sectionDisplayed + "is not displayed");
            }
            else if (sectionDisplayed == "Someone interacts with your post")
            {
                Assert.IsTrue(Browser.GetElement(MyNotificationElement.INTERACT_MANAGE).Displayed, sectionDisplayed + "is not displayed");
            }
            else if (sectionDisplayed == "New comment in a post shared,liked or commented")
            {
                Assert.IsTrue(Browser.GetElement(MyNotificationElement.COMMENT_MANAGE).Displayed, sectionDisplayed + "is not displayed");
            }
            else if (sectionDisplayed == "One of your contacts add a new post")
            {
                Assert.IsTrue(Browser.GetElement(MyNotificationElement.CONTACT_MANAGE).Displayed, sectionDisplayed + "is not displayed");
            }
            else
            {
                Assert.Fail(sectionDisplayed + "section is not displayed");
            }

        }
          

         public void ClickRandomDropDown()
        {
            Assert.IsTrue(Browser.GetElement(MyNotificationElement.RANDOM_DROPDOWN).Displayed, "DropDown is not clickable");
        }

    public void VerifyFilterNotification()
    {
        Assert.IsTrue(Browser.GetElement(MyNotificationElement.FILTER_SECTION).Displayed, "Filter Section is not displayed");
    }

    public void SelectDropDownImmediatly()
    {
        string URL = Browser.GetCurrentUrl();
        if (URL.Contains("my-notification"))
        {

            SelectElement DropDown1 = new SelectElement(Browser.GetElement(DROPDOWN_ONE));
            DropDown1.SelectByIndex(1);

            SelectElement DropDown2 = new SelectElement(Browser.GetElement(DROPDOWN_TWO));
            DropDown2.SelectByIndex(1);

            SelectElement DropDown3 = new SelectElement(Browser.GetElement(DROPDOWN_THREE));
            DropDown3.SelectByIndex(1);

            SelectElement DropDown4 = new SelectElement(Browser.GetElement(DROPDOWN_FOUR));
            DropDown4.SelectByIndex(1);

            SelectElement DropDown5 = new SelectElement(Browser.GetElement(DROPDOWN_FIVE));
            DropDown5.SelectByIndex(1);
        }
        else
        {

            Assert.IsTrue(Browser.GetElement(MyNotificationElement.DROPDOWN_ONE).Displayed, "Immediately is not displayed");
        }
    }

    public void SelectDropDownDaily()
    {
        string URL = Browser.GetCurrentUrl();
        if (URL.Contains("my-notification"))
        {

            SelectElement DropDown1 = new SelectElement(Browser.GetElement(DROPDOWN_ONE));
            DropDown1.SelectByIndex(2);

            SelectElement DropDown2 = new SelectElement(Browser.GetElement(DROPDOWN_TWO));
            DropDown2.SelectByIndex(2);

            SelectElement DropDown3 = new SelectElement(Browser.GetElement(DROPDOWN_THREE));
            DropDown3.SelectByIndex(2);

            SelectElement DropDown4 = new SelectElement(Browser.GetElement(DROPDOWN_FOUR));
            DropDown4.SelectByIndex(2);

            SelectElement DropDown5 = new SelectElement(Browser.GetElement(DROPDOWN_FIVE));
            DropDown5.SelectByIndex(2);
        }
        else
        {

            Assert.IsTrue(Browser.GetElement(MyNotificationElement.DROPDOWN_ONE).Displayed, "Daily is not displayed");
        }
    }
    public void SelectDropDownWeekly()
    {
        string URL = Browser.GetCurrentUrl();
        if (URL.Contains("my-notification"))
        {

            SelectElement DropDown1 = new SelectElement(Browser.GetElement(DROPDOWN_ONE));
            DropDown1.SelectByIndex(3);

            SelectElement DropDown2 = new SelectElement(Browser.GetElement(DROPDOWN_TWO));
            DropDown2.SelectByIndex(3);

            SelectElement DropDown3 = new SelectElement(Browser.GetElement(DROPDOWN_THREE));
            DropDown3.SelectByIndex(3);

            SelectElement DropDown4 = new SelectElement(Browser.GetElement(DROPDOWN_FOUR));
            DropDown4.SelectByIndex(3);

            SelectElement DropDown5 = new SelectElement(Browser.GetElement(DROPDOWN_FIVE));
            DropDown5.SelectByIndex(3);
        }
        else
        {

            Assert.IsTrue(Browser.GetElement(MyNotificationElement.DROPDOWN_ONE).Displayed, "Weekly is not displayed");
        }
    }
    public void SelectDropDownNever()
    {
        string URL = Browser.GetCurrentUrl();
        if (URL.Contains("my-notification"))
        {

            SelectElement DropDown1 = new SelectElement(Browser.GetElement(DROPDOWN_ONE));
            DropDown1.SelectByIndex(0);

            SelectElement DropDown2 = new SelectElement(Browser.GetElement(DROPDOWN_TWO));
            DropDown2.SelectByIndex(0);

            SelectElement DropDown3 = new SelectElement(Browser.GetElement(DROPDOWN_THREE));
            DropDown3.SelectByIndex(0);

            SelectElement DropDown4 = new SelectElement(Browser.GetElement(DROPDOWN_FOUR));
            DropDown4.SelectByIndex(0);

            SelectElement DropDown5 = new SelectElement(Browser.GetElement(DROPDOWN_FIVE));
            DropDown5.SelectByIndex(0);
        }
        else
        {
            Assert.IsTrue(Browser.GetElement(MyNotificationElement.DROPDOWN_ONE).Displayed, "Never is not displayed");
        }
    }
}



