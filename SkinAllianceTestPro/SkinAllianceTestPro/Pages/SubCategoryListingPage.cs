﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using NUnit.Framework;

namespace SkinAllianceTestPro.Pages
{
    public class SubCategoryListingPage : SubCategoryListingElements
    {
        public WebDriverService Browser;

        public SubCategoryListingPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public IList<IWebElement> GetSubCategoryNames()
        {
            return Browser.GetElements(SUB_CATEGORY_NAMES);
        }

        public IList<IWebElement> GetArticlesImage()
        {
            return Browser.GetElements(SUB_CATEGORY_IMAGE);
        }

        public IList<IWebElement> GetArticlesDescription()
        {
            return Browser.GetElements(SUB_CATEGORY_CONTENT);
        }

        public IList<IWebElement> GetArticlesLikeIcon()
        {
            return Browser.GetElements(LIKE_ICON);
        }

        public IList<IWebElement> GetArticlesCommentIcon()
        {
            return Browser.GetElements(COMMENT_ICON);
        }

        public IList<IWebElement> GetLikesCount()
        {
            return Browser.GetElements(LIKE_ICON_COUNT);
        }

        public IList<IWebElement> GetCommentsCount()
        {
            return Browser.GetElements(COMMENT_ICON_COUNT);
        }
        public IList<IWebElement> getArticlesAuthorName()
        {
            return Browser.GetElements(SUB_CATEGORY_AUTHOR);
        }

        public IList<IWebElement> getArticlesPostedDate()
        {
            return Browser.GetElements(SUB_CATEGORY_DATE);
        }


        public void ClickRandomArticleImage()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(SUB_CATEGORY_IMAGE).Count);
            //string ArticleImage = Browser.GetElements(SUB_CATEGORY_IMAGE)[i].Text;
            Browser.GetElements(SUB_CATEGORY_IMAGE)[i].Click();
           // return ArticleImage;
        }

        public string ClickRandomArticleContent()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(SUB_CATEGORY_CONTENT).Count);
            string ArticleContent = Browser.GetElements(SUB_CATEGORY_CONTENT)[i].Text;
            Browser.GetElements(SUB_CATEGORY_CONTENT)[i].Click();
            return ArticleContent;
        }

        public void ClickRandomCommentIcon()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(COMMENT_ICON).Count);
            //string Comment = Browser.GetElements(COMMENT_ICON)[i].Text;
            Browser.GetElements(COMMENT_ICON)[i].Click();

        }

        public void ClickRandomLikeIcon()
        {
            int i = Util.GetRandomNumber(Browser.GetElements(LIKE_ICON).Count);
            Browser.GetElements(LIKE_ICON)[i].Click();
        }

        public void ClickRandomViewAllLink()
        {
            //int i = Browser.GetElements(SUB_CATEGORY_NAMES).Count;
            //var a = Browser.GetElements(CATEGORY_TITLE)[Util.GetRandomNumber(i)];
            //string Sub_category_name = a.Text;
            //a.FindElement(By.LinkText("a")).Click();
            //Browser.GetElement(SUB_CATEGORY_NAMES).FindElements(By.LinkText("a"))[i].Click();
            //return Sub_category_name;
            
            int i = Util.GetRandomNumber(Browser.GetElements(VIEW_ALL_LINK).Count);
            
            Browser.GetElements(VIEW_ALL_LINK)[i].Click();
        }

        public void GetButtonName(string button)
        {
            IWebElement element = Browser.GetElement(FOLLOW_BUTTON);
            string buttonName = element.Text;
            if (buttonName == "UNFOLLOW")
            {
              
                Assert.AreEqual(button, buttonName, "Expected is: " +button+ "Actual is: " +buttonName);

            }
            else
            {
                Assert.IsTrue(buttonName.Equals("FOLLOW"), buttonName + "is not displayed");
            }
        }
    }
}
