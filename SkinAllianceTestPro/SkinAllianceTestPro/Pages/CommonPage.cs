﻿using System;
using OpenQA.Selenium.Remote;
using SkinAllianceTestPro.Service;
using NUnit.Framework;
using OpenQA.Selenium;

namespace SkinAllianceTestPro.Pages
{
    public class CommonPage
    {
        public WebDriverService Browser;
        
        public CommonPage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
            
        }

        public bool validateUrlContains(string partialUrl)
        {
            return Browser.GetCurrentUrl().Contains(partialUrl);
              
        }

        public void validatePageTitle(string title)
        {
           string pageTitle = Browser.GetPageTitle();
            

            Assert.AreEqual("Expected page title: " + title + "; but Actual: " + pageTitle, title, pageTitle);
            

        }

       

        public bool ElementIsPresent(By locator)
        {
            // return GetElementFromParent(locator, null);
            bool isPresent = false;
            try
            {
                //if (Browser.ElementPresent)
                {
                    isPresent = true;
                }
            }
            catch (ElementNotVisibleException)
            {
                //isPresent = false;
            }

            return isPresent;


        }



    }
}
