﻿using log4net;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SkinAllianceTestPro.Blocks.ElementBlocks;
using SkinAllianceTestPro.Framework.Common;
using SkinAllianceTestPro.Service;
using SkinAllianceTestPro.Steps;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SkinAllianceTestPro.Pages
{
    public class MyProfilePage : MyProfileElements
    {
        public WebDriverService Browser;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public MyProfilePage(RemoteWebDriver driver)
        {
            Browser = new WebDriverService(driver);
        }

        public void VerifyMyProfileContents(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps profileContent in rows)
            {
                switch (profileContent.Attributes)
                {
                    case "Profile Picture":
                        Assert.IsTrue(Browser.GetElement(PROFILE_PICTURE).Displayed, profileContent +"is not displayed");
                        Console.WriteLine(profileContent);
                        break;
                    case "Profile Name":
                        Assert.IsTrue(Browser.GetElement(USER_INFO).Displayed, profileContent + "is not displayed");
                        break;
                    case "Designation":
                        Assert.IsTrue(Browser.GetElement(USER_DESIGNATION).Displayed, profileContent + "is not displayed");
                        break;
                    case "Country":
                        Assert.IsTrue(Browser.GetElement(USER_COUNTRY).Displayed, profileContent + "is not displayed");
                        break;
                    case "Contact":
                        Assert.IsTrue(Browser.GetElement(USER_CONTACT).Displayed, profileContent + "is not displayed");
                        break;
                    case "Email Id":
                        Assert.IsTrue(Browser.GetElement(USER_EMAIL_ID).Displayed, profileContent + "is not displayed");
                        break;
                    case "Summary Section":
                        Assert.IsTrue(Browser.GetElement(USER_SUMMARY_SECTION).Displayed, profileContent + "is not displayed");
                        break;
                    case "Following Section":
                        Assert.IsTrue(Browser.GetElement(USER_FOLLOWING_SECTION).Displayed, profileContent + "is not displayed");
                        break;
                    case "Followed By":
                        Assert.IsTrue(Browser.GetElement(USER_FOLLOWED_BY_SECTION).Displayed, profileContent + "is not displayed");
                        break;
                    case "EDIT PROFILE":
                        Assert.IsTrue(Browser.GetElement(EDIT_BUTTON).Displayed, profileContent + "is not displayed");
                        break;
                    default:
                        Assert.Fail("a");
                        break;
                }
              }
            }
        public void VerifyEditProfileContents(Table table)
        {
            var rows = table.CreateSet<TableSteps>();
            foreach (TableSteps editProfileContents in rows)
            {
                switch(editProfileContents.Step)
                {
                    case "User Image":
                        Assert.IsTrue(Browser.GetElement(EDIT_USER_IMAGE).Displayed, editProfileContents + "is not displayed");
                        Console.WriteLine(editProfileContents);
                        break;
                    case "Title":
                        Assert.IsTrue(Browser.GetElement(EDIT_TITLE).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "First Name":
                        Assert.IsTrue(Browser.GetElement(EDIT_FIRST_NAME).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Last Name":
                        Assert.IsTrue(Browser.GetElement(EDIT_LAST_NAME).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Medical Speciality":
                        Assert.IsTrue(Browser.GetElement(EDIT_MEDICAL_SPECIALITY).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Physical License Number":
                        Assert.IsTrue(Browser.GetElement(EDIT_PHYSICAL_NO).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Address":
                        Assert.IsTrue(Browser.GetElement(EDIT_ADDRESS).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Prefered Language":
                        Assert.IsTrue(Browser.GetElement(EDIT_LANGUAGE).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "City":
                        Assert.IsTrue(Browser.GetElement(EDIT_CITY).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Zip Code":
                        Assert.IsTrue(Browser.GetElement(EDIT_ZIP_CODE).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Activity":
                        Assert.IsTrue(Browser.GetElement(EDIT_ACTIVITY).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Upload Resume":
                        Assert.IsTrue(Browser.GetElement(EDIT_RESUME).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Country":
                        Assert.IsTrue(Browser.GetElement(EDIT_COUNTRY).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Phone":
                        Assert.IsTrue(Browser.GetElement(EDIT_PHONE).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Summary":
                        Assert.IsTrue(Browser.GetElement(EDIT_SUMMARY).Displayed, editProfileContents + "is not displayed");
                        break;
                    case "Close X button":
                        Assert.IsTrue(Browser.GetElement(EDIT_CLOSE_BUTTON).Displayed, editProfileContents + "is not displayed");
                        break;
                    default:
                        Assert.Fail("a");
                        break;
                }
            }
        }
        public void VerifyUserSection(string sectionDisplayed)
        {
            if (sectionDisplayed == "Followed By")
            {
                Assert.IsTrue(Browser.GetElement(USER_FOLLOWED_BY_SECTION).Displayed, sectionDisplayed+ "is not displayed");
            }
            else if(sectionDisplayed == "Following")
                {
                Assert.IsTrue(Browser.GetElement(USER_FOLLOWING_SECTION).Displayed, sectionDisplayed + "is not displayed");
            }
        else if(sectionDisplayed == "Followed Categories" || sectionDisplayed == "Followed Sub Categories")
            {
                Assert.IsTrue(Browser.GetElements(CATEGORIES_SECTION).Count > 0, sectionDisplayed + "is not displayed");
            }

            else
            {
                Assert.Fail(sectionDisplayed + "section is not displayed");
            }
        }

        public void VerifyUsersList(string UserSection)
        {
            if(UserSection == "following")
            {
                 
                Assert.IsTrue(Browser.GetElements(USER_NAME_CONTENT_SECTION).Count >= 1,"List of users are not displayed in" + UserSection);
            }
            else if(UserSection == "followed by")
            {
                Assert.IsTrue(Browser.GetElements(USER_FOLLOWED_BY_SECTION).Count >= 1, "List of users are not displayed in" + UserSection);
            }
            
        }

        public void SelectResume()
        {
            string exeDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            exeDir = exeDir.Replace("SkinAllianceTestPro\\bin\\Debug", "");
            Browser.GetElement(UPLOAD_RESUME_BUTTON).SendKeys(exeDir + "\\Resources\\Resume\\Resume.txt");
        }

        public void SelectProfilePicture()
        {
            string exeDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            exeDir = exeDir.Replace("SkinAllianceTestPro\\bin\\Debug","");
            Browser.GetElement(UPLOAD_PHOTO_BUTTON).SendKeys(exeDir + "Resources\\Images\\Image.png");
            logger.Debug("Working directory --==-=-=-=-=->>>dasd>ADs>"+exeDir);
        }

         public void VerifyFollowingContents(string content)
        {
            if (content == "name")
            {
                Assert.IsTrue(Browser.GetElements(USER_NAME_FOLLOWING_LINK).Count > 0, content+ "section is not displayed");
            }
            else if (content == "Image")
            {
                Assert.IsTrue(Browser.GetElements(USER_IMAGE_FOLLOWING_LINK).Count >= 0, content + "section is not displayed");
            }
            
            }
        
        public void VerifyFollowedByContents(string content)
        {
            if(content == "name of the users")
            {
                Assert.IsTrue(Browser.GetElements(USER_NAME_FOLLOWED_BY).Count >= 1, content + "section is not displayed");
            }
            else if(content == "Image of the users")
            {
                Assert.IsTrue(Browser.GetElements(USER_IMAGE_FOLLOWED_BY).Count >= 1, content + "section is not displayed");
            }
            else if(content == "categories" || content == "Sub categories")
            {
                Assert.IsTrue(Browser.GetElements(CATEGORIES_NAMES).Count > 0, content + "section is not displayed");
            }
           
        }

        public void EditTitle()
        {
           IWebElement ele = Browser.GetElement(EDIT_TITLE);
            ele.Clear();
            Browser.EnterText(EDIT_TITLE, "alliance");
        }

        public string EditFirstName()
        {
          string first_Name =  Browser.GetElement(EDIT_FIRST_NAME).GetAttribute("value");
            Browser.GetElement(EDIT_FIRST_NAME).Clear();
            Browser.EnterText(EDIT_FIRST_NAME, first_Name + "a");
            string firstName = Browser.GetElement(EDIT_FIRST_NAME).GetAttribute("value");
                return firstName;
        }

        public string EditLastName()
        {
            string last_Name = Browser.GetElement(EDIT_LAST_NAME).GetAttribute("value");
            Browser.GetElement(EDIT_LAST_NAME).Clear();
            Browser.EnterText(EDIT_LAST_NAME, last_Name + "b");
            string lastName = Browser.GetElement(EDIT_LAST_NAME).GetAttribute("value");
            return lastName;
        }

        public string EditActivity()
        {
            Browser.EnterText(EDIT_ACTIVITY, "c");
            string Activity = Browser.GetElement(EDIT_ACTIVITY).Text;
            return Activity;
        }

        public string EditRole()
        {
            Browser.EnterText(EDIT_ROLE, "c");
            string Role = Browser.GetElement(EDIT_ROLE).Text;
            return Role;
        }

        public string EditAddress()
        {
            Browser.EnterText(EDIT_ADDRESS, "street");
            string Address = Browser.GetElement(EDIT_ADDRESS).Text;
            return Address;
        }

        

        public void SelectCity()
        {
            SelectElement e = new SelectElement(Browser.GetElement(EDIT_CITY));
            e.SelectByIndex(Util.GetRandomNumber(e.Options.Count()));
        }

        public void SelectLanguage()
        {
            SelectElement e = new SelectElement(Browser.GetElement(EDIT_LANGUAGE));
            e.SelectByIndex(Util.GetRandomNumber(e.Options.Count()));
        }

        


        public string EditPhone()
        {
            Browser.GetElement(EDIT_PHONE).Clear();
            Random rnd = new Random();
            int i = rnd.Next(0, 1000000);
            Browser.EnterText(EDIT_PHONE, "123"+i+"121");
            string PhoneNo = Browser.GetElement(EDIT_PHONE).GetAttribute("value");
            return PhoneNo;
        }

        public string EditPhysicianNo()
        {
            string PhysicianNo = Browser.GetElement(EDIT_PHYSICAL_NO).GetAttribute("value");
            Browser.GetElement(EDIT_PHYSICAL_NO).Clear();
            Random rnd = new Random();
            int i = rnd.Next(0, 1000000);
            Browser.EnterText(EDIT_PHYSICAL_NO, "1232" + i+"232");
            string NewNumber = Browser.GetElement(EDIT_PHYSICAL_NO).GetAttribute("value");
            return NewNumber;
        }

        public void EditEmailID()
        {
            Random rnd = new Random();
            int i = rnd.Next(0, 1000000);
            Browser.EnterText(EDIT_EMAIL, "qatestingloreals" + i + "@test.com");
        }

        public string EditSummary()
        {
            string SummaryText = Browser.GetElement(EDIT_SUMMARY).GetAttribute("value");
            Browser.GetElement(EDIT_SUMMARY).Clear();
            Browser.EnterText(EDIT_SUMMARY, SummaryText + "good user");
            string NewSummaryText = Browser.GetElement(EDIT_SUMMARY).GetAttribute("value");
            return NewSummaryText;
        }


        public void EditUserDetails()
        {
            EditFirstName();
           EditLastName();
            EditPhone();
            EditAddress();
            EditPhysicianNo();
            
        }

        public void VerifyChangePasswordField(string TextField)
        {
            if(TextField == "Current Password")
            {
                Assert.IsTrue(Browser.GetElement(CURRENT_PASSWORD_FIELD).Displayed, TextField +"is not displayed");
            }
            else if(TextField == "New Password")
            {
                Assert.IsTrue(Browser.GetElement(NEW_PASSWORD_FIELD).Displayed, TextField + "is not displayed");
            }
            else if(TextField == "Confirm Password")
            {
                Assert.IsTrue(Browser.GetElement(CONFIRM_PASSWORD_FIELD).Displayed, TextField + "is not displayed");
            }
            else
            {
                Assert.Fail(TextField + "is not displayed properly");
            }
        }

        public void ChangeUserPassword()
        {
            string Password = "SkinAlli@ncE";
            Browser.EnterText(CURRENT_PASSWORD_FIELD, "LaRocheP#say");
            Browser.EnterText(NEW_PASSWORD_FIELD, Password);
            Browser.EnterText(CONFIRM_PASSWORD_FIELD, Password);
            Browser.Click(CHANGE_PWD_SUBMIT_BUTTON);

        }

        public void EnterIncorrectCurrentPassword()
        {
            string Password = "SkinAlli@ncE";
            Browser.EnterText(CURRENT_PASSWORD_FIELD, "LaRocheP#say");
            Browser.EnterText(NEW_PASSWORD_FIELD, Password);
            Browser.EnterText(CONFIRM_PASSWORD_FIELD, Password);
            Browser.Click(CHANGE_PWD_SUBMIT_BUTTON);
        }


        public void EnterOnlyCurrentPassword()
        {
            Browser.EnterText(CURRENT_PASSWORD_FIELD, "LaRocheP#say");
            Browser.Click(CHANGE_PWD_SUBMIT_BUTTON);
        }
    }
    }
