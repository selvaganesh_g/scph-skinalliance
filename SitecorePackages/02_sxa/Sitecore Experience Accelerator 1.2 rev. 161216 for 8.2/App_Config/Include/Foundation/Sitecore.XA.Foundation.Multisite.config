<?xml version="1.0" encoding="utf-8" ?>
<!--

Purpose: This include file enables and configures the Multisite foundation module.

The multitenant nature of the SXA architecture means that clients can run multiple microsites along with their main websites on a single instance of Sitecore. 
Additionally each tenant is divided into sites allowing for entire customer journeys to be completed in various languages either through one-to-one translated versions (native Sitecore language support) or an internationalisation model whereby a separate site map exists for different languages.

To disable this file, change its extension to ".disabled".

-->
<configuration xmlns:patch="http://www.sitecore.net/xmlconfig/">
  <sitecore>
    <settings>
      <setting patch:instead="*[@name='BucketConfiguration.DynamicBucketFolderPath']" name="BucketConfiguration.DynamicBucketFolderPath" value="Sitecore.XA.Foundation.Multisite.Buckets.SiteBucketPathProvider, Sitecore.XA.Foundation.Multisite" />
      <!--  LANGUAGES ALWAYS STRIP LANGUAGE
            This setting specifies if the StripLanguage processor in the <preprocessRequest> pipeline should parse and remove languages from
            the URL, even when the languageEmbedding attribute of the linkProvider is set to "never". 
	    You should only change this setting to "false" if the default behavior causes problems in your solution.
            Default value: true 
      -->
      <setting name="Languages.AlwaysStripLanguage" value="true">
        <patch:attribute name="value">false</patch:attribute>
      </setting>
    </settings>
    <experienceAccelerator>
      <multisite>
        <!-- List of source item fields IDs that is used to determine whether linked items are used on any sites. 
        If linked items are used, only the site specific html cache is cleared after publishing. -->
        <htmlCacheClearer>
          <fieldID desc="IVirtualNode - AdditionalChildren">{390D6F29-E759-4888-AD04-E8FFCD857492}</fieldID>
          <fieldID desc="Settings - Templates">{E8881464-38AF-4655-BE4A-EE10586578A2}</fieldID>
        </htmlCacheClearer>
      </multisite>
    </experienceAccelerator>
    <events>
      <event name="publish:end">
        <handler type="Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnPublishEnd" />
        <handler patch:instead="*[@type='Sitecore.Publishing.HtmlCacheClearer, Sitecore.Kernel']" type="Sitecore.XA.Foundation.Multisite.EventHandlers.HtmlCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnPublishEnd" />
      </event>
      <event name="publish:end:remote">
        <handler type="Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnPublishEnd" />
        <handler patch:instead="*[@type='Sitecore.Publishing.HtmlCacheClearer, Sitecore.Kernel']" type="Sitecore.XA.Foundation.Multisite.EventHandlers.HtmlCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnPublishEndRemote" />
      </event>
      <event name="item:saved">
        <handler type="Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnItemSaved" />
        <handler patch:after="*[@type='Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite']" type="Sitecore.XA.Foundation.Multisite.EventHandlers.HttpRoutesRefresher, Sitecore.XA.Foundation.Multisite" method="OnItemSaved" />
      </event>
      <event name="item:renamed">
        <handler type="Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnItemRenamed" />
      </event>
      <event name="item:moved">
        <handler type="Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnItemMoved" />
      </event>      
      <event name="item:renamed:remote">
        <handler type="Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnItemRenamedRemote" />
      </event>
      <event name="item:moved:remote">
        <handler type="Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnItemMovedRemote" />
      </event>            
      <event name="item:saved:remote">
        <handler type="Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnItemSavedRemote" />
        <handler patch:after="*[@type='Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite']" type="Sitecore.XA.Foundation.Multisite.EventHandlers.HttpRoutesRefresher, Sitecore.XA.Foundation.Multisite" method="OnItemSavedRemote" />
      </event>
      <event name="item:deleted">
        <handler type="Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnItemSaved" />
      </event>
      <event name="item:deleted:remote">
        <handler type="Sitecore.XA.Foundation.Multisite.EventHandlers.SiteCacheClearer, Sitecore.XA.Foundation.Multisite" method="OnItemSavedRemote" />
      </event>
    </events>
    <pipelines>
      <httpRequestBegin>
        <processor patch:after="*[@type='Sitecore.Pipelines.HttpRequest.ItemResolver, Sitecore.Kernel']" type="Sitecore.XA.Foundation.Multisite.Pipelines.HttpRequest.LocalizableUrlItemResolver, Sitecore.XA.Foundation.Multisite">
          <CacheExpiration>5</CacheExpiration>
        </processor>
        <processor patch:instead="*[@type='Sitecore.Pipelines.HttpRequest.SiteResolver, Sitecore.Kernel']" type="Sitecore.XA.Foundation.Multisite.Pipelines.HttpRequest.SiteResolver, Sitecore.XA.Foundation.Multisite" />
        <processor patch:instead="*[@type='Sitecore.ExperienceEditor.Pipelines.HttpRequest.ResolveContentLanguage, Sitecore.ExperienceEditor']" type="Sitecore.XA.Foundation.Multisite.Pipelines.HttpRequest.ResolveContentLanguage, Sitecore.XA.Foundation.Multisite" />
      </httpRequestBegin>
      <httpRequestProcessed>
        <processor type="Sitecore.XA.Foundation.Multisite.Pipelines.HttpRequest.StoreSiteNameInCookie, Sitecore.XA.Foundation.Multisite"/>
        <processor type="Sitecore.XA.Foundation.Multisite.Pipelines.HttpRequest.FixMultitenancyEditMode, Sitecore.XA.Foundation.Multisite"/>
      </httpRequestProcessed>
      <resolveTokens>
        <processor patch:after="*[3]" type="Sitecore.XA.Foundation.Multisite.Pipelines.ResolveTokens.ResolveMultisiteTokens, Sitecore.XA.Foundation.Multisite" />
      </resolveTokens>
      <itemWebApiRequest>
        <processor patch:after="*[@type='Sitecore.ItemWebApi.Pipelines.Request.ResolveScope, Sitecore.ItemWebApi']"
            type="Sitecore.XA.Foundation.Multisite.Pipelines.ItemWebApiRequest.ResolveVirtualItems, Sitecore.XA.Foundation.Multisite" />
      </itemWebApiRequest>
      <itemWebApiSearch>
        <processor patch:instead="*[@type='Sitecore.ItemWebApi.Pipelines.Search.SetRootItem, Sitecore.Speak.ItemWebApi']"
            type="Sitecore.XA.Foundation.Multisite.Pipelines.ItemWebApiSearch.SetRootItem, Sitecore.XA.Foundation.Multisite" />
      </itemWebApiSearch>
      <contentSearch.stripQueryStringParameters>
        <processor patch:before="*[@type='Sitecore.ContentSearch.Pipelines.StripQueryStringParameters.RemoveEmptySearches, Sitecore.ContentSearch']"
            type="Sitecore.XA.Foundation.Multisite.Pipelines.StripQueryStringParameters.ResolveMultitenancyInMediaLibrary,Sitecore.XA.Foundation.Multisite"/>
      </contentSearch.stripQueryStringParameters>
      <contentSearch.getGlobalLinqFilters>
        <processor patch:after="*[@type='Sitecore.ContentSearch.Pipelines.QueryGlobalFilters.ApplyGlobalLinqFilters, Sitecore.ContentSearch']"
            type="Sitecore.XA.Foundation.Multisite.Pipelines.GetGlobalLinqFilters.ResolveMultitenancyInMediaLibrary,Sitecore.XA.Foundation.Multisite"/>
      </contentSearch.getGlobalLinqFilters>
      <getRenderingDatasource>
        <processor patch:before="*[@type='Sitecore.Pipelines.GetRenderingDatasource.SetFallbackDatasourceLocations, Sitecore.Kernel']"
            type="Sitecore.XA.Foundation.Multisite.Pipelines.GetRenderingDatasource.RestrictFallbackDatasourceLocations, Sitecore.XA.Foundation.Multisite" />
      </getRenderingDatasource>
      <ioc>
        <processor  type="Sitecore.XA.Foundation.Multisite.Pipelines.IoC.RegisterMultisiteServices, Sitecore.XA.Foundation.Multisite" />
      </ioc>
      <renderField>
        <processor patch:instead="*[@type='Sitecore.Pipelines.RenderField.GetLinkFieldValue, Sitecore.Kernel']"
                   type="Sitecore.XA.Foundation.Multisite.Pipelines.RenderField.GetLinkFieldValue, Sitecore.XA.Foundation.Multisite"/>

      </renderField>
    </pipelines>
    <processors>
      <uiGetMasters>
        <processor patch:after="*[@type='Sitecore.Pipelines.GetMasters.GetItemMasters,Sitecore.Kernel']" type="Sitecore.XA.Foundation.Multisite.Processors.UiGetMasters.GetItemMasters, Sitecore.XA.Foundation.Multisite" />
      </uiGetMasters>
      <uiMoveItems>
        <processor patch:instead="*[@method='GetDestination' and @type='Sitecore.Shell.Framework.Pipelines.MoveItems,Sitecore.Kernel']" mode="on" type="Sitecore.XA.Foundation.Multisite.Processors.UiMoveItems.MoveItems, Sitecore.XA.Foundation.Multisite" method="ShowDialog" />
      </uiMoveItems>
    </processors>
    <dataviews>
      <dataview name="Master" >
        <patch:attribute name="assembly">Sitecore.XA.Foundation.Multisite</patch:attribute>
        <patch:attribute name="type">Sitecore.XA.Foundation.Multisite.Views.SxaDataView</patch:attribute>
      </dataview>
    </dataviews>
    <sites>
      <site name="service" virtualFolder="/sitecore/service" physicalFolder="/sitecore/service">
        <patch:attribute name="enableAnalytics">false</patch:attribute>
      </site>
    </sites>
    <siteManager defaultProvider="sitecore">
      <providers>
        <clear/>
        <add name="sitecore" type="Sitecore.Sites.SitecoreSiteProvider, Sitecore.Kernel" checkSecurity="false">
          <providers hint="raw:AddProviderReference">
            <reference patch:before="*[@name='config']" name="backend"/>
            <reference patch:after="*[@name='config']" name="service"/>
            <reference patch:instead="*[@name='config']" name="sxa"/>
          </providers>
        </add>
        <add name="backend" type="Sitecore.XA.Foundation.Multisite.Providers.BackendSiteProvider, Sitecore.XA.Foundation.Multisite" inherits="config" checkSecurity="false"/>
        <add name="service" type="Sitecore.XA.Foundation.Multisite.Providers.ServiceSiteProvider, Sitecore.XA.Foundation.Multisite" inherits="config" checkSecurity="false"/>
        <add name="sxa" type="Sitecore.XA.Foundation.Multisite.Providers.SxaSiteProvider, Sitecore.XA.Foundation.Multisite" database="master" checkSecurity="false"/>
      </providers>
    </siteManager>
    <linkManager defaultProvider="sitecore">
      <patch:attribute name="defaultProvider">localizedProvider</patch:attribute>
      <providers>
        <clear/>
        <add name="localizedProvider" type="Sitecore.XA.Foundation.Multisite.LinkManagers.LocalizableLinkProvider, Sitecore.XA.Foundation.Multisite" cacheExpiration="5" addAspxExtension="false" alwaysIncludeServerUrl="false" encodeNames="true" languageEmbedding="never" languageLocation="filePath" shortenUrls="true" useDisplayName="false"/>
      </providers>
    </linkManager>
    <controlSources>
      <source patch:before="*[@namespace='Sitecore.Shell.Applications.ContentEditor']" mode="on" namespace="Sitecore.XA.Foundation.Multisite.CustomFields.FieldTypes" assembly="Sitecore.XA.Foundation.Multisite" prefix="content"/>
    </controlSources>
  </sitecore>
</configuration>

