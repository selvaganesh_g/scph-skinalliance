<?xml version="1.0" encoding="utf-8" ?>
<!--

Purpose: This include file enables and configures the Lucene search provider foundation module.

Lucene search provider for SXA search

To disable this file, change its extension to ".disabled".

-->
<configuration xmlns:patch="http://www.sitecore.net/xmlconfig/">
  <sitecore>
    <contentSearch>
      <indexConfigurations>
        <defaultLuceneIndexConfiguration type="Sitecore.ContentSearch.LuceneProvider.LuceneIndexConfiguration, Sitecore.ContentSearch.LuceneProvider">
          <analyzer type="Sitecore.ContentSearch.LuceneProvider.Analyzers.PerExecutionContextAnalyzer, Sitecore.ContentSearch.LuceneProvider">
            <param desc="defaultAnalyzer" type="Sitecore.ContentSearch.LuceneProvider.Analyzers.DefaultPerFieldAnalyzer, Sitecore.ContentSearch.LuceneProvider">
              <param patch:instead="*[@desc='defaultAnalyzer']" desc="defaultAnalyzer" type="Sitecore.ContentSearch.LuceneProvider.Analyzers.LowerCaseKeywordAnalyzer, Sitecore.ContentSearch.LuceneProvider" />
            </param>
            <param desc="map" type="System.Collections.Generic.List`1[[Sitecore.ContentSearch.LuceneProvider.Analyzers.PerExecutionContextAnalyzerMapEntry, Sitecore.ContentSearch.LuceneProvider]]">
              <map hint="list:Add">
                <mapEntry patch:instead="*[last()]" />
                <!-- Removing th-TH -->
              </map>
            </param>
          </analyzer>
          <fieldMap type="Sitecore.ContentSearch.FieldMap, Sitecore.ContentSearch">
            <fieldNames hint="raw:AddFieldByFieldName">
              <field fieldName="sxacontent" storageType="NO" indexType="TOKENIZED" vectorType="NO" boost="1f" type="System.String" settingType="Sitecore.ContentSearch.LuceneProvider.LuceneSearchFieldConfiguration, Sitecore.ContentSearch.LuceneProvider">
                <analyzer type="Lucene.Net.Analysis.Standard.StandardAnalyzer, Lucene.Net">
                  <param hint="version">Lucene_30</param>
                </analyzer>
              </field>
              <field fieldName="haslayout" storageType="NO" indexType="TOKENIZED" vectorType="NO" boost="1f" type="System.Boolean" settingType="Sitecore.ContentSearch.LuceneProvider.LuceneSearchFieldConfiguration, Sitecore.ContentSearch.LuceneProvider" />
              <field fieldName="searchable" storageType="NO" indexType="TOKENIZED" vectorType="NO" boost="1f" type="System.Boolean" settingType="Sitecore.ContentSearch.LuceneProvider.LuceneSearchFieldConfiguration, Sitecore.ContentSearch.LuceneProvider" />
              <field fieldName="ispoi" storageType="NO" indexType="TOKENIZED" vectorType="NO" boost="1f" type="System.Boolean" settingType="Sitecore.ContentSearch.LuceneProvider.LuceneSearchFieldConfiguration, Sitecore.ContentSearch.LuceneProvider" />
              <field fieldName="geolocation__x" storageType="NO" indexType="TOKENIZED" vectorType="NO" boost="1f" type="System.Double" settingType="Sitecore.ContentSearch.LuceneProvider.LuceneSearchFieldConfiguration, Sitecore.ContentSearch.LuceneProvider" />
              <field fieldName="geolocation__y" storageType="NO" indexType="TOKENIZED" vectorType="NO" boost="1f" type="System.Double" settingType="Sitecore.ContentSearch.LuceneProvider.LuceneSearchFieldConfiguration, Sitecore.ContentSearch.LuceneProvider" />
            </fieldNames>
            <fieldTypes>
              <fieldType fieldTypeName="tag treelist" storageType="NO" indexType="TOKENIZED" vectorType="NO" boost="1f" type="System.String" settingType="Sitecore.ContentSearch.LuceneProvider.LuceneSearchFieldConfiguration, Sitecore.ContentSearch.LuceneProvider" />
            </fieldTypes>
          </fieldMap>
          <exclude hint="list:ExcludeField">
            <!--TODO: Move these exclude fields to MVC or a place where BodyCSSClass template will be located --> 
            <BodyCssClass>{35BCB8FF-3FFF-4325-B7BA-AF856C76A5FD}</BodyCssClass>
            <BodyId>{D5ED90CB-1BAA-49E5-8EA3-55FEBD502918}</BodyId>
            <TrackingIDs>{75A7BAC0-DE28-4A5F-A346-9E90E4A794F0}</TrackingIDs>
          </exclude>
          <fields hint="raw:AddComputedIndexField">
            <field fieldName="site"><patch:delete /></field>
            <field fieldName="site">Sitecore.XA.Foundation.Search.ComputedFields.Site, Sitecore.XA.Foundation.Search</field>
            <field fieldName="sxacontent" type="Sitecore.XA.Foundation.Search.ComputedFields.AggregatedContent, Sitecore.XA.Foundation.Search">
              <mediaIndexing ref="contentSearch/indexConfigurations/defaultLuceneIndexConfiguration/mediaIndexing" />
            </field>
            <field fieldName="haslayout">Sitecore.XA.Foundation.Search.ComputedFields.HasLayout, Sitecore.XA.Foundation.Search</field>
            <field fieldName="searchable">Sitecore.XA.Foundation.Search.ComputedFields.Searchable, Sitecore.XA.Foundation.Search</field>
            <field fieldName="ispoi">Sitecore.XA.Foundation.Search.ComputedFields.IsPoi, Sitecore.XA.Foundation.Search</field>
            <field fieldName="geolocation__x">Sitecore.XA.Foundation.Search.Providers.Lucene.ComputedFields.LuceneCoordinate, Sitecore.XA.Foundation.Search.Providers.Lucene</field>
            <field fieldName="geolocation__y">Sitecore.XA.Foundation.Search.Providers.Lucene.ComputedFields.LuceneCoordinate, Sitecore.XA.Foundation.Search.Providers.Lucene</field>
          </fields>
          <fieldReaders type="Sitecore.ContentSearch.FieldReaders.FieldReaderMap, Sitecore.ContentSearch">
            <mapFieldByTypeName hint="raw:AddFieldReaderByFieldTypeName">
              <fieldReader fieldTypeName="integer" fieldNameFormat="{0}" fieldReaderType="Sitecore.XA.Foundation.Search.FieldReaders.NumericFieldReader, Sitecore.XA.Foundation.Search" />
              <fieldReader fieldTypeName="number" fieldNameFormat="{0}" fieldReaderType="Sitecore.XA.Foundation.Search.FieldReaders.PrecisionNumericFieldReader, Sitecore.XA.Foundation.Search" />
              <fieldReader fieldTypeName="tag treelist" fieldNameFormat="{0}" fieldReaderType="Sitecore.XA.Foundation.Search.FieldReaders.NameLookupListFieldReader, Sitecore.XA.Foundation.Search" />
            </mapFieldByTypeName>
          </fieldReaders>
        </defaultLuceneIndexConfiguration>
      </indexConfigurations>
      <configuration type="Sitecore.ContentSearch.ContentSearchConfiguration, Sitecore.ContentSearch">
        <indexes hint="list:AddIndex">
          <index id="sitecore_analytics_index" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
          </index>
          <index id="sitecore_core_index" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
            <locations hint="list:AddCrawler">
              <crawler type="Sitecore.ContentSearch.SitecoreItemCrawler, Sitecore.ContentSearch">
                <patch:attribute name="type">Sitecore.XA.Foundation.VersionSpecific.Search.SitecoreItemCrawler, Sitecore.XA.Foundation.VersionSpecific</patch:attribute>
              </crawler>
            </locations>
          </index>
          <index id="sitecore_master_index" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
            <locations hint="list:AddCrawler">
              <crawler type="Sitecore.ContentSearch.SitecoreItemCrawler, Sitecore.ContentSearch">
                <patch:attribute name="type">Sitecore.XA.Foundation.VersionSpecific.Search.SitecoreItemCrawler, Sitecore.XA.Foundation.VersionSpecific</patch:attribute>
              </crawler>
            </locations>
          </index>
          <index id="sitecore_web_index" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
            <locations hint="list:AddCrawler">
              <crawler type="Sitecore.ContentSearch.SitecoreItemCrawler, Sitecore.ContentSearch">
                <patch:attribute name="type">Sitecore.XA.Foundation.VersionSpecific.Search.SitecoreItemCrawler, Sitecore.XA.Foundation.VersionSpecific</patch:attribute>
              </crawler>
            </locations>
          </index>
          <index id="sitecore_testing_index" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
          </index>
          <index id="sitecore_suggested_test_index" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
          </index>
          <index id="sitecore_fxm_master_index" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
          </index>
          <index id="sitecore_fxm_web_index" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
          </index>
          <index id="sitecore_list_index" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
          </index>
          <index id="social_messages_master" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
          </index>
          <index id="social_messages_web" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
          </index>
          <index id="sitecore_marketing_asset_index_master" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
          </index>
          <index id="sitecore_marketing_asset_index_web" type="Sitecore.ContentSearch.LuceneProvider.LuceneIndex, Sitecore.ContentSearch.LuceneProvider">
            <patch:attribute name="type">Sitecore.XA.Foundation.Search.Providers.Lucene.LuceneIndex, Sitecore.XA.Foundation.Search.Providers.Lucene</patch:attribute>
          </index>
        </indexes>
      </configuration>
    </contentSearch>
  </sitecore>
</configuration>