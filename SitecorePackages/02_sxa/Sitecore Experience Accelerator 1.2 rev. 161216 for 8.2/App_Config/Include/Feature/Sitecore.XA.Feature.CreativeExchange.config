<?xml version="1.0" encoding="utf-8" ?>

<!--

Purpose: This include file enables and configures the Creative Exchange feature.

The Creative Exchange process is designed to facilitate the different teams that work together to create a website. For example, the front-end developers that are working on the theme or "skin" for the website should be able to work in parallel with the other teams and keep using their favourite tools.

You can export the site for the front-end developers as a ZIP file of the whole site with all the wireframes and content included, so they can skin it. 
This ZIP file is then sent back to be imported again, and the process can continue. We recommend that this is done in iterations as part of an agile methodology, so that your customers get the best possible website.

To disable this file, change its extension to ".disabled".

-->

<configuration xmlns:patch="http://www.sitecore.net/xmlconfig/">
  <sitecore>
    <settings>
      <!-- Skip server certificate validation
           You can skip server certificate validation while using Creative Exchange with self signed certificates
      -->
      <setting name="XA.Feature.CreativeExchange.SkipServerCertificateValidation" value="false" />
    </settings>
    <commands>
      <command name="ce:ribbon:exportsite" type="Sitecore.XA.Feature.CreativeExchange.Commands.ExportSiteCommand, Sitecore.XA.Feature.CreativeExchange"/>
      <command name="ce:ribbon:importsite" type="Sitecore.XA.Feature.CreativeExchange.Commands.ImportSiteCommand, Sitecore.XA.Feature.CreativeExchange"/>
    </commands>
    <pipelines>
      <ioc>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.IoC.RegisterCreativeExchangeServices, Sitecore.XA.Feature.CreativeExchange" />
      </ioc>
      <renderField>
        <processor patch:before="*[@type='Sitecore.Pipelines.RenderField.GetFieldValue, Sitecore.Kernel']" type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.RenderField.SetCreativeExchangeAttributes, Sitecore.XA.Feature.CreativeExchange"/>
      </renderField>
      <decorateRendering>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.DecorateRendering.AddCustomClass, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.DecorateRendering.AddPageId, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.DecorateRendering.AddRenderingId, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.DecorateRendering.AddVariantId, Sitecore.XA.Feature.CreativeExchange" />
      </decorateRendering>
      <decoratePage>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.DecoratePage.AddCustomClass, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.DecoratePage.AddPageId, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.DecoratePage.AddDeviceId, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.DecoratePage.AddLanguage, Sitecore.XA.Feature.CreativeExchange" />
      </decoratePage>
      <ceExport.getExportContext>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.GetExportContext.GetHost, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.GetExportContext.GetDevice, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.GetExportContext.GetLanguage, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.GetExportContext.GetMarkupMode, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.GetExportContext.GetExportScope, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.GetExportContext.GetStorageServiceDefinition, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.GetExportContext.GetStartItem, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.GetExportContext.GetSiteName, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.GetExportContext.GetFileSizeLimit, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.GetExportContext.GetBucketExportMode, Sitecore.XA.Feature.CreativeExchange" />
      </ceExport.getExportContext>
      <ceExport.pageValidation>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageValidation.ValidateLanguageVersion, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageValidation.ValidatePageLayout, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageValidation.ValidateBaseTemplate, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageValidation.ValidateUnpublishable, Sitecore.XA.Feature.CreativeExchange" />
      </ceExport.pageValidation>
      <ceExport.pageProcessing>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageProcessing.PageRequest, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageProcessing.RemoveSitecoreMarkup, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageProcessing.PageAssetExtract, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageProcessing.FixInternalAssetsLinks, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageProcessing.FixInternalLinks, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageProcessing.UpdatePageHtmlLinks, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageProcessing.AddBaseTag, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.PageProcessing.StorePage, Sitecore.XA.Feature.CreativeExchange" />
      </ceExport.pageProcessing>
      <ceExport.export>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.Export.BlobPersistanceInstatiation, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.Export.ExportStorageConfiguration, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.Export.ExportServicesInstatiation, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.Export.SiteAssetEnumeration, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.Export.PageEnumeration, Sitecore.XA.Feature.CreativeExchange" />
      </ceExport.export>
      <ceExport.siteAssetsEnumeration>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.SiteAssetsEnumeration.ThemeEnumerator, Sitecore.XA.Feature.CreativeExchange" />
      </ceExport.siteAssetsEnumeration>
      <ceExport.storeAsset>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.StoreAsset.ResolveMediaItem, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.StoreAsset.CheckDuplicates, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.StoreAsset.CheckFileSize, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.StoreAsset.NormalizeMediaItemName, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.StoreAsset.StoreAsset, Sitecore.XA.Feature.CreativeExchange" />
      </ceExport.storeAsset>
      <ceImport.getImportContext>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.GetImportContext.GetStoragePath, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.GetImportContext.GetStorageServiceDefinition, Sitecore.XA.Feature.CreativeExchange" />
      </ceImport.getImportContext>
      <ceImport.import>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.Import.BlobProviderInstatiation, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.Import.ImportStorageConfiguration, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.Import.AssetEnumeration, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.Import.ThemeAssetsValidation, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.Import.PageEnumeration, Sitecore.XA.Feature.CreativeExchange" />
      </ceImport.import>
      <ceImport.importPageProcessing>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.PageProcessing.PageValidation, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.PageProcessing.PageDecoration, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.PageProcessing.LinkedAssetsImporter, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.PageProcessing.RenderingParser, Sitecore.XA.Feature.CreativeExchange" />
      </ceImport.importPageProcessing>
      <ceImport.importRenderingProcessing>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.RenderingProcessing.RenderingDecorationParser, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.RenderingProcessing.RenderingDetailsExtractor, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.RenderingProcessing.SystemClassesParser, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.RenderingProcessing.VariantClassesParser, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.RenderingProcessing.StylesImporter, Sitecore.XA.Feature.CreativeExchange" />
      </ceImport.importRenderingProcessing>
      <ceImport.assetProcessing>
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.AssetProcessing.NameValidator, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.AssetProcessing.LocationValidator, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.AssetProcessing.ContainerValidation, Sitecore.XA.Feature.CreativeExchange" />
        <processor type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.AssetProcessing.BlobImporter, Sitecore.XA.Feature.CreativeExchange" />
      </ceImport.assetProcessing>
    </pipelines>
    <sitecore.experienceeditor.speak.requests>
      <request name="ExperienceEditor.CreativeExchange.CanRunCreativeExchangeCommands" type="Sitecore.XA.Feature.CreativeExchange.Requests.CanRunCreativeExchangeCommands, Sitecore.XA.Feature.CreativeExchange"/>
    </sitecore.experienceeditor.speak.requests>
    <processors>
      <ceExport.ui argsType="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.ExportUI.ExportUiArgs">
        <processor mode="on" type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Export.ExportUI.GetDialog,Sitecore.XA.Feature.CreativeExchange" />
      </ceExport.ui>
      <ceImport.ui argsType="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.ImportUI.ImportUiArgs">
        <processor mode="on" type="Sitecore.XA.Feature.CreativeExchange.Pipelines.Import.ImportUI.GetDialog,Sitecore.XA.Feature.CreativeExchange" />
      </ceImport.ui>
    </processors>
    <experienceAccelerator>
      <creativeExchange>
        <!-- The list of locations protected from modfication while importing. -->
        <restrictedLocations>
          <path desc="Basic">/sitecore/media library/Themes/Basic</path>
          <path desc="Wireframe">/sitecore/media library/Themes/Wireframe</path>
        </restrictedLocations>
        <sc.variable name="creativeExchangeFolder" value="$(dataFolder)/packages/CreativeExchange" />
      </creativeExchange>
    </experienceAccelerator>
  </sitecore>
</configuration>
