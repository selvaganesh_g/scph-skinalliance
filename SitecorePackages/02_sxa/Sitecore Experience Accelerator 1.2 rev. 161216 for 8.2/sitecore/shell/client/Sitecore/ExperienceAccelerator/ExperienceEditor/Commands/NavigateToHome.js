﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    Sitecore.Commands.NavigateToHome =
    {
        canExecute: function (context) {
            return true;
        },
        execute: function (context) {
            ExperienceEditor.modifiedHandling(true, function() {
                var requestContext = context.app.clone(context.currentContext);
                requestContext.target = "$home";
                ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.XA.GetItemUrl", function(response) {
                    if (response.responseValue.value) {
                        window.parent.location = response.responseValue.value;
                    }
                }, requestContext).execute(context);
            });
        },
    };
});