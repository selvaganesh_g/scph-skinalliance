﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    Sitecore.Commands.PublishSite =
    {
        canExecute: function (context) {
            return true;
        },
        execute: function (context) {
            ExperienceEditor.modifiedHandling(true, function () {
                var requestContext = context.app.clone(context.currentContext);
                requestContext.target = "$site";
                ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.XA.CheckWorkflow", function (response) {
                    if (!response.responseValue.value) {
                        return;
                    }

                    var dialogPath = "/sitecore/shell/Applications/Publish.aspx?id=" + response.responseValue.value;
                    var dialogFeatures = "dialogHeight: 600px;dialogWidth: 500px;";
                    ExperienceEditor.Dialogs.showModalDialog(dialogPath, '', dialogFeatures);
                }, requestContext).execute(context);
            });
        },
    };
});