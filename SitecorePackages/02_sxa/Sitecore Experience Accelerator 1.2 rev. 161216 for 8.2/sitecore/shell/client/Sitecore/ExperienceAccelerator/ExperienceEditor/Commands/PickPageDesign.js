﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    Sitecore.Commands.PickPageDesign =
    {
        canExecute: function (context) {
            var requestContext = context.app.clone(context.currentContext);
            requestContext.field = "Page Design";
            var canEdit = context.app.canExecute("ExperienceEditor.XA.CanEditField", requestContext);
            return canEdit;
        },
        execute: function (context) {
            var requestContext = context.app.clone(context.currentContext);
            requestContext.button = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Set Page Design Explicitly/Edit";
            ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.XA.GetFieldEditorDialog", function (response1) {
                if (!response1.responseValue.value) {
                    return;
                }

                var dialogFeatures = "dialogHeight:720px;dialogWidth:520px;";
                ExperienceEditor.Dialogs.showModalDialog(response1.responseValue.value, '', dialogFeatures, null, function (result) {
                    if (!result) {
                        return;
                    }

                    ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.XA.GetFieldEditorResult", function (response2) {
                        if (!response2.responseValue.value) {
                            return;
                        }

                        window.parent.Sitecore.PageModes.ChromeManager.handleMessage('chrome:editframe:updatestart');
                        response2.responseValue.value.forEach(function (field) {
                            var uri = new window.parent.Sitecore.ItemUri(field.ItemId, field.Language, field.Version, field.Revision);
                            window.parent.Sitecore.WebEdit.setFieldValue(uri, field.FieldId, field.Value);
                        });
                        window.parent.Sitecore.PageModes.ChromeManager.handleMessage('chrome:editframe:updateend');
                    }, { value: result }).execute(context);
                });
            }, requestContext).execute(context);
        },
    };
});