﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    //initializer helper
    var initializeHelper = function (initializeFunction) {
        var trialsCount = 1,
            tryInitialize,
            initSuccess = false;
        tryInitialize = setInterval(function () {
            if (initSuccess && typeof (tryInitialize) !== "undefined") {
                clearInterval(tryInitialize);
                return;
            }

            try {
                initializeFunction();
                initSuccess = true;
            } catch (ex) {
                if (++trialsCount > 20) {
                    clearInterval(tryInitialize);
                }
                return;
            }
        }, 500);
    },
    shouldInitialize = false;
    //initializer helper end
    Sitecore.Commands.ToggleDragNDrop =
    {
        canExecute: function (context) {
            if (!ExperienceEditor.isInMode("edit")) {
                return false;
            }
            var canDesign = context.app.canExecute("ExperienceEditor.EnableDesigning.CanDesign", context.currentContext);

            var self = this;
            var isEnabled = context.app.canExecute("ExperienceEditor.XA.IsDragNDropEnabled", context.currentContext);
            var isChecked = context.button.get("isChecked") == "1";
            if ((isEnabled && isChecked) === true) {
                self.register();
            } else {
                self.unregister();
            }
            return isEnabled && canDesign;
        },
        execute: function (context) {
            var self = this;
            ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.ToggleRegistryKey.Toggle", function (response) {
                response.context.button.set("isChecked", response.responseValue.value ? "1" : "0");
                if (response.responseValue.value) {
                    self.register();
                }
                else {
                    self.unregister();
                }
            }, { value: context.button.get("registryKey") }).execute(context);
        },
        register: function () {
            shouldInitialize = true;
            initializeHelper(function () {
                if (shouldInitialize) {
                    window.parent.$xa.dragndrop.init();
                    window.parent.$xa.toolbox.init();
                }
            });
        },
        unregister: function () {
            shouldInitialize = false;
            initializeHelper(function () {
                if (!shouldInitialize) {
                    window.parent.$xa.dragndrop.remove();
                    window.parent.$xa.toolbox.remove();
                }
            });
        }
    };
});