﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    Sitecore.Commands.ToggleGridLinesOnTop =
    {
        canExecute: function (context) {
            var self = this,
                isChecked = context.button.get("isChecked") == "1";
            //self.refresh(context);

            var showGridLinesEnabled = false;
            var showGridLines = ExperienceEditor.CommandsUtil.getControlsByCommand(ExperienceEditor.getContext().instance.Controls, "ToggleShowGridLines");
            if (showGridLines.length > 0 && showGridLines[0] && showGridLines[0].model) {
                showGridLinesEnabled = showGridLines[0].model.get("isChecked") == "1";
            }

            if (isChecked) {
                self.register();
            }
            return showGridLinesEnabled;
        },
        execute: function (context) {
            var self = this;
            ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.ToggleRegistryKey.Toggle", function (response) {
                response.context.button.set("isChecked", response.responseValue.value ? "1" : "0");

                if (response.responseValue.value) {
                    self.register();
                    self.refresh(context);
                }
                else {
                    self.unregister();
                    self.refresh(context);
                }
                //self.refresh(context);
            }, {value: context.button.get("registryKey")}).execute(context);
        },
        refresh: function () {
            var self = this;
            var flags = [self.isCheckedAndEnabled("ToggleShowGridLines"), self.isCheckedAndEnabled("ToggleGridLinesOnTop")];
            $('body', window.parent.document).trigger('Sitecore.Ribbon.RefreshedEx', flags);
        },
        isCheckedAndEnabled: function (commandName) {
            var command = ExperienceEditor.CommandsUtil.getControlsByCommand(ExperienceEditor.getContext().instance.Controls, commandName);
            if (command.length > 0 && command[0] && command[0].model) {
                return command[0].model.get("isEnabled") == "1" && command[0].model.get("isChecked") == "1";
            }
            return false;
        },
        register: function () {
            if (typeof(window.parent.$xa) !== "undefined" && window.parent.$xa.shouldInitialize()) {
                window.parent.$xa.gridlines.init(true, true);
            }
        },
        unregister: function () {
            if (typeof(window.parent.$xa) !== "undefined" && window.parent.$xa.shouldInitialize()) {
                window.parent.$xa.gridlines.init(true, true);
            }
        }
    };
});