﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {

    var setCookieWarningVisibility = function (context) {
        var cookies = window.parent.document.querySelectorAll(".cookie-notification"),
            isVisible = context.button.get("isChecked") == "1",
            c;
        for (c = 0; c < cookies.length; c++) {
            cookies[c].style.display = isVisible ? "block" : "none";
        }
    };

    Sitecore.Commands.ToggleCookieWarning =
    {
        canExecute: function (context) {
            if (context.currentContext.webEditMode !== "preview") {
                setCookieWarningVisibility(context);
                return true;
            }
            return false;
        },
        execute: function (context) {
            ExperienceEditor.modifiedHandling(true, function () {
                ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.ToggleRegistryKey.Toggle", function (response) {
                    response.context.button.set("isChecked", response.responseValue.value ? "1" : "0");
                    setCookieWarningVisibility(response.context);
                }, { value: context.button.get("registryKey") }).execute(context);
            });
        },
    };
});