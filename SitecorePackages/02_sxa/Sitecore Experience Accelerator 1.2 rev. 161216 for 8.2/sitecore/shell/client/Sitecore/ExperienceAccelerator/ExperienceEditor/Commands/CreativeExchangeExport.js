﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    Sitecore.Commands.CreativeExchangeExport =
    {
        canExecute: function (context) {
            var requestContext = context.app.clone(context.currentContext);
            requestContext.target = context.currentContext.value;
            var canEdit = context.app.canExecute("ExperienceEditor.CreativeExchange.CanRunCreativeExchangeCommands", requestContext);
            return canEdit;
        },
        execute: function (context) {
            var id = context.currentContext.itemId;
            var lang = context.currentContext.language;
            var ver = context.currentContext.version;

            var path = "/sitecore/shell/default.aspx?xmlcontrol=ExportSiteWizard" + "&id=" + id + "&lang=" + lang + "&ver=" + ver;

            var features = "dialogHeight: 600px;dialogWidth: 480px;";

            ExperienceEditor.Dialogs.showModalDialog(path, '', features);
        },
    };
});