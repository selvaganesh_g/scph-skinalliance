﻿define(
  [
    "sitecore"
  ],
  function () {
      var presentation = {
      };
      presentation.templates = {
          PartialDesign: "{BF680756-B2FA-4CAE-8B69-EE361080616F}",
          _DesignTemplateMapping: "{71B1F3CD-4460-430F-82EC-9CE310ABCB7C}",
          PageDesign: "{EBCFFAE1-B6B4-4B50-B722-155E869FC430}"
      }
      return presentation;
  });