﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    Sitecore.Commands.PublishDropDown =
    {
        canExecute: function (context) {
            var isEnabled = context.app.canExecute("ExperienceEditor.XA.IsPublishingEnabled", context.currentContext);
            return isEnabled;
        },
    };
});