﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    Sitecore.Commands.ToggleWireframeImages =
    {
        canExecute: function (context) {
            var state;
            ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.XA.GetWireframeImagesState", function (response) {
                state = response.responseValue.value;
                if (state !== null) {
                    response.context.button.set("isChecked", state ? "1" : "0");
                }
            }).execute(context);
            return state !== null;
        },
        execute: function (context) {
            ExperienceEditor.modifiedHandling(true, function () {
                ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.XA.ToggleWireframeImagesState", function (response) {
                    window.top.location.reload();
                }).execute(context);
            });
        },
    };
});