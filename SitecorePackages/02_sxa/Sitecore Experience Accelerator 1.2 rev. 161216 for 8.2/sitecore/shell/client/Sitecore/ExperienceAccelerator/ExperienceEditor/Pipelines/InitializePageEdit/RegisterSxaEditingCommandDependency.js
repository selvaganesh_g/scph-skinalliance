﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    return {
        priority: 1,
        execute: function (context) {
            var dependentCommands = context.app.LockCommandDependency.viewModel.dependantCommands();
            if (dependentCommands) {
                var commandsArray = dependentCommands.split('|');
                commandsArray.push("ToggleDragNDrop");
                var newValue = commandsArray.join('|');
                context.app.LockCommandDependency.set("dependantCommands", newValue);
            }
        }
    };
});