﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    return {
        priority: 1,

        execute: function (context) {
            var isInSxaContext = context.app.canExecute("ExperienceEditor.XA.IsInSxaContext", context.currentContext);
            if (!isInSxaContext) {
                var sxaRibbonButtons = ["ShowGridLinesRibbonButton", "ShowDragNDropRibbonButton", "ShowCookieWarningRibbonButton", "ShowStickyNotesRibbonButton"];

                context.commands = context.commands.filter(function (e) {
                    return e.stripId !== "ExperienceAcceleratorStrip";
                });

                context.commands = context.commands.filter(function (e) {
                    var name = e.initiator.viewModel.name();
                    name = name.substring(0, name.indexOf("_"));
                    return sxaRibbonButtons.indexOf(name) < 0;
                });
                var experienceAcceleratorStrip = document.querySelector('[stripid=ExperienceAcceleratorStrip]');
                if (experienceAcceleratorStrip) {
                    experienceAcceleratorStrip.hide();
                }

                var experienceAcceleratorViewChunk = document.querySelector('[data-sc-id=ExperienceAcceleratorViewChunk]');
                if (experienceAcceleratorViewChunk) {
                    experienceAcceleratorViewChunk.hide();
                }
            }
        }
    };
});