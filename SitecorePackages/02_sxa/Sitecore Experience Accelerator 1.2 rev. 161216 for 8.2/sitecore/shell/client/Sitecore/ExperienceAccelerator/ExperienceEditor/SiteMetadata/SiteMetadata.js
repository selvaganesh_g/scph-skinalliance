﻿define(
  [
    "sitecore"
  ],
  function () {
      var siteMetadata = {
      };
      siteMetadata.templates = {
          _SeoMetadata: "{9B5B9E0E-1CEF-4D54-8C47-27F241CE075B}",
          _OpenGraphMetadata: "{49B99587-1114-4BEB-894B-1455EBDC0C41}"
      }
      return siteMetadata;
  });