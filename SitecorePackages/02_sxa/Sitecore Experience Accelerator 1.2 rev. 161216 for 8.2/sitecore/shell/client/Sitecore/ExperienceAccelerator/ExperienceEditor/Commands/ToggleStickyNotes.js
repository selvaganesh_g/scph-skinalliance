﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    //initializer helper
    var initializeHelper = function (initializeFunction) {
        var trialsCount = 1,
            tryInitialize,
            initSuccess = false;
        tryInitialize = setInterval(function () {
            if (initSuccess && typeof (tryInitialize) !== "undefined") {
                clearInterval(tryInitialize);
                return;
            }

            try {
                initializeFunction();
                initSuccess = true;
            } catch (ex) {
                if (++trialsCount > 20) {
                    clearInterval(tryInitialize);
                }
                return;
            }
        }, 500);
    },
    shouldInitialize = false;
    //initializer helper end
    Sitecore.Commands.ToggleStickyNotes =
    {
        canExecute: function (context) {
            var self = this;
            var isEnabled = ExperienceEditor.Web.getUrlQueryStringValue("CreativeExchangeExport") != "true";
            var isChecked = context.button.get("isChecked") == "1";
            if (isEnabled && isChecked) {
                self.register();
            }
            return isEnabled;
        },
        execute: function (context) {
            var self = this;
            ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.ToggleRegistryKey.Toggle", function (response) {
                response.context.button.set("isChecked", response.responseValue.value ? "1" : "0");
                if (response.responseValue.value) {
                    self.register();
                }
                else {
                    self.unregister();
                }
            }, {value: context.button.get("registryKey")}).execute(context);
        },
        register: function () {
            shouldInitialize = true;
            initializeHelper(function () {
                if (shouldInitialize) {
                    window.parent.$xa.stickynotes.init(true);
                }
            });
        },
        unregister: function () {
            shouldInitialize = false;
            initializeHelper(function () {
                if (!shouldInitialize) {
                    window.parent.$xa.stickynotes.remove();
                }
            });
        }
    };
});