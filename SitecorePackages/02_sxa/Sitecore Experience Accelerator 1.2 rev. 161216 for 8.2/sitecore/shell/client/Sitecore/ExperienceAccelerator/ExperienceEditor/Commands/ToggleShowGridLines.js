﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    //initializer helper
    var initializeHelper = function (initializeFunction) {
        var trialsCount = 1,
            tryInitialize,
            initSuccess = false;
        tryInitialize = setInterval(function () {
            if (initSuccess && typeof (tryInitialize) !== "undefined") {
                clearInterval(tryInitialize);
                return;
            }

            try {
                initializeFunction();
                initSuccess = true;
            } catch (ex) {
                if (++trialsCount > 20) {
                    clearInterval(tryInitialize);
                }
                return;
            }
        }, 500);
    },
    shouldInitialize = false;
    //initializer helper end
    Sitecore.Commands.ToggleShowGridLines =
    {
        canExecute: function (context) {
            var self = this,
                isChecked = context.button.get("isChecked") == "1";

            if (isChecked) {
                self.register();
            }
            return true;
        },
        execute: function (context) {
            var self = this;
            ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.ToggleRegistryKey.Toggle", function (response) {
                response.context.button.set("isChecked", response.responseValue.value ? "1" : "0");

                var gridLinesOnTop = ExperienceEditor.CommandsUtil.getControlsByCommand(ExperienceEditor.getContext().instance.Controls, "ToggleGridLinesOnTop");
                if (gridLinesOnTop.length > 0 && gridLinesOnTop[0] && gridLinesOnTop[0].view) {
                    gridLinesOnTop[0].view.set({ isEnabled: response.responseValue.value });
                }

                if (response.responseValue.value) {
                    self.register();
                    self.refresh(context);
                }
                else {
                    self.unregister();
                    self.refresh(context);
                }
                //
            }, { value: context.button.get("registryKey") }).execute(context);
        },
        refresh: function () {
            var self = this;
            var flags = [self.isCheckedAndEnabled("ToggleShowGridLines"), self.isCheckedAndEnabled("ToggleGridLinesOnTop")];
            $('body', window.parent.document).trigger('Sitecore.Ribbon.RefreshedEx', flags);
        },
        isCheckedAndEnabled: function (commandName) {
            var command = ExperienceEditor.CommandsUtil.getControlsByCommand(ExperienceEditor.getContext().instance.Controls, commandName);
            if (command.length > 0 && command[0] && command[0].model) {
                return command[0].model.get("isEnabled") == "1" && command[0].model.get("isChecked") == "1";
            }
            return false;
        },
        register: function () {
            shouldInitialize = true;
            initializeHelper(function () {
                if (shouldInitialize) {
                    window.parent.$xa.gridlines.init();
                }
            });
        },
        unregister: function () {
            shouldInitialize = false;
            initializeHelper(function () {
                if (!shouldInitialize) {
                    window.parent.$xa.gridlines.remove();
                }
            });
        }
    };
});