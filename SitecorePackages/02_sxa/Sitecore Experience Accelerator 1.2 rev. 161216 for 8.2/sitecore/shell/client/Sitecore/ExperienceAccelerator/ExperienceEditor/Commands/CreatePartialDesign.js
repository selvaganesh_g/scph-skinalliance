﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    Sitecore.Commands.CreatePartialDesign =
    {
        canExecute: function (context) {
            var requestContext = context.app.clone(context.currentContext);
            requestContext.target = context.currentContext.parent;
            var canCreate = context.app.canExecute("ExperienceEditor.XA.CanCreateItem", requestContext);
            return canCreate;
        },
        execute: function (context) {
            var requestContext = context.app.clone(context.currentContext);
            requestContext.target = context.currentContext.parent;
            ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.XA.GetItemId", function (response1) {
                if (!response1.responseValue.value) {
                    return;
                }

                var dialogPath = "/sitecore/client/Applications/ExperienceEditor/Dialogs/InsertPage/?itemId=" + response1.responseValue.value;
                var dialogFeatures = "dialogHeight: 600px;dialogWidth: 800px; ignoreSpeakSizes: true;";
                ExperienceEditor.Dialogs.showModalDialog(dialogPath, '', dialogFeatures, null, function (result) {
                    if (!result) {
                        return;
                    }

                    var responseArray = result.split(',');
                    if (responseArray.length != 2) {
                        return;
                    }

                    context.currentContext.itemId = response1.responseValue.value;
                    context.currentContext.templateItemId = responseArray[0];
                    context.currentContext.name = responseArray[1];
                    ExperienceEditor.PipelinesUtil.generateRequestProcessor("ExperienceEditor.Insert", function (response2) {
                        var itemId = response2.responseValue.value.itemId;
                        if (itemId == null || itemId.length <= 0) {
                            ExperienceEditor.Dialogs.alert(ExperienceEditor.TranslationsUtils.translateText(ExperienceEditor.TranslationsUtils.keys.Could_not_create_item));
                            return;
                        }

                        var partialDesignDropDown = ExperienceEditor.CommandsUtil.getControlsByCommand(ExperienceEditor.getContext().instance.Controls, "PartialDesignDropDown");
                        if (partialDesignDropDown.length > 0 && partialDesignDropDown[0] && partialDesignDropDown[0].model && partialDesignDropDown[0].model.viewModel) {
                            partialDesignDropDown[0].model.viewModel.retrieveDropDownItems();
                        }

                        context.currentContext.value = itemId;
                        if (ExperienceEditor.CommandsUtil.runCommandCanExecute("PartialDesignDropDown", context)) {
                            context.currentContext.argument = itemId;
                            ExperienceEditor.CommandsUtil.runCommandExecute("PartialDesignDropDown", context);
                        }
                    }).execute(context);
                });
            }, requestContext).execute(context);
        },
    };
});