﻿define([], function () {
  var condition = function (context, args) {
    var control = context.app,
		parts = args.name.split('.'),
		getTemplateId = function(control){
			if (typeof control.attributes.selectedNode !== 'undefined'){
				if (typeof control.attributes.selectedNode.rawItem !== 'undefined'){
					if (typeof control.attributes.selectedNode.rawItem.$templateId !== 'undefined'){
						return control.attributes.selectedNode.rawItem.$templateId;
					}
				}
			}
			return null;
		},
		templateId;
		
    for (var n = 0; n < parts.length; n++) {
      control = control[parts[n]];

      if (control == null) {
        break;
      }
    }

    if (control == null) {
      throw "Control '" + name + "' not found";
    }
	var selectedItemId = control.get("selectedItemId");
    if (!selectedItemId) {
      return false;
    }
	else
	{
		templateId = getTemplateId(control);
		if (templateId !=null)
		{
		    if (templateId === "{E8E8C94F-4248-43C3-A79F-99FBB49D78E6}")
			{
				return true;
			}
			return false;			
		}
		
		var selectedItemTitle = control.attributes.selectedNode.title;
		if (templateId === null && selectedItemTitle === "Media")
		{
			return true;
		}
		return false;			
	}
    return false;
  };
  return condition;
});

