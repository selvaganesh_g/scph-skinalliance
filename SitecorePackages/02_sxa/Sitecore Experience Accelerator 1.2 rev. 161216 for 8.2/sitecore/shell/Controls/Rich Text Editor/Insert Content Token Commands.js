Telerik.Web.UI.Editor.CommandList["InsertContentToken"] = function (commandName, editor, args) {
    scEditor = editor;
    editor.showExternalDialog(
      "/sitecore/shell/default.aspx?xmlcontrol=RichText.InsertContentToken&la=" + window.scLanguage + "&contextItem=" + scItemID + "&fo=" + (scDatabase ? "&databasename=" + scDatabase : ""),
      null, //argument
      400,
      600,
      scGetVariable,
      null,
      "Insert Content Token",
      true, //modal
      Telerik.Web.UI.WindowBehaviors.Close, // behaviors
      false, //showStatusBar
      false //showTitleBar
    );
};

function scGetVariable(sender, returnValue) {
    if (returnValue) {
        scEditor.pasteHtml(returnValue.variable);
    }
}